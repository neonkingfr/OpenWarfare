class CfgFSMs
{
  /*%FSM<COMPILE "C:\Bis\fsmeditor\entityFSM.cfg, Dragonfly">*/
  /*%FSM<HEAD>*/
  /*
  item0[] = {"Random_Move",2,250,-250.000000,-300.000000,-150.000000,-250.000000,0.000000};
  item1[] = {"LongerWait",2,250,-375.000000,-425.000000,-275.000000,-375.000000,0.000000};
  item2[] = {"MoveCompleted",4,218,-650.000000,-300.000000,-550.000000,-250.000000,1.000000};
  item3[] = {"WaitCompleted",4,218,-250.000000,-425.000000,-150.000000,-375.000000,0.000000};
  item4[] = {"Init",0,4346,-250.000000,-650.000000,-150.000000,-600.000000,0.000000};
  item5[] = {"Always",8,218,-125.000000,-500.000000,-25.000000,-450.000000,0.000000};
  item6[] = {"TimeElapsed",4,218,-375.000000,-350.000000,-275.000000,-300.000000,0.000000};
  item7[] = {"SetTimerRandom",2,250,-125.000000,-575.000000,-25.000000,-525.000000,0.000000};
  item8[] = {"Always",8,218,-125.000000,-650.000000,-25.000000,-600.000000,0.000000};
  item9[] = {"WaitCompleted",4,218,-250.000000,-500.000000,-150.000000,-450.000000,0.000000};
  item10[] = {"ShortWait",2,250,-375.000000,-500.000000,-275.000000,-450.000000,0.000000};
  item11[] = {"SetRandom2",2,250,-575.000000,-350.000000,-475.000000,-300.000000,0.000000};
  item12[] = {"constProbability",4,218,-575.000000,-500.000000,-475.000000,-450.000000,1.000000};
  item13[] = {"Always",8,218,-500.000000,-425.000000,-400.000000,-375.000000,0.000000};
  item14[] = {"SetRandom3",2,250,-650.000000,-650.000000,-550.000000,-600.000000,0.000000};
  item15[] = {"constProbability",4,218,-375.000000,-650.000000,-275.000000,-600.000000,1.000000};
  item16[] = {"Always",8,218,-525.000000,-575.000000,-425.000000,-525.000000,0.000000};
  item17[] = {"LongWait",2,250,-375.000000,-575.000000,-275.000000,-525.000000,0.000000};
  item18[] = {"WaitCompleted",4,218,-250.000000,-575.000000,-150.000000,-525.000000,0.000000};
  item19[] = {"SetRandom",2,250,-125.000000,-425.000000,-25.000000,-375.000000,0.000000};
  item20[] = {"constProbability",4,218,-50.000000,-350.000000,50.000000,-300.000000,1.000000};
  item21[] = {"Always",8,218,-150.000000,-350.000000,-50.000000,-300.000000,0.000000};
  item22[] = {"LongMove",2,250,-250.000000,-225.000000,-150.000000,-175.000000,0.000000};
  item23[] = {"TimeElapsed",4,218,-375.000000,-250.000000,-275.000000,-200.000000,0.000000};
  item24[] = {"MoveCompleted",4,218,-650.000000,-225.000000,-550.000000,-175.000000,1.000000};
  item25[] = {"",7,210,-679.191589,-204.748215,-671.191589,-196.748215,0.000000};
  item26[] = {"",7,210,-678.303406,-631.116089,-670.303406,-623.116089,0.000000};
  link0[] = {0,2};
  link1[] = {0,6};
  link2[] = {1,3};
  link3[] = {2,14};
  link4[] = {3,7};
  link5[] = {4,8};
  link6[] = {5,19};
  link7[] = {6,11};
  link8[] = {7,5};
  link9[] = {8,7};
  link10[] = {9,7};
  link11[] = {10,9};
  link12[] = {11,12};
  link13[] = {11,13};
  link14[] = {12,17};
  link15[] = {13,1};
  link16[] = {14,15};
  link17[] = {14,16};
  link18[] = {15,17};
  link19[] = {16,10};
  link20[] = {17,18};
  link21[] = {18,7};
  link22[] = {19,20};
  link23[] = {19,21};
  link24[] = {20,22};
  link25[] = {21,0};
  link26[] = {22,23};
  link27[] = {22,24};
  link28[] = {23,11};
  link29[] = {24,25};
  link30[] = {25,26};
  link31[] = {26,14};
  globals[] = {25.000000,1,0,0,16777215,640,480,1,70,6316128,1,-758.622375,92.310829,91.994301,-707.901489,917,862,1};
  window[] = {2,-1,-1,-1,-1,951,176,1136,232,3,935};
  *//*%FSM</HEAD>*/
  class Dragonfly
  {
    class States
    {
      /*%FSM<STATE "Random_Move">*/
      class Random_Move
      {
        name = "Random_Move";
        class Init
        {
          /*%FSM<STATEINIT>*/
          function = "randomMove";
          parameters[] = {1, -0.1, 1.2, 5.0}; // straightDistance, minHeight, avgHeight, maxHeight 
          thresholds[] = {};                 
          /*%FSM</STATEINIT>*/
        };
        class Links
        {
          
          /*%FSM<LINK "MoveCompleted">*/
          class MoveCompleted
          {
            priority = 1.000000;
            to="SetRandom3";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "moveCompleted";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                          
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                                                                                                                              
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
          
          /*%FSM<LINK "TimeElapsed">*/
          class TimeElapsed
          {
            priority = 0.000000;
            to="SetRandom2";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "timeElapsed";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                                
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                                                                                                                                 
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
        };
      };
      /*%FSM</STATE>*/
      /*%FSM<STATE "LongerWait">*/
      class LongerWait
      {
        name = "LongerWait";
        class Init
        {
          /*%FSM<STATEINIT>*/
          function = "wait";
          parameters[] = {0.8,2.0}; // delay
          thresholds[] = {};                                 
          /*%FSM</STATEINIT>*/
        };
        class Links
        {
          
          /*%FSM<LINK "WaitCompleted">*/
          class WaitCompleted
          {
            priority = 0.000000;
            to="SetTimerRandom";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "waitCompleted";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                                
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                                                                                                                                 
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
        };
      };
      /*%FSM</STATE>*/
      /*%FSM<STATE "Init">*/
      class Init
      {
        name = "Init";
        class Init
        {
          /*%FSM<STATEINIT>*/
          function = "setNoBackwards";
          parameters[] = {1.0};
          thresholds[] = {{0, 0.5, 0.5}}; // set slot 0 to 0.5                                        
          /*%FSM</STATEINIT>*/
        };
        class Links
        {
          
          /*%FSM<LINK "Always">*/
          class Always
          {
            priority = 0.000000;
            to="SetTimerRandom";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "true";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                                
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                                                                                                                                                
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
        };
      };
      /*%FSM</STATE>*/
      /*%FSM<STATE "SetTimerRandom">*/
      class SetTimerRandom
      {
        name = "SetTimerRandom";
        class Init
        {
          /*%FSM<STATEINIT>*/
          function = "setTimer";
          parameters[] = {0.5,2}; // delay
          thresholds[] = {};                                        
          /*%FSM</STATEINIT>*/
        };
        class Links
        {
          
          /*%FSM<LINK "Always">*/
          class Always
          {
            priority = 0.000000;
            to="SetRandom";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "true";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                                
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                                                                                                                                                
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
        };
      };
      /*%FSM</STATE>*/
      /*%FSM<STATE "ShortWait">*/
      class ShortWait
      {
        name = "ShortWait";
        class Init
        {
          /*%FSM<STATEINIT>*/
          function = "wait";
          parameters[] = {0.6,1.5}; // delay
          thresholds[] = {};                                                                                                                   
          /*%FSM</STATEINIT>*/
        };
        class Links
        {
          
          /*%FSM<LINK "WaitCompleted">*/
          class WaitCompleted
          {
            priority = 0.000000;
            to="SetTimerRandom";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "waitCompleted";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                                
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                                                                                                                                 
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
        };
      };
      /*%FSM</STATE>*/
      /*%FSM<STATE "SetRandom2">*/
      class SetRandom2
      {
        name = "SetRandom2";
        class Init
        {
          /*%FSM<STATEINIT>*/
          function = "nothing";
          parameters[] = {};
          thresholds[] = {{1,0,1.0}};                                                
          /*%FSM</STATEINIT>*/
        };
        class Links
        {
          
          /*%FSM<LINK "constProbability">*/
          class constProbability
          {
            priority = 1.000000;
            to="LongWait";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "const";
              parameters[] = {0.1};
              threshold = 1; // slot 1 is used                                                                        
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                            
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
          
          /*%FSM<LINK "Always">*/
          class Always
          {
            priority = 0.000000;
            to="LongerWait";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "true";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                                
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                                                                                                                                                
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
        };
      };
      /*%FSM</STATE>*/
      /*%FSM<STATE "SetRandom3">*/
      class SetRandom3
      {
        name = "SetRandom3";
        class Init
        {
          /*%FSM<STATEINIT>*/
          function = "nothing";
          parameters[] = {};
          thresholds[] = {{1,0,1.0}};                                                
          /*%FSM</STATEINIT>*/
        };
        class Links
        {
          
          /*%FSM<LINK "constProbability">*/
          class constProbability
          {
            priority = 1.000000;
            to="LongWait";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "const";
              parameters[] = {0.15};
              threshold = 1; // slot 1 is used                                    
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                            
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
          
          /*%FSM<LINK "Always">*/
          class Always
          {
            priority = 0.000000;
            to="ShortWait";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "true";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                                
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                                                                                                                                                
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
        };
      };
      /*%FSM</STATE>*/
      /*%FSM<STATE "LongWait">*/
      class LongWait
      {
        name = "LongWait";
        class Init
        {
          /*%FSM<STATEINIT>*/
          function = "wait";
          parameters[] = {4.0,8.0}; // delay
          thresholds[] = {};                                                                                                                   
          /*%FSM</STATEINIT>*/
        };
        class Links
        {
          
          /*%FSM<LINK "WaitCompleted">*/
          class WaitCompleted
          {
            priority = 0.000000;
            to="SetTimerRandom";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "waitCompleted";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                                
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                                                                                                                                 
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
        };
      };
      /*%FSM</STATE>*/
      /*%FSM<STATE "SetRandom">*/
      class SetRandom
      {
        name = "SetRandom";
        class Init
        {
          /*%FSM<STATEINIT>*/
          function = "nothing";
          parameters[] = {};
          thresholds[] = {{1,0,1.0}};                                                
          /*%FSM</STATEINIT>*/
        };
        class Links
        {
          
          /*%FSM<LINK "constProbability">*/
          class constProbability
          {
            priority = 1.000000;
            to="LongMove";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "const";
              parameters[] = {0.1};
              threshold = 1; // slot 1 is used d                                                                       
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                            
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
          
          /*%FSM<LINK "Always">*/
          class Always
          {
            priority = 0.000000;
            to="Random_Move";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "true";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                                
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                                                                                                                                                
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
        };
      };
      /*%FSM</STATE>*/
      /*%FSM<STATE "LongMove">*/
      class LongMove
      {
        name = "LongMove";
        class Init
        {
          /*%FSM<STATEINIT>*/
          function = "randomMove";
          parameters[] = {2.5, -0.1, 1.2, 5.0}; // straightDistance, minHeight, avgHeight, maxHeight 
          thresholds[] = {};                 
          /*%FSM</STATEINIT>*/
        };
        class Links
        {
          
          /*%FSM<LINK "MoveCompleted">*/
          class MoveCompleted
          {
            priority = 1.000000;
            to="SetRandom3";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "moveCompleted";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                          
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                                                                                                                              
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
          
          /*%FSM<LINK "TimeElapsed">*/
          class TimeElapsed
          {
            priority = 0.000000;
            to="SetRandom2";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "timeElapsed";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                                
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                                                                                                                                 
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
        };
      };
      /*%FSM</STATE>*/
    };
    initState="Init";
    finalStates[] =
    {
    };
  };
  /*%FSM</COMPILE>*/

  /*%FSM<COMPILE "C:\Bis\fsmeditor\entityFSM.cfg, Butterfly">*/
  /*%FSM<HEAD>*/
  /*
  item0[] = {"Init",0,250,-50.000000,-275.000000,50.000000,-225.000000,0.000000};
  item1[] = {"Always",8,218,-50.000000,-200.000000,50.000000,-150.000000,0.000000};
  item2[] = {"MoveLand",2,250,-50.000000,25.000000,50.000000,75.000000,0.000000};
  item3[] = {"MoveCompleted",4,218,75.000000,25.000000,175.000000,75.000000,1.000000};
  item4[] = {"Always",8,218,-175.000000,-125.000000,-75.000000,-75.000000,0.000000};
  item5[] = {"SetTimer3",2,250,200.000000,25.000000,300.000000,75.000000,0.000000};
  item6[] = {"TimeElapsed",4,218,150.000000,-50.000000,250.000000,0.000000,0.000000};
  item7[] = {"checkWait",2,250,200.000000,-125.000000,300.000000,-75.000000,0.000000};
  item8[] = {"Always",8,218,250.000000,-50.000000,350.000000,0.000000,0.000000};
  item9[] = {"WaitCompleted",4,218,75.000000,-125.000000,175.000000,-75.000000,1.000000};
  item10[] = {"SetTimer",2,250,-50.000000,-125.000000,50.000000,-75.000000,0.000000};
  item11[] = {"TimeElapsed",4,218,-300.000000,-50.000000,-200.000000,0.000000,0.000000};
  item12[] = {"Land",2,250,-50.000000,175.000000,50.000000,225.000000,0.000000};
  item13[] = {"Always",8,218,75.000000,175.000000,175.000000,225.000000,0.000000};
  item14[] = {"Wait",2,250,200.000000,175.000000,300.000000,225.000000,0.000000};
  item15[] = {"Always",8,218,200.000000,100.000000,300.000000,150.000000,0.000000};
  item16[] = {"Move",2,250,-300.000000,-125.000000,-200.000000,-75.000000,0.000000};
  item17[] = {"MoveCompleted",4,218,-250.000000,-200.000000,-150.000000,-150.000000,1.000000};
  item18[] = {"SetTimer2",2,250,-300.000000,25.000000,-200.000000,75.000000,0.000000};
  item19[] = {"Always",8,218,-175.000000,25.000000,-75.000000,75.000000,0.000000};
  item20[] = {"TimeElapsed",4,218,-50.000000,100.000000,50.000000,150.000000,0.000000};
  item21[] = {"Continue",2,250,-300.000000,-275.000000,-200.000000,-225.000000,0.000000};
  item22[] = {"Always",8,218,-350.000000,-200.000000,-250.000000,-150.000000,0.000000};
  link0[] = {0,1};
  link1[] = {1,10};
  link2[] = {2,3};
  link3[] = {2,20};
  link4[] = {3,5};
  link5[] = {4,16};
  link6[] = {5,6};
  link7[] = {6,7};
  link8[] = {7,8};
  link9[] = {7,9};
  link10[] = {8,5};
  link11[] = {9,10};
  link12[] = {10,4};
  link13[] = {11,18};
  link14[] = {12,13};
  link15[] = {13,14};
  link16[] = {14,15};
  link17[] = {15,5};
  link18[] = {16,11};
  link19[] = {16,17};
  link20[] = {17,21};
  link21[] = {18,19};
  link22[] = {19,2};
  link23[] = {20,12};
  link24[] = {21,22};
  link25[] = {22,16};
  globals[] = {25.000000,1,0,1,16777215,640,480,1,35,6316128,1,-410.606506,399.987366,420.340698,-342.467041,916,862,1};
  window[] = {2,-1,-1,-1,-1,893,132,1092,174,3,934};
  *//*%FSM</HEAD>*/
  class Butterfly
  {
    class States
    {
      /*%FSM<STATE "Init">*/
      class Init
      {
        name = "Init";
        class Init
        {
          /*%FSM<STATEINIT>*/
          function = "nothing";
          parameters[] = {};
          thresholds[] = {{0, 0.5, 0.5}}; // set slot 0 to 0.5                                                                                                                        
          /*%FSM</STATEINIT>*/
        };
        class Links
        {
          
          /*%FSM<LINK "Always">*/
          class Always
          {
            priority = 0.000000;
            to="SetTimer";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "true";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                    
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                             
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
        };
      };
      /*%FSM</STATE>*/
      /*%FSM<STATE "MoveLand">*/
      class MoveLand
      {
        name = "MoveLand";
        class Init
        {
          /*%FSM<STATEINIT>*/
          function = "randomMoveLand";
          parameters[] = {2};
          thresholds[] = {};                                                                
          /*%FSM</STATEINIT>*/
        };
        class Links
        {
          
          /*%FSM<LINK "MoveCompleted">*/
          class MoveCompleted
          {
            priority = 1.000000;
            to="SetTimer3";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "moveCompletedVertical";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                              
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "wait";
              parameters[] = {5,15}; // delay
              thresholds[] = {};                                                            
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
          
          /*%FSM<LINK "TimeElapsed">*/
          class TimeElapsed
          {
            priority = 0.000000;
            to="Land";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "timeElapsed";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                    
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                     
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
        };
      };
      /*%FSM</STATE>*/
      /*%FSM<STATE "SetTimer3">*/
      class SetTimer3
      {
        name = "SetTimer3";
        class Init
        {
          /*%FSM<STATEINIT>*/
          function = "setTimer";
          parameters[] = {4,6}; // delay
          thresholds[] = {};                                                                                                                  
          /*%FSM</STATEINIT>*/
        };
        class Links
        {
          
          /*%FSM<LINK "TimeElapsed">*/
          class TimeElapsed
          {
            priority = 0.000000;
            to="checkWait";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "timeElapsed";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                    
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "switchAction";
              parameters[] = {1};
              thresholds[] = {};                                                                        
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
        };
      };
      /*%FSM</STATE>*/
      /*%FSM<STATE "checkWait">*/
      class checkWait
      {
        name = "checkWait";
        class Init
        {
          /*%FSM<STATEINIT>*/
          function = "nothing";
          parameters[] = {};
          thresholds[] = {};                                                                
          /*%FSM</STATEINIT>*/
        };
        class Links
        {
          
          /*%FSM<LINK "WaitCompleted">*/
          class WaitCompleted
          {
            priority = 1.000000;
            to="SetTimer";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "waitCompleted";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                    
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "switchAction";
              parameters[] = {0};
              thresholds[] = {};                                                                                                                                                                    
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
          
          /*%FSM<LINK "Always">*/
          class Always
          {
            priority = 0.000000;
            to="SetTimer3";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "true";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                    
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                             
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
        };
      };
      /*%FSM</STATE>*/
      /*%FSM<STATE "SetTimer">*/
      class SetTimer
      {
        name = "SetTimer";
        class Init
        {
          /*%FSM<STATEINIT>*/
          function = "setTimer";
          parameters[] = {10,25}; // delay
          thresholds[] = {};
          
          /*%FSM</STATEINIT>*/
        };
        class Links
        {
          
          /*%FSM<LINK "Always">*/
          class Always
          {
            priority = 0.000000;
            to="Move";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "true";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                    
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                             
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
        };
      };
      /*%FSM</STATE>*/
      /*%FSM<STATE "Land">*/
      class Land
      {
        name = "Land";
        class Init
        {
          /*%FSM<STATEINIT>*/
          function = "Land";
          parameters[] = {};
          thresholds[] = {};                                
          /*%FSM</STATEINIT>*/
        };
        class Links
        {
          
          /*%FSM<LINK "Always">*/
          class Always
          {
            priority = 0.000000;
            to="Wait";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "true";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                    
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                             
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
        };
      };
      /*%FSM</STATE>*/
      /*%FSM<STATE "Wait">*/
      class Wait
      {
        name = "Wait";
        class Init
        {
          /*%FSM<STATEINIT>*/
          function = "wait";
          parameters[] = {3,10}; // delay
          thresholds[] = {};                                                                                                            
          /*%FSM</STATEINIT>*/
        };
        class Links
        {
          
          /*%FSM<LINK "Always">*/
          class Always
          {
            priority = 0.000000;
            to="SetTimer3";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "true";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                    
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                             
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
        };
      };
      /*%FSM</STATE>*/
      /*%FSM<STATE "Move">*/
      class Move
      {
        name = "Move";
        class Init
        {
          /*%FSM<STATEINIT>*/
          function = "randomMove";
          parameters[] = {3};
          thresholds[] = {};
          
          /*%FSM</STATEINIT>*/
        };
        class Links
        {
          
          /*%FSM<LINK "MoveCompleted">*/
          class MoveCompleted
          {
            priority = 1.000000;
            to="Continue";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "moveCompleted";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                              
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                           
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
          
          /*%FSM<LINK "TimeElapsed">*/
          class TimeElapsed
          {
            priority = 0.000000;
            to="SetTimer2";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "timeElapsed";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                    
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                  
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
        };
      };
      /*%FSM</STATE>*/
      /*%FSM<STATE "SetTimer2">*/
      class SetTimer2
      {
        name = "SetTimer2";
        class Init
        {
          /*%FSM<STATEINIT>*/
          function = "setTimer";
          parameters[] = {3,6}; // delay
          thresholds[] = {};                                                                                                                      
          /*%FSM</STATEINIT>*/
        };
        class Links
        {
          
          /*%FSM<LINK "Always">*/
          class Always
          {
            priority = 0.000000;
            to="MoveLand";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "true";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                    
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                             
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
        };
      };
      /*%FSM</STATE>*/
      /*%FSM<STATE "Continue">*/
      class Continue
      {
        name = "Continue";
        class Init
        {
          /*%FSM<STATEINIT>*/
          function = "nothing";
          parameters[] = {};
          thresholds[] = {};
          
          /*%FSM</STATEINIT>*/
        };
        class Links
        {
          
          /*%FSM<LINK "Always">*/
          class Always
          {
            priority = 0.000000;
            to="Move";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "true";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                    
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                             
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
        };
      };
      /*%FSM</STATE>*/
    };
    initState="Init";
    finalStates[] =
    {
    };
  };
  /*%FSM</COMPILE>*/

  /*%FSM<COMPILE "C:\Bis\fsmeditor\entityFSM.cfg, HoneyBee">*/
  /*%FSM<HEAD>*/
  /*
  item0[] = {"ShortMove",2,250,-200.000000,-275.000000,-100.000000,-225.000000,0.000000};
  item1[] = {"Break",2,250,-125.000000,50.000000,-25.000000,100.000000,0.000000};
  item2[] = {"MoveCompleted",4,218,-300.000000,-175.000000,-200.000000,-125.000000,1.000000};
  item3[] = {"",7,210,-328.865082,-553.276611,-320.865082,-545.276611,0.000000};
  item4[] = {"Init",0,250,125.000000,-575.000000,225.000000,-525.000000,0.000000};
  item5[] = {"Always",8,218,-125.000000,-500.000000,-25.000000,-450.000000,0.000000};
  item6[] = {"TimeElapsed",4,218,-200.000000,-175.000000,-100.000000,-125.000000,0.000000};
  item7[] = {"SetTimerRandom",2,250,-125.000000,-575.000000,-25.000000,-525.000000,0.000000};
  item8[] = {"Always",8,218,0.000000,-575.000000,100.000000,-525.000000,0.000000};
  item9[] = {"TimeElapsed",4,218,-375.000000,-25.000000,-275.000000,25.000000,0.000000};
  item10[] = {"SetTimer",2,250,-375.000000,50.000000,-275.000000,100.000000,0.000000};
  item11[] = {"SetRandom",2,250,-125.000000,-100.000000,-25.000000,-50.000000,0.000000};
  item12[] = {"constProbability",4,218,0.000000,-100.000000,100.000000,-50.000000,1.000000};
  item13[] = {"Always",8,218,-125.000000,-25.000000,-25.000000,25.000000,0.000000};
  item14[] = {"",7,210,142.030182,-79.709511,150.030182,-71.709511,0.000000};
  item15[] = {"",7,210,142.265671,-403.679260,150.265671,-395.679260,0.000000};
  item16[] = {"SetRandom_1",2,250,-125.000000,-425.000000,-25.000000,-375.000000,0.000000};
  item17[] = {"constProbability",4,218,-75.000000,-350.000000,25.000000,-300.000000,1.000000};
  item18[] = {"Always",8,218,-200.000000,-350.000000,-100.000000,-300.000000,0.000000};
  item19[] = {"LongMove",2,250,-75.000000,-275.000000,25.000000,-225.000000,0.000000};
  item20[] = {"TimeElapsed",4,218,25.000000,-175.000000,125.000000,-125.000000,0.000000};
  item21[] = {"MoveCompleted",4,218,-75.870239,-175.000000,24.129704,-125.000000,1.000000};
  item22[] = {"Always",8,218,-250.000000,50.000000,-150.000000,100.000000,0.000000};
  link0[] = {0,2};
  link1[] = {0,6};
  link2[] = {1,22};
  link3[] = {2,11};
  link4[] = {3,7};
  link5[] = {4,8};
  link6[] = {5,16};
  link7[] = {6,11};
  link8[] = {7,5};
  link9[] = {8,7};
  link10[] = {9,3};
  link11[] = {10,9};
  link12[] = {11,12};
  link13[] = {11,13};
  link14[] = {12,14};
  link15[] = {13,1};
  link16[] = {14,15};
  link17[] = {15,16};
  link18[] = {16,17};
  link19[] = {16,18};
  link20[] = {17,19};
  link21[] = {18,0};
  link22[] = {19,20};
  link23[] = {19,21};
  link24[] = {20,11};
  link25[] = {21,11};
  link26[] = {22,10};
  globals[] = {25.000000,1,0,0,16777215,640,480,1,81,6316128,1,-455.210205,326.970123,133.523590,-601.742981,917,862,1};
  window[] = {2,-1,-1,-1,-1,806,66,1026,87,3,935};
  *//*%FSM</HEAD>*/
  class HoneyBee
  {
    class States
    {
      /*%FSM<STATE "ShortMove">*/
      class ShortMove
      {
        name = "ShortMove";
        class Init
        {
          /*%FSM<STATEINIT>*/
          function = "randomMove";
          parameters[] = {0.2}; // straightDistance, minHeight, avgHeight, maxHeight 
          thresholds[] = {};                                
          /*%FSM</STATEINIT>*/
        };
        class Links
        {
          
          /*%FSM<LINK "MoveCompleted">*/
          class MoveCompleted
          {
            priority = 1.000000;
            to="SetRandom";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "moveCompleted";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                                                                                                  
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                                                                                                                                                                                                      
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
          
          /*%FSM<LINK "TimeElapsed">*/
          class TimeElapsed
          {
            priority = 0.000000;
            to="SetRandom";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "timeElapsed";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                                                                                                        
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                                                                                                                                                                                                         
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
        };
      };
      /*%FSM</STATE>*/
      /*%FSM<STATE "Break">*/
      class Break
      {
        name = "Break";
        class Init
        {
          /*%FSM<STATEINIT>*/
          function = "break";
          parameters[] = {50.0};
          thresholds[] = {};                            
          /*%FSM</STATEINIT>*/
        };
        class Links
        {
          
          /*%FSM<LINK "Always">*/
          class Always
          {
            priority = 0.000000;
            to="SetTimer";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "true";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                                                                                                        
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                                                                                                                                                                                                                        
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
        };
      };
      /*%FSM</STATE>*/
      /*%FSM<STATE "Init">*/
      class Init
      {
        name = "Init";
        class Init
        {
          /*%FSM<STATEINIT>*/
          function = "setNoBackwards";
          parameters[] = {0.0};
          thresholds[] = {{0, 0.5, 0.5}}; // set slot 0 to 0.5                                                                                        
          /*%FSM</STATEINIT>*/
        };
        class Links
        {
          
          /*%FSM<LINK "Always">*/
          class Always
          {
            priority = 0.000000;
            to="SetTimerRandom";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "true";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                                                                                                        
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                                                                                                                                                                                                                        
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
        };
      };
      /*%FSM</STATE>*/
      /*%FSM<STATE "SetTimerRandom">*/
      class SetTimerRandom
      {
        name = "SetTimerRandom";
        class Init
        {
          /*%FSM<STATEINIT>*/
          function = "setTimer";
          parameters[] = {0.5,2}; // delay
          thresholds[] = {};                                
          /*%FSM</STATEINIT>*/
        };
        class Links
        {
          
          /*%FSM<LINK "Always">*/
          class Always
          {
            priority = 0.000000;
            to="SetRandom_1";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "true";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                                                                                                        
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                                                                                                                                                                                                                        
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
        };
      };
      /*%FSM</STATE>*/
      /*%FSM<STATE "SetTimer">*/
      class SetTimer
      {
        name = "SetTimer";
        class Init
        {
          /*%FSM<STATEINIT>*/
          function = "setTimer";
          parameters[] = {0.1,0.3}; // delay
          thresholds[] = {};                                                        
          /*%FSM</STATEINIT>*/
        };
        class Links
        {
          
          /*%FSM<LINK "TimeElapsed">*/
          class TimeElapsed
          {
            priority = 0.000000;
            to="SetTimerRandom";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "timeElapsed";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                                                                                                        
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                                                                                                                                                                                                         
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
        };
      };
      /*%FSM</STATE>*/
      /*%FSM<STATE "SetRandom">*/
      class SetRandom
      {
        name = "SetRandom";
        class Init
        {
          /*%FSM<STATEINIT>*/
          function = "nothing";
          parameters[] = {};
          thresholds[] = {{1,0,1.0}};                                                                                                
          /*%FSM</STATEINIT>*/
        };
        class Links
        {
          
          /*%FSM<LINK "constProbability">*/
          class constProbability
          {
            priority = 1.000000;
            to="SetRandom_1";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "const";
              parameters[] = {0.5};
              threshold = 1; // slot 1 is used                                                                                                
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                                                                                                    
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
          
          /*%FSM<LINK "Always">*/
          class Always
          {
            priority = 0.000000;
            to="Break";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "true";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                                                                                                        
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                                                                                                                                                                                                                        
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
        };
      };
      /*%FSM</STATE>*/
      /*%FSM<STATE "SetRandom_1">*/
      class SetRandom_1
      {
        name = "SetRandom_1";
        class Init
        {
          /*%FSM<STATEINIT>*/
          function = "nothing";
          parameters[] = {};
          thresholds[] = {{1,0,1.0}};                                                                                                
          /*%FSM</STATEINIT>*/
        };
        class Links
        {
          
          /*%FSM<LINK "constProbability">*/
          class constProbability
          {
            priority = 1.000000;
            to="LongMove";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "const";
              parameters[] = {0.1};
              threshold = 1; // slot 1 is used                                                                                                
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                                                                                                    
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
          
          /*%FSM<LINK "Always">*/
          class Always
          {
            priority = 0.000000;
            to="ShortMove";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "true";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                                                                                                        
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                                                                                                                                                                                                                        
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
        };
      };
      /*%FSM</STATE>*/
      /*%FSM<STATE "LongMove">*/
      class LongMove
      {
        name = "LongMove";
        class Init
        {
          /*%FSM<STATEINIT>*/
          function = "randomMove";
          parameters[] = {1.5}; // straightDistance, minHeight, avgHeight, maxHeight 
          thresholds[] = {};                                                                 
          /*%FSM</STATEINIT>*/
        };
        class Links
        {
          
          /*%FSM<LINK "MoveCompleted">*/
          class MoveCompleted
          {
            priority = 1.000000;
            to="SetRandom";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "moveCompleted";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                                                                                                  
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                                                                                                                                                                                                      
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
          
          /*%FSM<LINK "TimeElapsed">*/
          class TimeElapsed
          {
            priority = 0.000000;
            to="SetRandom";
            class Condition
            {
              /*%FSM<CONDITION>*/
              function = "timeElapsed";
              parameters[] = {};
              threshold = 0; // slot 0 is used                                                                                                                                                                                                                                                                        
              /*%FSM</CONDITION>*/
            };
            class Action
            {
              /*%FSM<ACTION>*/
              function = "nothing";
              parameters[] = {};
              thresholds[] = {};                                                                                                                                                                                                                                                         
              /*%FSM</ACTION>*/
            };
          };
          /*%FSM</LINK>*/
        };
      };
      /*%FSM</STATE>*/
    };
    initState="Init";
    finalStates[] =
    {
    };
  };
  /*%FSM</COMPILE>*/

  /*%FSM<APPEND/>*/

};
