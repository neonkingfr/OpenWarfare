#include <fcntl.h>
#include <el/ParamFile/paramFile.hpp>
#include "DecompileParser.h"

enum DecompileLex
{
  LEX_LEFT, LEX_RIGHT, LEX_OPEN, LEX_CLOSE, LEX_VALUE_BEGIN, 
  LEX_VALUE_QUOTE_AND_SINGLE_QUOTE, LEX_VALUE_SINGLE_QUOTE, LEX_VALUE_END_OR_QUOTE, 
  LEX_COUNT
};

static RString decompileLex[LEX_COUNT] = 
{
  "/*", "*/", FSMSTR_ID[0], FSMSTR_ID[1], " \"", "\"\"\"", "\"\"", "\""
};

void DecompileNode::Clear()
{
  for (int i=0, n=nodes.Size(); i<n; ++i) nodes[i]->Clear();
}

//only for debugging purposes
#ifdef _DEBUG
void DecompileNode::PrintStructure(Report &report, RString indent)
{
  report << (indent.Data() ? indent.Data() : "");
  if (isClass)
  {
    switch (type)
    {
    case DCI_ROOT: report << "ROOT\n"; break;
    case DCI_STATIC_TEXT: report.PrintF("TEXT: \"%s\"\n",value.Data()); break;
    default: report.PrintF("%s value=\"%s\"\n", DecompileClassNames[type], value.Data()); break;
    }
  }
  else 
  {
    report.PrintF("%s value=\"%s\"\n", PrintItemsNames[type], value.Data());
    return;
  }
  report.PrintF("");
  indent = indent + "  ";
  for (int i=0, n=nodes.Size(); i<n; i++) nodes[i]->PrintStructure(report, indent);
}
#endif

bool DecompileNode::CheckSubtag(FSMTag &tag)
{
  if (isClass && type == DCI_HEAD) return false; //no subtags available
  if (!tag.isClass) return true;
  if (!tag.isClass && tag.type==PFI_ITEM_NUMBER) return true;
  if (!isClass) return false; //print item cannot have any subclasses
  switch (type)
  {
  case DCI_ROOT:
    return (tag.type==DCI_COMPILE);
    break;
  case DCI_COMPILE:
    switch (tag.type)
    {
    case DCI_HEAD:
    case DCI_STATE:
      return true;
    default:
      return false;
    }
    break;
  case DCI_STATE:
    switch (tag.type)
    {
    case DCI_LINK:
      return true;
      break;
    default:
      return false;
    }
    break;
  case DCI_LINK:
    switch (tag.type)
    {
    case DCI_FINAL_STATES:
      return true;
      break;
    default:
      return false;
    }
    break;
  }
  return false;
}

void DecompileNode::AddStaticTextNode(DecompileParser &parser, int ix, int len)
{
  DecompileNodePtr newnode = new DecompileNode;
  newnode->parent = this;
  newnode->isClass=true;
  newnode->type=DCI_STATIC_TEXT;
  newnode->value = RString(parser.buffer+ix, len);
  nodes.Add(newnode);
}

bool DecompileNode::Parse(FSMTag &tag, DecompileParser &parser, DecompileChunks *chunks)
{
  beginTag = tag;
  isClass = tag.isClass;
  type = tag.type;
  name = tag.name;
  int lastix = tag.endix;
  FSMTag newtag;
  while (parser.FindNextTag(newtag))
  {
    if (newtag.isOpenTag && newtag.isClosed) 
    { //it is an open closed tag <someTag/>
      if (newtag.type == DCI_APPEND) chunks->append=parser.lastBufIx;
    }
    else if (!newtag.isOpenTag && newtag.isClass==isClass && newtag.type == type)
    { //closing tag
      endTag = newtag;
      if (lastix<=newtag.startix)
      {
        if (beginTag.isClass)
        {
          if (beginTag.type==DCI_HEAD) AddStaticTextNode(parser, lastix, newtag.startix-lastix);
          value = beginTag.value;
        }
        else 
          value=RString(parser.buffer+lastix, newtag.startix-lastix);
      }
      return true;
    }
    else if (!newtag.isOpenTag)
    {
      if (parser.report) parser.report->PrintF("Error: unexpected close tag: \"%s\"", newtag.GetText(parser.buffer).Data());
      return false;
    }
    else
    { //another tag
      if (isClass)
      {
        //check integrity
        if (!CheckSubtag(newtag)) return false;
        DecompileNodePtr newnode = new DecompileNode;
        newnode->parent=this;
        //static text node is used only for /*%FSM<HEAD>*/
        if (!newnode->Parse(newtag, parser, chunks)) return false;
        lastix=parser.bufix;
        nodes.Add(newnode);
        value = beginTag.value;
      }
      else 
      {
        if (parser.report) parser.report->PrintF("Error: only class tags can have subtags: \"%s\"\n", newtag.GetText(parser.buffer).Data());
        return false;
      }
    }
  }
  return true;
}

DecompileParser::DecompileParser(RString fileName, RString configFileName, Report *errrep)
{
  report = errrep;
  //read the fileName contents first
  FILE *file;
  if (strcmpi(fileName,"stdin")==0)
  {
    QOStrStream ostr; 
    QStdInStream in;
    while (!in.eof())
    {
      char line[2048]; 
      in.readLine(line, 2048);
      ostr << line << "\r\n";
    }
    bufsize = ostr.pcount();
    buffer = new char[bufsize+1];
    bufix = 0;
    strncpy(buffer, ostr.str(), bufsize);
  }
  else if ((file = fopen(fileName, "rb"))!=NULL)
  {
    int len = _filelength(file->_file);
    buffer = new char[len+1];
    bufix=0;
    bufsize = fread(buffer, 1, len, file);
    fclose(file);
  }
  else 
  {
    buffer = new char[1];
    buffer[0]=0;
    bufix=bufsize=0;
    if (report) report->PrintF("Error: file %s not found!\n", fileName);
  }
  //extract needed info from configFileName
  if (configFileName!=RString("") && QFBankQueryFunctions::FileExists(configFileName))
  {
    ParamFile compileConfig;
    compileConfig.Parse(configFileName);
    DecompileInfo decInfo(compileConfig);
    dInfo = &decInfo;
    decompileLex[LEX_LEFT] = dInfo->left;
    decompileLex[LEX_RIGHT] = dInfo->right;
  }
  //initialize parser state
  bufstart = bufix;
}

bool StringsMatch(char *text, const char *textTemplate)
{
  return (strnicmp(text, textTemplate, strlen(textTemplate))==0);
}

bool DecompileParser::ParseName(FSMTag &tag, int ix, int len)
{
  RString name(buffer+ix, len);
  name.Lower();
  for (int i=0; i<PFI_ITEM_NUMBER; i++)
  {
    if (name==PrintItemsNames[i]) 
    {
      tag.isClass = false;
      tag.type = i;
      return true;
    }
  }
  for (int i=0; i<DCI_CLASS_NUMBER; i++)
  {
    if (name==DecompileClassNames[i])
    {
      tag.isClass = true;
      tag.type = i;
      return true;
    }
  }
  //any unknown name is considered to be an attribute (defined in compile config)
  tag.name = name;
  tag.isClass=false;
  tag.type=PFI_ITEM_NUMBER;
  return true;
//  if (report) report->PrintF("Error: name of tag not known: \"%s\"\n", name.Data());
//  return false;
}

bool DecompileParser::ParseValue(FSMTag &tag, int ix)
{
  ix += decompileLex[LEX_VALUE_BEGIN].GetLength();
  int i=ix;
  for (; i<tag.endix && buffer[i]!='\"'; i++)
    if (buffer[i]=='\\') i++;
  if (i!=tag.endix)
  {
    tag.value = RString(buffer+ix, i-ix);
    bufix=i+1;
    return true;
  }
  if (report) report->PrintF("Error: value is not well formed, in tag: \"%s\"\n", tag.GetText(buffer).Data());
  return false;
}

//! it fill tag structure parsing tagStr
//! it changes bufix
bool DecompileParser::ParseTag(FSMTag &tag)
{
  bufix = tag.startix+decompileLex[LEX_LEFT].GetLength();
  tag.modifier=PMI_ITEM_NUMBER;  //default is "no modifier"
  tag.isClosed = false;
  //set indent
  int i=tag.startix;
  for (; buffer[i]!='\n' && buffer[i]!='\r' && i; i--) ;
  if (i!=tag.startix) i++;
  tag.indent = tag.startix-i;

  do //only to enable break to return false and adjust DecompileParser state
  {
    if (StringsMatch(buffer+bufix, decompileLex[LEX_OPEN].Data()))
    {
      RString tagStr(buffer + bufix, tag.endix-bufix-decompileLex[LEX_LEFT].GetLength());
//      LogF("Parsing: \"%s\"", tagStr.Data());
      bufix+=decompileLex[LEX_OPEN].GetLength();
      if (buffer[bufix]=='/')
      {
        bufix++;
        tag.isOpenTag=false;
      } 
      else tag.isOpenTag=true;
      int i=bufix;
      for (; i<tag.endix && (isalnum(buffer[i]) || buffer[i]=='_'); i++) ;
      if (i!=tag.endix) 
      {
        if (!ParseName(tag, bufix, i-bufix)) break;
        if (buffer[i]=='/') { tag.isClosed=true; }
        else if (StringsMatch(buffer+i, decompileLex[LEX_CLOSE].Data())) //no action, return true
          ;
        else if (StringsMatch(buffer+i, decompileLex[LEX_VALUE_BEGIN].Data()))
        {
          if (!ParseValue(tag, i)) break;
          if (!StringsMatch(buffer+bufix, decompileLex[LEX_CLOSE].Data()))
          {
            if (report) report->PrintF("Error: there should be and of tag, after value, in \"%s\"\n", RString(buffer+tag.startix, bufix+1-tag.startix).Data());
            break;
          }
        }
        else if (StringsMatch(buffer+i, decompileLex[LEX_VALUE_QUOTE_AND_SINGLE_QUOTE].Data()))
        {
          tag.modifier=PMI_QTQUOTED;
          i+=3;
          if (!StringsMatch(buffer+i, decompileLex[LEX_CLOSE].Data()))
          { //there should be end of tag
            if (report) report->PrintF("Error: there should be an and of tag, after QTQUOTED modifier: \"%s\"\n", tagStr.Data());
            break;
          }
        }
        else if (StringsMatch(buffer+i, decompileLex[LEX_VALUE_SINGLE_QUOTE].Data()))
        {
          tag.modifier=PMI_QUOTED;
          i+=2;
          if (!StringsMatch(buffer+i, decompileLex[LEX_CLOSE].Data()))
          { //there should be end of tag
            if (report) report->PrintF("Error: there should be an and of tag, after QUOTED modifier: \"%s\"\n", tagStr.Data());
            break;
          }
        }
        else if (StringsMatch(buffer+i, decompileLex[LEX_VALUE_END_OR_QUOTE].Data()))
        {
          tag.modifier=PMI_QUOTE_IT;
          i++;
          if (!StringsMatch(buffer+i, decompileLex[LEX_CLOSE].Data()))
          { //there should be end of tag
            if (report) report->PrintF("Error: there should be an and of tag, after QUOTE_IT modifier: \"%s\"\n", tagStr.Data());
            break;
          }
        }
        else 
        {
          if (report) report->PrintF("Error: invalid characters after tag name: \"%s\"\n", tagStr.Data());
          break;
        }
        bufix = tag.endix;
        return true;
      }
    }
    else 
      return false;
  } while (false);
  bufix = tag.endix;
  return false;
}

bool DecompileParser::FindNextTag(FSMTag &tag)
{
  do 
  {
    char *tagpos = strstr(buffer+bufix, decompileLex[LEX_LEFT]);
    if (!tagpos) return false;
    lastBufIx = tag.startix = tagpos - buffer;
    tagpos = strstr(buffer+tag.startix, decompileLex[LEX_RIGHT]);
    if (!tagpos) return false;
    tag.endix = tagpos - buffer + decompileLex[LEX_RIGHT].GetLength();
    if (!ParseTag(tag)) continue; //possible error is dumped inside
    return true;
  } while (true);
}

DecompileChunks::DecompileChunks(RString fileName, RString configFileName, Report *report)
{
  append = -1; globalIndent=0;
  DecompileParser parser(fileName, configFileName, report);
  FSMTag tag;
  root.Parse(tag, parser, this);
  //set the globalIndent
  if (append!=-1)
  {
    int i=append;
    for (; parser.buffer[i]!='\n' && parser.buffer[i]!='\r' && i; i--) ;
    if (i!=append) i++;
    globalIndent = append-i;
    append = i;
  }
//root.PrintStructure(*report); //debug
}

bool DecompileChunks::GetDecompileInfos(FSMEdGraph *graph)
{
  int i=0;
  for (int n=compile->nodes.Size(); i<n; ++i)
  {
    if (compile->nodes[i]->isClass && compile->nodes[i]->type==DCI_HEAD) break;
  }
  RString value;
  if (i!=compile->nodes.Size())
  {
    DecompileNode *node = compile->nodes[i];
    if (node->nodes.Size())
    {
      if (node->nodes[0]->type!=DCI_STATIC_TEXT) return false;
      value = node->nodes[0]->value;
    }
    else return false; //HEAD contains no info
    //value contains paramFile data to be parsed

      //TODO: this is only hack, in order to try to construct *.bifsm
      int beg=0;
      for (; value[beg]!='*'; beg++); beg++;
      int end=value.GetLength()-1;
      for (; value[end]!='*'; end--); end--;
      value = value.Substring(beg,end);
    
    ParamFile pfile;
    QIStrStream dInfoIn(value.MutableData(),value.GetLength());
    pfile.Parse(dInfoIn);
    for (int i=0, n=pfile.GetEntryCount(); i<n; ++i)
    {
      ParamEntryVal &entry = pfile.GetEntry(i);
      char *name = const_cast<char *> (entry.GetName().Data());
      if (StringsMatch(name , "item"))
      {
        if (entry.IsArray())
        {
          if (entry.GetSize()< DECOMPILE_INFO_ITEM_SIZE-1) return false;
          FSMItem vert;
          FSMDecompileInfo dinfo;
          vert.text = dinfo.id = entry[0].GetValue();
          vert.id = vert.ix = i;
          dinfo.itemtype = entry[1].GetInt();
          vert.type = static_cast<ItemType>(dinfo.itemtype);
          dinfo.flags = entry[2].GetInt();
          dinfo.left = entry[3].GetFloat();
          dinfo.top = entry[4].GetFloat();
          dinfo.right = entry[5].GetFloat();
          dinfo.bottom = entry[6].GetFloat();
          vert.priority = dinfo.priority = entry[7].GetFloat();
          if (entry.GetSize()==(DECOMPILE_INFO_ITEM_SIZE-1)) vert.textRaw=vert.text;
          else vert.textRaw = dinfo.text = entry[8].GetValue();
          //layout item maintanance
          QIStrStream dInfoIn(value.MutableData(),value.GetLength());
          ParamArchiveLoad ar(dInfoIn);
          ParamArchive layoutAr;
          if (ar.OpenSubclass("LayoutItems",layoutAr))
          {
            dinfo.layout = new LayoutItem;
            if (!dinfo.layout->SerializeItem(i, dinfo.itemtype, graph->dconfigInfo, graph->decompileInfo, layoutAr))
              dinfo.layout = NULL;
          }
          //end of layout item maintanance
          graph->decompileInfo.Add(dinfo);
          graph->vertices.Add(vert);
        } 
        else return false;
      }
      else if (StringsMatch(name, "link"))
      {
        if (entry.IsArray())
        {
          if (entry.GetSize()!=2) return false;
          FSMLink ed;
          ed.ixFrom = entry[0].GetInt();
          ed.ixTo = entry[1].GetInt();
          graph->edges.Add(ed);
        }
        else return false;
      }
      else if (StringsMatch(name, "globals"))
      {
        if (entry.IsArray())
        {
          if (entry.GetSize()!= DECOMPILE_INFO_GLOBALS_SIZE) return false;
          FSMDecompileInfoGlobal& ginfo = graph->decompileInfoGlobal;
          ginfo._grid = entry[0].GetFloat();
          ginfo._gridshow = entry[1].GetInt()!=0;
          ginfo._pageClip = entry[2].GetInt()!=0;
          ginfo._pageColor = entry[3].GetInt()!=0;
          ginfo._pageColorVal = entry[4].GetInt();
          ginfo._pageImageX = entry[5].GetInt();
          ginfo._pageImageY = entry[6].GetInt();
          ginfo.grpcnt = entry[7].GetInt();
          ginfo.nextID = entry[8].GetInt();
          ginfo.defLinkColor = entry[9].GetInt();
          ginfo.defLinkUseCustom = entry[10].GetInt()!=0;
          ginfo.pzoomLeft = entry[11].GetFloat();
          ginfo.pzoomRight = entry[12].GetFloat();
          ginfo.pzoomBottom = entry[13].GetFloat();
          ginfo.pzoomTop = entry[14].GetFloat();
          ginfo.clxs = entry[15].GetInt();
          ginfo.clys = entry[16].GetInt();
          ginfo.aspect = entry[17].GetInt()!=0;
        }
        else return false;
      }
      else if (StringsMatch(name, "window"))
      {
        if (entry.IsArray())
        {
          if (entry.GetSize()!= DECOMPILE_INFO_WINDOW_SIZE) return false;
          FSMDecompileInfoWindow& winfo = graph->decompileInfoWindow;
          winfo.flags = entry[0].GetInt();
          winfo.ptMaxPositionX = entry[1].GetInt();
          winfo.ptMaxPositionY = entry[2].GetInt();
          winfo.ptMinPositionX = entry[3].GetInt();
          winfo.ptMinPositionY = entry[4].GetInt();
          winfo.rcNormalPositionBottom = entry[5].GetInt();
          winfo.rcNormalPositionLeft = entry[6].GetInt();
          winfo.rcNormalPositionRight = entry[7].GetInt();
          winfo.rcNormalPositionTop = entry[8].GetInt();
          winfo.showCmd = entry[9].GetInt();
          winfo.splitpos = entry[10].GetInt();
        }
        else return false;
      }
    }
    return true;
  }
  return false;
}

//! uses FSMGraph *_fsmg, which must be preset
void DecompileChunks::ComputeStateConditionIx()
{
  _stateIx = _condIx = -1; //not found by default
  //find state in FSMGraph using _statename
  int i=0, n=_fsmg->vertices.Size();
  for (; i<n; ++i)
  {
    if (_fsmg->vertices[i].id==_statename) break;
  }
  if (i<n) _stateIx = _fsmg->vertices[i].ix;
  else return;
  int j=0;
  for (n=_fsmg->vertices[i].edges.Size(); j<n; ++j)
  {
    if (_fsmg->vertices[i].edges[j].id==_condname) break;
  }
  if (j<n) _condIx=_fsmg->vertices[i].edges[j].ix;
}

static void ClearTrailingNewlines(RString &text)
{
  int startix = 0;
  int endix = text.GetLength();
  for (int i=0; i<endix; ++i)
  {
    char c=text[i];
    if (!isspace(c)) break;
    if (c=='\n' || c=='\r') startix=i+1;
  }
  for (int i=endix-1; i>startix; --i)
  {
    char c=text[i];
    if (!isspace(c)) break;
    if (c=='\n' || c=='\r') endix=i;
  }
  if (startix<=endix) text = text.Substring(startix,endix);
}
static void DeQuote(RString &text)
{
  int i=0, n=text.GetLength();
  int firstQuoteIx=-1, lastQuoteIx=-1;
  char *txt = text.MutableData();
  for (; i<n; ++i)
    if (txt[i]=='"') break;
  if (i!=n) firstQuoteIx=i;
  else return;
  for (i=n-1; i>firstQuoteIx; --i)
    if (txt[i]=='"') break;
  if (i!=firstQuoteIx) lastQuoteIx=i;
  else return;
  int ix=0;
  for (i=0; i<lastQuoteIx; i++)
  {
    if (i==firstQuoteIx || i==lastQuoteIx) continue;
    txt[ix++]=txt[i];
  }
  txt[ix]=0;
}
static void ConvertToSingleQuote(RString &text)
{
  char *txt = text.MutableData();
  int ix=0;
  for (int i=0, n=text.GetLength(); i<n; i++)
  {
    if (txt[i]=='\"') { 
      if (txt[i+1]=='\"') i++;
      else 
      { //search for "   \n  " and replace it by newline
        char *to = strchr(txt+(i+1), '\"');
        if (to) {
          for (char *pc = txt+(i+1); pc!=to; pc++) {
            if (*pc=='\\' && *(pc+1)=='n') txt[ix++]='\n';
          }
          i = to-txt;
          continue;
        }
      }
    }
    txt[ix++]=txt[i];
  }
  if (ix!=text.GetLength()) txt[ix]=0;
}






RString DecompileChunks::_statename;
RString DecompileChunks::_condname;
//! Add action, condition, preCondition, and initcode values to FSMEdGraph from 
//! compiled file parsed into decompileChunks
void DecompileChunks::AddChunksToGraph(DecompileNodePtr node)
{
  if (node->isClass)
  {
    switch (node->type)
    {
    case DCI_STATE: _statename=node->value; break;
    case DCI_LINK: _condname=node->value; break;
    }
  }
  else
  {
    if (node->type==PFI_ITEM_NUMBER)
    { //it's an attribute (defined in compile config)
      ClearTrailingNewlines(node->value);
      if (node->beginTag.modifier==PMI_QUOTE_IT || node->beginTag.modifier==PMI_QTQUOTED) DeQuote(node->value);
      if (node->beginTag.modifier==PMI_QTQUOTED || node->beginTag.modifier==PMI_QUOTED) ConvertToSingleQuote(node->value);
      IndentText(node->value,0);
      _graph->decompileInfoGlobal.AddAttribute(node->name, node->value);
      return;  
    }
    else if (node->type==PFI_CONDITION || node->type==PFI_ACTION || node->type==PFI_STATE_INIT || 
        node->type==PFI_STATE_PRECONDITION || node->type==PFI_COND_PRECONDITION || 
        node->type==PFI_FSMNAME)
    {
      ComputeStateConditionIx();
      ClearTrailingNewlines(node->value);
      if (node->beginTag.modifier==PMI_QUOTE_IT || node->beginTag.modifier==PMI_QTQUOTED) DeQuote(node->value);
      if (node->beginTag.modifier==PMI_QTQUOTED || node->beginTag.modifier==PMI_QUOTED) ConvertToSingleQuote(node->value);
      IndentText(node->value,0);
      switch (node->type)
      {
      case PFI_CONDITION: 
        if (_condIx!=-1) _graph->vertices[_condIx].condition=node->value; 
        break;
      case PFI_ACTION: 
        if (_condIx!=-1) _graph->vertices[_condIx].action=node->value; 
        break;
      case PFI_STATE_INIT: 
        if (_stateIx!=-1) _graph->vertices[_stateIx].initCode=node->value; 
        break;
      case PFI_STATE_PRECONDITION:
        if (_stateIx!=-1) _graph->vertices[_stateIx].preCondition=node->value; 
        break;
      case PFI_COND_PRECONDITION:
        if (_condIx!=-1) _graph->vertices[_condIx].preCondition=node->value; 
        break;
      case PFI_FSMNAME: break;
      }
      return;
    }
  }
  for (int i=0, n=node->nodes.Size(); i<n; ++i) AddChunksToGraph(node->nodes[i]);
}

static const char configName[] = "FSMConfig.cfg";
FSMEdGraph *DecompileChunks::ConvertToFsmEdGraph(RString &fsmName, Report &rep)
{
  //find HEAD and convert it to FSMEdGraph structure
  report = &rep;
  FSMEdGraph *graph = new FSMEdGraph;
  _graph = graph;
  _graph->dconfigInfo.Clear();
  RString configPath = FindFilePath(configName);
  if (configPath!=RString(""))
  {
    ParamArchiveLoad parar(configPath);
    _graph->dconfigInfo.Load(parar);
    _graph->dconfigInfo.valid=true;
  }
  else _graph->dconfigInfo.valid=false;
  //find proper compile item with matching *.bifsm file name
  int compileIx=0;
  bool compileTagFound=false;
  for (int n=root.nodes.Size(); compileIx<n; ++compileIx)
  {
    if (root.nodes[compileIx]->isClass && root.nodes[compileIx]->type==DCI_COMPILE)
    {
      compileTagFound=true;
      RString lowfsmName(fsmName); lowfsmName.Lower();
      if (lowfsmName==GetFSMName(root.nodes[compileIx]->value))
      {
        configFileName = GetConfigFileName(root.nodes[compileIx]->value);
        fsmName = GetFSMName(root.nodes[compileIx]->value, false); //do not Lower()
        break;
      }
    }
  }
  if (compileIx==root.nodes.Size()) 
  {
    if (!compileTagFound) rep << "Error: cannot parse or find matching COMPILE tag!\n";
    else rep.PrintF("Error: cannot find matching COMPILE tag for FSM: \"%s\"\n", fsmName);
    delete graph;
    return NULL;
  }
  compile=root.nodes[compileIx];

  //get decompileInfo, decompileGlobalInfo, decompileWindowInfo
  //list of vertices and edges is created too
  if (!GetDecompileInfos(graph))
  {
    rep << "Error: cannot parse or find HEAD tag!\n";
    delete graph;
    return NULL;
  }

  //fill in the data of conditions and states (action, condition, stateinit)
  AutoArray<FSMLink> backupLinks = graph->edges;
  graph->EliminateKnees();
  //_fsmg is needed for ComputeStateConditionIx()
  _fsmg = graph->ConvertToFSMGraph(rep, graph->edges, false); //note: during decompilation the stateNames cannot be shortened for backward compatibility
  AddChunksToGraph(compile);
  graph->edges=backupLinks;
  delete _fsmg;
  graph->decompileInfoGlobal.compileConfig=configFileName;
  
  //delete FAKE state if such
  graph->DeleteFakeState();
  
  return graph;
}

//
//Global functions
//
RString GetFSMName(RString value, bool makeLovercase)
{
  int ix = value.Find(',');
  if (ix!=-1)
  {
    ix++;
    while (isspace(value[ix])) ix++;
    RString output = value.Substring(ix, value.GetLength());
    if (makeLovercase) output.Lower();
    return output;
  }
  return "";
}
RString GetConfigFileName(RString value)
{
  int ix = value.Find(',');
  if (ix!=-1)
  {
    int i=0;
    while (isspace(value[i])) i++;
    RString output = value.Substring(i, ix);
    return output;
  }
  return "";
}
