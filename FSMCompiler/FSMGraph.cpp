#include <El/ParamFile/paramFile.hpp>
#include <Es/Algorithms/qsort.hpp>
#include "FSMGraph.h"

const bool AddEmptyStringValues = true;

//////////////////////////////////////////////////////////////////////////
//
// Helper static functions
//

//! RString helper function
static RString DeleteTrailingWhiteChars(RString text)
{
  int len = text.GetLength();
  while (len>0 && isspace(text[len-1])) len--;
  text = text.Substring(0,len);
  return text;
}

//! RString helper function
RString FixWhiteChars(RString text)
{
  int ix=0;
  char *txt = text.MutableData();
  for (int i=0, n=text.GetLength(); i<n; i++)
  {
    if (txt[i]=='\r' || txt[i]=='\n') continue;
    txt[ix++]=txt[i];
  }
  if (ix<text.GetLength()) txt[ix]=0;
  return text;
}

static RString TrimWithinGivenLenghts(RString text, int min, int max, char separator=' ')
{
  saturateMax(min, 1);
  saturateMin(max, text.GetLength());
  if (!max) return text;
  for (int i=min-1; i<max; i++)
  {
    if (text[i]==separator) return text.Substring(0,i);
  }
  return text.Substring(0,min);
}

int GMinChars = 16; // default value
//! RString helper function
//note: during decompilation the stateNames cannot be shortened for backward compatibility
RString ChangeToName(RString text1, bool compile=true)
{
  if (compile) text1.Trim(); //remove trailing spaces
  RString text; text.CreateBuffer(text1.GetLength()+1);
  int ix=0;
  int len = text1.GetLength();
  if (len!=0)
  {
    for (int i=0; i<len; i++)
    {
      if (text1[i]=='\r') continue;
      if (i==0)
      {
        if (!isalpha(text1[i]) && text1[i]!='_') { text.MutableData()[ix++]= '_'; text.MutableData()[ix++]= '_'; }
        else text.MutableData()[ix++] = text1[i];
      }
      else if (!isalnum(text1[i])) text.MutableData()[ix++]='_';
      else text.MutableData()[ix++] = text1[i];
    }
  }
  if (!ix) return "_";
  text.MutableData()[ix]=0;
  if (compile) return TrimWithinGivenLenghts(text, GMinChars,GMinChars+16, '_');
  else return text;
}

//////////////////////////////////////////////////////////////////////////

//FSMGraph: static members
struct FSMGraph::CurrentValues FSMGraph::current_values;
DecompileInfo *FSMGraph::dInfo;

void FSMGraph::Save(QOStream &outStream, bool clearNewLines)
{
  ParamFile file;
  ParamClassPtr fsm = file.AddClass("FSM");
  ParamClassPtr states = fsm->AddClass("States");

  int initIx=-1;
  for (int vix=0; vix<vertices.Size(); vix++)
  {
    FSMState &st = vertices[vix];
    if (st.type==DF_START_ITEM) initIx=vix;
    ParamClassPtr newState = states->AddClass(st.id);
    //newState->Add("id", st.id);
    newState->Add("name", st.text);
    if (AddEmptyStringValues || st.initCode.GetLength()!=0) newState->Add("init", st.initCode);
    if (st.edges.Size()>0)
    {
      ParamClassPtr links = newState->AddClass("Links");
      for (int eix=0; eix<st.edges.Size(); eix++)
      {
        FSMCondition &lnk = st.edges[eix];
        ParamClassPtr newLink = links->AddClass(lnk.id);
        newLink->Add("priority",lnk.priority);
        newLink->Add("to",vertices[lnk.to].id);
        if (AddEmptyStringValues || lnk.condition.GetLength()!=0) 
          newLink->Add("condition", clearNewLines ? FixWhiteChars(lnk.condition) : lnk.condition);
        if (AddEmptyStringValues || lnk.action.GetLength()!=0) 
          newLink->Add("action", clearNewLines ? FixWhiteChars(lnk.action) : lnk.action);
      }
      if (AddEmptyStringValues || st.initCode.GetLength()!=0) 
        newState->Add("init", clearNewLines ? FixWhiteChars(st.initCode) : st.initCode);
    }
  }
  //Start & End States
  if (initIx!=-1)
    fsm->Add("initState", vertices[initIx].id);
  ParamEntryPtr finSt = fsm->AddArray("finalStates");
  for (int vix=0; vix<vertices.Size(); vix++)
    if (vertices[vix].type==DF_END_ITEM) finSt->AddValue(vertices[vix].id);
  file.Save(outStream, 0);
}

void FSMEdGraph::PrintEdges()
{
  QStdDebugStream out;
  for (int i=0; i<edges.Size(); i++)
  {
    out << "Linky: ";
    BString<8192> outstr;
    outstr.PrintF("#%d# ", edges.Size()); out << outstr;
    outstr.PrintF("(%d,%d) ", edges[i].ixFrom, edges[i].ixTo);
    out << outstr;
  }
  out << RString("\n");
}

bool FSMEdGraph::EliminateKnees()
{
  //Add edges instead of knees
  int cnt = vertices.Size();
  bool done;
  do
  {
    done=true;
    for (int kneeIx=0; kneeIx<cnt; kneeIx++)
    {
      if (vertices[kneeIx].type==DF_KNEE)
      {
        //take start and end inedexes from all edges ending in this knee
        AutoArray<int> startIx;
        AutoArray<int> endIx;
        int linkCnt=edges.Size();
        for (int lnk=0; lnk<linkCnt; lnk++) 
        {
          int from = edges[lnk].ixFrom;
          int to = edges[lnk].ixTo;
          if (from == kneeIx) { endIx.Add(to); done=false; }
          if (to == kneeIx) { startIx.Add(from); done=false; }
        }
        //delete all edges neighbouring this knee
        for (int lnk=linkCnt-1; lnk>=0; lnk--) 
          if (edges[lnk].ixFrom==kneeIx || edges[lnk].ixTo==kneeIx) 
          {
            //            LogF("Deleting: (%d,%d)", edges[lnk].ixFrom, edges[lnk].ixTo);
            //            PrintEdges();
            edges.Delete(lnk);
            //            PrintEdges();
          }
          //but knee cannot be deleted yet (because of indexes)
          //add new edges
          for (int fr=0; fr<startIx.Size(); fr++)
            for (int to=0; to<endIx.Size(); to++)
            {
              //            LogF("Adding: (%d,%d)", startIx[fr], endIx[to]);
              //            PrintEdges();
              edges.Add(FSMLink(startIx[fr],endIx[to]));
              //            PrintEdges();
            }
      }
    }
  }
  while (!done);
  //done. Knee vertices still exists, but there are no edges incident with them.
  //if there are multiple links between two items, delete them
  for (int i=0; i<edges.Size(); i++)
  {
    FSMLink lnk=edges[i];
    for (int j=i+1; j<edges.Size(); j++)
    {
      if (edges[j]==lnk)
      { //delete
        if (j<edges.Size()-1)
        {
          edges[j]=edges[edges.Size()-1];
          j--;
        }
        edges.Delete(edges.Size()-1);
      }
    }
  }
  return true;
}

//! based on project FSMEditor, FSMItemInfo::IsCondition(...)
static bool IsCondition(ItemType type)
{
  return (type==DF_CONDITION||type==DF_USER_CONDITION||type==DF_TRUE_CONDITION||type==DF_USER_INPUT_CONDITION);
}

int FSMEdGraph::DegOut(int vertIx)
{
  int eCnt=edges.Size();
  int deg=0;
  for (int i=0; i<eCnt; i++)
  {
    if (edges[i].ixFrom==vertIx) deg++;
  }
  return deg;
}
int FSMEdGraph::DegIn(int vertIx)
{
  int eCnt=edges.Size();
  int deg=0;
  for (int i=0; i<eCnt; i++)
  {
    if (edges[i].ixTo==vertIx) deg++;
  }
  return deg;
}
int FSMEdGraph::Deg(int vertIx)
{
  int eCnt=edges.Size();
  int deg=0;
  for (int i=0; i<eCnt; i++)
  {
    if (edges[i].ixFrom==vertIx || edges[i].ixTo==vertIx) deg++;
  }
  return deg;
}

FSMState::FSMState(FSMItem &item)
{
  //  id = item.id;
  initCode = item.initCode;
  preCondition = item.preCondition;
  text = item.text; //DO NOT USE textRaw! Text is to be used as ID this time. textRaw will be combined later.
  type = item.type;
  ix = item.ix;
}
FSMCondition::FSMCondition(FSMItem &item)
{
  //  id = item.id;
  action = item.action;
  condition = item.condition;
  preCondition = item.preCondition;
  type = item.type;
  text = item.text;//FixWhiteChars(item.text);
  priority = item.priority;
  ix = item.ix;
  from = to = NULL;
}

static RString GetNum(int num1, int num2)
{
  char val[50]; sprintf(val, "(%d,%d)      ", num1, num2);
  if (val[8]==' ') return RString(val).Substring(0,8);
  else 
  {
    int len = strlen(val);
    int i;
    for (i=0; i<len; i++) if (val[i]==' ') break;
    return RString(val).Substring(0,i+2);
  }
//  return val;
}

static RString GetNum(int num1)
{
  char val[50]; sprintf(val, "(%d)      ", num1);
  if (val[8]==' ') return RString(val).Substring(0,8);
  else 
  {
    int len = strlen(val);
    int i;
    for (i=0; i<len; i++) if (val[i]==' ') break;
    return RString(val).Substring(0,i+2);
  }
//  return val;
}

// Parameter bool compile: during decompilation the stateNames cannot be shortened for backward compatibility
void FSMGraph::GenerateUniqueIDs(Report &report, bool isCompiling)
{
  MapStringToClass< RString, AutoArray<RString>, MapClassTraitsNoCase<RString> > hashtab;
  for (int i=0; i<vertices.Size(); i++)
  {
    //RString name = (vertices[i].type!=DF_UNDEF) ? ChangeToName(vertices[i].text) : vertices[i].text;
    RString name = ChangeToName(vertices[i].text, isCompiling);
    RString newName = name;
    bool changed = false;
    for (int k=1; hashtab.Get(newName)!=RString(""); k++)
    {
      BString<8192> num;
      num.PrintF("_%d", k);
      newName = name + num;
      changed = true;
    }
#ifdef FSM_COMPILER_VERBOSE
    if (changed) { report << GetNum(vertices[i].ix) << "Warning: State with duplicate name, changing to: " << newName << "\n"; report.Warning();}
#endif
    hashtab.Add(newName);
    vertices[i].id=newName;
    decompileInfo[vertices[i].ix].id=newName;
    decompileInfo[vertices[i].ix].text=vertices[i].text;
    MapStringToClass< RString, AutoArray<RString>, MapClassTraitsNoCase<RString> > hashtabEd;
    for (int j=0; j<vertices[i].edges.Size(); j++)
    {
      RString name = ChangeToName(vertices[i].edges[j].text, isCompiling);
      RString newName = name;
      changed = false;
      for (int k=1; hashtabEd.Get(newName)!=RString(""); k++)
      {
        BString<8192> num;
        num.PrintF("_%d", k);
        newName = name + num;
        changed = true;
      }
      if (changed) { report << GetNum(vertices[i].edges[j].ix) << "Warning: Condition with duplicate name, changing to: " << newName << "\n"; report.Warning();}
      hashtabEd.Add(newName);
      vertices[i].edges[j].id=newName;
      decompileInfo[vertices[i].edges[j].ix].id=newName;
      decompileInfo[vertices[i].edges[j].ix].text=vertices[i].edges[j].text;
    }
  }
}

inline int sign( float x )
{
  if( x>0 ) return +1;
  if( x<0 ) return -1;
  return 0;
}

static int CmpEdgesByPriority(const FSMCondition *c0, const FSMCondition *c1)
{
  return sign(c1->priority - c0->priority);
}

void FSMEdGraph::DeleteFakeState()
{
  int ix=0;
  for (int vCnt=vertices.Size(); ix<vCnt; ix++) 
    if (vertices[ix].type==DF_FAKE_STATE) break;
  if (ix<vertices.Size())
  {
    vertices.Delete(ix);
    bool found;
    do {
      found = false;
      for (int j=0; j<edges.Size(); j++)
      {
        if (edges[j].ixFrom==ix || edges[j].ixTo==ix)  
        {
          edges.Delete(j);
          found=true;
          break;
        }
      }
    } while (found);
  }
}

// Parameter bool isCompiling: during decompilation the stateNames cannot be shortened for backward compatibility
FSMGraph *FSMEdGraph::ConvertToFSMGraph(Report &report, AutoArray<FSMLink> &links, bool isCompiling)
{
  GMinChars = 16; // reset default value
  if ( !decompileInfoGlobal.compileConfig.IsEmpty() )
  {
    ParamFile compileConfig;
    compileConfig.Parse(decompileInfoGlobal.compileConfig);
    if (compileConfig.FindEntry("minChars"))
    {
      GMinChars = compileConfig >> "minChars";
    }
  }

  int vCnt=vertices.Size();
  FSMGraph *fsmg = new FSMGraph;
  fsmg->decompileInfo=decompileInfo;
  fsmg->dconfigInfo=&dconfigInfo;
  fsmg->decompileLinkInfo=links;
  fsmg->decompileInfoGlobal=decompileInfoGlobal;
  fsmg->decompileInfoWindow=decompileInfoWindow;
  int *ix2state = new int[vCnt+1]; //possibly one more
  int fakeVertexIx = -1; 
  //first pass
  for (int vix=0; vix<vertices.Size(); vix++) //DO NOT USE vCnt (size can be changed inside)
  {
    if (vertices[vix].type==DF_KNEE) continue;     //no edges are incident with knees yet
    if (IsCondition(vertices[vix].type)) 
    {
      int dgOut = DegOut(vix);
      int dgIn = DegIn(vix);
      if (dgOut>1) { report << GetNum(vix) << "Error: Condition has out degree greater than 1\n"; report.Error();}
      if (dgIn>1) { report << GetNum(vix) << "Warning: Condition has in degree greater than 1\n"; report.Warning();}
      if (!dgOut || !dgIn)
      {
        //if there are some conditions not connected to proper states, we connect it to fake one 
        //(only not to loss any data after save/load through compilation)
        if (fakeVertexIx==-1)
        {
          FSMItem fakeItem;
          fakeItem.id = -1;
          fakeItem.text = fakeItem.textRaw = "____FAKE____";
          FSMDecompileInfo dcInfo; 
          dcInfo.itemtype = DF_FAKE_STATE;
          dcInfo.priority = 0;
          fakeItem.ix = fsmg->decompileInfo.Add(dcInfo);
          fakeItem.type = DF_FAKE_STATE;
          fakeVertexIx = vertices.Add(fakeItem);
        }
        if (!dgOut)
        {
          FSMLink newLink;
          newLink.ixFrom = vertices[vix].ix;
          newLink.ixTo = fakeVertexIx;
          edges.Add(newLink);
          fsmg->decompileLinkInfo.Add(newLink);
        }
        if (!dgIn)
        {
          FSMLink newLink;
          newLink.ixTo = vertices[vix].ix;
          newLink.ixFrom = fakeVertexIx;
          edges.Add(newLink);
          fsmg->decompileLinkInfo.Add(newLink);
        }
      }
      continue;
    }
    int ix=fsmg->vertices.Add(FSMState(vertices[vix]));
    ix2state[vix] = ix;
  }
  //second pass
  bool initStateExist=false;
  for (int vix=0; vix<vertices.Size(); vix++)
  {
    if (vertices[vix].type==DF_KNEE) continue;     //no edges are incident with knees yet
    if (vertices[vix].type==DF_UNDEF) continue;    //unknown item (not state, link or knee)
    if (IsCondition(vertices[vix].type)) continue;
    if (vertices[vix].type==DF_START_ITEM) initStateExist=true;
    int eCnt=edges.Size();
    for (int eix=0; eix<eCnt; eix++)
    {
      if (edges[eix].ixFrom==vix)
      {
        int ixcond=edges[eix].ixTo;
        FSMItem &item = vertices[ixcond];
        if (IsCondition(item.type))
        {
          int ixto=-1;
          for (int edix=0; edix<eCnt; edix++)
          {
            if (edges[edix].ixFrom==ixcond) 
            { 
              ixto=edges[edix].ixTo; 
              break; 
            }
          }
          if (ixto!=-1)
          {
            if (IsCondition(vertices[ixto].type))
            {
              report << GetNum(ixcond,ixto) << "Error: There is an edge between two conditions\n"; 
              report.Error();
              continue;
            }
          }
          else 
          {
            report << GetNum(ixcond) << "Warning: There is no output edge for this condition\n";
            report.Warning();
            continue;
          }
          FSMCondition cond(item);
          cond.from = ix2state[vix];
          cond.to = ix2state[ixto];
          fsmg->vertices[ix2state[vix]].edges.Add(cond);
          //          report.PrintF("Adding condition \"%s\" from state \"%s\" to state \"%s\"\n", 
          //                        cc_cast(cond.text), cc_cast(fsmg->vertices[ix2state[vix]].text), cc_cast(fsmg->vertices[ix2state[ixto]].text));
        }
        else
        { //true condition
          report << GetNum(vix,ixcond) << "Error: there is unspecified condition between two states!\n";
          report.Error();
          continue;
          /*
          FSMCondition cond;
          cond.action="";
          cond.preCondition="";
          cond.condition="true";
          cond.text="TrueCondtition";
          cond.priority=0;
          cond.type=DF_TRUE_CONDITION;
          cond.from=ix2state[vix];
          cond.to=ix2state[ixcond];
          cond.ix = decompileInfo.Size();
          fsmg->vertices[ix2state[vix]].edges.Add(cond);
          decompileInfo.Add(FSMDecompileInfo());
          //report.PrintF("Adding TRUE condition from state \"%s\" to state \"%s\"\n", 
          //   cc_cast(fsmg->vertices[ix2state[vix]].text), cc_cast(fsmg->vertices[ix2state[ixcond]].text));
          */
        }
      }
    }
    AutoArray<FSMCondition> &edges = fsmg->vertices[ix2state[vix]].edges;
    QSort(edges.Data(), edges.Size(), CmpEdgesByPriority);    
  }
  if (!initStateExist) { report << "Warning: There is no start state!\n"; report.Warning();}
  //and generate unique id
  fsmg->GenerateUniqueIDs(report, isCompiling);

  return fsmg;
}

bool FSMEdGraph::SerializeItem(int ix, FSMItem &nfo, ParamArchive &ar)
{
  ParamArchive arItem1, arItem;
  BString<8192> buf;
  buf.PrintF("Item%d", ix);
  nfo.ix=ix;  //it is index to the CGraphCtrl::item[...]
  if (!ar.OpenSubclass(cc_cast(buf), arItem1)) return false;
  if (!arItem1.OpenSubclass("ItemInfo", arItem)) return false;

  arItem.Serialize("ItemType", (int &)nfo.type, 1, -1);
  if (ar.IsSaving())
  {
    arItem.Serialize("Text",nfo.textRaw,1);
  }
  else {
    arItem.Serialize("Text",nfo.text,1);
    nfo.textRaw = nfo.text;
  }
  //arItem.Serialize("TextRaw",nfo.textRaw,1); if (nfo.textRaw==RString("")) nfo.textRaw=nfo.text;

  FSMDecompileInfo *dinfo, dinfoinst;
  if (ar.IsLoading()) dinfo = &dinfoinst;
  else dinfo = &decompileInfo[ix];
  dinfo->itemtype = nfo.type;
  arItem1.Serialize("Left", dinfo->left, 1);
  arItem1.Serialize("Right", dinfo->right, 1);
  arItem1.Serialize("Top", dinfo->top, 1);
  arItem1.Serialize("Bottom", dinfo->bottom, 1);
  arItem1.Serialize("Flags", dinfo->flags, 1, SGRI_CUSTOMDRAW);
  if (ar.IsSaving())
  {
    if (dinfo->layout /*&& dinfo->itemtype==DF_UNDEF*/)
    {
      arItem1.Serialize("BasicText", RString(""), 1);
      dinfo->layout->Serialize(arItem1,arItem,dinfo->itemtype,dconfigInfo);
    }
    else if (dconfigInfo.valid)
    {
      if (nfo.type!=-1)
      {
        RString bastext="";
        arItem1.Serialize("BasicText", bastext, 1);
        arItem1.Serialize("BgColor", dconfigInfo.items[nfo.type].BgColor, 1, (int)TRANSPARENTCOLOR);
        arItem.Serialize("FontCharSet",dconfigInfo.items[nfo.type].FontCharSet,1);
        arItem.Serialize("FontFace",dconfigInfo.items[nfo.type].FontFace,1);
        arItem.Serialize("FontHeight",dconfigInfo.items[nfo.type].FontHeight,1);
        arItem.Serialize("FontUnderline",dconfigInfo.items[nfo.type].FontUnderline,1);
        arItem.Serialize("FontWeight",dconfigInfo.items[nfo.type].FontWeight,1);
        arItem.Serialize("lStyle",dconfigInfo.items[nfo.type].lStyle,1);
        arItem.Serialize("Shape",dconfigInfo.items[nfo.type].Shape,1);
      }
      else 
      {
        arItem1.Serialize("BasicText", RString(""), 1);
        int lst=1; arItem.Serialize("lStyle",lst,1);
      }
    }
  }
  else
  { //loading
    //TODO: all non default layout items should be saved
    if (nfo.type==-1) //only layout (i.e. undef) items are saved
    {
      dinfo->layout = new LayoutItem;
      dinfo->layout->Serialize(arItem1,arItem,dinfo->itemtype,dconfigInfo);
    }
  }

  if (nfo.type!=DF_UNDEF)
  {
    ParamArchive arSub;
    if (!arItem.OpenSubclass("ItemInfo", arSub)) return false;
    arSub.Serialize("Id", nfo.id, 1, -1);
    arSub.Serialize("InitCode", nfo.initCode, 1, "");
    arSub.Serialize("Priority", nfo.priority, 1, 0.0f);
    dinfo->priority = nfo.priority;
    arSub.Serialize("Condition", nfo.condition, 1, "");
    arSub.Serialize("Action", nfo.action, 1, "");
    arSub.Serialize("PreCondition", nfo.preCondition, 1, "");
    arItem.CloseSubclass(arSub);
  }

  if (ar.IsLoading()) decompileInfo.Add(*dinfo);

  arItem1.CloseSubclass(arItem);
  ar.CloseSubclass(arItem1);
  return true;
}

LSError LayoutItem::Serialize(ParamArchive &ar1, ParamArchive &ar, int type, const DefaultConfigInfo &dconfigInfo)
{
  if (type!=-1)
  {
    ar1.Serialize("BgColor", bgColor, 1,dconfigInfo.items[type].BgColor);
    ar.Serialize("Shape",(int &)shape,1,dconfigInfo.items[type].Shape);
    ar.Serialize("FontCharSet",lgFont.lfCharSet,1,dconfigInfo.items[type].FontCharSet);
    RString faceName;
    ar.Serialize("FontFace", faceName,1,RString(dconfigInfo.items[type].FontFace));
    if (ar.IsLoading()) strncpy(lgFont.lfFaceName,cc_cast(faceName), LF_FACESIZE);
    ar.Serialize("FontHeight",ffHeight,1,dconfigInfo.items[type].FontHeight);
    ar.Serialize("FontUnderline",lgFont.lfUnderline,1,dconfigInfo.items[type].FontUnderline);
    ar.Serialize("FontWeight",(int &)lgFont.lfWeight,1,dconfigInfo.items[type].FontWeight);
    ar.Serialize("lStyle",(int &)lStyle,1,dconfigInfo.items[type].lStyle);
  }
  else //other defaults than for FSM type items
  {
    ar1.Serialize("BgColor", bgColor, 1, (int)TRANSPARENTCOLOR);
    ar.Serialize("Shape",(int &)shape,1,(int)stBox);
    ar.Serialize("FontCharSet",lgFont.lfCharSet,1,(BYTE)EASTEUROPE_CHARSET);
    RString faceName(lgFont.lfFaceName);
    ar.Serialize("FontFace", faceName,1);
    if (ar.IsLoading()) strncpy(lgFont.lfFaceName,cc_cast(faceName), LF_FACESIZE);
    ar.Serialize("FontHeight",ffHeight,1);
    ar.Serialize("FontUnderline",lgFont.lfUnderline,1,(BYTE)0);
    ar.Serialize("FontWeight",(int &)lgFont.lfWeight,1,(LONG)400);
    ar.Serialize("lStyle",(int &)lStyle,1,PS_SOLID);
  }
  ar.Serialize("FontColor",(int &)fontColor,1,(int)0);
  ar.Serialize("Align",textAlign,1,DT_CENTER);
  ar.Serialize("lWidth",lWidth,1,1);
  ar.Serialize("FontWidth",ffWidth,1,0.0f);
  ar.Serialize("FontAngle",(int &)lgFont.lfOrientation,1,(LONG)0);
  ar.Serialize("FontItalic",lgFont.lfItalic,1,(BYTE)0);
  ar.Serialize("FontStickeOut",lgFont.lfStrikeOut,1,(BYTE)0);
  ar.Serialize("lColor",(int &)lineColor,1,(COLORREF)0);
  return LSOK;
}

bool LayoutItem::SerializeItem(int &ix, int type, const DefaultConfigInfo &dconfigInfo, AutoArray<FSMDecompileInfo> &decompileInfo, ParamArchive &ar)
{
  ParamArchive arItem1, arItem;
  BString<8192> buf;
  buf.PrintF("Item%d", ix);
  if (!ar.OpenSubclass(cc_cast(buf), arItem1)) 
  {
    //Serialize(ar,ar,type,dconfigInfo); //everything will be default. UGLY solution!
    return false;
  }
  if (!arItem1.OpenSubclass("ItemInfo", arItem)) return false;

  Serialize(arItem1,arItem,type,dconfigInfo);

  ar.CloseSubclass(arItem);
  ar.CloseSubclass(arItem1);
  return true;
}

bool FSMEdGraph::SerializeLink(int ix, FSMLink &lnk, ParamArchive &asect)
{
  ParamArchive arLink;
  BString<8192> buf;
  buf.PrintF("Link%d", ix);
  if (!asect.OpenSubclass(cc_cast(buf), arLink)) return false;
  arLink.Serialize("From",lnk.ixFrom, 1);
  arLink.Serialize("To",lnk.ixTo, 1);
  if (asect.IsSaving())
  {
    arLink.Serialize("Color", decompileInfoGlobal.defLinkColor, 1);
    int flags=2; arLink.Serialize("Flags", flags, 1);
    ParamArchive arExtra;
    arLink.OpenSubclass("Extra", arExtra);
    int arrowSize=0; arExtra.Serialize("ArrowSize", arrowSize, 1);
    arLink.CloseSubclass(arExtra);
  }
  return true;
}

#include <El/Pathname/Pathname.h>
static const int BisFSMPathCount = 1;
static char *BisFSMPath[BisFSMPathCount] = {"c:\\bis\\fsmeditor\\fsmeditor.exe"};
RString FindFilePath(RString fname)
{ 
  if (QFBankQueryFunctions::FileExists(fname)) return fname; //use file in the current directory
  else //try directory of exeFile
  {
    RString fileNameOnly = Pathname(fname).GetFilename();
    if (QFBankQueryFunctions::FileExists(fileNameOnly)) return fileNameOnly;
    Pathname exepath = Pathname::GetExePath();
    exepath.SetFilename(fileNameOnly);
    const char *path = exepath.GetFullPath();
    if (QFBankQueryFunctions::FileExists(path)) return path;
    else //use file on path c:\bis\...
    {
      for (int i=0; i<BisFSMPathCount; ++i)
      {
        Pathname comppath(BisFSMPath[i]);
        comppath.SetFilename(fileNameOnly);
        if (QFBankQueryFunctions::FileExists(comppath)) return comppath.GetFullPath();
      }
    }
  }
  return RString("");
}

static const char configName[] = "FSMConfig.cfg";
LSError FSMEdGraph::Serialize(ParamArchive &ar)
{
  int i;
  ParamArchive asect;
  if (ar.IsSaving())
  {
    dconfigInfo.Clear();
    RString configPath = FindFilePath(configName);
    if (configPath!=RString("")/*QFBankQueryFunctions::FileExists(configName)*/)
    {
      ParamArchiveLoad parar(configPath);
      dconfigInfo.Load(parar);
      dconfigInfo.valid=true;
    }
    else dconfigInfo.valid=false;
    //Items
    ar.OpenSubclass("GraphItems",asect, true);
    //count should be known
    int vCnt = vertices.Size();
    for (i=0;i<vCnt;i++) 
    {
      SerializeItem(i, vertices[i], asect);
    }
    ar.CloseSubclass(asect);
    //Links
    ar.OpenSubclass("GraphLinks", asect);
    int edCnt = edges.Size();
    for (i=0;i<edCnt;i++)
    {
      SerializeLink(i, edges[i], asect);
    }
  }
  else //loading
  {
    i=0;
    vertices.Clear();
    if (ar.OpenSubclass("GraphItems",asect))
    {
      while (true)
      {
        FSMItem gitem;
        if (!SerializeItem(i,gitem,asect)) break;
        vertices.Add(gitem);
        i++;
      }
    }
    i=0;
    if (ar.OpenSubclass("GraphLinks",asect))
    {
      while (true) 
      {
        FSMLink lnk;
        if (!SerializeLink(i,lnk,asect)) break;
        edges.Add(lnk);
        i++;
      }
    }
  }
  //Globals
  if (ar.OpenSubclass("Globals", asect))
  {
    asect.Serialize("Grid",decompileInfoGlobal._grid,1,0.0f);
    asect.Serialize("GridShow",decompileInfoGlobal._gridshow,1,false);
    asect.Serialize("PageClip",decompileInfoGlobal._pageClip,1,false);
    asect.Serialize("PageColor",decompileInfoGlobal._pageColor,1,false);
    asect.Serialize("PageColorVal",(int &)decompileInfoGlobal._pageColorVal,1,0);
    asect.Serialize("PageImageX",(int &)decompileInfoGlobal._pageImageX,1,640);
    asect.Serialize("PageImageY",(int &)decompileInfoGlobal._pageImageY,1,480);
    asect.Serialize("NextGroupID",decompileInfoGlobal.grpcnt,1,(int)0);
    asect.Serialize("NextID", decompileInfoGlobal.nextID, 1,1);
    asect.Serialize("FSMName", decompileInfoGlobal.fsmName, 1, "");
    asect.Serialize("CompileConfig", decompileInfoGlobal.compileConfig, 1, "");
    asect.Serialize("Attributes", decompileInfoGlobal.attributes, 1);
    if (ar.IsLoading())
      for (int k=0, n=decompileInfoGlobal.attributes.Size(); k<n; ++k) decompileInfoGlobal.attributes[k].name.Lower();
    //link
    asect.Serialize("DefaultLinkColor", (int &)decompileInfoGlobal.defLinkColor, 1);
    asect.Serialize("DefaultLinkUseCustom", decompileInfoGlobal.defLinkUseCustom, 1);
    //zoom
    asect.Serialize("PZoomLeft", decompileInfoGlobal.pzoomLeft, 1);
    asect.Serialize("PZoomRight", decompileInfoGlobal.pzoomRight, 1);
    asect.Serialize("PZoomBottom", decompileInfoGlobal.pzoomBottom, 1);
    asect.Serialize("PZoomTop", decompileInfoGlobal.pzoomTop, 1);
    asect.Serialize("Clxs", decompileInfoGlobal.clxs, 1);
    asect.Serialize("Clys", decompileInfoGlobal.clys, 1);
    asect.Serialize("Aspect", decompileInfoGlobal.aspect, 1);
  }
  if (ar.OpenSubclass("Window", asect))
  {
    asect.Serialize("Flags",(int &)decompileInfoWindow.flags,1);
    asect.Serialize("MaxPosX",(int &)decompileInfoWindow.ptMaxPositionX,1);
    asect.Serialize("MaxPosY",(int &)decompileInfoWindow.ptMaxPositionY,1);
    asect.Serialize("MinPosX",(int &)decompileInfoWindow.ptMinPositionX,1);
    asect.Serialize("MinPosY",(int &)decompileInfoWindow.ptMinPositionY,1);
    asect.Serialize("Left",(int &)decompileInfoWindow.rcNormalPositionLeft,1);
    asect.Serialize("Top",(int &)decompileInfoWindow.rcNormalPositionTop,1);
    asect.Serialize("Right",(int &)decompileInfoWindow.rcNormalPositionRight,1);
    asect.Serialize("Bottom",(int &)decompileInfoWindow.rcNormalPositionBottom,1);
    asect.Serialize("ShowCmd",(int &)decompileInfoWindow.showCmd,1);
    asect.Serialize("SplitPos", decompileInfoWindow.splitpos, 1);
  }
  return LSOK;
}

bool DefaultConfigInfo::SerializeItem(int ix, DefaultConfigItem &nfo, ParamArchive &ar)
{
  ParamArchive arItem1, arItem;
  BString<8192> buf;
  buf.PrintF("Item%d", ix);
  if (!ar.OpenSubclass(cc_cast(buf), arItem1)) return false;
  if (!arItem1.OpenSubclass("ItemInfo", arItem)) return false;

  arItem1.Serialize("BgColor", nfo.BgColor, 1, (int)TRANSPARENTCOLOR);
  arItem.Serialize("FontCharSet", nfo.FontCharSet, 1);
  arItem.Serialize("FontFace", nfo.FontFace, 1, "");
  arItem.Serialize("FontHeight", nfo.FontHeight, 1, 10);
  arItem.Serialize("FontUnderline", nfo.FontUnderline, 1, 0);
  arItem.Serialize("FontWeight", nfo.FontWeight, 1, 400);
  arItem.Serialize("lStyle", nfo.lStyle, 1);
  arItem.Serialize("Shape", nfo.Shape, 1, 0);

  arItem1.CloseSubclass(arItem);
  ar.CloseSubclass(arItem1);
  return true;
}

bool DefaultConfigInfo::Load(ParamArchive &ar)
{
  ParamArchive asect;
  if (ar.IsSaving())
  {
    return false; //saving is forbidden
  }
  else //loading
  {
    int i=0;
    items.Clear();
    if (ar.OpenSubclass("DefaultItems",asect))
    {
      while (true)
      {
        DefaultConfigItem gitem;
        if (!SerializeItem(i,gitem,asect)) break;
        items.Add(gitem);
        i++;
      }
      ar.CloseSubclass(asect);
    }
  }
  return true;
}

void FSMDecompileInfoGlobal::AddAttribute(RString name, RString value)
{
  name.Lower();
  int i=0, n=attributes.Size();
  for (; i<n; ++i)
    if (attributes[i].name==name) break;
  if (i<n) attributes[i].value = value;
  else attributes.Add(FSMNamedAttribute(name, value));
}
