#ifndef _REPORT_H
#define _REPORT_H

#include <El/QStream/QStdStream.hpp>
#include <Es/Strings/rstring.hpp>

//! Generating warning/error reports
/*!
Used to report warnings and errors to output, which is QOStream specified in constructor.
*/
class Report
{
private:
  int _warnings, _errors;
  QOStream &_report;
public:
  //! Construct it, connect it to QOStream and reset error/warnings counters
  Report(QOStream &report) : _report(report), _warnings(0), _errors(0) {}
  //! Print formatted text
  void PrintF(const char format[], ...);
  //! Print summary about number of errors and warnings
  void PrintSummary();
  //! Print char
  Report &operator<<(const char *val);
  //! Print int
  Report &operator<<(const int val);
  //! Print float
  Report &operator<<(const float val);
  //! Print bool
  Report &operator<<(const bool val);
  //! increment errors counter
  void Error() {_errors++;}
  //! increment warnings counter
  void Warning() {_warnings++;}
  //! get number of errors
  int GetErrorCount() { return _errors; }
  //! get reference to underlying QOStream
  QOStream &GetQOStream() {return _report;}
};

#endif
