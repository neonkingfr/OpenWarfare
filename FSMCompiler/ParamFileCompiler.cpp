#include <io.h>
#include <stdio.h>
#include <fcntl.h>

#include "paramFileCompiler.h"
#include <El/QStream/QStdStream.hpp>
#include <Es/Files/filenames.hpp>
#include <El/ParamFile/paramFile.hpp>

//////////////////////////////////////////////////////////////////////////

#ifndef PARAM_FILE_PRESERVE_NL
#pragma message("Error: PARAM_FILE_PRESERVE_NL should be defined in FSM Compiler")
error//fake
#endif

void FSMGraph::ClearCurrentValues()
{
  current_values.state=NULL;
  current_values.link=NULL;
  current_values.finalIx=0;
}

void FSMGraph::FindInitAndFinalStateNames()
{
  int n = vertices.Size();
  for (int i=0; i<n; i++)
  {
    if (vertices[i].type==DF_START_ITEM) current_values.initStateName=vertices[i].id;
    else if (vertices[i].type==DF_END_ITEM) current_values.finalStateNames.Add(vertices[i].id);
  }
}

//! return true, if parameEntry name prefix matches specified prefix
//! ie: ParamEntryNameMatch(..."print_55", "print") return true
static bool ParamEntryNameMatch(ParamEntryVal &paramEntry, RString prefix)
{
  RString entryName = paramEntry.GetName();
  if (entryName.GetLength()>=prefix.GetLength())
  {
    //compare to print tag
    entryName.MutableData()[prefix.GetLength()]=0;
    entryName.Lower();
    if (entryName==prefix) return true;
  }
  return false;
}

//! if matching, return sufix after this name, else return "" (or for empty sufix)
static RString ParamEntryNameMatch2(ParamEntryVal &paramEntry, RString prefix)
{
  RString entryName = paramEntry.GetName();
  if (entryName.GetLength()>=prefix.GetLength())
  {
    //compare to print tag
    entryName.Lower();
    if (entryName.Substring(0,prefix.GetLength())==prefix) 
    {
      RString retVal = entryName.Substring(prefix.GetLength(),entryName.GetLength());
      retVal.Lower();
      return retVal;
    }
  }
  return RString("");
}

//! change "\\n" to "\n" and "\\t" to "\t" and clear other backslashes, ie. \" => " etc.
static RString RepairBackslashes(RString &text)
{
  char *mutableData = text.MutableData();
  if (!mutableData) return text;
  int ix=0;
  for (int i=0; i<text.GetLength(); i++, ix++)
  {
    if (mutableData[i]=='\\')
    {
      switch (mutableData[++i])
      {
      case 'n': 
        mutableData[ix++]=0x0d; mutableData[ix]=0x0a; 
        break;
      case 't': 
        mutableData[ix]='\t';
        break;
      case '\\':
        mutableData[ix]='\\';
        break;
      default:
        mutableData[ix]=mutableData[i];
      }
    } else mutableData[ix]=mutableData[i];
  }
  mutableData[ix]=0;
  return text;
}

//! double all chars '"'
static RString DoubleQuotes(RString &text)
{
  const char *txt=text.Data();
  int n=text.GetLength();
  RString retText;
  retText.CreateBuffer(2*n+1); //sufficient space
  char *retTxt=retText.MutableData();
  int ix=0;
  for (int i=0; i<n; i++)
  {
    if (txt[i]=='"') retTxt[ix++]='"';
    else if (txt[i]=='\n')
    {
      const char *newLN = "\" \\n\n \"";
      for (const char *nli = newLN; *nli; nli++) retTxt[ix++]=*nli;
      continue;
    }
    else if (txt[i]=='\r') continue;
    retTxt[ix++]=txt[i];
  }
  retTxt[ix]=0;
  return retText;
}

//! make the text XML compliant
RString XMLEncode(RString &text)
{
  RString result = "";
  const char *done = text;
  const char *ptr;
  for (ptr=text; (*ptr)!=0; ptr++)
  {
    switch (*ptr)
    {
    case '&':
      result = result + RString(done, ptr - done);
      result = result + "&amp;";
      done = ptr + 1;
      break;
    case '<':
      result = result + RString(done, ptr - done);
      result = result + "&lt;";
      done = ptr + 1;
      break;
    case '>':
      result = result + RString(done, ptr - done);
      result = result + "&gt;";
      done = ptr + 1;
      break;
    case '\'':
      result = result + RString(done, ptr - done);
      result = result + "&apos;";
      done = ptr + 1;
      break;
    case '"':
      result = result + RString(done, ptr - done);
      result = result + "&quot;";
      done = ptr + 1;
      break;
    }
  }
  return result + RString(done, ptr - done);
}

static RString PreBackslashChars(RString &text, const char *chars)
{
  const char *txt=text.Data();
  int n=text.GetLength();
  RString retText;
  retText.CreateBuffer(2*n+1); //sufficient space
  char *retTxt=retText.MutableData();
  int ix=0;
  for (int i=0; i<n; i++)
  {
    for (const char *ch = chars; *ch; ch++)
    {
      if (txt[i]==*ch) { retTxt[ix++]='\\'; break; }
    }
    retTxt[ix++]=txt[i];
  }
  retTxt[ix]=0;
  return retText;
}

static RString UnBackslashChars(RString &text, const char *chars)
{
  const char *txt=text.Data();
  int n=text.GetLength();
  RString retText;
  retText.CreateBuffer(2*n+1); //sufficient space
  char *retTxt=retText.MutableData();
  int ix=0;
  for (int i=0; i<n; i++)
  {
    if (txt[i]=='\\')
    {
      const char *ch = chars;
      for (; *ch; ch++)
      {
        if (txt[i+1]==*ch) { retTxt[ix++]=*ch; break; }
      }
      if (*ch) { ++i; continue; }
    }
    retTxt[ix++]=txt[i];
  }
  retTxt[ix]=0;
  return retText;
}

CompileItem::~CompileItem()
{
  for (CompileItemElement *item = items.Start(); items.NotEnd(item); item=items.Advance(item))
  {
    delete item->item; //virtual destructor
  }
}

void CompileItem::AddPrint(RString text, RString name, bool hprint)
{
  CompileItemElement *el = new CompileItemElement;
  el->item = new CompilePrint(text, name, hprint);
  items.Add(el);
}

CompileItemElement *CompileItem::AddClass(CompileItemType type)
{
  CompileItemElement *retval = new CompileItemElement;
  if (type!=CI_PASS) retval->item = new CompileItem(type);
  else retval->item = new PassItem(type);
  items.Add(retval);
  for (CompileItemElement *el=items.Start(); el!=items.End(); el=items.Advance(el))
  {
    _asm nop;
  }
  return retval;
}

void CompileItem::AddPreSufCmd(CompilePreSufCmd::CmdType tp, RString *strptr, RString val)
{
  CompileItemElement *el = new CompileItemElement;
  el->item = new CompilePreSufCmd(strptr, val, tp);
  items.Add(el);
}

void IndentItem::UpdateIndent()
{ 
  FSMGraph::dInfo->indent=indent; 
}

void IfStartItem::Update()
{
  FSMGraph::dInfo->ifStart=onOff;
}

void IfFinalItem::Update()
{
  FSMGraph::dInfo->ifFinal=onOff;
}

void IfToFinalItem::Update()
{
  FSMGraph::dInfo->ifToFinal=onOff;
}

void IfFirstItem::Update()
{
  FSMGraph::dInfo->ifFirst=onOff;
}

void CompileItem::AddIndentItem(int indent)
{
  CompileItemElement *el = new CompileItemElement;
  el->item = new IndentItem(indent);
  items.Add(el);
}

void CompileItem::AddIfStartItem(int onOff)
{
  CompileItemElement *el = new CompileItemElement;
  el->item = new IfStartItem(onOff);
  items.Add(el);
}

void CompileItem::AddIfFinalItem(int onOff)
{
  CompileItemElement *el = new CompileItemElement;
  el->item = new IfFinalItem(onOff);
  items.Add(el);
}

void CompileItem::AddIfToFinalItem(int onOff)
{
  CompileItemElement *el = new CompileItemElement;
  el->item = new IfToFinalItem(onOff);
  items.Add(el);
}

void CompileItem::AddIfFirstItem(int onOff)
{
  CompileItemElement *el = new CompileItemElement;
  el->item = new IfFirstItem(onOff);
  items.Add(el);
}

static void RepairNewLines(RString &text)
{
  int n=text.GetLength();
  int nlCount = 0;
  for (int i=0; i<n; i++) if (text[i]=='\n') nlCount++;
  RString newtext;
  newtext.CreateBuffer(n+nlCount+1);
  char *newbuf = newtext.MutableData();
  int ix=0;
  for (int i=0; i<n; i++)
  {
    if (text[i]=='\n' && text[i-1]!='\r') 
    {
      newbuf[ix++]='\r';
    }
    newbuf[ix++]=text[i];
  }
  newbuf[ix]=0;
  text = newtext;
}
static void AddFirstSpacesToEachLine(RString &text, int count)
{
  int n=text.GetLength();
  int nlCount = 0;
  for (int i=0; i<n; i++) if (text[i]=='\n') nlCount++;
  RString newtext;
  newtext.CreateBuffer(n+count*(nlCount+1));
  char *newbuf = newtext.MutableData();
  bool insertIt = true;
  int ix=0;
  for (int i=0; i<n; i++)
  {
    if (insertIt && text[i]!='\n' && text[i]!='\r') //do not indent empty lines
    {
      for (int j=0; j<count; j++) newbuf[ix++]=' ';
      insertIt=false;
    }
    if (text[i]=='\n') insertIt=true;
    newbuf[ix++]=text[i];
  }
  newbuf[ix]=0;
  text = newtext;
}
static void DeleteFirstCharsFromEachLine(RString &text, int count)
{
  char *buf = text.MutableData();
  int n=text.GetLength();
  bool deleteIt = true;
  int ix=0;
  for (int i=0; i<n; i++)
  {
    if (deleteIt) 
    {
      for (int j=0; j<count; j++)
        if (buf[i]!='\n') i++;
        else break;
      deleteIt=false;
    }
    if (buf[i]=='\n') deleteIt=true;
    buf[ix++]=buf[i];
  }
  if (ix<n) buf[ix]=0;
}

void IndentText(RString &text, int indent)
{
  const int minIndentInit = 1000;
  if (indent<0) return;
  int minIndent=minIndentInit;
  int count = 0;
  bool counting = true;
  const char *txt = text.Data();
  for (int i=0, n=text.GetLength(); i<n; i++)
  {
    if (txt[i]=='\n') 
    { 
      count=0; 
      counting = true;
    }
    else if (counting)
    {
      if (isspace(txt[i])) count++;
      else
      {
        counting = false;
        if (count<minIndent) minIndent=count;
      }
    }
  }
  if (minIndent!=minIndentInit)
    if (indent < minIndent) DeleteFirstCharsFromEachLine(text, minIndent-indent);
    else if (indent > minIndent) AddFirstSpacesToEachLine(text, indent-minIndent);
}

static bool ContainsOnlyWhiteChars(RString text)
{
  for (int i=0, n=text.GetLength(); i<n; ++i)
    if (!isspace(text[i])) return false;
  return true;
}

RString CompilePrint::FormatCurrent()
{
  int n=formatStrings.Size();
  int fn = formatItemInfos.Size();
  RString retVal="";

  if (graph->dInfo->process && hprint) retVal = graph->dInfo->Write(DCI_HPRINT, PMI_ITEM_NUMBER, true, false, tagName);
  for (int i=0; i<n; i++)
  {
    if (i<fn)
    {
      RString param;
      char buff2[64];
      if (formatItemInfos[i].printFormat>=PFI_ITEM_NUMBER)
      { //it's a compileConfig attribute
        int ix = formatItemInfos[i].printFormat-PFI_ITEM_NUMBER;
        param = graph->decompileInfoGlobal.attributes[ix].value;
      }
      switch (formatItemInfos[i].printFormat)
      {
      case PFI_PRIORITY: 
        {
          sprintf(buff2, "%f", FSMGraph::current_values.link ? FSMGraph::current_values.link->priority : 0.0); 
          param = buff2;
        }
        break;
      case PFI_STATETEXT: param = FSMGraph::current_values.state ? FSMGraph::current_values.state->text : ""; break;
      case PFI_STATENAME: param = FSMGraph::current_values.state ? FSMGraph::current_values.state->id : ""; break;
      case PFI_LINKTEXT: param = FSMGraph::current_values.link ? FSMGraph::current_values.link->text : ""; break;
      case PFI_LINKNAME: param = FSMGraph::current_values.link ? FSMGraph::current_values.link->id : ""; break;
      case PFI_CONDITION: param = FSMGraph::current_values.link ? FSMGraph::current_values.link->condition : ""; break;
      case PFI_ACTION: param = FSMGraph::current_values.link ? FSMGraph::current_values.link->action : ""; break;
      case PFI_COND_PRECONDITION: param = FSMGraph::current_values.link ? FSMGraph::current_values.link->preCondition : ""; break;
      case PFI_STATE_INIT: param = FSMGraph::current_values.state ? FSMGraph::current_values.state->initCode : ""; break;
      case PFI_STATE_PRECONDITION: param = FSMGraph::current_values.state ? FSMGraph::current_values.state->preCondition : ""; break;
      case PFI_TO: param = FSMGraph::current_values.link ? graph->vertices[FSMGraph::current_values.link->to].id : ""; break;
      case PFI_TO_TEXT: param = FSMGraph::current_values.link ? graph->vertices[FSMGraph::current_values.link->to].text : ""; break;
      case PFI_INITSTATENAME: param = FSMGraph::current_values.initStateName; break;
      case PFI_FINALSTATENAME: param = FSMGraph::current_values.finalStateNames[FSMGraph::current_values.finalIx]; break;
      case PFI_FSMNAME: param = graph->decompileInfoGlobal.fsmName; break;
      case PFI_NUM_STATES:
        {
          sprintf(buff2, "%d", FSMGraph::current_values.numStates);
          param = buff2;
        }
        break;
      case PFI_NUM_CONDITIONS:
        {
          sprintf(buff2, "%d", FSMGraph::current_values.numConditions);
          param = buff2;
        }
        break;
      case PFI_NUM_FINAL_STATES:
        {
          sprintf(buff2, "%d", FSMGraph::current_values.finalStateNames.Size());
          param = buff2;
        }
        break;
      case PFI_GRAPH_ITEM_NO:
        {
          if (FSMGraph::current_values.link)
            sprintf(buff2, "%d", FSMGraph::current_values.link->ix);
          else if (FSMGraph::current_values.state)
            sprintf(buff2, "%d", FSMGraph::current_values.state->ix);
          else *buff2=0;
          param = buff2;
        }
        break;
      }
      bool writeThisTag=true;
      bool indentIt = false;
      switch (formatItemInfos[i].printFormat)
      {
      case PFI_ACTION: case PFI_CONDITION: case PFI_COND_PRECONDITION:
      case PFI_STATE_INIT: case PFI_STATE_PRECONDITION:
        indentIt = true;
      }
      if (indentIt || formatItemInfos[i].printFormat>=PFI_ITEM_NUMBER)
      {
//        IndentText(param, FSMGraph::dInfo->indent);
        if (ContainsOnlyWhiteChars(param)) 
        {
          /*if (param==RString("")) */writeThisTag=false;
          param="";
        }
        if (graph->clearNewLines) 
        {
//          IndentText(param, 0);
          param = FixWhiteChars(param);
        }
      }

      RString modifiedParam;
      switch (formatItemInfos[i].printModifier)
      {
      case PMI_QUOTED: modifiedParam=DoubleQuotes(param); break;
      case PMI_QUOTE_IT: modifiedParam= RString("\"") + param + "\""; break;
      case PMI_QTQUOTED: 
        modifiedParam=DoubleQuotes(param);
        modifiedParam=RString("\"") + modifiedParam + "\""; 
        break;
      case PMI_XML: modifiedParam = XMLEncode(param); break;
      default: modifiedParam = param; break;
      }
      
      RString decomp1;
      //writeThisTag not used, as it seems better to get parts of code marked for possible change
      bool writable = formatItemInfos[i].printFormat>=PFI_ITEM_NUMBER || ItemIsWritable[formatItemInfos[i].printFormat] /*&& writeThisTag*/;
      if (graph->dInfo->process && !hprint && writable)
        decomp1 = graph->dInfo->Write(formatItemInfos[i].printFormat, formatItemInfos[i].printModifier, false, false, "");
//      IndentText(formatStrings[i], graph->dInfo->indent);
      char *buff = new char[formatStrings[i].GetLength()+decomp1.GetLength()+modifiedParam.GetLength()+1024];
      sprintf(buff, formatStrings[i], (decomp1+modifiedParam).Data());
      retVal = retVal + buff;
      if (graph->dInfo->process && !hprint && writable) 
        retVal = retVal+graph->dInfo->Write(formatItemInfos[i].printFormat, formatItemInfos[i].printModifier, false, true, "", !writeThisTag);
      delete buff;
    }
    else
    {
//      IndentText(formatStrings[i], graph->dInfo->indent);
      retVal = retVal + formatStrings[i];
    }
  }
  if (graph->dInfo->process && hprint) retVal = retVal+graph->dInfo->Write(DCI_HPRINT, PMI_ITEM_NUMBER,  true, true, "");
  IndentText(retVal, graph->dInfo->indent);
  RepairNewLines(retVal);
  return retVal;
}

FSMGraph * CompilePrint::graph = NULL;
CompilePrint::CompilePrint(RString text, RString name, bool hprint1) : hprint(hprint1)
{
  //find all formal parameters (enclosed in %...%)
  type = CI_PRINT;
  tagName = name;
  int len = text.GetLength();
  const char *txt = text.Data();
  RString formatString;
  formatString.CreateBuffer(text.GetLength());
  int ix=0;
  char *txtOut = formatString.MutableData();
  for (int i=0; i<len; i++)
  {
    if (txt[i]=='%')
    {
      int paramEnd=++i;
      if (txt[i]!='%') //no double %%
      {
        for (; txt[paramEnd]!='(' && txt[paramEnd]!=0; paramEnd++);
        if (txt[paramEnd]=='(') 
        {
          //take the format modifier %modifier(variableName)
          RString modifier = text.Substring(i,paramEnd);
          modifier.Lower();
          int modIx=0;
          for (; modIx<PMI_ITEM_NUMBER; modIx++)
          {
            if (modifier==PrintModifierNames[modIx]) break;
          }
          i=++paramEnd;
          for (; txt[paramEnd]!=')' && txt[paramEnd]!=0; paramEnd++);
          if (txt[paramEnd]==')')
          {
            RString paramName = text.Substring(i,paramEnd);
            paramName.Lower();
            int k=0;
            for (; k<PFI_ITEM_NUMBER; k++)
            {
              if (paramName==PrintItemsNames[k]) break;
            }
            if (k!=PFI_ITEM_NUMBER) 
            {
              formatItemInfos.Add(PrintInfo(k,modIx));
              txtOut[ix++]='%';
              txtOut[ix++]='s';
              txtOut[ix++]=0;
              formatStrings.Add(formatString);
              formatString="";
              formatString.CreateBuffer(text.GetLength());
              txtOut = formatString.MutableData();
              ix=0;
            }
            else //it can be compileConfig defined attribute (or typing error)
            {
              AutoArray<FSMNamedAttribute> &attr = graph->decompileInfoGlobal.attributes;
              int attrCount=attr.Size();
              for (k=0; k<attrCount; ++k)
              {
                if (paramName==attr[k].name) break;
              }
              if (k!=attrCount)
              {
                formatItemInfos.Add(PrintInfo(k+PFI_ITEM_NUMBER,modIx));
                txtOut[ix++]='%';
                txtOut[ix++]='s';
                txtOut[ix++]=0;
                formatStrings.Add(formatString);
                formatString="";
                formatString.CreateBuffer(text.GetLength());
                txtOut = formatString.MutableData();
                ix=0;
              }
            }
            i=paramEnd;
          }
        }
      } else txtOut[ix++]=txt[i]; //double %%
    }
    else txtOut[ix++]=txt[i];
  }
  if (ix>0)
  {
    txtOut[ix]=0;
    formatStrings.Add(formatString);
  }
}

CompilePreSufCmd::CompilePreSufCmd(RString *strp, RString val, CompilePreSufCmd::CmdType tp)
{
  type = CI_PRESUF_CMD;
  strptr = strp;
  newval = val;
  cmdType = tp;
}

void CompilePreSufCmd::ProcessCmd()
{
  switch (cmdType)
  {
  case CompilePreSufCmd::CMD_PRESUF_CHANGE: *strptr=newval; break;
  case CompilePreSufCmd::CMD_PRESUF_LOAD: FSMGraph::dInfo->Load(); break;
  case CompilePreSufCmd::CMD_PRESUF_SAVE: FSMGraph::dInfo->Save(); break;
  }
}

static bool CheckPreSufEntries(ParamEntryVal &entry, CompileItem *item)
{
  if (entry.IsClass()) return false;
  if ((ParamEntryNameMatch(entry, "indent")))
  {
    int indent=entry.GetInt();
    //TODO: add indent node item->AddIndent(indent);
    item->AddIndentItem(indent);
    return true;
  }
  else if ((ParamEntryNameMatch(entry, "ifstart")))
  {
    int onOff = entry.GetInt();
    item->AddIfStartItem(onOff);
    return true;
  }
  else if ((ParamEntryNameMatch(entry, "iffinal")))
  {
    int onOff = entry.GetInt();
    item->AddIfFinalItem(onOff);
    return true;
  }
  else if ((ParamEntryNameMatch(entry, "iftofinal")))
  {
    int onOff = entry.GetInt();
    item->AddIfToFinalItem(onOff);
    return true;
  }
  else if ((ParamEntryNameMatch(entry, "iffirst")))
  {
    int onOff = entry.GetInt();
    item->AddIfFirstItem(onOff);
    return true; 
  }
  //ix=0 for prefix, ix=1 for sufix, sada=0 for sufix/prefix, sada=1 for sufix2/prefix2
  int sada=-1, ix=0;
  RString sufix;
  if ((sufix=ParamEntryNameMatch2(entry, "prefix2")).GetLength()) { sada=1; ix=0; }
  else if ((sufix=ParamEntryNameMatch2(entry, "prefix")).GetLength()) { sada=0; ix=0; }
  else if ((sufix=ParamEntryNameMatch2(entry, "sufix2")).GetLength()) { sada=1; ix=1; }
  else if ((sufix=ParamEntryNameMatch2(entry, "sufix")).GetLength()) { sada=0; ix=1; }
  else if (ParamEntryNameMatch(entry, "presufload")) 
  { 
    item->AddPreSufCmd(CompilePreSufCmd::CMD_PRESUF_LOAD, NULL, "");
    return true;
  }
  else if (ParamEntryNameMatch(entry, "presufsave")) 
  {
    item->AddPreSufCmd(CompilePreSufCmd::CMD_PRESUF_SAVE, NULL, "");
    return true;
  }
  if (sada!=-1)
  {
    TPreSuf *presufptr = NULL;
    for (int i=0; i<DCI_CLASS_NUMBER; i++) 
    {
      if (sufix==DecompileClassNames[i])
      {
        presufptr = FSMGraph::dInfo->classValues+i;
        break;
      }
    }
    if (!presufptr)
    {
      for (int i=0; i<PFI_ITEM_NUMBER; i++) 
        if (sufix==PrintItemsNames[i])
        {
          presufptr = FSMGraph::dInfo->itemValues+i;
          break;
        }
    }
    if (!presufptr)
    { //it's compile config attribute
      
    }
    if (presufptr)
    {
      RString *strptr;
      if (ix)
        if (sada) strptr = &presufptr->sufix2;
        else strptr = &presufptr->sufix;
      else
        if (sada) strptr = &presufptr->prefix2;
        else strptr = &presufptr->prefix;
      RString value = entry.GetValue();
      item->AddPreSufCmd(CompilePreSufCmd::CMD_PRESUF_CHANGE, strptr, value);
      return true;
    }
  }
  return false;
}

static void AddFinalStatesClass(ParamEntryVal &parentEntry, CompileItem *parentItem)
{
  CompileItem *finalItem = parentItem->AddClass(CI_FINAL_STATES)->GetCompileItem();
  int n = parentEntry.GetEntryCount();
  for (int i=0; i<n; i++)
  {
    ParamEntryVal finalEntry = parentEntry.GetEntry(i);
    if (ParamEntryNameMatch(finalEntry, RString("print")))
    {
      finalItem->AddPrint(RepairBackslashes(finalEntry.GetValue()), finalEntry.GetName());
    }
    else if (ParamEntryNameMatch(finalEntry, RString("hprint")))
    {
      finalItem->AddPrint(RepairBackslashes(finalEntry.GetValue()), finalEntry.GetName(), true);
    }
    else if (ParamEntryNameMatch(finalEntry, RString("nodecompile"))) finalItem->nodecompile = finalEntry.GetInt()!=0;
    else CheckPreSufEntries(finalEntry, finalItem);
  }
}

static void AddLinkClass(ParamEntryVal &parentEntry, CompileItem *parentItem)
{
  {
    //Link
    CompileItem *linkItem = parentItem->AddClass(CI_LINK)->GetCompileItem();
    int linkn = parentEntry.GetEntryCount();
    for (int linki=0; linki<linkn; linki++)
    {
      ParamEntryVal linkEntry = parentEntry.GetEntry(linki);
      RString entryName = linkEntry.GetName();
      if (ParamEntryNameMatch(linkEntry, RString("print"))) linkItem->AddPrint(RepairBackslashes(linkEntry.GetValue()), entryName);
      else if (ParamEntryNameMatch(linkEntry, RString("hprint"))) linkItem->AddPrint(RepairBackslashes(linkEntry.GetValue()), entryName, true);
      else if (ParamEntryNameMatch(linkEntry, "finalstates")) AddFinalStatesClass(linkEntry, linkItem);
      else if (ParamEntryNameMatch(linkEntry, RString("nodecompile"))) linkItem->nodecompile = linkEntry.GetInt()!=0;
      else CheckPreSufEntries(linkEntry, linkItem);
    }
  }
}

static void AddStateClass(ParamEntryVal &parentEntry, CompileItem *parentItem)
{
  //State
  CompileItem *stateItem = parentItem->AddClass(CI_STATE)->GetCompileItem();
  int staten = parentEntry.GetEntryCount();
  for (int statei=0; statei<staten; statei++)
  {
    ParamEntryVal stateEntry = parentEntry.GetEntry(statei);
    if (stateEntry.IsClass() && ParamEntryNameMatch(stateEntry, "link")) AddLinkClass(stateEntry, stateItem);
    else if (ParamEntryNameMatch(stateEntry, "finalstates")) AddFinalStatesClass(stateEntry, stateItem);
    else if (ParamEntryNameMatch(stateEntry, RString("print"))) stateItem->AddPrint(RepairBackslashes(stateEntry.GetValue()), stateEntry.GetName());
    else if (ParamEntryNameMatch(stateEntry, RString("hprint"))) stateItem->AddPrint(RepairBackslashes(stateEntry.GetValue()),stateEntry.GetName(), true);
    else if (ParamEntryNameMatch(stateEntry, RString("nodecompile"))) stateItem->nodecompile = stateEntry.GetInt()!=0;
    else CheckPreSufEntries(stateEntry, stateItem);
  }
}

static void AddPassClass(ParamEntryVal &parentEntry, CompileItem *parentItem)
{
  PassItem *passItem = (PassItem *)parentItem->AddClass(CI_PASS)->GetCompileItem();
  QOStrStream strstream;
  parentEntry.Save(strstream, 0, "\r\n");
  passItem->passText = strstream.str(); passItem->passText.MutableData()[strstream.pcount()]=0;
  int passn = parentEntry.GetEntryCount();
  for (int passi=0; passi<passn; passi++)
  {
    ParamEntryVal passEntry = parentEntry.GetEntry(passi);
    RString name = passEntry.GetName();
    if (passEntry.IsClass() && ParamEntryNameMatch(passEntry, "state")) AddStateClass (passEntry, passItem);
    else if (ParamEntryNameMatch(passEntry, "finalstates")) AddFinalStatesClass(passEntry, passItem);
    else if (ParamEntryNameMatch(passEntry, RString("print"))) passItem->AddPrint(RepairBackslashes(passEntry.GetValue()), passEntry.GetName());
    else if (ParamEntryNameMatch(passEntry, RString("hprint"))) passItem->AddPrint(RepairBackslashes(passEntry.GetValue()),passEntry.GetName(), true);
    else if (ParamEntryNameMatch(passEntry, RString("nodecompile"))) passItem->nodecompile = passEntry.GetInt()!=0;
    else CheckPreSufEntries(passEntry, passItem);
  }
}

static void BeginNoDecompile(QOStream &out, FSMGraph *graph)
{
//x  out << graph->dInfo->Write(DCI_NODECOMPILE, PMI_ITEM_NUMBER, true, false, "");
  graph->dInfo->process=false;
}
static void EndNoDecompile(QOStream &out, FSMGraph *graph)
{
//x  out << graph->dInfo->Write(DCI_NODECOMPILE, PMI_ITEM_NUMBER, true, true, "");
}

static bool UnwantedIndex(FSMGraph *graph, int vertexIx)
{
  if (graph->dInfo->ifFirst==0) return false;
  return ( ((graph->dInfo->ifFirst>0) && (vertexIx!=0)) ||
    ((graph->dInfo->ifFirst<0) && (vertexIx==0)) );
}

static void CompileFinalStates(CompileItemElement *parentEl, QOStream &out, FSMGraph *graph)
{
  CompileItem *finalIt = parentEl->GetCompileItem();
  int num = FSMGraph::current_values.finalStateNames.Size();
//x  if (graph->dInfo->process) out << graph->dInfo->Write(DCI_FINAL_STATES, PMI_ITEM_NUMBER, true, false, "");
  bool oldProcess = graph->dInfo->process;
  if (finalIt->nodecompile) BeginNoDecompile(out, graph);
  for (int i=0; i<num; i++)
  {
    FSMGraph::current_values.finalIx = i;
    for (CompileItemElement *finalEl=finalIt->items.Start(); finalIt->items.NotEnd(finalEl); finalEl=finalIt->items.Advance(finalEl))
    {
      if (finalEl->item->type==CI_IFFIRST) ((IfFirstItem*)finalEl->item)->Update();
      else if (UnwantedIndex(graph,i)) continue; //iffirst
      else if (finalEl->item->type==CI_PRINT)
      {
        CompilePrint *printIt = finalEl->GetCompilePrint();
        out << printIt->FormatCurrent();
      }
      else if (finalEl->item->type==CI_PRESUF_CMD) ((CompilePreSufCmd*)finalEl->item)->ProcessCmd();
      else if (finalEl->item->type==CI_INDENT) ((IndentItem*)finalEl->item)->UpdateIndent();
      else if (finalEl->item->type==CI_IFSTART) ((IfStartItem*)finalEl->item)->Update();
      else if (finalEl->item->type==CI_IFFINAL) ((IfFinalItem*)finalEl->item)->Update();
      else if (finalEl->item->type==CI_IFTOFINAL) ((IfToFinalItem*)finalEl->item)->Update();
      else Fail("class FinalStates can contain only print elements");
    }
  }
  if (finalIt->nodecompile) EndNoDecompile(out, graph);
  graph->dInfo->process=oldProcess;
//x  if (graph->dInfo->process) out << graph->dInfo->Write(DCI_FINAL_STATES, PMI_ITEM_NUMBER, true, true, "");
  FSMGraph::current_values.finalIx = 0;
}


static bool BeyondStartEndSection(FSMGraph *graph)
{
  if (graph->current_values.state)
    return (((graph->dInfo->ifStart>0)&&(graph->current_values.state->type!=DF_START_ITEM)) ||
            ((graph->dInfo->ifFinal>0)&&(graph->current_values.state->type!=DF_END_ITEM)) ||
            ((graph->dInfo->ifStart<0)&&(graph->current_values.state->type==DF_START_ITEM)) ||
            ((graph->dInfo->ifFinal<0)&&(graph->current_values.state->type==DF_END_ITEM)));
  return false;
}

static bool BeyondToFinalSection(FSMGraph *graph)
{
  if (graph->current_values.link)
    return (((graph->dInfo->ifToFinal>0)&&(graph->vertices[graph->current_values.link->to].type!=DF_END_ITEM)) ||
            ((graph->dInfo->ifToFinal<0)&&(graph->vertices[graph->current_values.link->to].type==DF_END_ITEM))  );
  return false;
}

static void CompileLinks(CompileItemElement *parentEl, FSMState *state, QOStream &out, FSMGraph *graph)
{
  CompileItem *stateIt = parentEl->GetCompileItem();
  int linkNum=state->edges.Size();
//x  if (graph->dInfo->process) out << graph->dInfo->Write(DCI_LINKS, PMI_ITEM_NUMBER, true, false, "");
  bool oldProcess = graph->dInfo->process;
  if (stateIt->nodecompile) BeginNoDecompile(out, graph);
  for (int linkIx=0; linkIx<linkNum; linkIx++)
  {
    FSMGraph::current_values.link = &state->edges[linkIx];
    if (graph->dInfo->process) out << graph->dInfo->Write(DCI_LINK, PMI_ITEM_NUMBER, true, false, FSMGraph::current_values.link->id, false, true);
    for (CompileItemElement *linkEl=stateIt->items.Start(); stateIt->items.NotEnd(linkEl); linkEl=stateIt->items.Advance(linkEl))
    {
      if (linkEl->item->type==CI_IFSTART) ((IfStartItem*)linkEl->item)->Update();
      else if (linkEl->item->type==CI_IFFINAL) ((IfFinalItem*)linkEl->item)->Update();
      else if (linkEl->item->type==CI_IFTOFINAL) ((IfToFinalItem*)linkEl->item)->Update();
      else if (BeyondStartEndSection(graph)) continue;
      else if (BeyondToFinalSection(graph)) continue;
      else if (linkEl->item->type==CI_IFFIRST) ((IfFirstItem*)linkEl->item)->Update();
      else if (UnwantedIndex(graph,linkIx)) continue; //iffirst
      else if (linkEl->item->type==CI_PRINT)
      {
        CompilePrint *printIt = linkEl->GetCompilePrint();
        out << printIt->FormatCurrent();
      }
      else if (linkEl->item->type==CI_FINAL_STATES) CompileFinalStates(linkEl, out, graph);
      else if (linkEl->item->type==CI_PRESUF_CMD) ((CompilePreSufCmd*)linkEl->item)->ProcessCmd();
      else if (linkEl->item->type==CI_INDENT) ((IndentItem*)linkEl->item)->UpdateIndent();
      else Fail("class Link can contain only print elements");
    }
    if (graph->dInfo->process) out << graph->dInfo->Write(DCI_LINK, PMI_ITEM_NUMBER, true, true, "", false, true);
  }
  if (stateIt->nodecompile) EndNoDecompile(out, graph);
  graph->dInfo->process=oldProcess;
//x  if (graph->dInfo->process) out << graph->dInfo->Write(DCI_LINKS, PMI_ITEM_NUMBER, true, true, "");
  FSMGraph::current_values.link = NULL;
}

static void CompileStates(CompileItemElement *parentEl, QOStream &out, FSMGraph *graph)
{
  CompileItem *passIt = parentEl->GetCompileItem();
  int vertexNum=graph->vertices.Size();
//x  if (graph->dInfo->process) out << graph->dInfo->Write(DCI_STATES, PMI_ITEM_NUMBER, true, false, "");
  bool oldProcess = graph->dInfo->process;
  if (passIt->nodecompile) BeginNoDecompile(out, graph);
  for (int vertexIx=0; vertexIx<vertexNum; vertexIx++)
  {
    FSMState *state = &graph->vertices[vertexIx];
    if (state->type==DF_UNDEF) continue;
    FSMGraph::current_values.state = state;
    FSMGraph::current_values.numConditions=state->edges.Size();
    if (BeyondStartEndSection(graph)) continue;
    if (graph->dInfo->process) out << graph->dInfo->Write(DCI_STATE, PMI_ITEM_NUMBER, true, false, state->id, false, true);
    for (CompileItemElement *stateEl=passIt->items.Start(); passIt->items.NotEnd(stateEl); stateEl=passIt->items.Advance(stateEl))
    {
      if (stateEl->item->type==CI_IFSTART) ((IfStartItem*)stateEl->item)->Update();
      else if (stateEl->item->type==CI_IFFINAL) ((IfFinalItem*)stateEl->item)->Update();
      else if (BeyondStartEndSection(graph)) continue;
      else if (stateEl->item->type==CI_IFFIRST) ((IfFirstItem*)stateEl->item)->Update();
      else if (UnwantedIndex(graph,vertexIx)) continue; //iffirst
      else if (stateEl->item->type==CI_LINK) CompileLinks(stateEl, state, out, graph);
      else if (stateEl->item->type==CI_FINAL_STATES) CompileFinalStates(stateEl, out, graph);
      else if (stateEl->item->type==CI_PRINT)
      {
        CompilePrint *printIt = stateEl->GetCompilePrint();
        out << printIt->FormatCurrent();
      }
      else if (stateEl->item->type==CI_PRESUF_CMD) ((CompilePreSufCmd*)stateEl->item)->ProcessCmd();
      else if (stateEl->item->type==CI_INDENT) ((IndentItem*)stateEl->item)->UpdateIndent();
      else Fail ("class State can have only Link subclass or print elements");
    }
    if (graph->dInfo->process) out << graph->dInfo->Write(DCI_STATE, PMI_ITEM_NUMBER, true, true, "", false, true);
    FSMGraph::current_values.state = NULL;
  }
  if (passIt->nodecompile) EndNoDecompile(out, graph);
  graph->dInfo->process=oldProcess;
//x  if (graph->dInfo->process) out << graph->dInfo->Write(DCI_STATES, PMI_ITEM_NUMBER, true, true, "");
}

static void CompilePasses(CompileItemElement *parentEl, QOStream &out, FSMGraph *graph)
{
  PassItem *rootIt = (PassItem *)parentEl->GetCompileItem();
//x  if (graph->dInfo->process) out << graph->dInfo->Write(DCI_PASS, PMI_ITEM_NUMBER, true, false, PreBackslashChars(rootIt->passText, "\""));
  bool oldProcess = graph->dInfo->process;
  if (rootIt->nodecompile) BeginNoDecompile(out, graph);
  for (CompileItemElement *passEl=rootIt->items.Start(); rootIt->items.NotEnd(passEl); passEl=rootIt->items.Advance(passEl))
  {
    if (passEl->item->type==CI_IFSTART) ((IfStartItem*)passEl->item)->Update();
    else if (passEl->item->type==CI_IFFINAL) ((IfFinalItem*)passEl->item)->Update();
    //else if (BeyondStartEndSection(graph)) continue; // FIX: this condition takes NO SENSE beyond the 'class State' scope => should NOT be called here!
    else if (passEl->item->type==CI_STATE) CompileStates(passEl, out, graph);
    else if (passEl->item->type==CI_FINAL_STATES) CompileFinalStates(passEl, out, graph);
    else if (passEl->item->type==CI_PRINT)
    {
      CompilePrint *printIt = passEl->GetCompilePrint();
      out << printIt->FormatCurrent();
    }
    else if (passEl->item->type==CI_PRESUF_CMD) ((CompilePreSufCmd*)passEl->item)->ProcessCmd();
    else if (passEl->item->type==CI_INDENT) ((IndentItem*)passEl->item)->UpdateIndent();
    else Fail("class Pass can have only State subclass or print elements");
  }
  if (rootIt->nodecompile) EndNoDecompile(out, graph);
  graph->dInfo->process=oldProcess;
//x  if (graph->dInfo->process) out << graph->dInfo->Write(DCI_PASS, PMI_ITEM_NUMBER, true, true, "");
}

//! global variable used for saving / loading state of decompile info
DecompileInfo savedDecompileInfo;
void DecompileInfo::Save()
{
  savedDecompileInfo.left = left;
  savedDecompileInfo.right = right;
  savedDecompileInfo.process = process;
  for (int i=0; i<PFI_ITEM_NUMBER; i++) savedDecompileInfo.itemValues[i] = itemValues[i];
  for (int i=0; i<DCI_CLASS_NUMBER; i++) savedDecompileInfo.classValues[i] = classValues[i];
}

void DecompileInfo::Load()
{
  left = savedDecompileInfo.left;
  right = savedDecompileInfo.right;
  process = savedDecompileInfo.process;
  for (int i=0; i<PFI_ITEM_NUMBER; i++) itemValues[i] = savedDecompileInfo.itemValues[i];
  for (int i=0; i<DCI_CLASS_NUMBER; i++) classValues[i] = savedDecompileInfo.classValues[i];
}

static bool TagShouldBeWrited(int ix, bool isClass)
{
  if (isClass)
  {
    switch (ix)
    {
    case DCI_HPRINT:
    case DCI_NODECOMPILE:
      return false;
    }
  }
  else
  {
    if (ix>=PFI_ITEM_NUMBER) return true;
    switch (ix)
    {
    case PFI_LINKTEXT:
    case PFI_LINKNAME:
    case PFI_NUM_CONDITIONS:
    case PFI_NUM_STATES:
    case PFI_NUM_FINAL_STATES:
    case PFI_PRIORITY:
    case PFI_STATETEXT:
    case PFI_STATENAME:
    case PFI_FINALSTATENAME:
    case PFI_INITSTATENAME:
    case PFI_TO:
    case PFI_TO_TEXT:
    case PFI_GRAPH_ITEM_NO:
      return false;
    }
  }
  return true;
}

RString DecompileInfo::Write(int ix, int modix, bool isClass, bool isEnd, RString val, bool omitNewLinePrefix, bool useIndent)
{
  if (TagShouldBeWrited(ix,isClass))
  {
    RString text;
    AutoArray<FSMNamedAttribute> &attr = CompilePrint::graph->decompileInfoGlobal.attributes;
    if (ix<PFI_ITEM_NUMBER)
      text = isClass ? DecompileClassNames[ix] : PrintItemsNames[ix];
    else
      text = attr[ix-PFI_ITEM_NUMBER].name;
    switch (modix)
    {
    case PMI_QTQUOTED: 
      text = text + "\"\"\""; break;
    case PMI_QUOTED: 
      text = text + "\"\""; break;
    case PMI_QUOTE_IT: 
      text = text + "\""; break;
    }
    RString prefix, sufix;
    if (isClass)
    {
      if (isEnd) 
      {
        prefix=classValues[ix].prefix2;
        sufix=classValues[ix].sufix2;
      }
      else 
      {
        prefix=classValues[ix].prefix; 
        sufix=classValues[ix].sufix; 
      }
    }
    else
    {
      if (ix<PFI_ITEM_NUMBER)
      {
        if (isEnd) 
        {
          prefix=itemValues[ix].prefix2;
          sufix=itemValues[ix].sufix2;
        }
        else 
        {
          prefix=itemValues[ix].prefix; 
          sufix=itemValues[ix].sufix;
        }
      }
      else //it's compile config attribute
      {
        if (isEnd) 
        {
          prefix=attr[ix-PFI_ITEM_NUMBER].presuf.prefix2;
          sufix=attr[ix-PFI_ITEM_NUMBER].presuf.sufix2;
        }
        else 
        {
          prefix=attr[ix-PFI_ITEM_NUMBER].presuf.prefix; 
          sufix=attr[ix-PFI_ITEM_NUMBER].presuf.sufix;
        }
      }
    }
    RepairBackslashes(prefix);
    RepairBackslashes(sufix);
    if (isEnd && prefix=="\r\n" && omitNewLinePrefix) prefix="";
    text.Upper();
    if (isEnd) text = "/" + text;
    if (val!=RString("")) val = RString(" \"") + val + "\"";
    RString retval = prefix + left + FSMSTR_ID[0] + text + val + FSMSTR_ID[1] + right + sufix;
    bool indentIt = true;
    if (!isClass) switch (ix) 
    {
      case PFI_ACTION: case PFI_CONDITION: case PFI_COND_PRECONDITION:
      case PFI_STATE_INIT: case PFI_STATE_PRECONDITION:
        indentIt = !clearNewLines;
        break;
    }
    if (indentIt && useIndent) IndentText(retval, indent);

    return retval;
  }
  else
  {
    return RString("");
  }
}

void DecompileInfo::ReadPreSufixes(ParamEntryVal &parent, int ix, int sada)
{
  int n = parent.GetEntryCount();
  RString defaultPreSuf = parent >> "default";
  for (int j=0; j<DCI_CLASS_NUMBER; j++)
    if (ix) 
      if (sada) classValues[j].sufix2=defaultPreSuf;
      else classValues[j].sufix=defaultPreSuf;
    else 
      if (sada) classValues[j].prefix2=defaultPreSuf;
      else classValues[j].prefix=defaultPreSuf;
  for (int j=0; j<PFI_ITEM_NUMBER; j++)
    if (ix)
      if (sada) itemValues[j].sufix2=defaultPreSuf;
      else itemValues[j].sufix=defaultPreSuf;
    else 
      if (sada) itemValues[j].prefix2=defaultPreSuf;
      else itemValues[j].prefix=defaultPreSuf;
  if (n>0)
  {
    for (int i=0; i<n; i++)
    {
      ParamEntryVal item = parent.GetEntry(i);
      RString entryName = item.GetName(); entryName.Lower();
      bool done=false;
      for (int j=0; j<DCI_CLASS_NUMBER; j++)
      {
        if (entryName==DecompileClassNames[j])
        {
          if (ix) 
            if (sada) classValues[j].sufix2 = item.GetValue();
            else classValues[j].sufix = item.GetValue();
          else 
            if (sada) classValues[j].prefix2 = item.GetValue();
            else classValues[j].prefix = item.GetValue();
          done = true;
          break;
        }
      }
      if (!done)
      {
        for (int j=0; j<PFI_ITEM_NUMBER; j++)
        {
          if (entryName==PrintItemsNames[j])
          {
            if (ix) 
              if (sada) itemValues[j].sufix2 = item.GetValue();
              else itemValues[j].sufix = item.GetValue();
            else 
              if (sada) itemValues[j].prefix2 = item.GetValue();
              else itemValues[j].prefix = item.GetValue();
            done = true;
            break;
          }
        }
      }
      if (!done)
      { //it's compile config attribute
        AutoArray<FSMNamedAttribute> &attr = CompilePrint::graph->decompileInfoGlobal.attributes;
        for (int i=0, n=attr.Size(); i<n; ++i)
        {
          if (entryName==attr[i].name)
          {
            if (ix) 
              if (sada) attr[i].presuf.sufix2 = item.GetValue();
              else attr[i].presuf.sufix = item.GetValue();
            else 
              if (sada) attr[i].presuf.prefix2 = item.GetValue();
              else attr[i].presuf.prefix = item.GetValue();
          }
        }
      }
    }
  }
}

DecompileInfo::DecompileInfo(const ParamEntry &compileConfig)
{
  indent=0;ifStart=ifFinal=ifToFinal=ifFirst=0;
  left=right=""; process=false;
  ParamEntryVal decompile = compileConfig >> "Decompile";
  if (decompile.IsClass())
  {
    process = decompile >> "process";
    left = decompile >> "FSMLeft";
    right = decompile >> "FSMRight";
    ParamEntryVal prefix = decompile >> "FSMPrefix";
    ReadPreSufixes(prefix, 0, 0);
    prefix = decompile >> "FSMPrefix2";
    ReadPreSufixes(prefix, 0, 1);
    ParamEntryVal sufix = decompile >> "FSMSufix";
    ReadPreSufixes(sufix, 1, 0);
    sufix = decompile >> "FSMSufix2";
    ReadPreSufixes(sufix, 1, 1);
  }
}

static RString ConvertNewlinesToParamFile(const RString &text)
{
  QOStrStream out;
  for (int i=0, n=text.GetLength(); i<n; i++)
  {
    if (text[i]=='\r') {}
    else if (text[i]=='\n') out << "\" \\n \"";
    else if (text[i]=='\"') out << "\"\"";
    else out.put(text[i]);
  }
  return RString(out.str(),out.pcount());
}

// All classes (except for class Compile) can be used more than once, using any postfix, 
// to make class names unique.
void FSMGraph::Compile(const char *configFileName, const char *compiledFileName, QOStream &out)
{
  CompilePrint::graph = this;
  ParamFile compileConfig;
  compileConfig.Parse(configFileName);

  // read and prepare compileConfig first
  // prepare decompile
  DecompileInfo decInfo(compileConfig);
  dInfo = &decInfo;
  // prepare compile
  ParamEntryVal compile = compileConfig >> "Compile";
  int n = compile.GetEntryCount();
  CompileItem *root = NULL;
  if (n>0)
  {
    root = new CompileItem(CI_ROOT);
    for (int i=0; i<n; i++)
    {
      //there should be Pass_1 .. Pass_n
      ParamEntryVal pass = compile.GetEntry(i);
      if (pass.IsClass() && ParamEntryNameMatch(pass, "pass")) AddPassClass(pass, root);
      else if (pass.IsIntValue() && ParamEntryNameMatch(pass, "clearnewlines")) dInfo->clearNewLines = clearNewLines = pass.GetInt()!=0;
      else if (pass.IsIntValue() && ParamEntryNameMatch(pass, "rewritefile")) rewriteFile = pass.GetInt()!=0;
      else if (!CheckPreSufEntries(pass, root))
        Fail("Compile root class should have only Pass subclasses or clearNewLines element");
    }
  }
  else Fail("Compile root class should have some Pass subclasses or clearNewLines element");
  
  // insert FSMEditor comments first
  if (dInfo->process)
  {
    RString compileVal; //format is: "configFileName.cfg, compiledFileName.bifsm"
    RString &fsmName = decompileInfoGlobal.fsmName; //if empty, we need to change it globally
    if (fsmName==RString(""))
    {
      char fsmNameBuf[512];
      GetFilename(fsmNameBuf, compiledFileName);
      fsmName=RString(fsmNameBuf);
    }
    compileVal = configFileName + RString(", ") + fsmName;
    out << dInfo->Write(DCI_COMPILE, PMI_ITEM_NUMBER, true, false, compileVal);
    out << dInfo->Write(DCI_HEAD, PMI_ITEM_NUMBER, true, false, "");
    int n = decompileInfo.Size();
    char buf[8192];
    //vertices
    bool isLayoutItem = false;
    for (int i=0; i<n; ++i)
    {
      FSMDecompileInfo &di = decompileInfo[i];
      RString ditext = ConvertNewlinesToParamFile(di.text);
      sprintf(buf, "item%d[] = {\"%s\",%d,%d,%f,%f,%f,%f,%f,\"%s\"};\r\n", i, di.id.Data(), di.itemtype, di.flags, 
        di.left, di.top, di.right, di.bottom, di.priority, ditext.Data());
      out << buf;
      if (di.layout) isLayoutItem=true;
    }
    //layout items
    if (isLayoutItem)
    {
      ParamArchiveSave ar(1);
      ParamArchive layoutAr;
      if (ar.OpenSubclass("LayoutItems",layoutAr))
      {
        for (int i=0; i<n; ++i)
          if (decompileInfo[i].layout)
          {
            FSMDecompileInfo &di = decompileInfo[i];
            di.layout->SerializeItem(i,di.itemtype,*dconfigInfo,decompileInfo,layoutAr);
          }
      }
      QOStrStream ostr;
      ar.Save(ostr);
      out << RString(ostr.str(), ostr.pcount());
    }
    //links
    for (int i=0, n=decompileLinkInfo.Size(); i<n; i++)
    {
      sprintf(buf, "link%d[] = {%d,%d};\r\n", i, decompileLinkInfo[i].ixFrom, decompileLinkInfo[i].ixTo);
      out << buf;
    }
    //line for globals
    FSMDecompileInfoGlobal &gi = decompileInfoGlobal;
    sprintf
    (
    buf, "globals[] = {%f,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%f,%f,%f,%f,%d,%d,%d};\r\n", 
      gi._grid, gi._gridshow, gi._pageClip, gi._pageColor, gi._pageColorVal, 
      gi._pageImageX, gi._pageImageY, gi.grpcnt, gi.nextID,
      gi.defLinkColor, gi.defLinkUseCustom,
      gi.pzoomLeft, gi.pzoomRight, gi.pzoomBottom, gi.pzoomTop,
      gi.clxs, gi.clys, gi.aspect
    );
    out << buf;
    //line for window
    FSMDecompileInfoWindow &wi = decompileInfoWindow;
    sprintf
    (
    buf, "window[] = {%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d};\r\n", 
      wi.flags, wi.ptMaxPositionX, wi.ptMaxPositionY, wi.ptMinPositionX, wi.ptMinPositionY,
      wi.rcNormalPositionBottom, wi.rcNormalPositionLeft, wi.rcNormalPositionRight, wi.rcNormalPositionTop,
      wi.showCmd, wi.splitpos
    );
    out << buf;
    out << dInfo->Write(DCI_HEAD, PMI_ITEM_NUMBER, true, true, "");
  }

  if (root)
  {
    //Start & End States to current_values
    FindInitAndFinalStateNames();
    current_values.numStates=vertices.Size();
    for (CompileItemElement *rootEl=root->items.Start(); root->items.NotEnd(rootEl); rootEl=root->items.Advance(rootEl))
    {
      Assert(rootEl->item->type==CI_PASS || rootEl->item->type==CI_INDENT);
      // for each pass, reread the whole FSMGraph
      ClearCurrentValues();
      if (rootEl->item->type==CI_PASS) CompilePasses(rootEl, out, this);
      else if (rootEl->item->type==CI_INDENT) ((IndentItem*)rootEl->item)->UpdateIndent();
      else if (rootEl->item->type==CI_IFSTART) ((IfStartItem*)rootEl->item)->Update();
      else if (rootEl->item->type==CI_IFFINAL) ((IfFinalItem*)rootEl->item)->Update();
      else if (rootEl->item->type==CI_IFTOFINAL) ((IfToFinalItem*)rootEl->item)->Update();
      else Fail("Compile root class can contain only PASS subclasses or elements: indent, clearNewLines\n");
    }
  }
  if (dInfo->process) out << dInfo->Write(DCI_COMPILE, PMI_ITEM_NUMBER, true, true, "");
  delete root;
}
