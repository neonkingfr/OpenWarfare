#include "QStdStream.hpp"

RefQIStreamBuffer QStdInStream::ReadBuffer(RefQIStreamBuffer &buf, QFileSize pos)
{
  Assert(pos==buf._start+buf._len);
  buf.Free();
  int pageSize = GFileServerFunctions->GetPageSize();

  QIStreamBuffer *page = new QIStreamBufferPage(pageSize);
  void *data = page->DataLock(0,pageSize);

  // try to read some more characters
  int rd = ::read(_handle,data,pageSize);

  page->DataUnlock(0,pageSize);

  // return how much we read
  return RefQIStreamBuffer(page,pos,rd);
}
