#ifdef _MSC_VER
#pragma once
#endif

#ifndef _QSTDSTREAM_HPP
#define _QSTDSTREAM_HPP

#include <io.h>
#include <el/qstream/QStream.hpp>
#include <windows.h>  //because of QOutputDebugStringStream

enum { STDIN_HANDLE, STDOUT_HANDLE, STDERR_HANDLE };

//stdout & stderr connected stream
struct StdOutTraits
{
  static bool IsFileStream() {return true; }
  static int GetHandle() {return STDOUT_HANDLE; }
};
struct StdErrTraits
{
  static bool IsFileStream() {return true; }
  static int GetHandle() {return STDERR_HANDLE; }
};
struct StdOutputDebugStringTraits
{
  static bool IsFileStream() {return false; }
  static int GetHandle() {return NULL; }
};

template<class Traits=StdOutTraits>
class QStdStream : public QOStream
{
public:
  ~QStdStream() { close(); }
  void open() {}
  void close() { Flush(); _buf.Free(); } 
  void SetFilePos(int pos) {} //_error=LSUnknownError; _fail=true; //these are not members of QOStream

  virtual void Flush(int size=0);
};

template<class Traits>
void QStdStream<Traits>::Flush(int size)
{
  const char *data = (const char *)_buf->DataLock(0, _bufferSize);
  if (Traits::IsFileStream())
    ::write(Traits::GetHandle(), data, _bufferSize);
  else //no FileStream means OutputDebugStr wanted
  {
    RString debugStr(data, _bufferSize);
    OutputDebugString(debugStr.Data());
  }
  _buf->DataUnlock(0, _bufferSize);
  _bufferStart += _bufferOffset;
  _bufferSize = 0;
  _bufferOffset = 0;
}

typedef QStdStream<StdOutTraits> QStdOutStream;
typedef QStdStream<StdErrTraits> QStdErrStream;
typedef QStdStream<StdOutputDebugStringTraits> QOutputDebugStringStream;

/// stdin connected stream
class QStdInStream: public QIStream
{
  int _handle;

  //! source can read more, but it always has to read at least on byte on pos
  virtual RefQIStreamBuffer ReadBuffer(RefQIStreamBuffer &buf, QFileSize pos); 

  virtual bool PreloadBuffer(
    RequestableObject *obj, RequestableObject::RequestContext *ctx,
    FileRequestPriority priority, QFileSize pos, QFileSize size
    ) const
  {
    // no pre loading on stdin possible
    return true;
  }
  virtual QFileSize GetDataSize() const
  {
    // pretend stdin is infinite, handle eof using ReadBuffer
    return QFileSizeMax;
  }
public:
  QStdInStream(int handle=STDIN_HANDLE)
    :_handle(handle)
  {
  }
  ~QStdInStream()
  {
    _buf.Free();
  }
};

#endif
