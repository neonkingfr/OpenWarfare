/*
  ParamFileForBeginners only shows usage of the basic ParamFile methods.
  It creates some paramFile (name must be supplied as commandline parameter)
  and converts it to xml (manually).
*/

// FSMCompiler.cpp : Defines the entry point for the console application.
//

//#include <El/elementpch.hpp>
#include <El/ParamFile/paramFile.hpp>
#include <Es/Strings/rstring.hpp>
#include <Es/Strings/bstring.hpp>
#include <stdio.h>

#include "QStdStream.hpp"

static void CreateParamFile(const char* fileName)
{
  ParamFile f;
  f.Add("myStringEntry", "myStringValue");
  ParamClassPtr ptr = f.AddClass("myFirstClassssss");
  ParamClassPtr ptr2 = f.AddClass("myFirstClassssss"); //the same class, so ptr==ptr2
  if (ptr==ptr2) {
    ptr->Add("parentEntry", 200);
    ParamEntryPtr array = ptr->AddArray("myFirstArray");
    //IParamArrayValue *valArr = array->AddArrayValue();
    array->AddValue("ptr");
    array->AddValue("equals");
    array->AddValue("ptr2");
    ParamClassPtr subclass = ptr->AddClass("firstSubclass");
    subclass->Add("onefivethree", 153);
    subclass->Add("onepointfivethree", 1.53f);
  }
  else
  {
    ptr->Add("message","ptr does not equal to ptr2");
  }
  f.Save(fileName);
}

static RString OneTab = "\t";
BString<2048> outstr;
static void PrintAsXMLRecursive(const ParamEntry &entry, QOStream &out, RString indentS=RString())
{
  // print any entry that has "comment" member present
  // when printing, print also "scope" member (if present)
  RString indentS1 = indentS+OneTab;
  int i;
  int count = entry.GetEntryCount();
  for (i=0; i<entry.GetEntryCount(); i++)
  {
    const ParamEntry &e = entry.GetEntry(i);
    if (!e.IsClass()) 
    {
      if (e.IsArray())
      {
        outstr.PrintF("%s<array name=\"%s\">\n", indentS.Data(), e.GetName().Data()); out<<outstr;
        for (int ix=0; ix<e.GetSize(); ix++)
        {
          outstr.PrintF("%s<value>%s</value>\n", indentS1.Data(), e[ix].GetValue().Data());  out<<outstr;
        }
        outstr.PrintF("%s</array>\n", indentS.Data()); out<<outstr;
      }
      else {
        RString entryType;
        if (e.IsFloatValue()) 
        {
          entryType="float";
          //float val = float(e);  //possible
        }
        if (e.IsTextValue()) entryType="text";
        if (e.IsIntValue()) entryType="int";
        outstr.PrintF
        (
          "%s<entry name=\"%s\" type=\"%s\" val=\"%s\"/>\n", 
          indentS.Data(), e.GetName().Data(), entryType.Data(), e.GetValue().Data()
        ); out<<outstr;
      }
    }
    else
    {
      outstr.PrintF("%s<class name=\"%s\">\n", indentS.Data(), e.GetName().Data());  out<<outstr;
      PrintAsXMLRecursive(e, out, indentS1);
      outstr.PrintF("%s</class>\n", indentS.Data());  out<<outstr;
    }
  }
}

static void PrintAsXML(const ParamEntry &entry, QOStream &out)
{
  outstr.PrintF("<?xml version=\"1.0\" ?>\n" "<paramfile>\n"); out<<outstr;
  PrintAsXMLRecursive(entry, out, OneTab);
  outstr.PrintF("</paramfile>\n"); out<<outstr;
}

void TryFinding(const char* fileName)
{
  ParamFile in;
  in.Parse(fileName);
  ParamEntryPtr ptr1 = in.FindEntry("onefivethree");
  ParamEntryPtr ptr2 = in.FindEntryNoInheritance("onefivethree");
  ParamEntryPtr ptr3 = in.FindEntryPath("myFirstClassssss/firstSubclass");
  ParamEntryPtr ptr4;
  if (ptr3) ptr4 = ptr3->FindEntry("oneFivetHree"); //yes, its case insensitive
  if (ptr4 && ptr4->IsIntValue())
    printf("int valued parentEntry found\n");
  else 
    printf("int valued parentEntry not found\n");
}

int main(int argc, const char *argv[])
{
  if (argc!=2) {printf("ussage: FSMCompiler fileToCreateName"); return 1; };
  CreateParamFile(argv[1]);
  ParamFile in;
  in.Parse(argv[1]);
/*
  if (in.Parse("cfgVehicles.hpp")==LSOK)  //cfgVehicles can be well parsed with Cpreprocessing (#including manActions.hpp)
    _asm nop;
*/
  QStdOutStream outStream;
  //QOFStream outStream("bebul.xml");
  PrintAsXML(in, outStream);
  //TryFinding(argv[1]);
  outStream.Flush();
  return 0;
}
