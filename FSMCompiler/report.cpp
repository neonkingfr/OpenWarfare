#include "report.h"
#include <Es/Strings/bstring.hpp>

static BString<8192> outstr;

void Report::PrintF(const char format[], ...)
{
  va_list vl;
  va_start(vl, format);
  vsprintf(outstr, format, vl);
  _report << outstr;
  va_end(vl);
}
void Report::PrintSummary()
{
  PrintF("\nFSMCompiler - %d error(s), %d warning(s)\n", _errors, _warnings);
  if (_errors) PrintF("Compilation ends with errors!\n");
}
Report& Report::operator<<(const char *val)
{
  PrintF(val);
  return(*this);
}
Report& Report::operator<<(const int val)
{
  PrintF("%d", val);
  return(*this);
}
Report& Report::operator<<(const float val)
{
  PrintF("%f", val);
  return(*this);
}
Report& Report::operator<<(const bool val)
{
  PrintF(val ? "true":"false");
  return(*this);
}
