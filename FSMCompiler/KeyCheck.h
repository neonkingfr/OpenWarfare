/*!
  Serial number verification support functions
  For checking serial number, use VerifyKey() function.

  Add to project: keycheck.cpp, serial.cpp, rstring.cpp

  KeyCheck.cpp uses rsa.lib, which must be linked (or other (rsamt, rsamtmfc, ...))
  Use sth. like:
  #ifdef _DEBUG
    #pragma comment(lib,"rsad")
  #else
    #pragma comment(lib,"rsa")
  #endif
*/
#ifndef _KEY_CHECK_H
#define _KEY_CHECK_H

#include "Es\Strings\rString.hpp"

RString GetPublicKey();
bool GetKeyFromRegistry(const char *path, unsigned char *cdkey);
//! Verify serial number
bool VerifyKey();

#endif
