// CD Key parameters

#include <El/CDKey/serial.hpp>
#include <Es/Strings/rString.hpp>
#include "KeyCheck.h"

//#define ConfigApp "Software\\Codemasters\\Operation Flashpoint"
//#define ConfigApp "Software\\Bohemia Interactive Studio\\FSMEditor"

const bool CDKeyRegistryUser = true;
const char CDKeyRegistryPath[] = "Software\\Bohemia Interactive Studio";
const unsigned char CDKeyPublicKey[] =
{
  0x11, 0x00, 0x00, 0x00,
    0x2F, 0xAD, 0xE2, 0xFA, 0x1A, 0xA2, 0x60, 0x95, 0xEB, 0x81, 0x76, 0xDB, 0xCC, 0xE3, 0x50
};
const char CDKeyFixed[] = "BIS Internal.";

const int CDKeyIdLength = (sizeof(CDKeyPublicKey) / sizeof(*CDKeyPublicKey) - 4) - strlen(CDKeyFixed);

CDKey GCDKey;

bool GetKeyFromRegistry(const char *path, unsigned char *cdkey)
{
  memset(cdkey, 0, KEY_BYTES);
  HKEY root = CDKeyRegistryUser ? HKEY_CURRENT_USER : HKEY_LOCAL_MACHINE;
  HKEY key;
  if
    (
    !::RegOpenKeyEx
    (
    root, path,
    0, KEY_READ, &key
    ) == ERROR_SUCCESS
    ) return false;

  DWORD size = KEY_BYTES;
  DWORD type = REG_BINARY;
  ::RegQueryValueEx(key, "KEY", 0, &type, cdkey, &size);
  ::RegCloseKey(key);
  return true;
}

bool VerifyKey()
{
  // no Win32 function may be called here
  // as it would make protection impossible

  unsigned char cdkey[KEY_BYTES];
  if (!GetKeyFromRegistry(CDKeyRegistryPath, cdkey))
  {
    return false;
  }

  GCDKey.Init(cdkey, CDKeyPublicKey);

  return GCDKey.Check(CDKeyIdLength, CDKeyFixed);  //this function generates memory leaks
}
