#include <io.h>
#include <stdio.h>
#include "KeyCheck.h"
#define _VERIFY_KEY  0

#ifdef _DEBUG
  #pragma comment(lib,"rsamtd")
#else
  #pragma comment(lib,"rsamt")
#endif

// FSMCompiler.cpp : Defines the entry point for the console application.
//

#define EXECUTE_FSMCOMPILER
#ifdef EXECUTE_FSMCOMPILER

#include <El/QStream/QStdStream.hpp>
#include <Es/Files/filenames.hpp>
#include <El/ParamFile/paramFile.hpp>
#include <fcntl.h>
#include "FSMGraph.h"
#include "paramFileCompiler.h"

//////////////////////////////////////////////////////////////////////////

static const char UsageString[] = 
"FSMCompiler.exe (ver. 1.2) usage:\r\n"
 "  Compile:\r\n"
 "    FSMCompiler [-n] [filename.bifsm|-stdin] [-o filename2|-stdout]\r\n"
 "  Decompile:\r\n"
 "    FSMCompiler -d filename|-stdin -f fsmName \r\n"
 "               [-c config.cfg] [-o filename.bifsm|-stdout]\r\n"
 "  List FSM from file: \r\n"
 "    FSMCompiler -l filename|-stdin\r\n"
 "List of parameters:\r\n"
 " -d      decompile                     -n      get only compilation report\r\n"
 " -stdin  compile/decompile from stdin  -stdout compile/decompile to stdout\r\n"
 " -c file specify compileConfig         -o file output file\r\n"
 " -l file list all FSMs from file       -f name specify FSM name\r\n"
 " -nocompile not compile through any compileConfig\r\n";
static QStdDebugStream debugOutStream;
static QStdOutStream stdOutStream;

//
//decompileMain ... the decompilation process
//
#include "DecompileParser.h"
static int decompileMain(const char *fileName,const char *configFileName, const char *outputFileName, const char *fsmName, bool saveOutput, Report &report)
{
  if (QFBankQueryFunctions::FileExists(fileName) || strcmpi(fileName,"stdin")==0)
  {
    DecompileChunks decChunks(fileName, configFileName, &report);
    RString outFSMName;
    if (!fsmName)
    {
      char buffile[1024];
      GetFilename(buffile, outputFileName);
      outFSMName = RString(buffile);
    }
    else outFSMName = RString(fsmName);
    FSMEdGraph *graph = decChunks.ConvertToFsmEdGraph(outFSMName, report);
    if (!graph)
    {
      report.PrintF("Error: cannot decompile FSM \"%s\" from \"%s\"\n", outFSMName.Data(), fileName);
      return 3;
    }
    graph->decompileInfoGlobal.fsmName=outFSMName;
    if (configFileName) graph->decompileInfoGlobal.compileConfig=configFileName;
    ParamArchiveSave ar(1);
    graph->Serialize(ar);
    RString outFileName;
    if (!outputFileName)
    {
      char buffile[1024];
      GetFilename(buffile, fileName);
      strcat(buffile,".bifsm");
      outFileName = RString(buffile);
    } 
    else outFileName = RString(outputFileName);
    if (strcmpi(outFileName,"stdout")==0)
    {
      QStdOutStream out;
      ar.Save(out);
//      ar.Save("debugOut.bifsm");
    }
    else ar.Save(outFileName);
    delete graph;
  }
  else 
  {
    report.PrintF("Error: file \"%s\" does not exist!\n", fileName);
    return 2;
  }

  return 0;
}

//
//Compilation helper functions
//
static RString GetFileContents(const char *fileName)
{
  FILE *file;
  RString output;
  if ((file = fopen(fileName, "rb"))!=NULL)
  {
    int len = _filelength(file->_file);
    char *buffer = new char[len+1];
    int bufsize = fread(buffer, 1, len, file);
    fclose(file);
    output = RString(buffer, bufsize);
    delete buffer;
  }
  return output;
}

//! if (outputFile exists)
//!   if (section to write compileOutput inside exists) write it inside;
//!   else write it to the end of this file;
//! else leave compileOutput untouched;
static RString UpdateByOutputFile(RString compileOutput, RString fsmName, const char *outputFileName)
{
  if (QFBankQueryFunctions::FileExists(outputFileName))
  {
    //read all COMPILE fields
    DecompileChunks decomp(outputFileName, "", NULL);
    //try to find COMPILE field matching compiledFileName
    DecompileNode* root = &decomp.root;
    fsmName.Lower();
    for (int i=0; i<root->nodes.Size(); ++i)
    {
      DecompileNodePtr compile = root->nodes[i];
      if (compile->isClass && compile->type==DCI_COMPILE)
      {
        //compile->beginTag.startix <<== start of particular field (index into outputFile)
        //compile->endTag.endix <<== end of particular field
        if (GetFSMName(compile->value)==fsmName)
        {
          IndentText(compileOutput, compile->beginTag.indent);
          RString fileContent = GetFileContents(outputFileName);
          int startIx = compile->beginTag.startix-(compile->beginTag.indent>0 ? compile->beginTag.indent : 0);
          RString header = fileContent.Substring(0, startIx);
          RString tail = fileContent.Substring(compile->endTag.endix, fileContent.GetLength());
          return header + compileOutput + tail;
        }
      }
    }
    //add compileOutput to the end of outputFile
    if (decomp.append==-1)
      return GetFileContents(outputFileName) + RString("\r\n\r\n") + compileOutput;
    else
    { //append to the specified place
      IndentText(compileOutput, decomp.globalIndent);
      RString fileContent = GetFileContents(outputFileName);
      RString header = fileContent.Substring(0, decomp.append);
      RString tail = fileContent.Substring(decomp.append, fileContent.GetLength());
      return header + compileOutput + RString("\r\n\r\n") + tail;
    }
  }
  //leave compileOutput untouched
  return compileOutput;
}

//
//compileMain ... the compilation process
//
static int compileMain(
  const char *fileName,const char *configFileName, const char *outputFileName1, 
  const char *fsmName, bool saveOutput, Report &report, bool compile)
{
  RString outputFileName = outputFileName1;
  if (fsmName!=NULL) report.PrintF("Warning: specifying parameter \"-f %s\" has no reason while compiling!\n", fsmName);
  ParamArchiveLoad ar;
  if (strcmp(fileName, "stdin")==0)
  {
    QStdInStream in;
    if (!ar.Load(in)) 
    {
      report << "Error: Cannot load fsm from stdin.\n";
      return 1;
    }
  }
  else if (!QFBankQueryFunctions::FileExists(fileName) || !ar.Load(fileName))
  {
    report.PrintF("Error: Cannot load file %s.\n", fileName);
    report.Error();
    return 2;
  }
  //Read FSM
  //FSMEditor structure first
  FSMEdGraph graph;
  if (graph.Serialize(ar)!=LSOK)
  {
    report.PrintF("Error while processing file %s.\n", fileName);
    report.Error();    
    return 3;
  }
  RString fsmName1(graph.decompileInfoGlobal.fsmName);
  report << "FSMCompiler: compiling FSM " << fsmName1 << "\n";
  //Eliminate knees
  AutoArray<FSMLink> backupLinks = graph.edges;
  graph.EliminateKnees();
  //Convert FSMEdGraph to FSMGraph

  FSMGraph *fsmg = graph.ConvertToFSMGraph(report, backupLinks, true);  //note: during decompilation the stateNames cannot be shortened for backward compatibility
  report.PrintSummary();

  RString configFile = FindFilePath(configFileName);
  if (configFile==RString("")) configFile = FindFilePath(graph.decompileInfoGlobal.compileConfig);
  //And Serialize it at last
#if _DEBUG
  if (saveOutput)
  {
    QStdDebugStream &out = debugOutStream;
    fsmg->Save(out, true);
    out.Flush();

    if (compile && configFile!=RString(""))
    {
      out << "\n************* Compilation result follows *************\n";
      if (configFile==RString(""))
      {
        report << "Error: Config file " << configFileName << " does not found\n";
        delete fsmg;
        return 1;
      }
      QOStrStream outstr;
      fsmg->Compile(configFile, fileName, outstr);
      RString compileOutput = RString(outstr.str(), outstr.pcount());
      if (!fsmg->rewriteFile) compileOutput = UpdateByOutputFile(compileOutput, fsmg->decompileInfoGlobal.fsmName, outputFileName);
      out << compileOutput;
      out.Flush();
    }
  }
#else
  if (!compile)
  {
    if (outputFileName)
    {
      if (saveOutput) 
      {
        if (strcmpi(outputFileName,"stdout")==0)
        {
          fsmg->Save(report.GetQOStream(), true);
        }
        else
        {
          QOFStream out(outputFileName);
          fsmg->Save(out, true);
          if (out.fail())
          {
            report << "\nError: cannot save the output into file " << outputFileName << "\n(Possibly read only attribute...)\n";
            report.Error();
          }
          out.close();
        }
      }
    }
    else
    {
      char outfile[1024];
      GetFilename(outfile, fileName);
      strcat(outfile,".fsm");
      if (saveOutput) 
      {
        QOFStream out(outfile);
        fsmg->Save(out, true);
        out.close();
      }
    }
  }
  else if (compile)
  {
    // parse using fsm compile config
    if (configFile==RString(""))
    {
      report << "Error: Configuration file " << (configFileName?configFileName:"") << " does not found\n";
      delete fsmg;
      return 1;
    }
    if (!outputFileName/* && strcmpi(outputFileName,"stdout")!=0*/)
    {
      char outfile[1024];
      GetFilename(outfile, fileName);
      strcat(outfile,".fsm");
      outputFileName = outfile;
    }
    if (saveOutput) 
    {
      QOStrStream outstr;
      fsmg->Compile(configFile, fileName, outstr);
      RString compileOutput = RString(outstr.str(), outstr.pcount());
      if (strcmpi(outputFileName,"stdout")!=0)
      {
        if (!report.GetErrorCount())
        {
          if (!fsmg->rewriteFile) compileOutput = UpdateByOutputFile(compileOutput, fsmg->decompileInfoGlobal.fsmName, outputFileName);
          QOFStream out(outputFileName);
          out << compileOutput;
          if (out.fail())
          {
            report << "\nError: cannot save the output into file " << outputFileName << "\n(Possibly read only attribute...)\n";
            report.Error();
          }
          out.close();
        }
        else
        {
          report << "There were errors in compilation! No file saved!!!\n";
          report.Error();
        }
      }
      else 
      {
        QStdOutStream out;
        out << compileOutput;
      }
    }
  }
#endif

  delete fsmg;
  return report.GetErrorCount();
}

static int listMain(const char *fileName, Report &report)
{
  DecompileChunks decomp(fileName, "", NULL);
  //try to find COMPILE field matching compiledFileName
  DecompileNode* root = &decomp.root;
  for (int i=0; i<root->nodes.Size(); i++)
  {
    DecompileNodePtr node = root->nodes[i];
    if (node->isClass && node->type==DCI_COMPILE)
      report.PrintF("%s\n", GetFSMName(node->value).Data());
  }
  return 0;
}

int consoleMain(int argc, const char* argv[])
{
  QOStream *poutStream;
#if _DEBUG
  poutStream = &debugOutStream;
#else
  poutStream = &stdOutStream;
#endif
  Report report(*poutStream);

  #if _VERIFY_KEY
  if (!VerifyKey()) 
  {
    report << "Bad serial number!\n"; return 5;
  }
  #endif
  //check commandline parameters
  if (argc<2) 
  {
    report << UsageString;
    return 1;
  }
  int paramIx=1;
  const char *fileName=NULL;
  const char *configFileName=NULL;
  const char *outputFileName=NULL;
  const char *fsmName=NULL;
  bool saveOutput = true;
  enum {COMPILE, DECOMPILE, LIST, NOCOMPILE} action = COMPILE;
  for (; paramIx<argc; paramIx++)
  {
    if (!strcmpi(argv[paramIx],"-n")) saveOutput = false;
    else if (!strcmpi(argv[paramIx],"-c")) configFileName=argv[++paramIx];
    else if (!strcmpi(argv[paramIx],"-o")) outputFileName=argv[++paramIx];
    else if (!strcmpi(argv[paramIx],"-f")) fsmName=argv[++paramIx];
    else if (!strcmpi(argv[paramIx],"-d")) action = DECOMPILE;
    else if (!strcmpi(argv[paramIx],"-l")) action = LIST;
    else if (!strcmpi(argv[paramIx],"-nocompile")) action = NOCOMPILE;
    else if (!strcmpi(argv[paramIx],"-stdin")) fileName="stdin";
    else if (!strcmpi(argv[paramIx],"-stdout")) outputFileName="stdout";
    else if (!fileName) fileName = argv[paramIx];
    else
    {
      report << UsageString;
      return 1;
    }
  }
  if (fileName==NULL) 
  {
    report << "Error: file name or -stdin attribute must be specified!\n";
    return 1;
  }

  switch (action)
  {
  case NOCOMPILE:
  case COMPILE: 
    return compileMain(fileName, configFileName, outputFileName, fsmName, saveOutput, report, action==COMPILE);
  case DECOMPILE: return decompileMain(fileName, configFileName, outputFileName, fsmName, saveOutput, report);
  case LIST: return listMain(fileName, report);
  }
  return 1; //but impossible
}

int main( int argc, const char *argv[] )
{
  //InitConsoleApp();
  int ret = consoleMain(argc,argv);
  //if (ret) wasError = true;
  return ret;
}

#endif

//////////////////////////////////////////////////////////////////////////
//
// Spawning FSMCompiler example
//

#ifndef EXECUTE_FSMCOMPILER

#include <stdlib.h>
#include <fcntl.h>
#include <process.h>
#include <Windows.h>

#define   OUT_BUFF_SIZE 512
#define   READ_FD 0
#define   WRITE_FD 1
char szBuffer[OUT_BUFF_SIZE];
char * myargv[] = {"c:\\bis\\fsmeditor\\fsmcompiler.exe", "-n", "w:\\c\\aitools\\graphtest\\teleport.bifsm"};

int consoleMain(int argc, const char* argv[])
{
  HANDLE hProcess;
  int fdStdOut;
  int fdStdOutPipe[2];
  if(_pipe(fdStdOutPipe, 512, O_NOINHERIT) == -1) return   1;
  fdStdOut = _dup(_fileno(stdout));
  if(_dup2(fdStdOutPipe[WRITE_FD], _fileno(stdout)) != 0) return   2;
  close(fdStdOutPipe[WRITE_FD]);
  hProcess = (HANDLE)spawnvp(P_NOWAIT, myargv[0], myargv);
  if(_dup2(fdStdOut, _fileno(stdout)) != 0) return   3;
  close(fdStdOut);
  if(hProcess)
  {
    int nOutRead;
    int nExitCode = STILL_ACTIVE;
    while (nExitCode == STILL_ACTIVE)
    {
      nOutRead = read(fdStdOutPipe[READ_FD], szBuffer, OUT_BUFF_SIZE);
      if(nOutRead)
      {
        //nOutRead = Zpracuj(szBuffer, nOutRead); //tady to nejak parsuju, ulozim...
        fwrite(szBuffer, 1, nOutRead, stdout);  //a treba printnu
      }
      if(!GetExitCodeProcess(hProcess,(unsigned long*)&nExitCode)) return 4;
    }
  }
  return 0;
}

int main( int argc, const char *argv[] )
{
  //InitConsoleApp();
  int ret = consoleMain(argc,argv);
  //if (ret) wasError = true;
  return ret;
}

#endif
