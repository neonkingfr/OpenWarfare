#ifndef _DECOMPILE_PARSER_H
#define _DECOMPILE_PARSER_H

#include "paramFileCompiler.h"

class FSMTag
{
public:
  //! indices into DecompileParser::buffer
  int startix, endix;
  //! indent
  int indent;
  //! true for open <tag>, false for close </tag>
  bool isOpenTag;
  //!when the tag is Open and Closed at the same time <append/>
  bool isClosed; 
  //! tag value
  RString value;
  //! tag name (valid only for compile config attributes)
  RString name;
  //! tag type and classeness
  bool isClass;     
  int type;     //PrintFormatItem or DecompileClass enum (depend on isClass)
  //! only for %modifier(...)
  PrintModifierItem modifier;

  FSMTag() { isClass=true; startix=endix=indent=0; type=DCI_ROOT; }
  RString GetText(char *buffer) { return RString(buffer+startix, endix-startix); }
};

//! helper class for parsing compiled file
class DecompileParser
{
public:
  //! for error announcement
  Report *report;
  char *buffer;
  int   bufix, bufsize;
  int   lastBufIx; //last bufix, before call to FindNextTag
  DecompileInfo *dInfo;
  int   bufstart;  //start of begining of last not handled text

  //! constructor initializes buffer with fileName content and grab some infos from configFileName, if specified
  DecompileParser(RString fileName, RString configFileName, Report *errrep);

  //! return false, iff next tag found
  bool FindNextTag(FSMTag &tag);
  //! return true, iff parsing successful
  bool ParseTag(FSMTag &tab);
  //! returns true for correct tag name
  bool ParseName(FSMTag &tag, int ix, int len);
  //! returns true for correct parsing of value (especially all " backslashed \")
  bool ParseValue(FSMTag &tag, int ix);

};

//! DecompileNode keep information needed for decompiling file into *.bifsm 
//! or updating file using new *.bifsm
class DecompileNode;
class DecompileChunks;
typedef DecompileNode * DecompileNodePtr;
TypeIsMovable(DecompileNodePtr);
class DecompileNode : public NoCopy
{
public:
  DecompileNode *parent;
  bool isClass;
  int type;      //PrintFormatItem or DecompileClass enum (depend on isClass)
  //!for printItems: text between begin and end tags
  //!for PASS class: begin tag value
  RString value; 
  //!name is valid only for compile config attribute
  RString name;
  FSMTag beginTag, endTag;
  AutoArray<DecompileNodePtr> nodes;

  DecompileNode() {parent=NULL;}
  //! destruct the hierarchy recursively
  void Clear();

  //! returns true, iff tag can be subtag of this
  bool CheckSubtag(FSMTag &tag);
  //! used only for value of HEAD, now
  void AddStaticTextNode(DecompileParser &parser, int ix, int len);
  //! parse the subtree of specified tag, using DecompileParser
  bool Parse(FSMTag &tag, DecompileParser &parser, DecompileChunks *chunks);
  bool Parse();  //root node parsing, no begin, nor end tags

#ifdef _DEBUG
  void PrintStructure(Report &report, RString indent="");
#endif
};

//! Header structure for tree of DecompileNodes
class DecompileChunks
{
private:
  static RString _condname, _statename;
  FSMEdGraph * _graph;
  FSMGraph   * _fsmg;
  int _condIx, _stateIx;
public:
  Report *report;
  RString configFileName;
  //!root of DecompileNode tree structure with possibly many compile items
  DecompileNode root;
  //!compile node matching proper *.bifsm file name
  DecompileNodePtr compile;
  //!append==-1 if appending to the end of file, or index to file otherwise
  int append;
  int globalIndent; //indent of append tag is used as global Indent

  //! "deep" constructor, it creates the whole DecompileNode tree structure using DecompileParser
  DecompileChunks(RString fileName, RString configFileName, Report *report);
  //! deep destructor for DecompileNode tree structure
  ~DecompileChunks() { root.Clear(); }
  
  //! get info from value of %FSM<HEAD> tag (structure and attributes of FSMEditor graph)
  bool GetDecompileInfos(FSMEdGraph *graph);
  //! conversion to FSMEdGraph is useful because of FSMEdGraph::Serialize() method, which 
  //! can be used to create *.bifsm file, native for FSMEditor
  FSMEdGraph *ConvertToFsmEdGraph(RString &fsmName, Report &rep);
  //! compute indexes into vertices array. FSMGraph *_fsmg must be set before call to this function
  void ComputeStateConditionIx();
  //! add action, condition, preCondition, initcode from chunks into the FSMEdGraph structure (_graph)
  void AddChunksToGraph(DecompileNodePtr node);
};

extern RString GetFSMName(RString value, bool makeLoverCase=true);
extern RString GetConfigFileName(RString value);

#endif
