/*
	Class perform resampling of texture with using bicubic filter. Bicubic filter use precomputed coefficients,
	which are stored in 1D texture (lookUpTexture). Coefficinets are determined from fractional parts of 
	filtered texture coordinates. Fractional parts are periodic in the interval [0, 1]. Each texel of 1D 
	texture contain two offsets and two weights.
	When filtering is performed, then for each pixel are determined offsets and weights from lookup texture. 
	Lookup texture fetch is made on	the basis of the current texture coordinates. Offsets define relative 
	position of samples for linear texture fetches from filtered texture and wights are define linear combining
	colors get from texture fetches.

	Detailed description can be found in GPU Gems 2, chapter 20 - Fast Third-Order Texture Filtering.
*/

#pragma once

#include <d3d9.h>
#include <d3dx9.h>

#include <vector>

class BicubicFilter
{
private:
	struct Vertex	// full scene quad vertex element
	{
		D3DXVECTOR3 position;
		D3DXVECTOR2 texCoord;
	};

private:
	IDirect3DTexture9	*_srcTexture;	// filtered texture
	IDirect3DTexture9	*_lookUpTexture;// texture with precomputed offset and weights
	D3DXVECTOR4			_srcTexSize;	// _srcTexture dimension
	D3DXVECTOR4			_texelSize;		// _srcTexture texel dimension 

	ID3DXConstantTable		*_constTable;
	IDirect3DVertexShader9	*_vsShader;
	IDirect3DPixelShader9	*_psShader;
	IDirect3DVertexBuffer9	*_vb;		// full scene quad
	IDirect3DVertexDeclaration9 *_decl;	// position & texcoord

public:
	BicubicFilter(IDirect3DTexture9 *sourceTexture);
	~BicubicFilter(void);

public:
	bool Initialize(IDirect3DDevice9 *device);	
	void Release(void);							
	bool Render(IDirect3DDevice9 *device);

private:
	bool CreateLookupTexture(IDirect3DDevice9 *device, int width);
	void GenCoeefficients(std::vector<float> *vec, int itemsNum);
	bool CompileShaders(IDirect3DDevice9 *device);
	bool CreateVertexBuffer(IDirect3DDevice9 *device);
};
