
#include "msic.h"


unsigned int CBicDirectory::_counter = 0;

CBicDirectory::CBicDirectory (LPCTSTR directory, LPCTSTR defaultDir, CBicDirectory *parent)
: CBicBaseProperty (directory), _directoryParent (parent), _defaultDir (defaultDir)
{
}

CBicDirectory::CBicDirectory (LPCTSTR defaultDir, CBicDirectory *parent)
: _directoryParent (parent), _defaultDir (defaultDir)
{
	_property.Format (_T ("DIRECTORY_%08u"), ++_counter);
}

void CBicDirectory::ChangeDirectory ()
{
	_property.Format (_T ("DIRECTORY_%08u"), ++_counter);
}

void CBicDirectory::GetSourceDirectory (CBicTString &dir) const
{
	if (_directoryParent != NULL)
	{
		_directoryParent->GetSourceDirectory (dir);
	}

	LPCTSTR srcDir = _tcschr (_defaultDir, _T (':'));
	srcDir = srcDir == NULL ? static_cast<LPCTSTR> (_defaultDir) : srcDir + 1;

	if (_tcscmp (srcDir, _T ("SourceDir")) != 0)
	{
		dir += srcDir;
		dir += _T ("\\");
	}
	else
	{
		dir = _T (".\\");
	}
}

void CBicDirectory::GetDestinationDirectory (CBicTString &dir) const
{
	if (_directoryParent != NULL)
	{
		_directoryParent->GetDestinationDirectory (dir);
	}

	LPCTSTR dstDirEnd = _tcschr (static_cast<LPCTSTR> (_defaultDir), _T (':'));
	CBicTString dstDir (_defaultDir, dstDirEnd == NULL ? -1 : dstDirEnd - static_cast<LPCTSTR> (_defaultDir));

	if (dstDir != _T ("SourceDir"))
	{
		dir += dstDir;
		dir += _T ("\\");
	}
	else
	{
		dir = _T (".\\");
	}
}

void CBicDirectory::SetDirectory (LPCTSTR dir)
{
	_property = dir;
}

LPCTSTR CBicDirectory::GetDirectory () const
{
	return _property;
}

LPCTSTR CBicDirectory::GetDefaultDir () const
{
	return _defaultDir;
}

CBicDirectory *CBicDirectory::GetDirectoryParent () const
{
	return _directoryParent;
}

bool CBicDirectory::Fill (CBicMsiRecord &rec) const
{
	if (!rec.Fill (1, _property) ||
		!rec.Fill (2, _directoryParent != NULL ? static_cast<LPCTSTR> (_directoryParent->_property) : static_cast<LPCTSTR> (NULL)) ||
		!rec.Fill (3, _defaultDir))
	{
		return false;
	}

	return true;
}
