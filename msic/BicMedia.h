
//! Class of one record in Media table.
class CBicMedia
{
private:
	static unsigned int _counter;
	
protected:
	int _diskId;

	CBicArray<CBicFile*, CBicFile*> _files;

public:
	CBicMedia ();

	bool AddFile (CBicFile *file);
	
	bool Fill (CBicMsiRecord &rec, bool compress) const;

	static inline void ResetCounter ()
	{
		_counter = 0;
	}
};
