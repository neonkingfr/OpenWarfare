

class CBicMsiInfo;

//! Database of all needed information to create installer.
class CBicDatabase
{
	friend CBicMsiInfo;
	
protected:
  //! Database records.
	CBicArraySelfDestruct<CBicDirectory*, CBicDirectory*> _directories;
	CBicArraySelfDestruct<CBicComponent*, CBicComponent*> _components;
	CBicArraySelfDestruct<CBicFile*, CBicFile*> _files;
	CBicArraySelfDestruct<CBicMedia*, CBicMedia*> _media;
	CBicArraySelfDestruct<CBicRegLocator*, CBicRegLocator*> _regLocators;
	CBicArraySelfDestruct<CBicFeature*, CBicFeature*> _features;
	CBicArraySelfDestruct<CBicProperty*, CBicProperty*> _properties;
	CBicArraySelfDestruct<CBicInstallExecutiveAction*, CBicInstallExecutiveAction*> _installExecutiveActions;
	CBicArraySelfDestruct<CBicInstallUIAction*, CBicInstallUIAction*> _installUIActions;
	CBicArraySelfDestruct<CBicActionText*, CBicActionText*> _actionTexts;
	CBicArraySelfDestruct<CBicError*, CBicError*> _errors;
	CBicArraySelfDestruct<CBicEventMapping*, CBicEventMapping*> _eventMappings;
	CBicArraySelfDestruct<CBicDialog*, CBicDialog*> _dialogs;
	CBicArraySelfDestruct<CBicControl*, CBicControl*> _controls;
	CBicArraySelfDestruct<CBicControlEvent*, CBicControlEvent*> _controlEvents;
	CBicArraySelfDestruct<CBicControlCondition*, CBicControlCondition*> _controlConditions;
	CBicArraySelfDestruct<CBicTextStyle*, CBicTextStyle*> _textStyles;
	CBicArraySelfDestruct<CBicRadioButton*, CBicRadioButton*> _radioButtons;
	CBicArraySelfDestruct<CBicBinary*, CBicBinary*> _binaries;
	CBicArraySelfDestruct<CBicUIText*, CBicUIText*> _uiTexts;
	CBicArraySelfDestruct<CBicCustomAction*, CBicCustomAction*> _customActions;
	CBicArraySelfDestruct<CBicValidation*, CBicValidation*> _validations;
	CBicArraySelfDestruct<CBicMsiSummaryProperty*, CBicMsiSummaryProperty*> _summaryProperties;
	
  //! Contents of table RemoveFile of patch file *.MSP
	CBicArraySelfDestruct<CBicRemoveFile*, CBicRemoveFile*> _removeFiles;

  //! Data from past installer creation.
	CBicByteStream _legacyMsiInfoPropertyStream;
	CBicByteStream _legacyMsiInfoDirectoryStream;
	CBicByteStream _legacyMsiInfoFileStream;
	
public:
	CBicDatabase ();

	~CBicDatabase ();

  //! Save this database into installer *.MSI file.
	bool SaveAsMsi (LPCTSTR fromDir, LPCTSTR msiDbDir, LPCTSTR filename, bool tempDelete, bool compress);

  //! Methods to add records into database.
	CBicDirectory* NewDirectory (LPCTSTR directory, LPCTSTR defaultDir, CBicDirectory *parent = NULL);
	CBicDirectory* NewDirectory (LPCTSTR defaultDir, CBicDirectory *parent = NULL);
	CBicComponent* NewComponent (CBicDirectory *directory, short attributes, LPCTSTR condition, CBicFile *keyPath);
	CBicComponent* NewComponent (CBicDirectory *directory, short attributes, LPCTSTR condition, short sequence);
	CBicFile* NewFile (CBicMedia *media, LPCTSTR path, unsigned long size, short attributes, CBicComponent *component = NULL);
	CBicRemoveFile* NewRemoveFile (LPCTSTR file, CBicComponent *component, LPCTSTR fileName, short installMode);
	CBicMedia* NewMedia ();
	CBicRegLocator* NewRegLocator (CBicBaseProperty *property, short root, LPCTSTR key, LPCTSTR name, short type);
	CBicFeature* NewFeature (LPCTSTR title, LPCTSTR description, short display, short level, CBicDirectory *directory, 
		short attributes, CBicFeature *featureParent = NULL);
	CBicProperty* NewProperty (LPCTSTR property, bool storing = true);
	CBicProperty* NewProperty (LPCTSTR property, LPCTSTR value, bool storing = true);
	CBicProperty* NewProperty (LPCTSTR property, CBicDialog *dialog, bool storing = true);
	CBicCustomAction* NewCustomAction (short type, CBicBaseProperty *source, LPCTSTR target);
	CBicCustomAction* NewCustomAction (short type, CBicBinary *source, LPCTSTR target);
	CBicInstallExecutiveAction* NewInstallExecutiveAction (LPCTSTR action, LPCTSTR condition, short sequence);
	CBicInstallExecutiveAction* NewInstallExecutiveAction (LPCTSTR action, LPCTSTR condition);
	CBicInstallExecutiveAction* NewInstallExecutiveAction (CBicCustomAction *customAction, LPCTSTR condition);
	CBicInstallUIAction* NewInstallUIAction (LPCTSTR action, LPCTSTR condition, short sequence);
	CBicInstallUIAction* NewInstallUIAction (LPCTSTR action, LPCTSTR condition);
	CBicInstallUIAction* NewInstallUIAction (CBicDialog *actionDialog, LPCTSTR condition, short sequence);
	CBicInstallUIAction* NewInstallUIAction (CBicDialog *actionDialog, LPCTSTR condition);
	CBicInstallUIAction* NewInstallUIAction (CBicCustomAction *customAction, LPCTSTR condition);
	CBicMsiSummaryProperty* NewSummaryProperty (UINT property, UINT datatype);
	CBicMsiSummaryProperty* NewSummaryProperty (UINT property, UINT datatype, INT iValue);
	CBicMsiSummaryProperty* NewSummaryProperty (UINT property, UINT datatype, FILETIME &pftValue);
	CBicMsiSummaryProperty* NewSummaryProperty (UINT property, UINT datatype, LPCTSTR szValue);
	CBicActionText* NewActionText (LPCTSTR action, LPCTSTR condition, LPCTSTR = _T (""));
	CBicDialog* NewDialog (short hCentering, short vCentering, short width, short height, long attributes, LPCTSTR title);
	CBicDialog* NewDialog (LPCTSTR dialog, short hCentering, short vCentering, short width, short height, long attributes, LPCTSTR title);
	CBicControl* NewControl (LPCTSTR type, short x, short y, short width, short height, long attributes, 
		CBicBaseProperty *property, LPCTSTR text, LPCTSTR help, bool tab);
	CBicControl* NewControl (LPCTSTR control, LPCTSTR type, short x, short y, short width, short height, long attributes, 
		CBicBaseProperty *property, LPCTSTR text, LPCTSTR help, bool tab);
	CBicControl* NewControl (LPCTSTR type, short x, short y, short width, short height, long attributes, 
		CBicBaseProperty *property, CBicBinary *icon, LPCTSTR help, bool tab);
	CBicControlCondition* NewControlCondition (CBicControl *control, LPCTSTR action, LPCTSTR condition);
	CBicControlEvent* NewControlEvent (CBicControl *control, LPCTSTR event, LPCTSTR argument, LPCTSTR condition);
	CBicControlEvent* NewControlEvent (CBicControl *control, LPCTSTR event, CBicDialog *argumentDialog, LPCTSTR condition);
	CBicError* NewError (short error, LPCTSTR message = _T (""));
	CBicEventMapping* NewEventMapping (CBicControl *control, LPCTSTR event, LPCTSTR attribute);
	CBicTextStyle* NewTextStyle (LPCTSTR textStyle, LPCTSTR faceName, short size, long color, short styleBits);
	CBicRadioButtonGroup* NewRadioButtonGroup (LPCTSTR property, LPCTSTR value);
	CBicRadioButton* NewRadioButton (CBicRadioButtonGroup *group, LPCTSTR value, short x, short y, short width, short height,
		LPCTSTR text, LPCTSTR help);
	CBicBinary* NewIcon (LPCTSTR property, const unsigned char *binaries, unsigned long size);
	CBicBinary* NewIcon (LPCTSTR property, LPCTSTR fileName);
	CBicBinary* NewBinary (HMODULE module, LPCTSTR rscName, LPCTSTR rscType);
	CBicUIText* NewUIText (LPCTSTR key, LPCTSTR text);
	CBicValidation* NewValidation (LPCTSTR table, LPCTSTR column, LPCTSTR nullable, long minValue, long maxValue, LPCTSTR keyTable, 
		long keyColumn, LPCTSTR category, LPCTSTR set, LPCTSTR description);

	bool NewRecursiveDirectoriesComponentsFiles (CBicMedia *media, CBicFeature *feature, 
		LPCTSTR prefixDir, LPCTSTR defaultDir, CBicDirectory *parentDir, 
		short compAttributes, LPCTSTR compCondition, short fileAttributes);

  //! Methods to save database into installer *.MSI or patch *.MSP file.
	bool SaveMsiDirectories (CBicMsiDatabase &db);
	bool SaveMsiComponents (CBicMsiDatabase &db);
	bool SaveMsiFiles (CBicMsiDatabase &db);
	bool SaveMsiMedia (CBicMsiDatabase &db, bool compress);
	bool SaveMsiRegLocators (CBicMsiDatabase &db);
	bool SaveMsiFeatures (CBicMsiDatabase &db);
	bool SaveMsiProperties (CBicMsiDatabase &db);
	bool SaveMsiInstallExecutiveActions (CBicMsiDatabase &db);
	bool SaveMsiInstallUIActions (CBicMsiDatabase &db);
	bool SaveMsiActionTexts (CBicMsiDatabase &db);
	bool SaveMsiErrors (CBicMsiDatabase &db);
	bool SaveMsiEventMappings (CBicMsiDatabase &db);
	bool SaveMsiDialogs (CBicMsiDatabase &db);
	bool SaveMsiControls (CBicMsiDatabase &db);
	bool SaveMsiControlEvents (CBicMsiDatabase &db);
	bool SaveMsiControlConditions (CBicMsiDatabase &db);
	bool SaveMsiTextStyles (CBicMsiDatabase &db);
	bool SaveMsiRadioButtons (CBicMsiDatabase &db);
	bool SaveMsiBinary (CBicMsiDatabase &db);
	bool SaveMsiUITexts (CBicMsiDatabase &db);
	bool SaveMsiValidations (CBicMsiDatabase &db);
	bool SaveMsiListBoxes (CBicMsiDatabase &db);
	bool SaveMsiCustomActions (CBicMsiDatabase &db);
	bool SaveMsiRemoveFiles (CBicMsiDatabase &db);
	bool SaveMsiSummaryInformation (CBicMsiDatabase &db);
	bool SaveMsiCab (CBicMsiDatabase &db, LPCTSTR fromDir, bool tempDelete);
	bool CopyFiles (LPCTSTR fromDir, LPCTSTR msiDbDir);
	
	inline void ResetCounters ()
	{
		CBicBinary::ResetCounter ();
		CBicControl::ResetCounter ();
		CBicCustomAction::ResetCounter ();
		CBicDialog::ResetCounter ();
		CBicDirectory::ResetCounter ();
		CBicFeature::ResetCounter ();
		CBicFile::ResetCounter ();
		CBicInstallExecutiveAction::ResetCounter ();
		CBicInstallUIAction::ResetCounter ();
		CBicMedia::ResetCounter ();
		CBicRegLocator::ResetCounter ();
	}
	
	void SortFiles ()
	{
		unsigned int n = _files.GetSize ();
		if (n != 0)
		{
			::qsort (const_cast<void*> (static_cast<const void*>(_files.GetData ())), n, sizeof (CBicFile*), &CompareFiles);
		}
	}

	static int __cdecl CompareFiles (const void *elem1, const void *elem2)
	{
		const CBicFile *const*f1 = reinterpret_cast<const CBicFile *const *> (elem1);
		const CBicFile *const*f2 = reinterpret_cast<const CBicFile *const *> (elem2);
		return (**f1).GetSequence () - (**f2).GetSequence ();
	}
};
