
#include "msic.h"


CBicMsiSummaryProperty::CBicMsiSummaryProperty (UINT property, UINT datatype)
: _property (property), _datatype (datatype), _iValue (0), _pftValue (NULL), _szValue (NULL)
{
}

CBicMsiSummaryProperty::CBicMsiSummaryProperty (UINT property, UINT datatype, INT iValue)
: _property (property), _datatype (datatype), _pftValue (NULL), _szValue (NULL), 
_iValue (iValue)
{
}

CBicMsiSummaryProperty::CBicMsiSummaryProperty (UINT property, UINT datatype, FILETIME &pftValue)
: _property (property), _datatype (datatype), _iValue (0), _szValue (NULL),
 _pftValueBuffer (pftValue), _pftValue (&_pftValueBuffer)
{
}

CBicMsiSummaryProperty::CBicMsiSummaryProperty (UINT property, UINT datatype, LPCTSTR szValue)
: _property (property), _datatype (datatype), _iValue (0), _pftValue (NULL), _szValue (szValue)
{
}

bool CBicMsiSummaryProperty::SetProperty (CBicMsiSummaryInformation &sumInfo) const
{
	UINT err = MsiSummaryInfoSetProperty (sumInfo.GetMsiHandle (), _property, _datatype, _iValue, _pftValue, _szValue);
	if (err != ERROR_SUCCESS)
	{
		msicLogErr1 (_T ("MsiSummaryInfoSetProperty failed ["MSI_ERR_FMT"].\r\n"), __FILE__, __LINE__, err);
		return false;
	}

	return true;
}

bool CBicMsiSummaryProperty::GenGuid ()
{
	return ::GenGuid (_szValue);
}
