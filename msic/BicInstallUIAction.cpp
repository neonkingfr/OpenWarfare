
#include "msic.h"


unsigned int CBicInstallUIAction::_counter = 0;


CBicInstallUIAction::CBicInstallUIAction (LPCTSTR action, LPCTSTR condition, short sequence)
: _sequence (sequence), _actionDialog (NULL), _action (action), _condition (condition), _customAction (NULL)
{
}

CBicInstallUIAction::CBicInstallUIAction (LPCTSTR action, LPCTSTR condition)
: _sequence (++_counter * 100), _actionDialog (NULL), _action (action), _condition (condition), _customAction (NULL)
{
}

CBicInstallUIAction::CBicInstallUIAction (CBicDialog *actionDialog, LPCTSTR condition, short sequence)
: _sequence (sequence), _actionDialog (actionDialog), _action (), _condition (condition), _customAction (NULL)
{
}

CBicInstallUIAction::CBicInstallUIAction (CBicDialog *actionDialog, LPCTSTR condition)
: _sequence (++_counter * 100), _actionDialog (actionDialog), _action (), _condition (condition), _customAction (NULL)
{
}

CBicInstallUIAction::CBicInstallUIAction (CBicCustomAction *customAction, LPCTSTR condition)
: _sequence (++_counter * 100), _actionDialog (NULL), _action (), _condition (condition), _customAction (customAction)
{
}

bool CBicInstallUIAction::Fill (CBicMsiRecord &rec) const
{
	if (!rec.Fill (1, _actionDialog != NULL ? _actionDialog->GetDialog () : (_customAction != NULL ? _customAction->GetAction () : _action)) ||
		!rec.Fill (2, _condition) ||
		!rec.Fill (3, _sequence))
	{
		return false;
	}

	return true;
}