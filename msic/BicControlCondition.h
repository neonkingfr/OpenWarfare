
//! Class of one record in ControlCondition table.
class CBicControlCondition
{
protected:
	CBicControl *_control;
	CBicTString _action;
	CBicTString _condition;

public:
	CBicControlCondition (CBicControl *control, LPCTSTR action, LPCTSTR condition);
	
	bool Fill (CBicMsiRecord &rec) const;
};