
#include "msic.h"


CBicTextStyle::CBicTextStyle (LPCTSTR textStyle, LPCTSTR faceName, short size, long color, short styleBits)
: _size (size), _color (color), _styleBits (styleBits), _textStyle (textStyle), _faceName (faceName)
{
}

bool CBicTextStyle::Fill (CBicMsiRecord &rec) const
{
	if (!rec.Fill (1, _textStyle) ||
		!rec.Fill (2, _faceName) ||
		!rec.Fill (3, _size) ||
		!rec.Fill (4, _color) ||
		!rec.Fill (5, _styleBits))
	{
		return false;
	}

	return true;
}