
#define MSI_PID_CODEPAGE		  1
#define MSI_PID_TITLE			    2
#define MSI_PID_SUBJECT			  3
#define MSI_PID_AUTHOR			  4
#define MSI_PID_KEYWORDS		  5
#define MSI_PID_COMMENTS		  6
#define MSI_PID_TEMPLATE		  7
#define MSI_PID_LASTAUTHOR	  8
#define MSI_PID_REVNUMBER		  9
#define MSI_PID_LASTPRINTED		11
#define MSI_PID_CREATE_DTM		12
#define MSI_PID_LASTSAVE_DTM	13
#define MSI_PID_PAGECOUNT		  14
#define MSI_PID_WORDCOUNT		  15
#define MSI_PID_CHARCOUNT		  16
#define MSI_PID_APPNAME			  18
#define MSI_PID_SECURITY		  19

//! Summary information of MSI database.
class CBicMsiSummaryInformation
{
protected:
	MSIHANDLE _summaryInfo;

public:
	CBicMsiSummaryInformation ();
	~CBicMsiSummaryInformation ();

	MSIHANDLE GetMsiHandle () const;

	bool Open (CBicMsiDatabase &db, UINT updateCount);
	void Close ();
};