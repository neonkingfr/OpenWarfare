
#include "msic.h"


CBicUIText::CBicUIText (LPCTSTR key, LPCTSTR text) : _key (key), _text (text)
{
}

bool CBicUIText::Fill (CBicMsiRecord &rec) const
{
	if (!rec.Fill (1, _key) ||
		!rec.Fill (2, _text))
	{
		return false;
	}

	return true;
}