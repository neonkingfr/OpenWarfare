
#include "msic.h"


CBicMsiDatabase::CBicMsiDatabase ()
: _database (NULL)
{
}

CBicMsiDatabase::~CBicMsiDatabase ()
{
	Close ();
}

MSIHANDLE CBicMsiDatabase::GetMsiHandle () const
{
	return _database;
}

bool CBicMsiDatabase::Create (LPCTSTR msiDbDir, LPCTSTR filename)
{
	if (_database != NULL)
	{
		msicLogErr0( _T( "MSI database already opened.\r\n" ), __FILE__, __LINE__ );
		return false;
	}

	if (_taccess (msiDbDir, 06) != 0)
	{
		if (!CreateDirectory (msiDbDir, NULL))
		{
			return false;
		}
	}

	CBicTString full (msiDbDir);
	full += filename;
	UINT err = MsiOpenDatabase (full, MSIDBOPEN_CREATE, &_database);
	if (err != ERROR_SUCCESS)
	{
		msicLogErr1 (_T ("MsiOpenDatabase failed ["MSI_ERR_FMT"].\r\n"), __FILE__, __LINE__, err);
		return false;
	}

	return true;
}

void CBicMsiDatabase::Close ()
{
	if (_database != NULL)
	{
		MsiDatabaseCommit (_database);
		MsiCloseHandle (_database);
		_database = NULL;
	}
}

bool CBicMsiDatabase::ExeCmd (LPCTSTR cmd)
{
	CBicMsiView view;

	if (!view.Open (*this, cmd) || !view.Execute ())
	{
		return false;
	}

	view.Close ();

	return true;
}