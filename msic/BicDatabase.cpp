
#include "msic.h"

CBicDatabase::CBicDatabase ()
{
}

CBicDatabase::~CBicDatabase ()
{
}

CBicDirectory* CBicDatabase::NewDirectory (LPCTSTR directory, LPCTSTR defaultDir, CBicDirectory *parent)
{
	CBicDirectory *o = new CBicDirectory (directory, defaultDir, parent);
	if (o != NULL)
	{
		if (!_directories.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicDirectory* CBicDatabase::NewDirectory (LPCTSTR defaultDir, CBicDirectory *parent)
{
	CBicDirectory *o = new CBicDirectory (defaultDir, parent);
	if (o != NULL)
	{
		if (!_directories.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicComponent* CBicDatabase::NewComponent (CBicDirectory *directory, short attributes, LPCTSTR condition, CBicFile *keyPath)
{
	CBicComponent *o = new CBicComponent (directory, attributes, condition, keyPath);
	if (o != NULL)
	{
		if (!_components.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicComponent* CBicDatabase::NewComponent (CBicDirectory *directory, short attributes, LPCTSTR condition, short sequence)
{
	CBicComponent *o = new CBicComponent (directory, attributes, condition, sequence);
	if (o != NULL)
	{
		if (!_components.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicFile* CBicDatabase::NewFile (CBicMedia *media, LPCTSTR path, unsigned long size, short attributes, CBicComponent *component)
{
	CBicFile *o = new CBicFile (path, size, attributes, component);
	if (o != NULL)
	{
		if (!_files.Add (o)) { delete o; return NULL; }
		if (!media->AddFile (o)) { return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicMedia* CBicDatabase::NewMedia ()
{
	CBicMedia *o = new CBicMedia ();
	if (o != NULL)
	{
		if (!_media.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicRegLocator* CBicDatabase::NewRegLocator (CBicBaseProperty *property, short root, LPCTSTR key, LPCTSTR name, short type)
{
	CBicRegLocator *o = new CBicRegLocator (property, root, key, name, type);
	if (o != NULL)
	{
		if (!_regLocators.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicFeature* CBicDatabase::NewFeature (LPCTSTR title, LPCTSTR description, short display, short level, 
									   CBicDirectory *directory, short attributes, CBicFeature *featureParent)
{
	CBicFeature *o = new CBicFeature (title, description, display, level, directory, attributes, featureParent);
	if (o != NULL)
	{
		if (!_features.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicProperty* CBicDatabase::NewProperty (LPCTSTR property, bool storing)
{
	CBicProperty *o = new CBicProperty (property, storing);
	if (o != NULL)
	{
		if (!_properties.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicProperty* CBicDatabase::NewProperty (LPCTSTR property, LPCTSTR value, bool storing)
{
	CBicProperty *o = new CBicProperty (property, value, storing);
	if (o != NULL)
	{
		if (!_properties.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicProperty* CBicDatabase::NewProperty (LPCTSTR property, CBicDialog *dialog, bool storing)
{
	CBicProperty *o = new CBicProperty (property, dialog, storing);
	if (o != NULL)
	{
		if (!_properties.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicRadioButtonGroup* CBicDatabase::NewRadioButtonGroup (LPCTSTR property, LPCTSTR value)
{
	CBicRadioButtonGroup *o = new CBicRadioButtonGroup (property, value);
	if (o != NULL)
	{
		if (!_properties.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicInstallExecutiveAction* CBicDatabase::NewInstallExecutiveAction (LPCTSTR action, LPCTSTR condition)
{
	CBicInstallExecutiveAction *o = new CBicInstallExecutiveAction (action, condition);
	if (o != NULL)
	{
		if (!_installExecutiveActions.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicInstallExecutiveAction* CBicDatabase::NewInstallExecutiveAction (LPCTSTR action, LPCTSTR condition, short sequence)
{
	CBicInstallExecutiveAction *o = new CBicInstallExecutiveAction (action, condition, sequence);
	if (o != NULL)
	{
		if (!_installExecutiveActions.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicInstallExecutiveAction* CBicDatabase::NewInstallExecutiveAction (CBicCustomAction *customAction, LPCTSTR condition)
{
	CBicInstallExecutiveAction *o = new CBicInstallExecutiveAction (customAction, condition);
	if (o != NULL)
	{
		if (!_installExecutiveActions.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicInstallUIAction* CBicDatabase::NewInstallUIAction (LPCTSTR action, LPCTSTR condition)
{
	CBicInstallUIAction *o = new CBicInstallUIAction (action, condition);
	if (o != NULL)
	{
		if (!_installUIActions.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicInstallUIAction* CBicDatabase::NewInstallUIAction (LPCTSTR action, LPCTSTR condition, short sequence)
{
	CBicInstallUIAction *o = new CBicInstallUIAction (action, condition, sequence);
	if (o != NULL)
	{
		if (!_installUIActions.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicInstallUIAction* CBicDatabase::NewInstallUIAction (CBicDialog *actionDialog, LPCTSTR condition)
{
	CBicInstallUIAction *o = new CBicInstallUIAction (actionDialog, condition);
	if (o != NULL)
	{
		if (!_installUIActions.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicInstallUIAction* CBicDatabase::NewInstallUIAction (CBicDialog *actionDialog, LPCTSTR condition, short sequence)
{
	CBicInstallUIAction *o = new CBicInstallUIAction (actionDialog, condition, sequence);
	if (o != NULL)
	{
		if (!_installUIActions.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicInstallUIAction* CBicDatabase::NewInstallUIAction (CBicCustomAction *customAction, LPCTSTR condition)
{
	CBicInstallUIAction *o = new CBicInstallUIAction (customAction, condition);
	if (o != NULL)
	{
		if (!_installUIActions.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicMsiSummaryProperty* CBicDatabase::NewSummaryProperty (UINT property, UINT datatype)
{
	CBicMsiSummaryProperty *o = new CBicMsiSummaryProperty (property, datatype);
	if (o != NULL)
	{
		if (!_summaryProperties.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicMsiSummaryProperty* CBicDatabase::NewSummaryProperty (UINT property, UINT datatype, INT iValue)
{
	CBicMsiSummaryProperty *o = new CBicMsiSummaryProperty (property, datatype, iValue);
	if (o != NULL)
	{
		if (!_summaryProperties.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicMsiSummaryProperty* CBicDatabase::NewSummaryProperty (UINT property, UINT datatype, FILETIME &pftValue)
{
	CBicMsiSummaryProperty *o = new CBicMsiSummaryProperty (property, datatype, pftValue);
	if (o != NULL)
	{
		if (!_summaryProperties.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicMsiSummaryProperty* CBicDatabase::NewSummaryProperty (UINT property, UINT datatype, LPCTSTR szValue)
{
	CBicMsiSummaryProperty *o = new CBicMsiSummaryProperty (property, datatype, szValue);
	if (o != NULL)
	{
		if (!_summaryProperties.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

bool CBicDatabase::NewRecursiveDirectoriesComponentsFiles (CBicMedia *media, CBicFeature *feature, 
														   LPCTSTR prefixDir, LPCTSTR defaultDir, CBicDirectory *parentDir,
														   short compAttributes, LPCTSTR compCondition, short fileAttributes)
{
	CBicDirectory *dir = NewDirectory (defaultDir, parentDir);
	if (dir == NULL)
	{
		return false;
	}

	CBicTString dirStr;
	dir->GetSourceDirectory (dirStr);
	dirStr.AddPrefix (prefixDir);
	DWORD fAttr = GetFileAttributes (dirStr);
	if ((fAttr & FILE_ATTRIBUTE_DIRECTORY) == 0)
	{
		msicLogErr1( _T ("Bad directory path \"%s\".\r\n"), __FILE__, __LINE__, static_cast< LPCTSTR >( dirStr ) );
		return false;
	}

	WIN32_FIND_DATA findData;
	CBicTString findMask (dirStr);
	findMask += _T ("*.*");
	HANDLE findHandle = FindFirstFile (findMask, &findData);
	if (findHandle == INVALID_HANDLE_VALUE)
	{
		// no file found
		// msicLogErr0 (_T ("no file \"%s\" could NOT BE FOUND.\r\n"), __FILE__, __LINE__, findMask);
		return true;
	}

	do {
		if ((findData.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) == 0)
		{
			CBicFile *file = NewFile (media, findData.cFileName, findData.nFileSizeLow, fileAttributes);
			if (file == NULL)
			{
				return false;
			}

			CBicComponent *comp = NewComponent (dir, compAttributes, compCondition, file);
			if (comp == NULL)
			{
				return false;
			}
			if (!feature->AddComponent (comp))
			{
				return false;
			}

			file->SetComponent (comp);
		}
		else if (_tcscmp (findData.cFileName, _T (".")) != 0 && _tcscmp (findData.cFileName, _T ("..")) != 0)
		{
			if (!NewRecursiveDirectoriesComponentsFiles (media, feature, prefixDir, findData.cFileName, dir, compAttributes, compCondition, fileAttributes))
			{
				return false;
			}
		}

	} while (FindNextFile (findHandle, &findData));

	FindClose (findHandle);

	return true;
}

CBicActionText* CBicDatabase::NewActionText (LPCTSTR action, LPCTSTR condition, LPCTSTR format /* = _T  */)
{
	CBicActionText *o = new CBicActionText (action, condition, format);
	if (o != NULL)
	{
		if (!_actionTexts.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicError* CBicDatabase::NewError (short error, LPCTSTR message /* = _T  */)
{
	CBicError *o = new CBicError (error, message);
	if (o != NULL)
	{
		if (!_errors.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicEventMapping* CBicDatabase::NewEventMapping (CBicControl *control, LPCTSTR event, LPCTSTR attribute)
{
	CBicEventMapping *o = new CBicEventMapping (control, event, attribute);
	if (o != NULL)
	{
		if (!_eventMappings.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicDialog* CBicDatabase::NewDialog (short hCentering, short vCentering, short width, short height, long attributes, LPCTSTR title)
{
	CBicDialog *o = new CBicDialog (hCentering, vCentering, width, height, attributes, title);
	if (o != NULL)
	{
		if (!_dialogs.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicDialog* CBicDatabase::NewDialog (LPCTSTR dialog, short hCentering, short vCentering, short width, short height, long attributes, LPCTSTR title)
{
	CBicDialog *o = new CBicDialog (dialog, hCentering, vCentering, width, height, attributes, title);
	if (o != NULL)
	{
		if (!_dialogs.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicControl* CBicDatabase::NewControl (LPCTSTR type, short x, short y, short width, short height, long attributes, 
									   CBicBaseProperty *property, LPCTSTR text, LPCTSTR help, bool tab)
{
	CBicControl *o = new CBicControl (type, x, y, width, height, attributes, property, text, help, tab);
	if (o != NULL)
	{
		if (!_controls.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicControl* CBicDatabase::NewControl (LPCTSTR control, LPCTSTR type, short x, short y, short width, short height, long attributes, 
									   CBicBaseProperty *property, LPCTSTR text, LPCTSTR help, bool tab)
{
	CBicControl *o = new CBicControl (control, type, x, y, width, height, attributes, property, text, help, tab);
	if (o != NULL)
	{
		if (!_controls.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicControl* CBicDatabase::NewControl (LPCTSTR type, short x, short y, short width, short height, long attributes, 
	CBicBaseProperty *property, CBicBinary *icon, LPCTSTR help, bool tab)
{
	CBicControl *o = new CBicControl (type, x, y, width, height, attributes, property, icon, help, tab);
	if (o != NULL)
	{
		if (!_controls.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicControlEvent* CBicDatabase::NewControlEvent (CBicControl *control, LPCTSTR event, LPCTSTR argument, LPCTSTR condition)
{
	CBicControlEvent *o = new CBicControlEvent (control, event, argument, condition);
	if (o != NULL)
	{
		if (!_controlEvents.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicControlEvent* CBicDatabase::NewControlEvent (CBicControl *control, LPCTSTR event, CBicDialog *argumentDialog, LPCTSTR condition)
{
	CBicControlEvent *o = new CBicControlEvent (control, event, argumentDialog, condition);
	if (o != NULL)
	{
		if (!_controlEvents.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicControlCondition* CBicDatabase::NewControlCondition (CBicControl *control, LPCTSTR action, LPCTSTR condition)
{
	CBicControlCondition *o = new CBicControlCondition (control, action, condition);
	if (o != NULL)
	{
		if (!_controlConditions.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicTextStyle* CBicDatabase::NewTextStyle (LPCTSTR textStyle, LPCTSTR faceName, short size, long color, short styleBits)
{
	CBicTextStyle *o = new CBicTextStyle (textStyle, faceName, size, color, styleBits);
	if (o != NULL)
	{
		if (!_textStyles.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicRadioButton* CBicDatabase::NewRadioButton (CBicRadioButtonGroup *group, LPCTSTR value, short x, short y, short width, short height, LPCTSTR text, LPCTSTR help)
{
	CBicRadioButton *o = new CBicRadioButton (group, value, x, y, width, height, text, help);
	if (o != NULL)
	{
		if (!_radioButtons.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicBinary* CBicDatabase::NewIcon (LPCTSTR property, const unsigned char *binaries, unsigned long size)
{
	CBicProperty *o1 = new CBicProperty (property);
	if (o1 != NULL)
	{
		if (!_properties.Add (o1)) { delete o1; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	CBicBinary *o2 = new CBicBinary (o1, binaries, size);
	if (o2 != NULL)
	{
		if (!_binaries.Add (o2)) { delete o2; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o2;
}

CBicBinary* CBicDatabase::NewIcon (LPCTSTR property, LPCTSTR fileName)
{
	CBicProperty *o1 = new CBicProperty (property);
	if (o1 != NULL)
	{
		if (!_properties.Add (o1)) { delete o1; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	CBicBinary *o2 = new CBicBinary (o1, fileName);
	if (o2 != NULL)
	{
		if (!_binaries.Add (o2)) { delete o2; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o2;
}

CBicBinary* CBicDatabase::NewBinary (HMODULE module, LPCTSTR rscName, LPCTSTR rscType)
{
	HRSRC rscInfo = FindResource (module, rscName, rscType);
	if (rscInfo != NULL)
	{
		HGLOBAL rsc = LoadResource (module, rscInfo);
		if (rsc != NULL)
		{
			LPVOID rscPtr = LockResource (rsc);
			if (rscPtr != NULL)
			{
				DWORD size = SizeofResource (module, rscInfo);
				CBicBinary *o1 = new CBicBinary (static_cast<const unsigned char*> (rscPtr), size);
				if (o1 != NULL)
				{
					if (_binaries.Add (o1)) { return o1; }
					delete o1;
				}
				else
				{
					msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
				}

				UnlockResource (rsc);
			}
			else
			{
				msicLogErr1( _T ("Resource \"%s\" could not be locked.\r\n"), __FILE__, __LINE__, rscName );
			}
		}
		else
		{
			msicLogErr1( _T ("Resource \"%s\" could not be loaded.\r\n"), __FILE__, __LINE__, rscName );
		}
	}
	else
	{
		msicLogErr1( _T ("Resource \"%s\" could not be found.\r\n"), __FILE__, __LINE__, rscName );
	}
	
	return NULL;
}

CBicUIText* CBicDatabase::NewUIText (LPCTSTR key, LPCTSTR text)
{
	CBicUIText *o = new CBicUIText (key, text);
	if (o != NULL)
	{
		if (!_uiTexts.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicValidation* CBicDatabase::NewValidation (LPCTSTR table, LPCTSTR column, LPCTSTR nullable, long minValue, long maxValue, 
											 LPCTSTR keyTable, long keyColumn, LPCTSTR category, LPCTSTR set, 
											 LPCTSTR description)
{
	CBicValidation *o = new CBicValidation (table, column, nullable, minValue, maxValue, keyTable, keyColumn, category, set, description);
	if (o != NULL)
	{
		if (!_validations.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

bool CBicDatabase::SaveAsMsi (LPCTSTR fromDir, LPCTSTR msiDbDir, LPCTSTR filename, bool tempDelete, bool compress)
{
	SortFiles ();
	
	CBicMsiDatabase db;
	if (!db.Create (msiDbDir, filename))
	{
		return false;
	}

#define CALL_2(method, par1, par2);		\
	if (!method (par1, par2))\
	{\
		db.Close ();\
		return false;\
	}

#define SAVE_MSI_TABLE(method);		\
	if (!method (db))\
	{\
		db.Close ();\
		return false;\
	}

#define SAVE_MSI_TABLE1(method, par);		\
	if (!method (db, par))\
	{\
		db.Close ();\
		return false;\
	}

#define SAVE_MSI_TABLE2(method, par1, par2);		\
	if (!method (db, par1, par2))\
	{\
		db.Close ();\
		return false;\
	}

	SAVE_MSI_TABLE (SaveMsiDirectories);
	SAVE_MSI_TABLE (SaveMsiComponents);
	SAVE_MSI_TABLE (SaveMsiFiles);
	SAVE_MSI_TABLE1 (SaveMsiMedia, compress);
	SAVE_MSI_TABLE (SaveMsiRegLocators);
	SAVE_MSI_TABLE (SaveMsiFeatures);
	SAVE_MSI_TABLE (SaveMsiProperties);
	SAVE_MSI_TABLE (SaveMsiInstallExecutiveActions);
	SAVE_MSI_TABLE (SaveMsiInstallUIActions);
	SAVE_MSI_TABLE (SaveMsiActionTexts);
	SAVE_MSI_TABLE (SaveMsiErrors);
	SAVE_MSI_TABLE (SaveMsiEventMappings);
	SAVE_MSI_TABLE (SaveMsiDialogs);
	SAVE_MSI_TABLE (SaveMsiControls);
	SAVE_MSI_TABLE (SaveMsiControlEvents);
	SAVE_MSI_TABLE (SaveMsiControlConditions);
	SAVE_MSI_TABLE (SaveMsiTextStyles);
	SAVE_MSI_TABLE (SaveMsiRadioButtons);
	SAVE_MSI_TABLE (SaveMsiBinary);
	SAVE_MSI_TABLE (SaveMsiUITexts);
	SAVE_MSI_TABLE (SaveMsiValidations);
	SAVE_MSI_TABLE (SaveMsiListBoxes);
	SAVE_MSI_TABLE (SaveMsiCustomActions);
	SAVE_MSI_TABLE (SaveMsiRemoveFiles);
	SAVE_MSI_TABLE (SaveMsiSummaryInformation);
	if (compress)
	{
		SAVE_MSI_TABLE2 (SaveMsiCab, fromDir, tempDelete);
	}
	else
	{
		CALL_2 (CopyFiles, fromDir, msiDbDir);
	}

#undef SAVE_MSI_TABLE
#undef SAVE_MSI_TABLE1
#undef CALL_2

	db.Close ();

	return true;
}

bool CBicDatabase::SaveMsiDirectories (CBicMsiDatabase &db)
{
	if (!db.ExeCmd (_T ("CREATE TABLE `Directory` ("
		"  `Directory` CHAR(72) NOT NULL,"
		"  `Directory_Parent` CHAR(72),"
		"  `DefaultDir` CHAR(255) NOT NULL LOCALIZABLE PRIMARY KEY `Directory`)")))
	{
		return false;
	}

	CBicMsiView view;
	if (!view.Open (db, _T ("INSERT INTO `Directory` ("
		"  `Directory`,"
		"  `Directory_Parent`,"
		"  `DefaultDir`)"
		"  VALUES (?, ?, ?)")))
	{
		return false;
	}

	CBicMsiRecord rec;
	if (!rec.Open (3))
	{
		return false;
	}

	int n = _directories.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (!_directories[i]->Fill (rec) || !view.Execute (rec))
		{
			rec.Close ();
			view.Close ();
			return false;
		}
	}

	rec.Close ();
	view.Close ();

	return true;
}

bool CBicDatabase::SaveMsiComponents (CBicMsiDatabase &db)
{
	if (!db.ExeCmd (_T ("CREATE TABLE `Component` ("
		"  `Component` CHAR(72) NOT NULL,"
		"  `ComponentId` CHAR(38),"
		"  `Directory_` CHAR(72) NOT NULL,"
		"  `Attributes` SHORT NOT NULL,"
		"  `Condition` CHAR(255),"
		"  `KeyPath` CHAR(72) PRIMARY KEY `Component`)")))
	{
		return false;
	}

	CBicMsiView view;
	if (!view.Open (db, _T ("INSERT INTO `Component` ("
		"  `Component`,"
		"  `ComponentId`,"
		"  `Directory_`,"
		"  `Attributes`,"
		"  `Condition`,"
		"  `KeyPath`)"
		"  VALUES (?, ?, ?, ?, ?, ?)")))
	{
		return false;
	}

	CBicMsiRecord rec;
	if (!rec.Open (6))
	{
		return false;
	}

	int n = _components.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (!_components[i]->Fill (rec) || !view.Execute (rec))
		{
			rec.Close ();
			view.Close ();
			return false;
		}
	}

	rec.Close ();
	view.Close ();

	return true;
}

bool CBicDatabase::SaveMsiFiles (CBicMsiDatabase &db)
{
	if (!db.ExeCmd (_T ("CREATE TABLE `File` ("
		"  `File` CHAR(72) NOT NULL,"
		"  `Component_` CHAR(72) NOT NULL,"
		"  `FileName` CHAR(255) NOT NULL LOCALIZABLE,"
		"  `FileSize` LONG NOT NULL,"
		"  `Version` CHAR(72),"
		"  `Language` CHAR(20),"
		"  `Attributes` SHORT,"
		"  `Sequence` SHORT NOT NULL PRIMARY KEY `File`)")))
	{
		return false;
	}

	CBicMsiView view;
	if (!view.Open (db, _T ("INSERT INTO `File` ("
		"  `File`,"
		"  `Component_`,"
		"  `FileName`,"
		"  `FileSize`,"
		"  `Version`,"
		"  `Language`,"
		"  `Attributes`,"
		"  `Sequence`)"
		"  VALUES (?, ?, ?, ?, ?, ?, ?, ?)")))
	{
		return false;
	}

	CBicMsiRecord rec;
	if (!rec.Open (8))
	{
		return false;
	}

	int n = _files.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (!_files[i]->Fill (rec) || !view.Execute (rec))
		{
			rec.Close ();
			view.Close ();
			return false;
		}
	}

	rec.Close ();
	view.Close ();

	return true;
}

bool CBicDatabase::SaveMsiMedia (CBicMsiDatabase &db, bool compress)
{
	if (!db.ExeCmd (_T ("CREATE TABLE `Media` ("
		"  `DiskId` SHORT NOT NULL,"
		"  `LastSequence` SHORT NOT NULL,"
		"  `DiskPrompt` CHAR(64) LOCALIZABLE,"
		"  `Cabinet` CHAR(255),"
		"  `VolumeLabel` CHAR(32),"
		"  `Source` CHAR(72) PRIMARY KEY `DiskId`)")))
	{
		return false;
	}

	CBicMsiView view;
	if (!view.Open (db, _T ("INSERT INTO `Media` ("
		"  `DiskId`,"
		"  `LastSequence`,"
		"  `DiskPrompt`,"
		"  `Cabinet`,"
		"  `VolumeLabel`,"
		"  `Source`)"
		"  VALUES (?, ?, ?, ?, ?, ?)")))
	{
		return false;
	}

	CBicMsiRecord rec;
	if (!rec.Open (6))
	{
		return false;
	}

	int n = _media.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (!_media[i]->Fill (rec, compress) || !view.Execute (rec))
		{
			rec.Close ();
			view.Close ();
			return false;
		}
	}

	rec.Close ();
	view.Close ();

	return true;
}

bool CBicDatabase::SaveMsiRegLocators (CBicMsiDatabase &db)
{
	// AppSearch
	if (!db.ExeCmd (_T ("CREATE TABLE `AppSearch` ("
		"  `Property` CHAR(72) NOT NULL,"
		"  `Signature_` CHAR(72) NOT NULL PRIMARY KEY `Property`, `Signature_`)")))
	{
		return false;
	}

	CBicMsiView view;
	if (!view.Open (db, _T ("INSERT INTO `AppSearch` ("
		"  `Property`,"
		"  `Signature_`)"
		"  VALUES (?, ?)")))
	{
		return false;
	}

	CBicMsiRecord rec;
	if (!rec.Open (2))
	{
		return false;
	}

	int n = _regLocators.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (!_regLocators[i]->FillForAppSearch (rec) || !view.Execute (rec))
		{
			rec.Close ();
			view.Close ();
			return false;
		}
	}

	rec.Close ();
	view.Close ();

	// RegLocator
	if (!db.ExeCmd (_T ("CREATE TABLE `RegLocator` ("
		"  `Signature_` CHAR(72) NOT NULL,"
		"  `Root` SHORT NOT NULL,"
		"  `Key` CHAR(255) NOT NULL,"
		"  `Name` CHAR(255),"
		"  `Type` SHORT PRIMARY KEY `Signature_`)")))
	{
		return false;
	}

	if (!view.Open (db, _T ("INSERT INTO `RegLocator` ("
		"  `Signature_`,"
		"  `Root`,"
		"  `Key`,"
		"  `Name`,"
		"  `Type`)"
		"  VALUES (?, ?, ?, ?, ?)")))
	{
		return false;
	}

	if (!rec.Open (5))
	{
		return false;
	}

	n = _regLocators.GetSize ();
	for (i = 0; i < n; ++i)
	{
		if (!_regLocators[i]->FillForRegLocator (rec) || !view.Execute (rec))
		{
			rec.Close ();
			view.Close ();
			return false;
		}
	}

	rec.Close ();
	view.Close ();

	// Signature
	if (!db.ExeCmd (_T ("CREATE TABLE `Signature` ("
		"  `Signature` CHAR(72) NOT NULL,"
		"  `FileName` CHAR(255) NOT NULL,"
		"  `MinVersion` CHAR(20),"
		"  `MaxVersion` CHAR(20),"
		"  `MinSize` LONG,"
		"  `MaxSize` LONG,"
		"  `MinDate` LONG,"
		"  `MaxDate` LONG,"
		"  `Languages` CHAR(255) PRIMARY KEY `Signature`)")))
	{
		return false;
	}

	return true;
}

bool CBicDatabase::SaveMsiFeatures (CBicMsiDatabase &db)
{
	if (!db.ExeCmd (_T ("CREATE TABLE `Feature` ("
		"  `Feature` CHAR(38) NOT NULL,"
		"  `Feature_Parent` CHAR(38),"
		"  `Title` CHAR(64) LOCALIZABLE,"
		"  `Description` CHAR(255) LOCALIZABLE,"
		"  `Display` SHORT,"
		"  `Level` SHORT NOT NULL,"
		"  `Directory_` CHAR(72),"
		"  `Attributes` SHORT NOT NULL PRIMARY KEY `Feature`)")))
	{
		return false;
	}

	CBicMsiView view;
	if (!view.Open (db, _T ("INSERT INTO `Feature` ("
		"  `Feature`,"
		"  `Feature_Parent`,"
		"  `Title`,"
		"  `Description`,"
		"  `Display`,"
		"  `Level`,"
		"  `Directory_`,"
		"  `Attributes`)"
		"  VALUES (?, ?, ?, ?, ?, ?, ?, ?)")))
	{
		return false;
	}

	CBicMsiRecord rec;
	if (!rec.Open (8))
	{
		return false;
	}

	int n = _features.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (!_features[i]->FillForFeature (rec) || !view.Execute (rec))
		{
			rec.Close ();
			view.Close ();
			return false;
		}
	}

	rec.Close ();
	view.Close ();

	if (!db.ExeCmd (_T ("CREATE TABLE `FeatureComponents` ("
		"  `Feature_` CHAR(38) NOT NULL,"
		"  `Component_` CHAR(72) NOT NULL PRIMARY KEY `Feature_`, `Component_`)")))
	{
		return false;
	}

	if (!view.Open (db, _T ("INSERT INTO `FeatureComponents` ("
		"  `Feature_`,"
		"  `Component_`)"
		"  VALUES (?, ?)")))
	{
		return false;
	}

	if (!rec.Open (2))
	{
		return false;
	}

	n = _features.GetSize ();
	for (i = 0; i < n; ++i)
	{
		CBicFeature *f = _features[i];
		int m = f->GetComponentsSize ();
		for (int j = 0; j < m; ++j)
		{
			if (!f->FillForFeatureComponents (rec, j) || !view.Execute (rec))
			{
				rec.Close ();
				view.Close ();
				return false;
			}
		}
	}

	rec.Close ();
	view.Close ();

	return true;
}

bool CBicDatabase::SaveMsiProperties (CBicMsiDatabase &db)
{
	if (!db.ExeCmd (_T ("CREATE TABLE `Property` ("
		"  `Property` CHAR(72) NOT NULL,"
		"  `Value` CHAR(0) NOT NULL LOCALIZABLE PRIMARY KEY `Property`)")))
	{
		return false;
	}

	CBicMsiView view;
	if (!view.Open (db, _T ("INSERT INTO `Property` ("
		"  `Property`,"
		"  `Value`)"
		"  VALUES (?, ?)")))
	{
		return false;
	}

	CBicMsiRecord rec;
	if (!rec.Open (2))
	{
		return false;
	}

	int n = _properties.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		CBicProperty *p = _properties[i];
		if (p->IsStoring ())
		{
			if (!p->Fill (rec) || !view.Execute (rec))
			{
				rec.Close ();
				view.Close ();
				return false;
			}
		}
	}

	rec.Close ();
	view.Close ();

	return true;
}

bool CBicDatabase::SaveMsiInstallExecutiveActions (CBicMsiDatabase &db)
{
	if (!db.ExeCmd (_T ("CREATE TABLE `InstallExecuteSequence` ("
		"  `Action` CHAR(72) NOT NULL,"
		"  `Condition` CHAR(255),"
		"  `Sequence` SHORT PRIMARY KEY `Action`)")))
	{
		return false;
	}

	CBicMsiView view;
	if (!view.Open (db, _T ("INSERT INTO `InstallExecuteSequence` ("
		"  `Action`,"
		"  `Condition`,"
		"  `Sequence`)"
		"  VALUES (?, ?, ?)")))
	{
		return false;
	}

	CBicMsiRecord rec;
	if (!rec.Open (3))
	{
		return false;
	}

	int n = _installExecutiveActions.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (!_installExecutiveActions[i]->Fill (rec) || !view.Execute (rec))
		{
			rec.Close ();
			view.Close ();
			return false;
		}
	}

	rec.Close ();
	view.Close ();

	return true;
}

bool CBicDatabase::SaveMsiInstallUIActions (CBicMsiDatabase &db)
{
	if (!db.ExeCmd (_T ("CREATE TABLE `InstallUISequence` ("
		"  `Action` CHAR(72) NOT NULL,"
		"  `Condition` CHAR(255),"
		"  `Sequence` SHORT PRIMARY KEY `Action`)")))
	{
		return false;
	}

	CBicMsiView view;
	if (!view.Open (db, _T ("INSERT INTO `InstallUISequence` ("
		"  `Action`,"
		"  `Condition`,"
		"  `Sequence`)"
		"  VALUES (?, ?, ?)")))
	{
		return false;
	}

	CBicMsiRecord rec;
	if (!rec.Open (3))
	{
		return false;
	}

	int n = _installUIActions.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (!_installUIActions[i]->Fill (rec) || !view.Execute (rec))
		{
			rec.Close ();
			view.Close ();
			return false;
		}
	}

	rec.Close ();
	view.Close ();

	return true;
}

bool CBicDatabase::SaveMsiActionTexts (CBicMsiDatabase &db)
{
	if (!db.ExeCmd (_T ("CREATE TABLE `ActionText` ("
		"  `Action` CHAR(72) NOT NULL,"
		"  `Description` CHAR(0) LOCALIZABLE,"
		"  `Template` CHAR(0) LOCALIZABLE PRIMARY KEY `Action`)")))
	{
		return false;
	}

	CBicMsiView view;
	if (!view.Open (db, _T ("INSERT INTO `ActionText` ("
		"  `Action`,"
		"  `Description`,"
		"  `Template`)"
		"  VALUES (?, ?, ?)")))
	{
		return false;
	}

	CBicMsiRecord rec;
	if (!rec.Open (3))
	{
		return false;
	}

	int n = _actionTexts.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (!_actionTexts[i]->Fill (rec) || !view.Execute (rec))
		{
			rec.Close ();
			view.Close ();
			return false;
		}
	}

	rec.Close ();
	view.Close ();

	return true;
}

bool CBicDatabase::SaveMsiErrors (CBicMsiDatabase &db)
{
	if (!db.ExeCmd (_T ("CREATE TABLE `Error` ("
		"  `Error` SHORT NOT NULL,"
		"  `Message` CHAR(0) LOCALIZABLE PRIMARY KEY `Error`)")))
	{
		return false;
	}

	CBicMsiView view;
	if (!view.Open (db, _T ("INSERT INTO `Error` ("
		"  `Error`,"
		"  `Message`)"
		"  VALUES (?, ?)")))
	{
		return false;
	}

	CBicMsiRecord rec;
	if (!rec.Open (2))
	{
		return false;
	}

	int n = _errors.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (!_errors[i]->Fill (rec) || !view.Execute (rec))
		{
			rec.Close ();
			view.Close ();
			return false;
		}
	}

	rec.Close ();
	view.Close ();

	return true;
}

bool CBicDatabase::SaveMsiEventMappings (CBicMsiDatabase &db)
{
	if (!db.ExeCmd (_T ("CREATE TABLE `EventMapping` ("
		"  `Dialog_` CHAR(72) NOT NULL,"
		"  `Control_` CHAR(50) NOT NULL,"
		"  `Event` CHAR(50) NOT NULL,"
		"  `Attribute` CHAR(50) NOT NULL PRIMARY KEY `Dialog_`, `Control_`, `Event`)")))
	{
		return false;
	}

	CBicMsiView view;
	if (!view.Open (db, _T ("INSERT INTO `EventMapping` ("
		"  `Dialog_`,"
		"  `Control_`,"
		"  `Event`,"
		"  `Attribute`)"
		"  VALUES (?, ?, ?, ?)")))
	{
		return false;
	}

	CBicMsiRecord rec;
	if (!rec.Open (4))
	{
		return false;
	}

	int n = _eventMappings.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (!_eventMappings[i]->Fill (rec) || !view.Execute (rec))
		{
			rec.Close ();
			view.Close ();
			return false;
		}
	}

	rec.Close ();
	view.Close ();

	return true;
}

bool CBicDatabase::SaveMsiDialogs (CBicMsiDatabase &db)
{
	if (!db.ExeCmd (_T ("CREATE TABLE `Dialog` ("
		"  `Dialog` CHAR(72) NOT NULL,"
		"  `HCentering` SHORT NOT NULL,"
		"  `VCentering` SHORT NOT NULL,"
		"  `Width` SHORT NOT NULL,"
		"  `Height` SHORT NOT NULL,"
		"  `Attributes` LONG,"
		"  `Title` CHAR(128) LOCALIZABLE,"
		"  `Control_First` CHAR(50) NOT NULL,"
		"  `Control_Default` CHAR(50),"
		"  `Control_Cancel` CHAR(50) PRIMARY KEY `Dialog`)")))
	{
		return false;
	}

	CBicMsiView view;
	if (!view.Open (db, _T ("INSERT INTO `Dialog` ("
		"  `Dialog`,"
		"  `HCentering`,"
		"  `VCentering`,"
		"  `Width`,"
		"  `Height`,"
		"  `Attributes`,"
		"  `Title`,"
		"  `Control_First`,"
		"  `Control_Default`,"
		"  `Control_Cancel`)"
		"  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")))
	{
		return false;
	}

	CBicMsiRecord rec;
	if (!rec.Open (10))
	{
		return false;
	}

	int n = _dialogs.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (!_dialogs[i]->FillForDialog (rec) || !view.Execute (rec))
		{
			rec.Close ();
			view.Close ();
			return false;
		}
	}

	rec.Close ();
	view.Close ();

	return true;
}

bool CBicDatabase::SaveMsiControls (CBicMsiDatabase &db)
{
	if (!db.ExeCmd (_T ("CREATE TABLE `Control` ("
		"  `Dialog_` CHAR(72) NOT NULL,"
		"  `Control` CHAR(50) NOT NULL,"
		"  `Type` CHAR(20) NOT NULL,"
		"  `X` SHORT NOT NULL,"
		"  `Y` SHORT NOT NULL,"
		"  `Width` SHORT NOT NULL,"
		"  `Height` SHORT NOT NULL,"
		"  `Attributes` LONG,"
		"  `Property` CHAR(50),"
		"  `Text` CHAR(0) LOCALIZABLE,"
		"  `Control_Next` CHAR(50),"
		"  `Help` CHAR(50) LOCALIZABLE PRIMARY KEY `Dialog_`, `Control`)")))
	{
		return false;
	}

	CBicMsiView view;
	if (!view.Open (db, _T ("INSERT INTO `Control` ("
		"  `Dialog_`,"
		"  `Control`,"
		"  `Type`,"
		"  `X`,"
		"  `Y`,"
		"  `Width`,"
		"  `Height`,"
		"  `Attributes`,"
		"  `Property`,"
		"  `Text`,"
		"  `Control_Next`,"
		"  `Help`)"
		"  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")))
	{
		return false;
	}

	CBicMsiRecord rec;
	if (!rec.Open (12))
	{
		return false;
	}

	int n = _dialogs.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		CBicDialog *d = _dialogs[i];
		int m = d->GetControlsSize ();
		for (int j = 0; j < m; ++j)
		{
			if (!d->FillForControl (rec, j) || !view.Execute (rec))
			{
				rec.Close ();
				view.Close ();
				return false;
			}
		}
	}

	rec.Close ();
	view.Close ();

	return true;
}

bool CBicDatabase::SaveMsiControlEvents (CBicMsiDatabase &db)
{
	if (!db.ExeCmd (_T ("CREATE TABLE `ControlEvent` ("
		"  `Dialog_` CHAR(72) NOT NULL,"
		"  `Control_` CHAR(50) NOT NULL,"
		"  `Event` CHAR(50) NOT NULL,"
		"  `Argument` CHAR(255) NOT NULL,"
		"  `Condition` CHAR(255),"
		"  `Ordering` SHORT PRIMARY KEY `Dialog_`, `Control_`, `Event`, `Argument`, `Condition`)")))
	{
		return false;
	}

	CBicMsiView view;
	if (!view.Open (db, _T ("INSERT INTO `ControlEvent` ("
		"  `Dialog_`,"
		"  `Control_`,"
		"  `Event`,"
		"  `Argument`,"
		"  `Condition`,"
		"  `Ordering`)"
		"  VALUES (?, ?, ?, ?, ?, ?)")))
	{
		return false;
	}

	CBicMsiRecord rec;
	if (!rec.Open (6))
	{
		return false;
	}

	int n = _controlEvents.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (!_controlEvents[i]->Fill (rec) || !view.Execute (rec))
		{
			rec.Close ();
			view.Close ();
			return false;
		}
	}

	rec.Close ();
	view.Close ();

	return true;
}

bool CBicDatabase::SaveMsiControlConditions (CBicMsiDatabase &db)
{
	if (!db.ExeCmd (_T ("CREATE TABLE `ControlCondition` ("
		"  `Dialog_` CHAR(72) NOT NULL,"
		"  `Control_` CHAR(50) NOT NULL,"
		"  `Action` CHAR(50) NOT NULL,"
		"  `Condition` CHAR(255) NOT NULL PRIMARY KEY `Dialog_`, `Control_`, `Action`, `Condition`)")))
	{
		return false;
	}

	CBicMsiView view;
	if (!view.Open (db, _T ("INSERT INTO `ControlCondition` ("
		"  `Dialog_`,"
		"  `Control_`,"
		"  `Action`,"
		"  `Condition`)"
		"  VALUES (?, ?, ?, ?)")))
	{
		return false;
	}

	CBicMsiRecord rec;
	if (!rec.Open (4))
	{
		return false;
	}

	int n = _controlConditions.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (!_controlConditions[i]->Fill (rec) || !view.Execute (rec))
		{
			rec.Close ();
			view.Close ();
			return false;
		}
	}

	rec.Close ();
	view.Close ();

	return true;
}

bool CBicDatabase::SaveMsiTextStyles (CBicMsiDatabase &db)
{
	if (!db.ExeCmd (_T ("CREATE TABLE `TextStyle` ("
		"  `TextStyle` CHAR(72) NOT NULL,"
		"  `FaceName` CHAR(32) NOT NULL,"
		"  `Size` SHORT NOT NULL,"
		"  `Color` LONG,"
		"  `StyleBits` SHORT PRIMARY KEY `TextStyle`)")))
	{
		return false;
	}

	CBicMsiView view;
	if (!view.Open (db, _T ("INSERT INTO `TextStyle` ("
		"  `TextStyle`,"
		"  `FaceName`,"
		"  `Size`,"
		"  `Color`,"
		"  `StyleBits`)"
		"  VALUES (?, ?, ?, ?, ?)")))
	{
		return false;
	}

	CBicMsiRecord rec;
	if (!rec.Open (5))
	{
		return false;
	}

	int n = _textStyles.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (!_textStyles[i]->Fill (rec) || !view.Execute (rec))
		{
			rec.Close ();
			view.Close ();
			return false;
		}
	}

	rec.Close ();
	view.Close ();

	return true;
}

bool CBicDatabase::SaveMsiRadioButtons (CBicMsiDatabase &db)
{
	if (!db.ExeCmd (_T ("CREATE TABLE `RadioButton` ("
		"  `Property` CHAR(72) NOT NULL,"
		"  `Order` SHORT NOT NULL,"
		"  `Value` CHAR(64) NOT NULL,"
		"  `X` SHORT NOT NULL,"
		"  `Y` SHORT NOT NULL,"
		"  `Width` SHORT NOT NULL,"
		"  `Height` SHORT NOT NULL,"
		"  `Text` CHAR(64) LOCALIZABLE,"
		"  `Help` CHAR(50) LOCALIZABLE PRIMARY KEY `Property`, `Order`)")))
	{
		return false;
	}

	CBicMsiView view;
	if (!view.Open (db, _T ("INSERT INTO `RadioButton` ("
		"  `Property`,"
		"  `Order`,"
		"  `Value`,"
		"  `X`,"
		"  `Y`,"
		"  `Width`,"
		"  `Height`,"
		"  `Text`,"
		"  `Help`)"
		"  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)")))
	{
		return false;
	}

	CBicMsiRecord rec;
	if (!rec.Open (9))
	{
		return false;
	}

	int n = _radioButtons.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (!_radioButtons[i]->Fill (rec) || !view.Execute (rec))
		{
			rec.Close ();
			view.Close ();
			return false;
		}
	}

	rec.Close ();
	view.Close ();

	return true;
}

bool CBicDatabase::SaveMsiBinary (CBicMsiDatabase &db)
{
	// Save Icons, Binaries

	if (!db.ExeCmd (_T ("CREATE TABLE `Binary` ("
		"  `Name` CHAR(72) NOT NULL,"
		"  `Data` OBJECT NOT NULL PRIMARY KEY `Name`)")))
	{
		return false;
	}

	CBicMsiView view;
	if (!view.Open (db, _T ("INSERT INTO `Binary` ("
		"  `Name`,"
		"  `Data`)"
		"  VALUES (?, ?)")))
	{
		return false;
	}

	CBicMsiRecord rec;
	if (!rec.Open (2))
	{
		return false;
	}

	int n = _binaries.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (!_binaries[i]->Fill (rec) || !view.Execute (rec))
		{
			rec.Close ();
			view.Close ();
			return false;
		}
	}

	rec.Close ();
	view.Close ();

	return true;
}

bool CBicDatabase::SaveMsiUITexts (CBicMsiDatabase &db)
{
	if (!db.ExeCmd (_T ("CREATE TABLE `UIText` ("
		"  `Key` CHAR(72) NOT NULL,"
		"  `Text` CHAR(255) LOCALIZABLE PRIMARY KEY `Key`)")))
	{
		return false;
	}

	CBicMsiView view;
	if (!view.Open (db, _T ("INSERT INTO `UIText` ("
		"  `Key`,"
		"  `Text`)"
		"  VALUES (?, ?)")))
	{
		return false;
	}

	CBicMsiRecord rec;
	if (!rec.Open (2))
	{
		return false;
	}

	int n = _uiTexts.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (!_uiTexts[i]->Fill (rec) || !view.Execute (rec))
		{
			rec.Close ();
			view.Close ();
			return false;
		}
	}

	rec.Close ();
	view.Close ();

	return true;
}

bool CBicDatabase::SaveMsiValidations (CBicMsiDatabase &db)
{
	if (!db.ExeCmd (_T ("CREATE TABLE `_Validation` ("
		"  `Table` CHAR(32) NOT NULL,"
		"  `Column` CHAR(32) NOT NULL,"
		"  `Nullable` CHAR(4) NOT NULL,"
		"  `MinValue` LONG,"
		"  `MaxValue` LONG,"
		"  `KeyTable` CHAR(255),"
		"  `KeyColumn` SHORT,"
		"  `Category` CHAR(32),"
		"  `Set` CHAR(255),"
		"  `Description` CHAR(255) PRIMARY KEY `Table`, `Column`)")))
	{
		return false;
	}

	CBicMsiView view;
	if (!view.Open (db, _T ("INSERT INTO `_Validation` ("
		"  `Table`,"
		"  `Column`,"
		"  `Nullable`,"
		"  `MinValue`,"
		"  `MaxValue`,"
		"  `KeyTable`,"
		"  `KeyColumn`,"
		"  `Category`,"
		"  `Set`,"
		"  `Description`)"
		"  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")))
	{
		return false;
	}

	CBicMsiRecord rec;
	if (!rec.Open (10))
	{
		return false;
	}

	int n = _validations.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (!_validations[i]->Fill (rec) || !view.Execute (rec))
		{
			rec.Close ();
			view.Close ();
			return false;
		}
	}

	rec.Close ();
	view.Close ();

	return true;
}

bool CBicDatabase::SaveMsiListBoxes (CBicMsiDatabase &db)
{
	if (!db.ExeCmd (_T ("CREATE TABLE `ListBox` ("
		"  `Property` CHAR(72) NOT NULL,"
		"  `Order` SHORT NOT NULL,"
		"  `Value` CHAR(64) NOT NULL,"
		"  `Text` CHAR(64) LOCALIZABLE PRIMARY KEY `Property`, `Order`)")))
	{
		return false;
	}

	return true;
}

bool CBicDatabase::SaveMsiSummaryInformation (CBicMsiDatabase &db)
{
	unsigned int n = _summaryProperties.GetSize ();

	CBicMsiSummaryInformation info;
	if (!info.Open (db, n))
	{
		return false;
	}

	for (unsigned int i = 0; i < n; ++i)
	{
		if (!_summaryProperties[i]->SetProperty (info))
		{
			return false;
		}
	}

	info.Close ();
	
	return true;
}

bool CBicDatabase::CopyFiles (LPCTSTR fromDir, LPCTSTR msiDbDir)
{
	CBicTString dstPath;

	// Directory
	int n = _directories.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		CBicDirectory *d = _directories[i];
		d->GetSourceDirectory (dstPath);
		dstPath.AddPrefix (msiDbDir);

		if (_taccess (dstPath, 06) == 0 || CreateDirectory (dstPath, NULL))
		{
			continue;
		}

		DWORD err = GetLastError ();
		return false;
	}

	// Files
	CBicTString srcPath;
	n = _files.GetSize ();
	for (i = 0; i < n; ++i)
	{
		CBicFile *f = _files[i];
		f->GetComponent ()->GetDirectory ()->GetSourceDirectory (srcPath);
		dstPath = srcPath;
		
		srcPath.AddPrefix (fromDir);
		srcPath += f->GetSrcFileName ();
		
		dstPath.AddPrefix (msiDbDir);
		dstPath += f->GetDstFileName ();

		// Copy file
		if (!CopyFile (srcPath, dstPath, FALSE))
		{
			DWORD err = GetLastError ();
			return false;
		}
	}

	return true;
}

bool CBicDatabase::SaveMsiCab (CBicMsiDatabase &db, LPCTSTR fromDir, bool tempDelete)
{
	// Create CAB file
	CBicTempFile cabFile (_T ("cab"), tempDelete);
	if (!cabFile.Initialize (_T (".")))
	{
		return false;
	}

	// Create INF file
	CBicTempFile infFile (_T ("inf"), tempDelete);
	if (!infFile.Initialize (_T (".")))
	{
		return false;
	}

	// Create RPT file
	CBicTempFile rptFile (_T ("rpt"), tempDelete);
	if (!rptFile.Initialize (_T (".")))
	{
		return false;
	}

	// Create DDF content
	CBicTString ddfFileContents (1024);
	ddfFileContents = _T (".Set CabinetNameTemplate=");
	ddfFileContents += cabFile.GetFileName ();
	ddfFileContents += _T ("*\n"
		".Set CabinetName1=");
	ddfFileContents += cabFile.GetFileName ();
	ddfFileContents += _T ("\n"
		".Set ReservePerCabinetSize=8\n"
		".Set MaxDiskSize=0\n"
		".Set CompressionType=MSZIP\n"
		".Set InfFileLineFormat=(*disk#*) *file#*: *file* = *Size*\n"
		".Set InfFileName=");
	ddfFileContents += infFile.GetFileName ();
	ddfFileContents += _T ("\n"
		".Set RptFileName=");
	ddfFileContents += rptFile.GetFileName ();
	ddfFileContents += _T ("\n"
		".Set InfHeader=\n"
		".Set InfFooter=\n"
		".Set DiskDirectoryTemplate=.\n"
		".Set Compress=ON\n"
		".Set Cabinet=ON\n");

	// Files
	CBicTString path;
	int n = _files.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		CBicFile *f = _files[i];
		f->GetComponent ()->GetDirectory ()->GetSourceDirectory (path);
		ddfFileContents += _T ("\"");
		ddfFileContents += fromDir;
		ddfFileContents += path;
		ddfFileContents += f->GetSrcFileName ();
		ddfFileContents += _T ("\" ");
		ddfFileContents += f->GetFile ();
		ddfFileContents += _T ("\n");
	}

	// Create DDF file
	CBicTempFile ddfFile (_T ("ddf"), tempDelete);
	if (!ddfFile.Open (_T (".")))
	{
		return false;
	}

	// write DDF content
	if (!ddfFile.Write (ddfFileContents))
	{
		return false;
	}

	ddfFile.Close ();

	// Fill cmd line for makecab execution
	CBicTString cmdLine;
	cmdLine.Format (_T ("makecab /f %s"), ddfFile.GetFileName ());

	STARTUPINFO stInfo;
	memset (&stInfo, 0, sizeof (stInfo));
	stInfo.cb = sizeof(stInfo);
	
	PROCESS_INFORMATION prInfo;
	memset (&prInfo, 0, sizeof (prInfo));

	if (::CreateProcess (NULL, cmdLine.GetBuffer (), NULL, NULL,
		FALSE, CREATE_DEFAULT_ERROR_MODE | NORMAL_PRIORITY_CLASS,
		NULL, NULL, &stInfo, &prInfo) == 0)
	{
		msicLogErr0 (_T ("CreateProcess failed.\r\n"), __FILE__, __LINE__);
		return false;
	}

	DWORD dwExitCode;
	do {
		::Sleep(500);

		if (::GetExitCodeProcess(prInfo.hProcess, &dwExitCode) == 0)
		{
			msicLogErr0 (_T ("CreateProcess failed.\r\n"), __FILE__, __LINE__);
			::CloseHandle (prInfo.hThread);
			::CloseHandle (prInfo.hProcess);
			ddfFile.Delete ();
			infFile.Delete ();
			rptFile.Delete ();
			cabFile.Delete ();
			return false;
		}
	} while (dwExitCode == STILL_ACTIVE);

	if (dwExitCode != 0)
	{
		msicLogErr0 (_T ("Makecab failed.\r\n"), __FILE__, __LINE__);
		::CloseHandle (prInfo.hThread);
		::CloseHandle (prInfo.hProcess);
		ddfFile.Delete ();
		infFile.Delete ();
		rptFile.Delete ();
		cabFile.Delete ();
		return false;
	}

	::CloseHandle (prInfo.hThread);
	::CloseHandle (prInfo.hProcess);

	CBicMsiView view;
	if (!view.Open (db, _T ("SELECT `Name`,`Data` FROM `_Streams`")))
	{
		return false;
	}

	CBicMsiRecord rec;
	if (!rec.Open (2))
	{
		return false;
	}

	if (!rec.Fill (1, _T ("archiv.cab")) ||
		!rec.FillStream (2, cabFile.GetFileName ()) ||
		!view.Modify (MSIMODIFY_ASSIGN, rec))
	{
		return false;
	}

	rec.Close ();
	view.Close ();

	ddfFile.Delete ();
	infFile.Delete ();
	rptFile.Delete ();
	cabFile.Delete ();

	return true;
}

CBicCustomAction* CBicDatabase::NewCustomAction (short type, CBicBaseProperty *source, LPCTSTR target)
{
	CBicCustomAction *o = new CBicCustomAction (type, source, target);
	if (o != NULL)
	{
		if (!_customActions.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

CBicCustomAction* CBicDatabase::NewCustomAction (short type, CBicBinary *source, LPCTSTR target)
{
	CBicCustomAction *o = new CBicCustomAction (type, source, target);
	if (o != NULL)
	{
		if (!_customActions.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}

bool CBicDatabase::SaveMsiCustomActions (CBicMsiDatabase &db)
{
	if (!db.ExeCmd (_T ("CREATE TABLE `CustomAction` ("
		"  `Action` CHAR(72) NOT NULL,"
		"  `Type` SHORT NOT NULL,"
		"  `Source` CHAR(72),"
		"  `Target` CHAR(255) PRIMARY KEY `Action`)")))
	{
		return false;
	}

	CBicMsiView view;
	if (!view.Open (db, _T ("INSERT INTO `CustomAction` ("
		"  `Action`,"
		"  `Type`,"
		"  `Source`,"
		"  `Target`)"
		"  VALUES (?, ?, ?, ?)")))
	{
		return false;
	}

	CBicMsiRecord rec;
	if (!rec.Open (4))
	{
		return false;
	}

	int n = _customActions.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (!_customActions[i]->Fill (rec) || !view.Execute (rec))
		{
			rec.Close ();
			view.Close ();
			return false;
		}
	}

	rec.Close ();
	view.Close ();

	return true;
}

bool CBicDatabase::SaveMsiRemoveFiles (CBicMsiDatabase &db)
{
	if (!db.ExeCmd (_T ("CREATE TABLE `RemoveFile` ("
		"  `FileKey` CHAR(72) NOT NULL,"
		"  `Component_` CHAR(72) NOT NULL,"
		"  `FileName` CHAR(255) LOCALIZABLE,"
		"  `DirProperty` CHAR(72) NOT NULL,"
		"  `InstallMode` SHORT NOT NULL PRIMARY KEY `FileKey`)")))
	{
		return false;
	}

	CBicMsiView view;
	if (!view.Open (db, _T ("INSERT INTO `RemoveFile` ("
		"  `FileKey`,"
		"  `Component_`,"
		"  `FileName`,"
		"  `DirProperty`,"
		"  `InstallMode`)"
		"  VALUES (?, ?, ?, ?, ?)")))
	{
		return false;
	}

	CBicMsiRecord rec;
	if (!rec.Open (5))
	{
		return false;
	}

	int n = _removeFiles.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		if (!_removeFiles[i]->Fill (rec) || !view.Execute (rec))
		{
			rec.Close ();
			view.Close ();
			return false;
		}
	}

	rec.Close ();
	view.Close ();

	return true;
}

CBicRemoveFile* CBicDatabase::NewRemoveFile (LPCTSTR file, CBicComponent *component, LPCTSTR fileName, short installMode)
{
	CBicRemoveFile *o = new CBicRemoveFile (file, component, fileName, installMode);
	if (o != NULL)
	{
		if (!_removeFiles.Add (o)) { delete o; return NULL; }
	}
	else
	{
		msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
	}

	return o;
}
