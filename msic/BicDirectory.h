
//! Class of one record in Directory table.
class CBicDirectory : public CBicBaseProperty
{
protected:
	static unsigned int _counter;

	CBicDirectory *_directoryParent;
	CBicTString _defaultDir;

public:
	CBicDirectory (LPCTSTR directory, LPCTSTR defaultDir, CBicDirectory *parent = NULL);
	CBicDirectory (LPCTSTR defaultDir, CBicDirectory *parent = NULL);

	void ChangeDirectory ();

	void GetSourceDirectory (CBicTString &dir) const;
	void GetDestinationDirectory (CBicTString &dir) const;
	void SetDirectory (LPCTSTR dir);
	LPCTSTR GetDirectory () const;
	LPCTSTR GetDefaultDir () const;
	CBicDirectory *GetDirectoryParent () const;
	
	bool Fill (CBicMsiRecord &rec) const;

	static inline void ResetCounter ()
	{
		_counter = 0;
	}
	
	static inline unsigned int GetCounter ()
	{
		return _counter;
	}

	static inline void SetCounter (unsigned int c)
	{
		_counter = c;
	}
};