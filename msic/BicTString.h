
//! Dynamic string.
class CBicTString
{
protected:
	CBicArray<TCHAR, TCHAR> _string;

public:
	CBicTString(int initLength = 30, int step = 10) : _string (initLength, step)
	{
		/*
		if (!_string.Add (_T ('\0')))
		{
			msicLogErr0 (_T ("CBicTString::CBicTString failed.\r\n"), __FILE__, __LINE__);
		}
		*/
	}

	CBicTString(const CBicTString &str) : _string (str._string.GetSize (), 10)
	{
		if (!_string.Copy (str._string))
		{
			msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
		}
	}

	CBicTString(LPCTSTR str) : _string (30, 10)
	{
		if (str != NULL)
		{
			if (!_string.Copy (str, _tcslen (str) + 1))
			{
				msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
			}
		}
	}

	CBicTString(LPCTSTR str, int len) : _string (30, 10)
	{
		if (str != NULL)
		{
			if (len < 0)
			{
				if (!_string.Copy (str, _tcslen (str) + 1))
				{
					msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
				}
			}
			else
			{
				if (!_string.Copy (str, len))
				{
					msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
				}
				_string.Add (_T ('\0'));
			}
		}
	}

	inline void operator = (LPCTSTR src)
	{
		if (!_string.Copy (src, _tcslen (src) + 1))
		{
			msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
		}
	}

	inline void operator = (const CBicTString &src)
	{
		if (!_string.Copy (src._string))
		{
			msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
		}
	}

	inline void operator += (LPCTSTR src)
	{
		_string.RemoveLast ();
		if (!_string.Append (src, _tcslen (src) + 1))
		{
			msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
		}
	}

	inline void operator += (const CBicTString &src)
	{
		_string.RemoveLast ();
		if (!_string.Append (src._string))
		{
			msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
		}
	}

	inline operator LPCTSTR () const
	{
		return _string.GetData ();
	}

	inline bool operator == (const CBicTString &op2) const
	{
		return _tcscmp (operator LPCTSTR (), op2.operator LPCTSTR ()) == 0;
	}

	inline bool operator == (LPCTSTR op2) const
	{
		return _tcscmp (operator LPCTSTR (), op2) == 0;
	}

	inline bool operator != (const CBicTString &op2) const
	{
		return _tcscmp (operator LPCTSTR (), op2.operator LPCTSTR ()) != 0;
	}

	inline bool operator != (LPCTSTR op2) const
	{
		return _tcscmp (operator LPCTSTR (), op2) != 0;
	}

	void Format (LPCTSTR format, ...)
	{
		va_list pars;
		va_start (pars, format);
    int n;
		while( ( n = _vsntprintf (_string.GetBuffer (), _string.GetLength (), format, pars ) ) < 0 || n >= _string.GetLength() )
		{
			if (!_string.Enlarge ())
			{
				msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
			}
		}
		va_end (pars);
		_string.Shrink (_tcslen (_string.GetData ()) + 1);
	}

	TCHAR *GetBuffer ()
	{
		return _string.GetBuffer ();
	}

	TCHAR *GetByteBuffer (unsigned int size)
	{
		return _string.GetBuffer ((size + sizeof (TCHAR) - 1) / sizeof (TCHAR));
	}

	TCHAR *GetBuffer (unsigned int size)
	{
		return _string.GetBuffer (size);
	}

	inline unsigned int GetByteSize () const
	{
		return _string.GetByteSize ();
	}

	inline unsigned int GetSize () const
	{
		return _string.GetSize ();
	}

	inline void SetAt (unsigned int i, TCHAR c)
	{
		_string.SetAt (i, c);
	}
	
	inline void AddPrefix (LPCTSTR prefix)
	{
		if (!_string.AddPrefix (prefix, _tcslen (prefix)))
		{
			msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
		}
	}
	
	inline void AddPrefix (const CBicTString &prefix)
	{
		if (!_string.AddPrefix (prefix._string.GetData (), prefix._string.GetSize () - 1))
		{
			msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
		}
	}

  inline void FindReplace( TCHAR find, TCHAR replace )
  {
    int n = _string.GetSize();
    for( int i = 0; i < n; ++i )
    {
      if( _string[ i ] == find )
      {
        _string.SetAt( i, replace );
      }
    }
  }
};