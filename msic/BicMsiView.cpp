
#include "msic.h"


CBicMsiView::CBicMsiView ()
: _view (NULL)
{
}

CBicMsiView::~CBicMsiView ()
{
	Close ();
}

bool CBicMsiView::Open (CBicMsiDatabase &db, LPCTSTR query)
{
	if (_view != NULL)
	{
		msicLogErr0 (_T ("MSI view already opened.\r\n"), __FILE__, __LINE__);
		return false;
	}

	UINT err = MsiDatabaseOpenView (db.GetMsiHandle (), query, &_view);
	if (err != ERROR_SUCCESS)
	{
		msicLogErr1 (_T ("MsiDatabaseOpenView failed ["MSI_ERR_FMT"].\r\n"), __FILE__, __LINE__, err);
		return false;
	}

	return true;
}

void CBicMsiView::Close ()
{
	if (_view != NULL)
	{
		MsiViewClose (_view);
		MsiCloseHandle (_view);
		_view = NULL;
	}
}

bool CBicMsiView::Execute ()
{
	UINT err = MsiViewExecute (_view, NULL);
	if (err != ERROR_SUCCESS)
	{
		msicLogErr1 (_T ("MsiViewExecute failed ["MSI_ERR_FMT"].\r\n"), __FILE__, __LINE__, err);
		return false;
	}

	return true;
}

bool CBicMsiView::Execute (CBicMsiRecord &rec)
{
	UINT err = MsiViewExecute (_view, rec.GetMsiHandle ());
	if (err != ERROR_SUCCESS)
	{
		msicLogErr1 (_T ("MsiViewExecute failed ["MSI_ERR_FMT"].\r\n"), __FILE__, __LINE__, err);
		return false;
	}

	return true;
}

bool CBicMsiView::Modify (MSIMODIFY modifyMode, CBicMsiRecord &rec)
{
	UINT err = MsiViewModify (_view, modifyMode, rec.GetMsiHandle ());
	if (err != ERROR_SUCCESS)
	{
		msicLogErr1 (_T ("MsiViewModify failed ["MSI_ERR_FMT"].\r\n"), __FILE__, __LINE__, err);
		return false;
	}

	return true;
}
