
//! Read content of a file.
class CBicFileReader
{
protected:
	HANDLE _file;
  CBicTString _fileName;

public:
	CBicFileReader ();
	~CBicFileReader ();

	bool Open (LPCTSTR fileName);
	bool Read (unsigned char *buf, unsigned int &size);
	bool Read (CBicTString &str);
	bool Read (CBicArray<unsigned char, unsigned char> &buf);
	void Close ();
};