
#include "msic.h"


unsigned int CBicInstallExecutiveAction::_counter = 0;


CBicInstallExecutiveAction::CBicInstallExecutiveAction (LPCTSTR action, LPCTSTR condition, short sequence)
: _sequence (sequence), _action (action), _condition (condition), _customAction (NULL)
{
}

CBicInstallExecutiveAction::CBicInstallExecutiveAction (LPCTSTR action, LPCTSTR condition)
: _sequence (++_counter * 100), _action (action), _condition (condition), _customAction (NULL)
{
}

CBicInstallExecutiveAction::CBicInstallExecutiveAction (CBicCustomAction *customAction, LPCTSTR condition)
: _sequence (++_counter * 100), _action (), _condition (condition), _customAction (customAction)
{
}

bool CBicInstallExecutiveAction::Fill (CBicMsiRecord &rec) const
{
	if (!rec.Fill (1, _customAction == NULL ? static_cast<LPCTSTR> (_action) : _customAction->GetAction ()) ||
		!rec.Fill (2, _condition) ||
		!rec.Fill (3, _sequence))
	{
		return false;
	}

	return true;
}