
#include "msic.h"


unsigned int CBicControl::_counter = 0;


CBicControl::CBicControl (LPCTSTR type, short x, short y, short width, short height, long attributes, 
						  CBicBaseProperty *property, LPCTSTR text, LPCTSTR help, bool tab)
: _dialog (NULL), _x (x), _y (y), _width (width), _height (height), _attributes (attributes), _tab (tab), 
_icon (NULL), _eventOrdering (0), _property (property), _text (text), _propertyText (_T ("")),
_type (type), _help (help)
{
	_control.Format (_T ("Control_%08u"), ++_counter);
}

CBicControl::CBicControl (LPCTSTR type, short x, short y, short width, short height, long attributes, 
						  LPCTSTR property, LPCTSTR text, LPCTSTR help, bool tab)
: _dialog (NULL), _x (x), _y (y), _width (width), _height (height), _attributes (attributes), _tab (tab), 
_icon (NULL), _eventOrdering (0), _property (NULL), _text (text), _propertyText (property),
_type (type), _help (help)
{
	_control.Format (_T ("Control_%08u"), ++_counter);
}

CBicControl::CBicControl (LPCTSTR control, LPCTSTR type, short x, short y, short width, short height, long attributes, 
						  CBicBaseProperty *property, LPCTSTR text, LPCTSTR help, bool tab)
: _dialog (NULL), _x (x), _y (y), _width (width), _height (height), _attributes (attributes), _tab (tab), 
_icon (NULL), _eventOrdering (0), _property (property), _text (text), _control (control), _propertyText (_T ("")),
_type (type), _help (help)
{
}

CBicControl::CBicControl (LPCTSTR type, short x, short y, short width, short height, long attributes, 
						  CBicBaseProperty *property, CBicBinary *icon, LPCTSTR help, bool tab)
: _dialog (NULL), _x (x), _y (y), _width (width), _height (height), _attributes (attributes), _tab (tab), 
_icon (icon), _eventOrdering (0), _property (property), _text (_T ("")), _propertyText (_T ("")),
_type (type), _help (help)
{
	_control.Format (_T ("Control_%08u"), ++_counter);
}

LPCTSTR CBicControl::GetControl () const
{
	return _control;
}

LPCTSTR CBicControl::GetDialog () const
{
	return _dialog != NULL ? _dialog->GetDialog () : NULL;
}

bool CBicControl::SetDialog (CBicDialog *dialog)
{
	if (_dialog == NULL)
	{
		_dialog = dialog;
		return true;
	}

	return false;
}

short CBicControl::NewEventOrdering ()
{
	return ++_eventOrdering;
}

bool CBicControl::Fill (CBicMsiRecord &rec, const CBicControl *next) const
{
	if (!rec.Fill (1, _dialog->GetDialog ()) ||
		!rec.Fill (2, _control) ||
		!rec.Fill (3, _type) ||
		!rec.Fill (4, _x) ||
		!rec.Fill (5, _y) ||
		!rec.Fill (6, _width) ||
		!rec.Fill (7, _height) ||
		!rec.Fill (8, _attributes) ||
		!rec.Fill (9, _property != NULL ? _property->GetProperty () : _propertyText) ||
		!rec.Fill (11, next != NULL ? next->GetControl () : static_cast<LPCTSTR> (NULL)) ||
		!rec.Fill (12, _help))
	{
		return false;
	}

	if (_icon == NULL)
	{
		if (!rec.Fill (10, _text))
		{
			return false;
		}
	}
	else
	{
		CBicTString buf;
		buf.Format (_T ("[%s]"), _icon->GetProperty ());
		if (!rec.Fill (10, buf))
		{
			return false;
		}
	}

	return true;
}

bool CBicControl::IsTab () const
{
	return _tab;
}