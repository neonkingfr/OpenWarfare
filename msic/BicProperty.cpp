
#include "msic.h"


CBicProperty::CBicProperty (LPCTSTR property, bool storing)
: CBicBaseProperty (property), _dialog (NULL), _storing (storing), _value (_T (""))
{
}

CBicProperty::CBicProperty (LPCTSTR property, LPCTSTR value, bool storing)
: CBicBaseProperty (property), _dialog (NULL), _storing (storing), _value (value)
{
}

CBicProperty::CBicProperty (LPCTSTR property, CBicDialog *dialog, bool storing)
: CBicBaseProperty (property), _dialog (dialog), _storing (storing), _value (_T (""))
{
}

CBicProperty::~CBicProperty ()
{
}

LPCTSTR CBicProperty::GetValue () const
{
	return _value;
}

bool CBicProperty::IsStoring () const
{
	return _storing;
}

bool CBicProperty::GenGuid ()
{
	return ::GenGuid (_value);
}

void CBicProperty::SetValue (LPCTSTR value)
{
	_value = value;
}

bool CBicProperty::Fill (CBicMsiRecord &rec) const
{
	if (!rec.Fill (1, _property) ||
		!rec.Fill (2, _dialog == NULL ? _value : _dialog->GetDialog ()))
	{
		return false;
	}

	return true;
}