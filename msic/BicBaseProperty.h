
//! Base class capsulizing property name (derived classes are CBicProperty and CBicDirectory).
class CBicBaseProperty
{
protected:
	CBicTString _property;

public:
	CBicBaseProperty ();
	CBicBaseProperty (LPCTSTR property);
	virtual ~CBicBaseProperty ();
	
	LPCTSTR GetProperty () const;
};