
#include "msic.h"


unsigned int CBicMedia::_counter = 0;


CBicMedia::CBicMedia ()
: _diskId (++_counter)
{
}

bool CBicMedia::AddFile (CBicFile *file)
{
	return _files.Add (file);
}

bool CBicMedia::Fill (CBicMsiRecord &rec, bool compress) const
{
	int lastSequence = 0, n = _files.GetSize ();
	for (int i = 0; i < n; ++i)
	{
		short seq = _files[i]->GetSequence ();
		if (lastSequence < seq)
		{
			lastSequence = seq;
		}
	}
	
	if (!rec.Fill (1, _diskId) ||
		!rec.Fill (2, lastSequence) ||
		!rec.Fill (3, static_cast<LPCTSTR> (NULL)) ||
		!rec.Fill (4, compress ? _T ("#archiv.cab") : static_cast<LPCTSTR> (NULL)) ||
		!rec.Fill (5, static_cast<LPCTSTR> (NULL)) ||
		!rec.Fill (6, static_cast<LPCTSTR> (NULL)))
	{
		return false;
	}

	return true;
}