
//! Class of one record in TargetImages table.
class CBicTargetImage
{
protected:
	static unsigned int _counter;

	CBicTString _target;
	CBicTString _msiPath;
	CBicUpgradedImage *_upgraded;
	short _order;
	short _ignoreMissingSrcFiles;
	
public:
	CBicTargetImage (LPCTSTR msiPath, CBicUpgradedImage *upgraded, short ignoreMissingSrcFiles)
		: _msiPath (msiPath), _upgraded (upgraded), _order (++_counter), 
		_ignoreMissingSrcFiles (ignoreMissingSrcFiles)
	{
		_target.Format (_T ("Tar_%04u"), _order);
	}

	bool Fill (CBicMsiRecord &rec) const
	{
		if (!rec.Fill (1, _target) ||
			!rec.Fill (2, _msiPath) ||
			!rec.Fill (3, static_cast<LPCTSTR> (NULL)) ||
			!rec.Fill (4, _upgraded->GetUpgraded ()) ||
			!rec.Fill (5, _order) ||
			!rec.Fill (6, static_cast<LPCTSTR> (NULL)) ||
			!rec.Fill (7, _ignoreMissingSrcFiles))
		{
			return false;
		}

		return true;
	}

	static inline void ResetCounter ()
	{
		_counter = 0;
	}
};