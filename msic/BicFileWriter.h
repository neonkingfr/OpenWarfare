
//! Write data to the file.
class CBicDataWriter
{
protected:
	CBicTString _fileName;
	HANDLE _file;
	bool _initialized;
	bool _autoDelete;
	DWORD _flagsAndAttributes;

protected:
	CBicDataWriter (bool autoDelete, DWORD flagsAndAttributes)
		: _flagsAndAttributes (flagsAndAttributes), _fileName (), _autoDelete (autoDelete),
		_file (INVALID_HANDLE_VALUE), _initialized (false)
	{
	}

public:
	CBicDataWriter (bool autoDelete)
		: _flagsAndAttributes (FILE_ATTRIBUTE_NORMAL), _fileName (), _autoDelete (autoDelete),
		_file (INVALID_HANDLE_VALUE), _initialized (false)
	{
	}

	~CBicDataWriter ()
	{
		Delete ();
	}

	LPCTSTR GetFileName () const
	{
		return _fileName;
	}

	bool Open ()
	{
		if (!_initialized && !Initialize (NULL))
		{
			return false;
		}

		_file = CreateFile (_fileName, GENERIC_WRITE, 0, NULL, OPEN_ALWAYS, _flagsAndAttributes, NULL);
		if (_file == INVALID_HANDLE_VALUE)
		{
  		msicLogErr1( _T( "File \"%s\" could not be created.\r\n" ), __FILE__, __LINE__, static_cast< LPCTSTR >( _fileName ) );
			return false;
		}

		return true;
	}

	bool Open (LPCTSTR path)
	{
		if (!Initialize (path) || !Open ())
		{
			return false;
		}

		return true;
	}

	void Close ()
	{
		if (_file != INVALID_HANDLE_VALUE)
		{
			CloseHandle (_file);
			_file = INVALID_HANDLE_VALUE;
		}
	}

	void Delete ()
	{
		Close ();

		if (_initialized && _autoDelete)
		{
			DeleteFile (_fileName);
			_initialized = false;
			_fileName = _T ("");
		}
	}

	bool Write (const unsigned char *buf, unsigned int size)
	{
		DWORD written;
		DWORD offset = 0;
		do {
			if (!WriteFile (_file, buf + offset, size - offset, &written, NULL))
			{
    		msicLogErr1( _T( "File \"%s\" could not be written to.\r\n" ), __FILE__, __LINE__, static_cast< LPCTSTR >( _fileName ) );
				return false;
			}
			offset += written;
		} while (offset < size);

		return true;
	}

	bool Write (const CBicTString &str)
	{
		DWORD written;
		DWORD offset = 0;
		const unsigned char *buf = reinterpret_cast<const unsigned char*> (static_cast<LPCTSTR> (str));
		unsigned int size = str.GetSize ();
		if (size < 2)
		{
			// zero length or only terminating zero char
			return true;
		}
		--size;
		size *= sizeof (TCHAR);
		do {
			if (!WriteFile (_file, buf + offset, size - offset, &written, NULL))
			{
    		msicLogErr1( _T( "File \"%s\" could not be written to.\r\n" ), __FILE__, __LINE__, static_cast< LPCTSTR >( _fileName ) );
				return false;
			}
			offset += written;
		} while (offset < size);

		return true;
	}

	bool Write (const CBicByteStream &stream)
	{
		DWORD written;
		DWORD offset = 0;
		const unsigned char *buf = stream.GetData ();
		unsigned int size = stream.GetSize ();
		do {
			if (!WriteFile (_file, buf + offset, size - offset, &written, NULL))
			{
    		msicLogErr1( _T( "File \"%s\" could not be written to.\r\n" ), __FILE__, __LINE__, static_cast< LPCTSTR >( _fileName ) );
				return false;
			}
			offset += written;
		} while (offset < size);

		return true;
	}

	virtual bool Initialize (LPCTSTR path)
	{
		_fileName = path;
		_initialized = true;
		return true;
	}
};
