
#include "msic.h"


unsigned int CBicFeature::_counter = 0;


CBicFeature::CBicFeature (LPCTSTR title, LPCTSTR description, short display, short level, CBicDirectory *directory, 
						  short attributes, CBicFeature *featureParent)
	: _featureParent (featureParent), _display (display), _level (level), _directory (directory), _attributes (attributes),
	_title (title), _description (description)
{
	_feature.Format (_T ("Feature_%04u"), ++_counter);
}

bool CBicFeature::AddComponent (CBicComponent *component)
{
	if (component != NULL)
	{
		return _components.Add (component);
	}

	return false;
}

bool CBicFeature::FillForFeature (CBicMsiRecord &rec) const
{
	if (!rec.Fill (1, _feature) ||
		!rec.Fill (2, _featureParent != NULL ? static_cast<LPCTSTR> (_featureParent->_feature) : static_cast<LPCTSTR> (NULL)) ||
		!rec.Fill (3, _title) ||
		!rec.Fill (4, _description) ||
		!rec.Fill (5, _display) ||
		!rec.Fill (6, _level) ||
		!rec.Fill (7, _directory->GetDirectory ()) ||
		!rec.Fill (8, _attributes))
	{
		return false;
	}

	return true;
}

bool CBicFeature::FillForFeatureComponents (CBicMsiRecord &rec, unsigned int comp) const
{
	if (!rec.Fill (1, _feature) ||
		!rec.Fill (2, _components[comp]->GetComponent ()))
	{
		return false;
	}

	return true;
}

unsigned int CBicFeature::GetComponentsSize () const
{
	return _components.GetSize ();
}
