
#include "msic.h"


CBicControlEvent::CBicControlEvent (CBicControl *control, LPCTSTR event, LPCTSTR argument, LPCTSTR condition)
: _ordering (control->NewEventOrdering ()), _control (control), _argumentDialog (NULL), _event (event),
_argument (argument), _condition (condition)
{
}

CBicControlEvent::CBicControlEvent (CBicControl *control, LPCTSTR event, CBicDialog *argumentDialog, LPCTSTR condition)
: _ordering (control->NewEventOrdering ()), _control (control), _argumentDialog (argumentDialog),
_event (event), _argument (_T ("")), _condition (condition)
{
}

bool CBicControlEvent::Fill (CBicMsiRecord &rec) const
{
	if (!rec.Fill (1, _control->GetDialog ()) ||
		!rec.Fill (2, _control->GetControl ()) ||
		!rec.Fill (3, _event) ||
		!rec.Fill (4, _argumentDialog == NULL ? _argument : _argumentDialog->GetDialog ()) ||
		!rec.Fill (5, _condition) ||
		!rec.Fill (6, _ordering))
	{
		return false;
	}

	return true;
}