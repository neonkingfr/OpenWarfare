
#define WRITE_PTR(ptr, size)	\
	if (!_data.Append ((ptr), (size)))\
	{\
		return false;\
	}

#define WRITE_LONG(i)	\
	{\
		long ml = static_cast<long> (i);\
		if (!_data.Append (reinterpret_cast<unsigned char*> (&ml), sizeof (ml)))\
		{\
			return false;\
		}\
	}

#define WRITE_STR(str)	\
	if (!AddString (str))\
	{\
		return false;\
	}

#define READ_PTR(ptr, n)		\
	{\
		memcpy (ptr, _reader, n);\
		_reader += n;\
	}

#define READ_LONG(i)		\
	{\
		long ml;\
		memcpy (reinterpret_cast<void*> (&ml), _reader, sizeof (ml));\
		_reader += sizeof (ml);\
		i = ml;\
	}

#define READ_STR(str)	ReadString(str);

class CBicByteStream
{
	CBicArray<unsigned char, unsigned char> _data;
	const unsigned char *_reader;
	const unsigned char *_dataEnd;

public:
	inline bool ReadFromFile (CBicFileReader &file, unsigned int &size)
	{
		return file.Read (_data.GetBuffer (size), size);
	}

	inline bool ReadFromFile (LPCTSTR filename)
	{
		CBicFileReader file;
		return file.Open (filename) && file.Read (_data);
	}

	inline const unsigned char* GetData () const
	{
		return _data.GetData ();
	}

	inline unsigned int GetSize () const
	{
		return _data.GetSize ();
	}

	inline unsigned int GetByteSize () const
	{
		return _data.GetByteSize ();
	}

	inline void Empty ()
	{
		_data.Empty ();
	}

	inline void PrepareForReading ()
	{
		_reader = _data.GetData ();
		_dataEnd = _data.GetEnd ();
	}

	inline bool IsNotEof () const
	{
		return _reader < _dataEnd;
	}

	inline bool WriteString (LPCTSTR str)
	{
		unsigned int n = (_tcslen (str) + 1) * sizeof (TCHAR);
		WRITE_LONG (n)
		WRITE_PTR (reinterpret_cast<const unsigned char*> (str), n)
		return true;
	}

	inline bool WriteString (const CBicTString &str)
	{
		unsigned int n = str.GetByteSize ();
		WRITE_LONG (n)
		WRITE_PTR (reinterpret_cast<const unsigned char*> (str.operator LPCTSTR ()), n)
		return true;
	}

	inline bool WriteLong (const long l)
	{
		WRITE_LONG (l)
		return true;
	}

	inline void ReadString (CBicTString &str)
	{
		unsigned int n;
		READ_LONG (n)
		TCHAR *buf = str.GetByteBuffer (n);
		READ_PTR (buf, n)
	}

	inline void ReadLong (long &l)
	{
		READ_LONG (l)
	}
	
	inline bool Append (const CBicByteStream &src)
	{
		return _data.Append (src._data);
	}
};

#undef READ_STR
#undef READ_PTR
#undef WRITE_STR
#undef WRITE_LONG
#undef WRITE_PTR
