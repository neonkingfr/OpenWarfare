

class CBicRadioButtonGroup;

//! Class of one record in Control table.
class CBicControl
{
protected:
	static unsigned int _counter;
	
	short _eventOrdering;

	CBicDialog *_dialog;
	CBicTString _control;
	CBicTString _type;
	short _x;
	short _y;
	short _width;
	short _height;
	long _attributes;

	CBicBaseProperty *_property;
	CBicTString _propertyText;

	CBicTString _text;
	CBicBinary *_icon;

	CBicTString _help;
	
	bool _tab;

public:
	enum
	{
		attrBiDi = 0x000000E0,
		attrEnabled = 0x00000002,
		attrIndirect = 0x00000008,
		attrInteger = 0x00000010,
		attrLeftScroll = 0x00000080,
		attrRightAligned = 0x00000040,
		attrRTLRO = 0x00000020,
		attrSunken = 0x00000004,
		attrVisible = 0x00000001,

		// text controls
		attrFormatSize = 0x00080000,
		attrNoPrefix = 0x00020000,
		attrNoWrap = 0x00040000,
		attrPasswordInput = 0x00200000,
		attrTransparent = 0x00010000,
		attrUsersLanguage = 0x00100000,

		// progress bar
		attrProgress95 = 0x00010000,
		
		// volume and directory select combo controls 
		attrCDROMVolume = 0x00080000,
		attrFixedVolume = 0x00020000,
		attrFloppyVolume = 0x00200000,
		attrRAMDiskVolume = 0x00100000,
		attrRemoteVolume = 0x00040000,
		attrRemovableVolume = 0x00010000,
		
		// list box and combo box controls
		attrComboList = 0x00020000,
		attrSorted = 0x00010000,
		
		// edit control
		attrMultiline = 0x00010000,
		
		// picture button controls
		attrBitmap = 0x00040000,
		attrFixedSize = 0x00100000,
		attrIcon = 0x00080000,
		attrIconSize16 = 0x00200000,
		attrIconSize32 = 0x00400000,
		attrIconSize48 = 0x00600000,
		attrImageHandle = 0x00010000,
		attrPushLike = 0x00020000,

		// radio button controls
		attrHasBorder = 0x01000000,

		// VolumeCostList Control
		ctrlShowRollbackCost = 0x00400000,
	};

	CBicControl (LPCTSTR type, short x, short y, short width, short height, long attributes, CBicBaseProperty *property, 
		LPCTSTR text, LPCTSTR help, bool tab);

	CBicControl (LPCTSTR type, short x, short y, short width, short height, long attributes, LPCTSTR property, 
		LPCTSTR text, LPCTSTR help, bool tab);

	CBicControl (LPCTSTR control, LPCTSTR type, short x, short y, short width, short height, long attributes, CBicBaseProperty *property, 
		LPCTSTR text, LPCTSTR help, bool tab);

	CBicControl (LPCTSTR type, short x, short y, short width, short height, long attributes, CBicBaseProperty *property, 
		CBicBinary *icon, LPCTSTR help, bool tab);

	LPCTSTR GetControl () const;
	LPCTSTR GetDialog () const;
	bool IsTab () const;
	
	bool SetDialog (CBicDialog *dialog);

	short NewEventOrdering ();

	bool Fill (CBicMsiRecord &rec, const CBicControl *next) const;

	static inline void ResetCounter ()
	{
		_counter = 0;
	}
};