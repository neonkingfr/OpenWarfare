
#include "resource.h"
#include "msic.h"

LPCTSTR msicLanguage = _T ("1033");
FILE *msicLogFile = stdout;
bool msicLogInFile = false;
bool msicLogInWindow = true;

// Dialog Texts

// FilesInUseDlg
LPCTSTR dtFilesInUse1 = _T ("The following applications are using files that need to be updated by this setup. "
		"Close these applications and then click Retry to continue the installation or Cancel to exit it.");
LPCTSTR dtFilesInUse2 = _T ("{\\DlgFontBold8}Files in Use");
LPCTSTR dtFilesInUse3 = _T ("Some files that need to be updated are currently in use.");

// OutOfDiskDlg
LPCTSTR dtOutOfDisk1 = _T ("The highlighted volumes do not have enough disk space available for the currently selected features.  "
		"You can either remove some files from the highlighted volumes, "
		"or choose to install less features onto local drive(s), or select different destination drive(s).");
LPCTSTR dtOutOfDisk2 = _T ("{\\DlgFontBold8}Out of Disk Space");

// PrepareDlg
LPCTSTR dtPrepare1 = _T ("Please wait while the [Wizard] prepares to guide you through the installation.");
LPCTSTR dtPrepare2 = _T ("{\\VerdanaBold13}Welcome to the [SingleProductName] [Wizard]");

// BadSerialNumberDlg
#if _VBS1
LPCTSTR dtBadSerialNumber1 = _T ("The [Wizard] found problems with your VBS1 instalation."
		"  Remember: original games do not fade."
		"  Click Cancel to exit the [Wizard].");
#else
LPCTSTR dtBadSerialNumber1 = _T ("The [Wizard] found problems with your Operation Flashpoint instalation."
		"  Remember: original games do not fade."
		"  Click Cancel to exit the [Wizard].");
#endif


LPCTSTR dtBadSerialNumber2 = _T ("{\\VerdanaBold13}Welcome to the [SingleProductName] [Wizard]");

// WelcomeDlg
LPCTSTR dtWelcome1 = _T ("The [Wizard] will install [SingleProductName] on your computer."
		"  Click Next to continue or Cancel to exit the [Wizard].");
LPCTSTR dtWelcome2 = _T ("{\\VerdanaBold13}Welcome to the [SingleProductName] [Wizard]");

// LicenceDlg
LPCTSTR dtLicence1 = _T ("{\\DlgFont8}I &accept the terms in the License Agreement");
LPCTSTR dtLicence2 = _T ("{\\DlgFont8}I &do not accept the terms in the License Agreement");
LPCTSTR dtLicence3 = _T ("Please read the following license agreement carefully");
LPCTSTR dtLicence4 = _T ("{\\DlgFontBold8}End-User License Agreement");
LPCTSTR dtLicence5 = _T ("[SingleProductName] License Agreement");

// WaitForCostingDlg
LPCTSTR dtWaitForCosting1 = _T ("Please wait while the installer finishes determining your disk space requirements.");

// VerifyReadyDlg
LPCTSTR dtVerifyReady1 = _T ("Click Install to begin the installation.  "
							 "If you want to review or change any of your installation settings, "
							 "click Back.  Click Cancel to exit the wizard.");
LPCTSTR dtVerifyReady2 = _T ("The [Wizard] is ready to begin the installation");
LPCTSTR dtVerifyReady3 = _T ("{\\DlgFontBold8}Ready to Install");

// OutOfRbDiskDlg
LPCTSTR dtOutOfRbDisk1 = _T ("Disk space required for the installation exceeds available disk space.");
LPCTSTR dtOutOfRbDisk2 = _T ("The highlighted volumes do not have enough disk space available for the currently selected features.  "
		"You can either remove some files from the highlighted volumes, "
		"or choose to install less features onto local drive(s), or select different destination drive(s).");
LPCTSTR dtOutOfRbDisk3 = _T ("Alternatively, you may choose to disable the installer's rollback functionality.  "
		"This allows the installer to restore your computer's original state should the installation be "
		"interrupted in any way.  Click Yes if you wish to take the risk to disable rollback.");
LPCTSTR dtOutOfRbDisk4 = _T ("{\\DlgFontBold8}Out of Disk Space");

// ResumeDlg
LPCTSTR dtResume1 = _T ("The [Wizard] will complete the installation of [SingleProductName] on your computer.  "
		"Click Install to continue or Cancel to exit the [Wizard].");
LPCTSTR dtResume2 = _T ("{\\VerdanaBold13}Resuming the [SingleProductName] [Wizard]");

// MaintenanceWelcomeDlg
LPCTSTR dtMaintenanceWelcome1 = _T ("The [Wizard] will allow you to change the way [SingleProductName] features are installed on your computer "
		"or even to remove [SingleProductName] from your computer.  Click Next to continue or Cancel to exit the [Wizard].");
LPCTSTR dtMaintenanceWelcome2 = _T ("{\\VerdanaBold13}Welcome to the [SingleProductName] [Wizard]");

// MaintenanceTypeDlg
LPCTSTR dtMaintenaceType1 = _T ("Repairs errors in the most recent installation state - fixes missing or corrupt files, "
		"shortcuts and registry entries.");
LPCTSTR dtMaintenaceType2 = _T ("Removes [SingleProductName] from your computer.");
LPCTSTR dtMaintenanceType3 = _T ("Select the operation you wish to perform.");
LPCTSTR dtMaintenanceType4 = _T ("{\\DlgFontBold8}Modify, Repair or Remove installation");

// ProgressDlg
LPCTSTR dtProgress1 = _T ("Please wait while the [Wizard] [Progress2] [SingleProductName].  This may take several minutes.");

// VerifyRepairDlg
LPCTSTR dtVerifyRepair1 = _T ("Click Repair to repair the installation of [SingleProductName].  "
		"If you want to review or change any of your installation settings, click Back.  "
		"Click Cancel to exit the wizard.");
LPCTSTR dtVerifyRepair2 = _T ("The [Wizard] is ready to begin the repair of [SingleProductName].");

// VerifyRemoveDlg
LPCTSTR dtVerifyRemove1 = _T ("You have chosen to remove the program from your computer.");
LPCTSTR dtVerifyRemove2 = _T ("Click Remove to remove [SingleProductName] from your computer.  "
		"If you want to review or change any of your installation settings, click Back.  "
		"Click Cancel to exit the wizard.");

// FatalErrorDlg
LPCTSTR dtFatalError1 = _T ("[SingleProductName] setup ended prematurely because of an error.  "
		"Your system has not been modified.  To install this program at a later time, "
		"please run the installation again.");
LPCTSTR dtFatalError2 = _T ("Click the Finish button to exit the [Wizard].");
LPCTSTR dtFatalError3 = _T ("{\\VerdanaBold13}[SingleProductName] [Wizard] ended prematurely");

// NoRegistry
#if _VBS1
LPCTSTR dtNoRegistry1 = _T ("No application of VBS1 was found.");
#else
LPCTSTR dtNoRegistry1 = _T ("No application of Operation Flashpoint was found.");
#endif
LPCTSTR dtNoRegistry2 = _T ("{\\VerdanaBold13}[SingleProductName] [Wizard] ended prematurely");

// UserExitDlg
LPCTSTR dtUserExit1 = _T ("[SingleProductName] setup was interrupted.  Your system has not been modified.  "
		"To install this program at a later time, please run the installation again.");
LPCTSTR dtUserExit2 = _T ("Click the Finish button to exit the [Wizard].");
LPCTSTR dtUserExit3 = _T ("{\\VerdanaBold13}[SingleProductName] [Wizard] was interrupted");

// ExitDlg
LPCTSTR dtExit1 = _T ("Click the Finish button to exit the [Wizard].");
LPCTSTR dtExit2 = _T ("{\\VerdanaBold13}Completing the [SingleProductName] [Wizard]");

// CancelDlg
LPCTSTR dtCancel1 = _T ("Are you sure you want to cancel [SingleProductName] installation?");

// OutOfDiskDlg
LPCTSTR dtOutOfDisk3 = _T ("Disk space required for the installation exceeds available disk space.");

void RemoveWholeDirectory (LPCTSTR path)
{
	WIN32_FIND_DATA find;
	CBicTString file (path);
	file += _T ("*.*");
	HANDLE h = FindFirstFile (file, &find);
	BOOL found = h != INVALID_HANDLE_VALUE;
	while (found)
	{
		if (find.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			if (_tcscmp (find.cFileName, _T (".")) != 0 && _tcscmp (find.cFileName, _T ("..")) != 0)
			{
				file = path;
				file += find.cFileName;
				file += _T ("\\");
				RemoveWholeDirectory (file);
			}
			found = FindNextFile (h, &find);
		}
		else
		{
			file = path;
			file += find.cFileName;
			found = FindNextFile (h, &find);
			DeleteFile (file);
		}
	}

	RemoveDirectory (path);
}

bool GenGuid (CBicTString &str)
{
	GUID guid;
	HRESULT result = CoCreateGuid (&guid);
	if (result != S_OK)
	{
		msicLogErr0 (_T ("CoCreateGuid failed.\r\n"), __FILE__, __LINE__);
		return false;
	}
	OLECHAR guidOleString[255];
	if (StringFromGUID2 (guid, guidOleString, 255) > 255)
	{
		msicLogErr0 (_T ("StringFromGUID2 failed.\r\n"), __FILE__, __LINE__);
		return false;
	}
	USES_CONVERSION;
	str = OLE2CT (guidOleString);
	return true;
}

bool GenGuid (LPTSTR tStr, int len)
{
	GUID guid;
	HRESULT result = CoCreateGuid (&guid);
	if (result != S_OK)
	{
		msicLogErr0 (_T ("CoCreateGuid failed.\r\n"), __FILE__, __LINE__);
		return false;
	}
	OLECHAR guidOleString[255];
	if (StringFromGUID2 (guid, guidOleString, 255) > len)
	{
		msicLogErr0 (_T ("StringFromGUID2 failed.\r\n"), __FILE__, __LINE__);
		return false;
	}
	USES_CONVERSION;
	_tcscpy (tStr, OLE2CT (guidOleString));
	return true;
}

bool AddAddonDirectories (CBicDatabase &db, LPCTSTR fromDir, bool compressed, bool &readMePresent, CBicTString &readMeFileName, const CBicTString &productName, const CBicTString &productVersion)
{
	// Media
	CBicMedia *media = db.NewMedia ();
	if (media == NULL) { return false; }

	// Directories
	CBicDirectory *dirRoot = db.NewDirectory (_T ("TARGETDIR"), _T ("SourceDir"));
	if (dirRoot == NULL) { return false; }

	CBicDirectory *dirInstall = db.NewDirectory (_T ("INSTALLDIR"), _T ("."), dirRoot);
	if (dirInstall == NULL) { return false; }

	CBicDirectory *dirAddonsAtEase = db.NewDirectory (_T ("ADDONSATEASE"), _T ("AddonsAtEase:."), dirInstall);
	if (dirAddonsAtEase == NULL) { return false; }

#if !_VBS1
	CBicDirectory *dirMissions = db.NewDirectory (_T ("MISSIONS"), _T ("Missions"), dirInstall);
	if (dirMissions == NULL) { return false; }
#endif

	CBicDirectory *dirResistance = db.NewDirectory (_T ("RES"), _T ("Res"), dirInstall);
	if (dirResistance == NULL) { return false; }

	// Feature
#if _VBS1
	CBicFeature *feature = db.NewFeature (productName, _T ("Addon for VBS1."), 1, 3, dirInstall, 
		CBicFeature::attrFavorLocal | CBicFeature::attrDisallowAdvertise | CBicFeature::attrUIDisallowAbsent);
#else
	CBicFeature *feature = db.NewFeature (productName, _T ("Addon for Operation Flashpoint."), 1, 3, dirInstall, 
		CBicFeature::attrFavorLocal | CBicFeature::attrDisallowAdvertise | CBicFeature::attrUIDisallowAbsent);
#endif

	if (feature == NULL) { return false; }

	// Registry
#if _VBS1
	CBicRegLocator *regFlashpointPath = db.NewRegLocator (dirInstall, CBicRegLocator::rootLocalMachine, 
		_T ("Software\\BIS\\VBS1"), _T ("MAIN"), CBicRegLocator::typeDirectory);
#else
	CBicRegLocator *regFlashpointPath = db.NewRegLocator (dirInstall, CBicRegLocator::rootLocalMachine, 
		_T ("SOFTWARE\\Codemasters\\Operation Flashpoint"), _T ("MAIN"), CBicRegLocator::typeDirectory);
#endif
	if (regFlashpointPath == NULL) { return false; }

	CBicProperty *prop = db.NewProperty (_T ("PATHTAG"), _T (":"));
	if (prop == NULL) { return false; }

#if _VBS1
	CBicRegLocator *tagFlashpointPath = db.NewRegLocator (prop, CBicRegLocator::rootLocalMachine, 
		_T ("Software\\BIS\\VBS1"), _T ("MAIN"), CBicRegLocator::typeDirectory);
#else
	CBicRegLocator *tagFlashpointPath = db.NewRegLocator (prop, CBicRegLocator::rootLocalMachine, 
		_T ("SOFTWARE\\Codemasters\\Operation Flashpoint"), _T ("MAIN"), CBicRegLocator::typeDirectory);
#endif

	if (tagFlashpointPath == NULL) { return false; }

	CBicProperty *keyNote = db.NewProperty (_T ("KEYNOTE"), _T ("0"));
	if (keyNote == NULL) { return false; }

#if _VBS1
	CBicRegLocator *tagFlashpointKey = db.NewRegLocator (keyNote, CBicRegLocator::rootLocalMachine, 
		_T ("Software\\BIS\\VBS1"), _T ("KEY_NOTE"), CBicRegLocator::typeRawValue);
#else
	CBicRegLocator *tagFlashpointKey = db.NewRegLocator (keyNote, CBicRegLocator::rootLocalMachine, 
		_T ("SOFTWARE\\Codemasters\\Operation Flashpoint"), _T ("KEY_NOTE"), CBicRegLocator::typeRawValue);
#endif

	if (tagFlashpointKey == NULL) { return false; }

	// Directories, Components, Files
	unsigned int c = CBicFile::GetCounter ();

	CBicTString addFiles;
	addFiles = fromDir;
	addFiles += _T ("readme.txt");
	WIN32_FIND_DATA fData;
	HANDLE h = FindFirstFile (addFiles, &fData);
	if (h != INVALID_HANDLE_VALUE)
	{
		readMePresent = _taccess (addFiles, 04) == 0;
		if (readMePresent)
		{
			CBicFile *f = db.NewFile (media, _T ("readme.txt"), fData.nFileSizeLow, CBicFile::attrVital);
			if (f == NULL) { return false; }

			CBicComponent *c = db.NewComponent (dirAddonsAtEase, CBicComponent::attrLocalOnly, _T ("1"), f);
			if (c == NULL) { return false; }

			if (!feature->AddComponent (c)) { return false; }

			f->SetComponent (c);

			readMeFileName = productName;
			readMeFileName += _T (" ReadMe.txt");
			f->SetDstFileName (readMeFileName);
		}
	}

	if (!db.NewRecursiveDirectoriesComponentsFiles (media, feature, fromDir, _T ("Addons"), dirInstall, CBicComponent::attrLocalOnly, _T ("1"), CBicFile::attrVital))
	{
		return false;
	}

#if _VBS1
  if (!db.NewRecursiveDirectoriesComponentsFiles (media, feature, fromDir, _T ("Missions"), dirInstall, CBicComponent::attrLocalOnly, _T ("1"), CBicFile::attrVital))
  {
    return false;
  }
#else
	if (!db.NewRecursiveDirectoriesComponentsFiles (media, feature, fromDir, _T ("Addons At Ease:."), dirMissions, CBicComponent::attrLocalOnly, _T ("1"), CBicFile::attrVital))
	{
		return false;
	}
#endif

	if (!db.NewRecursiveDirectoriesComponentsFiles (media, feature, fromDir, _T ("MPMissions"), dirInstall, CBicComponent::attrLocalOnly, _T ("1"), CBicFile::attrVital))
	{
		return false;
	}

	if (!db.NewRecursiveDirectoriesComponentsFiles (media, feature, fromDir, _T ("Campaigns"), dirInstall, CBicComponent::attrLocalOnly, _T ("1"), CBicFile::attrVital))
	{
		return false;
	}

	if (!db.NewRecursiveDirectoriesComponentsFiles (media, feature, fromDir, _T ("Addons"), dirResistance, CBicComponent::attrLocalOnly, _T ("1"), CBicFile::attrVital))
	{
		return false;
	}

	if (CBicFile::GetCounter () == c)
	{
		msicLogErr0 (_T ("No files present.\r\n"), __FILE__, __LINE__);
		return false;
	}

	// Properties - required
	prop = db.NewProperty (_T ("ProductCode"));
	if (prop == NULL) { return false; }
	if (!prop->GenGuid ()) { return false; }

	prop = db.NewProperty (_T ("ProductLanguage"), _T ("1033"));
	if (prop == NULL) { return false; }

	prop = db.NewProperty (_T ("Manufacturer"), _T ("Bohemia Interactive Studio"));
	if (prop == NULL) { return false; }

	prop = db.NewProperty (_T ("ProductVersion"), productVersion);
	if (prop == NULL) { return false; }

#if _VBS1
  CBicTString buf = _T ("VBS Addon ");
#else
	CBicTString buf = _T ("OFP Addon ");
#endif
	buf += productName;
	prop = db.NewProperty (_T ("ProductName"), buf);
	if (prop == NULL) { return false; }

	prop = db.NewProperty (_T ("SingleProductName"), productName);
	if (prop == NULL) { return false; }

	prop = db.NewProperty (_T ("UpgradeCode"));
	if (prop == NULL) { return false; }
	if (!prop->GenGuid ()) { return false; }

	// Properties - optional
	return true;
}

bool AddInstallExecutiveActions (CBicDatabase &db)
{
#define NEW_INSTALL_EXE_ACTION(action, condition);		\
	{\
		CBicInstallExecutiveAction *exe = db.NewInstallExecutiveAction (action, condition);\
		if (exe == NULL) { return false; }\
	}

	NEW_INSTALL_EXE_ACTION (_T ("LaunchConditions"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("FindRelatedProducts"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("AppSearch"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("CCPSearch"), _T ("NOT Installed"));
	NEW_INSTALL_EXE_ACTION (_T ("RMCCPSearch"), _T ("NOT Installed"));
	NEW_INSTALL_EXE_ACTION (_T ("ValidateProductID"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("CostInitialize"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("FileCost"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("CostFinalize"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("SetODBCFolders"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("MigrateFeatureStates"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("InstallValidate"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("InstallInitialize"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("AllocateRegistrySpace"), _T ("NOT Installed"));
	NEW_INSTALL_EXE_ACTION (_T ("ProcessComponents"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("UnpublishComponents"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("UnpublishFeatures"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("StopServices"), _T ("VersionNT"));
	NEW_INSTALL_EXE_ACTION (_T ("DeleteServices"), _T ("VersionNT"));
	NEW_INSTALL_EXE_ACTION (_T ("UnregisterComPlus"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("SelfUnregModules"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("UnregisterTypeLibraries"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("RemoveODBC"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("UnregisterFonts"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("RemoveRegistryValues"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("UnregisterClassInfo"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("UnregisterExtensionInfo"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("UnregisterProgIdInfo"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("UnregisterMIMEInfo"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("RemoveIniValues"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("RemoveShortcuts"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("RemoveEnvironmentStrings"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("RemoveDuplicateFiles"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("RemoveFiles"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("RemoveFolders"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("CreateFolders"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("MoveFiles"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("InstallFiles"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("PatchFiles"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("DuplicateFiles"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("BindImage"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("CreateShortcuts"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("RegisterClassInfo"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("RegisterExtensionInfo"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("RegisterProgIdInfo"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("RegisterMIMEInfo"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("WriteRegistryValues"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("WriteIniValues"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("WriteEnvironmentStrings"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("RegisterFonts"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("InstallODBC"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("RegisterTypeLibraries"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("SelfRegModules"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("RegisterComPlus"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("InstallServices"), _T ("VersionNT"));
	NEW_INSTALL_EXE_ACTION (_T ("StartServices"), _T ("VersionNT"));
	NEW_INSTALL_EXE_ACTION (_T ("RegisterUser"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("RegisterProduct"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("PublishComponents"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("PublishFeatures"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("PublishProduct"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("InstallFinalize"), NULL);
	NEW_INSTALL_EXE_ACTION (_T ("RemoveExistingProducts"), NULL);

#undef NEW_INSTALL_EXE_ACTION

	return true;
}

bool AddActionTexts (CBicDatabase &db)
{
#define NEW_ACTION_TEXT(action, description, format);		\
	{\
		CBicActionText *text = db.NewActionText (action, description, format);\
		if (text == NULL) { return false; }\
	}

#define NEW_ACTION_TEXT2(action, description);		\
	{\
		CBicActionText *text = db.NewActionText (action, description);\
		if (text == NULL) { return false; }\
	}

	NEW_ACTION_TEXT2 (_T ("Advertise"), _T ("Advertising application"));
	NEW_ACTION_TEXT (_T ("AllocateRegistrySpace"), _T ("Allocating registry space"), _T ("Free space: [1]"));
	NEW_ACTION_TEXT (_T ("AppSearch"), _T ("Searching for installed applications"), _T ("Property: [1], Signature: [2]"));
	NEW_ACTION_TEXT (_T ("BindImage"), _T ("Binding executables"), _T ("File: [1]"));
	NEW_ACTION_TEXT2 (_T ("CCPSearch"), _T ("Searching for qualifying products"));
	NEW_ACTION_TEXT2 (_T ("CostFinalize"), _T ("Computing space requirements"));
	NEW_ACTION_TEXT2 (_T ("CostInitialize"), _T ("Computing space requirements"));
	NEW_ACTION_TEXT (_T ("CreateFolders"), _T ("Creating folders"), _T ("Folder: [1]"));
	NEW_ACTION_TEXT (_T ("CreateShortcuts"), _T ("Creating shortcuts"), _T ("Shortcut: [1]"));
	NEW_ACTION_TEXT (_T ("DeleteServices"), _T ("Deleting services"), _T ("Service: [1]"));
	NEW_ACTION_TEXT (_T ("DuplicateFiles"), _T ("Creating duplicate files"), _T ("File: [1],  Directory: [9],  Size: [6]"));
	NEW_ACTION_TEXT2 (_T ("FileCost"), _T ("Computing space requirements"));
	NEW_ACTION_TEXT (_T ("FindRelatedProducts"), _T ("Searching for related applications"), _T ("Found application: [1]"));
	NEW_ACTION_TEXT (_T ("GenerateScript"), _T ("Generating script operations for action:"), _T ("[1]"));
	NEW_ACTION_TEXT (_T ("InstallAdminPackage"), _T ("Copying network install files"), _T ("File: [1], Directory: [9], Size: [6]"));
	NEW_ACTION_TEXT (_T ("InstallFiles"), _T ("Copying new files"), _T ("File: [1],  Directory: [9],  Size: [6]"));
	NEW_ACTION_TEXT2 (_T ("InstallODBC"), _T ("Installing ODBC components"));
	NEW_ACTION_TEXT (_T ("InstallSFPCatalogFile"), _T ("Installing system catalog"), _T ("File: [1],  Dependencies: [2]"));
	NEW_ACTION_TEXT (_T ("InstallServices"), _T ("Installing new services"), _T ("Service: [2]"));
	NEW_ACTION_TEXT2 (_T ("InstallValidate"), _T ("Validating install"));
	NEW_ACTION_TEXT2 (_T ("LaunchConditions"), _T ("Evaluating launch conditions"));
	NEW_ACTION_TEXT (_T ("MigrateFeatureStates"), _T ("Migrating feature states from related applications"), _T ("Application: [1]"));
	NEW_ACTION_TEXT (_T ("MoveFiles"), _T ("Moving files"), _T ("File: [1],  Directory: [9],  Size: [6]"));
	NEW_ACTION_TEXT (_T ("MsiPublishAssemblies"), _T ("Publishing assembly information"), _T ("Application Context:[1], Assembly Name:[2]"));
	NEW_ACTION_TEXT (_T ("MsiUnpublishAssemblies"), _T ("Unpublishing assembly information"), _T ("Application Context:[1], Assembly Name:[2]"));
	NEW_ACTION_TEXT (_T ("PatchFiles"), _T ("Patching files"), _T ("File: [1],  Directory: [2],  Size: [3]"));
	NEW_ACTION_TEXT2 (_T ("ProcessComponents"), _T ("Updating component registration"));
	NEW_ACTION_TEXT (_T ("PublishComponents"), _T ("Publishing Qualified Components"), _T ("Component ID: [1], Qualifier: [2]"));
	NEW_ACTION_TEXT (_T ("PublishFeatures"), _T ("Publishing Product Features"), _T ("Feature: [1]"));
	NEW_ACTION_TEXT2 (_T ("PublishProduct"), _T ("Publishing product information"));
	NEW_ACTION_TEXT2 (_T ("RMCCPSearch"), _T ("Searching for qualifying products"));
	NEW_ACTION_TEXT (_T ("RegisterClassInfo"), _T ("Registering Class servers"), _T ("Class Id: [1]"));
	NEW_ACTION_TEXT (_T ("RegisterComPlus"), _T ("Registering COM+ Applications and Components"), _T ("AppId: [1]{{, AppType: [2], Users: [3], RSN: [4]}}"));
	NEW_ACTION_TEXT (_T ("RegisterExtensionInfo"), _T ("Registering extension servers"), _T ("Extension: [1]"));
	NEW_ACTION_TEXT (_T ("RegisterFonts"), _T ("Registering fonts"), _T ("Font: [1]"));
	NEW_ACTION_TEXT (_T ("RegisterMIMEInfo"), _T ("Registering MIME info"), _T ("MIME Content Type: [1], Extension: [2]"));
	NEW_ACTION_TEXT (_T ("RegisterProduct"), _T ("Registering product"), _T ("[1]"));
	NEW_ACTION_TEXT (_T ("RegisterProgIdInfo"), _T ("Registering program identifiers"), _T ("ProgId: [1]"));
	NEW_ACTION_TEXT (_T ("RegisterTypeLibraries"), _T ("Registering type libraries"), _T ("LibID: [1]"));
	NEW_ACTION_TEXT (_T ("RegisterUser"), _T ("Registering user"), _T ("[1]"));
	NEW_ACTION_TEXT (_T ("RemoveDuplicateFiles"), _T ("Removing duplicated files"), _T ("File: [1], Directory: [9]"));
	NEW_ACTION_TEXT (_T ("RemoveEnvironmentStrings"), _T ("Updating environment strings"), _T ("Name: [1], Value: [2], Action [3]"));
	NEW_ACTION_TEXT (_T ("RemoveExistingProducts"), _T ("Removing applications"), _T ("Application: [1], Command line: [2]"));
	NEW_ACTION_TEXT (_T ("RemoveFiles"), _T ("Removing files"), _T ("File: [1], Directory: [9]"));
	NEW_ACTION_TEXT (_T ("RemoveFolders"), _T ("Removing folders"), _T ("Folder: [1]"));
	NEW_ACTION_TEXT (_T ("RemoveIniValues"), _T ("Removing INI files entries"), _T ("File: [1],  Section: [2],  Key: [3], Value: [4]"));
	NEW_ACTION_TEXT2 (_T ("RemoveODBC"), _T ("Removing ODBC components"));
	NEW_ACTION_TEXT (_T ("RemoveRegistryValues"), _T ("Removing system registry values"), _T ("Key: [1], Name: [2]"));
	NEW_ACTION_TEXT (_T ("RemoveShortcuts"), _T ("Removing shortcuts"), _T ("Shortcut: [1]"));
	NEW_ACTION_TEXT (_T ("Rollback"), _T ("Rolling back action:"), _T ("[1]"));
	NEW_ACTION_TEXT (_T ("RollbackCleanup"), _T ("Removing backup files"), _T ("File: [1]"));
	NEW_ACTION_TEXT (_T ("SelfRegModules"), _T ("Registering modules"), _T ("File: [1], Folder: [2]"));
	NEW_ACTION_TEXT (_T ("SelfUnregModules"), _T ("Unregistering modules"), _T ("File: [1], Folder: [2]"));
	NEW_ACTION_TEXT2 (_T ("SetODBCFolders"), _T ("Initializing ODBC directories"));
	NEW_ACTION_TEXT (_T ("StartServices"), _T ("Starting services"), _T ("Service: [1]"));
	NEW_ACTION_TEXT (_T ("StopServices"), _T ("Stopping services"), _T ("Service: [1]"));
	NEW_ACTION_TEXT (_T ("UnmoveFiles"), _T ("Removing moved files"), _T ("File: [1], Directory: [9]"));
	NEW_ACTION_TEXT (_T ("UnpublishComponents"), _T ("Unpublishing Qualified Components"), _T ("Component ID: [1], Qualifier: [2]"));
	NEW_ACTION_TEXT (_T ("UnpublishFeatures"), _T ("Unpublishing Product Features"), _T ("Feature: [1]"));
	NEW_ACTION_TEXT2 (_T ("UnpublishProduct"), _T ("Unpublishing product information"));
	NEW_ACTION_TEXT (_T ("UnregisterClassInfo"), _T ("Unregister Class servers"), _T ("Class Id: [1]"));
	NEW_ACTION_TEXT (_T ("UnregisterComPlus"), _T ("Unregistering COM+ Applications and Components"), _T ("AppId: [1]{{, AppType: [2]}}"));
	NEW_ACTION_TEXT (_T ("UnregisterExtensionInfo"), _T ("Unregistering extension servers"), _T ("Extension: [1]"));
	NEW_ACTION_TEXT (_T ("UnregisterFonts"), _T ("Unregistering fonts"), _T ("Font: [1]"));
	NEW_ACTION_TEXT (_T ("UnregisterMIMEInfo"), _T ("Unregistering MIME info"), _T ("MIME Content Type: [1], Extension: [2]"));
	NEW_ACTION_TEXT (_T ("UnregisterProgIdInfo"), _T ("Unregistering program identifiers"), _T ("ProgId: [1]"));
	NEW_ACTION_TEXT (_T ("UnregisterTypeLibraries"), _T ("Unregistering type libraries"), _T ("LibID: [1]"));
	NEW_ACTION_TEXT (_T ("WriteEnvironmentStrings"), _T ("Updating environment strings"), _T ("Name: [1], Value: [2], Action [3]"));
	NEW_ACTION_TEXT (_T ("WriteIniValues"), _T ("Writing INI files values"), _T ("File: [1],  Section: [2],  Key: [3], Value: [4]"));
	NEW_ACTION_TEXT (_T ("WriteRegistryValues"), _T ("Writing system registry values"), _T ("Key: [1], Name: [2], Value: [3]"));

#undef NEW_ACTION_TEXT
#undef NEW_ACTION_TEXT2

	return true;
}

bool AddErrors (CBicDatabase &db)
{
#define NEW_ERROR(error, message);		\
	{\
		CBicError *err = db.NewError (error, message);\
		if (err == NULL) { return false; }\
	}

#define NEW_ERROR2(error);		\
	{\
		CBicError *err = db.NewError (error);\
		if (err == NULL) { return false; }\
	}

	NEW_ERROR (0, _T ("{{Fatal error: }}"));
	NEW_ERROR (1, _T ("{{Error [1]. }}"));
	NEW_ERROR (2, _T ("Warning [1]. "));
	NEW_ERROR2 (3);
	NEW_ERROR (4, _T ("Info [1]. "));
	NEW_ERROR (5, _T ("The installer has encountered an unexpected error installing this package. This may indicate a problem with this package. The error code is [1]. {{The arguments are: [2], [3], [4]}}"));
	NEW_ERROR2 (6);
	NEW_ERROR (7, _T ("{{Disk full: }}"));
	NEW_ERROR (8, _T ("Action [Time]: [1]. [2]"));
	NEW_ERROR (9, _T ("[SingleProductName]"));
	NEW_ERROR (10, _T ("{[2]}{, [3]}{, [4]}"));
	NEW_ERROR (11, _T ("Message type: [1], Argument: [2]"));
	NEW_ERROR (12, _T ("=== Logging started: [Date]  [Time] ==="));
	NEW_ERROR (13, _T ("=== Logging stopped: [Date]  [Time] ==="));
	NEW_ERROR (14, _T ("Action start [Time]: [1]."));
	NEW_ERROR (15, _T ("Action ended [Time]: [1]. Return value [2]."));
	NEW_ERROR (16, _T ("Time remaining: {[1] minutes }{[2] seconds}"));
	NEW_ERROR (17, _T ("Out of memory. Shut down other applications before retrying."));
	NEW_ERROR (18, _T ("Installer is no longer responding."));
	NEW_ERROR (19, _T ("Installer stopped prematurely."));
	NEW_ERROR (20, _T ("Please wait while Windows configures [SingleProductName]"));
	NEW_ERROR (21, _T ("Gathering required information..."));
	NEW_ERROR (22, _T ("Removing older versions of this application..."));
	NEW_ERROR (23, _T ("Preparing to remove older versions of this application..."));
	NEW_ERROR (32, _T ("{[SingleProductName] }Setup completed successfully."));
	NEW_ERROR (33, _T ("{[SingleProductName] }Setup failed."));
	NEW_ERROR (1101, _T ("Error reading from file: [2]. {{ System error [3].}}  Verify that the file exists and that you can access it."));
	NEW_ERROR (1301, _T ("Cannot create the file '[2]'.  A directory with this name already exists.  Cancel the install and try installing to a different location."));
	NEW_ERROR (1302, _T ("Please insert the disk: [2]"));
	NEW_ERROR (1303, _T ("The installer has insufficient privileges to access this directory: [2].  The installation cannot continue.  Log on as administrator or contact your system administrator."));
	NEW_ERROR (1304, _T ("Error writing to file: [2].  Verify that you have access to that directory."));
	NEW_ERROR (1305, _T ("Error reading from file [2]. {{ System error [3].}} Verify that the file exists and that you can access it."));
	NEW_ERROR (1306, _T ("Another application has exclusive access to the file '[2]'.  Please shut down all other applications, then click Retry."));
	NEW_ERROR (1307, _T ("There is not enough disk space to install this file: [2].  Free some disk space and click Retry, or click Cancel to exit."));
	NEW_ERROR (1308, _T ("Source file not found: [2].  Verify that the file exists and that you can access it."));
	NEW_ERROR (1309, _T ("Error reading from file: [3]. {{ System error [2].}}  Verify that the file exists and that you can access it."));
	NEW_ERROR (1310, _T ("Error writing to file: [3]. {{ System error [2].}}  Verify that you have access to that directory."));
	NEW_ERROR (1311, _T ("Source file not found{{(cabinet)}}: [2].  Verify that the file exists and that you can access it."));
	NEW_ERROR (1312, _T ("Cannot create the directory '[2]'.  A file with this name already exists.  Please rename or remove the file and click retry, or click Cancel to exit."));
	NEW_ERROR (1313, _T ("The volume [2] is currently unavailable.  Please select another."));
	NEW_ERROR (1314, _T ("The specified path '[2]' is unavailable."));
	NEW_ERROR (1315, _T ("Unable to write to the specified folder: [2]."));
	NEW_ERROR (1316, _T ("A network error occurred while attempting to read from the file: [2]"));
	NEW_ERROR (1317, _T ("An error occurred while attempting to create the directory: [2]"));
	NEW_ERROR (1318, _T ("A network error occurred while attempting to create the directory: [2]"));
	NEW_ERROR (1319, _T ("A network error occurred while attempting to open the source file cabinet: [2]"));
	NEW_ERROR (1320, _T ("The specified path is too long: [2]"));
	NEW_ERROR (1321, _T ("The Installer has insufficient privileges to modify this file: [2]."));
	NEW_ERROR (1322, _T ("A portion of the folder path '[2]' is invalid.  It is either empty or exceeds the length allowed by the system."));
	NEW_ERROR (1323, _T ("The folder path '[2]' contains words that are not valid in folder paths."));
	NEW_ERROR (1324, _T ("The folder path '[2]' contains an invalid character."));
	NEW_ERROR (1325, _T ("'[2]' is not a valid short file name."));
	NEW_ERROR (1326, _T ("Error getting file security: [3] GetLastError: [2]"));
	NEW_ERROR (1327, _T ("Invalid Drive: [2]"));
	NEW_ERROR (1328, _T ("Error applying patch to file [2].  It has probably been updated by other means, and can no longer be modified by this patch.  For more information contact your patch vendor.  {{System Error: [3]}}"));
	NEW_ERROR (1329, _T ("A file that is required cannot be installed because the cabinet file [2] is not digitally signed.  This may indicate that the cabinet file is corrupt."));
	NEW_ERROR (1330, _T ("A file that is required cannot be installed because the cabinet file [2] has an invalid digital signature.  This may indicate that the cabinet file is corrupt.{{  Error [3] was returned by WinVerifyTrust.}}"));
	NEW_ERROR (1331, _T ("Failed to correctly copy [2] file: CRC error."));
	NEW_ERROR (1332, _T ("Failed to correctly move [2] file: CRC error."));
	NEW_ERROR (1333, _T ("Failed to correctly patch [2] file: CRC error."));
	NEW_ERROR (1334, _T ("The file '[2]' cannot be installed because the file cannot be found in cabinet file '[3]'. This could indicate a network error, an error reading from the CD-ROM, or a problem with this package."));
	NEW_ERROR (1335, _T ("The cabinet file '[2]' required for this installation is corrupt and cannot be used. This could indicate a network error, an error reading from the CD-ROM, or a problem with this package."));
	NEW_ERROR (1336, _T ("There was an error creating a temporary file that is needed to complete this installation.{{  Folder: [3]. System error code: [2]}}"));
	NEW_ERROR (1401, _T ("Could not create key: [2]. {{ System error [3].}}  Verify that you have sufficient access to that key, or contact your support personnel. "));
	NEW_ERROR (1402, _T ("Could not open key: [2]. {{ System error [3].}}  Verify that you have sufficient access to that key, or contact your support personnel. "));
	NEW_ERROR (1403, _T ("Could not delete value [2] from key [3]. {{ System error [4].}}  Verify that you have sufficient access to that key, or contact your support personnel. "));
	NEW_ERROR (1404, _T ("Could not delete key [2]. {{ System error [3].}}  Verify that you have sufficient access to that key, or contact your support personnel. "));
	NEW_ERROR (1405, _T ("Could not read value [2] from key [3]. {{ System error [4].}}  Verify that you have sufficient access to that key, or contact your support personnel. "));
	NEW_ERROR (1406, _T ("Could not write value [2] to key [3]. {{ System error [4].}}  Verify that you have sufficient access to that key, or contact your support personnel."));
	NEW_ERROR (1407, _T ("Could not get value names for key [2]. {{ System error [3].}}  Verify that you have sufficient access to that key, or contact your support personnel."));
	NEW_ERROR (1408, _T ("Could not get sub key names for key [2]. {{ System error [3].}}  Verify that you have sufficient access to that key, or contact your support personnel."));
	NEW_ERROR (1409, _T ("Could not read security information for key [2]. {{ System error [3].}}  Verify that you have sufficient access to that key, or contact your support personnel."));
	NEW_ERROR (1410, _T ("Could not increase the available registry space. [2] KB of free registry space is required for the installation of this application."));
	NEW_ERROR (1500, _T ("Another installation is in progress. You must complete that installation before continuing this one."));
	NEW_ERROR (1501, _T ("Error accessing secured data. Please make sure the Windows Installer is configured properly and try the install again."));
	NEW_ERROR (1502, _T ("User '[2]' has previously initiated an install for product '[3]'.  That user will need to run that install again before they can use that product.  Your current install will now continue."));
	NEW_ERROR (1503, _T ("User '[2]' has previously initiated an install for product '[3]'.  That user will need to run that install again before they can use that product."));
	NEW_ERROR (1601, _T ("Out of disk space -- Volume: '[2]'; required space: [3] KB; available space: [4] KB.  Free some disk space and retry."));
	NEW_ERROR (1602, _T ("Are you sure you want to cancel?"));
	NEW_ERROR (1603, _T ("The file [2][3] is being held in use{ by the following process: Name: [4], Id: [5], Window Title: '[6]'}.  Close that application and retry."));
	NEW_ERROR (1604, _T ("The product '[2]' is already installed, preventing the installation of this product.  The two products are incompatible."));
	NEW_ERROR (1605, _T ("There is not enough disk space on the volume '[2]' to continue the install with recovery enabled. [3] KB are required, but only [4] KB are available. Click Ignore to continue the install without saving recovery information, click Retry to check for available space again, or click Cancel to quit the installation."));
	NEW_ERROR (1606, _T ("Could not access network location [2]."));
	NEW_ERROR (1607, _T ("The following applications should be closed before continuing the install:"));
	NEW_ERROR (1608, _T ("Could not find any previously installed compliant products on the machine for installing this product."));
	NEW_ERROR (1609, _T ("An error occurred while applying security settings. [2] is not a valid user or group. This could be a problem with the package, or a problem connecting to a domain controller on the network. Check your network connection and click Retry, or Cancel to end the install. {{Unable to locate the user's SID, system error [3]}}"));
	NEW_ERROR (1701, _T ("The key [2] is not valid.  Verify that you entered the correct key."));
	NEW_ERROR (1702, _T ("The installer must restart your system before configuration of [2] can continue.  Click Yes to restart now or No if you plan to manually restart later."));
	NEW_ERROR (1703, _T ("You must restart your system for the configuration changes made to [2] to take effect. Click Yes to restart now or No if you plan to manually restart later."));
	NEW_ERROR (1704, _T ("An installation for [2] is currently suspended.  You must undo the changes made by that installation to continue.  Do you want to undo those changes?"));
	NEW_ERROR (1705, _T ("A previous installation for this product is in progress.  You must undo the changes made by that installation to continue.  Do you want to undo those changes?"));
	NEW_ERROR (1706, _T ("An installation package for the product [2] cannot be found. Try the installation again using a valid copy of the installation package '[3]'."));
	NEW_ERROR (1707, _T ("Installation completed successfully."));
	NEW_ERROR (1708, _T ("Installation failed."));
	NEW_ERROR (1709, _T ("Product: [2] -- [3]"));
	NEW_ERROR (1710, _T ("You may either restore your computer to its previous state or continue the install later. Would you like to restore?"));
	NEW_ERROR (1711, _T ("An error occurred while writing installation information to disk.  Check to make sure enough disk space is available, and click Retry, or Cancel to end the install."));
	NEW_ERROR (1712, _T ("One or more of the files required to restore your computer to its previous state could not be found.  Restoration will not be possible."));
	NEW_ERROR (1713, _T ("[2] cannot install one of its required products. Contact your technical support group.  {{System Error: [3].}}"));
	NEW_ERROR (1714, _T ("The older version of [2] cannot be removed.  Contact your technical support group.  {{System Error [3].}}"));
	NEW_ERROR (1715, _T ("Installed [2]"));
	NEW_ERROR (1716, _T ("Configured [2]"));
	NEW_ERROR (1717, _T ("Removed [2]"));
	NEW_ERROR (1718, _T ("File [2] was rejected by digital signature policy."));
	NEW_ERROR (1719, _T ("The Windows Installer Service could not be accessed. This can occur if you are running Windows in safe mode, or if the Windows Installer is not correctly installed. Contact your support personnel for assistance."));
	NEW_ERROR (1720, _T ("There is a problem with this Windows Installer package. A script required for this install to complete could not be run. Contact your support personnel or package vendor.  {{Custom action [2] script error [3], [4]: [5] Line [6], Column [7], [8] }}"));
	NEW_ERROR (1721, _T ("There is a problem with this Windows Installer package. A program required for this install to complete could not be run. Contact your support personnel or package vendor. {{Action: [2], location: [3], command: [4] }}"));
	NEW_ERROR (1722, _T ("There is a problem with this Windows Installer package. A program run as part of the setup did not finish as expected. Contact your support personnel or package vendor.  {{Action [2], location: [3], command: [4] }}"));
	NEW_ERROR (1723, _T ("There is a problem with this Windows Installer package. A DLL required for this install to complete could not be run. Contact your support personnel or package vendor.  {{Action [2], entry: [3], library: [4] }}"));
	NEW_ERROR (1724, _T ("Removal completed successfully."));
	NEW_ERROR (1725, _T ("Removal failed."));
	NEW_ERROR (1726, _T ("Advertisement completed successfully."));
	NEW_ERROR (1727, _T ("Advertisement failed."));
	NEW_ERROR (1728, _T ("Configuration completed successfully."));
	NEW_ERROR (1729, _T ("Configuration failed."));
	NEW_ERROR (1730, _T ("You must be an Administrator to remove this application. To remove this application, you can log on as an Administrator, or contact your technical support group for assistance."));
	NEW_ERROR (1801, _T ("The path [2] is not valid.  Please specify a valid path."));
	NEW_ERROR (1802, _T ("Out of memory. Shut down other applications before retrying."));
	NEW_ERROR (1803, _T ("There is no disk in drive [2]. Please insert one and click Retry, or click Cancel to go back to the previously selected volume."));
	NEW_ERROR (1804, _T ("There is no disk in drive [2]. Please insert one and click Retry, or click Cancel to return to the browse dialog and select a different volume."));
	NEW_ERROR (1805, _T ("The folder [2] does not exist.  Please enter a path to an existing folder."));
	NEW_ERROR (1806, _T ("You have insufficient privileges to read this folder."));
	NEW_ERROR (1807, _T ("A valid destination folder for the install could not be determined."));
	NEW_ERROR (1901, _T ("Error attempting to read from the source install database: [2]."));
	NEW_ERROR (1902, _T ("Scheduling reboot operation: Renaming file [2] to [3]. Must reboot to complete operation."));
	NEW_ERROR (1903, _T ("Scheduling reboot operation: Deleting file [2]. Must reboot to complete operation."));
	NEW_ERROR (1904, _T ("Module [2] failed to register.  HRESULT [3].  Contact your support personnel."));
	NEW_ERROR (1905, _T ("Module [2] failed to unregister.  HRESULT [3].  Contact your support personnel."));
	NEW_ERROR (1906, _T ("Failed to cache package [2]. Error: [3]. Contact your support personnel."));
	NEW_ERROR (1907, _T ("Could not register font [2].  Verify that you have sufficient permissions to install fonts, and that the system supports this font."));
	NEW_ERROR (1908, _T ("Could not unregister font [2]. Verify that you that you have sufficient permissions to remove fonts."));
	NEW_ERROR (1909, _T ("Could not create Shortcut [2]. Verify that the destination folder exists and that you can access it."));
	NEW_ERROR (1910, _T ("Could not remove Shortcut [2]. Verify that the shortcut file exists and that you can access it."));
	NEW_ERROR (1911, _T ("Could not register type library for file [2].  Contact your support personnel."));
	NEW_ERROR (1912, _T ("Could not unregister type library for file [2].  Contact your support personnel."));
	NEW_ERROR (1913, _T ("Could not update the ini file [2][3].  Verify that the file exists and that you can access it."));
	NEW_ERROR (1914, _T ("Could not schedule file [2] to replace file [3] on reboot.  Verify that you have write permissions to file [3]."));
	NEW_ERROR (1915, _T ("Error removing ODBC driver manager, ODBC error [2]: [3]. Contact your support personnel."));
	NEW_ERROR (1916, _T ("Error installing ODBC driver manager, ODBC error [2]: [3]. Contact your support personnel."));
	NEW_ERROR (1917, _T ("Error removing ODBC driver: [4], ODBC error [2]: [3]. Verify that you have sufficient privileges to remove ODBC drivers."));
	NEW_ERROR (1918, _T ("Error installing ODBC driver: [4], ODBC error [2]: [3]. Verify that the file [4] exists and that you can access it."));
	NEW_ERROR (1919, _T ("Error configuring ODBC data source: [4], ODBC error [2]: [3]. Verify that the file [4] exists and that you can access it."));
	NEW_ERROR (1920, _T ("Service '[2]' ([3]) failed to start.  Verify that you have sufficient privileges to start system services."));
	NEW_ERROR (1921, _T ("Service '[2]' ([3]) could not be stopped.  Verify that you have sufficient privileges to stop system services."));
	NEW_ERROR (1922, _T ("Service '[2]' ([3]) could not be deleted.  Verify that you have sufficient privileges to remove system services."));
	NEW_ERROR (1923, _T ("Service '[2]' ([3]) could not be installed.  Verify that you have sufficient privileges to install system services."));
	NEW_ERROR (1924, _T ("Could not update environment variable '[2]'.  Verify that you have sufficient privileges to modify environment variables."));
	NEW_ERROR (1925, _T ("You do not have sufficient privileges to complete this installation for all users of the machine.  Log on as administrator and then retry this installation."));
	NEW_ERROR (1926, _T ("Could not set file security for file '[3]'. Error: [2].  Verify that you have sufficient privileges to modify the security permissions for this file."));
	NEW_ERROR (1927, _T ("Component Services (COM+ 1.0) are not installed on this computer.  This installation requires Component Services in order to complete successfully.  Component Services are available on Windows 2000."));
	NEW_ERROR (1928, _T ("Error registering COM+ Application.  Contact your support personnel for more information."));
	NEW_ERROR (1929, _T ("Error unregistering COM+ Application.  Contact your support personnel for more information."));
	NEW_ERROR (1930, _T ("The description for service '[2]' ([3]) could not be changed."));
	NEW_ERROR (1931, _T ("The Windows Installer service cannot update the system file [2] because the file is protected by Windows.  You may need to update your operating system for this program to work correctly. {{Package version: [3], OS Protected version: [4]}}"));
	NEW_ERROR (1932, _T ("The Windows Installer service cannot update the protected Windows file [2]. {{Package version: [3], OS Protected version: [4], SFP Error: [5]}}"));
	NEW_ERROR (1933, _T ("The Windows Installer service cannot update one or more protected Windows files. {{SFP Error: [2].  List of protected files:\r\n[3]}}"));
	NEW_ERROR (1934, _T ("User installations are disabled via policy on the machine."));
	NEW_ERROR (1935, _T ("An error occured during the installation of assembly component [2]. HRESULT: [3]. {{assembly interface: [4], function: [5], assembly name: [6]}}"));

#undef NEW_ERROR
#undef NEW_ERROR2

	return true;
}

bool AddTextStyles (CBicDatabase &db)
{
#define NEW_TEXT_STYLE(textStyle, faceName, size, color, styleBits);		\
	{\
		CBicTextStyle *ts = db.NewTextStyle (textStyle, faceName, size, color, styleBits);\
		if (ts == NULL) { return false; }\
	}

	NEW_TEXT_STYLE (_T ("DlgFont8"), _T ("Tahoma"), 8, 0, 0);
	NEW_TEXT_STYLE (_T ("DlgFontBold8"), _T ("Tahoma"), 8, 0, CBicTextStyle::bitBold);
	NEW_TEXT_STYLE (_T ("VerdanaBold13"), _T ("Verdana"), 13, 0, CBicTextStyle::bitBold);
	NEW_TEXT_STYLE (_T ("DlgFont8__UL"), _T ("Tahoma"), 8, 0, 0);

	return true;
}

bool AddUI (CBicDatabase &db, HINSTANCE instance, LPCTSTR fromDir, const bool readMePresent, const CBicTString &readMeFileName)
{
#define NEW_DIALOG(dialog, hCentering, vCentering, width, height, attributes, title);		\
	CBicDialog *dialog = db.NewDialog (hCentering, vCentering, width, height, attributes, title);\
	if (dialog == NULL) { return false; }

#define NEW_DIALOG2(dialog, dlgName, hCentering, vCentering, width, height, attributes, title);		\
	CBicDialog *dialog = db.NewDialog (dlgName, hCentering, vCentering, width, height, attributes, title);\
	if (dialog == NULL) { return false; }

#define NEW_CONTROL(control, dialog, type, x, y, width, height, attributes, property, text, help, tab);			\
	CBicControl *control = db.NewControl (type, x, y, width, height, attributes, property, text, help, tab);\
	if (control == NULL) { return false; }\
	if (!dialog->AddControl (control)) { return false; }

#define NEW_CONTROL2(control, ctrlName, dialog, type, x, y, width, height, attributes, property, text, help, tab);			\
	CBicControl *control = db.NewControl (ctrlName, type, x, y, width, height, attributes, property, text, help, tab);\
	if (control == NULL) { return false; }\
	if (!dialog->AddControl (control)) { return false; }

#define NEW_CONTROL_EVENT(control, event, argument, condition);			\
	{\
		CBicControlEvent *ctrlEvent = db.NewControlEvent (control, event, argument, condition);\
		if (ctrlEvent == NULL) { return false; }\
	}

#define NEW_CONTROL_CONDITION(control, action, condition);		\
	{\
		CBicControlCondition *ctrlCond = db.NewControlCondition (control, action, condition);\
		if (ctrlCond == NULL) { return false; }\
	}

#define NEW_EVENT_MAPPING(control, event, attribute);		\
	{\
		CBicEventMapping *map = db.NewEventMapping (control, event, attribute);\
		if (map == NULL) { return false; }\
	}

#define NEW_PROPERTY(property, value);			\
	{\
		CBicProperty *prop = db.NewProperty (property, value);\
		if (prop == NULL) { return false; }\
	}

#define NEW_PROPERTY2(prop, property, value);			\
	prop = db.NewProperty (property, value);\
	if (prop == NULL) { return false; }

#define NEW_RADIO_BUTTON_GROUP(group, property, value);		\
	CBicRadioButtonGroup *group = db.NewRadioButtonGroup (property, value);\
	if (group == NULL) { return false; }

#define NEW_RADIO_BUTTON(group, value, x, y, width, height, text, help);		\
	{\
		CBicRadioButton *radio = db.NewRadioButton (group, value, x, y, width, height, text, help);\
		if (radio == NULL) { return false; }\
	}

#define NEW_ICON(icon, property, binaries);		\
	icon = db.NewIcon (property, binaries, sizeof (binaries));\
	if (icon == NULL) { return false; }

	// Icons and Bitmaps
	CBicBinary *repairIcon;
	NEW_ICON (repairIcon, _T ("RepairIcon"), CBicBinary::_iconBinariesRepair);

	CBicBinary *removeIcon;
	NEW_ICON (removeIcon, _T ("RemoveIcon"), CBicBinary::_iconBinariesRemove);

	CBicBinary *dlgBmp;
	if (_taccess (_T ("background.bmp"), 04) == 0)
	{
		dlgBmp = db.NewIcon (_T ("DialogBitmap"), _T ("background.bmp"));
		if (dlgBmp == NULL) { return false; }
	}
	else
	{
		NEW_ICON (dlgBmp, _T ("DialogBitmap"), CBicBinary::_bmpBinariesDialog);
	}
	CBicBinary *bnrBmp;
	if (_taccess (_T ("banner.bmp"), 04) == 0)
	{
		bnrBmp = db.NewIcon (_T ("BannerBitmap"), _T ("banner.bmp"));
		if (bnrBmp == NULL) { return false; }
	}
	else
	{
		NEW_ICON (bnrBmp, _T ("BannerBitmap"), CBicBinary::_bmpBinariesBanner);
	}

	CBicBinary *newBmp;
	NEW_ICON (newBmp, _T ("NewBitmap"), CBicBinary::_bmpBinariesNew);

	CBicBinary *upBmp;
	NEW_ICON (upBmp, _T ("UpBitmap"), CBicBinary::_bmpBinariesUp);

	CBicBinary *infoBmp;
	NEW_ICON (infoBmp, _T ("InfoIcon"), CBicBinary::_bmpBinariesInfo);

	// Dialogs & Controls & ControlEvents & ControlConditions & EventMappings
	NEW_DIALOG (badSerialNumberDlg, 50, 50, 370, 270, CBicDialog::attrVisible | CBicDialog::attrModal, _T ("[SingleProductName] [Setup]"));
	NEW_DIALOG (welcomeDlg, 50, 50, 370, 270, CBicDialog::attrVisible | CBicDialog::attrModal, _T ("[SingleProductName] [Setup]"));
	NEW_DIALOG (prepareDlg, 50, 50, 370, 270, CBicDialog::attrVisible, _T ("[SingleProductName] [Setup]"));
	NEW_DIALOG (cancelDlg, 50, 10, 260, 85, CBicDialog::attrVisible | CBicDialog::attrModal, _T ("[SingleProductName] [Setup]"));
	NEW_DIALOG (waitForCostingDlg, 50, 10, 260, 85, CBicDialog::attrVisible | CBicDialog::attrModal, _T ("[SingleProductName] [Setup]"));
	NEW_DIALOG (verifyReadyDlg, 50, 50, 370, 270, CBicDialog::attrVisible | CBicDialog::attrModal | CBicDialog::attrTrackDiskSpace, _T ("[SingleProductName] [Setup]"));
	NEW_DIALOG (outOfRbDiskDlg, 50, 50, 370, 270, CBicDialog::attrVisible | CBicDialog::attrModal, _T ("[SingleProductName] [Setup]"));
	NEW_DIALOG (outOfDiskDlg, 50, 50, 370, 270, CBicDialog::attrVisible | CBicDialog::attrModal, _T ("[SingleProductName] [Setup]"));
	NEW_DIALOG (resumeDlg, 50, 50, 370, 270, CBicDialog::attrVisible | CBicDialog::attrModal, _T ("[SingleProductName] [Setup]"));
	NEW_DIALOG (maintenanceWelcomeDlg, 50, 50, 370, 270, CBicDialog::attrVisible | CBicDialog::attrModal, _T ("[SingleProductName] [Setup]"));
	NEW_DIALOG (progressDlg, 50, 50, 370, 270, CBicDialog::attrVisible, _T ("[SingleProductName] [Setup]"));
	NEW_DIALOG (maintenanceTypeDlg, 50, 50, 370, 270, CBicDialog::attrVisible | CBicDialog::attrModal, _T ("[SingleProductName] [Setup]"));
	NEW_DIALOG (verifyRepairDlg, 50, 50, 370, 270, CBicDialog::attrVisible | CBicDialog::attrModal | CBicDialog::attrTrackDiskSpace, _T ("[SingleProductName] [Setup]"));
	NEW_DIALOG (verifyRemoveDlg, 50, 50, 370, 270, CBicDialog::attrVisible | CBicDialog::attrModal | CBicDialog::attrTrackDiskSpace, _T ("[SingleProductName] [Setup]"));
	NEW_DIALOG (fatalErrorDlg, 50, 50, 370, 270, CBicDialog::attrVisible | CBicDialog::attrModal, _T ("[SingleProductName] [Setup]"));
	NEW_DIALOG (userExitDlg, 50, 50, 370, 270, CBicDialog::attrVisible | CBicDialog::attrModal, _T ("[SingleProductName] [Setup]"));
	NEW_DIALOG (exitDlg, 50, 50, 370, 270, CBicDialog::attrVisible | CBicDialog::attrModal, _T ("[SingleProductName] [Setup]"));
	NEW_DIALOG (errorDlg, 50, 10, 270, 105, CBicDialog::attrVisible | CBicDialog::attrModal | CBicDialog::attrError, _T ("Installer Information"));
	NEW_DIALOG2 (filesInUseDlg, _T ("FilesInUse"), 50, 50, 370, 270, CBicDialog::attrVisible | CBicDialog::attrModal | CBicDialog::attrKeepModeless, _T ("[SingleProductName] [Setup]"));
	NEW_DIALOG (noRegistryDlg, 50, 50, 370, 270, CBicDialog::attrVisible | CBicDialog::attrModal, _T ("[SingleProductName] [Setup]"));
	NEW_DIALOG (browseDlg, 50, 50, 370, 270, CBicDialog::attrVisible | CBicDialog::attrModal, _T ("[SingleProductName] [Setup]"));
	NEW_DIALOG (customizeDlg, 50, 50, 370, 270, CBicDialog::attrVisible | CBicDialog::attrModal | CBicDialog::attrTrackDiskSpace, _T ("[SingleProductName] [Setup]"));
	CBicDialog *licenceDlg = NULL;

	// Controls properties
	NEW_PROPERTY (_T ("ButtonText_Next"), _T ("&Next >"));
	NEW_PROPERTY (_T ("ButtonText_Cancel"), _T ("Cancel"));
	NEW_PROPERTY (_T ("ButtonText_Back"), _T ("< &Back"));
	NEW_PROPERTY (_T ("ButtonText_Return"), _T ("&Return"));
	NEW_PROPERTY (_T ("ButtonText_Install"), _T ("&Install"));
	NEW_PROPERTY (_T ("ButtonText_No"), _T ("&No"));
	NEW_PROPERTY (_T ("ButtonText_Yes"), _T ("&Yes"));
	NEW_PROPERTY (_T ("ButtonText_Repair"), _T ("Re&pair"));
	NEW_PROPERTY (_T ("ButtonText_Remove"), _T ("&Remove"));
	NEW_PROPERTY (_T ("ButtonText_Finish"), _T ("&Finish"));
	NEW_PROPERTY (_T ("ButtonText_Ignore"), _T ("&Ignore"));
	NEW_PROPERTY (_T ("ButtonText_OK"), _T ("OK"));
	NEW_PROPERTY (_T ("ButtonText_Retry"), _T ("&Retry"));
	NEW_PROPERTY (_T ("ButtonText_Exit"), _T ("&Exit"));
	NEW_PROPERTY (_T ("ButtonText_Reset"), _T ("&Reset"));
	NEW_PROPERTY (_T ("ButtonText_Browse"), _T ("Br&owse"));
	NEW_PROPERTY (_T ("Wizard"), _T ("Setup Wizard"));
	NEW_PROPERTY (_T ("Setup"), _T ("Setup"));
	NEW_PROPERTY (_T ("InstallMode"), _T ("Complete"));
	NEW_PROPERTY (_T ("Progress1"), _T ("Installing"));
	NEW_PROPERTY (_T ("Progress2"), _T ("installs"));
	NEW_PROPERTY (_T ("ErrorDialog"), errorDlg);
	NEW_PROPERTY (_T ("DefaultUIFont"), _T ("DlgFont8"));

	CBicProperty *showReadMe;
	CBicProperty *readMeViewer;
	CBicCustomAction *showReadMeAction;
	if (readMePresent)
	{
		NEW_PROPERTY (_T ("ButtonText_ShowReadMe"), _T ("Show ReadMe.txt"));
		NEW_PROPERTY2 (readMeViewer, _T ("ReadMeViewer"), _T ("notepad.exe"));
		showReadMe = db.NewProperty (_T ("ShowReadMe"), _T ("1"), true);
		if (showReadMe == NULL) { return false; }

		// CustomAction
		CBicTString buf;
		buf = _T ("[ADDONSATEASE]");
		buf += readMeFileName;
		showReadMeAction = db.NewCustomAction (
			50 | CBicCustomAction::typeAsync | CBicCustomAction::typeContinue, readMeViewer, buf);
		if (showReadMeAction == NULL) { return false; }
	}

	CBicProperty *fileInUseProcess = db.NewProperty (_T ("FileInUseProcess"), false);
	if (fileInUseProcess == NULL)
	{
		return false;
	}

	CBicProperty *browseProperty = db.NewProperty (_T ("_BrowseProperty"), false);
	if (browseProperty == NULL)
	{
		return false;
	}

	// FilesInUseDlg
	NEW_CONTROL (filesInUseRetry, filesInUseDlg, _T ("PushButton"), 304, 243, 56, 17,
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Retry]"), _T (""), true);
	filesInUseDlg->SetControlDefault (filesInUseRetry);
	filesInUseDlg->SetControlCancel (filesInUseRetry);
	NEW_CONTROL_EVENT (filesInUseRetry, _T ("EndDialog"), _T ("Retry"), _T ("1"));

	NEW_CONTROL (filesInUseBanner, filesInUseDlg, _T ("Bitmap"), 0, 0, 374, 44, 
		CBicControl::attrVisible, NULL, bnrBmp, _T (""), false);

	NEW_CONTROL (filesInUseIgnore, filesInUseDlg, _T ("PushButton"), 235, 243, 56, 17,
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Ignore]"), _T (""), true);
	NEW_CONTROL_EVENT (filesInUseIgnore, _T ("EndDialog"), _T ("Ignore"), _T ("1"));

	NEW_CONTROL (filesInUseExit, filesInUseDlg, _T ("PushButton"), 166, 243, 56, 17,
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Exit]"), _T (""), true);
	NEW_CONTROL_EVENT (filesInUseExit, _T ("EndDialog"), _T ("Exit"), _T ("1"));

	NEW_CONTROL (filesInUseBannerLine, filesInUseDlg, _T ("Line"), 0, 44, 374, 0,
		CBicControl::attrVisible, NULL, _T (""), _T (""), false);

	NEW_CONTROL (filesInUseBottomLine, filesInUseDlg, _T ("Line"), 0, 234, 374, 0,
		CBicControl::attrVisible, NULL, _T (""), _T (""), false);
	
	NEW_CONTROL (filesInUseDescription, filesInUseDlg, _T ("Text"), 20, 23, 280, 20,
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtFilesInUse3, _T (""), false);
	
	NEW_CONTROL (filesInUseList, filesInUseDlg, _T ("ListBox"), 20, 87, 330, 130,
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrSunken, 
		fileInUseProcess, _T (""), _T (""), false);

	NEW_CONTROL (filesInUseText, filesInUseDlg, _T ("Text"), 20, 55, 330, 30,
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, 
		dtFilesInUse1, _T (""), false);
	
	NEW_CONTROL (filesInUseTitle, filesInUseDlg, _T ("Text"), 15, 6, 200, 15, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtFilesInUse2, _T (""), false);

	// ErrorDlg
	NEW_CONTROL2 (errorText, _T ("ErrorText"), errorDlg, _T ("Text"), 48, 15, 205, 60, 
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("Information text"), _T (""), false);

	NEW_CONTROL2 (errorIcon, _T ("ErrorIcon"), errorDlg, _T ("Icon"), 15, 15, 24, 24, 
		CBicControl::attrVisible | CBicControl::attrFixedSize | CBicControl::attrIconSize32, 
		NULL, _T ("[InfoIcon]"), _T ("Information icon|"), false);

	NEW_CONTROL2 (errorA, _T ("A"), errorDlg, _T ("PushButton"), 100, 80, 56, 17,
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Cancel]"), _T (""), false);
	NEW_CONTROL_EVENT (errorA, _T ("EndDialog"), _T ("ErrorAbort"), _T ("1"));

	NEW_CONTROL2 (errorC, _T ("C"), errorDlg, _T ("PushButton"), 100, 80, 56, 17, 
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Cancel]"), _T (""), false);
	NEW_CONTROL_EVENT (errorC, _T ("EndDialog"), _T ("ErrorCancel"), _T ("1"));

	NEW_CONTROL2 (errorI, _T ("I"), errorDlg, _T ("PushButton"), 100, 80, 56, 17, 
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Ignore]"), _T (""), false);
	NEW_CONTROL_EVENT (errorI, _T ("EndDialog"), _T ("ErrorIgnore"), _T ("1"));

	NEW_CONTROL2 (errorN, _T ("N"), errorDlg, _T ("PushButton"), 100, 80, 56, 17, 
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_No]"), _T (""), false);
	NEW_CONTROL_EVENT (errorN, _T ("EndDialog"), _T ("ErrorNo"), _T ("1"));

	NEW_CONTROL2 (errorO, _T ("O"), errorDlg, _T ("PushButton"), 100, 80, 56, 17, 
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_OK]"), _T (""), false);
	NEW_CONTROL_EVENT (errorO, _T ("EndDialog"), _T ("ErrorOk"), _T ("1"));

	NEW_CONTROL2 (errorR, _T ("R"), errorDlg, _T ("PushButton"), 100, 80, 56, 17, 
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Retry]"), _T (""), false);
	NEW_CONTROL_EVENT (errorR, _T ("EndDialog"), _T ("ErrorRetry"), _T ("1"));

	NEW_CONTROL2 (errorY, _T ("Y"), errorDlg, _T ("PushButton"), 100, 80, 56, 17, 
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Yes]"), _T (""), false);
	NEW_CONTROL_EVENT (errorY, _T ("EndDialog"), _T ("ErrorYes"), _T ("1"));

	// CancelDlg
	NEW_CONTROL (cancelNo, cancelDlg, _T ("PushButton"), 132, 57, 56, 17,
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_No]"), _T (""), true);
	cancelDlg->SetControlCancel (cancelNo);
	cancelDlg->SetControlDefault (cancelNo);
	NEW_CONTROL_EVENT (cancelNo, _T ("EndDialog"), _T ("Return"), _T ("1"));
	
	NEW_CONTROL (cancelYes, cancelDlg, _T ("PushButton"), 72, 57, 56, 17,
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Yes]"), _T (""), true);
	NEW_CONTROL_EVENT (cancelYes, _T ("EndDialog"), _T ("Exit"), _T ("1"));

	NEW_CONTROL (cancelText, cancelDlg, _T ("Text"), 48, 15, 194, 30,
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, 
		dtCancel1, _T (""), false);

	NEW_CONTROL (cancelIcon, cancelDlg, _T ("Icon"), 15, 15, 24, 24, 
		CBicControl::attrVisible | CBicControl::attrFixedSize | CBicControl::attrIconSize32,
		NULL, _T ("[InfoIcon]"), _T ("Information icon|"), false);

	// OutOfDiskDlg
	NEW_CONTROL (outOfDiskOk, outOfDiskDlg, _T ("PushButton"), 304, 243, 56, 17,
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_OK]"), _T (""), true);
	outOfDiskDlg->SetControlCancel (outOfDiskOk);
	outOfDiskDlg->SetControlDefault (outOfDiskOk);
	NEW_CONTROL_EVENT (outOfDiskOk, _T ("EndDialog"), _T ("Return"), _T ("1"));

	NEW_CONTROL (outOfDiskBanner, outOfDiskDlg, _T ("Bitmap"), 0, 0, 374, 44, 
		CBicControl::attrVisible, NULL, bnrBmp, _T (""), false);

	NEW_CONTROL (outOfDiskBannerLine, outOfDiskDlg, _T ("Line"), 0, 44, 374, 0,
		CBicControl::attrVisible, NULL, _T (""), _T (""), false);
	
	NEW_CONTROL (outOfDiskBottomLine, outOfDiskDlg, _T ("Line"), 0, 234, 374, 0,
		CBicControl::attrVisible, NULL, _T (""), _T (""), false);

	NEW_CONTROL (outOfDiskDescription, outOfDiskDlg, _T ("Text"), 20, 20, 280, 20,
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtOutOfDisk3, _T (""), false);

	NEW_CONTROL (outOfDiskText, outOfDiskDlg, _T ("Text"), 20, 53, 330, 40,
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, 
		dtOutOfDisk1, _T (""), false);

	NEW_CONTROL (outOfDiskTitle, outOfDiskDlg, _T ("Text"), 15, 6, 200, 15,
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtOutOfDisk2, _T (""), false);

	NEW_CONTROL (outOfDiskVolumeList, outOfDiskDlg, _T ("VolumeCostList"), 20, 100, 330, 120,
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrSunken | CBicControl::attrFixedVolume | 
		CBicControl::attrRemoteVolume, NULL, _T ("{120}{70}{70}{70}{70}"), _T (""), false);
	
	// PrepareDlg
	NEW_CONTROL (prepareCancel, prepareDlg, _T ("PushButton"), 304, 243, 56, 17, 
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Cancel]"), _T (""), true);
	prepareDlg->SetControlDefault (prepareCancel);
	prepareDlg->SetControlCancel (prepareCancel);
	NEW_CONTROL_EVENT (prepareCancel, _T ("SpawnDialog"), cancelDlg, _T ("1"));

	NEW_CONTROL (prepareDlgBitmap, prepareDlg, _T ("Bitmap"), 0, 0, 115, 234, 
		CBicControl::attrVisible, NULL, dlgBmp, _T (""), false);

	NEW_CONTROL (prepareActionData, prepareDlg, _T ("Text"), 135, 125, 220, 30, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, _T (""), _T (""), false);
	NEW_EVENT_MAPPING (prepareActionData, _T ("ActionData"), _T ("Text"));

	NEW_CONTROL (prepareActionText, prepareDlg, _T ("Text"), 135, 100, 220, 20, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, _T (""), _T (""), false);
	NEW_EVENT_MAPPING (prepareActionText, _T ("ActionText"), _T ("Text"));

	NEW_CONTROL (prepareBack, prepareDlg, _T ("PushButton"), 180, 243, 56, 17, 
		CBicControl::attrVisible, NULL, _T ("[ButtonText_Back]"), _T (""), false);

	NEW_CONTROL (prepareBottomLine, prepareDlg, _T ("Line"), 0, 234, 374, 0, CBicControl::attrVisible, NULL, _T (""), _T (""), false);

	NEW_CONTROL (prepareDescription, prepareDlg, _T ("Text"), 135, 70, 220, 20, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtPrepare1, _T (""), false);

	NEW_CONTROL (prepareNext, prepareDlg, _T ("PushButton"), 236, 243, 56, 17, CBicControl::attrVisible, 
		NULL, _T ("[ButtonText_Next]"), _T (""), false);

	NEW_CONTROL (prepareTitle, prepareDlg, _T ("Text"), 135, 20, 220, 60,
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtPrepare2, _T (""), false);

	// BadSerialNumberDlg
	NEW_CONTROL (badSerialNumberNext, badSerialNumberDlg, _T ("PushButton"), 236, 243, 56, 17, 
		CBicControl::attrVisible, NULL, _T ("[ButtonText_Next]"), _T (""), true);

	NEW_CONTROL (badSerialNumberDlgBitmap, badSerialNumberDlg, _T ("Bitmap"), 0, 0, 115, 234, 
		CBicControl::attrVisible, NULL, dlgBmp, _T (""), false);

	NEW_CONTROL (badSerialNumberCancel, badSerialNumberDlg, _T ("PushButton"), 304, 243, 56, 17, 
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Cancel]"), _T (""), true);
	badSerialNumberDlg->SetControlDefault (badSerialNumberCancel);
	badSerialNumberDlg->SetControlCancel (badSerialNumberCancel);
	NEW_CONTROL_EVENT (badSerialNumberCancel, _T ("EndDialog"), _T ("Exit"), _T ("1"));
	
	NEW_CONTROL (badSerialNumberBack, badSerialNumberDlg, _T ("PushButton"), 180, 243, 56, 17,
		CBicControl::attrVisible, NULL, _T ("[ButtonText_Back]"), _T (""), true);

	NEW_CONTROL (badSerialNumberBottomLine, badSerialNumberDlg, _T ("Line"), 0, 234, 374, 0, 
		CBicControl::attrVisible, NULL, _T (""), _T (""), false);

	NEW_CONTROL (badSerialNumberDescription, badSerialNumberDlg, _T ("Text"), 135, 70, 220, 60, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtBadSerialNumber1, _T (""), false);

	NEW_CONTROL (badSerialNumberTitle, badSerialNumberDlg, _T ("Text"), 135, 20, 220, 60, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtBadSerialNumber2, _T (""), false);

	// WelcomeDlg
	NEW_CONTROL (welcomeNext, welcomeDlg, _T ("PushButton"), 236, 243, 56, 17, 
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Next]"), _T (""), true);
	welcomeDlg->SetControlDefault (welcomeNext);
	CBicTString eulaText;
	bool eulaTag;
	{
		CBicTString path (fromDir);
		path += _T ("eula.rtf");
		CBicFileReader eulaFile;
		eulaTag = eulaFile.Open (path) && eulaFile.Read (eulaText);
		if (eulaTag)
		{
			licenceDlg = db.NewDialog (50, 50, 370, 270, CBicDialog::attrVisible | CBicDialog::attrModal, dtLicence5);
			if (licenceDlg == NULL) { return false; }
			NEW_CONTROL_EVENT (welcomeNext, _T ("NewDialog"), licenceDlg, _T ("1"));
		}
		else
		{
			NEW_CONTROL_EVENT (welcomeNext, _T ("SpawnWaitDialog"), waitForCostingDlg, _T ("CostingComplete = 1"));
			NEW_CONTROL_EVENT (welcomeNext, _T ("SetInstallLevel"), _T ("1000"), _T ("1"));
			NEW_CONTROL_EVENT (welcomeNext, _T ("NewDialog"), customizeDlg, _T ("1"));
		}
	}

	NEW_CONTROL (welcomeDlgBitmap, welcomeDlg, _T ("Bitmap"), 0, 0, 115, 234, 
		CBicControl::attrVisible, NULL, dlgBmp, _T (""), false);

	NEW_CONTROL (welcomeCancel, welcomeDlg, _T ("PushButton"), 304, 243, 56, 17, 
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Cancel]"), _T (""), true);
	welcomeDlg->SetControlCancel (welcomeCancel);
	NEW_CONTROL_EVENT (welcomeCancel, _T ("SpawnDialog"), cancelDlg, _T ("1"));
	
	NEW_CONTROL (welcomeBack, welcomeDlg, _T ("PushButton"), 180, 243, 56, 17,
		CBicControl::attrVisible, NULL, _T ("[ButtonText_Back]"), _T (""), true);

	NEW_CONTROL (welcomeBottomLine, welcomeDlg, _T ("Line"), 0, 234, 374, 0, 
		CBicControl::attrVisible, NULL, _T (""), _T (""), false);

	NEW_CONTROL (welcomeDescription, welcomeDlg, _T ("Text"), 135, 70, 220, 30, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtWelcome1, _T (""), false);

	NEW_CONTROL (welcomeTitle, welcomeDlg, _T ("Text"), 135, 20, 220, 60, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtWelcome2, _T (""), false);

	// LicenceDlg
	if (eulaTag)
	{
		NEW_RADIO_BUTTON_GROUP (agreeGroup, _T ("IAgree"), _T ("No"));
		NEW_RADIO_BUTTON (agreeGroup, _T ("Yes"), 5, 0, 250, 15, dtLicence1, _T (""));
		NEW_RADIO_BUTTON (agreeGroup, _T ("No"), 5, 20, 250, 15, dtLicence2, _T (""));
		NEW_CONTROL (licenceButtons, licenceDlg, _T ("RadioButtonGroup"), 20, 187, 330, 40, 
			CBicControl::attrVisible | CBicControl::attrEnabled, agreeGroup, _T (""), _T (""), true);

		NEW_CONTROL (licenceBanner, licenceDlg, _T ("Bitmap"), 0, 0, 374, 44, 
			CBicControl::attrVisible, NULL, bnrBmp, _T (""), false);

		NEW_CONTROL (licenceBack, licenceDlg, _T ("PushButton"), 180, 243, 56, 17, 
			CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Back]"), _T (""), true);
		NEW_CONTROL_EVENT (licenceBack, _T ("NewDialog"), welcomeDlg, _T ("1"));

		NEW_CONTROL (licenceNext, licenceDlg, _T ("PushButton"), 236, 243, 56, 17, 
			CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Next]"), _T (""), true);
		licenceDlg->SetControlDefault (licenceNext);
		NEW_CONTROL_EVENT (licenceNext, _T ("SpawnWaitDialog"), waitForCostingDlg, _T ("CostingComplete = 1"));
		NEW_CONTROL_EVENT (licenceNext, _T ("SetInstallLevel"), _T ("1000"), _T ("1"));
		NEW_CONTROL_EVENT (licenceNext, _T ("NewDialog"), customizeDlg, _T ("1"));
		NEW_CONTROL_CONDITION (licenceNext, _T ("Disable"), _T ("IAgree <> \"Yes\""));
		NEW_CONTROL_CONDITION (licenceNext, _T ("Enable"), _T ("IAgree = \"Yes\""));

		NEW_CONTROL (licenceCancel, licenceDlg, _T ("PushButton"), 304, 243, 56, 17,
			CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Cancel]"), _T (""), true);
		licenceDlg->SetControlCancel (licenceCancel);
		NEW_CONTROL_EVENT (licenceCancel, _T ("SpawnDialog"), cancelDlg, _T ("1"));

		/*
		CBicTString eulaRtf (_T ("{\\rtf1\\ansi\\ansicpg1252\\deff0\\deftab720{\\fonttbl{\\f0\\froman\\fprq2 Times New Roman;}}"
			"{\\colortbl\\red0\\green0\\blue0;} \\deflang1033\\horzdoc{\\*\\fchars }{\\*\\lchars }\\pard\\plain\\f0\\fs20 "));
		eulaRtf += eulaText;
		eulaRtf += _T ("\\par }");
		*/
		CBicTString eulaRtf (eulaText);
		NEW_CONTROL (licenceAgreement, licenceDlg, _T ("ScrollableText"), 20, 60, 330, 120, 
			CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrSunken, NULL, eulaRtf, _T (""), true);

		NEW_CONTROL (licenceBannerLine, licenceDlg, _T ("Line"), 0, 44, 374, 0, CBicControl::attrVisible, NULL, _T (""), _T (""), false);
		
		NEW_CONTROL (licenceBottomLine, licenceDlg, _T ("Line"), 0, 234, 374, 0, CBicControl::attrVisible, NULL, _T (""), _T (""), false);
		
		NEW_CONTROL (licenceDescription, licenceDlg, _T ("Text"), 25, 23, 280, 15, 
			CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
			NULL, dtLicence3, _T (""), false);

		NEW_CONTROL (licenceTitle, licenceDlg, _T ("Text"), 15, 6, 200, 15, 
			CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
			NULL, dtLicence4, _T (""), false);
	}

	// WaitForCostingDlg
	NEW_CONTROL (waitForCostingReturn, waitForCostingDlg, _T ("PushButton"), 102, 57, 56, 17, 
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Return]"), _T (""), true);
	waitForCostingDlg->SetControlDefault (waitForCostingReturn);
	waitForCostingDlg->SetControlCancel (waitForCostingReturn);
	NEW_CONTROL_EVENT (waitForCostingReturn, _T ("EndDialog"), _T ("Exit"), _T ("1"));

	NEW_CONTROL (waitForCostingText, waitForCostingDlg, _T ("Text"), 48, 15, 194, 30,
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, 
		dtWaitForCosting1, _T (""), false);

	// VerifyReadyDlg
	NEW_CONTROL (verifyReadyInstall, verifyReadyDlg, _T ("PushButton"), 236, 243, 56, 17, 
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Install]"), _T (""), true);
	verifyReadyDlg->SetControlDefault (verifyReadyInstall);
	NEW_CONTROL_EVENT (verifyReadyInstall, _T ("EndDialog"), _T ("Return"), _T ("OutOfDiskSpace <> 1"));
	NEW_CONTROL_EVENT (verifyReadyInstall, _T ("SpawnDialog"), outOfRbDiskDlg, _T ("OutOfDiskSpace = 1 AND OutOfNoRbDiskSpace = 0 AND (PROMPTROLLBACKCOST=\"P\" OR NOT PROMPTROLLBACKCOST)"));
	NEW_CONTROL_EVENT (verifyReadyInstall, _T ("EndDialog"), _T ("Return"), _T ("OutOfDiskSpace = 1 AND OutOfNoRbDiskSpace = 0 AND PROMPTROLLBACKCOST=\"D\""));
	NEW_CONTROL_EVENT (verifyReadyInstall, _T ("EnableRollback"), _T ("False"), _T ("OutOfDiskSpace = 1 AND OutOfNoRbDiskSpace = 0 AND PROMPTROLLBACKCOST=\"D\""));
	NEW_CONTROL_EVENT (verifyReadyInstall, _T ("SpawnDialog"), outOfDiskDlg, _T ("(OutOfDiskSpace = 1 AND OutOfNoRbDiskSpace = 1) OR (OutOfDiskSpace = 1 AND PROMPTROLLBACKCOST=\"F\")"));

	NEW_CONTROL (verifyReadyBanner, verifyReadyDlg, _T ("Bitmap"), 0, 0, 374, 44, 
		CBicControl::attrVisible, NULL, bnrBmp, _T (""), false);

	NEW_CONTROL (verifyReadyCancel, verifyReadyDlg, _T ("PushButton"), 304, 243, 56, 17, 
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Cancel]"), _T (""), true);
	verifyReadyDlg->SetControlCancel (verifyReadyCancel);
	NEW_CONTROL_EVENT (verifyReadyCancel, _T ("SpawnDialog"), cancelDlg, _T ("1"));

	NEW_CONTROL (verifyReadyBack, verifyReadyDlg, _T ("PushButton"), 180, 243, 56, 17,
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Back]"), _T (""), true);
	NEW_CONTROL_EVENT (verifyReadyBack, _T ("NewDialog"), maintenanceTypeDlg, _T("InstallMode = \"Repair\""));
	NEW_CONTROL_EVENT (verifyReadyBack, _T ("NewDialog"), customizeDlg, _T ("InstallMode = \"Complete\""));

	NEW_CONTROL (verifyReadyText, verifyReadyDlg, _T ("Text"), 25, 70, 320, 30, 
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, 
		dtVerifyReady1, _T (""), false);

	NEW_CONTROL (verifyReadyBannerLine, verifyReadyDlg, _T ("Line"), 0, 44, 374, 0, CBicControl::attrVisible, 
		NULL, _T (""), _T (""), false);

	NEW_CONTROL (verifyReadyBottomLine, verifyReadyDlg, _T ("Line"), 0, 234, 374, 0, CBicControl::attrVisible, 
		NULL, _T (""), _T (""), false);

	NEW_CONTROL (verifyReadyDescription, verifyReadyDlg, _T ("Text"), 25, 23, 280, 15, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtVerifyReady2, _T (""), false);

	NEW_CONTROL (verifyReadyTitle, verifyReadyDlg, _T ("Text"), 15, 6, 200, 15, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtVerifyReady3, _T (""), false);

	// OutOfRbDiskDlg
	NEW_CONTROL (outOfRbDiskNo, outOfRbDiskDlg, _T ("PushButton"), 304, 243, 56, 17, 
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_No]"), _T (""), true);
	outOfRbDiskDlg->SetControlDefault (outOfRbDiskNo);
	outOfRbDiskDlg->SetControlCancel (outOfRbDiskNo);
	NEW_CONTROL_EVENT (outOfRbDiskNo, _T ("EndDialog"), _T ("Return"), _T ("1"));

	NEW_CONTROL (outOfRbDiskBanner, outOfRbDiskDlg, _T ("Bitmap"), 0, 0, 374, 44, 
		CBicControl::attrVisible, NULL, bnrBmp, _T (""), false);

	NEW_CONTROL (outOfRbDiskYes, outOfRbDiskDlg, _T ("PushButton"), 240, 243, 56, 17,
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Yes]"), _T (""), true);
	NEW_CONTROL_EVENT (outOfRbDiskYes, _T ("EnableRollback"), _T ("False"), _T ("1"));
	NEW_CONTROL_EVENT (outOfRbDiskYes, _T ("EndDialog"), _T ("Return"), _T ("1"));

	NEW_CONTROL (outOfRbDiskBannerLine, outOfRbDiskDlg, _T ("Line"), 0, 44, 374, 0,
		CBicControl::attrVisible, NULL, _T (""), _T (""), false);

	NEW_CONTROL (outOfRbDiskBottomLine, outOfRbDiskDlg, _T ("Line"), 0, 234, 374, 0, 
		CBicControl::attrVisible, NULL, _T (""), _T (""), false);

	NEW_CONTROL (outOfRbDiskDescription, outOfRbDiskDlg, _T ("Text"), 20, 20, 280, 20, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtOutOfRbDisk1, _T (""), false);

	NEW_CONTROL (outOfRbDiskText, outOfRbDiskDlg, _T ("Text"), 20, 53, 330, 40,  
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, 
		dtOutOfRbDisk2, _T (""), false);

	NEW_CONTROL (outOfRbDiskText2, outOfRbDiskDlg, _T ("Text"), 20, 94, 330, 40,
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, 
		dtOutOfRbDisk3, _T (""), false);

	NEW_CONTROL (outOfRbDiskTitle, outOfRbDiskDlg, _T ("Text"), 15, 6, 200, 15, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtOutOfRbDisk4, _T (""), false);

	NEW_CONTROL (outOfRbDiskVolumeList, outOfRbDiskDlg, _T ("VolumeCostList"), 20, 140, 330, 80, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrSunken | CBicControl::attrFixedVolume | 
		CBicControl::attrRemoteVolume | CBicControl::ctrlShowRollbackCost, NULL, _T ("{120}{70}{70}{70}{70}"), _T (""), false);

	// ResumeDlg
	NEW_CONTROL (resumeInstall, resumeDlg, _T ("PushButton"), 236, 243, 56, 17,
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Install]"), _T (""), true);
	resumeDlg->SetControlDefault (resumeInstall);
	NEW_CONTROL_EVENT (resumeInstall, _T ("SpawnWaitDialog"), waitForCostingDlg, _T ("CostingComplete = 1"));
	NEW_CONTROL_EVENT (resumeInstall, _T ("EndDialog"), _T ("Return"), _T ("OutOfDiskSpace <> 1"));
	NEW_CONTROL_EVENT (resumeInstall, _T ("SpawnDialog"), outOfRbDiskDlg, _T ("OutOfDiskSpace = 1 AND OutOfNoRbDiskSpace = 0 AND (PROMPTROLLBACKCOST=\"P\" OR NOT PROMPTROLLBACKCOST)"));
	NEW_CONTROL_EVENT (resumeInstall, _T ("EndDialog"), _T ("Return"), _T ("OutOfDiskSpace = 1 AND OutOfNoRbDiskSpace = 0 AND PROMPTROLLBACKCOST=\"D\""));
	NEW_CONTROL_EVENT (resumeInstall, _T ("EnableRollback"), _T ("False"), _T ("OutOfDiskSpace = 1 AND OutOfNoRbDiskSpace = 0 AND PROMPTROLLBACKCOST=\"D\""));
	NEW_CONTROL_EVENT (resumeInstall, _T ("SpawnDialog"), outOfDiskDlg, _T ("(OutOfDiskSpace = 1 AND OutOfNoRbDiskSpace = 1) OR (OutOfDiskSpace = 1 AND PROMPTROLLBACKCOST=\"F\")"));

	NEW_CONTROL (resumeDlgBitmap, resumeDlg, _T ("Bitmap"), 0, 0, 115, 234, 
		CBicControl::attrVisible, NULL, dlgBmp, _T (""), false);

	NEW_CONTROL (resumeCancel, resumeDlg, _T ("PushButton"), 304, 243, 56, 17,
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Cancel]"), _T (""), true);
	resumeDlg->SetControlCancel (resumeCancel);
	NEW_CONTROL_EVENT (resumeCancel, _T ("SpawnDialog"), cancelDlg, _T ("1"));

	NEW_CONTROL (resumeBack, resumeDlg, _T ("PushButton"), 180, 243, 56, 17, CBicControl::attrVisible, NULL,
		_T ("[ButtonText_Back]"), _T (""), true);

	NEW_CONTROL (resumeBottomLine, resumeDlg, _T ("Line"), 0, 234, 374, 0, CBicControl::attrVisible, 
		NULL, _T (""), _T (""), false);

	NEW_CONTROL (resumeDescription, resumeDlg, _T ("Text"), 135, 70, 220, 30, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtResume1, _T (""), false);

	NEW_CONTROL (resumeTitle, resumeDlg, _T ("Text"), 135, 20, 220, 60, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtResume2, _T (""), false);

	// MaintenanceWelcomeDlg
	NEW_CONTROL (maintenanceWelcomeNext, maintenanceWelcomeDlg, _T ("PushButton"), 236, 243, 56, 17,
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Next]"), _T (""), true);
	maintenanceWelcomeDlg->SetControlDefault (maintenanceWelcomeNext);
	NEW_CONTROL_EVENT (maintenanceWelcomeNext, _T ("SpawnWaitDialog"), waitForCostingDlg, _T ("CostingComplete = 1"));
	NEW_CONTROL_EVENT (maintenanceWelcomeNext, _T ("NewDialog"), maintenanceTypeDlg, _T ("1"));

	NEW_CONTROL (maintenanceWelcomeDlgBitmap, maintenanceWelcomeDlg, _T ("Bitmap"), 0, 0, 115, 234, 
		CBicControl::attrVisible, NULL, dlgBmp, _T (""), false);

	NEW_CONTROL (maintenanceWelcomeCancel, maintenanceWelcomeDlg, _T ("PushButton"), 304, 243, 56, 17,
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Cancel]"), _T (""), true);
	maintenanceWelcomeDlg->SetControlCancel (maintenanceWelcomeCancel);
	NEW_CONTROL_EVENT (maintenanceWelcomeCancel, _T ("SpawnDialog"), cancelDlg, _T ("1"));

	NEW_CONTROL (maintenanceWelcomeBack, maintenanceWelcomeDlg, _T ("PushButton"), 180, 243, 56, 17,
		CBicControl::attrVisible, NULL, _T ("[ButtonText_Back]"), _T (""), true);

	NEW_CONTROL (maintenaceWelcomeBottomLine, maintenanceWelcomeDlg, _T ("Line"), 0, 234, 374, 0, 
		CBicControl::attrVisible, NULL, _T (""), _T (""), false);

	NEW_CONTROL (maintenanceWelcomeDescription, maintenanceWelcomeDlg, _T ("Text"), 135, 70, 220, 60, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtMaintenanceWelcome1, _T (""), false);

	NEW_CONTROL (maintenanceWelcomeTitle, maintenanceWelcomeDlg, _T ("Text"), 135, 20, 220, 60,
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtMaintenanceWelcome2, _T (""), false);

	// MaintenanceTypeDlg
	NEW_CONTROL (maintenanceTypeRepairLabel, maintenanceTypeDlg, _T ("Text"), 105, 65, 100, 10, 
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("{\\DlgFontBold8}Re&pair"), _T (""), true);

	NEW_CONTROL (maintenanceTypeRepairButton, maintenanceTypeDlg, _T ("PushButton"), 50, 65, 38, 38, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrIcon | CBicControl::attrIconSize32 | 
		CBicControl::attrFixedSize, NULL, repairIcon, _T ("Repair Installation|"), true);
	maintenanceTypeDlg->SetControlDefault (maintenanceTypeRepairButton);
	maintenanceTypeDlg->SetControlFirst (maintenanceTypeRepairButton);
	NEW_CONTROL_EVENT (maintenanceTypeRepairButton, _T ("[InstallMode]"), _T ("Repair"), _T ("1"));
	NEW_CONTROL_EVENT (maintenanceTypeRepairButton, _T ("[Progress1]"), _T ("Repairing"), _T ("1"));
	NEW_CONTROL_EVENT (maintenanceTypeRepairButton, _T ("[Progress2]"), _T ("repaires"), _T ("1"));
	NEW_CONTROL_EVENT (maintenanceTypeRepairButton, _T ("NewDialog"), verifyRepairDlg, _T ("1"));

	NEW_CONTROL (maintenanceTypeRepairText, maintenanceTypeDlg, _T ("Text"), 105, 78, 230, 30, 
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, 
		dtMaintenaceType1, _T (""), false);

	NEW_CONTROL (maintenanceTypeRemoveLabel, maintenanceTypeDlg, _T ("Text"), 105, 114, 100, 10, 
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("{\\DlgFontBold8}&Remove"), _T (""), true);

	NEW_CONTROL (maintenanceTypeRemoveButton, maintenanceTypeDlg, _T ("PushButton"), 50, 114, 38, 38,
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrIcon | CBicControl::attrIconSize32 | 
		CBicControl::attrFixedSize, NULL, removeIcon, _T ("Remove Installation|"), true);
	NEW_CONTROL_EVENT (maintenanceTypeRemoveButton, _T ("[InstallMode]"), _T ("Remove"), _T ("1"));
	NEW_CONTROL_EVENT (maintenanceTypeRemoveButton, _T ("[Progress1]"), _T ("Removing"), _T ("1"));
	NEW_CONTROL_EVENT (maintenanceTypeRemoveButton, _T ("[Progress2]"), _T ("removes"), _T ("1"));
	NEW_CONTROL_EVENT (maintenanceTypeRemoveButton, _T ("NewDialog"), verifyRemoveDlg, _T ("1"));

	NEW_CONTROL (maintenanceTypeRemoveText, maintenanceTypeDlg, _T ("Text"), 105, 127, 230, 20, 
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, dtMaintenaceType2, 
		_T (""), false);
	
	NEW_CONTROL (maintenanceTypeBanner, maintenanceTypeDlg, _T ("Bitmap"), 0, 0, 374, 44, 
		CBicControl::attrVisible, NULL, bnrBmp, _T (""), false);

	NEW_CONTROL (maintenanceTypeBack, maintenanceTypeDlg, _T ("PushButton"), 180, 243, 56, 17,
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Back]"), _T (""), true);
	NEW_CONTROL_EVENT (maintenanceTypeBack, _T ("NewDialog"), maintenanceWelcomeDlg, _T ("1"));

	NEW_CONTROL (maintenanceTypeNext, maintenanceTypeDlg, _T ("PushButton"), 236, 243, 56, 17,
		CBicControl::attrVisible, NULL, _T ("[ButtonText_Next]"), _T (""), true);
	
	NEW_CONTROL (maintenanceTypeCancel, maintenanceTypeDlg, _T ("PushButton"), 304, 243, 56, 17,
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Cancel]"), _T (""), true);
	maintenanceTypeDlg->SetControlCancel (maintenanceTypeCancel);
	NEW_CONTROL_EVENT (maintenanceTypeCancel, _T ("SpawnDialog"), cancelDlg, _T ("1"));

	NEW_CONTROL (maintenanceTypeBannerLine, maintenanceTypeDlg, _T ("Line"), 0, 44, 374, 0, CBicControl::attrVisible, 
		NULL, _T (""), _T (""), false);

	NEW_CONTROL (maintenanceTypeBottomLine, maintenanceTypeDlg, _T ("Line"), 0, 234, 374, 0, CBicControl::attrVisible,
		NULL, _T (""), _T (""), false);

	NEW_CONTROL (maintenanceTypeDescription, maintenanceTypeDlg, _T ("Text"), 25, 23, 280, 20, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtMaintenanceType3, _T (""), false);

	NEW_CONTROL (maintenanceTypeTitle, maintenanceTypeDlg, _T ("Text"), 15, 6, 240, 15, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtMaintenanceType4, _T (""), false);

	// ProgressDlg
	NEW_CONTROL (progressCancel, progressDlg, _T ("PushButton"), 304, 243, 56, 17,
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Cancel]"), _T (""), true);
	progressDlg->SetControlDefault (progressCancel);
	progressDlg->SetControlCancel (progressCancel);
	NEW_CONTROL_EVENT (progressCancel, _T ("SpawnDialog"), cancelDlg, _T ("1"));

	NEW_CONTROL (progreaaBanner, progressDlg, _T ("Bitmap"), 0, 0, 374, 44, 
		CBicControl::attrVisible, NULL, bnrBmp, _T (""), false);

	NEW_CONTROL (progressBack, progressDlg, _T ("PushButton"), 180, 243, 56, 17,
		CBicControl::attrVisible, NULL, _T ("[ButtonText_Back]"), _T (""), true);

	NEW_CONTROL (progressNext, progressDlg, _T ("PushButton"), 236, 243, 56, 17,
		CBicControl::attrVisible, NULL, _T ("[ButtonText_Next]"), _T (""), true);

	NEW_CONTROL (progressActionText, progressDlg, _T ("Text"), 70, 100, 265, 10, 
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T (""), _T (""), false);
	NEW_EVENT_MAPPING (progressActionText, _T ("ActionText"), _T ("Text"));

	NEW_CONTROL (progressBannerLine, progressDlg, _T ("Line"), 0, 44, 374, 0, 
		CBicControl::attrVisible, NULL, _T (""), _T (""), false);

	NEW_CONTROL (progressBottomLine, progressDlg, _T ("Line"), 0, 234, 374, 0, 
		CBicControl::attrVisible, NULL, _T (""), _T (""), false);

	NEW_CONTROL (progressProgressBar, progressDlg, _T ("ProgressBar"), 35, 115, 300, 10, 
		CBicControl::attrVisible | CBicControl::attrProgress95, NULL, _T ("Progress done"), _T (""), false);
	NEW_EVENT_MAPPING (progressProgressBar, _T ("SetProgress"), _T ("Progress"));

	NEW_CONTROL (progressStatusLabel, progressDlg, _T ("Text"), 35, 100, 35, 10, 
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("Status:"), _T (""), false);

	NEW_CONTROL (progressText, progressDlg, _T ("Text"), 35, 65, 300, 20,
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, 
		dtProgress1, _T (""), false);

	NEW_CONTROL (progressTitle, progressDlg, _T ("Text"), 20, 15, 200, 15, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, _T ("{\\DlgFontBold8}[Progress1] [SingleProductName]"), _T (""), false);

	// VerifyRepairDlg
	NEW_CONTROL (verifyRepairRepair, verifyRepairDlg, _T ("PushButton"), 236, 243, 56, 17,
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Repair]"), _T (""), true);
	verifyRepairDlg->SetControlCancel (verifyRepairRepair);
	NEW_CONTROL_EVENT (verifyRepairRepair, _T ("ReinstallMode"), _T ("ecmus"), _T ("OutOfDiskSpace <> 1"));
	NEW_CONTROL_EVENT (verifyRepairRepair, _T ("Reinstall"), _T ("All"), _T ("OutOfDiskSpace <> 1"));
	NEW_CONTROL_EVENT (verifyRepairRepair, _T ("EndDialog"), _T ("Return"), _T ("OutOfDiskSpace <> 1"));
	NEW_CONTROL_EVENT (verifyRepairRepair, _T ("SpawnDialog"), outOfRbDiskDlg, _T ("OutOfDiskSpace = 1 AND OutOfNoRbDiskSpace = 0 AND (PROMPTROLLBACKCOST=\"P\" OR NOT PROMPTROLLBACKCOST)"));
	NEW_CONTROL_EVENT (verifyRepairRepair, _T ("EndDialog"), _T ("Return"), _T ("OutOfDiskSpace = 1 AND OutOfNoRbDiskSpace = 0 AND PROMPTROLLBACKCOST=\"D\""));
	NEW_CONTROL_EVENT (verifyRepairRepair, _T ("EnableRollback"), _T ("False"), _T ("OutOfDiskSpace = 1 AND OutOfNoRbDiskSpace = 0 AND PROMPTROLLBACKCOST=\"D\""));
	NEW_CONTROL_EVENT (verifyRepairRepair, _T ("SpawnDialog"), outOfDiskDlg, _T ("(OutOfDiskSpace = 1 AND OutOfNoRbDiskSpace = 1) OR (OutOfDiskSpace = 1 AND PROMPTROLLBACKCOST=\"F\")"));

	NEW_CONTROL (verifyRepairBanner, verifyRepairDlg, _T ("Bitmap"), 0, 0, 374, 44, 
		CBicControl::attrVisible, NULL, bnrBmp, _T (""), false);

	NEW_CONTROL (verifyRepairCancel, verifyRepairDlg, _T ("PushButton"), 304, 243, 56, 17,
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Cancel]"), _T (""), true);
	verifyRepairDlg->SetControlCancel (verifyRepairCancel);
	NEW_CONTROL_EVENT (verifyRepairCancel, _T ("SpawnDialog"), cancelDlg, _T ("1"));

	NEW_CONTROL (verifyRepairBack, verifyRepairDlg, _T ("PushButton"), 180, 243, 56, 17,
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Back]"), _T (""), true);
	NEW_CONTROL_EVENT (verifyRepairBack, _T ("NewDialog"), maintenanceTypeDlg, _T ("1"));

	NEW_CONTROL (verifyRepairBannerLine, verifyRepairDlg, _T ("Line"), 0, 44, 374, 0, 
		CBicControl::attrVisible, NULL, _T (""), _T (""), false);

	NEW_CONTROL (verifyRepairBottomLine, verifyRepairDlg, _T ("Line"), 0, 234, 374, 0,
		CBicControl::attrVisible, NULL, _T (""), _T (""), false);

	NEW_CONTROL (verifyRepairDescription, verifyRepairDlg, _T ("Text"), 25, 23, 280, 15,
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtVerifyRepair2, _T (""), false);

	NEW_CONTROL (verifyRepairText, verifyRepairDlg, _T ("Text"), 25, 70, 320, 30, 
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, 
		dtVerifyRepair1, _T (""), false);

	NEW_CONTROL (verifyRepairTitle, verifyRepairDlg, _T ("Text"), 15, 6, 200, 15, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, _T ("{\\DlgFontBold8}Repair [SingleProductName]"), _T (""), false);

	// VerifyRemoveDlg
	NEW_CONTROL (verifyRemoveBack, verifyRemoveDlg, _T ("PushButton"), 180, 243, 56, 17,
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Back]"), _T (""), true);
	verifyRemoveDlg->SetControlDefault (verifyRemoveBack);
	NEW_CONTROL_EVENT (verifyRemoveBack, _T ("NewDialog"), maintenanceTypeDlg, _T ("1"));

	NEW_CONTROL (verifyRemoveBanner, verifyRemoveDlg, _T ("Bitmap"), 0, 0, 374, 44, 
		CBicControl::attrVisible, NULL, bnrBmp, _T (""), false);

	NEW_CONTROL (verifyRemoveRemove, verifyRemoveDlg, _T ("PushButton"), 236, 243, 56, 17,
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Remove]"), _T (""), true);
	NEW_CONTROL_EVENT (verifyRemoveRemove, _T ("Remove"), _T ("All"), _T ("OutOfDiskSpace <> 1"));
	NEW_CONTROL_EVENT (verifyRemoveRemove, _T ("EndDialog"), _T ("Return"), _T ("OutOfDiskSpace <> 1"));
	NEW_CONTROL_EVENT (verifyRemoveRemove, _T ("SpawnDialog"), outOfRbDiskDlg, _T ("OutOfDiskSpace = 1 AND OutOfNoRbDiskSpace = 0 AND (PROMPTROLLBACKCOST=\"P\" OR NOT PROMPTROLLBACKCOST)"));
	NEW_CONTROL_EVENT (verifyRemoveRemove, _T ("EndDialog"), _T ("Return"), _T ("OutOfDiskSpace = 1 AND OutOfNoRbDiskSpace = 0 AND PROMPTROLLBACKCOST=\"D\""));
	NEW_CONTROL_EVENT (verifyRemoveRemove, _T ("EnableRollback"), _T ("False"), _T ("OutOfDiskSpace = 1 AND OutOfNoRbDiskSpace = 0 AND PROMPTROLLBACKCOST=\"D\""));
	NEW_CONTROL_EVENT (verifyRemoveRemove, _T ("SpawnDialog"), outOfDiskDlg, _T ("(OutOfDiskSpace = 1 AND OutOfNoRbDiskSpace = 1) OR (OutOfDiskSpace = 1 AND PROMPTROLLBACKCOST=\"F\")"));

	NEW_CONTROL (verifyRemoveCancel, verifyRemoveDlg, _T ("PushButton"), 304, 243, 56, 17,
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Cancel]"), _T (""), true);
	verifyRemoveDlg->SetControlCancel (verifyRemoveCancel);
	NEW_CONTROL_EVENT (verifyRemoveCancel, _T ("SpawnDialog"), cancelDlg, _T ("1"));

	NEW_CONTROL (verifyRemoveBannerLine, verifyRemoveDlg, _T ("Line"), 0, 44, 374, 0, 
		CBicControl::attrVisible, NULL, _T (""), _T (""), false);
	
	NEW_CONTROL (verifyRemoveBottomLine, verifyRemoveDlg, _T ("Line"), 0, 234, 374, 0, 
		CBicControl::attrVisible, NULL, _T (""), _T (""), false);
	
	NEW_CONTROL (verifyRemoveDescription, verifyRemoveDlg, _T ("Text"), 25, 23, 280, 15, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtVerifyRemove1, _T (""), false);
	
	NEW_CONTROL (verifyRemoveText, verifyRemoveDlg, _T ("Text"), 25, 70, 320, 30,
		CBicControl::attrVisible, NULL, dtVerifyRemove2, _T (""), false);
	
	NEW_CONTROL (verifyRemoveTitle, verifyRemoveDlg, _T ("Text"), 15, 6, 200, 15,
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, _T ("{\\DlgFontBold8}Remove [SingleProductName]"), _T (""), false);

	// FatalErrorDlg
	NEW_CONTROL (fatalErrorFinish, fatalErrorDlg, _T ("PushButton"), 236, 243, 56, 17,
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Finish]"), _T (""), true);
	fatalErrorDlg->SetControlDefault (fatalErrorFinish);
	fatalErrorDlg->SetControlCancel (fatalErrorFinish);
	NEW_CONTROL_EVENT (fatalErrorFinish, _T ("EndDialog"), _T ("Exit"), _T ("1"));

	NEW_CONTROL (fatalErrorDlgBitmap, fatalErrorDlg, _T ("Bitmap"), 0, 0, 115, 234, 
		CBicControl::attrVisible, NULL, dlgBmp, _T (""), false);

	NEW_CONTROL (fatalErrorCancel, fatalErrorDlg, _T ("PushButton"), 304, 243, 56, 17,
		CBicControl::attrVisible, NULL, _T ("[ButtonText_Cancel]"), _T (""), true);

	NEW_CONTROL (fatalErrorBack, fatalErrorDlg, _T ("PushButton"), 180, 243, 56, 17,
		CBicControl::attrVisible, NULL, _T ("[ButtonText_Back]"), _T (""), true);

	NEW_CONTROL (fatalErrorBottomLine, fatalErrorDlg, _T ("Line"), 0, 234, 374, 0, 
		CBicControl::attrVisible, NULL, _T (""), _T (""), false);

	NEW_CONTROL (fatalErrorDescription1, fatalErrorDlg, _T ("Text"), 135, 70, 220, 40,
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtFatalError1, _T (""), false);

	NEW_CONTROL (fatalErrorDescription2, fatalErrorDlg, _T ("Text"), 135, 115, 220, 20, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtFatalError2, _T (""), false);

	NEW_CONTROL (fatalErrorTitle, fatalErrorDlg, _T ("Text"), 135, 20, 220, 60, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtFatalError3, _T (""), false);
	
	// NoRegistry
	NEW_CONTROL (noRegistryCancel, noRegistryDlg, _T ("PushButton"), 304, 243, 56, 17, 
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Cancel]"), _T (""), true);
	noRegistryDlg->SetControlDefault (noRegistryCancel);
	noRegistryDlg->SetControlCancel (noRegistryCancel);
	NEW_CONTROL_EVENT (noRegistryCancel, _T ("EndDialog"), _T ("Exit"), _T ("1"));

	NEW_CONTROL (noRegistryDlgBitmap, noRegistryDlg, _T ("Bitmap"), 0, 0, 115, 234, 
		CBicControl::attrVisible, NULL, dlgBmp, _T (""), false);

	NEW_CONTROL (noRegistryBack, noRegistryDlg, _T ("PushButton"), 180, 243, 56, 17, 
		CBicControl::attrVisible, NULL, _T ("[ButtonText_Back]"), _T (""), false);

	NEW_CONTROL (noRegistryBottomLine, noRegistryDlg, _T ("Line"), 0, 234, 374, 0, CBicControl::attrVisible, NULL, _T (""), _T (""), false);

	NEW_CONTROL (noRegistryDescription, noRegistryDlg, _T ("Text"), 135, 70, 220, 20, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtNoRegistry1, _T (""), false);

	NEW_CONTROL (noRegistryNext, noRegistryDlg, _T ("PushButton"), 236, 243, 56, 17, CBicControl::attrVisible, 
		NULL, _T ("[ButtonText_Next]"), _T (""), false);

	NEW_CONTROL (noRegistryTitle, noRegistryDlg, _T ("Text"), 135, 20, 220, 60,
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtNoRegistry2, _T (""), false);

	// BrowseDlg
	NEW_CONTROL (browsePathEdit, browseDlg, _T ("PathEdit"), 84, 202, 261, 18, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrIndirect, browseProperty, _T (""), _T (""), true);

	NEW_CONTROL (browseOk, browseDlg, _T ("PushButton"), 304, 243, 56, 17, CBicControl::attrVisible | CBicControl::attrEnabled,
		NULL, _T ("[ButtonText_OK]"), _T (""), true);
	NEW_CONTROL_EVENT (browseOk, _T ("SetTargetPath"), _T ("[_BrowseProperty]"), _T ("1"));
	NEW_CONTROL_EVENT (browseOk, _T ("EndDialog"), _T ("Return"), _T ("1"));

	NEW_CONTROL (browseCancel, browseDlg, _T ("PushButton"), 240, 243, 56, 17, CBicControl::attrVisible | CBicControl::attrEnabled,
		NULL, _T ("[ButtonText_Cancel]"), _T (""), true);
	NEW_CONTROL_EVENT (browseCancel, _T ("Reset"), _T ("0"), _T ("1"));
	NEW_CONTROL_EVENT (browseCancel, _T ("EndDialog"), _T ("Return"), _T ("1"));
	browseDlg->SetControlDefault (browseOk);
	browseDlg->SetControlCancel (browseCancel);

	NEW_CONTROL (browseComboLabel, browseDlg, _T ("Text"), 25, 58, 44, 10, CBicControl::attrVisible | CBicControl::attrEnabled,
		NULL, _T ("&Look in:"), _T (""), false);

	NEW_CONTROL (browseDirectoryCombo, browseDlg, _T ("DirectoryCombo"), 70, 55, 220, 80, 
		CBicControl::attrRemoteVolume | CBicControl::attrFixedVolume | CBicControl::attrIndirect | CBicControl::attrVisible | CBicControl::attrEnabled, 
		browseProperty, _T (""), _T (""), true);
	NEW_EVENT_MAPPING (browseDirectoryCombo, _T ("IgnoreChange"), _T ("IgnoreChange"));

	NEW_CONTROL (browseUp, browseDlg, _T ("PushButton"), 298, 55, 19, 19, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrIcon | CBicControl::attrIconSize16 | CBicControl::attrFixedSize,
		NULL, upBmp, _T ("Up One Level|"), true);
	NEW_CONTROL_EVENT (browseUp, _T ("DirectoryListUp"), _T ("0"), _T ("1"));

	NEW_CONTROL (browseNewFolder, browseDlg, _T ("PushButton"), 325, 55, 19, 19, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrIcon | CBicControl::attrIconSize16 | CBicControl::attrFixedSize, 
		NULL, newBmp, _T ("Create A New Folder|"), true);
	NEW_CONTROL_EVENT (browseNewFolder, _T ("DirectoryListNew"), _T ("0"), _T ("1"));

	NEW_CONTROL (browseDirectoryList, browseDlg, _T ("DirectoryList"), 25, 83, 320, 110, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrSunken | CBicControl::attrIndirect,
		browseProperty, _T (""), _T (""), true);

	NEW_CONTROL (browsePathLabel, browseDlg, _T ("Text"), 25, 205, 59, 10, CBicControl::attrVisible | CBicControl::attrEnabled,
		NULL, _T ("&Folder name:"), _T (""), false);

	NEW_CONTROL (browseBannerBitmap, browseDlg, _T ("Bitmap"), 0, 0, 374, 44, CBicControl::attrVisible, NULL, 
		bnrBmp, _T (""), false);

	NEW_CONTROL (browseBannerLine, browseDlg, _T ("Line"), 0, 44, 374, 0, CBicControl::attrVisible, NULL, _T (""), _T (""), false);

	NEW_CONTROL (browseBottomLine, browseDlg, _T ("Line"), 0, 234, 374, 0, CBicControl::attrVisible, NULL, _T (""), _T (""), false);

	NEW_CONTROL (browseDescription, browseDlg, _T ("Text"), 25, 23, 280, 15, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, _T ("Browse to the destination folder"), _T (""), NULL);

	NEW_CONTROL (browseTitle, browseDlg, _T ("Text"), 15, 6, 200, 15, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, _T ("{\\DlgFontBold8}Change current destination folder"), _T (""), NULL);

	// customizeDlg
	NEW_CONTROL (customizeNext, customizeDlg, _T ("PushButton"), 236, 243, 56, 17, 
		CBicControl::attrEnabled | CBicControl::attrVisible, NULL, _T ("[ButtonText_Next]"), _T (""), true);
	NEW_CONTROL_EVENT (customizeNext, _T ("NewDialog"), verifyReadyDlg, _T ("1"));
	NEW_EVENT_MAPPING (customizeNext, _T ("SelectionNoItems"), _T ("Enabled"));
	customizeDlg->SetControlDefault (customizeNext);
	
	NEW_CONTROL (customizeCancel, customizeDlg, _T ("PushButton"), 304, 243, 56, 17, 
		CBicControl::attrEnabled | CBicControl::attrVisible, NULL, _T ("[ButtonText_Cancel]"), _T (""), true);
	NEW_CONTROL_EVENT (customizeCancel, _T ("SpawnDialog"), cancelDlg, _T ("1"));
	customizeDlg->SetControlCancel (customizeCancel);

	NEW_CONTROL (customizeBrowse, customizeDlg, _T ("PushButton"), 304, 200, 56, 17, 
		CBicControl::attrEnabled | CBicControl::attrVisible, NULL, _T ("[ButtonText_Browse]"), _T (""), true);
	NEW_CONTROL_EVENT (customizeBrowse, _T ("SelectionBrowse"), browseDlg, _T ("1"));
	NEW_CONTROL_CONDITION (customizeBrowse, _T ("Hide"), _T ("Installed"));

	/*
	NEW_CONTROL (customizeReset, customizeDlg, _T ("PushButton"), 42, 243, 56, 17, 
		CBicControl::attrEnabled | CBicControl::attrVisible, NULL, _T ("[ButtonText_Reset]"), _T (""), true);
	NEW_CONTROL_EVENT (customizeReset, _T ("Reset"), _T ("0"), _T ("1"));
	NEW_EVENT_MAPPING (customizeReset, _T ("SelectionNoItems"), _T ("Enabled"));
	*/

	/*
	NEW_CONTROL (customizeDiskCost, customizeDlg, _T ("PushButton"), 111, 243, 56, 17, 
		CBicControl::attrEnabled | CBicControl::attrVisible, NULL, _T ("Disk &Usage"), _T (""), true);
	*/

	NEW_CONTROL (customizeBack, customizeDlg, _T ("PushButton"), 180, 243, 56, 17, 
		CBicControl::attrEnabled | CBicControl::attrVisible, NULL, _T ("[ButtonText_Back]"), _T (""), true);
	NEW_CONTROL_EVENT (customizeBack, _T ("NewDialog"), eulaTag ? licenceDlg : welcomeDlg, _T ("1"));

	NEW_CONTROL (customizeTree, customizeDlg, _T ("SelectionTree"), 25, 85, 175, 95, 
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrSunken, browseProperty, 
		_T ("Tree of selections"), _T (""), false);

	NEW_CONTROL (customizeBannerBitmap, customizeDlg, _T ("Bitmap"), 0, 0, 374, 44, 
		CBicControl::attrVisible, NULL, _T ("[BannerBitmap]"), _T (""), false);

	NEW_CONTROL (customizeBannerLine, customizeDlg, _T ("Line"), 0, 44, 374, 0, 
		CBicControl::attrVisible, NULL, _T (""), _T (""), false);
	
	NEW_CONTROL (customizeBottomLine, customizeDlg, _T ("Line"), 0, 234, 374, 0, 
		CBicControl::attrVisible, NULL, _T (""), _T (""), false);

	NEW_CONTROL (customizeDescription, customizeDlg, _T ("Text"), 25, 23, 280, 15,
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, _T ("Select the way you want features to be installed."), _T (""), false);

	NEW_CONTROL (customizeLocation, customizeDlg, _T ("Text"), 75, 200, 215, 20, 
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("<The selection's path>"), _T (""), false);
	NEW_CONTROL_CONDITION (customizeLocation, _T ("Hide"), _T ("Installed"));
	NEW_EVENT_MAPPING (customizeLocation, _T ("SelectionPath"), _T ("Text"));
	NEW_EVENT_MAPPING (customizeLocation, _T ("SelectionPathOn"), _T ("Visible"));
	
	NEW_CONTROL (customizeLocationLabel, customizeDlg, _T ("Text"), 25, 200, 50, 10, 
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("Location:"), _T (""), false);
	NEW_CONTROL_CONDITION (customizeLocationLabel, _T ("Hide"), _T ("Installed"));
	NEW_EVENT_MAPPING (customizeLocationLabel, _T ("SelectionPathOn"), _T ("Visible"));

	NEW_CONTROL (customizeTitle, customizeDlg, _T ("Text"), 15, 6, 200, 15,
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, _T ("{\\DlgFontBold8}Custom Setup"), _T (""), false);

	NEW_CONTROL (customizeBox, customizeDlg, _T ("GroupBox"), 210, 81, 140, 98, CBicControl::attrVisible,
		NULL, _T (""), _T (""), false);

	NEW_CONTROL (customizeItemDescription, customizeDlg, _T ("Text"), 215, 90, 131, 30, 
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, 
		_T ("Multiline description of the currently selected item."), _T (""), false);
	NEW_EVENT_MAPPING (customizeItemDescription, _T ("SelectionDescription"), _T ("Text"));

	NEW_CONTROL (customizeItemSize, customizeDlg, _T ("Text"), 215, 130, 131, 45, 
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, 
		_T ("The size of the currently selected item."), _T (""), false);
	NEW_EVENT_MAPPING (customizeItemSize, _T ("SelectionSize"), _T ("Text"));

	// UserExitDlg
	NEW_CONTROL (userExitFinish, userExitDlg, _T ("PushButton"), 236, 243, 56, 17,
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Finish]"), _T (""), true);
	userExitDlg->SetControlDefault (userExitFinish);
	userExitDlg->SetControlCancel (userExitFinish);
	NEW_CONTROL_EVENT (userExitFinish, _T ("EndDialog"), _T ("Exit"), _T ("1"));

	NEW_CONTROL (userExitDlgBitmap, userExitDlg, _T ("Bitmap"), 0, 0, 115, 234, 
		CBicControl::attrVisible, NULL, dlgBmp, _T (""), false);

	NEW_CONTROL (userExitCancel, userExitDlg, _T ("PushButton"), 304, 243, 56, 17,
		CBicControl::attrVisible, NULL, _T ("[ButtonText_Cancel]"), _T (""), true);

	NEW_CONTROL (userExitBack, userExitDlg, _T ("PushButton"), 180, 243, 56, 17,
		CBicControl::attrVisible, NULL, _T ("[ButtonText_Back]"), _T (""), true);

	NEW_CONTROL (userExitBottomLine, userExitDlg, _T ("Line"), 0, 234, 374, 0,
		CBicControl::attrVisible, NULL, _T (""), _T (""), false);

	NEW_CONTROL (userExitDescription1, userExitDlg, _T ("Text"), 135, 70, 220, 40,
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtUserExit1, _T (""), false);
	
	NEW_CONTROL (userExitDescription2, userExitDlg, _T ("Text"), 135, 115, 220, 20,
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtUserExit2, _T (""), false);

	NEW_CONTROL (userExitTitle, userExitDlg, _T ("Text"), 135, 20, 220, 60,
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtUserExit3, _T (""), false);

	// ExitDlg
	NEW_CONTROL (exitDlgFinish, exitDlg, _T ("PushButton"), 236, 243, 56, 17,
		CBicControl::attrVisible | CBicControl::attrEnabled, NULL, _T ("[ButtonText_Finish]"), _T (""), true);
	exitDlg->SetControlDefault (exitDlgFinish);
	exitDlg->SetControlCancel (exitDlgFinish);
	if (readMePresent)
	{
		NEW_CONTROL_EVENT (exitDlgFinish, _T ("DoAction"), showReadMeAction->GetAction (), _T ("InstallMode = \"Complete\" AND ShowReadMe = \"1\""));
	}
	NEW_CONTROL_EVENT (exitDlgFinish, _T ("EndDialog"), _T ("Return"), _T ("1"));

	NEW_CONTROL (exitDlgBitmap, exitDlg, _T ("Bitmap"), 0, 0, 115, 234, 
		CBicControl::attrVisible, NULL, dlgBmp, _T (""), false);
	
	if (readMePresent)
	{
		NEW_CONTROL (exitDlgShowReadMe, exitDlg, _T ("CheckBox"), 135, 105, 120, 17,
			CBicControl::attrEnabled, showReadMe, _T ("[ButtonText_ShowReadMe]"), _T (""), true);
		NEW_CONTROL_CONDITION (exitDlgShowReadMe, _T ("Hide"), _T ("InstallMode <> \"Complete\""));
		NEW_CONTROL_CONDITION (exitDlgShowReadMe, _T ("Show"), _T ("InstallMode = \"Complete\""));
	}

	NEW_CONTROL (exitDlgCancel, exitDlg, _T ("PushButton"), 304, 243, 56, 17,
		CBicControl::attrVisible, NULL, _T ("[ButtonText_Cancel]"), _T (""), true);

	NEW_CONTROL (exitDlgBack, exitDlg, _T ("PushButton"), 180, 243, 56, 17,
		CBicControl::attrVisible, NULL, _T ("[ButtonText_Back]"), _T (""), true);

	NEW_CONTROL (exitDlgBottomLine, exitDlg, _T ("Line"), 0, 234, 374, 0,
		CBicControl::attrVisible, NULL, _T (""), _T (""), false);

	NEW_CONTROL (exitDlgDescription, exitDlg, _T ("Text"), 135, 70, 220, 20,
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtExit1, _T (""), false);

	NEW_CONTROL (exitDlgTitle, exitDlg, _T ("Text"), 135, 20, 220, 60,
		CBicControl::attrVisible | CBicControl::attrEnabled | CBicControl::attrTransparent | CBicControl::attrNoPrefix, 
		NULL, dtExit2, _T (""), false);

#undef NEW_DIALOG
#undef NEW_CONTROL
#undef NEW_CONTROL_CONDITION
#undef NEW_CONTROL_EVENT
#undef NEW_EVENT_MAPPING
#undef NEW_PROPERTY

	// CustomAction - Check SN
#define NEW_BINARY(binary, rscName, rscType);		\
	binary = db.NewBinary (instance, rscName, rscType);\
	if (binary == NULL) { return false; }

	CBicBinary *checksn;
	NEW_BINARY (checksn, MAKEINTRESOURCE (IDR_CHECKSN_EXE), _T ("BINARY"));

	CBicCustomAction *checkSnAction;
	checkSnAction = db.NewCustomAction (2, checksn, _T (""));
	if (checkSnAction == NULL) { return false; }

#undef NEW_BINARY

	// InstallUISequence
#define NEW_INSTALL_UI_ACTION(action, condition);		\
	{\
		CBicInstallUIAction *ui = db.NewInstallUIAction (action, condition);\
		if (ui == NULL) { return false; }\
	}

#define NEW_INSTALL_UI_ACTION2(action, condition, sequence);		\
	{\
		CBicInstallUIAction *ui = db.NewInstallUIAction (action, condition, sequence);\
		if (ui == NULL) { return false; }\
	}

	NEW_INSTALL_UI_ACTION2 (fatalErrorDlg, _T ("1"), -3);
	NEW_INSTALL_UI_ACTION2 (userExitDlg, _T ("1"), -2);
	NEW_INSTALL_UI_ACTION2 (exitDlg, _T ("1"), -1);
	NEW_INSTALL_UI_ACTION (checkSnAction, _T ("1"));
	NEW_INSTALL_UI_ACTION (_T ("LaunchConditions"), _T ("1"));
	NEW_INSTALL_UI_ACTION (prepareDlg, _T ("1"));
	NEW_INSTALL_UI_ACTION (_T ("FindRelatedProducts"), _T ("1"));
	NEW_INSTALL_UI_ACTION (_T ("AppSearch"), _T ("1"));
	NEW_INSTALL_UI_ACTION (_T ("CCPSearch"), _T ("NOT Installed"));
	NEW_INSTALL_UI_ACTION (_T ("RMCCPSearch"), _T ("NOT Installed"));
	NEW_INSTALL_UI_ACTION (_T ("CostInitialize"), _T ("1"));
	NEW_INSTALL_UI_ACTION (_T ("FileCost"), _T ("1"));
	NEW_INSTALL_UI_ACTION (_T ("CostFinalize"), _T ("1"));
	NEW_INSTALL_UI_ACTION (_T ("MigrateFeatureStates"), _T ("1"));
	NEW_INSTALL_UI_ACTION (badSerialNumberDlg, _T ("NOT Installed AND PATHTAG <> \":\" AND KEYNOTE <> \"#1\""));
	NEW_INSTALL_UI_ACTION (welcomeDlg, _T ("NOT Installed AND PATHTAG <> \":\" AND KEYNOTE = \"#1\""));
	NEW_INSTALL_UI_ACTION (noRegistryDlg, _T ("NOT Installed AND PATHTAG = \":\""));
	NEW_INSTALL_UI_ACTION (resumeDlg, _T ("Installed AND (RESUME OR Preselected)"));
	NEW_INSTALL_UI_ACTION (maintenanceWelcomeDlg, _T ("Installed AND NOT RESUME AND NOT Preselected"));
	NEW_INSTALL_UI_ACTION (progressDlg, _T ("1"));
	NEW_INSTALL_UI_ACTION (_T ("ExecuteAction"), _T ("1"));

#undef NEW_INSTALL_UI_ACTION
#undef NEW_INSTALL_UI_ACTION2

	return true;
}

bool AddUIText (CBicDatabase &db)
{
#define NEW_UI_TEXT(key, text);		\
	{\
		CBicUIText *uit = db.NewUIText (key, text);\
		if (uit == NULL) { return false; }\
	}

	NEW_UI_TEXT (_T ("AbsentPath"), _T (""));
	NEW_UI_TEXT (_T ("bytes"), _T ("bytes"));
	NEW_UI_TEXT (_T ("GB"), _T ("GB"));
	NEW_UI_TEXT (_T ("KB"), _T ("KB"));
	NEW_UI_TEXT (_T ("MB"), _T ("MB"));
	NEW_UI_TEXT (_T ("MenuAbsent"), _T ("Entire feature will be unavailable"));
	NEW_UI_TEXT (_T ("MenuAdvertise"), _T ("Feature will be installed when required"));
	NEW_UI_TEXT (_T ("MenuAllCD"), _T ("Entire feature will be installed to run from CD"));
	NEW_UI_TEXT (_T ("MenuAllLocal"), _T ("Entire feature will be installed on local hard drive"));
	NEW_UI_TEXT (_T ("MenuAllNetwork"), _T ("Entire feature will be installed to run from network"));
	NEW_UI_TEXT (_T ("MenuCD"), _T ("Will be installed to run from CD"));
	NEW_UI_TEXT (_T ("MenuLocal"), _T ("Will be installed on local hard drive"));
	NEW_UI_TEXT (_T ("MenuNetwork"), _T ("Will be installed to run from network"));
	NEW_UI_TEXT (_T ("ScriptInProgress"), _T ("Gathering required information..."));
	NEW_UI_TEXT (_T ("SelAbsentAbsent"), _T ("This feature will remain uninstalled"));
	NEW_UI_TEXT (_T ("SelAbsentAdvertise"), _T ("This feature will be set to be installed when required"));
	NEW_UI_TEXT (_T ("SelAbsentCD"), _T ("This feature will be installed to run from CD"));
	NEW_UI_TEXT (_T ("SelAbsentLocal"), _T ("This feature will be installed on the local hard drive"));
	NEW_UI_TEXT (_T ("SelAbsentNetwork"), _T ("This feature will be installed to run from the network"));
	NEW_UI_TEXT (_T ("SelAdvertiseAbsent"), _T ("This feature will become unavailable"));
	NEW_UI_TEXT (_T ("SelAdvertiseAdvertise"), _T ("Will be installed when required"));
	NEW_UI_TEXT (_T ("SelAdvertiseCD"), _T ("This feature will be available to run from CD"));
	NEW_UI_TEXT (_T ("SelAdvertiseLocal"), _T ("This feature will be installed on your local hard drive"));
	NEW_UI_TEXT (_T ("SelAdvertiseNetwork"), _T ("This feature will be available to run from the network"));
	NEW_UI_TEXT (_T ("SelCDAbsent"), _T ("This feature will be uninstalled completely, you won't be able to run it from CD"));
	NEW_UI_TEXT (_T ("SelCDAdvertise"), _T ("This feature will change from run from CD state to set to be installed when required"));
	NEW_UI_TEXT (_T ("SelCDCD"), _T ("This feature will remain to be run from CD"));
	NEW_UI_TEXT (_T ("SelCDLocal"), _T ("This feature will change from run from CD state to be installed on the local hard drive"));
	NEW_UI_TEXT (_T ("SelChildCostNeg"), _T ("This feature frees up [1] on your hard drive."));
	NEW_UI_TEXT (_T ("SelChildCostPos"), _T ("This feature requires [1] on your hard drive."));
	NEW_UI_TEXT (_T ("SelCostPending"), _T ("Compiling cost for this feature..."));
	NEW_UI_TEXT (_T ("SelLocalAbsent"), _T ("This feature will be completely removed"));
	NEW_UI_TEXT (_T ("SelLocalAdvertise"), _T ("This feature will be removed from your local hard drive, but will be set to be installed when required"));
	NEW_UI_TEXT (_T ("SelLocalCD"), _T ("This feature will be removed from your local hard drive, but will be still available to run from CD"));
	NEW_UI_TEXT (_T ("SelLocalLocal"), _T ("This feature will remain on you local hard drive"));
	NEW_UI_TEXT (_T ("SelLocalNetwork"), _T ("This feature will be removed from your local hard drive, but will be still available to run from the network"));
	NEW_UI_TEXT (_T ("SelNetworkAbsent"), _T ("This feature will be uninstalled completely, you won't be able to run it from the network"));
	NEW_UI_TEXT (_T ("SelNetworkAdvertise"), _T ("This feature will change from run from network state to set to be installed when required"));
	NEW_UI_TEXT (_T ("SelNetworkLocal"), _T ("This feature will change from run from network state to be installed on the local hard drive"));
	NEW_UI_TEXT (_T ("SelNetworkNetwork"), _T ("This feature will remain to be run from the network"));
	NEW_UI_TEXT (_T ("SelParentCostNegNeg"), _T ("This feature frees up [1] on your hard drive. It has [2] of [3] subfeatures selected. The subfeatures free up [4] on your hard drive."));
	NEW_UI_TEXT (_T ("SelParentCostNegPos"), _T ("This feature frees up [1] on your hard drive. It has [2] of [3] subfeatures selected. The subfeatures require [4] on your hard drive."));
	NEW_UI_TEXT (_T ("SelParentCostPosNeg"), _T ("This feature requires [1] on your hard drive. It has [2] of [3] subfeatures selected. The subfeatures free up [4] on your hard drive."));
	NEW_UI_TEXT (_T ("SelParentCostPosPos"), _T ("This feature requires [1] on your hard drive. It has [2] of [3] subfeatures selected. The subfeatures require [4] on your hard drive."));
	NEW_UI_TEXT (_T ("TimeRemaining"), _T ("Time remaining: {[1] minutes }{[2] seconds}"));
	NEW_UI_TEXT (_T ("VolumeCostAvailable"), _T ("Available"));
	NEW_UI_TEXT (_T ("VolumeCostDifference"), _T ("Difference"));
	NEW_UI_TEXT (_T ("VolumeCostRequired"), _T ("Required"));
	NEW_UI_TEXT (_T ("VolumeCostSize"), _T ("Disk Size"));
	NEW_UI_TEXT (_T ("VolumeCostVolume"), _T ("Volume"));

#undef NEW_UI_TEXT

	return true;
}

bool AddValidation (CBicDatabase &db)
{
#define NEW_VALIDATION(table, column, nullable, minValue, maxValue, keyTable, keyColumn, category, set, description);		\
	{\
		CBicValidation *valida = db.NewValidation (table, column, nullable, minValue, maxValue, keyTable, keyColumn, category, set, description);\
		if (valida == NULL) { return false; }\
	}

	NEW_VALIDATION (_T ("AdminExecuteSequence"), _T ("Action"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Name of action to invoke, either in the engine or the handler DLL."));
	NEW_VALIDATION (_T ("AdminExecuteSequence"), _T ("Sequence"), _T ("Y"), -4, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Number that determines the sort order in which the actions are to be executed.  Leave blank to suppress action."));
	NEW_VALIDATION (_T ("AdminExecuteSequence"), _T ("Condition"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Condition"), _T (""), _T ("Optional expression which skips the action if evaluates to expFalse.If the expression syntax is invalid, the engine will terminate, returning iesBadActionData."));
	NEW_VALIDATION (_T ("AdminUISequence"), _T ("Action"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Name of action to invoke, either in the engine or the handler DLL."));
	NEW_VALIDATION (_T ("AdminUISequence"), _T ("Sequence"), _T ("Y"), -4, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Number that determines the sort order in which the actions are to be executed.  Leave blank to suppress action."));
	NEW_VALIDATION (_T ("AdminUISequence"), _T ("Condition"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Condition"), _T (""), _T ("Optional expression which skips the action if evaluates to expFalse.If the expression syntax is invalid, the engine will terminate, returning iesBadActionData."));
	NEW_VALIDATION (_T ("Condition"), _T ("Condition"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Condition"), _T (""), _T ("Expression evaluated to determine if Level in the Feature table is to change."));
	NEW_VALIDATION (_T ("Condition"), _T ("Feature_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Feature"), 1, _T ("Identifier"), _T (""), _T ("Reference to a Feature entry in Feature table."));
	NEW_VALIDATION (_T ("Condition"), _T ("Level"), _T ("N"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("New selection Level to set in Feature table if Condition evaluates to TRUE."));
	NEW_VALIDATION (_T ("AdvtExecuteSequence"), _T ("Action"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Name of action to invoke, either in the engine or the handler DLL."));
	NEW_VALIDATION (_T ("AdvtExecuteSequence"), _T ("Sequence"), _T ("Y"), -4, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Number that determines the sort order in which the actions are to be executed.  Leave blank to suppress action."));
	NEW_VALIDATION (_T ("AdvtExecuteSequence"), _T ("Condition"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Condition"), _T (""), _T ("Optional expression which skips the action if evaluates to expFalse.If the expression syntax is invalid, the engine will terminate, returning iesBadActionData."));
	NEW_VALIDATION (_T ("BBControl"), _T ("Type"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("The type of the control."));
	NEW_VALIDATION (_T ("BBControl"), _T ("BBControl"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Name of the control. This name must be unique within a billboard, but can repeat on different billboard."));
	NEW_VALIDATION (_T ("BBControl"), _T ("Billboard_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Billboard"), 1, _T ("Identifier"), _T (""), _T ("External key to the Billboard table, name of the billboard."));
	NEW_VALIDATION (_T ("BBControl"), _T ("X"), _T ("N"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Horizontal coordinate of the upper left corner of the bounding rectangle of the control."));
	NEW_VALIDATION (_T ("BBControl"), _T ("Y"), _T ("N"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Vertical coordinate of the upper left corner of the bounding rectangle of the control."));
	NEW_VALIDATION (_T ("BBControl"), _T ("Width"), _T ("N"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Width of the bounding rectangle of the control."));
	NEW_VALIDATION (_T ("BBControl"), _T ("Height"), _T ("N"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Height of the bounding rectangle of the control."));
	NEW_VALIDATION (_T ("BBControl"), _T ("Attributes"), _T ("Y"), 0, 2147483647, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("A 32-bit word that specifies the attribute flags to be applied to this control."));
	NEW_VALIDATION (_T ("BBControl"), _T ("Text"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("A string used to set the initial text contained within a control (if appropriate)."));
	NEW_VALIDATION (_T ("Billboard"), _T ("Action"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("The name of an action. The billboard is displayed during the progress messages received from this action."));
	NEW_VALIDATION (_T ("Billboard"), _T ("Billboard"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Name of the billboard."));
	NEW_VALIDATION (_T ("Billboard"), _T ("Feature_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Feature"), 1, _T ("Identifier"), _T (""), _T ("An external key to the Feature Table. The billboard is shown only if this feature is being installed."));
	NEW_VALIDATION (_T ("Billboard"), _T ("Ordering"), _T ("Y"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("A positive integer. If there is more than one billboard corresponding to an action they will be shown in the order defined by this column."));
	NEW_VALIDATION (_T ("Binary"), _T ("Name"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Unique key identifying the binary data."));
	NEW_VALIDATION (_T ("Binary"), _T ("Data"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Binary"), _T (""), _T ("The unformatted binary data."));
	NEW_VALIDATION (_T ("CheckBox"), _T ("Property"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("A named property to be tied to the item."));
	NEW_VALIDATION (_T ("CheckBox"), _T ("Value"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("The value string associated with the item."));
	NEW_VALIDATION (_T ("Property"), _T ("Property"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Name of property, uppercase if settable by launcher or loader."));
	NEW_VALIDATION (_T ("Property"), _T ("Value"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("String value for property.  Never null or empty."));
	NEW_VALIDATION (_T ("ComboBox"), _T ("Text"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("The visible text to be assigned to the item. Optional. If this entry or the entire column is missing, the text is the same as the value."));
	NEW_VALIDATION (_T ("ComboBox"), _T ("Property"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("A named property to be tied to this item. All the items tied to the same property become part of the same combobox."));
	NEW_VALIDATION (_T ("ComboBox"), _T ("Value"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("The value string associated with this item. Selecting the line will set the associated property to this value."));
	NEW_VALIDATION (_T ("ComboBox"), _T ("Order"), _T ("N"), 1, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("A positive integer used to determine the ordering of the items within one list.\tThe integers do not have to be consecutive."));
	NEW_VALIDATION (_T ("Control"), _T ("Type"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("The type of the control."));
	NEW_VALIDATION (_T ("Control"), _T ("X"), _T ("N"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Horizontal coordinate of the upper left corner of the bounding rectangle of the control."));
	NEW_VALIDATION (_T ("Control"), _T ("Y"), _T ("N"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Vertical coordinate of the upper left corner of the bounding rectangle of the control."));
	NEW_VALIDATION (_T ("Control"), _T ("Width"), _T ("N"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Width of the bounding rectangle of the control."));
	NEW_VALIDATION (_T ("Control"), _T ("Height"), _T ("N"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Height of the bounding rectangle of the control."));
	NEW_VALIDATION (_T ("Control"), _T ("Attributes"), _T ("Y"), 0, 2147483647, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("A 32-bit word that specifies the attribute flags to be applied to this control."));
	NEW_VALIDATION (_T ("Control"), _T ("Text"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("A string used to set the initial text contained within a control (if appropriate)."));
	NEW_VALIDATION (_T ("Control"), _T ("Property"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("The name of a defined property to be linked to this control. "));
	NEW_VALIDATION (_T ("Control"), _T ("Control"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Name of the control. This name must be unique within a dialog, but can repeat on different dialogs. "));
	NEW_VALIDATION (_T ("Control"), _T ("Dialog_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Dialog"), 1, _T ("Identifier"), _T (""), _T ("External key to the Dialog table, name of the dialog."));
	NEW_VALIDATION (_T ("Control"), _T ("Control_Next"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Control"), 2, _T ("Identifier"), _T (""), _T ("The name of an other control on the same dialog. This link defines the tab order of the controls. The links have to form one or more cycles!"));
	NEW_VALIDATION (_T ("Control"), _T ("Help"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("The help strings used with the button. The text is optional. "));
	NEW_VALIDATION (_T ("Icon"), _T ("Name"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Primary key. Name of the icon file."));
	NEW_VALIDATION (_T ("Icon"), _T ("Data"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Binary"), _T (""), _T ("Binary stream. The binary icon data in PE (.DLL or .EXE) or icon (.ICO) format."));
	NEW_VALIDATION (_T ("ListBox"), _T ("Text"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("The visible text to be assigned to the item. Optional. If this entry or the entire column is missing, the text is the same as the value."));
	NEW_VALIDATION (_T ("ListBox"), _T ("Property"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("A named property to be tied to this item. All the items tied to the same property become part of the same listbox."));
	NEW_VALIDATION (_T ("ListBox"), _T ("Value"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("The value string associated with this item. Selecting the line will set the associated property to this value."));
	NEW_VALIDATION (_T ("ListBox"), _T ("Order"), _T ("N"), 1, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("A positive integer used to determine the ordering of the items within one list..The integers do not have to be consecutive."));
	NEW_VALIDATION (_T ("ActionText"), _T ("Action"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Name of action to be described."));
	NEW_VALIDATION (_T ("ActionText"), _T ("Description"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("Localized description displayed in progress dialog and log when action is executing."));
	NEW_VALIDATION (_T ("ActionText"), _T ("Template"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Template"), _T (""), _T ("Optional localized format template used to format action data records for display during action execution."));
	NEW_VALIDATION (_T ("ControlCondition"), _T ("Action"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T (""), _T ("Default;Disable;Enable;Hide;Show"), _T ("The desired action to be taken on the specified control."));
	NEW_VALIDATION (_T ("ControlCondition"), _T ("Condition"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Condition"), _T (""), _T ("A standard conditional statement that specifies under which conditions the action should be triggered."));
	NEW_VALIDATION (_T ("ControlCondition"), _T ("Dialog_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Dialog"), 1, _T ("Identifier"), _T (""), _T ("A foreign key to the Dialog table, name of the dialog."));
	NEW_VALIDATION (_T ("ControlCondition"), _T ("Control_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Control"), 2, _T ("Identifier"), _T (""), _T ("A foreign key to the Control table, name of the control."));
	NEW_VALIDATION (_T ("ControlEvent"), _T ("Condition"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Condition"), _T (""), _T ("A standard conditional statement that specifies under which conditions an event should be triggered."));
	NEW_VALIDATION (_T ("ControlEvent"), _T ("Ordering"), _T ("Y"), 0, 2147483647, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("An integer used to order several events tied to the same control. Can be left blank."));
	NEW_VALIDATION (_T ("ControlEvent"), _T ("Dialog_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Dialog"), 1, _T ("Identifier"), _T (""), _T ("A foreign key to the Dialog table, name of the dialog."));
	NEW_VALIDATION (_T ("ControlEvent"), _T ("Control_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Control"), 2, _T ("Identifier"), _T (""), _T ("A foreign key to the Control table, name of the control"));
	NEW_VALIDATION (_T ("ControlEvent"), _T ("Event"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("An identifier that specifies the type of the event that should take place when the user interacts with control specified by the first two entries."));
	NEW_VALIDATION (_T ("ControlEvent"), _T ("Argument"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("A value to be used as a modifier when triggering a particular event."));
	NEW_VALIDATION (_T ("Dialog"), _T ("Width"), _T ("N"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Width of the bounding rectangle of the dialog."));
	NEW_VALIDATION (_T ("Dialog"), _T ("Height"), _T ("N"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Height of the bounding rectangle of the dialog."));
	NEW_VALIDATION (_T ("Dialog"), _T ("Attributes"), _T ("Y"), 0, 2147483647, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("A 32-bit word that specifies the attribute flags to be applied to this dialog."));
	NEW_VALIDATION (_T ("Dialog"), _T ("Title"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("A text string specifying the title to be displayed in the title bar of the dialog's window."));
	NEW_VALIDATION (_T ("Dialog"), _T ("Dialog"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Name of the dialog."));
	NEW_VALIDATION (_T ("Dialog"), _T ("HCentering"), _T ("N"), 0, 100, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Horizontal position of the dialog on a 0-100 scale. 0 means left end, 100 means right end of the screen, 50 center."));
	NEW_VALIDATION (_T ("Dialog"), _T ("VCentering"), _T ("N"), 0, 100, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Vertical position of the dialog on a 0-100 scale. 0 means top end, 100 means bottom end of the screen, 50 center."));
	NEW_VALIDATION (_T ("Dialog"), _T ("Control_First"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Control"), 2, _T ("Identifier"), _T (""), _T ("Defines the control that has the focus when the dialog is created."));
	NEW_VALIDATION (_T ("Dialog"), _T ("Control_Default"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Control"), 2, _T ("Identifier"), _T (""), _T ("Defines the default control. Hitting return is equivalent to pushing this button."));
	NEW_VALIDATION (_T ("Dialog"), _T ("Control_Cancel"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Control"), 2, _T ("Identifier"), _T (""), _T ("Defines the cancel control. Hitting escape or clicking on the close icon on the dialog is equivalent to pushing this button."));
	NEW_VALIDATION (_T ("EventMapping"), _T ("Dialog_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Dialog"), 1, _T ("Identifier"), _T (""), _T ("A foreign key to the Dialog table, name of the Dialog."));
	NEW_VALIDATION (_T ("EventMapping"), _T ("Control_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Control"), 2, _T ("Identifier"), _T (""), _T ("A foreign key to the Control table, name of the control."));
	NEW_VALIDATION (_T ("EventMapping"), _T ("Event"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("An identifier that specifies the type of the event that the control subscribes to."));
	NEW_VALIDATION (_T ("EventMapping"), _T ("Attribute"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("The name of the control attribute, that is set when this event is received."));
	NEW_VALIDATION (_T ("InstallExecuteSequence"), _T ("Action"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Name of action to invoke, either in the engine or the handler DLL."));
	NEW_VALIDATION (_T ("InstallExecuteSequence"), _T ("Sequence"), _T ("Y"), -4, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Number that determines the sort order in which the actions are to be executed.  Leave blank to suppress action."));
	NEW_VALIDATION (_T ("InstallExecuteSequence"), _T ("Condition"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Condition"), _T (""), _T ("Optional expression which skips the action if evaluates to expFalse.If the expression syntax is invalid, the engine will terminate, returning iesBadActionData."));
	NEW_VALIDATION (_T ("AppSearch"), _T ("Property"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("The property associated with a Signature"));
	NEW_VALIDATION (_T ("AppSearch"), _T ("Signature_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Signature;RegLocator;IniLocator;DrLocator;CompLocator"), 1, _T ("Identifier"), _T (""), _T ("The Signature_ represents a unique file signature and is also the foreign key in the Signature,  RegLocator, IniLocator, CompLocator and the DrLocator tables."));
	NEW_VALIDATION (_T ("BindImage"), _T ("File_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("File"), 1, _T ("Identifier"), _T (""), _T ("The index into the File table. This must be an executable file."));
	NEW_VALIDATION (_T ("BindImage"), _T ("Path"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Paths"), _T (""), _T ("A list of ;  delimited paths that represent the paths to be searched for the import DLLS. The list is usually a list of properties each enclosed within square brackets [] ."));
	NEW_VALIDATION (_T ("CCPSearch"), _T ("Signature_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Signature;RegLocator;IniLocator;DrLocator;CompLocator"), 1, _T ("Identifier"), _T (""), _T ("The Signature_ represents a unique file signature and is also the foreign key in the Signature,  RegLocator, IniLocator, CompLocator and the DrLocator tables."));
	NEW_VALIDATION (_T ("InstallUISequence"), _T ("Action"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Name of action to invoke, either in the engine or the handler DLL."));
	NEW_VALIDATION (_T ("InstallUISequence"), _T ("Sequence"), _T ("Y"), -4, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Number that determines the sort order in which the actions are to be executed.  Leave blank to suppress action."));
	NEW_VALIDATION (_T ("InstallUISequence"), _T ("Condition"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Condition"), _T (""), _T ("Optional expression which skips the action if evaluates to expFalse.If the expression syntax is invalid, the engine will terminate, returning iesBadActionData."));
	NEW_VALIDATION (_T ("ListView"), _T ("Text"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("The visible text to be assigned to the item. Optional. If this entry or the entire column is missing, the text is the same as the value."));
	NEW_VALIDATION (_T ("ListView"), _T ("Property"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("A named property to be tied to this item. All the items tied to the same property become part of the same listview."));
	NEW_VALIDATION (_T ("ListView"), _T ("Value"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("The value string associated with this item. Selecting the line will set the associated property to this value."));
	NEW_VALIDATION (_T ("ListView"), _T ("Order"), _T ("N"), 1, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("A positive integer used to determine the ordering of the items within one list..The integers do not have to be consecutive."));
	NEW_VALIDATION (_T ("ListView"), _T ("Binary_"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Binary"), 1, _T ("Identifier"), _T (""), _T ("The name of the icon to be displayed with the icon. The binary information is looked up from the Binary Table."));
	NEW_VALIDATION (_T ("RadioButton"), _T ("X"), _T ("N"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("The horizontal coordinate of the upper left corner of the bounding rectangle of the radio button."));
	NEW_VALIDATION (_T ("RadioButton"), _T ("Y"), _T ("N"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("The vertical coordinate of the upper left corner of the bounding rectangle of the radio button."));
	NEW_VALIDATION (_T ("RadioButton"), _T ("Width"), _T ("N"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("The width of the button."));
	NEW_VALIDATION (_T ("RadioButton"), _T ("Height"), _T ("N"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("The height of the button."));
	NEW_VALIDATION (_T ("RadioButton"), _T ("Text"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("The visible title to be assigned to the radio button."));
	NEW_VALIDATION (_T ("RadioButton"), _T ("Property"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("A named property to be tied to this radio button. All the buttons tied to the same property become part of the same group."));
	NEW_VALIDATION (_T ("RadioButton"), _T ("Value"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("The value string associated with this button. Selecting the button will set the associated property to this value."));
	NEW_VALIDATION (_T ("RadioButton"), _T ("Order"), _T ("N"), 1, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("A positive integer used to determine the ordering of the items within one list..The integers do not have to be consecutive."));
	NEW_VALIDATION (_T ("RadioButton"), _T ("Help"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("The help strings used with the button. The text is optional."));
	NEW_VALIDATION (_T ("TextStyle"), _T ("TextStyle"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Name of the style. The primary key of this table. This name is embedded in the texts to indicate a style change."));
	NEW_VALIDATION (_T ("TextStyle"), _T ("FaceName"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("A string indicating the name of the font used. Required. The string must be at most 31 characters long."));
	NEW_VALIDATION (_T ("TextStyle"), _T ("Size"), _T ("N"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("The size of the font used. This size is given in our units (1/12 of the system font height). Assuming that the system font is set to 12 point size, this is equivalent to the point size."));
	NEW_VALIDATION (_T ("TextStyle"), _T ("Color"), _T ("Y"), 0, 16777215, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("A long integer indicating the color of the string in the RGB format (Red, Green, Blue each 0-255, RGB = R + 256*G + 256^2*B)."));
	NEW_VALIDATION (_T ("TextStyle"), _T ("StyleBits"), _T ("Y"), 0, 15, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("A combination of style bits."));
	NEW_VALIDATION (_T ("UIText"), _T ("Text"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("The localized version of the string."));
	NEW_VALIDATION (_T ("UIText"), _T ("Key"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("A unique key that identifies the particular string."));
	NEW_VALIDATION (_T ("_Validation"), _T ("Table"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Name of table"));
	NEW_VALIDATION (_T ("_Validation"), _T ("Description"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("Description of column"));
	NEW_VALIDATION (_T ("_Validation"), _T ("Column"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Name of column"));
	NEW_VALIDATION (_T ("_Validation"), _T ("Nullable"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T (""), _T ("Y;N;@"), _T ("Whether the column is nullable"));
	NEW_VALIDATION (_T ("_Validation"), _T ("MinValue"), _T ("Y"), -2147483647, 2147483647, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Minimum value allowed"));
	NEW_VALIDATION (_T ("_Validation"), _T ("MaxValue"), _T ("Y"), -2147483647, 2147483647, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Maximum value allowed"));
	NEW_VALIDATION (_T ("_Validation"), _T ("KeyTable"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("For foreign key, Name of table to which data must link"));
	NEW_VALIDATION (_T ("_Validation"), _T ("KeyColumn"), _T ("Y"), 1, 32, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Column to which foreign key connects"));
	NEW_VALIDATION (_T ("_Validation"), _T ("Category"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T (""), _T ("Text;Formatted;Template;Condition;Guid;Path;Version;Language;Identifier;Binary;UpperCase;LowerCase;Filename;Paths;AnyPath;WildCardFilename;RegPath;KeyFormatted;CustomSource;Property;Cabinet;Shortcut;URL"), _T ("String category"));
	NEW_VALIDATION (_T ("_Validation"), _T ("Set"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("Set of values that are permitted"));
	NEW_VALIDATION (_T ("AdvtUISequence"), _T ("Action"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Name of action to invoke, either in the engine or the handler DLL."));
	NEW_VALIDATION (_T ("AdvtUISequence"), _T ("Sequence"), _T ("Y"), -4, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Number that determines the sort order in which the actions are to be executed.  Leave blank to suppress action."));
	NEW_VALIDATION (_T ("AdvtUISequence"), _T ("Condition"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Condition"), _T (""), _T ("Optional expression which skips the action if evaluates to expFalse.If the expression syntax is invalid, the engine will terminate, returning iesBadActionData."));
	NEW_VALIDATION (_T ("AppId"), _T ("AppId"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Guid"), _T (""), _T (""));
	NEW_VALIDATION (_T ("AppId"), _T ("ActivateAtStorage"), _T ("Y"), 0, 1, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T (""));
	NEW_VALIDATION (_T ("AppId"), _T ("DllSurrogate"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T (""));
	NEW_VALIDATION (_T ("AppId"), _T ("LocalService"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T (""));
	NEW_VALIDATION (_T ("AppId"), _T ("RemoteServerName"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T (""));
	NEW_VALIDATION (_T ("AppId"), _T ("RunAsInteractiveUser"), _T ("Y"), 0, 1, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T (""));
	NEW_VALIDATION (_T ("AppId"), _T ("ServiceParameters"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T (""));
	NEW_VALIDATION (_T ("Feature"), _T ("Attributes"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T (""), _T ("0;1;2;4;5;6;8;9;10;16;17;18;20;21;22;24;25;26;32;33;34;36;37;38;48;49;50;52;53;54"), _T ("Feature attributes"));
	NEW_VALIDATION (_T ("Feature"), _T ("Description"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("Longer descriptive text describing a visible feature item."));
	NEW_VALIDATION (_T ("Feature"), _T ("Title"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("Short text identifying a visible feature item."));
	NEW_VALIDATION (_T ("Feature"), _T ("Feature"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Primary key used to identify a particular feature record."));
	NEW_VALIDATION (_T ("Feature"), _T ("Directory_"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Directory"), 1, _T ("UpperCase"), _T (""), _T ("The name of the Directory that can be configured by the UI. A non-null value will enable the browse button."));
	NEW_VALIDATION (_T ("Feature"), _T ("Level"), _T ("N"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("The install level at which record will be initially selected. An install level of 0 will disable an item and prevent its display."));
	NEW_VALIDATION (_T ("Feature"), _T ("Display"), _T ("Y"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Numeric sort order, used to force a specific display ordering."));
	NEW_VALIDATION (_T ("Feature"), _T ("Feature_Parent"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Feature"), 1, _T ("Identifier"), _T (""), _T ("Optional key of a parent record in the same table. If the parent is not selected, then the record will not be installed. Null indicates a root item."));
	NEW_VALIDATION (_T ("File"), _T ("Sequence"), _T ("N"), 1, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Sequence with respect to the media images; order must track cabinet order."));
	NEW_VALIDATION (_T ("File"), _T ("Attributes"), _T ("Y"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Integer containing bit flags representing file attributes (with the decimal value of each bit position in parentheses)"));
	NEW_VALIDATION (_T ("File"), _T ("File"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Primary key, non-localized token, must match identifier in cabinet.  For uncompressed files, this field is ignored."));
	NEW_VALIDATION (_T ("File"), _T ("Component_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Component"), 1, _T ("Identifier"), _T (""), _T ("Foreign key referencing Component that controls the file."));
	NEW_VALIDATION (_T ("File"), _T ("FileName"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Filename"), _T (""), _T ("File name used for installation, may be localized.  This may contain a \"short name|long name\" pair."));
	NEW_VALIDATION (_T ("File"), _T ("FileSize"), _T ("N"), 0, 2147483647, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Size of file in bytes (long integer)."));
	NEW_VALIDATION (_T ("File"), _T ("Language"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Language"), _T (""), _T ("List of decimal language Ids, comma-separated if more than one."));
	NEW_VALIDATION (_T ("File"), _T ("Version"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("File"), 1, _T ("Version"), _T (""), _T ("Version string for versioned files;  Blank for unversioned files."));
	NEW_VALIDATION (_T ("Class"), _T ("Attributes"), _T ("Y"), MSI_NULL_INTEGER, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Class registration attributes."));
	NEW_VALIDATION (_T ("Class"), _T ("Feature_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Feature"), 1, _T ("Identifier"), _T (""), _T ("Required foreign key into the Feature Table, specifying the feature to validate or install in order for the CLSID factory to be operational."));
	NEW_VALIDATION (_T ("Class"), _T ("Description"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("Localized description for the Class."));
	NEW_VALIDATION (_T ("Class"), _T ("Argument"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("optional argument for LocalServers."));
	NEW_VALIDATION (_T ("Class"), _T ("AppId_"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("AppId"), 1, _T ("Guid"), _T (""), _T ("Optional AppID containing DCOM information for associated application (string GUID)."));
	NEW_VALIDATION (_T ("Class"), _T ("CLSID"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Guid"), _T (""), _T ("The CLSID of an OLE factory."));
	NEW_VALIDATION (_T ("Class"), _T ("Component_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Component"), 1, _T ("Identifier"), _T (""), _T ("Required foreign key into the Component Table, specifying the component for which to return a path when called through LocateComponent."));
	NEW_VALIDATION (_T ("Class"), _T ("Context"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("The numeric server context for this server. CLSCTX_xxxx"));
	NEW_VALIDATION (_T ("Class"), _T ("DefInprocHandler"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Filename"), _T ("1;2;3"), _T ("Optional default inproc handler.  Only optionally provided if Context=CLSCTX_LOCAL_SERVER.  Typically \"ole32.dll\" or \"mapi32.dll\""));
	NEW_VALIDATION (_T ("Class"), _T ("FileTypeMask"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("Optional string containing information for the HKCRthis CLSID) key. If multiple patterns exist, they must be delimited by a semicolon, and numeric subkeys will be generated: 0,1,2..."));
	NEW_VALIDATION (_T ("Class"), _T ("Icon_"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Icon"), 1, _T ("Identifier"), _T (""), _T ("Optional foreign key into the Icon Table, specifying the icon file associated with this CLSID. Will be written under the DefaultIcon key."));
	NEW_VALIDATION (_T ("Class"), _T ("IconIndex"), _T ("Y"), -32767, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Optional icon index."));
	NEW_VALIDATION (_T ("Class"), _T ("ProgId_Default"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("ProgId"), 1, _T ("Text"), _T (""), _T ("Optional ProgId associated with this CLSID."));
	NEW_VALIDATION (_T ("Component"), _T ("Condition"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Condition"), _T (""), _T ("A conditional statement that will disable this component if the specified condition evaluates to the 'True' state. If a component is disabled, it will not be installed, regardless of the 'Action' state associated with the component."));
	NEW_VALIDATION (_T ("Component"), _T ("Attributes"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Remote execution option, one of irsEnum"));
	NEW_VALIDATION (_T ("Component"), _T ("Component"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Primary key used to identify a particular component record."));
	NEW_VALIDATION (_T ("Component"), _T ("ComponentId"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Guid"), _T (""), _T ("A string GUID unique to this component, version, and language."));
	NEW_VALIDATION (_T ("Component"), _T ("Directory_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Directory"), 1, _T ("Identifier"), _T (""), _T ("Required key of a Directory table record. This is actually a property name whose value contains the actual path, set either by the AppSearch action or with the default setting obtained from the Directory table."));
	NEW_VALIDATION (_T ("Component"), _T ("KeyPath"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("File;Registry;ODBCDataSource"), 1, _T ("Identifier"), _T (""), _T ("Either the primary key into the File table, Registry table, or ODBCDataSource table. This extract path is stored when the component is installed, and is used to detect the presence of the component and to return the path to it."));
	NEW_VALIDATION (_T ("ProgId"), _T ("Description"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("Localized description for the Program identifier."));
	NEW_VALIDATION (_T ("ProgId"), _T ("Icon_"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Icon"), 1, _T ("Identifier"), _T (""), _T ("Optional foreign key into the Icon Table, specifying the icon file associated with this ProgId. Will be written under the DefaultIcon key."));
	NEW_VALIDATION (_T ("ProgId"), _T ("IconIndex"), _T ("Y"), -32767, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Optional icon index."));
	NEW_VALIDATION (_T ("ProgId"), _T ("ProgId"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("The Program Identifier. Primary key."));
	NEW_VALIDATION (_T ("ProgId"), _T ("Class_"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Class"), 1, _T ("Guid"), _T (""), _T ("The CLSID of an OLE factory corresponding to the ProgId."));
	NEW_VALIDATION (_T ("ProgId"), _T ("ProgId_Parent"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("ProgId"), 1, _T ("Text"), _T (""), _T ("The Parent Program Identifier. If specified, the ProgId column becomes a version independent prog id."));
	NEW_VALIDATION (_T ("CompLocator"), _T ("Type"), _T ("Y"), 0, 1, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("A boolean value that determines if the registry value is a filename or a directory location."));
	NEW_VALIDATION (_T ("CompLocator"), _T ("Signature_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("The table key. The Signature_ represents a unique file signature and is also the foreign key in the Signature table."));
	NEW_VALIDATION (_T ("CompLocator"), _T ("ComponentId"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Guid"), _T (""), _T ("A string GUID unique to this component, version, and language."));
	NEW_VALIDATION (_T ("Complus"), _T ("Component_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Component"), 1, _T ("Identifier"), _T (""), _T ("Foreign key referencing Component that controls the ComPlus component."));
	NEW_VALIDATION (_T ("Complus"), _T ("ExpType"), _T ("Y"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("ComPlus component attributes."));
	NEW_VALIDATION (_T ("Directory"), _T ("Directory"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Unique identifier for directory entry, primary key. If a property by this name is defined, it contains the full path to the directory."));
	NEW_VALIDATION (_T ("Directory"), _T ("DefaultDir"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("DefaultDir"), _T (""), _T ("The default sub-path under parent's path."));
	NEW_VALIDATION (_T ("Directory"), _T ("Directory_Parent"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Directory"), 1, _T ("Identifier"), _T (""), _T ("Reference to the entry in this table specifying the default parent directory. A record parented to itself or with a Null parent represents a root of the install tree."));
	NEW_VALIDATION (_T ("CreateFolder"), _T ("Component_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Component"), 1, _T ("Identifier"), _T (""), _T ("Foreign key into the Component table."));
	NEW_VALIDATION (_T ("CreateFolder"), _T ("Directory_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Directory"), 1, _T ("Identifier"), _T (""), _T ("Primary key, could be foreign key into the Directory table."));
	NEW_VALIDATION (_T ("CustomAction"), _T ("Type"), _T ("N"), 1, 16383, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("The numeric custom action type, consisting of source location, code type, entry, option flags."));
	NEW_VALIDATION (_T ("CustomAction"), _T ("Action"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Primary key, name of action, normally appears in sequence table unless private use."));
	NEW_VALIDATION (_T ("CustomAction"), _T ("Source"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("CustomSource"), _T (""), _T ("The table reference of the source of the code."));
	NEW_VALIDATION (_T ("CustomAction"), _T ("Target"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("Excecution parameter, depends on the type of custom action"));
	NEW_VALIDATION (_T ("DrLocator"), _T ("Signature_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("The Signature_ represents a unique file signature and is also the foreign key in the Signature table."));
	NEW_VALIDATION (_T ("DrLocator"), _T ("Path"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("AnyPath"), _T (""), _T ("The path on the user system. This is a either a subpath below the value of the Parent or a full path. The path may contain properties enclosed within [ ] that will be expanded."));
	NEW_VALIDATION (_T ("DrLocator"), _T ("Depth"), _T ("Y"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("The depth below the path to which the Signature_ is recursively searched. If absent, the depth is assumed to be 0."));
	NEW_VALIDATION (_T ("DrLocator"), _T ("Parent"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("The parent file signature. It is also a foreign key in the Signature table. If null and the Path column does not expand to a full path, then all the fixed drives of the user system are searched using the Path."));
	NEW_VALIDATION (_T ("DuplicateFile"), _T ("File_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("File"), 1, _T ("Identifier"), _T (""), _T ("Foreign key referencing the source file to be duplicated."));
	NEW_VALIDATION (_T ("DuplicateFile"), _T ("Component_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Component"), 1, _T ("Identifier"), _T (""), _T ("Foreign key referencing Component that controls the duplicate file."));
	NEW_VALIDATION (_T ("DuplicateFile"), _T ("DestFolder"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Name of a property whose value is assumed to resolve to the full pathname to a destination folder."));
	NEW_VALIDATION (_T ("DuplicateFile"), _T ("DestName"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Filename"), _T (""), _T ("Filename to be given to the duplicate file."));
	NEW_VALIDATION (_T ("DuplicateFile"), _T ("FileKey"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Primary key used to identify a particular file entry"));
	NEW_VALIDATION (_T ("Environment"), _T ("Name"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("The name of the environmental value."));
	NEW_VALIDATION (_T ("Environment"), _T ("Value"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("The value to set in the environmental settings."));
	NEW_VALIDATION (_T ("Environment"), _T ("Component_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Component"), 1, _T ("Identifier"), _T (""), _T ("Foreign key into the Component table referencing component that controls the installing of the environmental value."));
	NEW_VALIDATION (_T ("Environment"), _T ("Environment"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Unique identifier for the environmental variable setting"));
	NEW_VALIDATION (_T ("Error"), _T ("Error"), _T ("N"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Integer error number, obtained from header file IError(...) macros."));
	NEW_VALIDATION (_T ("Error"), _T ("Message"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Template"), _T (""), _T ("Error formatting template, obtained from user ed. or localizers."));
	NEW_VALIDATION (_T ("Extension"), _T ("Feature_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Feature"), 1, _T ("Identifier"), _T (""), _T ("Required foreign key into the Feature Table, specifying the feature to validate or install in order for the CLSID factory to be operational."));
	NEW_VALIDATION (_T ("Extension"), _T ("Component_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Component"), 1, _T ("Identifier"), _T (""), _T ("Required foreign key into the Component Table, specifying the component for which to return a path when called through LocateComponent."));
	NEW_VALIDATION (_T ("Extension"), _T ("Extension"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("The extension associated with the table row."));
	NEW_VALIDATION (_T ("Extension"), _T ("MIME_"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("MIME"), 1, _T ("Text"), _T (""), _T ("Optional Context identifier, typically \"type/format\" associated with the extension"));
	NEW_VALIDATION (_T ("Extension"), _T ("ProgId_"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("ProgId"), 1, _T ("Text"), _T (""), _T ("Optional ProgId associated with this extension."));
	NEW_VALIDATION (_T ("MIME"), _T ("CLSID"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Guid"), _T (""), _T ("Optional associated CLSID."));
	NEW_VALIDATION (_T ("MIME"), _T ("ContentType"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("Primary key. Context identifier, typically \"type/format\"."));
	NEW_VALIDATION (_T ("MIME"), _T ("Extension_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Extension"), 1, _T ("Text"), _T (""), _T ("Optional associated extension (without dot)"));
	NEW_VALIDATION (_T ("FeatureComponents"), _T ("Feature_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Feature"), 1, _T ("Identifier"), _T (""), _T ("Foreign key into Feature table."));
	NEW_VALIDATION (_T ("FeatureComponents"), _T ("Component_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Component"), 1, _T ("Identifier"), _T (""), _T ("Foreign key into Component table."));
	NEW_VALIDATION (_T ("FileSFPCatalog"), _T ("File_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("File"), 1, _T ("Identifier"), _T (""), _T ("File associated with the catalog"));
	NEW_VALIDATION (_T ("FileSFPCatalog"), _T ("SFPCatalog_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("SFPCatalog"), 1, _T ("Filename"), _T (""), _T ("Catalog associated with the file"));
	NEW_VALIDATION (_T ("SFPCatalog"), _T ("SFPCatalog"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Filename"), _T (""), _T ("File name for the catalog."));
	NEW_VALIDATION (_T ("SFPCatalog"), _T ("Catalog"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Binary"), _T (""), _T ("SFP Catalog"));
	NEW_VALIDATION (_T ("SFPCatalog"), _T ("Dependency"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("Parent catalog - only used by SFP"));
	NEW_VALIDATION (_T ("Font"), _T ("File_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("File"), 1, _T ("Identifier"), _T (""), _T ("Primary key, foreign key into File table referencing font file."));
	NEW_VALIDATION (_T ("Font"), _T ("FontTitle"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("Font name."));
	NEW_VALIDATION (_T ("IniFile"), _T ("Action"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T (""), _T ("0;1;3"), _T ("The type of modification to be made, one of iifEnum"));
	NEW_VALIDATION (_T ("IniFile"), _T ("Value"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("The value to be written."));
	NEW_VALIDATION (_T ("IniFile"), _T ("Key"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("The .INI file key below Section."));
	NEW_VALIDATION (_T ("IniFile"), _T ("Component_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Component"), 1, _T ("Identifier"), _T (""), _T ("Foreign key into the Component table referencing component that controls the installing of the .INI value."));
	NEW_VALIDATION (_T ("IniFile"), _T ("FileName"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Filename"), _T (""), _T ("The .INI file name in which to write the information"));
	NEW_VALIDATION (_T ("IniFile"), _T ("IniFile"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Primary key, non-localized token."));
	NEW_VALIDATION (_T ("IniFile"), _T ("DirProperty"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Foreign key into the Directory table denoting the directory where the .INI file is."));
	NEW_VALIDATION (_T ("IniFile"), _T ("Section"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("The .INI file Section."));
	NEW_VALIDATION (_T ("IniLocator"), _T ("Type"), _T ("Y"), 0, 2, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("An integer value that determines if the .INI value read is a filename or a directory location or to be used as is w/o interpretation."));
	NEW_VALIDATION (_T ("IniLocator"), _T ("Key"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("Key value (followed by an equals sign in INI file)."));
	NEW_VALIDATION (_T ("IniLocator"), _T ("Signature_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("The table key. The Signature_ represents a unique file signature and is also the foreign key in the Signature table."));
	NEW_VALIDATION (_T ("IniLocator"), _T ("FileName"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Filename"), _T (""), _T ("The .INI file name."));
	NEW_VALIDATION (_T ("IniLocator"), _T ("Section"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("Section name within in file (within square brackets in INI file)."));
	NEW_VALIDATION (_T ("IniLocator"), _T ("Field"), _T ("Y"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("The field in the .INI line. If Field is null or 0 the entire line is read."));
	NEW_VALIDATION (_T ("IsolatedComponent"), _T ("Component_Application"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Component"), 1, _T ("Identifier"), _T (""), _T ("Key to Component table item for application"));
	NEW_VALIDATION (_T ("IsolatedComponent"), _T ("Component_Shared"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Component"), 1, _T ("Identifier"), _T (""), _T ("Key to Component table item to be isolated"));
	NEW_VALIDATION (_T ("LaunchCondition"), _T ("Condition"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Condition"), _T (""), _T ("Expression which must evaluate to TRUE in order for install to commence."));
	NEW_VALIDATION (_T ("LaunchCondition"), _T ("Description"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("Localizable text to display when condition fails and install must abort."));
	NEW_VALIDATION (_T ("LockPermissions"), _T ("Table"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T ("Directory;File;Registry"), _T ("Reference to another table name"));
	NEW_VALIDATION (_T ("LockPermissions"), _T ("Domain"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("Domain name for user whose permissions are being set. (usually a property)"));
	NEW_VALIDATION (_T ("LockPermissions"), _T ("LockObject"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Foreign key into Registry or File table"));
	NEW_VALIDATION (_T ("LockPermissions"), _T ("Permission"), _T ("Y"), -2147483647, 2147483647, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Permission Access mask.  Full Control = 268435456 (GENERIC_ALL = 0x10000000)"));
	NEW_VALIDATION (_T ("LockPermissions"), _T ("User"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("User for permissions to be set.  (usually a property)"));
	NEW_VALIDATION (_T ("Media"), _T ("Source"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Property"), _T (""), _T ("The property defining the location of the cabinet file."));
	NEW_VALIDATION (_T ("Media"), _T ("Cabinet"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Cabinet"), _T (""), _T ("If some or all of the files stored on the media are compressed in a cabinet, the name of that cabinet."));
	NEW_VALIDATION (_T ("Media"), _T ("DiskId"), _T ("N"), 1, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Primary key, integer to determine sort order for table."));
	NEW_VALIDATION (_T ("Media"), _T ("DiskPrompt"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("Disk name: the visible text actually printed on the disk.  This will be used to prompt the user when this disk needs to be inserted."));
	NEW_VALIDATION (_T ("Media"), _T ("LastSequence"), _T ("N"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("File sequence number for the last file for this media."));
	NEW_VALIDATION (_T ("Media"), _T ("VolumeLabel"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("The label attributed to the volume."));
	NEW_VALIDATION (_T ("ModuleComponents"), _T ("Component"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Component"), 1, _T ("Identifier"), _T (""), _T ("Component contained in the module."));
	NEW_VALIDATION (_T ("ModuleComponents"), _T ("Language"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("ModuleSignature"), 2, _T (""), _T (""), _T ("Default language ID for module (may be changed by transform)."));
	NEW_VALIDATION (_T ("ModuleComponents"), _T ("ModuleID"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("ModuleSignature"), 1, _T ("Identifier"), _T (""), _T ("Module containing the component."));
	NEW_VALIDATION (_T ("ModuleSignature"), _T ("Language"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Default decimal language of module."));
	NEW_VALIDATION (_T ("ModuleSignature"), _T ("Version"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Version"), _T (""), _T ("Version of the module."));
	NEW_VALIDATION (_T ("ModuleSignature"), _T ("ModuleID"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Module identifier (String.GUID)."));
	NEW_VALIDATION (_T ("ModuleDependency"), _T ("ModuleID"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("ModuleSignature"), 1, _T ("Identifier"), _T (""), _T ("Module requiring the dependency."));
	NEW_VALIDATION (_T ("ModuleDependency"), _T ("ModuleLanguage"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("ModuleSignature"), 2, _T (""), _T (""), _T ("Language of module requiring the dependency."));
	NEW_VALIDATION (_T ("ModuleDependency"), _T ("RequiredID"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("String.GUID of required module."));
	NEW_VALIDATION (_T ("ModuleDependency"), _T ("RequiredLanguage"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("LanguageID of the required module."));
	NEW_VALIDATION (_T ("ModuleDependency"), _T ("RequiredVersion"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Version"), _T (""), _T ("Version of the required version."));
	NEW_VALIDATION (_T ("ModuleExclusion"), _T ("ModuleID"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("ModuleSignature"), 1, _T ("Identifier"), _T (""), _T ("String.GUID of module with exclusion requirement."));
	NEW_VALIDATION (_T ("ModuleExclusion"), _T ("ModuleLanguage"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("ModuleSignature"), 2, _T (""), _T (""), _T ("LanguageID of module with exclusion requirement."));
	NEW_VALIDATION (_T ("ModuleExclusion"), _T ("ExcludedID"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("String.GUID of excluded module."));
	NEW_VALIDATION (_T ("ModuleExclusion"), _T ("ExcludedLanguage"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Language of excluded module."));
	NEW_VALIDATION (_T ("ModuleExclusion"), _T ("ExcludedMaxVersion"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Version"), _T (""), _T ("Maximum version of excluded module."));
	NEW_VALIDATION (_T ("ModuleExclusion"), _T ("ExcludedMinVersion"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Version"), _T (""), _T ("Minimum version of excluded module."));
	NEW_VALIDATION (_T ("MoveFile"), _T ("Component_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Component"), 1, _T ("Identifier"), _T (""), _T ("If this component is not \"selected\" for installation or removal, no action will be taken on the associated MoveFile entry"));
	NEW_VALIDATION (_T ("MoveFile"), _T ("DestFolder"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Name of a property whose value is assumed to resolve to the full path to the destination directory"));
	NEW_VALIDATION (_T ("MoveFile"), _T ("DestName"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Filename"), _T (""), _T ("Name to be given to the original file after it is moved or copied.  If blank, the destination file will be given the same name as the source file"));
	NEW_VALIDATION (_T ("MoveFile"), _T ("FileKey"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Primary key that uniquely identifies a particular MoveFile record"));
	NEW_VALIDATION (_T ("MoveFile"), _T ("Options"), _T ("N"), 0, 1, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Integer value specifying the MoveFile operating mode, one of imfoEnum"));
	NEW_VALIDATION (_T ("MoveFile"), _T ("SourceFolder"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Name of a property whose value is assumed to resolve to the full path to the source directory"));
	NEW_VALIDATION (_T ("MoveFile"), _T ("SourceName"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("Name of the source file(s) to be moved or copied.  Can contain the '*' or '?' wildcards."));
	NEW_VALIDATION (_T ("MsiAssembly"), _T ("Attributes"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Assembly attributes"));
	NEW_VALIDATION (_T ("MsiAssembly"), _T ("Feature_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Feature"), 1, _T ("Identifier"), _T (""), _T ("Foreign key into Feature table."));
	NEW_VALIDATION (_T ("MsiAssembly"), _T ("Component_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Component"), 1, _T ("Identifier"), _T (""), _T ("Foreign key into Component table."));
	NEW_VALIDATION (_T ("MsiAssembly"), _T ("File_Application"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("File"), 1, _T ("Identifier"), _T (""), _T ("Foreign key into File table, denoting the application context for private assemblies. Null for global assemblies."));
	NEW_VALIDATION (_T ("MsiAssembly"), _T ("File_Manifest"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("File"), 1, _T ("Identifier"), _T (""), _T ("Foreign key into the File table denoting the manifest file for the assembly."));
	NEW_VALIDATION (_T ("MsiAssemblyName"), _T ("Name"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("The name part of the name-value pairs for the assembly name."));
	NEW_VALIDATION (_T ("MsiAssemblyName"), _T ("Value"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("The value part of the name-value pairs for the assembly name."));
	NEW_VALIDATION (_T ("MsiAssemblyName"), _T ("Component_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Component"), 1, _T ("Identifier"), _T (""), _T ("Foreign key into Component table."));
	NEW_VALIDATION (_T ("MsiDigitalCertificate"), _T ("CertData"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Binary"), _T (""), _T ("A certificate context blob for a signer certificate"));
	NEW_VALIDATION (_T ("MsiDigitalCertificate"), _T ("DigitalCertificate"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("A unique identifier for the row"));
	NEW_VALIDATION (_T ("MsiDigitalSignature"), _T ("Table"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T (""), _T ("Media"), _T ("Reference to another table name (only Media table is supported)"));
	NEW_VALIDATION (_T ("MsiDigitalSignature"), _T ("DigitalCertificate_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("MsiDigitalCertificate"), 1, _T ("Identifier"), _T (""), _T ("Foreign key to MsiDigitalCertificate table identifying the signer certificate"));
	NEW_VALIDATION (_T ("MsiDigitalSignature"), _T ("Hash"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Binary"), _T (""), _T ("The encoded hash blob from the digital signature"));
	NEW_VALIDATION (_T ("MsiDigitalSignature"), _T ("SignObject"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("Foreign key to Media table"));
	NEW_VALIDATION (_T ("MsiFileHash"), _T ("File_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("File"), 1, _T ("Identifier"), _T (""), _T ("Primary key, foreign key into File table referencing file with this hash"));
	NEW_VALIDATION (_T ("MsiFileHash"), _T ("Options"), _T ("N"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Various options and attributes for this hash."));
	NEW_VALIDATION (_T ("MsiFileHash"), _T ("HashPart1"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Size of file in bytes (long integer)."));
	NEW_VALIDATION (_T ("MsiFileHash"), _T ("HashPart2"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Size of file in bytes (long integer)."));
	NEW_VALIDATION (_T ("MsiFileHash"), _T ("HashPart3"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Size of file in bytes (long integer)."));
	NEW_VALIDATION (_T ("MsiFileHash"), _T ("HashPart4"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Size of file in bytes (long integer)."));
	NEW_VALIDATION (_T ("MsiPatchHeaders"), _T ("StreamRef"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Primary key. A unique identifier for the row."));
	NEW_VALIDATION (_T ("MsiPatchHeaders"), _T ("Header"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Binary"), _T (""), _T ("Binary stream. The patch header, used for patch validation."));
	NEW_VALIDATION (_T ("ODBCAttribute"), _T ("Value"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("Value for ODBC driver attribute"));
	NEW_VALIDATION (_T ("ODBCAttribute"), _T ("Attribute"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("Name of ODBC driver attribute"));
	NEW_VALIDATION (_T ("ODBCAttribute"), _T ("Driver_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("ODBCDriver"), 1, _T ("Identifier"), _T (""), _T ("Reference to ODBC driver in ODBCDriver table"));
	NEW_VALIDATION (_T ("ODBCDriver"), _T ("Description"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("Text used as registered name for driver, non-localized"));
	NEW_VALIDATION (_T ("ODBCDriver"), _T ("File_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("File"), 1, _T ("Identifier"), _T (""), _T ("Reference to key driver file"));
	NEW_VALIDATION (_T ("ODBCDriver"), _T ("Component_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Component"), 1, _T ("Identifier"), _T (""), _T ("Reference to associated component"));
	NEW_VALIDATION (_T ("ODBCDriver"), _T ("Driver"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Primary key, non-localized.internal token for driver"));
	NEW_VALIDATION (_T ("ODBCDriver"), _T ("File_Setup"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("File"), 1, _T ("Identifier"), _T (""), _T ("Optional reference to key driver setup DLL"));
	NEW_VALIDATION (_T ("ODBCDataSource"), _T ("Description"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("Text used as registered name for data source"));
	NEW_VALIDATION (_T ("ODBCDataSource"), _T ("Component_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Component"), 1, _T ("Identifier"), _T (""), _T ("Reference to associated component"));
	NEW_VALIDATION (_T ("ODBCDataSource"), _T ("DataSource"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Primary key, non-localized.internal token for data source"));
	NEW_VALIDATION (_T ("ODBCDataSource"), _T ("DriverDescription"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("Reference to driver description, may be existing driver"));
	NEW_VALIDATION (_T ("ODBCDataSource"), _T ("Registration"), _T ("N"), 0, 1, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Registration option: 0=machine, 1=user, others t.b.d."));
	NEW_VALIDATION (_T ("ODBCSourceAttribute"), _T ("Value"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("Value for ODBC data source attribute"));
	NEW_VALIDATION (_T ("ODBCSourceAttribute"), _T ("Attribute"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("Name of ODBC data source attribute"));
	NEW_VALIDATION (_T ("ODBCSourceAttribute"), _T ("DataSource_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("ODBCDataSource"), 1, _T ("Identifier"), _T (""), _T ("Reference to ODBC data source in ODBCDataSource table"));
	NEW_VALIDATION (_T ("ODBCTranslator"), _T ("Description"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("Text used as registered name for translator"));
	NEW_VALIDATION (_T ("ODBCTranslator"), _T ("File_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("File"), 1, _T ("Identifier"), _T (""), _T ("Reference to key translator file"));
	NEW_VALIDATION (_T ("ODBCTranslator"), _T ("Component_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Component"), 1, _T ("Identifier"), _T (""), _T ("Reference to associated component"));
	NEW_VALIDATION (_T ("ODBCTranslator"), _T ("File_Setup"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("File"), 1, _T ("Identifier"), _T (""), _T ("Optional reference to key translator setup DLL"));
	NEW_VALIDATION (_T ("ODBCTranslator"), _T ("Translator"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Primary key, non-localized.internal token for translator"));
	NEW_VALIDATION (_T ("Patch"), _T ("Sequence"), _T ("N"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Primary key, sequence with respect to the media images; order must track cabinet order."));
	NEW_VALIDATION (_T ("Patch"), _T ("Attributes"), _T ("N"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Integer containing bit flags representing patch attributes"));
	NEW_VALIDATION (_T ("Patch"), _T ("File_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Primary key, non-localized token, foreign key to File table, must match identifier in cabinet."));
	NEW_VALIDATION (_T ("Patch"), _T ("Header"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Binary"), _T (""), _T ("Binary stream. The patch header, used for patch validation."));
	NEW_VALIDATION (_T ("Patch"), _T ("PatchSize"), _T ("N"), 0, 2147483647, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Size of patch in bytes (long integer)."));
	NEW_VALIDATION (_T ("Patch"), _T ("StreamRef_"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Identifier. Foreign key to the StreamRef column of the MsiPatchHeaders table."));
	NEW_VALIDATION (_T ("PatchPackage"), _T ("Media_"), _T ("N"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Foreign key to DiskId column of Media table. Indicates the disk containing the patch package."));
	NEW_VALIDATION (_T ("PatchPackage"), _T ("PatchId"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Guid"), _T (""), _T ("A unique string GUID representing this patch."));
	NEW_VALIDATION (_T ("PublishComponent"), _T ("Feature_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Feature"), 1, _T ("Identifier"), _T (""), _T ("Foreign key into the Feature table."));
	NEW_VALIDATION (_T ("PublishComponent"), _T ("Component_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Component"), 1, _T ("Identifier"), _T (""), _T ("Foreign key into the Component table."));
	NEW_VALIDATION (_T ("PublishComponent"), _T ("ComponentId"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Guid"), _T (""), _T ("A string GUID that represents the component id that will be requested by the alien product."));
	NEW_VALIDATION (_T ("PublishComponent"), _T ("AppData"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("This is localisable Application specific data that can be associated with a Qualified Component."));
	NEW_VALIDATION (_T ("PublishComponent"), _T ("Qualifier"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("This is defined only when the ComponentId column is an Qualified Component Id. This is the Qualifier for ProvideComponentIndirect."));
	NEW_VALIDATION (_T ("Registry"), _T ("Name"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("The registry value name."));
	NEW_VALIDATION (_T ("Registry"), _T ("Value"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("The registry value."));
	NEW_VALIDATION (_T ("Registry"), _T ("Key"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("RegPath"), _T (""), _T ("The key for the registry value."));
	NEW_VALIDATION (_T ("Registry"), _T ("Component_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Component"), 1, _T ("Identifier"), _T (""), _T ("Foreign key into the Component table referencing component that controls the installing of the registry value."));
	NEW_VALIDATION (_T ("Registry"), _T ("Registry"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Primary key, non-localized token."));
	NEW_VALIDATION (_T ("Registry"), _T ("Root"), _T ("N"), -1, 3, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("The predefined root key for the registry value, one of rrkEnum."));
	NEW_VALIDATION (_T ("RegLocator"), _T ("Name"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("The registry value name."));
	NEW_VALIDATION (_T ("RegLocator"), _T ("Type"), _T ("Y"), 0, 18, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("An integer value that determines if the registry value is a filename or a directory location or to be used as is w/o interpretation."));
	NEW_VALIDATION (_T ("RegLocator"), _T ("Key"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("RegPath"), _T (""), _T ("The key for the registry value."));
	NEW_VALIDATION (_T ("RegLocator"), _T ("Signature_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("The table key. The Signature_ represents a unique file signature and is also the foreign key in the Signature table. If the type is 0, the registry values refers a directory, and _Signature is not a foreign key."));
	NEW_VALIDATION (_T ("RegLocator"), _T ("Root"), _T ("N"), 0, 3, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("The predefined root key for the registry value, one of rrkEnum."));
	NEW_VALIDATION (_T ("RemoveFile"), _T ("InstallMode"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T (""), _T ("1;2;3"), _T ("Installation option, one of iimEnum."));
	NEW_VALIDATION (_T ("RemoveFile"), _T ("Component_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Component"), 1, _T ("Identifier"), _T (""), _T ("Foreign key referencing Component that controls the file to be removed."));
	NEW_VALIDATION (_T ("RemoveFile"), _T ("FileKey"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Primary key used to identify a particular file entry"));
	NEW_VALIDATION (_T ("RemoveFile"), _T ("FileName"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("WildCardFilename"), _T (""), _T ("Name of the file to be removed."));
	NEW_VALIDATION (_T ("RemoveFile"), _T ("DirProperty"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Name of a property whose value is assumed to resolve to the full pathname to the folder of the file to be removed."));
	NEW_VALIDATION (_T ("RemoveIniFile"), _T ("Action"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T (""), _T ("2;4"), _T ("The type of modification to be made, one of iifEnum."));
	NEW_VALIDATION (_T ("RemoveIniFile"), _T ("Value"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("The value to be deleted. The value is required when Action is iifIniRemoveTag"));
	NEW_VALIDATION (_T ("RemoveIniFile"), _T ("Key"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("The .INI file key below Section."));
	NEW_VALIDATION (_T ("RemoveIniFile"), _T ("Component_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Component"), 1, _T ("Identifier"), _T (""), _T ("Foreign key into the Component table referencing component that controls the deletion of the .INI value."));
	NEW_VALIDATION (_T ("RemoveIniFile"), _T ("FileName"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Filename"), _T (""), _T ("The .INI file name in which to delete the information"));
	NEW_VALIDATION (_T ("RemoveIniFile"), _T ("DirProperty"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Foreign key into the Directory table denoting the directory where the .INI file is."));
	NEW_VALIDATION (_T ("RemoveIniFile"), _T ("Section"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("The .INI file Section."));
	NEW_VALIDATION (_T ("RemoveIniFile"), _T ("RemoveIniFile"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Primary key, non-localized token."));
	NEW_VALIDATION (_T ("RemoveRegistry"), _T ("Name"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("The registry value name."));
	NEW_VALIDATION (_T ("RemoveRegistry"), _T ("Key"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("RegPath"), _T (""), _T ("The key for the registry value."));
	NEW_VALIDATION (_T ("RemoveRegistry"), _T ("Component_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Component"), 1, _T ("Identifier"), _T (""), _T ("Foreign key into the Component table referencing component that controls the deletion of the registry value."));
	NEW_VALIDATION (_T ("RemoveRegistry"), _T ("Root"), _T ("N"), -1, 3, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("The predefined root key for the registry value, one of rrkEnum"));
	NEW_VALIDATION (_T ("RemoveRegistry"), _T ("RemoveRegistry"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Primary key, non-localized token."));
	NEW_VALIDATION (_T ("ReserveCost"), _T ("Component_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Component"), 1, _T ("Identifier"), _T (""), _T ("Reserve a specified amount of space if this component is to be installed."));
	NEW_VALIDATION (_T ("ReserveCost"), _T ("ReserveFolder"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Name of a property whose value is assumed to resolve to the full path to the destination directory"));
	NEW_VALIDATION (_T ("ReserveCost"), _T ("ReserveKey"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Primary key that uniquely identifies a particular ReserveCost record"));
	NEW_VALIDATION (_T ("ReserveCost"), _T ("ReserveLocal"), _T ("N"), 0, 2147483647, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Disk space to reserve if linked component is installed locally."));
	NEW_VALIDATION (_T ("ReserveCost"), _T ("ReserveSource"), _T ("N"), 0, 2147483647, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Disk space to reserve if linked component is installed to run from the source location."));
	NEW_VALIDATION (_T ("SelfReg"), _T ("File_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("File"), 1, _T ("Identifier"), _T (""), _T ("Foreign key into the File table denoting the module that needs to be registered."));
	NEW_VALIDATION (_T ("SelfReg"), _T ("Cost"), _T ("Y"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("The cost of registering the module."));
	NEW_VALIDATION (_T ("ServiceControl"), _T ("Name"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("Name of a service. /, \\, comma and space are invalid"));
	NEW_VALIDATION (_T ("ServiceControl"), _T ("Event"), _T ("N"), 0, 187, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Bit field:  Install:  0x1 = Start, 0x2 = Stop, 0x8 = Delete, Uninstall: 0x10 = Start, 0x20 = Stop, 0x80 = Delete"));
	NEW_VALIDATION (_T ("ServiceControl"), _T ("Component_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Component"), 1, _T ("Identifier"), _T (""), _T ("Required foreign key into the Component Table that controls the startup of the service"));
	NEW_VALIDATION (_T ("ServiceControl"), _T ("ServiceControl"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Primary key, non-localized token."));
	NEW_VALIDATION (_T ("ServiceControl"), _T ("Arguments"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("Arguments for the service.  Separate by [~]."));
	NEW_VALIDATION (_T ("ServiceControl"), _T ("Wait"), _T ("Y"), 0, 1, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Boolean for whether to wait for the service to fully start"));
	NEW_VALIDATION (_T ("ServiceInstall"), _T ("Name"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("Internal Name of the Service"));
	NEW_VALIDATION (_T ("ServiceInstall"), _T ("Description"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("Description of service."));
	NEW_VALIDATION (_T ("ServiceInstall"), _T ("Component_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Component"), 1, _T ("Identifier"), _T (""), _T ("Required foreign key into the Component Table that controls the startup of the service"));
	NEW_VALIDATION (_T ("ServiceInstall"), _T ("Arguments"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("Arguments to include in every start of the service, passed to WinMain"));
	NEW_VALIDATION (_T ("ServiceInstall"), _T ("ServiceInstall"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Primary key, non-localized token."));
	NEW_VALIDATION (_T ("ServiceInstall"), _T ("Dependencies"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("Other services this depends on to start.  Separate by [~], and end with [~][~]"));
	NEW_VALIDATION (_T ("ServiceInstall"), _T ("DisplayName"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("External Name of the Service"));
	NEW_VALIDATION (_T ("ServiceInstall"), _T ("ErrorControl"), _T ("N"), -2147483647, 2147483647, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Severity of error if service fails to start"));
	NEW_VALIDATION (_T ("ServiceInstall"), _T ("LoadOrderGroup"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("LoadOrderGroup"));
	NEW_VALIDATION (_T ("ServiceInstall"), _T ("Password"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("password to run service with.  (with StartName)"));
	NEW_VALIDATION (_T ("ServiceInstall"), _T ("ServiceType"), _T ("N"), -2147483647, 2147483647, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Type of the service"));
	NEW_VALIDATION (_T ("ServiceInstall"), _T ("StartName"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("User or object name to run service as"));
	NEW_VALIDATION (_T ("ServiceInstall"), _T ("StartType"), _T ("N"), 0, 4, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Type of the service"));
	NEW_VALIDATION (_T ("Shortcut"), _T ("Name"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Filename"), _T (""), _T ("The name of the shortcut to be created."));
	NEW_VALIDATION (_T ("Shortcut"), _T ("Description"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("The description for the shortcut."));
	NEW_VALIDATION (_T ("Shortcut"), _T ("Component_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Component"), 1, _T ("Identifier"), _T (""), _T ("Foreign key into the Component table denoting the component whose selection gates the the shortcut creation/deletion."));
	NEW_VALIDATION (_T ("Shortcut"), _T ("Icon_"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Icon"), 1, _T ("Identifier"), _T (""), _T ("Foreign key into the File table denoting the external icon file for the shortcut."));
	NEW_VALIDATION (_T ("Shortcut"), _T ("IconIndex"), _T ("Y"), -32767, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("The icon index for the shortcut."));
	NEW_VALIDATION (_T ("Shortcut"), _T ("Directory_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Directory"), 1, _T ("Identifier"), _T (""), _T ("Foreign key into the Directory table denoting the directory where the shortcut file is created."));
	NEW_VALIDATION (_T ("Shortcut"), _T ("Target"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Shortcut"), _T (""), _T ("The shortcut target. This is usually a property that is expanded to a file or a folder that the shortcut points to."));
	NEW_VALIDATION (_T ("Shortcut"), _T ("Arguments"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("The command-line arguments for the shortcut."));
	NEW_VALIDATION (_T ("Shortcut"), _T ("Shortcut"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Primary key, non-localized token."));
	NEW_VALIDATION (_T ("Shortcut"), _T ("Hotkey"), _T ("Y"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("The hotkey for the shortcut. It has the virtual-key code for the key in the low-order byte, and the modifier flags in the high-order byte. "));
	NEW_VALIDATION (_T ("Shortcut"), _T ("ShowCmd"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T (""), _T ("1;3;7"), _T ("The show command for the application window.The following values may be used."));
	NEW_VALIDATION (_T ("Shortcut"), _T ("WkDir"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("Name of property defining location of working directory."));
	NEW_VALIDATION (_T ("Signature"), _T ("FileName"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Filename"), _T (""), _T ("The name of the file. This may contain a \"short name|long name\" pair."));
	NEW_VALIDATION (_T ("Signature"), _T ("Signature"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Identifier"), _T (""), _T ("The table key. The Signature represents a unique file signature."));
	NEW_VALIDATION (_T ("Signature"), _T ("Languages"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Language"), _T (""), _T ("The languages supported by the file."));
	NEW_VALIDATION (_T ("Signature"), _T ("MaxDate"), _T ("Y"), 0, 2147483647, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("The maximum creation date of the file."));
	NEW_VALIDATION (_T ("Signature"), _T ("MaxSize"), _T ("Y"), 0, 2147483647, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("The maximum size of the file. "));
	NEW_VALIDATION (_T ("Signature"), _T ("MaxVersion"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("The maximum version of the file."));
	NEW_VALIDATION (_T ("Signature"), _T ("MinDate"), _T ("Y"), 0, 2147483647, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("The minimum creation date of the file."));
	NEW_VALIDATION (_T ("Signature"), _T ("MinSize"), _T ("Y"), 0, 2147483647, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("The minimum size of the file."));
	NEW_VALIDATION (_T ("Signature"), _T ("MinVersion"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("The minimum version of the file."));
	NEW_VALIDATION (_T ("TypeLib"), _T ("Feature_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Feature"), 1, _T ("Identifier"), _T (""), _T ("Required foreign key into the Feature Table, specifying the feature to validate or install in order for the type library to be operational."));
	NEW_VALIDATION (_T ("TypeLib"), _T ("Description"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T (""));
	NEW_VALIDATION (_T ("TypeLib"), _T ("Component_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Component"), 1, _T ("Identifier"), _T (""), _T ("Required foreign key into the Component Table, specifying the component for which to return a path when called through LocateComponent."));
	NEW_VALIDATION (_T ("TypeLib"), _T ("Directory_"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Directory"), 1, _T ("Identifier"), _T (""), _T ("Optional. The foreign key into the Directory table denoting the path to the help file for the type library."));
	NEW_VALIDATION (_T ("TypeLib"), _T ("Language"), _T ("N"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("The language of the library."));
	NEW_VALIDATION (_T ("TypeLib"), _T ("Version"), _T ("Y"), 0, 16777215, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("The version of the library. The minor version is in the lower 8 bits of the integer. The major version is in the next 16 bits. "));
	NEW_VALIDATION (_T ("TypeLib"), _T ("Cost"), _T ("Y"), 0, 2147483647, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("The cost associated with the registration of the typelib. This column is currently optional."));
	NEW_VALIDATION (_T ("TypeLib"), _T ("LibID"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Guid"), _T (""), _T ("The GUID that represents the library."));
	NEW_VALIDATION (_T ("Upgrade"), _T ("Attributes"), _T ("N"), 0, 2147483647, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("The attributes of this product set."));
	NEW_VALIDATION (_T ("Upgrade"), _T ("Remove"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("The list of features to remove when uninstalling a product from this set.  The default is \"ALL\"."));
	NEW_VALIDATION (_T ("Upgrade"), _T ("Language"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Language"), _T (""), _T ("A comma-separated list of languages for either products in this set or products not in this set."));
	NEW_VALIDATION (_T ("Upgrade"), _T ("ActionProperty"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("UpperCase"), _T (""), _T ("The property to set when a product in this set is found."));
	NEW_VALIDATION (_T ("Upgrade"), _T ("UpgradeCode"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Guid"), _T (""), _T ("The UpgradeCode GUID belonging to the products in this set."));
	NEW_VALIDATION (_T ("Upgrade"), _T ("VersionMax"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("The maximum ProductVersion of the products in this set.  The set may or may not include products with this particular version."));
	NEW_VALIDATION (_T ("Upgrade"), _T ("VersionMin"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("The minimum ProductVersion of the products in this set.  The set may or may not include products with this particular version."));
	NEW_VALIDATION (_T ("Verb"), _T ("Sequence"), _T ("Y"), 0, 32767, _T (""), MSI_NULL_INTEGER, _T (""), _T (""), _T ("Order within the verbs for a particular extension. Also used simply to specify the default verb."));
	NEW_VALIDATION (_T ("Verb"), _T ("Argument"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("Optional value for the command arguments."));
	NEW_VALIDATION (_T ("Verb"), _T ("Extension_"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T ("Extension"), 1, _T ("Text"), _T (""), _T ("The extension associated with the table row."));
	NEW_VALIDATION (_T ("Verb"), _T ("Verb"), _T ("N"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Text"), _T (""), _T ("The verb for the command."));
	NEW_VALIDATION (_T ("Verb"), _T ("Command"), _T ("Y"), MSI_NULL_INTEGER, MSI_NULL_INTEGER, _T (""), MSI_NULL_INTEGER, _T ("Formatted"), _T (""), _T ("The command text."));

#undef NEW_VALIDATION

	return true;
}

bool AddSumProperties (CBicDatabase &db, bool compress)
{
#define NEW_SUM_PROPERTY(property, datatype, value);		\
	{\
		CBicMsiSummaryProperty *p = db.NewSummaryProperty (property, datatype, value);\
		if (p == NULL) { return false; }\
	}

#define NEW_SUM_PROPERTY2(property);		\
	{\
		CBicMsiSummaryProperty *p = db.NewSummaryProperty (property, VT_LPSTR);\
		if (p == NULL) { return false; }\
		if (!p->GenGuid ()) { return false; }\
	}

	NEW_SUM_PROPERTY (MSI_PID_TITLE, VT_LPSTR, _T ("Installation Database"));
	NEW_SUM_PROPERTY (MSI_PID_SUBJECT, VT_LPSTR, _T ("Subject"));
	NEW_SUM_PROPERTY (MSI_PID_AUTHOR, VT_LPSTR, _T ("Bohemia Interactive Studio"));
#if _VBS1
	NEW_SUM_PROPERTY (MSI_PID_KEYWORDS, VT_LPSTR, _T ("Installer, MSI, Database, VBS1, AddonAtEase"));
	NEW_SUM_PROPERTY (MSI_PID_COMMENTS, VT_LPSTR, _T ("This installer database contains the logic and data required to install AddonAtEase for VBS1."));
#else
	NEW_SUM_PROPERTY (MSI_PID_KEYWORDS, VT_LPSTR, _T ("Installer, MSI, Database, Operation, Flashpoint, AddonAtEase"));
	NEW_SUM_PROPERTY (MSI_PID_COMMENTS, VT_LPSTR, _T ("This installer database contains the logic and data required to install AddonAtEase for Operation Flashpoint."));
#endif
	NEW_SUM_PROPERTY (MSI_PID_TEMPLATE, VT_LPSTR, _T (";1033"));
	NEW_SUM_PROPERTY2 (MSI_PID_REVNUMBER);
	NEW_SUM_PROPERTY (MSI_PID_PAGECOUNT, VT_I4, 100);
	NEW_SUM_PROPERTY (MSI_PID_WORDCOUNT, VT_I4, compress ? 2 : 0);
	NEW_SUM_PROPERTY (MSI_PID_APPNAME, VT_LPSTR, _T ("Orca"));
	NEW_SUM_PROPERTY (MSI_PID_SECURITY, VT_I4, 0);

#undef NEW_SUM_PROPERTY
#undef NEW_SUM_PROPERTY2

	return true;
}

int CompileFullInstallation (HINSTANCE hInstance, LPCTSTR fromDir, LPCTSTR toDir, bool tempDelete, bool compress, CBicMsiInfo *inInfo, CBicMsiInfo *outInfo, CBicTString *msiFileName)
{
	bool readMePresent = false;
	CBicTString readMeFileName;
	CBicTString productName;
	CBicTString productVersion;
	CBicTString tmpPath;

	{
		tmpPath = fromDir;
		tmpPath += _T ("name.txt");
		CBicFileReader proFile;
		if (!proFile.Open (tmpPath) || !proFile.Read (productName))
		{
			productName = _T ("ProductName");
		}
	}
	{
		tmpPath = fromDir;
		tmpPath += _T ("version.txt");
		CBicFileReader verFile;
		if (verFile.Open (tmpPath) && verFile.Read (productVersion))
		{
			unsigned int v1 = 1, v2 = 0, v3 = 1;
			if (_stscanf (static_cast<LPCTSTR> (productVersion), _T ("%u.%u.%u"), &v1, &v2, &v3) != 3)
			{
				return false;
			}
			productVersion.Format (_T ("%02u.%02u.%04u"), v1, v2, v3);
		}
		else
		{
			productVersion = _T ("01.00.0001");
		}
	}

	CBicDatabase db;
	db.ResetCounters ();

	if (!AddAddonDirectories (db, fromDir, compress, readMePresent, readMeFileName, productName, productVersion))
	{
		return 1;
	}

	if (!AddInstallExecutiveActions (db))
	{
		return 1;
	}

	if (!AddActionTexts (db))
	{
		return 1;
	}

	if (!AddErrors (db))
	{
		return 1;
	}

	if (!AddTextStyles (db))
	{
		return 1;
	}

	if (!AddUI (db, hInstance, fromDir, readMePresent, readMeFileName))
	{
		return 1;
	}

	if (!AddUIText (db))
	{
		return 1;
	}

	if (!AddValidation (db))
	{
		return 1;
	}

	if (!AddSumProperties (db, compress))
	{
		return 1;
	}

	if (inInfo != NULL && !inInfo->ApplyToDatabase (db, productVersion))
	{
		return 1;
	}

	CBicTString outFileName (productName);
  outFileName.FindReplace( _T( ' ' ), _T( '_' ) );
#if _VBS1
  outFileName += _T ("_VBSAddon.msi");
#else
	outFileName += _T ("_OFPAddon.msi");
#endif
	if (!db.SaveAsMsi (fromDir, toDir, outFileName, tempDelete, compress))
	{
		return 1;
	}

	if (outInfo != NULL)
	{
		outInfo->ReadFromDatabase (db);
	}

	if (msiFileName != NULL)
	{
		msiFileName->operator = (outFileName);
	}
	return 0;
}

int WINAPI WinMain (HINSTANCE instance, HINSTANCE prevInstance, LPSTR cmdLine, int cmdShow)
{
#define ERR_END		\
	if (showResult && !msicLogInWindow)\
	{\
		MessageBox (NULL, _T ("An error has occured."), _T ("Addon At Ease"), MB_OK);\
	}\
	return 1

	bool showResult = true;
	bool tempDelete = true;
	bool compress = true;
  bool deleteMsiInfoBinFile = true;
	TCHAR *find = _tcspbrk (cmdLine, _T ("/-"));
	TCHAR *end = cmdLine + (_tcslen (cmdLine) - 1);
	while (find != NULL && find < end)
	{
		++find;
    if( _tcsnicmp( find, _T( "l" ), 1 ) == 0 )
    {
			msicLogInFile = true;
    }
    else if( _tcsnicmp( find, _T( "w" ), 1 ) == 0 )
    {
			msicLogInWindow = false;
    }
    else if( _tcsnicmp( find, _T( "t" ), 1 ) == 0 )
    {
			tempDelete = false;
    }
    else if( _tcsnicmp( find, _T( "w" ), 1 ) == 0 )
    {
			compress = false;
    }
    else if( _tcsnicmp( find, _T( "r" ), 1 ) == 0 )
    {
			showResult = false;
    }
    else if( _tcsnicmp( find, _T( "patch" ), 5 ) == 0 )
    {
			deleteMsiInfoBinFile = false;
    }

    find = _tcspbrk (find, _T ("/-"));
	}

	if (msicLogInFile)
	{
		msicLogFile = fopen (".\\msic.log", "wt");
	}

	CBicTString source;
	{
		CBicFileReader srcPath;
		if (!srcPath.Open (_T ("source.txt")) || !srcPath.Read (source))
		{
			source = _T (".\\");
		}
	}

	CBicTString msiInfoPath (source);
	msiInfoPath += _T ("msi-info.bin");

  if( deleteMsiInfoBinFile )
  {
    DeleteFile( msiInfoPath );
  }

	if (_taccess (_T (".\\upgrade.txt"), 04) != 0)
	{
		// generate classic installation
		CBicMsiInfo inInfo, outInfo, *inInfoPtr = NULL;
		if (inInfo.Open (msiInfoPath))
		{
			inInfoPtr = &inInfo;
		}

		if (CompileFullInstallation (instance, source, _T (".\\"), tempDelete, compress, inInfoPtr, &outInfo, NULL))
		{
			ERR_END;
		}

		if (!outInfo.Save (msiInfoPath))
		{
			ERR_END;
		}
	}
	else
	{
		// generate patch
		CBicMsiInfo inInfo, mainInfo;
		if (!inInfo.Open (msiInfoPath))
		{
			msicLogErr1( _T( "File \"%s\" is missing for patch.\r\n" ), __FILE__, __LINE__, static_cast< LPCTSTR >( msiInfoPath ) );
			ERR_END;
		}

		CBicTString upgrPath;
		if (CompileFullInstallation (instance, source, _T (".\\tmp_install_000\\"), tempDelete, false, &inInfo, &mainInfo, &upgrPath))
		{
			ERR_END;
		}
		upgrPath.AddPrefix (_T (".\\tmp_install_000\\"));

		CBicTString upgrade;
		{
			CBicFileReader upgrFile;
			if (!upgrFile.Open (_T (".\\upgrade.txt")) || !upgrFile.Read (upgrade))
			{
				ERR_END;
			}
		}

		CBicPatchInfo patchInfo;
		if (!patchInfo.ReadVersionPaths (upgrade))
		{
			ERR_END;
		}

		CBicTString toDir;
		CBicTString tmpFileName;
		int n = patchInfo.GetVersionSize ();
		for (int i = 0; i < n; ++i)
		{
			if (CompileFullInstallation (instance, patchInfo.GetVersionPath (i), patchInfo.GetDestinationPath (i), tempDelete, false, &mainInfo, NULL, &tmpFileName))
			{
				ERR_END;
			}
			patchInfo.SetDestinationFileName (i, tmpFileName);
		}
		
		// Create patch.msp
		if (!patchInfo.CreatePatch (_T ("patch.msp"), upgrPath, tempDelete))
		{
			ERR_END;
		}

		// delete temp files
		if (tempDelete)
		{
			RemoveWholeDirectory (_T ("tmp_install_000\\"));
			for (int i = 0; i < n; ++i)
			{
				RemoveWholeDirectory (patchInfo.GetDestinationPath (i));
			}
		}
	}

	fclose (msicLogFile);
	if (showResult)
	{
		MessageBox (NULL, _T ("Successfully finished."), _T ("Addon At Ease"), MB_OK);
	}

#undef ERR_END

	return 0;
}
