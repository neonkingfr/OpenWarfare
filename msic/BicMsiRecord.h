
//! One single record of MSI database.
class CBicMsiRecord
{
protected:
	MSIHANDLE _record;

public:
	CBicMsiRecord ();
	~CBicMsiRecord ();

	MSIHANDLE GetMsiHandle () const;

	bool Open (unsigned int nParams);
	void Close ();

	bool Fill (unsigned int param, LPCTSTR data) const;
	bool FillStream (unsigned int param, LPCTSTR data) const;
	bool Fill (unsigned int param, int data) const;
};