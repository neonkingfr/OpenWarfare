
//! MSI database.
class CBicMsiDatabase
{
protected:
	MSIHANDLE _database;

public:
	CBicMsiDatabase ();
	~CBicMsiDatabase ();

	MSIHANDLE GetMsiHandle () const;

	bool Create (LPCTSTR msiDbDir, LPCTSTR filename);
	void CBicMsiDatabase::Close ();

	bool ExeCmd (LPCTSTR cmd);
};
