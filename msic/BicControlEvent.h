
//! Class of one record in ControlEvent table.
class CBicControlEvent
{
protected:
	CBicControl *_control;
	CBicTString _event;
	
	CBicTString _argument;
	CBicDialog *_argumentDialog;
	
	CBicTString _condition;
	short _ordering;

public:
	CBicControlEvent (CBicControl *control, LPCTSTR event, LPCTSTR argument, LPCTSTR condition);
	CBicControlEvent (CBicControl *control, LPCTSTR event, CBicDialog *argumentDialog, LPCTSTR condition);

	bool Fill (CBicMsiRecord &rec) const;
};