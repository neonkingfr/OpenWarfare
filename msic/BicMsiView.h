
//! View to the MSI database.
class CBicMsiView
{
protected:
	MSIHANDLE _view;

public:
	CBicMsiView ();
	~CBicMsiView ();

	bool Open (CBicMsiDatabase &db, LPCTSTR query);
	void Close ();

	bool Execute ();
	bool Execute (CBicMsiRecord &rec);
	bool Modify (MSIMODIFY modifyMode, CBicMsiRecord &rec);
};