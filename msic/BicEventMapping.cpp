
#include "msic.h"


CBicEventMapping::CBicEventMapping (CBicControl *control, LPCTSTR event, LPCTSTR attribute)
: _control (control), _event (event), _attribute (attribute)
{
}

bool CBicEventMapping::Fill (CBicMsiRecord &rec) const
{
	if (!rec.Fill (1, _control->GetDialog ()) ||
		!rec.Fill (2, _control->GetControl ()) ||
		!rec.Fill (3, _event) ||
		!rec.Fill (4, _attribute))
	{
		return false;
	}

	return true;
}