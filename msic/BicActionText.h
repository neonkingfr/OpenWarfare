
//! Class of one record in ActionText table.
class CBicActionText
{
protected:
	CBicTString _action;
	CBicTString _description;
	CBicTString _template;
	
public:
	CBicActionText (LPCTSTR action, LPCTSTR description, LPCTSTR format = _T (""));
	
	bool Fill (CBicMsiRecord &rec) const;
};