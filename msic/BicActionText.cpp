
#include "msic.h"


CBicActionText::CBicActionText (LPCTSTR action, LPCTSTR description, LPCTSTR format /* = _T ("") */)
: _action (action), _description (description), _template (format)
{
}

bool CBicActionText::Fill (CBicMsiRecord &rec) const
{
	if (!rec.Fill (1, _action) ||
		!rec.Fill (2, _description) ||
		!rec.Fill (3, _template))
	{
		return false;
	}

	return true;
}