
#include "msic.h"


CBicRadioButtonGroup::CBicRadioButtonGroup (LPCTSTR property, LPCTSTR value)
: CBicProperty (property, value), _counter (0)
{
}

CBicRadioButtonGroup::~CBicRadioButtonGroup ()
{
}

short CBicRadioButtonGroup::NewButtonOrder ()
{
	return ++_counter;
}