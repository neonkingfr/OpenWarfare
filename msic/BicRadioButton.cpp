
#include "msic.h"


CBicRadioButton::CBicRadioButton (CBicRadioButtonGroup *group, LPCTSTR value, short x, short y, short width, short height, LPCTSTR text, LPCTSTR help)
: _group (group), _order (group->NewButtonOrder ()), _x (x), _y (y), _width (width), _height (height),
_value (value), _text (text), _help (help)
{
}

bool CBicRadioButton::Fill (CBicMsiRecord &rec) const
{
	if (!rec.Fill (1, _group->GetProperty ()) ||
		!rec.Fill (2, _order) ||
		!rec.Fill (3, _value) ||
		!rec.Fill (4, _x) ||
		!rec.Fill (5, _y) ||
		!rec.Fill (6, _width) ||
		!rec.Fill (7, _height) ||
		!rec.Fill (8, _text) ||
		!rec.Fill (9, _help))
	{
		return false;
	}

	return true;
}