
#include "msic.h"


CBicControlCondition::CBicControlCondition (CBicControl *control, LPCTSTR action, LPCTSTR condition)
: _control (control), _action (action), _condition (condition)
{
}

bool CBicControlCondition::Fill (CBicMsiRecord &rec) const
{
	if (!rec.Fill (1, _control->GetDialog ()) ||
		!rec.Fill (2, _control->GetControl ()) ||
		!rec.Fill (3, _action) ||
		!rec.Fill (4, _condition))
	{
		return false;
	}

	return true;
}