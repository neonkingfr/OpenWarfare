
//! Array of type T, objects can be passed through argument A.
template<typename T, typename A>
class CBicArray
{
protected:
  //! Pointer to the data of the array.
	T *_data;

  //! Size of the array.
	unsigned int _size;

  //! Allocated length..
	unsigned int _length;

  //! Length of the first allocation.
	unsigned int _initLength;

  //! Increase allocated length by this step (number of elements).
	unsigned int _step;

public:
	inline CBicArray (int initLength = 30, int step = 10) : _data (NULL), _size (0), _length (0), _initLength (initLength), _step (step)
	{
	}

	inline virtual ~CBicArray ()
	{
		delete []_data;
	}

	bool Realloc (unsigned int newLength, unsigned int zeroIdx = 0)
	{
		if (_data == NULL)
		{
			if (newLength < _initLength)
			{
				newLength = _initLength;
			}

			_data = new T[newLength];
			if (_data != NULL)
			{
				_length = newLength;
				_size = zeroIdx;
				return true;
			}
			return false;
		}

		T *newData = new T[newLength];
		if (newData == NULL)
		{
			return false;
		}

		_length = newLength;
		for (unsigned int i = 0; i < _size; ++i)
		{
			newData[zeroIdx + i] = _data[i];
		}
		delete []_data;
		_data = newData;
		_size += zeroIdx;
		return true;
	}

	bool Add (A a)
	{
		if (_size < _length || Realloc (_size + _step))
		{
			_data[_size++] = a;
			return true;
		}
		return false;
	}

	inline unsigned int GetSize () const
	{
		return _size;
	}

	inline unsigned int GetLength () const
	{
		return _length;
	}

	inline T& GetAt (unsigned int i) const
	{
		assert (i < _size);
		return _data[i];
	}

	inline T operator [](unsigned int i) const
	{
		assert (i < _size);
		return _data[i];
	}

	bool operator == (const CBicArray<T, A> &op2) const
	{
		if (_size != op2._size)
		{
			return false;
		}

		for (int i = 0; i < _size)
		{
			if (_data[i] != op2._data[i])
			{
				return false;
			}
		}

		return true;
	}

	bool operator != (const CBicArray<T, A> &op2) const
	{
		if (_size != op2._size)
		{
			return true;
		}

		for (int i = 0; i < _size)
		{
			if (_data[i] != op2._data[i])
			{
				return true;
			}
		}

		return false;
	}
	
	inline void SetAt (unsigned int i, A val)
	{
		assert (i < _size);
		_data[i] = val;
	}

	bool MoveAt (unsigned int zero)
	{
		unsigned int newSize = _size + zero;
		if (newSize <= _length)
		{
			for (int i = _size; i != 0; )
			{
				--i;
				_data[zero + i] = _data[i];
			}
			_size = newSize;
			return true;
		}
		else if (Realloc (newSize + _step, zero))
		{
			return true;
		}
		return false;
	}

	bool AddPrefix (const A* src, unsigned int size)
	{
		if (MoveAt (size))
		{
			for (unsigned int i = 0; i < size; i++)
			{
				_data[i] = src[i];
			}
			return true;
		}
		return false;
	}

	bool AddPrefix (const CBicArray<T, A> &src)
	{
		unsigned int srcSize = src.GetSize ();
		if (MoveAt (srcSize))
		{
			for (unsigned int i = 0; i < srcSize; i++)
			{
				_data[i] = src.GetAt (i);
			}
			return true;
		}
		return false;
	}

	bool Append (const A* src, unsigned int size)
	{
		unsigned int newSize = _size + size;
		if (newSize <= _length || Realloc (newSize + _step))
		{
			for (unsigned int i = 0; i < size; )
			{
				_data[_size++] = src[i++];
			}
			return true;
		}
		return false;
	}

	bool Append (const CBicArray<T, A> &src)
	{
		unsigned int srcSize = src.GetSize ();
		unsigned int newSize = _size + srcSize;
		if (newSize <= _length || Realloc (newSize + _step))
		{
			for (unsigned int i = 0; i < srcSize; )
			{
				_data[_size++] = src.GetAt (i++);
			}
			return true;
		}
		return false;
	}
	
	bool Copy (const A* src, unsigned int size)
	{
		if (size <= _length || Realloc (size + _step))
		{
			for (_size = 0; _size < size; ++_size)
			{
				_data[_size] = src[_size];
			}
			return true;
		}
		return false;
	}

	bool Copy (const CBicArray<T, A> &src)
	{
		unsigned int srcSize = src.GetSize ();
		if (srcSize <= _length || Realloc (srcSize + _step))
		{
			for (_size = 0; _size < srcSize; ++_size)
			{
				_data[_size] = src.GetAt (_size);
			}
			return true;
		}
		return false;
	}

	inline const T* GetData () const
	{
		return _data;
	}
	
	inline T* GetBuffer (unsigned int size)
	{
		if (_length < size && !Realloc (size))
		{
			msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
		}
		_size = size;
		return _data;
	}

	inline T* GetBuffer ()
	{
		if (_data == NULL && !Realloc (_initLength))
		{
			msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
		}
		_size = _length;
		return _data;
	}

	inline bool Enlarge ()
	{
		return Realloc (_length + _step);
	}
	
	inline void RemoveLast ()
	{
		assert (_size > 0);
		if (_size > 0)
		{
			--_size;
		}
	}

	inline void Empty ()
	{
		_size = 0;
	}

	inline void Shrink (unsigned int size)
	{
		assert (_size >= size);
		_size = size;
	}
	
	inline unsigned int GetByteSize () const
	{
		return _size * sizeof (T);
	}

	inline bool IsInside (void *ptr) const
	{
		return _data != NULL && _data <= ptr && reinterpret_cast<char*> (_data) + _size * sizeof (T) > ptr;
	}

	inline const T* GetEnd () const
	{
		return _data != NULL ? _data + _size : NULL;
	}
};

//! Call desctructor of the elements in the destructor of this array.
template<typename T, typename A>
class CBicArraySelfDestruct : public CBicArray<T, A>
{
public:
	inline CBicArraySelfDestruct (int initLength = 30, int step = 10) : CBicArray<T, A> (initLength, step)
	{
	}

	inline virtual ~CBicArraySelfDestruct ()
	{
		for (unsigned int i = 0; i < _size; ++i)
		{
			delete _data[i];
		}
	}
};
