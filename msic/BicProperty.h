

class CBicDialog;

//! Class of one record in Property table.
class CBicProperty : public CBicBaseProperty
{
protected:
	CBicDialog *_dialog;
	CBicTString _value;
	
	bool _storing;

public:
	CBicProperty (LPCTSTR property, bool storing = true);
	CBicProperty (LPCTSTR property, LPCTSTR value, bool storing = true);
	CBicProperty (LPCTSTR property, CBicDialog *dialog, bool storing = true);
	virtual ~CBicProperty ();

	LPCTSTR GetValue () const;
	bool IsStoring () const;

	bool GenGuid ();

	void SetValue (LPCTSTR value);
	bool Fill (CBicMsiRecord &rec) const;
};