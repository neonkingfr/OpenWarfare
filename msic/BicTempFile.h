
//! Temporary file class.
class CBicTempFile : public CBicDataWriter
{
protected:
	CBicTString _prefix;

public:
	CBicTempFile (LPCTSTR prefix, bool autoDelete);
	~CBicTempFile ();

	virtual bool Initialize (LPCTSTR path);
};