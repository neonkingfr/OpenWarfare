
//! Class of one record in UpgradedImages table.
class CBicUpgradedImage
{
protected:
	static unsigned int _counter;

	CBicTString _upgraded;
	CBicTString _msiPath;
	CBicImageFamily *_imageFamily;
	
public:
	CBicUpgradedImage (LPCTSTR msiPath, CBicImageFamily *family)
		: _msiPath (msiPath), _imageFamily (family)
	{
		_upgraded.Format (_T ("Upg_%04u"), ++_counter);
	}

	LPCTSTR GetUpgraded () const
	{
		return _upgraded;
	}

	bool Fill (CBicMsiRecord &rec) const
	{
		if (!rec.Fill (1, _upgraded) ||
			!rec.Fill (2, _msiPath) ||
			!rec.Fill (3, static_cast<LPCTSTR> (NULL)) ||
			!rec.Fill (4, static_cast<LPCTSTR> (NULL)) ||
			!rec.Fill (5, _imageFamily->GetFamily ()))
		{
			return false;
		}

		return true;
	}

	static inline void ResetCounter ()
	{
		_counter = 0;
	}
};