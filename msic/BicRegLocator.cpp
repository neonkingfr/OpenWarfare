
#include "msic.h"


unsigned int CBicRegLocator::_counter = 0;

CBicRegLocator::CBicRegLocator (CBicBaseProperty *property, short root, LPCTSTR key, LPCTSTR name, short type)
: _property (property), _root (root), _type (type), _key (key), _name (name)
{
	_signature.Format (_T ("Registry_%08u"), ++_counter);
}

bool CBicRegLocator::FillForAppSearch (CBicMsiRecord &rec) const
{
	if (!rec.Fill (1, _property->GetProperty ()) ||
		!rec.Fill (2, _signature))
	{
		return false;
	}

	return true;
}

bool CBicRegLocator::FillForRegLocator (CBicMsiRecord &rec) const
{
	if (!rec.Fill (1, _signature) ||
		!rec.Fill (2, _root) ||
		!rec.Fill (3, _key) ||
		!rec.Fill (4, _name) ||
		!rec.Fill (5, _type))
	{
		return false;
	}

	return true;
}
