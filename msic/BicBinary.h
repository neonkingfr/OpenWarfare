
//! Class of one record in Binary table.
class CBicBinary
{
protected:
	static unsigned short _counter;

	CBicProperty *_property;
	
	const unsigned char *_binaries;
	const unsigned long _size;
	CBicTString _fileName;
	CBicTString _name;

public:
	static const unsigned char _iconBinariesRepair[2998];
	static const unsigned char _iconBinariesRemove[2998];
	static const unsigned char _bmpBinariesDialog[26496];
	static const unsigned char _bmpBinariesBanner[94554];
	static const unsigned char _bmpBinariesUp[318];
	static const unsigned char _bmpBinariesNew[318];
	static const unsigned char _bmpBinariesInfo[1078];

	CBicBinary (CBicProperty *property, LPCTSTR fileName);
	CBicBinary (CBicProperty *property, const unsigned char *binaries, unsigned long size);
	CBicBinary (const unsigned char *binaries, unsigned long size);
	
	LPCTSTR GetProperty () const;
	LPCTSTR GetName () const;

	bool Fill (CBicMsiRecord &rec) const;
	
	static inline void ResetCounter ()
	{
		_counter = 0;
	}
};