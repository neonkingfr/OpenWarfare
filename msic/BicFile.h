
//! Class of one record in File table.
class CBicFile
{
protected:
	static unsigned int _counter;

	CBicTString _file;
	CBicComponent *_component;
	CBicTString _fileName;
	long _fileSize;
	// CBicTString _version;
	// CBicTString _language;
	short _attributes;
	short _sequence;

	CBicTString _dstFileName;
	
public:
	enum
	{
		attrReadOnly = 0x0001,
		attrHidden = 0x0002,
		attrSystem = 0x0004,
		attrVital = 0x0200,
		attrChecksum = 0x0400,
		attrPatchAdded = 0x1000,
		attrNoncompressed = 0x2000,
		attrCompressed = 0x4000,
	};

	CBicFile (LPCTSTR fileName, unsigned long fileSize, short attributes, CBicComponent *component = NULL);

	inline void SetSequence (short seq)
	{
		_sequence = seq;
	}

	short GetSequence () const;

	void SetComponent (CBicComponent *component);
	
	const CBicComponent* GetComponent () const;
	
	LPCTSTR GetSrcFileName () const;

	void SetDstFileName (LPCTSTR fileName);
	
	LPCTSTR GetDstFileName () const;
	
	inline void SetFile (LPCTSTR file)
	{
		_file = file;
	}

	LPCTSTR GetFile () const;
	
	void GetDestinationPath (CBicTString &path) const;

	bool Fill (CBicMsiRecord &rec) const;
	
	void ChangeFileComponentSequence ();

	static inline void ResetCounter ()
	{
		_counter = 0;
	}

	static inline unsigned int GetCounter ()
	{
		return _counter;
	}

	static inline void SetCounter (unsigned int c)
	{
		_counter = c;
	}
};
