
#include <stddef.h>
#include <crtdbg.h>

#include <tchar.h>
#include <windows.h>
#include <msi.h>
#include <msiquery.h>
#include <atlconv.h>

#include <stdio.h>
#include <fcntl.h>
#include <io.h>
#include <conio.h>
#include <assert.h>

#ifdef _DEBUG

#define msicLogErr0(errMsg, fileName, line);		\
{\
	assert (0);\
	if (msicLogInFile || msicLogInWindow)\
	{\
		CBicTString buf;\
		buf.Format( _T( "(%s, %d): %s" ), fileName, line, static_cast< LPCTSTR >( errMsg ) );\
		if (msicLogInFile)\
		{\
			_ftprintf( msicLogFile, static_cast< LPCTSTR >( buf ) );\
		}\
		if (msicLogInWindow)\
		{\
			MessageBox( NULL, static_cast< LPCTSTR >( buf ), _T ("Error"), MB_OK );\
		}\
	}\
}

#define msicLogErr1( errMsgFormat, fileName, line, errParam );		\
{\
	assert (0);\
	if (msicLogInFile || msicLogInWindow)\
	{\
    CBicTString buf1;\
    buf1.Format( errMsgFormat, errParam );\
		CBicTString buf2;\
		buf2.Format( _T( "(%s, %d): %s" ), fileName, line, static_cast< LPCTSTR >( buf1 ) );\
		if (msicLogInFile)\
		{\
			_ftprintf( msicLogFile, static_cast< LPCTSTR >( buf2 ) );\
		}\
		if (msicLogInWindow)\
		{\
			MessageBox( NULL, static_cast< LPCTSTR >( buf2 ), _T ("Error"), MB_OK );\
		}\
	}\
}

#else // #ifdef _DEBUG

#define msicLogErr0(errMsg, fileName, line);		\
{\
	assert (0);\
	if (msicLogInFile || msicLogInWindow)\
	{\
		if (msicLogInFile)\
		{\
			_ftprintf( msicLogFile, static_cast< LPCTSTR >( errMsg ) );\
		}\
		if (msicLogInWindow)\
		{\
			MessageBox( NULL, static_cast< LPCTSTR >( errMsg ), _T ("Error"), MB_OK );\
		}\
	}\
}

#define msicLogErr1( errMsgFormat, fileName, line, errParam );		\
{\
	assert (0);\
	if (msicLogInFile || msicLogInWindow)\
	{\
    CBicTString buf1;\
    buf1.Format( errMsgFormat, errParam );\
		if (msicLogInFile)\
		{\
			_ftprintf( msicLogFile, static_cast< LPCTSTR >( buf1 ) );\
		}\
		if (msicLogInWindow)\
		{\
			MessageBox( NULL, static_cast< LPCTSTR >( buf1 ), _T ("Error"), MB_OK );\
		}\
	}\
}

#endif // #ifdef _DEBUG

#define MSI_ERR_FMT _T ("%u")
//#define MSI_ERR_FMT _T ("0x%08x")

extern FILE *msicLogFile;
extern bool msicLogInFile;
extern bool msicLogInWindow;

#include "BicArray.h"
#include "BicTString.h"
#include "BicFileReader.h"
#include "BicByteStream.h"
#include "BicFileWriter.h"
#include "BicTempFile.h"

bool GenGuid (CBicTString &str);
bool GenGuid (LPTSTR tStr, int len);
void RemoveWholeDirectory (LPCTSTR path);

extern LPCTSTR msicLanguage;

#include "BicMsiDatabase.h"
#include "BicMsiRecord.h"
#include "BicMsiView.h"
#include "BicMsiSummaryInformation.h"
#include "BicMsiSummaryProperty.h"

class CBicDatabase;

#include "BicBaseProperty.h"

#include "BicDirectory.h"
#include "BicComponent.h"
#include "BicFeature.h"
#include "BicFile.h"
#include "BicMedia.h"
#include "BicProperty.h"
#include "BicRegLocator.h"
#include "BicActionText.h"
#include "BicError.h"
#include "BicDialog.h"
#include "BicBinary.h"
#include "BicControl.h"
#include "BicControlCondition.h"
#include "BicControlEvent.h"
#include "BicEventMapping.h"
#include "BicTextStyle.h"
#include "BicRadioButtonGroup.h"
#include "BicRadioButton.h"
#include "BicCustomAction.h"
#include "BicInstallExecutiveAction.h"
#include "BicInstallUIAction.h"
#include "BicUIText.h"
#include "BicValidation.h"
#include "BicRemoveFile.h"

#include "BicDatabase.h"
#include "BicMsiInfo.h"

#include "BicImageFamily.h"
#include "BicUpgradedImage.h"
#include "BicTargetImage.h"
#include "BicPatchInfo.h"