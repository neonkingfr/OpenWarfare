
//! Class of one record in InstallUISequence table.
class CBicInstallUIAction
{
protected:
	static unsigned int _counter;

	CBicDialog *_actionDialog;
	CBicTString _action;
	CBicCustomAction *_customAction;
	
	CBicTString _condition;
	short _sequence;

public:
	CBicInstallUIAction (LPCTSTR action, LPCTSTR condition, short sequence);
	CBicInstallUIAction (LPCTSTR action, LPCTSTR condition);
	CBicInstallUIAction (CBicDialog *actionDialog, LPCTSTR condition, short sequence);
	CBicInstallUIAction (CBicDialog *actionDialog, LPCTSTR condition);
	CBicInstallUIAction (CBicCustomAction *customAction, LPCTSTR condition);

	bool Fill (CBicMsiRecord &rec) const;

	static inline void ResetCounter ()
	{
		_counter = 0;
	}
};