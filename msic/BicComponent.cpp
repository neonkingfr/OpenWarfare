
#include "msic.h"


CBicComponent::CBicComponent (CBicDirectory *directory, short attributes, LPCTSTR condition, CBicFile *keyPath)
: _directory (directory), _attributes (attributes), _condition (condition), _keyPath (keyPath)
{
	_component.Format (_T ("Component_%08u"), keyPath->GetSequence ());
	GenGuid (_componentId);
}

CBicComponent::CBicComponent (CBicDirectory *directory, short attributes, LPCTSTR condition, short sequence)
: _directory (directory), _attributes (attributes), _condition (condition), _keyPath (NULL)
{
	_component.Format (_T ("Component_%08u"), sequence);
	GenGuid (_componentId);
}

LPCTSTR CBicComponent::GetComponent () const
{
	return _component;
}

LPCTSTR CBicComponent::GetComponentId () const
{
	return _componentId;
}

const CBicDirectory* CBicComponent::GetDirectory () const
{
	return _directory;
}

bool CBicComponent::Fill (CBicMsiRecord &rec) const
{
	if (!rec.Fill (1, _component) ||
		!rec.Fill (2, _componentId) ||
		!rec.Fill (3, _directory->GetDirectory ()) ||
		!rec.Fill (4, _attributes) ||
		!rec.Fill (5, _condition) ||
		!rec.Fill (6, _keyPath != NULL ? _keyPath->GetFile () : NULL))
	{
		return false;
	}

	return true;
}

void CBicComponent::ChangeComponent (unsigned int counter)
{
	_component.Format (_T ("Component_%08u"), counter);
}
