
//! Class of one record in RemoveFile table.
class CBicRemoveFile
{
protected:
	CBicTString _file;
	CBicComponent *_component;
	CBicTString _fileName;
	short _installMode;

public:
	enum
	{
		installModeOnInstall = 0x0001,
		installModeOnRemove = 0x0002,
		installModeOnBoth = 0x0003,
	};

	CBicRemoveFile (LPCTSTR file, CBicComponent *component, LPCTSTR fileName, short installMode)
		: _file (file), _component (component), _fileName (fileName), _installMode (installMode)
	{
	}

	bool Fill (CBicMsiRecord &rec) const
	{
		if (!rec.Fill (1, _file) ||
			!rec.Fill (2, _component->GetComponent ()) ||
			!rec.Fill (3, _fileName) ||
			!rec.Fill (4, _component->GetDirectory ()->GetDirectory ()) ||
			!rec.Fill (5, _installMode))
		{
			return false;
		}

		return true;
	}
};