
#include "msic.h"


CBicValidation::CBicValidation (LPCTSTR table, LPCTSTR column, LPCTSTR nullable, long minValue, long maxValue, LPCTSTR keyTable, 
		long keyColumn, LPCTSTR category, LPCTSTR set, LPCTSTR description)
		: _minValue (minValue), _maxValue (maxValue), _keyColumn (keyColumn), _table (table), 
		_column (column), _nullable (nullable), _keyTable (keyTable), _category (category), 
		_set (set), _description (description)
{
}

bool CBicValidation::Fill (CBicMsiRecord &rec) const
{
	if (!rec.Fill (1, _table) ||
		!rec.Fill (2, _column) ||
		!rec.Fill (3, _nullable) ||
		!rec.Fill (4, _minValue) ||
		!rec.Fill (5, _maxValue) ||
		!rec.Fill (6, _keyTable) ||
		!rec.Fill (7, _keyColumn) ||
		!rec.Fill (8, _category) ||
		!rec.Fill (9, _set) ||
		!rec.Fill (10, _description))
	{
		return false;
	}

	return true;
}
