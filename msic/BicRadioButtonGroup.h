
//! Part of CBicRadioButton class. This record is generated into Property table.
class CBicRadioButtonGroup : public CBicProperty
{
protected:
	unsigned int _counter;

public:
	CBicRadioButtonGroup (LPCTSTR property, LPCTSTR value);
	virtual ~CBicRadioButtonGroup ();

	short NewButtonOrder ();
};