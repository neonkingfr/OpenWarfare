
#include "msic.h"


unsigned int CBicFile::_counter = 0;


CBicFile::CBicFile (LPCTSTR fileName, unsigned long fileSize, short attributes, CBicComponent *component)
: _fileSize (fileSize), _attributes (attributes), _sequence (++_counter), _component (component),
_fileName (fileName), _dstFileName (fileName)
{
	_file.Format (_T ("File_%08u"), _counter);
	
	USES_CONVERSION;
	int f = _open (T2CA (fileName), _O_BINARY | _O_RDONLY);
	if (f != -1)
	{
		_fileSize = _filelength (f);
		_close (f);
	}
}

void CBicFile::SetComponent (CBicComponent *component)
{
	_component = component;
}

const CBicComponent* CBicFile::GetComponent () const
{
	return _component;
}

LPCTSTR CBicFile::GetSrcFileName () const
{
	return _fileName;
}

LPCTSTR CBicFile::GetDstFileName () const
{
	return _dstFileName;
}

void CBicFile::SetDstFileName (LPCTSTR fileName)
{
	_dstFileName = fileName;
}

LPCTSTR CBicFile::GetFile () const
{
	return _file;
}

bool CBicFile::Fill (CBicMsiRecord &rec) const
{
	if (!rec.Fill (1, _file) ||
		!rec.Fill (2, _component->GetComponent ()) ||
		!rec.Fill (3, _dstFileName) ||
		!rec.Fill (4, _fileSize) ||
		!rec.Fill (5, static_cast<LPCTSTR> (NULL)) ||
		!rec.Fill (6, static_cast<LPCTSTR> (NULL)) ||
		!rec.Fill (7, _attributes) ||
		!rec.Fill (8, _sequence))
	{
		return false;
	}

	return true;
}

short CBicFile::GetSequence () const
{
	return _sequence;
}

void CBicFile::GetDestinationPath (CBicTString &path) const
{
	_component->GetDirectory ()->GetDestinationDirectory (path);
	path += _dstFileName;
}

void CBicFile::ChangeFileComponentSequence ()
{
	_sequence = ++_counter;
	_file.Format (_T ("File_%08u"), _counter);
	_component->ChangeComponent (_counter);
}
