
//! Class of one record in EventMapping table.
class CBicEventMapping
{
protected:
	CBicControl *_control;
	CBicTString _event;
	CBicTString _attribute;

public:
	CBicEventMapping (CBicControl *control, LPCTSTR event, LPCTSTR attribute);
	
	bool Fill (CBicMsiRecord &rec) const;
};