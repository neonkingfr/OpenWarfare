

class CBicFile;

//! Class of one record in Component table.
class CBicComponent
{
protected:
	CBicTString _component;
	CBicTString _componentId;
	CBicDirectory *_directory;
	short _attributes;
	CBicTString _condition;
	CBicFile *_keyPath;

public:
	enum
	{
		attrLocalOnly = 0x0000,
		attrSourceOnly = 0x0001,
		attrOptional = 0x0002,
		attrRegistryKeyPath = 0x0004,
		attrSharedDllRefCount = 0x0008,
		attrPermanent = 0x0010,
		attrODBCDataSource = 0x0020,
		attrTransitive = 0x0040,
		attrNeverOverwrite = 0x0080,
		attr64bit = 0x0100,
	};
	CBicComponent (CBicDirectory *directory, short attributes, LPCTSTR condition, CBicFile *keyPath);
	CBicComponent (CBicDirectory *directory, short attributes, LPCTSTR condition, short sequence);

	inline void SetComponent (LPCTSTR comp)
	{
		_component = comp;
	}

	LPCTSTR GetComponent () const;

	inline void SetComponentId (LPCTSTR id)
	{
		_componentId = id;
	}

	LPCTSTR GetComponentId () const;
	
	const CBicDirectory* GetDirectory () const;

	inline CBicFile* GetKeyPath () const
	{
		return _keyPath;
	}

	void SetKeyPath (CBicFile *keyPath)
	{
		_keyPath = keyPath;
	}

	bool Fill (CBicMsiRecord &rec) const;

	void ChangeComponent (unsigned int counter);
};
