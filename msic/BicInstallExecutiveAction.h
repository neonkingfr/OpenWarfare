
//! Class of one record in InstallExecuteSequence table.
class CBicInstallExecutiveAction
{
protected:
	static unsigned int _counter;

	CBicTString _action;
	CBicCustomAction *_customAction;
	CBicTString _condition;
	short _sequence;

public:
	CBicInstallExecutiveAction (LPCTSTR action, LPCTSTR condition, short sequence);
	CBicInstallExecutiveAction (LPCTSTR action, LPCTSTR condition);
	CBicInstallExecutiveAction (CBicCustomAction *customAction, LPCTSTR condition);

	bool Fill (CBicMsiRecord &rec) const;

	static inline void ResetCounter ()
	{
		_counter = 0;
	}
};