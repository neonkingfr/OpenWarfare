
/*! Handles unique information about some installer to be reused during regeneration of the same installation. 
    Required for patch creation, because patches can be made only from installers not having files in CAB. 
    Thus such installers must be recreated with the same GUIDs as last time.
*/
class CBicMsiInfo
{
protected:
	CBicByteStream _propertyStream;
	CBicByteStream _directoryStream;
	CBicByteStream _fileStream;

	enum
	{
		_directoryCounter = 1,
		_fileCounter,
		_productName,
		_productCode,
		_manufacturer,
		_upgradeCode,
		_directory,
		_file,
	};

public:
	inline CBicMsiInfo ()
	{
	}

	inline bool Open (LPCTSTR filename)
	{
#define READ_STREAM_PART(name)		\
		rsize = sizeof (unsigned int);\
		if (!file.Read (reinterpret_cast<unsigned char*> (&size), rsize))\
		{\
			return false;\
		}\
		if (!name.ReadFromFile (file, size))\
		{\
			return false;\
		}

		unsigned int size;
		unsigned int rsize;
		{
			CBicFileReader file;
			if (!file.Open (filename))
			{
				return false;
			}

			READ_STREAM_PART (_propertyStream)
			READ_STREAM_PART (_directoryStream)
			READ_STREAM_PART (_fileStream)
		}

#undef READ_STREAM_PART

		return true;
	}

	bool Save (LPCTSTR filename)
	{
#define WRITE_STREAM_PART(name)		\
		size = name.GetSize ();\
		if (!file.Write (reinterpret_cast<unsigned char*> (&size), sizeof (size)) ||\
			!file.Write (name))\
		{\
			return false;\
		}

		{
			CBicDataWriter file (false);
			if (!file.Open (filename))
			{
				return false;
			}
		
			unsigned int size;
			WRITE_STREAM_PART (_propertyStream)
			WRITE_STREAM_PART (_directoryStream)
			WRITE_STREAM_PART (_fileStream)
		}

#undef WRITE_STREAM_PART

		return true;
	}

	inline void Empty ()
	{
		_propertyStream.Empty ();
		_directoryStream.Empty ();
		_fileStream.Empty ();
	}

	bool ReadFromDatabase (CBicDatabase &db)
	{
#define ADD_ALL(arr, method)	\
	{\
		int n = (arr).GetSize ();\
		for (int i = 0; i < n; ++i)\
		{\
			##method (*(arr)[i]);\
		}\
	}
#define FIND_PROP(name, var)	\
	{\
		int _0n = db._properties.GetSize ();\
		for (int _0i = 0; ; ++_0i)\
		{\
			if (_0i == _0n)\
			{\
				return false;\
			}\
			if (_tcscmp (db._properties[_0i]->GetProperty (), name) != 0)\
			{\
				continue;\
			}\
			var = db._properties[_0i]->GetValue ();\
			break;\
		}\
	}
#define ADD_PROP1(name, method)	\
	{\
		LPCTSTR _0p1;\
		FIND_PROP (name, _0p1)\
		##method (_0p1);\
	}
#define ADD_PROP2(name1, name2, method)	\
	{\
		LPCTSTR _0p1, _0p2;\
		FIND_PROP (name1, _0p1)\
		FIND_PROP (name2, _0p2)\
		##method (_0p1, _0p2);\
	}

		AddDirectoryCounter ();
		AddFileCounter ();
		ADD_PROP1 (_T ("ProductName"), AddProductName)
		ADD_PROP1 (_T ("ProductCode"), AddProductCode)
		ADD_PROP1 (_T ("Manufacturer"), AddManufacturer)
		ADD_PROP2 (_T ("ProductVersion"), _T ("UpgradeCode"), AddUpgradeCode)
		ADD_ALL (db._directories, AddDirectory)
		ADD_ALL (db._files, AddFile)

#undef ADD_ALL
#undef FIND_PROP
#undef ADD_PROP1
#undef ADD_PROP2

		if (!_propertyStream.Append (db._legacyMsiInfoPropertyStream) ||
			!_directoryStream.Append (db._legacyMsiInfoDirectoryStream) ||
			!_fileStream.Append (db._legacyMsiInfoFileStream))
		{
			return false;
		}

		return true;
	}

	bool ApplyToDatabase (CBicDatabase &db, LPCTSTR version)
	{
		db._legacyMsiInfoPropertyStream.Empty ();
		db._legacyMsiInfoDirectoryStream.Empty ();
		db._legacyMsiInfoFileStream.Empty ();

		return ApplyToDatabase (db, version, _propertyStream) &&
			ApplyToDatabase (db, version, _directoryStream) &&
			ApplyToDatabase (db, version, _fileStream);
	}

protected:
	bool ApplyToDatabase (CBicDatabase &db, LPCTSTR version, CBicByteStream &stream)
	{
		stream.PrepareForReading ();
		while (stream.IsNotEof ())
		{
			long type;
			stream.ReadLong (type);
			switch (type)
			{
				case _directoryCounter:
					{
						long l;
						stream.ReadLong (l);
						if (CBicDirectory::GetCounter () < static_cast<unsigned int> (l))
						{
							CBicDirectory::SetCounter (l);
						}
					}
					break;

				case _fileCounter:
					{
						long l;
						stream.ReadLong (l);
						if (CBicFile::GetCounter () < static_cast<unsigned int> (l))
						{
							CBicFile::SetCounter (l);
						}
					}
					break;
				
				case _productName:
					if (!ApplyProperty (db, _T ("ProductName"), stream))
					{
						return false;
					}
					break;

				case _productCode:
					if (!ApplyProperty (db, _T ("ProductCode"), stream))
					{
						return false;
					}
					break;

				case _manufacturer:
					if (!ApplyProperty (db, _T ("Manufacturer"), stream))
					{
						return false;
					}
					break;

				case _upgradeCode:
					{
						CBicTString v;
						stream.ReadString (v);
						if (v == version)
						{
							if (!ApplyProperty (db, _T ("UpgradeCode"), stream))
							{
								return false;
							}
						}
						else
						{
							CBicTString upCode;
							stream.ReadString (upCode);
							if (!db._legacyMsiInfoPropertyStream.WriteLong (_upgradeCode) ||
								!db._legacyMsiInfoPropertyStream.WriteString (v) ||
								!db._legacyMsiInfoPropertyStream.WriteString (upCode))
							{
								return false;
							}
						}
					}
					break;

				case _directory:
					if (!ApplyDirectory (db, stream))
					{
						return false;
					}
					break;

				case _file:
					if (!ApplyFile (db, stream))
					{
						return false;
					}
					break;
			}
		}

		return true;
	}

	inline bool AddDirectoryCounter ()
	{
		return _propertyStream.WriteLong (_directoryCounter) &&
			_propertyStream.WriteLong (CBicDirectory::GetCounter ());
	}

	inline bool AddFileCounter ()
	{
		return _propertyStream.WriteLong (_fileCounter) &&
			_propertyStream.WriteLong (CBicFile::GetCounter ());
	}

	inline bool AddProductName (LPCTSTR productName)
	{
		return _propertyStream.WriteLong (_productName) &&
			_propertyStream.WriteString (productName);
	}

	inline bool AddProductCode (LPCTSTR productCode)
	{
		return _propertyStream.WriteLong (_productCode) &&
			_propertyStream.WriteString (productCode);
	}

	inline bool AddManufacturer (LPCTSTR manufacturer)
	{
		return _propertyStream.WriteLong (_manufacturer) &&
			_propertyStream.WriteString (manufacturer);
	}

	inline bool AddUpgradeCode (LPCTSTR productVersion, LPCTSTR upgradeCode)
	{
		return _propertyStream.WriteLong (_upgradeCode) &&
			_propertyStream.WriteString (productVersion) &&
			_propertyStream.WriteString (upgradeCode);
	}

	bool ApplyProperty (CBicDatabase &db, LPCTSTR name, CBicByteStream &stream)
	{
		CBicTString value;
		stream.ReadString (value);
		int n = db._properties.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			CBicProperty *prop = db._properties[i];
			if (_tcscmp (prop->GetProperty (), name) == 0)
			{
				prop->SetValue (value);
				return true;
			}
		}
		return false;
	}
	
	inline bool AddDirectory (const CBicDirectory &directory)
	{
		CBicTString dstPath;
		directory.GetDestinationDirectory (dstPath);
		CBicDirectory *parent = directory.GetDirectoryParent ();
		return _directoryStream.WriteLong (_directory) &&
			_directoryStream.WriteString (directory.GetDirectory ()) &&
			_directoryStream.WriteString (dstPath) &&
			_directoryStream.WriteString (parent != NULL ? parent->GetDirectory () : _T ("")) &&
			_directoryStream.WriteString (directory.GetDefaultDir ());
	}

	bool ApplyDirectory (CBicDatabase &db, CBicByteStream &stream)
	{
		bool result = false;
		CBicTString dir, dst, dstPath, parent, defaultDir;
		stream.ReadString (dir);
		stream.ReadString (dst);
		stream.ReadString (parent);
		stream.ReadString (defaultDir);
		int n = db._directories.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			CBicDirectory *d = db._directories[i];
			d->GetDestinationDirectory (dstPath);
			if (dstPath == dst)
			{
				d->SetDirectory (dir);
				result = true;
			}
			else if (dir == d->GetDirectory ())
			{
				d->ChangeDirectory ();
			}
		}
		if (!result)
		{
			// ignore result because of removing some directories in new version
			// remember all old ones
			CBicDirectory *d = NULL;
			for (int i = 0; i < n; ++i)
			{
				d = db._directories[i];
				if (parent == d->GetDirectory ())
				{
					break;
				}
			}

			return db.NewDirectory (dir, defaultDir, d) != NULL;
		}
		return true;
	}

	inline bool AddFile (const CBicFile &file)
	{
		CBicTString dstPath;
		file.GetDestinationPath (dstPath);
		return _fileStream.WriteLong (_file) &&
			_fileStream.WriteString (file.GetFile ()) &&
			_fileStream.WriteString (file.GetComponent ()->GetComponent ()) &&
			_fileStream.WriteString (file.GetComponent ()->GetComponentId ()) &&
			_fileStream.WriteString (file.GetDstFileName ()) &&
			_fileStream.WriteLong (file.GetSequence ()) &&
			_fileStream.WriteString (file.GetComponent ()->GetDirectory ()->GetDirectory ()) &&
			_fileStream.WriteString (dstPath);
	}

	bool ApplyFile (CBicDatabase &db, CBicByteStream &stream)
	{
		bool result = false;
		CBicTString file, component, componentId, fileName, directory, dstPath, tDstPath;
		long sequence;
		stream.ReadString (file);
		stream.ReadString (component);
		stream.ReadString (componentId);
		stream.ReadString (fileName);
		stream.ReadLong (sequence);
		stream.ReadString (directory);
		stream.ReadString (dstPath);
		int n = db._files.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			CBicFile *f = db._files[i];
			f->GetDestinationPath (tDstPath);
			if (tDstPath == dstPath)
			{
				f->SetFile (file);
				const_cast<CBicComponent*> (f->GetComponent ())->SetComponent (component);
				const_cast<CBicComponent*> (f->GetComponent ())->SetComponentId (componentId);
				f->SetDstFileName (fileName);
				f->SetSequence (static_cast<short> (sequence));
				result = true;
			}
			else if (file == f->GetFile ())
			{
				f->ChangeFileComponentSequence ();
			}
		}
		if (!result)
		{
			// ignore result because of removing some files in new version
			// remember all old ones
			CBicDirectory *d;
			n = db._directories.GetSize ();
			for (int i = 0; ; ++i)
			{
				if (i == n)
				{
					return false;
				}

				d = db._directories[i];
				if (directory == d->GetDirectory ())
				{
					break;
				}
			}

			CBicComponent *c = db.NewComponent (d, CBicComponent::attrLocalOnly, _T ("1"), static_cast<short> (sequence));
			if (c == NULL)
			{
				return false;
			}

			CBicRemoveFile *f = db.NewRemoveFile (file, c, fileName, CBicRemoveFile::installModeOnBoth);
			if (f == NULL)
			{
				return false;
			}

			return db._legacyMsiInfoFileStream.WriteLong (_file) &&
				db._legacyMsiInfoFileStream.WriteString (file) &&
				db._legacyMsiInfoFileStream.WriteString (component) &&
				db._legacyMsiInfoFileStream.WriteString (componentId) &&
				db._legacyMsiInfoFileStream.WriteString (fileName) &&
				db._legacyMsiInfoFileStream.WriteLong (sequence) &&
				db._legacyMsiInfoFileStream.WriteString (directory) &&
				db._legacyMsiInfoFileStream.WriteString (dstPath);
		}
		return true;
	}
};
