
//! Class of one record in Error table.
class CBicError
{
protected:
	short _error;
	CBicTString _message;

public:
	CBicError (short error, LPCTSTR message = _T (""));
	
	bool Fill (CBicMsiRecord &rec) const;
};