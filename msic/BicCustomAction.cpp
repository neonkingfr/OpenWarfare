
#include "msic.h"


unsigned int CBicCustomAction::_counter = 0;


CBicCustomAction::CBicCustomAction (short type, CBicBaseProperty *source, LPCTSTR target)
: _type (type), _property (source), _target (target), _binary (NULL)
{
	_action.Format (_T ("Action_%08u"), ++_counter);
}

CBicCustomAction::CBicCustomAction (short type, CBicBinary *source, LPCTSTR target)
: _type (type), _property (NULL), _target (target), _binary (source)
{
	_action.Format (_T ("Action_%08u"), ++_counter);
}

LPCTSTR CBicCustomAction::GetAction () const
{
	return _action;
}

bool CBicCustomAction::Fill (CBicMsiRecord &rec) const
{
	if (!rec.Fill (1, _action) ||
		!rec.Fill (2, _type) ||
		!rec.Fill (3, _property != NULL ? _property->GetProperty () : _binary->GetName ()) ||
		!rec.Fill (4, _target))
	{
		return false;
	}

	return true;
}