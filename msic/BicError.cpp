
#include "msic.h"


CBicError::CBicError (short error, LPCTSTR message /* = _T ("") */)
: _error (error), _message (message)
{
}

bool CBicError::Fill (CBicMsiRecord &rec) const
{
	if (!rec.Fill (1, _error) ||
		!rec.Fill (2, _message))
	{
		return false;
	}

	return true;
}