
//! One property of summary information of MSI database.
class CBicMsiSummaryProperty
{
protected:
	UINT _property;
	UINT _datatype;
	INT _iValue;
	FILETIME *_pftValue;
	FILETIME _pftValueBuffer;
	CBicTString _szValue;

public:
	CBicMsiSummaryProperty (UINT property, UINT datatype);
	CBicMsiSummaryProperty (UINT property, UINT datatype, INT iValue);
	CBicMsiSummaryProperty (UINT property, UINT datatype, FILETIME &pftValue);
	CBicMsiSummaryProperty (UINT property, UINT datatype, LPCTSTR szValue);

	bool GenGuid ();

	bool SetProperty (CBicMsiSummaryInformation &sumInfo) const;
};