
#include "msic.h"


CBicMsiSummaryInformation::CBicMsiSummaryInformation ()
: _summaryInfo (NULL)
{
}

CBicMsiSummaryInformation::~CBicMsiSummaryInformation ()
{
	Close ();
}

MSIHANDLE CBicMsiSummaryInformation::GetMsiHandle () const
{
	return _summaryInfo;
}

bool CBicMsiSummaryInformation::Open (CBicMsiDatabase &db, UINT updateCount)
{
	UINT err = MsiGetSummaryInformation (db.GetMsiHandle (), NULL, updateCount, &_summaryInfo);
	if (err != ERROR_SUCCESS)
	{
		msicLogErr1 (_T ("MsiGetSummaryInformation failed ["MSI_ERR_FMT"].\r\n"), __FILE__, __LINE__, err);
		return false;
	}

	return true;
}

void CBicMsiSummaryInformation::Close ()
{
	if (_summaryInfo != NULL)
	{
		MsiSummaryInfoPersist (_summaryInfo);
		MsiCloseHandle (_summaryInfo);
		_summaryInfo = NULL;
	}
}