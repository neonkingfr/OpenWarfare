
//! Class of one record in TextStyle table.
class CBicTextStyle
{
protected:
	CBicTString _textStyle;
	CBicTString _faceName;
	short _size;
	long _color;
	short _styleBits;
	
public:
	enum
	{
		bitBold = 0x001,
		bitItalic = 0x002,
		bitUnderline = 0x004,
		bitStrike = 0x008,
	};
	CBicTextStyle (LPCTSTR textStyle, LPCTSTR faceName, short size, long color, short styleBits);
	
	bool Fill (CBicMsiRecord &rec) const;
};