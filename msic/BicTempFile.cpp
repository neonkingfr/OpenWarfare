
#include "msic.h"

CBicTempFile::CBicTempFile (LPCTSTR prefix, bool autoDelete)
: CBicDataWriter (autoDelete, FILE_ATTRIBUTE_TEMPORARY), _prefix (prefix)
{
}

CBicTempFile::~CBicTempFile ()
{
}

bool CBicTempFile::Initialize (LPCTSTR path)
{
	CBicTString tempPath (MAX_PATH);
	if (path != NULL)
	{
		tempPath = path;
	}
	else
	{
		if (GetTempPath (MAX_PATH, tempPath.GetBuffer (MAX_PATH)) == 0)
		{
			msicLogErr0 (_T ("GetTempPath failed.\r\n"), __FILE__, __LINE__);
			return false;
		}
	}

	if (GetTempFileName (tempPath, _prefix, 0, _fileName.GetBuffer (MAX_PATH)) == 0)
	{
		msicLogErr0 (_T ("GetTempFileName failed.\r\n"), __FILE__, __LINE__);
		return false;
	}

	_initialized = true;
	return true;
}
