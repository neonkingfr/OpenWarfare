
//! Class of one record in CustomAction table.
class CBicCustomAction
{
protected:
	static unsigned int _counter;

	CBicTString _action;
	short _type;
	CBicBaseProperty *_property;
	CBicBinary *_binary;
	CBicTString _target;
	
public:
	enum 
	{
		typeContinue = 0x00000040,
		typeAsync = 0x00000080,
	};
	CBicCustomAction (short type, CBicBaseProperty *source, LPCTSTR target);
	CBicCustomAction (short type, CBicBinary *source, LPCTSTR target);

	LPCTSTR GetAction () const;

	bool Fill (CBicMsiRecord &rec) const;

	static inline void ResetCounter ()
	{
		_counter = 0;
	}
};