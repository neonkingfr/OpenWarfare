
//! Class of one record in UIText table.
class CBicUIText
{
protected:
	CBicTString _key;
	CBicTString _text;
	
public:
	CBicUIText (LPCTSTR key, LPCTSTR text);

	bool Fill (CBicMsiRecord &rec) const;
};