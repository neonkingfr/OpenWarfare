
//! Class of one record in _Validation table.
class CBicValidation
{
protected:
	CBicTString _table;
	CBicTString _column;
	CBicTString _nullable;
	long _minValue;
	long _maxValue;
	CBicTString _keyTable;
	long _keyColumn;
	CBicTString _category;
	CBicTString _set;
	CBicTString _description;

public:
	CBicValidation (LPCTSTR table, LPCTSTR column, LPCTSTR nullable, long minValue, long maxValue, LPCTSTR keyTable, 
		long keyColumn, LPCTSTR category, LPCTSTR set, LPCTSTR description);

	bool Fill (CBicMsiRecord &rec) const;
};