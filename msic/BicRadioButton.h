
//! Class of one record in RadioButton table.
class CBicRadioButton
{
protected:
	CBicRadioButtonGroup *_group;
	short _order;

	CBicTString _value;
	short _x;
	short _y;
	short _width;
	short _height;
	CBicTString _text;
	CBicTString _help;
	
public:
	CBicRadioButton (CBicRadioButtonGroup *group, LPCTSTR value, short x, short y, short width, short height, LPCTSTR text, LPCTSTR help);

	bool Fill (CBicMsiRecord &rec) const;
};