
#include "msic.h"


CBicFileReader::CBicFileReader ()
: _file (INVALID_HANDLE_VALUE)
{
}

CBicFileReader::~CBicFileReader ()
{
	Close ();
}

bool CBicFileReader::Open (LPCTSTR fileName)
{
	assert (_file == INVALID_HANDLE_VALUE);

  _fileName = fileName;

	_file = CreateFile (fileName, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
	if (_file == INVALID_HANDLE_VALUE)
	{
		// msicLogErr0 (_T ("CreateFile failed.\r\n"), __FILE__, __LINE__);
		return false;
	}

	return true;
}

bool CBicFileReader::Read (unsigned char *buf, unsigned int &size)
{
	assert (_file != INVALID_HANDLE_VALUE);

	DWORD read;
	if (!ReadFile (_file, buf, size, &read, NULL))
	{
		msicLogErr1( _T( "File \"%s\" could not be read.\r\n" ), __FILE__, __LINE__, static_cast< LPCTSTR >( _fileName ) );
		return false;
	}

	size = read;
	return true;
}

bool CBicFileReader::Read (CBicArray<unsigned char, unsigned char> &buf)
{
	assert (_file != INVALID_HANDLE_VALUE);

	DWORD fileSize = GetFileSize (_file, NULL);
	unsigned char *bufPtr = buf.GetBuffer (fileSize);
	DWORD read;
	if (!ReadFile (_file, bufPtr, fileSize, &read, NULL))
	{
		msicLogErr1( _T( "File \"%s\" could not be read.\r\n" ), __FILE__, __LINE__, static_cast< LPCTSTR >( _fileName ) );
		return false;
	}

	return true;
}

bool CBicFileReader::Read (CBicTString &str)
{
	assert (_file != INVALID_HANDLE_VALUE);

	DWORD fileSize = GetFileSize (_file, NULL);
	unsigned int strSize = fileSize / sizeof (TCHAR) + 1;
	unsigned char *buf = reinterpret_cast<unsigned char*> (str.GetBuffer (strSize));
	DWORD read;
	if (!ReadFile (_file, buf, fileSize, &read, NULL))
	{
		msicLogErr1( _T( "File \"%s\" could not be read.\r\n" ), __FILE__, __LINE__, static_cast< LPCTSTR >( _fileName ) );
		return false;
	}

	str.SetAt (strSize - 1, _T ('\0'));
	return true;
}

void CBicFileReader::Close ()
{
	if (_file != INVALID_HANDLE_VALUE)
	{
		CloseHandle (_file);
		_file = INVALID_HANDLE_VALUE;
	}
}