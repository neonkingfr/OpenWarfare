
#include "msic.h"


unsigned int CBicDialog::_counter = 0;


CBicDialog::CBicDialog (short hCentering, short vCentering, short width, short height, long attributes, LPCTSTR title)
: _hCentering (hCentering), _vCentering (vCentering), _width (width), _height (height), _attributes (attributes), 
_controlFirst (NULL), _controlDefault (NULL), _controlCancel (NULL), _title (title)
{
	_dialog.Format (_T ("Dialog_%08u"), ++_counter);
}

CBicDialog::CBicDialog (LPCTSTR dialog, short hCentering, short vCentering, short width, short height, long attributes, LPCTSTR title)
: _hCentering (hCentering), _vCentering (vCentering), _width (width), _height (height), _attributes (attributes), 
_controlFirst (NULL), _controlDefault (NULL), _controlCancel (NULL), _dialog (dialog), _title (title)
{
}

LPCTSTR CBicDialog::GetDialog () const
{
	return _dialog;
}

bool CBicDialog::AddControl (CBicControl *control)
{
	if (!control->SetDialog (this)) { return false; }
	if (!_controls.Add (control)) { return false; }
	if (_controlFirst == NULL) { _controlFirst = control; }
	return true;
}

bool CBicDialog::SetControlFirst (CBicControl *first)
{
	unsigned int n = _controls.GetSize ();
	for (unsigned int i = 0; i < n; ++i)
	{
		if (first == _controls[i])
		{
			_controlFirst = first;
			return true;
		}
	}
	return false;
}

bool CBicDialog::SetControlDefault (CBicControl *def)
{
	unsigned int n = _controls.GetSize ();
	for (unsigned int i = 0; i < n; ++i)
	{
		if (def == _controls[i])
		{
			_controlDefault = def;
			return true;
		}
	}
	return false;
}

bool CBicDialog::SetControlCancel (CBicControl *cancel)
{
	unsigned int n = _controls.GetSize ();
	for (unsigned int i = 0; i < n; ++i)
	{
		if (cancel == _controls[i])
		{
			_controlCancel = cancel;
			return true;
		}
	}
	return false;
}

bool CBicDialog::FillForDialog (CBicMsiRecord &rec) const
{
	if (!rec.Fill (1, _dialog) ||
		!rec.Fill (2, _hCentering) ||
		!rec.Fill (3, _vCentering) ||
		!rec.Fill (4, _width) ||
		!rec.Fill (5, _height) ||
		!rec.Fill (6, _attributes) ||
		!rec.Fill (7, _title) ||
		!rec.Fill (8, _controlFirst->GetControl ()) ||
		!rec.Fill (9, _controlDefault != NULL ? _controlDefault->GetControl () : static_cast<LPCTSTR> (NULL)) ||
		!rec.Fill (10, _controlCancel != NULL ? _controlCancel->GetControl () : static_cast<LPCTSTR> (NULL)))
	{
		return false;
	}

	return true;
}

bool CBicDialog::FillForControl (CBicMsiRecord &rec, unsigned int ctrl) const
{
	CBicControl *c = _controls[ctrl];
	CBicControl *next = NULL;
	if (c->IsTab ())
	{
		if (!FindNextControl (ctrl + 1, next) && !FindNextControl (0, next))
		{
			msicLogErr0 (_T ("Internal error.\r\n"), __FILE__, __LINE__); // no control found in dialog
			return false;
		}
	}

	if (!c->Fill (rec, next))
	{
		return false;
	}

	return true;
}

bool CBicDialog::FindNextControl (unsigned int from, CBicControl *&next) const
{
	unsigned int n = _controls.GetSize ();
	while (from < n)
	{
		CBicControl *c = _controls[from++];
		if (c->IsTab ())
		{
			next = c;
			return true;
		}
	}

	return false;
}

unsigned int CBicDialog::GetControlsSize () const
{
	return _controls.GetSize ();
}