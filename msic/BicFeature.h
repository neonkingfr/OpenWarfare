
//! Class of one record in Feature or FeatureComponents table.
class CBicFeature
{
protected:
	static unsigned int _counter;

	CBicTString _feature;
	CBicFeature *_featureParent;
	CBicTString _title;
	CBicTString _description;
	short _display;
	short _level;
	CBicDirectory *_directory;
	short _attributes;

	CBicArray<CBicComponent*, CBicComponent*> _components;

public:
	enum
	{
		attrFavorLocal = 0x0000,
		attrFavorSource = 0x0001,
		attrFollowParent = 0x0002,
		attrFavorAdvertise = 0x0004,
		attrDisallowAdvertise = 0x0008,
		attrUIDisallowAbsent = 0x0010,
		attrNoUnsupportedAdvertise = 0x0020,
	};

	CBicFeature (LPCTSTR title, LPCTSTR description, short display, short level, CBicDirectory *directory, short attributes, 
		CBicFeature *featureParent = NULL);

	bool AddComponent (CBicComponent *component);
	
	bool FillForFeature (CBicMsiRecord &rec) const;
	bool FillForFeatureComponents (CBicMsiRecord &rec, unsigned int comp) const;

	unsigned int GetComponentsSize () const;

	static inline void ResetCounter ()
	{
		_counter = 0;
	}
};
