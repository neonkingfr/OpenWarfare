
//! Class of one record in ImageFamilies table.
class CBicImageFamily
{
protected:
	static unsigned int _counter;

	CBicTString _family;

public:
	CBicImageFamily ()
	{
		_family.Format (_T ("Fam_%04u"), ++_counter);
	}

	LPCTSTR GetFamily () const
	{
		return _family;
	}

	bool Fill (CBicMsiRecord &rec) const
	{
		if (!rec.Fill (1, _family) ||
			!rec.Fill (2, static_cast<LPCTSTR> (NULL)) ||
			!rec.Fill (3, MSI_NULL_INTEGER) ||
			!rec.Fill (4, MSI_NULL_INTEGER) ||
			!rec.Fill (5, static_cast<LPCTSTR> (NULL)) ||
			!rec.Fill (6, static_cast<LPCTSTR> (NULL)))
		{
			return false;
		}

		return true;
	}

	static inline void ResetCounter ()
	{
		_counter = 0;
	}
};