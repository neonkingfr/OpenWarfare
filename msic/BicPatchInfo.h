
//! Information needed to create patch.
class CBicPatchInfo
{
protected:
	CBicArraySelfDestruct<CBicTString*, CBicTString*> _versionDirs;
	CBicArraySelfDestruct<CBicTString*, CBicTString*> _destinationDirs;
	CBicArraySelfDestruct<CBicTString*, CBicTString*> _destinationFileNames;

	CBicArraySelfDestruct<CBicImageFamily*, CBicImageFamily*> _imageFamilies;
	CBicArraySelfDestruct<CBicProperty*, CBicProperty*> _properties;
	CBicArraySelfDestruct<CBicUpgradedImage*, CBicUpgradedImage*> _upgradedImages;
	CBicArraySelfDestruct<CBicTargetImage*, CBicTargetImage*> _targetImages;

public:
	bool ReadVersionPaths (LPCTSTR line)
	{
		if (line == NULL)
		{
			return 1;
		}

		int verIdx = 1;
		for (;;)
		{
			LPCTSTR end = _tcspbrk (line, _T ("\n\r"));
			int lineLen = end != NULL ? (end - line) : _tcslen (line);
			if (lineLen == 0)
			{
				break;
			}

			CBicTString *path = new CBicTString (line, lineLen);
			if (path == NULL) { return false; }
			if (!_versionDirs.Add (path))
			{
				delete path;
				return false;
			}

			path = new CBicTString ();
			if (path == NULL) { return false; }
			path->Format (_T (".\\tmp_install_%03u\\"), verIdx);
			if (!_destinationDirs.Add (path))
			{
				delete path;
				return false;
			}

			path = new CBicTString ();
			if (path == NULL) { return false; }
			if (!_destinationFileNames.Add (path))
			{
				delete path;
				return false;
			}

			if (end == NULL)
			{
				break;
			}

			line = end + 1;
			while (*line == _T ('\n') || *line == _T ('\r'))
			{
				++line;
			}
			++verIdx;
		};

		return true;
	}

	void SetDestinationFileName (unsigned int i, LPCTSTR fileName)
	{
		*_destinationFileNames[i] = fileName;
	}

	inline LPCTSTR GetVersionPath (unsigned int i) const
	{
		return _versionDirs[i]->operator LPCTSTR ();
	}

	inline LPCTSTR GetDestinationPath (unsigned int i) const
	{
		return _destinationDirs[i]->operator LPCTSTR ();
	}

	inline unsigned int GetVersionSize () const
	{
		return _versionDirs.GetSize ();
	}
	
	bool CreatePatch (LPCTSTR patchPath, LPCTSTR upgrPath, bool tempDelete)
	{
		if (!SaveAsPcp (_T ("patch.pcp"), upgrPath))
		{
			return false;
		}

		HMODULE patchWizDll = LoadLibrary (_T ("PatchWiz.dll"));
		if (patchWizDll == NULL)
		{
			msicLogErr0 (_T ("File \"PatchWiz.dll\" not found.\r\n"), __FILE__, __LINE__);
			return false;
		}

		FARPROC proc;
#ifndef UNICODE
		proc = GetProcAddress (patchWizDll, _T ("UiCreatePatchPackageA"));
#else
		proc = GetProcAddress (patchWizDll, _T ("UiCreatePatchPackageW"));
#endif

		if (proc == NULL)
		{
			msicLogErr0 (_T ("Function \"UiCreatePatchPackage\" not found in \"PatchWiz.dll\".\r\n"), __FILE__, __LINE__);
			FreeLibrary (patchWizDll);
			return false;
		}
		
		typedef UINT (WINAPI *procType) (LPSTR, LPSTR, LPSTR, HWND, LPSTR, BOOL);
		UINT res = ((procType) proc) (_T ("patch.pcp"), _T ("patch.msp"), NULL, NULL, _T ("pcw_temp"), TRUE);
		if (res != ERROR_SUCCESS)
		{
			msicLogErr1 (_T ("UiCreatePatchPackage failed ["MSI_ERR_FMT"].\r\n"), __FILE__, __LINE__, res);
			FreeLibrary (patchWizDll);
			return false;
		}
		
		FreeLibrary (patchWizDll);

		if (tempDelete)
		{
			RemoveWholeDirectory (_T ("pcw_temp\\"));
		}

		return true;
	}

protected:
	bool SaveAsPcp (LPCTSTR pcpPath, LPCTSTR upgrPath)
	{
		ResetCounters ();

		CBicMsiDatabase db;
		if (!db.Create (_T (".\\"), _T ("patch.pcp")))
		{
			return false;
		}

#define ERR_SEQ				db.Close (); return false


		if (!db.ExeCmd (_T ("CREATE TABLE `ExternalFiles` ("
			"  `Family` CHAR(8) NOT NULL,"
			"  `FTK` CHAR(128) NOT NULL,"
			"  `FilePath` CHAR(255) NOT NULL,"
			"  `SymbolPaths` CHAR(255),"
			"  `IgnoreOffsets` CHAR(255),"
			"  `IgnoreLengths` CHAR(255),"
			"  `RetainOffsets` CHAR(255),"
			"  `Order` SHORT NOT NULL PRIMARY KEY `Family`, `FTK`, `FilePath`)")))
		{
			ERR_SEQ;
		}

		if (!db.ExeCmd (_T ("CREATE TABLE `FamilyFileRanges` ("
			"  `Family` CHAR(13) NOT NULL,"
			"  `FTK` CHAR(128) NOT NULL,"
			"  `RetainOffsets` CHAR(128) NOT NULL,"
			"  `RetainLengths` CHAR(128) NOT NULL PRIMARY KEY `Family`, `FTK`)")))
		{
			ERR_SEQ;
		}

		if (!db.ExeCmd (_T ("CREATE TABLE `TargetFiles_OptionalData` ("
			"  `Target` CHAR(13) NOT NULL,"
			"  `FTK` CHAR(255) NOT NULL,"
			"  `SymbolPaths` CHAR(255),"
			"  `IgnoreOffsets` CHAR(255),"
			"  `IgnoreLengths` CHAR(255),"
			"  `RetainOffsets` CHAR(255) PRIMARY KEY `Target`, `FTK`)")))
		{
			ERR_SEQ;
		}

		if (!db.ExeCmd (_T ("CREATE TABLE `UpgradedFilesToIgnore` ("
			"  `Upgraded` CHAR(13) NOT NULL,"
			"  `FTK` CHAR(255) NOT NULL PRIMARY KEY `Upgraded`, `FTK`)")))
		{
			ERR_SEQ;
		}

		if (!db.ExeCmd (_T ("CREATE TABLE `UpgradedFiles_OptionalData` ("
			"  `Upgraded` CHAR(13) NOT NULL,"
			"  `FTK` CHAR(255) NOT NULL,"
			"  `SymbolPaths` CHAR(255),"
			"  `AllowIgnoreOnPatchError` SHORT,"
			"  `IncludeWholeFile` SHORT PRIMARY KEY `Upgraded`, `FTK`)")))
		{
			ERR_SEQ;
		}

#define CHECK_NEW(___v)	\
	if (___v == NULL) \
	{\
		ERR_SEQ; \
	}

		// ImageFamilies
		CBicImageFamily *fam = NewImageFamily ();
		CHECK_NEW (fam)
		if (!SavePcpImageFamilies (db))
		{
			ERR_SEQ;
		}

		// Properties
		CBicProperty *prop;
		prop = NewProperty (_T ("AllowProductCodeMismatches"), _T ("1"));
		CHECK_NEW (prop)
		prop = NewProperty (_T ("AllowProductVersionMajorMismatches"), _T ("1"));
		CHECK_NEW (prop)
		prop = NewProperty (_T ("ApiPatchingSymbolFlags"), _T ("0x00000000"));
		CHECK_NEW (prop)
		prop = NewProperty (_T ("DontRemoveTempFolderWhenFinished"), _T ("1"));
		CHECK_NEW (prop)
		prop = NewProperty (_T ("IncludeWholeFilesOnly"), _T ("0"));
		CHECK_NEW (prop)
		prop = NewProperty (_T ("ListOfPatchGUIDsToReplace"), _T (""));
		CHECK_NEW (prop)
		prop = NewProperty (_T ("ListOfTargetProductCodes"), _T ("*"));
		CHECK_NEW (prop)
		prop = NewProperty (_T ("MinimumRequiredMsiVersion"), _T ("200"));
		CHECK_NEW (prop)
		CBicTString tmpGuid;
		if (!GenGuid (tmpGuid))
		{
			ERR_SEQ;
		}
		prop = NewProperty (_T ("PatchGUID"), tmpGuid);
		CHECK_NEW (prop)
		prop = NewProperty (_T ("PatchOutputPath"), _T ("patch.msp"));
		CHECK_NEW (prop)
		if (!SavePcpProperties (db))
		{
			ERR_SEQ;
		}

		// UpgradedImages
		CBicUpgradedImage *upgr = NewUpgradedImage (upgrPath, fam);
		CHECK_NEW (upgr)
		if (!SavePcpUpgradedImages (db))
		{
			ERR_SEQ;
		}

		// TargetImages
		CBicTString tmpStr;
		int n = _destinationDirs.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			tmpStr = *_destinationDirs[i];
			tmpStr += *_destinationFileNames[i];
			CBicTargetImage *tar = NewTargetImage (tmpStr, upgr, 0);
			CHECK_NEW (tar)
		}
		if (!SavePcpTargetImages (db))
		{
			ERR_SEQ;
		}

#undef CHECK_NEW
#undef ERR_SEQ

		db.Close ();
		return true;
	}

	CBicImageFamily* NewImageFamily ()
	{
		CBicImageFamily *fam = new CBicImageFamily ();
		if (fam != NULL)
		{
			if (_imageFamilies.Add (fam))
			{
				return fam;
			}

			delete fam;
		}
		else
		{
			msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
		}

		return NULL;
	}

	bool SavePcpImageFamilies (CBicMsiDatabase &db)
	{
		if (!db.ExeCmd (_T ("CREATE TABLE `ImageFamilies` ("
			"  `Family` CHAR(8) NOT NULL,"
			"  `MediaSrcPropName` CHAR(72),"
			"  `MediaDiskId` SHORT,"
			"  `FileSequenceStart` SHORT,"
			"  `DiskPrompt` CHAR(128),"
			"  `VolumeLabel` CHAR(32) PRIMARY KEY `Family`)")))
		{
			return false;
		}

		CBicMsiView view;
		if (!view.Open (db, _T ("INSERT INTO `ImageFamilies` ("
			"  `Family`,"
			"  `MediaSrcPropName`,"
			"  `MediaDiskId`,"
			"  `FileSequenceStart`,"
			"  `DiskPrompt`,"
			"  `VolumeLabel`)"
			"  VALUES (?, ?, ?, ?, ?, ?)")))
		{
			return false;
		}

		CBicMsiRecord rec;
		if (!rec.Open (6))
		{
			return false;
		}

		int n = _imageFamilies.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			if (!_imageFamilies[i]->Fill (rec) || !view.Execute (rec))
			{
				rec.Close ();
				view.Close ();
				return false;
			}
		}

		rec.Close ();
		view.Close ();

		return true;
	}

	CBicProperty *NewProperty (LPCTSTR name, LPCTSTR value)
	{
		CBicProperty *p = new CBicProperty (name, value);
		if (p != NULL)
		{
			if (_properties.Add (p))
			{
				return p;
			}

			delete p;
		}
		else
		{
			msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
		}

		return NULL;
	}

	bool SavePcpProperties (CBicMsiDatabase &db)
	{
		if (!db.ExeCmd (_T ("CREATE TABLE `Properties` ("
			"  `Name` CHAR(72) NOT NULL,"
			"  `Value` CHAR(128) LOCALIZABLE PRIMARY KEY `Name`)")))
		{
			return false;
		}

		CBicMsiView view;
		if (!view.Open (db, _T ("INSERT INTO `Properties` ("
			"  `Name`,"
			"  `Value`)"
			"  VALUES (?, ?)")))
		{
			return false;
		}

		CBicMsiRecord rec;
		if (!rec.Open (2))
		{
			return false;
		}

		int n = _properties.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			CBicProperty *p = _properties[i];
			if (p->IsStoring ())
			{
				if (!p->Fill (rec) || !view.Execute (rec))
				{
					rec.Close ();
					view.Close ();
					return false;
				}
			}
		}

		rec.Close ();
		view.Close ();

		return true;
	}

	CBicUpgradedImage *NewUpgradedImage (LPCTSTR msiPath, CBicImageFamily *family)
	{
		CBicUpgradedImage *p = new CBicUpgradedImage (msiPath, family);
		if (p != NULL)
		{
			if (_upgradedImages.Add (p))
			{
				return p;
			}

			delete p;
		}
		else
		{
			msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
		}

		return NULL;
	}

	bool SavePcpUpgradedImages (CBicMsiDatabase &db)
	{
		if (!db.ExeCmd (_T ("CREATE TABLE `UpgradedImages` ("
			"  `Upgraded` CHAR(13) NOT NULL,"
			"  `MsiPath` CHAR(255) NOT NULL,"
			"  `PatchMsiPath` CHAR(255),"
			"  `SymbolPaths` CHAR(255),"
			"  `Family` CHAR(8) NOT NULL PRIMARY KEY `Upgraded`)")))
		{
			return false;
		}

		CBicMsiView view;
		if (!view.Open (db, _T ("INSERT INTO `UpgradedImages` ("
			"  `Upgraded`,"
			"  `MsiPath`,"
			"  `PatchMsiPath`,"
			"  `SymbolPaths`,"
			"  `Family`)"
			"  VALUES (?, ?, ?, ?, ?)")))
		{
			return false;
		}

		CBicMsiRecord rec;
		if (!rec.Open (5))
		{
			return false;
		}

		int n = _upgradedImages.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			CBicUpgradedImage *p = _upgradedImages[i];
			if (!p->Fill (rec) || !view.Execute (rec))
			{
				rec.Close ();
				view.Close ();
				return false;
			}
		}

		rec.Close ();
		view.Close ();

		return true;
	}

	CBicTargetImage *NewTargetImage (LPCTSTR msiPath, CBicUpgradedImage *upgraded, short ignoreMissingSrcFiles)
	{
		CBicTargetImage *p = new CBicTargetImage (msiPath, upgraded, ignoreMissingSrcFiles);
		if (p != NULL)
		{
			if (_targetImages.Add (p))
			{
				return p;
			}

			delete p;
		}
		else
		{
			msicLogErr0 (_T ("Not enough memory.\r\n"), __FILE__, __LINE__);
		}

		return NULL;
	}

	bool SavePcpTargetImages (CBicMsiDatabase &db)
	{
		if (!db.ExeCmd (_T ("CREATE TABLE `TargetImages` ("
			"  `Target` CHAR(13) NOT NULL,"
			"  `MsiPath` CHAR(255) NOT NULL,"
			"  `SymbolPaths` CHAR(255),"
			"  `Upgraded` CHAR(13) NOT NULL,"
			"  `Order` SHORT NOT NULL,"
			"  `ProductValidateFlags` CHAR(16),"
			"  `IgnoreMissingSrcFiles` SHORT NOT NULL PRIMARY KEY `Target`)")))
		{
			return false;
		}

		CBicMsiView view;
		if (!view.Open (db, _T ("INSERT INTO `TargetImages` ("
			"  `Target`,"
			"  `MsiPath`,"
			"  `SymbolPaths`,"
			"  `Upgraded`,"
			"  `Order`,"
			"  `ProductValidateFlags`,"
			"  `IgnoreMissingSrcFiles`)"
			"  VALUES (?, ?, ?, ?, ?, ?, ?)")))
		{
			return false;
		}

		CBicMsiRecord rec;
		if (!rec.Open (7))
		{
			return false;
		}

		int n = _targetImages.GetSize ();
		for (int i = 0; i < n; ++i)
		{
			CBicTargetImage *p = _targetImages[i];
			if (!p->Fill (rec) || !view.Execute (rec))
			{
				rec.Close ();
				view.Close ();
				return false;
			}
		}

		rec.Close ();
		view.Close ();

		return true;
	}
	
	inline void ResetCounters ()
	{
		CBicImageFamily::ResetCounter ();
		CBicTargetImage::ResetCounter ();
		CBicUpgradedImage::ResetCounter ();
	}
};