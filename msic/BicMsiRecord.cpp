
#include "msic.h"


CBicMsiRecord::CBicMsiRecord ()
: _record (NULL)
{
}

CBicMsiRecord::~CBicMsiRecord ()
{
	Close ();
}

bool CBicMsiRecord::Open (unsigned int nParams)
{
	if (_record != NULL)
	{
		msicLogErr0 (_T ("MSI record already opened.\r\n"), __FILE__, __LINE__);
		return false;
	}

	_record = MsiCreateRecord (nParams);
	if (_record == NULL)
	{
		msicLogErr0 (_T ("MsiCreateRecord failed.\r\n"), __FILE__, __LINE__);
		return false;
	}

	return true;
}

void CBicMsiRecord::Close ()
{
	if (_record != NULL)
	{
		MsiCloseHandle (_record);
		_record = NULL;
	}
}

MSIHANDLE CBicMsiRecord::GetMsiHandle () const
{
	return _record;
}

bool CBicMsiRecord::Fill (unsigned int param, LPCTSTR data) const
{
	if (_record == NULL)
	{
		msicLogErr0 (_T ("MSI record is not opened.\r\n"), __FILE__, __LINE__);
		return false;
	}
	
	UINT err = MsiRecordSetString (_record, param, data);
	if (err != ERROR_SUCCESS)
	{
		msicLogErr1 (_T ("MsiRecordSetString failed ["MSI_ERR_FMT"].\r\n"), __FILE__, __LINE__, err);
		return false;
	}

	return true;
}

bool CBicMsiRecord::Fill (unsigned int param, int data) const
{
	if (_record == NULL)
	{
		msicLogErr0 (_T ("MSI record is not opened.\r\n"), __FILE__, __LINE__);
		return false;
	}
	
	UINT err = MsiRecordSetInteger (_record, param, data);
	if (err != ERROR_SUCCESS)
	{
		msicLogErr1 (_T ("MsiRecordSetInteger failed ["MSI_ERR_FMT"].\r\n"), __FILE__, __LINE__, err);
		return false;
	}
	
	return true;
}

bool CBicMsiRecord::FillStream (unsigned int param, LPCTSTR data) const
{
	if (_record == NULL)
	{
		msicLogErr0 (_T ("MSI record is not opened.\r\n"), __FILE__, __LINE__);
		return false;
	}
	
	UINT err = MsiRecordSetStream (_record, param, data);
	if (err != ERROR_SUCCESS)
	{
		msicLogErr1 (_T ("MsiRecordSetString failed ["MSI_ERR_FMT"].\r\n"), __FILE__, __LINE__, err);
		return false;
	}

	return true;
}
