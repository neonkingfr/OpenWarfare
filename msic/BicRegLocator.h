
//! Class of one record in AppSearch and RegLocator table.
class CBicRegLocator
{
protected:
	static unsigned int _counter;

	CBicBaseProperty *_property;
	CBicTString _signature;
	short _root;
	CBicTString _key;
	CBicTString _name;
	short _type;
	
public:
	enum
	{
		rootClassesRoot = 0x0000,
		rootCurrentUser = 0x0001,
		rootLocalMachine = 0x0002,
		rootUsers = 0x003,
	};

	enum
	{
		typeDirectory = 0x0000,
		typeFileName = 0x0001,
		typeRawValue = 0x0002,
		type64bit = 0x0010,
	};

	CBicRegLocator (CBicBaseProperty *property, short root, LPCTSTR key, LPCTSTR name, short type);

	bool FillForAppSearch (CBicMsiRecord &rec) const;
	bool FillForRegLocator (CBicMsiRecord &rec) const;

	static inline void ResetCounter ()
	{
		_counter = 0;
	}
};