

class CBicControl;

//! Class of one record in Dialog table. Contains also all controls of this dialog for Control table.
class CBicDialog
{
protected:
	static unsigned int _counter;

	CBicTString _dialog;
	short _hCentering;
	short _vCentering;
	short _width;
	short _height;
	long _attributes;
	CBicTString _title;
	CBicArray<CBicControl*, CBicControl*> _controls;
	CBicControl *_controlFirst;
	CBicControl *_controlDefault;
	CBicControl *_controlCancel;
	
public:
	enum
	{
		attrVisible = 0x00000001L,
		attrModal = 0x00000002L,
		attrMinimize = 0x00000004L,
		attrSysModal = 0x00000008L,
		attrKeepModeless = 0x00000010L,
		attrTrackDiskSpace = 0x00000020L,
		attrUseCustomPalette = 0x00000040L,
		attrRTLRO = 0x00000080L,
		attrRightAligned = 0x00000100L,
		attrLeftScroll = 0x00000200L,
		attrBiDi = attrRTLRO | attrRightAligned | attrLeftScroll,
		attrError = 0x00010000L,
	};

	CBicDialog (short hCentering, short vCentering, short width, short height, long attributes, LPCTSTR title);
	CBicDialog (LPCTSTR dialog, short hCentering, short vCentering, short width, short height, long attributes, LPCTSTR title);

	LPCTSTR GetDialog () const;
	unsigned int GetControlsSize () const;

	bool AddControl (CBicControl *control);

	bool SetControlFirst (CBicControl *first);
	bool SetControlDefault (CBicControl *def);
	bool SetControlCancel (CBicControl *cancel);
	
	bool FillForDialog (CBicMsiRecord &rec) const;
	bool FillForControl (CBicMsiRecord &rec, unsigned int ctrl) const;
	bool FindNextControl (unsigned int from, CBicControl *&next) const;

	static inline void ResetCounter ()
	{
		_counter = 0;
	}
};