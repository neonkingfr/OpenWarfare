#ifndef __OBRAZKY
#define __OBRAZKY

/* definice zakladni tridy pro praci s obrazem */
enum {MaxPal=256};

enum {RC=3,GC=5,BC=2}; /* vyznam slozky v jasu */
enum {Jas1=0xff*(RC+GC+BC)};

typedef struct
{
	void *Obr;
	/* obrazek organizovan podobne jako v ST video - za sebou wordy vsech planu */
	int W,H; /* rozmery obrazku v pixelech */
	long RGBPal[MaxPal]; /* 0x00RRGGBB */
	int LenPal;
	Flag Direct; /* Direct Color / Palety */ /* direct color je pixel paket */
	Flag Gray; /* nejsou slozky - jen jas! */
	int NPlanu; /* monochromni obrazek je Direct a Gray, 1 plan */
} Obrazek;

/* z�kladn� vstupn� filtry */

/* na��t�n� v re�imu rgb565 */
int NactiTC16( const char *S, Obrazek *Obr );

/* na��t�n� v re�imu rgb888 */
int NactiTC24( const char *S, Obrazek *Obr );

#endif