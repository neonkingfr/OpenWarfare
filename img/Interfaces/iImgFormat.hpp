#ifdef _MSC_VER
#pragma once
#endif

#ifndef _IMG_I_IMG_FORMAT_HPP
#define _IMG_I_IMG_FORMAT_HPP

#include <Es/Containers/array.hpp>

/// picture loading interface
class IPictureFormat
{
  public:
  /// pixel type (32b for ARGB8888)
  typedef unsigned long Pixel;
  
  /// load picture
  /**
  @param w picture horizontal size
  @param h picture vertical size
  @param argbData picture data allocated using new Pixel[w*h]
  */
  virtual int Load(const char *name, int &w, int &h, Pixel *&argbData)
  {
    return -1;
  }
  /// save picture buffer
  virtual int Save(const char *name, int w, int h, const Pixel *argbData)
  {
    return -1;
  }
  /// load, rows allocated separately
  /** Big pictures often cannot be allocated as one region
  due to virtual address space fragmentation
  */
  virtual int LoadRows(const char *name, int &w, AutoArray< SRefArray<Pixel> > &argbRows)
  {
    // by default we implement this using Load
    int h;
    Pixel *argbData = NULL;
    int ret = Load(name,w,h,argbData);
    if (ret>=0 && argbData)
    {
      argbRows.Resize(h);
      for (int i=0; i<h; i++)
      {
        argbRows[i].Realloc(w);
        memcpy(argbRows[i].Data(),argbData+i*w,w*sizeof(Pixel));
      }
    }
    return ret;
  }
};

/// plug-in points for prepared file formats

extern IPictureFormat *GFormatPNG;
//extern IPictureFormat *GFormatJPG;
//extern IPictureFormat *GFormatGIF;
//extern IPictureFormat *GFormatTGA;

#endif

