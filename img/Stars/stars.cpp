// command line interface for star conversion

#include <windows.h>
#include <stdio.h>

// command line interface to dll

const int DefTreshold=35;

int ConvertStars( const char *s1, const char *d, int thold );

int consoleMain( int argc, const char *argv[] )
{
	int treshold=DefTreshold;

	const char *S1=NULL,*S2=NULL,*D=NULL;
	
	const char **argv0=argv;
	while( argc>1 )
	{
		const char *arg=argv[1];
		if( *arg=='-' || *arg=='/' )
		{
			arg++;
			static const char limitText[]="thold=";
			if( !strnicmp(arg,limitText,strlen(limitText)) )
			{
				arg+=strlen(limitText);
				treshold=atoi(arg);
			}
			else goto Usage;
		}
		else
		{
			if( !S1 ) S1=arg;
			else if( !S2 ) S2=arg;
			else if( !D ) D=arg;
			else goto Usage;
		}
		argc--,argv++;
	}
	if( !S1 || !S2 )
	{
		Usage:
		printf("Arguments used:\n");
		for( int i=1; argv0[i]; i++ )
		{
			printf("'%s'\n",argv0[i]);
		}
		
		printf("Usage:\tstars [-thold=<number>] <north_map> <south_map> <destination>\n");
		printf("Or:\tstars [-thold=<number>] <catalog.dat> <destination>\n");
		printf("Example:\tstars north.gif south.gif stars.str\n\n");
		printf("\n");
		exit(1);
	}

	// star catalogue
	return ConvertStars(S1,S2,treshold);
}

