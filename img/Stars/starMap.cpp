// read Brightest Star Catalogue and convert it to a vector representation

#include <Es/platform.hpp>
#include <El/Math/math3d.hpp>
#include <El/QStream/QStream.hpp>
#include <Es/Containers/array.hpp>

struct Star
{
	float _x,_y,_z;
	float _brightness;
	Star(){}
	Star( const Vector3 &pos, float brightness )
	:_x(pos.X()),_y(pos.Y()),_z(pos.Z()),_brightness(brightness)
	{
	}
};

TypeIsMovableZeroed(Star)

#pragma pack(push,1)
struct CatalogueEntry
{
	char HR[4];
	//	 1-  4  I4     ---       HR         [1/9110]+ Harvard Revised Number
	//                                      = Bright Star Number
	char Name[10];
	//   5- 14  A10    ---       Name       Name, generally Bayer and/or Flamsteed name	
	char DM[11];
	//  15- 25  A11    ---       DM         Durchmusterung Identification (zone in
	//                                      bytes 17-19)
	char HD[6];
	//  26- 31  I6     ---       HD         [1/225300]? Henry Draper Catalog Number
	char SAO[6];
	//  32- 37  I6     ---       SAO        [1/258997]? SAO Catalog Number
	char FK5[4];
	//  38- 41  I4     ---       FK5        ? FK5 star Number
	char IRFlag;
	//      42  A1     ---       IRflag     [I] I if infrared source
	char r_IRFlag;
	//      43  A1     ---       r_IRflag  *[ ':] Coded reference for infrared source
	char Multiple;
	//      44  A1     ---       Multiple  *[AWDIRS] Double or multiple-star code
	char ADS[5];
	//  45- 49  A5     ---       ADS        Aitken's Double Star Catalog (ADS) designation
	char ADScomp[2];
	//  50- 51  A2     ---       ADScomp    ADS number components
	char VarID[9];
	//  52- 60  A9     ---       VarID      Variable star identification
	char RAh1900[2];
	//  61- 62  I2     h         RAh1900    ?Hours RA, equinox B1900, epoch 1900.0 (1)
	char RAm1900[2];
	//  63- 64  I2     min       RAm1900    ?Minutes RA, equinox B1900, epoch 1900.0 (1)
	char RAs1900[4];
	//  65- 68  F4.1   s         RAs1900    ?Seconds RA, equinox B1900, epoch 1900.0 (1)
	char DEn1900;
	//      69  A1     ---       DE-1900    ?Sign Dec, equinox B1900, epoch 1900.0 (1)
	char DEd1900[2];
	//  70- 71  I2     deg       DEd1900    ?Degrees Dec, equinox B1900, epoch 1900.0 (1)
	char DEm1900[2];
	//  72- 73  I2     arcmin    DEm1900    ?Minutes Dec, equinox B1900, epoch 1900.0 (1)
	char DEs1900[2];
	//  74- 75  I2     arcsec    DEs1900    ?Seconds Dec, equinox B1900, epoch 1900.0 (1)
	char RAh[2];
	//  76- 77  I2     h         RAh        ?Hours RA, equinox J2000, epoch 2000.0 (1)
	char RAm[2];
	//  78- 79  I2     min       RAm        ?Minutes RA, equinox J2000, epoch 2000.0 (1)
	char RAs[4];
	//  80- 83  F4.1   s         RAs        ?Seconds RA, equinox J2000, epoch 2000.0 (1)
	char DEn;
	//      84  A1     ---       DE-        ?Sign Dec, equinox J2000, epoch 2000.0 (1)
	char DEd[2];
	//  85- 86  I2     deg       DEd        ?Degrees Dec, equinox J2000, epoch 2000.0 (1)
	char DEm[2];
	//  87- 88  I2     arcmin    DEm        ?Minutes Dec, equinox J2000, epoch 2000.0 (1)
	char DEs[2];
	//  89- 90  I2     arcsec    DEs        ?Seconds Dec, equinox J2000, epoch 2000.0 (1)
	char GLON[6];
	//  91- 96  F6.2   deg       GLON       ?Galactic longitude (1)
	char GLAT[6];
	//  97-102  F6.2   deg       GLAT       ?Galactic latitude (1)
	char Vmag[5];
	// 103-107  F5.2   mag       Vmag       ?Visual magnitude (1)
	char n_Vmag;
	//     108  A1     ---       n_Vmag    *[ HR] Visual magnitude code
	char u_Vmag;
	//     109  A1     ---       u_Vmag     [ :?] Uncertainty flag on V
	char BnV[5];
	// 110-114  F5.2   mag       B-V        ? B-V color in the UBV system
	char u_BnV;
	//     115  A1     ---       u_B-V      [ :?] Uncertainty flag on B-V
	char UnB[5];
	// 116-120  F5.2   mag       U-B        ? U-B color in the UBV system
	char u_UnB;
	//     121  A1     ---       u_U-B      [ :?] Uncertainty flag on U-B
	char RnI[5];
	// 122-126  F5.2   mag       R-I        ? R-I   in system specified by n_R-I
	char n_RnI;
	//     127  A1     ---       n_R-I      [CE:?D] Code for R-I system (Cousin, Eggen)
	char SpType[20];
	// 128-147  A20    ---       SpType     Spectral type
	char n_SpType;
	//     148  A1     ---       n_SpType   [evt] Spectral type code
	char pmRA[6];
	// 149-154  F6.3   arcsec/yr pmRA       ?Annual proper motion in RA J2000, FK5 system
	char pmDE[6];
	// 155-160  F6.3   arcsec/yr pmDE       ?Annual proper motion in Dec J2000, FK5 system
	char n_Parallax;
	//     161  A1     ---       n_Parallax [D] D indicates a dynamical parallax,
	//                                      otherwise a trigonometric parallax
	char Parallax[5];
	// 162-166  F5.3   arcsec    Parallax   ? Trigonometric parallax (unless n_Parallax)
	char RadVel[4];
	// 167-170  I4     km/s      RadVel     ? Heliocentric Radial Velocity
	char n_RadVel[4];
	// 171-174  A4     ---       n_RadVel  *[V?SB123O ] Radial velocity comments
	char l_RotVel[2];
	// 175-176  A2     ---       l_RotVel   [<=> ] Rotational velocity limit characters
	char RotVel[3];
	// 177-179  I3     km/s      RotVel     ? Rotational velocity, v sin i
	char u_RotVel[1];
	//     180  A1     ---       u_RotVel   [ :v] uncertainty and variability flag on
	//                                      RotVel
	char Dmag[4];
	// 181-184  F4.1   mag       Dmag       ? Magnitude difference of double,
	//                                        or brightest multiple
	char Sep[6];
	// 185-190  F6.1   arcsec    Sep        ? Separation of components in Dmag
	//                                        if occultation binary.
	char MultID[4];
	// 191-194  A4     ---       MultID     Identifications of components in Dmag
	char MultCnt[2];
	// 195-196  I2     ---       MultCnt    ? Number of components assigned to a multiple
	char NoteFlag;
	//     197  A1     ---       NoteFlag   [*] a star indicates that there is a note
	//                                        (file notes.dat)
};
#pragma pack(pop)

static int GetInt( char *f, int len )
{
	char buf[24];
	strncpy(buf,f,len);
	buf[len]=0;
	return atoi(buf);
}

static float GetFloat( char *f, int len, int dec )
{
	char buf[24];
	strncpy(buf,f,len);
	buf[len]=0;
	return (float)atof(buf);
}

static void ScanCatalogue(AutoArray<Star> &starList, QIStream &f, int thold)
{
	Assert( sizeof(CatalogueEntry)==197 );
	float limit=thold*(1.0f/512);
	for(;;)
	{
		union
		{
			char line[256];
			CatalogueEntry e;
		};
		memset(line,0,sizeof(line));
		f.readLine(line,sizeof(line));
		if( f.eof() || f.fail() ) break;
		// get visual magnitude
		float mag=GetFloat(e.Vmag,5,2);
		float brightness=pow(0.5f,mag);
		brightness*=2;
		if( brightness<limit ) continue;
		// we found a star
		// we have to calculate current position
		// first convert from rectangular to polar coordinates
		float longitude=
		(
			GetInt(e.RAh,2)*(H_PI*2)/(24)+
			GetInt(e.RAm,2)*(H_PI*2)/(24*60)+
			GetFloat(e.RAs,4,1)*(H_PI*2)/(24*60*60)
		);
		float latitude=
		(
			GetInt(e.DEd,2)*(H_PI*2)/(360)+
			GetInt(e.DEm,2)*(H_PI*2)/(360*60)+
			GetInt(e.DEs,2)*(H_PI*2)/(360*60*60)
		);
		if( longitude==0 && latitude==0 ) continue; // deleted
		if( e.DEn!='-' ) latitude=-latitude;
		longitude-=H_PI/2;
		// now we use these as angle parameters (euler rotation)
		{
			Matrix3 latitudeR(MRotationX,latitude);
			Matrix3 longitudeR(MRotationY,longitude);
			Vector3 direction=(longitudeR*latitudeR).Direction();
			saturate(brightness,0,1);
			starList.Add(Star(direction,brightness));
		}
	}
}


int ConvertStars( const char *s1, const char *d, int thold )
{
	AutoArray<Star> starList;
	QIFStream in;
	in.open(s1);
	// scan both pictures and create a list of stars
	ScanCatalogue(starList,in,thold);
	// save resulting starlist
	QOFStream f;
	f.open(d);
	int n=starList.Size();
	f.write((char *)&n,sizeof(n));
	f.write((char *)&starList[0],sizeof(starList[0])*starList.Size());
	return 0;
}

