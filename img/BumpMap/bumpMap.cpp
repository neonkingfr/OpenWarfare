/*!
\file
Bump map from height map or tree texture.
*/


#include <direct.h>
#include <io.h>
#include <fcntl.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <class/debugLog.hpp>
#include <class/fltopts.hpp>
#include <class/filenames.hpp>
#include <class/array.hpp>
#include <class/rstring.hpp>
#include <memtype.h>
#include "..\pics\macros.h"
#include "..\pics\fileutil.h"
#include "..\picture\picture.hpp"
#include "\c\Poseidon\lib\math3d.hpp"
#include "\c\Poseidon\lib\colors.hpp"

int consoleMain( int argc, const char *argv[] )
{
	AutoArray<RString> source;
	
	const char *src = NULL;
	const char *dst = NULL;

	char dstTmp[1024];	
	if (argc>=2 && argc<=3)
	{
		src = argv[1];
		if (argc>2) dst = argv[2];
		else
		{
			// autogenerate destination name
			strcpy(dstTmp,src);
			strcpy(GetFileExt(dstTmp),".bump.paa");
			dst = dstTmp;
		}
	}
	else goto Usage;

	if( !dst )
	{
		Usage:
		// input is
		
		fprintf
		(
			stderr,
			"Usage:\n"
			"ProcedTex expression_filename [destination]\n"
		);
		return 1;
	}

	Picture srcPic;
	if (srcPic.Load(src)<0)
	{
		fprintf(stderr,"Cannot load %s\n",src);
		return 1;
	}
	srcPic.ConvertARGB();
	Picture pic;
	pic.CreateOpaque(srcPic.Width(),srcPic.Height());

	fprintf
	(
		stderr,"generating texture %s\n",dst
	);
	int wMask = pic.Width()-1;
	int hMask = pic.Height()-1;
	float scale = 100;
	for (int y=0; y<pic.Height(); y++)
	{
		for (int x=0; x<pic.Width(); x++)
		{
			// check differences against neighbourhgs
			float pix00 = (srcPic.GetPixelARGB(x,y)>>24)&0xff;
			float pix0p = (srcPic.GetPixelARGB(x,(y+1)&hMask)>>24)&0xff;
			float pix0m = (srcPic.GetPixelARGB(x,(y-1)&hMask)>>24)&0xff;
			float pixp0 = (srcPic.GetPixelARGB((x+1)&wMask,y)>>24)&0xff;
			float pixm0 = (srcPic.GetPixelARGB((x-1)&wMask,y)>>24)&0xff;
			// calculate normal direction


			// see "/c/Poseidon/lib/landscape.cpp", function Landscape::GenerateSegment

			float xDelta = pixp0-pixm0;
			float zDelta = pix0p-pix0m;
			Vector3 offX(0,xDelta,scale);
			Vector3 offZ(scale,zDelta,0);
			Vector3Val cp = -offX.CrossProduct(offZ);
			Vector3Val normal = -cp.Normalized();
			// convert normal to RGB representation
			Color pix
			(
				normal.X()*0.5+0.5,
				normal.Y()*0.5+0.5,
				normal.Z()*0.5+0.5
			);

			pic.SetPixel(x,y,PackedColor(pix));
		}
		fprintf(stderr,"%4d of %4d\r",y,pic.Height());
	}
	fprintf(stderr,"\nSaving...\n");

	// save target picture
	int ret = pic.Save(dst);

	return ret;
}
