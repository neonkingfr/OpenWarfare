//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by resource.rc
//
#define IDS_MSG_NOREGISTRY              1
#define IDS_MSG_NOCONFIG                61207
#define IDS_MSG_BADCONFIG               61208
#define IDS_MSG_CONFIGERR               61209
#define IDS_MSG_CONFIGOLDAPP            61210
#define IDS_APPNAME                     61211

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        101
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
