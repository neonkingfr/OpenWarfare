/*!
\file
Merge channels from two input files.
Any combination of rgba/rgba is possible, including swizzling.
Primary purpose is batch operation on _no./_co. pairs.
*/


#include <direct.h>
#include <io.h>
#include <fcntl.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <El/elementpch.hpp>
#include <El/math/math3d.hpp>
#include <Es/Framework/debugLog.hpp>
#include <Es/Common/win.h>
#include <Es/Common/fltopts.hpp>
#include <Es/Files/filenames.hpp>
#include <Es/Files/fileContainer.hpp>
#include <Es/Containers/array.hpp>
#include <Es/Strings/rString.hpp>
#include <Es/Strings/bString.hpp>
#include "..\pics\macros.h"
#include "..\pics\fileutil.h"
#include "..\picture\picture.hpp"
#include "..\pal2pace\resource.h"

#include <Es/Framework/consoleBase.h>

int consoleMain(int argc, const char *argv[])
{
  FILE *file = fopen("TreeCo2Ca.txt", "w");
  if (!file) return 2;

  BString<1024> noMask;
  strcpy(noMask,"*_co.tga");
  _finddata_t fd;
  long f = _findfirst(noMask, &fd);
  if (f >= 0)
  {
    do
    {
      // Get the lowered file name
      BString<1024> srcName;
      strcpy(srcName, fd.name);
      srcName.StrLwr();

      if (fd.attrib&_A_SUBDIR) continue;

      // Skip textures with "shadow" in the name
      if (strstr(srcName, "shadow") != NULL) continue;

      Picture srcPic;
      srcPic.Load(srcName);

      // If the texture is solid it's probably the trunk, leave it
      if (srcPic.IsOpaque()) continue;

      printf("Processing %s ...\n", srcName.cstr());

      // Resize the texture
      srcPic.HalfsizeSRGB();

      // Create the modified name
      BString<1024> dstName;
      strcpy(dstName, srcName);
      const char *s = strstr(dstName, "_co.tga");
      // change o in _co to a in _ca
      dstName[s-dstName+2] = 'a';

      // Save the modified tga with the new name
      srcPic.SaveTGA(dstName);

      // Store the file name change
      fprintf(file, "[\"%s\", \"%s\"],\n", srcName.cstr(), dstName.cstr());

      // Try to find normal map with the same name (it should exists)
      {
        BString<1024> noName;
        strcpy(noName, srcName);
        const char *s = strstr(noName, "_co.tga");
        noName[s-noName+1] = 'n';
        Picture noPic;
        if (noPic.Load(noName) < 0)
        {
          printf("File %s failed to load\n", noName.cstr());
        }
        else
        {
          // Resize the normal texture
          noPic.Halfsize();

          // Create the modified name
          BString<1024> dstNoName;
          strcpy(dstNoName, srcName);
          const char *s = strstr(dstNoName, "_co.tga");
          strcpy(dstNoName+(s-dstNoName), "_non.tga");

          // Save the modified tga with the new name
          noPic.SaveTGA(dstNoName);
        }
      }
    } while (_findnext(f, &fd)==0);
    _findclose(f);

    // Wait for key
    getc(stdin);
    
    fclose(file);
    return 0;
  }
  
  fclose(file);
  return 1;
}