#include <stdlib.h>
#include <stdio.h>

void *mallocSpc( size_t size, long Idtf )
{
	void *ret=malloc(size);
	(void)Idtf;
	if( !ret )
	{
		printf("Out of memory\n");
		exit(1);
	}
	return ret;
}
void freeSpc( void *mem )
{
	free(mem);
}

