// define all veriables required to dynamic linking of pal2pac

UpdateTextureT *UpdateTextureF;
CreateTextureDataT *CreateTextureDataF;
DestroyTextureDataT *DestroyTextureDataF;

void InitPal2Pac()
{
  if (UpdateTextureF) return;
  HINSTANCE lib = ::LoadLibrary("pal2pac.dll");
  if (!lib) return;
  UpdateTextureF = (UpdateTextureT *)::GetProcAddress(lib,"_UpdateTexture@12");
  CreateTextureDataF = (CreateTextureDataT *)::GetProcAddress(lib,"_CreateTextureData@8");
  DestroyTextureDataF = (DestroyTextureDataT *)::GetProcAddress(lib,"_DestroyTextureData@4");
}

