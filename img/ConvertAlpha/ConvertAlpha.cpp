#include <windows.h>
#include <direct.h>
#include <io.h>
#include <fcntl.h>
#include "..\pics\macros.h"
#include "..\pics\fileutil.h"
#include "..\picture\picture.hpp"

// command line interface to dll

static BOOL wasError=TRUE;


static HANDLE hConsoleOutput,hConsoleInput;
	
static void ExitCon()
{
	if( wasError )
	{
		DWORD ret;
		static const char text[]="\n-----------------------\nPress ENTER to continue";
		char buf[80];
		WriteConsole(hConsoleOutput,text,strlen(text),&ret,NULL);
		ReadConsole(hConsoleInput,buf,sizeof(buf),&ret,NULL);
	}
}

void __cdecl LogF( const char *format, ... )
{
	char buf[512];
		
	va_list arglist;
	va_start( arglist, format );

	vsprintf(buf,format,arglist);
	strcat(buf,"\n");
	OutputDebugString(buf);

	#if 0
		// create or append to session log
		static bool notFirst;
		
		FILE *f;
		if( notFirst )  f=fopen("debug.log","at");
		else f=fopen("debug.log","wt"),notFirst=true;
		if( !f ) return;
		vfprintf(f,format,arglist);
		fputc('\n',f);
		fclose(f);
	#endif

	va_end( arglist );

}

void __cdecl FailHook( const char *text )
{
	OutputDebugString(text);
	__asm int 3
}

void ConvertError( const char *D, const char *S )
{
	printf("Error %s->%s\n",S,D);
}

bool AskIfOverwrite( const char *D, const char *S, unsigned dTime, unsigned sTime )
{
	printf("Overwrite %s->%s\n",S,D);
	return true;
}

typedef enum {ComNone,ComCombine} Command;

// vars not used...
static float XBorder=1.0/8,YBorder=1.0/8; // relative border size
// DLL has its own static version

static PathName BankName,WaterName;


int main( int argc, const char *argv[] )
{
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	BOOL bLaunched;
	
	// Lets try a trick to determine if we were 'launched' as a seperate
	// screen, or just running from the command line.
	
	hConsoleOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	hConsoleInput = GetStdHandle(STD_INPUT_HANDLE);
	GetConsoleScreenBufferInfo(hConsoleOutput, &csbi);
	bLaunched = ((csbi.dwCursorPosition.X==0) && (csbi.dwCursorPosition.Y==0));
	if ((csbi.dwSize.X<=0) || (csbi.dwSize.Y <= 0)) bLaunched = FALSE;

	if( bLaunched ) atexit(ExitCon);

	//CoInitialize(NULL); // initialize COM interface
	
	const char *S=NULL,*D=NULL;
	bool reverse=false;
	
	const char **argv0=argv;
	while( argc>1 )
	{
		const char *arg=argv[1];
		if( *arg=='-' || *arg=='/' )
		{
			arg++;
			if( !strcmpi(arg,"rev") ) reverse=true;
			else goto Usage;
		}
		else
		{
			if( !S ) S=arg;
			else if( !D ) D=arg;
			else goto Usage;
		}
		argc--,argv++;
	}
	if( !S )
	{
		Usage:
		printf("Arguments used:\n");
		for( int i=1; argv0[i]; i++ )
		{
			printf("'%s'\n",argv0[i]);
		}
		
		printf("Usage:\tConvertAlpha [options] <source> [<destination>]\n");
		printf("\n");
		printf("Options:\t-rev (reversed conversion)\n");
		exit(1);
	}

	int ret=-1;
	if( !D ) D=S;
	Picture pic;
	if( pic.Load(S)>=0 )
	{
		if( reverse )
		{
			pic.AddBlackToAlpha();
		}
		else
		{
			pic.RemoveBlackFromAlpha();
		}
		pic.Save(D);
	}
	wasError=FALSE;
	//Error:
	if( !wasError ) return 0;
	return 1;
}

