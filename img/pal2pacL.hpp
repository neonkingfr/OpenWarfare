#ifndef _PAL2PACL_HPP
#define _PAL2PACL_HPP

// static library interface

unsigned int FileTime( const char *path );

// convert single file, perform animated texture conversion if neccessary
int Convert( const char *D, const char *S, int size );
int Copy( const char *D, const char *S, int size );

// convert single file (timestamp check)
int ConvertFile( const char *D, const char *S, int size );

// convert single file (always)
int DoConvertFile( const char *D, const char *S, int size );

int CombTexture
(
	const char *D, const char *S1, const char *S2, const char *S3, const char *S4,
	int size, const char *bank, const char *water
);

// global texture info routines
bool LoadTextureInfos();
int GetMaxSize( const char *name );


#endif