#ifndef __MEMSPC
#define __MEMSPC

#ifdef __cplusplus
  extern "C" {
#endif

void freeSpc( void *mem );
void  *mallocSpc( size_t size, long Idtf );

#ifdef __cplusplus
  };
#endif

#endif
