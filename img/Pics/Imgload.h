#ifdef __cplusplus
extern "C" {
#endif

void *IMGLoad( const char *N, int *W, int *H, int *NPlanu );

void *MTPILoad( const char *N, int *W, int *H, int *NP, int *NC, long *RGB, long Mag );
void *TPILoad( const char *N, int *W, int *H, int *NP, int *NC, long *RGB );
int MTPISave( const char *N, int W, int H, int NP, int NC, long *RGB, void *Buf, long Len, long Mag );
int TPISave( const char *N, int W, int H, int NP, int NC, long *RGB, void *Buf, long Len );

void *PANLoad( const char *N, int *W, int *H, int *NP, int *NC, long *RGB );
int PANSave( const char *N, int W, int H, int NP, int NC, long *RGB, void *Buf, long Len );

void *RAWPCCLoad( const char *N, int *W, int *H );
void *PCCLoad( const char *N, int *W, int *H );
void *PAC256Load( const char *N, int *W, int *H, long *RGB ); /* PixelPaket */

int SaveRAWPCC( const char *N, word *Buf, int W, int H );
int SavePCC( const char *N, word *Buf, int W, int H ); /* DC */
int SavePAC256( const char *N, byte *Buf, int W, int H, long *RGB, Flag TGA ); /* PP256 */

void *TGALoad( const char *N, int *W, int *H, int *NPlanu );
int TGASave( const char *N, int W, int H, word *Buf ); /* jen DC */
int TGA256Save( const char *N, int W, int H, void *Buf, long *RGB ); /* PixelPaket, paleta - 256 barev */
int TGALSave( const char *N, int W, int H, unsigned long *Buf ); /* TC s hloubkou Resol */
int TGANSave( const char *N, int W, int H, void *Buf, long *RGB, int NC ); /* PixelPaket, paleta - N barev */

void *KonvNBP2PP( word *Buf, int W, int H, int N ); /* N, BitPlanes->PixelPaket */

void KonvPP2BP( word *Buf, int W, int H ); /* 256, PixelPaket->BitPlanes */
void KonvBP2PP( word *Buf, int W, int H ); /* 256, PixelPaket->BitPlanes */

void KonvTransparent( word *Buf, int W, int H ); /* zmena bile (TruePaint) na 0 */
long KonvTitl( word *To, word *Buf, int W, int H ); /* vzdy opak/kolikrat - vraci delku - shrinkni si sam */

word *TransRadek( word *TBuf, int W, int H ); /* vyhleda radek */

#ifdef __cplusplus
};
#endif

