/**
@file
Default selector for PNG support - no PNG by default.
*/

#include <Es/essencepch.hpp>
#include <Img/Interfaces/iImgFormat.hpp>

static IPictureFormat SFormatNoPNG;

IPictureFormat *GFormatPNG = &SFormatNoPNG;


