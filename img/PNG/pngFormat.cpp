/**
@file
selector for PNG support - PNG support.
Normally this should be used in the exe project
when linking against Picture library, which already contains selector for no PNG.
*/

#include <Es/essencepch.hpp>
#include <Es/Framework/debugLog.hpp>
#include <Es/Types/pointers.hpp>
#include <Img/Interfaces/iImgFormat.hpp>
#include "pngFormat.hpp"
#include <libpng/include/png.h>

/// PNG format support
class PictureFormatPNG: public IPictureFormat
{
  public:
  //@{ ILoadPicture implementation
  virtual int Load(const char *name, int &w, int &h, Pixel *&argbData);
  virtual int Save(const char *name, int w, int h, const Pixel *argbData);
  virtual int LoadRows(const char *name, int &w, AutoArray< SRefArray<Pixel> > &argbRows);
  //@}
};


PNGFormatLoader::PNGFormatLoader(const char *name)
:_f(fopen(name,"rb"))
{
  _png_ptr = NULL;
  _info_ptr = NULL;
  _end_info = NULL;
}

int PNGFormatLoader::LoadHeader()
{
  if (!_f) return -1;
  png_byte header[8];
  int rd = fread(header, 1, sizeof(header), _f);
  if (rd<sizeof(header))
  {
    return -1;
  }
  bool is_png = !png_sig_cmp(header, 0, rd);
  if (!is_png)
  {
    return -1;
  }

  _png_ptr = png_create_read_struct(
    PNG_LIBPNG_VER_STRING, NULL, NULL, NULL
  );
  if (!_png_ptr) return -1;

  _info_ptr = png_create_info_struct(_png_ptr);
  if (!_info_ptr) return -1;

  _end_info = png_create_info_struct(_png_ptr);
  if (!_end_info) return -1;
  return 0;
}


int PNGFormatLoader::LoadDimensions(int &w, int &h)
{
  png_init_io(_png_ptr, _f);

  // we have read all sig. bytes
  png_set_sig_bytes(_png_ptr, 8);

  /*
  int png_transforms = (
    PNG_TRANSFORM_STRIP_16| // convert to 16b to 8b
    PNG_TRANSFORM_PACKING| // expand 1-4b to 8b
    PNG_TRANSFORM_BGR // convert to BGR / ABGR
  );
  png_read_png(png_ptr, info_ptr, png_transforms, NULL);
  */
  png_read_info(_png_ptr, _info_ptr);
  
  png_uint_32 width,height;
  int bit_depth,color_type;
  png_get_IHDR(
    _png_ptr, _info_ptr, &width, &height,
    &bit_depth, &color_type, NULL, NULL, NULL
  );
  

  if (bit_depth < 8)
      png_set_packing(_png_ptr);
  //if (bit_depth == 16) png_set_swap(png_ptr);
  // no low-level support for PNG_TRANSFORM_STRIP_16?
  // we want BGRA byte ordering
  if (color_type == PNG_COLOR_TYPE_RGB)
    png_set_filler(_png_ptr, 0xff, PNG_FILLER_AFTER);
  if (color_type == PNG_COLOR_TYPE_GRAY || color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
    png_set_gray_to_rgb(_png_ptr);
  png_set_bgr(_png_ptr);

  png_read_update_info(_png_ptr, _info_ptr);

  png_get_IHDR(
    _png_ptr, _info_ptr, &width, &height,
    &bit_depth, &color_type, NULL, NULL, NULL
  );
  
  if (color_type!=PNG_COLOR_TYPE_RGB_ALPHA && color_type!=PNG_COLOR_TYPE_RGB)
  {
    return -1;
  }
  if (bit_depth!=8)
  {
    return -1;
  }
  
  h = height;
  w = width;
  
  return 0;
}




PNGFormatLoader::~PNGFormatLoader()
{
  if (_png_ptr)
  {
    png_infop *info = _info_ptr ? &_info_ptr : NULL;
    png_infop *end = _end_info ? &_end_info : NULL;
    png_destroy_read_struct(&_png_ptr,info,end);
  }
  _png_ptr = NULL;
  _info_ptr = NULL;
  _end_info = NULL;
}

int PictureFormatPNG::Load(const char *name, int &w, int &h, Pixel *&argbData)
{
  Assert(argbData==NULL);
  
  PNGFormatLoader loader(name);

  if (loader.LoadHeader()<0)
  {
    return -1;
  }

  if (setjmp(loader.GetJmpbuf()))
  {
    // any error goes here
    return -1;
  }

  if (loader.LoadDimensions(w,h)<0)
  {
    return -1;
  }
  
  argbData = new Pixel[w*h];
  
  SRefArray<png_bytep> row_pointers;
  row_pointers.Realloc(h);

  // set row pointers based on info
  for(int i = 0; i < h; i++)
  {
    row_pointers[i] = (png_bytep)(argbData + i * w);
  }

  loader.ReadImage(row_pointers);
  loader.ReadEnd();
  
  return 0;
}

int PictureFormatPNG::LoadRows(const char *name, int &w, AutoArray< SRefArray<Pixel> > &argbRows)
{
  PNGFormatLoader loader(name);

  if (loader.LoadHeader()<0)
  {
    return -1;
  }

  if (setjmp(loader.GetJmpbuf()))
  {
    // any error goes here
    return -1;
  }

  int h;
  if (loader.LoadDimensions(w,h)<0)
  {
    return -1;
  }

  
  argbRows.Realloc(h);
  argbRows.Resize(h);
  for (int i=0; i<h; i++)
  {
    argbRows[i] = new Pixel[w];
  }
    
  SRefArray<png_bytep> row_pointers = new png_bytep[h];

  // set row pointers based on info
  for(int i = 0; i < h; i++)
  {
    row_pointers[i] = (png_bytep)(argbRows[i].Data());
  }

  loader.ReadImage(row_pointers);
  loader.ReadEnd();
  
  return 0;
}

int PictureFormatPNG::Save(const char *name, int w, int h, const Pixel *argbData)
{
	AutoCloseFILE out = fopen(name,"wb");
	if (!out)
	{
	  return -1;
	}
#if 0
	png_text text[] = {
#define TEXT(k,t) { PNG_TEXT_COMPRESSION_NONE, k, t, strlen(t) }
		TEXT("Title",		"Sample image"),
		TEXT("Author",		getlogin()),
		TEXT("Description",	"A sample image"),
		TEXT("Copyright",	"Public domain"),
/*		TEXT("Creation Time",	png_convert_to_rfc1123(time(0))), */
		TEXT("Software",	argv[0]),
		TEXT("Disclaimer",	""),
	};
#endif

	png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING,
		(png_voidp)NULL, NULL, NULL);
	if (!png_ptr)
	{
	  return -1;
	}

	png_infop info_ptr = png_create_info_struct(png_ptr);
	if (!info_ptr) 
	{
  	png_destroy_write_struct(&png_ptr, NULL);
  	return -1;
	}

	if (setjmp(png_jmpbuf(png_ptr)))
	{
		/* All future errors jump here */
  	png_destroy_write_struct(&png_ptr, &info_ptr);
  	return -1;
	}

	png_init_io(png_ptr, out);
/*	png_set_write_status_fn(png_ptr, NULL);	*/
/*	png_set_filter(png_ptr, 0, ...); */
	//png_set_compression_level(png_ptr, Z_BEST_COMPRESSION);
	png_set_IHDR(png_ptr, info_ptr,
		w,	/* width */
		h,	/* height */
		8,	/* bit depth */
		PNG_COLOR_TYPE_RGB_ALPHA,
		PNG_INTERLACE_NONE,
		PNG_COMPRESSION_TYPE_DEFAULT,
		PNG_FILTER_TYPE_DEFAULT);
/*	png_set_sBIT(png_ptr, info_ptr, 8); */

#if 0
	png_set_text(png_ptr, info_ptr, text, sizeof text / sizeof text[0]);
#endif

#if 0
	png_set_pHYs(png_ptr, info_ptr, 
		1,	/* horiz pixels/unit  */
		1,	/* vert pixels/unit */
		PNG_RESOLUTION_UNKNOWN);
	png_set_oFFs(png_ptr, info_ptr, 
		0,	/* x offset */
		0,	/* y offset */
		PNG_OFFSET_PIXEL);
	png_set_sCAL(png_ptr, info_ptr,
		1,	/* unit */
		1,	/* width in units */
		1);	/* height */
#endif

/*	png_set_rows(png_ptr, info_ptr, imgrows);
	png_write_png(png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, NULL);
*/

  
  //PNG_TRANSFORM_SWAP_ALPHA, PNG_TRANSFORM_SWAP_ENDIAN
  
	png_write_info(png_ptr, info_ptr);

//  png_set_swap_alpha(png_ptr);
  png_set_bgr(png_ptr);
  
  //SRefArray<Pixel> lineBuffer(w);
	for (int y = 0; y < h; y++)
	{
    const Pixel *line = argbData+y*w;	  
    // perform line transformation
//    for (int x=0; x<w; x++)
//    {
//      // ARGB -> ABGR
//      lineBuffer[x] = line[x];
//    }
		png_write_row(png_ptr, (png_bytep)line);
	}
	png_write_end(png_ptr, info_ptr);

	png_destroy_write_struct(&png_ptr, &info_ptr);
	return out.Close();
}

static PictureFormatPNG SFormatPNG;

IPictureFormat *GFormatPNG = &SFormatPNG;
