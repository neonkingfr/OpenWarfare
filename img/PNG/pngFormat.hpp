#ifdef _MSC_VER
#pragma once
#endif

/**
@file
PNG low-level interface access.
*/

#ifndef _IMG_PNG_FORMAT_HPP
#define _IMG_PNG_FORMAT_HPP

#include <libpng/include/png.h>

// close the file when it goes off the scope
struct AutoCloseFILE: private NoCopy
{
  FILE *f;
  
  operator FILE *() const {return f;}
  FILE *operator -> () const {return f;}
  
  AutoCloseFILE(FILE *file=NULL):f(file){}
  ~AutoCloseFILE(){if (f) fclose(f);}
  int Close()
  {
    if (!f) return 0;
    int ret = fclose(f);
    f=NULL;
    return ret;
  }
};

/// allow simple and safe access to low-level interface
class PNGFormatLoader
{
  AutoCloseFILE _f;
  
  png_structp _png_ptr;
  png_infop _info_ptr;
  png_infop _end_info;

  public:
  PNGFormatLoader(const char *name);
  
  /// load and check file header
  int LoadHeader();
  
  /// prepare error handling
  jmp_buf &GetJmpbuf() {return png_jmpbuf(_png_ptr);}

  /// read image size
  int LoadDimensions(int &w, int &h);

  /// read whole image at once
  void ReadImage(png_byte **row_pointers)
  {
    png_read_image(_png_ptr, row_pointers);
  }
  
  void ReadRow(png_byte *row_pointers)
  {
    png_read_row(_png_ptr, row_pointers, NULL);
  }
  /// read the rest of the file
  void ReadEnd()
  {
    png_read_end(_png_ptr, _end_info);
  }
  
  ~PNGFormatLoader();
};

#endif
