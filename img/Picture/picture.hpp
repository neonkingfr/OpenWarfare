#ifndef PICTURE_HPP
#define PICTURE_HPP

#include "..\pics\macros.h"

// TODO: use QStream instead of STL streams

//#include "QStream.hpp"

//#include <fstream>
//#include <strstream>

//#include "textLib.hpp"

#include <El/elementpch.hpp>
#include <El/QStream/qStream.hpp>
//#include <El/QStream/ssCompress.hpp>
#include <Es/Types/pointers.hpp>
#include <Es/Memory/checkMem.hpp>
#include <Es/Containers/array.hpp>
#include <El/ParamArchive/serializeClass.hpp>

// Flag to say if we want to use an LZO compression or not
#define USE_LZO_COMPRESSION 1

enum {MaxPal=256};

#define TRANSPARENT_RGB 0xff00ff
#define RESERVED_RGB 0x00ffff

#define R_EYE 0.299f
#define G_EYE 0.587f
#define B_EYE 0.114f

struct palStat
{
	float count;
	long rgb;
};

TypeIsSimple(palStat);

typedef unsigned long PPixel;

class ParamArchive;

enum TexMipmapFilter
{
  //! normal mipmap super-sampling
  TexMipDefault,                
  //! filter useful for detail textures - all components get closer to 0.5 as mip-maps are generated
  TexMipFadeOut,                
  //! The texture is supposed to be a normal map and we'll normalize it's elements
  TexMipNormalizeNormalMap,
  //! The texture is supposed to be a tiled normal map and we'll normalize it's elements and move more quickly to the surface normal in it's mipmaps
  TexMipNormalizeNormalMapFade,
  /// The texture is supposed to be a normal map, alpha channel indicates opacity
  TexMipNormalizeNormalMapAlpha,
  /// Alpha channel will be used for alpha testing - add noise
  TexMipAlphaNoise,
  /// The texture is supposed to be a normal map and we'll normalize it's elements, noise will be in alpha
  TexMipNormalizeNormalMapNoise,
  /// 
  TexMipAddAlphaNoise,
};

/// DXT compression error metrics should be different for different texture types
enum TexErrorMode
{
  TexErrEye,
  TexErrDistance,
  TexErrDistanceXY
};

enum TexFormat
{
  TexFormDefault, ///< no preference, autoselect
  TexFormARGB1555,
  TexFormARGB4444,
  TexFormARGB8888,
  TexFormAI88,
  TexFormDXT1,
  TexFormDXT2,TexFormDXT3,
  TexFormDXT4,TexFormDXT5,
  TexFormP8,
};

/// channel swizzle - any channel may be filled with any other, with 1 or with 1-other
enum TexSwizzle
{
  TSAlpha,
  TSRed,
  TSGreen,
  TSBlue,
  TSInvAlpha,
  TSInvRed,
  TSInvGreen,
  TSInvBlue,
  TSOne,
};

enum TexAlpha
{
  TADefault,
  TAForceAlpha,
  TAForceMasked,
};

struct TextureHints: public SerializeClass
{
	RStringB _name;

	bool _enableDXT;
	bool _enableDynRange;
	//bool _fadeOutMipmaps;

	int _limitSize;

  TexMipmapFilter _mipmapFilter;
  /// required texture format
  TexFormat _format;
  TexErrorMode _errorMode;
  /// swizzling (one usage is to store some of SRGB in A of DXT5)
  TexSwizzle _channelSwizzle[4]; // ARGB channels swizzle
  /// is the swizzling real, or virtual only? (This currently seems to be always set to true)
  bool _virtualSwizzle;
  
  /// when true, textures with uniform color are reduced to smallest possible size allowed by the format
  bool _autoreduce;

  //! When true, dithering will be used for color reduction
  bool _dithering;

	bool EnableDXT() const {return _enableDXT;}
	const char *GetName() const {return _name;}

	LSError Serialize(ParamArchive &ar);

	TextureHints();
};

TypeIsMovable(TextureHints);

//! Picture manipulation
class Picture
{
	private:

	bool _noAlphaDither; // disable dithering
	bool _noColorDither;

	int _w,_h; /* rozmery obrazku v pixelech */
	mutable PPixel _rgbPal[MaxPal]; /* 0x00RRGGBB */
	int _lenPal;
	mutable PPixel _averageColor;
	mutable PPixel _maxColor; // max. color used during normalization - default is white (no normalization)

	mutable byte *_picData; // palette representation
	mutable PPixel *_argb; // rgb representation
  TexFormat _sourceFormat; ///< format of the source file
  RString _procSource; //!< when the texture is created by CreateProcedural, source

	mutable bool _alphaWithBlack; // black is mixed into all alpha pixels - premult

	mutable AutoArray<long> _prefer; // preferred colors
	mutable bool reserved; // transparent is used instead of reserved
	mutable byte *_nearestCache;
	
	protected:
	void Destruct();
	void ConstructCopy( const Picture &src );
	void ConstructEmpty();
	
	public:
	Picture(){ConstructEmpty();}
	Picture( const Picture &src ){ConstructCopy(src);}
	~Picture(){Destruct();}
	void operator = ( const Picture &src ){Destruct();ConstructCopy(src);}
	void Delete(){Destruct();}
	
	void Create( int w, int h );
	void CreateOpaque( int w, int h );
  int FilterProcedural(
    const char *expression,
    void (*progress)(int y, int h, void *context),
    void *progressContext
  );
  int CreateProcedural(
    int w, int h, const char *expression,
    void (*progress)(int y, int h, void *context),
    void *progressContext
  );
  const char *GetProcSource() const
  {
    return _procSource;
  }
  RString GetDebugName() const;

	int LoadPAADesc( QIStream &f, int level );


	void DisableDithering()
	{
		_noColorDither = true;
		_noAlphaDither = true;
	}
	int LoadTaggs( QIStream &f, TexSwizzle *swizzle=NULL);
	int LoadJPG( const char *src );
	//int LoadGIF( const char *src );
	int LoadTGA( const char *src );
	// Gravon format (compressed 565) processing
	int LoadPCC( const char *src );
	int LoadPNG( const char *src );
	int LoadDDS( const char *src, int level=0 );
	int LoadPAC( QIStream &f, int level);
	int LoadPAA( QIStream &f, int level);
	int Load( QIStream &f, int level, bool isPaa );
	int LoadTransparent();

  //! Function loads specified image. Returns -1 in case the loading failed
	int Load( const char *S, int level=0 ); // autodetect format

  /// return source format - if known.
  /**
  May return TexFormDefault is there is no source or source format is uknown.
  */
  TexFormat GetSourceFormat() const
  {
    return _sourceFormat;
  }
  void ResetSourceFormat(TexFormat form=TexFormARGB8888)
  {
    _sourceFormat = form;
  }
  /// check how much memory would such texture use in the memory
  size_t GetVRAMSize() const;
  //! check default hints based on given file name and current picture content
	void GetHints(TextureHints &hints, const char *S);
  //! you can provide explicit hints if you want
	int Save( const char *S, const TextureHints *explicitHints=NULL, RString *err=NULL);
  //! you can provide explicit hints if you want
	int Save( QOStream &out, const TextureHints &hints, bool isPaa);
  /// apply hints as if file is saved and loaded
  void ApplyHints(const TextureHints &hints);

  /// compare pictures using current error metrics
  double Difference(const Picture &with) const;
  /// compare pictures, return human eye error metrics
  double DifferenceRGB(const Picture &with) const;
  /// compare pictures, return distance error metrics
  double DifferenceXYZ(const Picture &with) const;
  
  void ChannelSwizzle( const TexSwizzle *channelSwizzle);

	int GetPixelARGBRaw( int x, int y ) const {return _argb[y*_w+x];}
	int GetPixelARGB( int x, int y ) const {return TransformByMax(_argb[y*_w+x]);}

	int GetPixelRaw( int x, int y ) const
  {
    // if possible use RGB representation
    if( _argb )
    {
      return _argb[y*_w+x];
    }
    else
    {
      return _rgbPal[_picData[y*_w+x]];
    }
  }
	int GetPixel( int x, int y ) const
  {
    return TransformByMax(GetPixelRaw(x,y));
  }
	void SetPixel( int x, int y, PPixel pixel ) const;
	int operator () ( int x, int y ) const {return GetPixel(x,y);}
	unsigned int GetAverageColorRaw() const; // return packed RGB color
	unsigned int GetAverageColorRawSRGB() const; // return packed RGB color
	unsigned int GetMinColorRaw() const; // return packed RGB color
	unsigned int GetMaxColorRaw() const; // return packed RGB color


	int SaveOffsets( QOStream &f, int *offsets, int nOffsets );
	int SaveTaggs(QOStream &f, const TextureHints &hints, TexAlpha texAlpha = TADefault);
	int SavePalette( QOStream &f );
	int SaveNulPalette( QOStream &f );

	int SaveData( QOStream &f, bool lzw );
	int SaveData4444( QOStream &f, const TextureHints &hints );
	int SaveData1555( QOStream &f, const TextureHints &hints );
	int SaveData88( QOStream &f );

	int SaveDataDXTOpaque( QOStream &f );
	int SaveDataDXTAlpha( QOStream &f, bool implicit );

	int SavePNG( const char *dst ); // save single mipmap
	int SaveTGA( const char *dst ); // save single mipmap
	int SaveDDS( const char *dst ); /// DX dds file saved

	int SaveMipmaps( QOStream &out, const TextureHints &hints ); // save all mipmaps - destructive
	int SaveMipmapsP8( QOStream &out, const TextureHints &hints ); // save all mipmaps - destructive
	int SaveMipmaps1555( QOStream &out, const TextureHints &hints ); // save all mipmaps - destructive
	int SaveMipmaps4444( QOStream &out, const TextureHints &hints ); // save all mipmaps - destructive
	int SaveMipmaps88( QOStream &out, const TextureHints &hints ); // save all mipmaps - destructive

	int SaveMipmapsDXTOpaque( QOStream &out, const TextureHints &hints );
	int SaveMipmapsDXTAlpha( QOStream &out, const TextureHints &hints );

	int Compare( const Picture &with ) const;

	void CopyARGB( void *dest ) const
	{
		ConvertARGB();
		memcpy(dest,_argb,_w*_h*sizeof(PPixel));
	}
	void ConvertARGB() const;

	void SortPal();

	int DoublesizeX();
	int DoublesizeY();
	
  /// discard anything outside given area
	void Crop( int width, int height );
  /// resize&resample to given size
	void Resize( int width, int height );
	bool IsPowerOf2() const;
	
	int ResizeFilter( const Picture &src, int xFactorLog, int yFactorLog );
  /// downsize the picture, assume colors are in linear space
	bool Halfsize();
  /// downsize the picture, assume colors are in space SRGB
	bool HalfsizeSRGB();
  /// downsize the picture, assume colors are in linear space and alpha channel gives opacity
	bool HalfsizeAlpha();
	
	/// change size of the picture, assuming it is uniform color one
	bool ChangeSizeUniformColor(int w, int h);
	
  //! Downsizes the picture and uses the filter to the newly created picture
  /*!
    \param hints Type of filter to run
    \return True if the creation succeeded, false elsewhere
  */
	bool MipmapFilter(int level, const TextureHints &hints);
  //! Fade out filter
	bool FilterFadeOut(float factor);
  //! Normalization of normal map filter
	bool FilterNormalizeNormalMap(float factor);

  /// resize mipmaps, perform no filtering
	bool MipmapHalfsize(int skipLevels, int minDimension, const TextureHints &hints);

  //! Adding noise into the alpha channel
	bool FilterAlphaNoise(int level, float orig=128.0f/255, float noise=127.0f/255);

  //! adding noise into the alpha channel ( finalA = ( sourceA + noise ) * 0.5 * maskA )
  bool FilterAddAlphaNoise(int level);

	// max RGB including current max, setting
	int MaxRGBA(int &maxR,int &maxG, int &maxB, int &maxA);
	//! use full dynamic range for RGB components
	void MaximizeDynamicRange();
	//! use default dynamic range (maxcolor = 1,1,1)
	void DefaultDynamicRange();
	
	// check if dynamic range compression has been applied
	bool IsDefaultDynamicRange() const
	{
	  return (_maxColor&0xffffff)==0xffffff;
	}
	float MaxDynamicRangeCompression() const;
	

	void CompressDynamicRange(bool compress)
  {
    if (compress) MaximizeDynamicRange();
    else DefaultDynamicRange();
  }

	int HalfsizeX();
	int HalfsizeY();
	
	int Doublesize();

	int XBlend( Picture &with ); // horizontal blending
	int YBlend( Picture &with ); // vertical blending

	// use combination of X/Y blending to get corner blending
	
	void XBank( const Picture &with ); // create bank - horizontal
	void YBank( const Picture &with ); // - vertical

	int Bilinear( const Picture &w1, const Picture &w2, const Picture &w3 );
	int Blend( const Picture &src, float dstFactor, float srcFactor );
	void ModulateAlpha( float factor );

	int Width() const {return _w;}
	int Height() const {return _h;}
	int W() const {return _w;}
	int H() const {return _h;}
	bool IsRGB() const {return _lenPal<=0;}

	bool Valid() const {return _picData!=NULL || _argb!=NULL;}

	// mark some colors as preferred
	void Prefer( const Picture &src );

	bool IsGray() const; //!< check if the picture is gray
	bool IsMasked() const; //!< check if the picture is 1bit alpha format
	bool IsOpaque() const; //!< check if the picture alpha is always opaque
  bool IsUniformColor() const; //!< check if the picture contains only one color (all pixels are the same)
	bool IsPalTransparent() const; // check if the picture alpha is always opaque

	void SetAlphaWithBlack( bool alphaWithBlack ) {_alphaWithBlack=alphaWithBlack;}
	// conditional - tests _alphaWithBlack

	bool IsBlackInAlpha() const;

	void RemoveBlackFromAlpha() const;
	void AddBlackToAlpha();

	PPixel GetAverageColor() const;
	PPixel GetMaxColor() const;
	PPixel GetMinColor() const;
	PPixel GetAverageColorCalculated() const;

	protected:
	byte ExactNearestColor( long rgb, int limit=MaxPal ) const;
	byte NearestColor( long rgb ) const;
	void TouchPalette() const;
	void HideReserved() const;
	void ShowReserved() const;

	void DeleteARGB() const;
	void ConvertPal() const;


	//! min RGBA from raw data
	int MinRGBARaw(int &maxR,int &maxG, int &maxB, int &maxA) const;
	//! max RGBA from raw data
	int MaxRGBARaw(int &maxR,int &maxG, int &maxB, int &maxA) const;
	//! perform transformation by current max color
	PPixel TransformByMax(PPixel color) const
	{
		unsigned a = (color>>24)&0xff;
		unsigned r = (color>>16)&0xff;
		unsigned g = (color>>8)&0xff;
		unsigned b = (color>>0)&0xff;
		unsigned ma = (_maxColor>>24)&0xff;
		unsigned mr = (_maxColor>>16)&0xff;
		unsigned mg = (_maxColor>>8)&0xff;
		unsigned mb = (_maxColor>>0)&0xff;
		unsigned na = (a*ma)/255;
		unsigned nr = (r*mr)/255;
		unsigned ng = (g*mg)/255;
		unsigned nb = (b*mb)/255;
		return (na<<24)|(nr<<16)|(ng<<8)|nb;
	}
	void CalculateAverageColor() const;
	void CalculateAverageColorSRGB() const;

	bool Histogram( AutoArray<palStat> &histogram ) const;

	void Popularity();
	void OcTree();
	void MedianCut();
};

#include <El/ParamFile/paramFileDecl.hpp>

//! after external config is loaded, callback function must be called
bool PictureConfigLoaded(const ParamEntryVal &cfg);

/// after external config is unloaded, callback function must be called
void PictureConfigUnloaded();

enum LoadPictureConfigRetVal
{
	LoadPictureConfigOK,
	LoadPictureConfigNoRegistry,
	LoadPictureConfigNoFile,
	LoadPictureConfigBadFile,
	LoadPictureConfigOldExe,
};
//! default implementation - Load picture config from location defined in registry
LoadPictureConfigRetVal LoadPictureConfig(RString &ret);
/// unload config when it is no longer needed
void UnloadPictureConfig();
//! find hints for given texture
const TextureHints &FindHints(const char *name);

#endif
