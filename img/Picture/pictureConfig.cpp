#include <Es/essencepch.hpp>
#include <Es/Common/win.h>
#include <Es/Memory/checkMem.hpp>
#include <El/ParamFile/paramFile.hpp>
#include "picture.hpp"

#define ConfigApp "Software\\BIStudio\\TextureConvert"

static RString GetConfigPath(RString &reg)
{
	reg = "HKEY_LOCAL_MACHINE/" ConfigApp;
	HKEY key;
	if
	(
		!::RegOpenKeyEx
		(
			HKEY_LOCAL_MACHINE, ConfigApp,
			0, KEY_READ, &key
		) == ERROR_SUCCESS
	) return RString();

	RString ret;
	DWORD type = REG_SZ;
	char buffer[1024];
	DWORD size = sizeof(buffer);
	HRESULT hr = ::RegQueryValueEx(key, "configPath", 0, &type, (BYTE *)buffer, &size);
	if (SUCCEEDED(hr))
	{
		ret = buffer;
	}
	::RegCloseKey(key);
	return ret;
}

#if USE_LZO_COMPRESSION
const int AppVersion = 8;
#else
const int AppVersion = 7;
#endif

LoadPictureConfigRetVal LoadPictureConfig(RString &ret)
{
	ParamFile file;
	RString cfgPath = GetConfigPath(ret);
	if (cfgPath.GetLength()<=0 )
	{
		return LoadPictureConfigNoRegistry;
	}
	// special value - run without a config file
	if (cfgPath=="<none>")
	{
	  return LoadPictureConfigOK;
	}
	ret = cfgPath;
	file.Parse(cfgPath);
	if (file.GetEntryCount()<=0) return LoadPictureConfigNoFile;
	int versionNeeded = file>>"convertVersion";
	if (PictureConfigLoaded(file))
	{
		if (versionNeeded>AppVersion) return LoadPictureConfigOldExe;
		return LoadPictureConfigOK;
	}
	return LoadPictureConfigBadFile;
}

void UnloadPictureConfig()
{
  PictureConfigUnloaded();
  //StringBank.Clear();
}