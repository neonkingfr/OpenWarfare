#include <Es/essencepch.hpp>
#include <El/elementpch.hpp>
#include <stdarg.h>
#include <windows.h>
#include "..\pics\macros.h"
#include "..\pics\fileutil.h"
#include "..\pics\imgload.h"
#include "picture.hpp"
#include "lzcompr.hpp"
#include <math.h>
#include <float.h>

#include <Img/Interfaces/iImgFormat.hpp>
#include <Es/Memory/checkMem.hpp>
#include <Es/Common/fltOpts.hpp>
#include <Es/Containers/staticarray.hpp>
#include <El/ParamFile/paramFile.hpp>
#include <El/ParamFile/classDbParamFile.hpp>
#include <El/ParamArchive/paramArchiveDb.hpp>
#include <El/Enum/Enumnames.hpp>
#include <El/Evaluator/express.hpp>
#include <El/PerlinNoise/perlinNoise.hpp>
#include <El/Color/colors.hpp>
#include <El/Common/perfProf.hpp>

#include <ijl.h>

/// libpng support
#include <libpng/include/png.h>

#if USE_LZO_COMPRESSION
/// LZO compression
#include <El/LZO/lzo/lzo1x.h>
#endif

void Picture::TouchPalette() const
{
  if( _nearestCache ) delete[] _nearestCache,_nearestCache=NULL;
}

void Picture::Destruct()
{
  if( _picData ) delete[] _picData,_picData=NULL;
  if( _argb ) delete[] _argb,_argb=NULL;
  if( _nearestCache ) delete[] _nearestCache,_nearestCache=NULL;
  _prefer.Clear();
  _averageColor = 0;
  _maxColor = 0xffffffff;
  _sourceFormat = TexFormDefault;
  _procSource = "";
}

void Picture::ConstructCopy( const Picture &src )
{
  ConstructEmpty();
  _w=src._w;
  _h=src._h;
  memcpy(_rgbPal,src._rgbPal,sizeof(_rgbPal));
  _lenPal=src._lenPal;
  _nearestCache=NULL;
  _procSource = src._procSource;

  if( src._picData )
  {
    _picData=new byte[src._w*src._h];
    if( !_picData ) return;
    memcpy(_picData,src._picData,src._w*src._h);
  }
  else _picData=NULL;

  if( src._argb )
  {
    _argb=new PPixel[src._w*src._h];
    if( !_argb ) return;
    memcpy(_argb,src._argb,src._w*src._h*sizeof(long));
  }
  else _argb=NULL;

  _alphaWithBlack=src._alphaWithBlack;
  _prefer=src._prefer;
  _noAlphaDither=src._noAlphaDither;
  _noColorDither=src._noColorDither;
  _averageColor = src._averageColor;
  _maxColor = src._maxColor;
  _sourceFormat = src._sourceFormat;
}

void Picture::ConstructEmpty()
{
  _picData=NULL;
  _argb=NULL;
  _nearestCache=NULL;
  _prefer.Clear();
  _alphaWithBlack=false;
  _noAlphaDither=false,_noColorDither=false;
  _lenPal=0;
  _maxColor = 0xffffffff;
  _sourceFormat = TexFormDefault;
  _procSource = "";
}


void Picture::Create( int w, int h )
{
  _lenPal=0;
  _w=w,_h=h;
    
  _argb=new PPixel[_w*_h];
  // default context: gray, half transparent
  memset(_argb,0x80,_w*_h*sizeof(*_argb));
  _alphaWithBlack=false;
  _noAlphaDither=false,_noColorDither=false;
  _maxColor = 0xffffffff;
  _procSource = "";
  _sourceFormat = TexFormDefault;
}

void Picture::CreateOpaque( int w, int h )
{
  _lenPal=0;
  _w=w,_h=h;
    
  _argb=new PPixel[_w*_h];
  // default context: gray, half transparent
  PPixel *d = _argb;
  for (int i=_w*_h; --i>=0; )
  {
    *d++ = 0xff808080;
  }
  _alphaWithBlack=false;
  _noAlphaDither=false,_noColorDither=false;
  _maxColor = 0xffffffff;
  _procSource = "";
  _sourceFormat = TexFormDefault;
}

#define TYPES_PICTURE(XX, Category) \
  XX("PICTURE",GamePicture, CreateGameDataPicture, "Picture", "Picture", "Picture.", Category) \
  XX("COLOR",GameColor, CreateGameDataColor, "Color", "Color", "Color.", Category)

TYPES_PICTURE(DECLARE_TYPE, "Picture")

typedef Picture *GamePictureType;
typedef Color GameColorType;

#include <Es/Memory/normalNew.hpp>

template <class DataType>
struct GameValTypeTraits
{
  static const char *GetName() {return "???";}
  static RString GetText(DataType val) {return "";}
  static GameType GetType() {return GameType(0);}
};


template <class DataType, class Traits=GameValTypeTraits<DataType> >
class GameValType: public GameData
{
  typedef GameData base;

  DataType _value;

  public:
  GameValType():_value(0){}
  GameValType( DataType value ):_value(value){}
  ~GameValType(){}

  const GameType &GetType() const {return Traits::GetType();}
  DataType GetObject() const {return _value;}

  RString GetText() const
  {
    return Traits::GetText(_value);
  }
  bool IsEqualTo(const GameData *data) const
  {
    DataType val1 = GetObject();
    DataType val2 = static_cast<const GameValType *>(data)->GetObject();
    return val1==val2;
  }
  const char *GetTypeName() const {return Traits::GetName();}
  GameData *Clone() const {return new GameValType(*this);}

  LSError Serialize(ParamArchive &ar)
  {
    // specialization needed for serialization
    Fail("Specialization needed.");
    return LSError(-1);
  }

  USE_FAST_ALLOCATOR
};

#include <Es/Memory/debugNew.hpp>

template<>
struct GameValTypeTraits<GamePictureType>
{
  static const char *GetName() {return "picture";}
  static RString GetText(GamePictureType val)
  {
    if (val==NULL) return "<NULL-picture>";
    return Format("%dx%d",val->W(),val->H());
  }
  static const GameType &GetType() {return GamePicture;}
};

typedef GameValType<GamePictureType> GameDataPicture;

template<>
struct GameValTypeTraits<GameColorType>
{
  static const char *GetName() {return "color";}
  static RString GetText(GameColorType val)
  {
    return Format("(%g,%g,%g;%g)",val.R(),val.G(),val.B(),val.A());
  }
  static const GameType &GetType() {return GameColor;}
};

typedef GameValType<GameColorType> GameDataColor;

DEFINE_FAST_ALLOCATOR(GameDataPicture)

GameData *CreateGameDataPicture(ParamArchive *ar) {return new GameDataPicture();}

DEFINE_FAST_ALLOCATOR(GameDataColor)

GameData *CreateGameDataColor(ParamArchive *ar) {return new GameDataColor();}

class GameValuePicExt: public GameValue
{
  public:

  GameValuePicExt( Picture *value ) {_data=new GameDataPicture(value);}
  GameValuePicExt( Color value ) {_data=new GameDataColor(value);}
};

static Picture *GetPicture( GameValuePar oper )
{
  if (oper.GetNil()) return NULL;
  if (oper.GetType()==GamePicture)
  {
    return static_cast<GameDataPicture *>(oper.GetData())->GetObject();
  }
  return NULL;
}

static Color GetColor( GameValuePar oper )
{
  if (oper.GetNil()) return HWhite;
  if (oper.GetType()==GameColor)
  {
    return static_cast<GameDataColor *>(oper.GetData())->GetObject();
  }
  if (oper.GetType()==GameScalar)
  {
    float val = oper;
    return Color(val,val,val,val);
  }
  return HWhite;
}

static GameValue ColorRed( const GameState *state, GameValuePar oper1)
{
  Color col = GetColor(oper1);
  return col.R();
}
static GameValue ColorGreen( const GameState *state, GameValuePar oper1)
{
  Color col = GetColor(oper1);
  return col.G();
}
static GameValue ColorBlue( const GameState *state, GameValuePar oper1)
{
  Color col = GetColor(oper1);
  return col.B();
}
static GameValue ColorAlpha( const GameState *state, GameValuePar oper1)
{
  Color col = GetColor(oper1);
  return col.A();
}

static GameValue ColorConstruct( const GameState *state, GameValuePar oper1)
{
  const GameArrayType &array = oper1;
  if(array.Size()!=4)
  {
    state->SetError(EvalDim,array.Size(),4);
    return GameValue();
  }
  for (int i=0; i<4; i++)
  {
    if (array[i].GetType() != GameScalar)
    {
      state->TypeError(GameScalar,array[i].GetType());
      return GameValue();
    }
  }
  Color color(array[0],array[1],array[2],array[3]);
  return GameValuePicExt(color);
}

//static GameValue ColorMin( const GameState *state, GameValuePar oper1)
//{
//  // oper1 is GameDataPicture
//  Picture *pic = GetPicture(oper1);
//  if (!pic) return GameValue();
//  return pic->MaxRGBA();
//}

static GameValue ColorAverage( const GameState *state, GameValuePar oper1)
{
  // oper1 is GameDataPicture
  Picture *pic = GetPicture(oper1);
  if (!pic) return GameValue();
  PackedColor pixel = PackedColor(pic->GetAverageColor());
  Color col(pixel);
  return GameValuePicExt(col);
}

static GameValue GetPicPixel( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  // oper1 is GameDataPicture
  Picture *pic = GetPicture(oper1);
  if (!pic) return GameValue();
  // oper2 is array
  const GameArrayType &array = oper2;
  if(array.Size()!=2)
  {
    state->SetError(EvalDim,array.Size(),2);
    return GameValue();
  }
  if (array[0].GetType() != GameScalar)
  {
    state->TypeError(GameScalar,array[0].GetType());
    return GameValue();
  }
  if (array[1].GetType() != GameScalar)
  {
    state->TypeError(GameScalar,array[1].GetType());
    return GameValue();
  }
  float u = array[0];
  float v = array[1];
  int x = toInt((u+1)*0.5*(pic->W()-1));
  int y = toInt((v+1)*0.5*(pic->H()-1));
  saturate(x,0,pic->W()-1);
  saturate(y,0,pic->H()-1);
  PackedColor pixel = PackedColor(pic->GetPixel(x,y));
  Color col(pixel);
  return GameValuePicExt(col);
}

static GameValue ColorAdd( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  Color op1 = GetColor(oper1);
  Color op2 = GetColor(oper2);
  return GameValuePicExt(op1+op2);
}
static GameValue ColorSub( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  Color op1 = GetColor(oper1);
  Color op2 = GetColor(oper2);
  return GameValuePicExt(op1-op2);
}
static GameValue ColorMul( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  Color op1 = GetColor(oper1);
  Color op2 = GetColor(oper2);
  return GameValuePicExt(op1*op2);
}
static GameValue ColorDiv( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  Color op1 = GetColor(oper1);
  Color op2 = GetColor(oper2);
  if (op2.R()<1e-10) op2.SetR(1e-10);
  if (op2.G()<1e-10) op2.SetG(1e-10);
  if (op2.B()<1e-10) op2.SetB(1e-10);
  if (op2.A()<1e-10) op2.SetA(1e-10);
  return GameValuePicExt(op1/op2);
}


static GameValue ColorSwizzle( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  Color op1 = GetColor(oper1);
  GameStringType op2 = oper2;
  const char *swiz = op2;
  if (!*swiz)
  {
    return GameValuePicExt(op1);
  }
  float src[4]={op1.R(),op1.G(),op1.B(),op1.A()};
  float ret[4];
  for (int i=0; i<4; i++)
  {
    char s = tolower(swiz[0]);
    switch (s)
    {
      case 'r': case 'x': ret[i] = src[0]; break;
      case 'g': case 'y': ret[i] = src[1]; break;
      case 'b': case 'z': ret[i] = src[2]; break;
      case 'a': case 'w': ret[i] = src[3]; break;
      case '0': ret[i] = 0; break;
      default:
      case '1': ret[i] = 1; break;
    }

    // repeat the last component
    if (swiz[0] && swiz[1]) swiz++;
  }
  return GameValuePicExt(Color(ret[0],ret[1],ret[2],ret[3]));
}

//static GameNular PicNular[]= {
//};

#define FUNC_DOCS ,"","","","","","",""
#define OPER_DOCS ,"", "", "", "", "", "", "", ""
    
static GameFunction PicUnary[]=
{
  GameFunction( GameScalar, "red", ColorRed, GameColor FUNC_DOCS),
  GameFunction( GameScalar, "green", ColorGreen, GameColor FUNC_DOCS),
  GameFunction( GameScalar, "blue", ColorBlue, GameColor FUNC_DOCS),
  GameFunction( GameScalar, "alpha", ColorAlpha, GameColor FUNC_DOCS),
  GameFunction( GameColor, "color", ColorConstruct, GameArray FUNC_DOCS),
//  GameFunction( GameColor, "minColor", ColorMin, GamePicture FUNC_DOCS),
//  GameFunction( GameColor, "maxColor", ColorMax, GamePicture FUNC_DOCS),
  GameFunction( GameColor, "averageColor", ColorAverage, GamePicture FUNC_DOCS)
};

static GameOperator PicBinary[]=
{
  GameOperator( GameColor, "pixel", mocnina, GetPicPixel, GamePicture, GameArray OPER_DOCS),
  // basic operations with colors
  GameOperator( GameColor, "+", soucet, ColorAdd, GameColor, GameColor OPER_DOCS),
  GameOperator( GameColor, "-", soucet, ColorSub, GameColor, GameColor OPER_DOCS),
  GameOperator( GameColor, "*", soucin, ColorMul, GameColor, GameColor OPER_DOCS),
  GameOperator( GameColor, "*", soucin, ColorMul, GameScalar, GameColor OPER_DOCS),
  GameOperator( GameColor, "*", soucin, ColorMul, GameColor, GameScalar OPER_DOCS),
  GameOperator( GameColor, "/", soucin, ColorDiv, GameColor, GameScalar OPER_DOCS),
  GameOperator( GameColor, "/", soucin, ColorDiv, GameColor, GameColor OPER_DOCS),
  GameOperator( GameColor, "swizzle", mocnina, ColorSwizzle, GameColor, GameString OPER_DOCS),

};

TYPES_PICTURE(DEFINE_TYPE, "Picture")

//INIT_MODULE(GameStateExt, 2)

void RegisterPicFunctions()
{
  GameState &state = GGameState; // helper to make macro works
  TYPES_PICTURE(REGISTER_TYPE, "Picture")
  
//  for( int i=0; i<sizeof(PicNular)/sizeof(*PicNular); i++ )
//  {
//    GGameState.NewNularOp(PicNular[i]);
//  }
  for( int i=0; i<sizeof(PicUnary)/sizeof(*PicUnary); i++ )
  {
    GGameState.NewFunction(PicUnary[i]);
  }
  for( int i=0; i<sizeof(PicBinary)/sizeof(*PicBinary); i++ )
  {
    GGameState.NewOperator(PicBinary[i]);
  }
}

static int EvaluateColor(const char *name)
{
  float a = GGameState.VarGet(name);
  int ia = toLargeInt(a*255);
  saturate(ia,0,255);
  return ia;
}

static unsigned int EvaluateColorVector(const char *name)
{
  Color a = GetColor(GGameState.VarGet(name));
  a.SaturateMinMax();
  return PackedColor(a);
}

static RString EvaluateLine(const char *src, EvalError &error)
{
  // line beginning with variable name expected
  GGameState.Execute(src);

  error = GGameState.GetLastError();
  if (error!=EvalOK)
  {
    return GGameState.GetLastErrorText(); 
  }
  return RString();
}


struct EvaluateContext
{
  Color avgColor;
  Color maxColor;
  Color minColor;
  
  EvaluateContext(const Picture *src)
  {
    avgColor = PackedColor(src->GetAverageColor());
    maxColor = PackedColor(src->GetMaxColor());
    minColor = PackedColor(src->GetMinColor());
  }
};

static int EvaluateScript(
  RString script, float u, float v, int x, int y, Picture *src, EvaluateContext &ctx
)
{
  GGameState.Reset();
  GGameState.VarSet("u",u,true);
  GGameState.VarSet("v",v,true);
  GGameState.VarSet("ux",float(x),true);
  GGameState.VarSet("vy",float(y),true);
  if (src)
  {
    GGameState.VarSet("avg",GameValuePicExt(ctx.avgColor),true);
    GGameState.VarSet("min",GameValuePicExt(ctx.minColor),true);
    GGameState.VarSet("max",GameValuePicExt(ctx.maxColor),true);
    
    GGameState.VarSet("src",GameValuePicExt(src),true);

    //int 1 = toInt((du)*0.5*(pic->W()-1));
    //int 1 = toInt((dv)*0.5*(pic->H()-1));
    //int 2/(pic->W()-1) = toInt((du));

    GGameState.VarSet("du",2.0f/(src->W()-1),true);
    GGameState.VarSet("dv",2.0f/(src->H()-1),true);
  }
  EvalError error;
  RString ret = EvaluateLine(script,error);
  if (ret.GetLength()>0)
  {
    fprintf(stderr,"Error %s in expression %s\n",(const char *)ret,(const char *)script);
    return 1;
  }
  return 0;
}

RString Picture::GetDebugName() const
{
  return "Picture";
}

int Picture::FilterProcedural(
  const char *expression,
  void (*progress)(int y, int h, void *context),
  void *progressContext
)
{
  DefaultDynamicRange();
  CalculateAverageColor();
  // create an old copy of the picture
  Picture oldPic(*this);
  // perform testing evaluation to detect any error
  EvaluateContext ctx(this);

  if (EvaluateScript(expression,1,1,_w,_h,&oldPic,ctx))
  {
    return -1;
  }

  //CreateOpaque(_w,_h);

  for (int y=0; y<_h; y++)
  {
    float v = (float)y/(_h-1);
    for (int x=0; x<_w; x++)
    {
      // parse source expression using basic expression evaluator
      // source expression is command line argument
      float u = (float)x/(_w-1);
      EvaluateScript(expression,(u-0.5)*2,(v-0.5)*2,x,y,&oldPic,ctx);
      unsigned int pix = EvaluateColorVector("c");
      SetPixel(x,y,pix);
    }
    progress(y,_h,progressContext);
  }
  _maxColor = 0xffffffff;
  CalculateAverageColor();
  return 0;
}

int Picture::CreateProcedural(
  int w, int h, const char *expression,
  void (*progress)(int y, int h, void *context),
  void *progressContext
)
{
  // perform testing evaluation to detect any error
  EvaluateContext ctx(this);
  if (EvaluateScript(expression,0,0,w,h,NULL,ctx))
  {
    return -1;
  }

  CreateOpaque(w,h);

  for (int y=0; y<h; y++)
  {
    float v = (float)y/(h-1);
    for (int x=0; x<w; x++)
    {
      // parse source expression using basic expression evaluator
      // source expression is command line argument
      float u = (float)x/(w-1);
      EvaluateScript(expression,(u-0.5)*2,(v-0.5)*2,x,y,NULL,ctx);
      int ia = EvaluateColor("a");
      int ir = EvaluateColor("r");
      int ig = EvaluateColor("g");
      int ib = EvaluateColor("b");
      PPixel pix = (ia<<24)|(ir<<16)|(ig<<8)|ib;
      SetPixel(x,y,pix);
    }
    progress(y,h,progressContext);
  }
  _procSource = expression;
  _sourceFormat = TexFormARGB8888;
  CalculateAverageColor();
  return 0;
}

/// {{ JPG support

typedef IJLERR IJL_STDCALL ijlInitFT ( JPEG_CORE_PROPERTIES* jcprops );
typedef IJLERR IJL_STDCALL ijlReadFT ( JPEG_CORE_PROPERTIES* jcprops, IJLIOTYPE iotype );
typedef IJLERR IJL_STDCALL ijlFreeFT ( JPEG_CORE_PROPERTIES* jcprops );

#define MIN_MIP_SIZE 4

class JPGLibrary
{
  HMODULE _libHandle;
  ijlInitFT *_init;
  ijlReadFT *_read;
  ijlFreeFT *_free;
  int _refCount;

  void Load();
  void Unload();

  public:
  JPGLibrary()
  {
    _init = NULL;
    _read = NULL;
    _free = NULL;
    _libHandle = NULL;
    _refCount = 0;
  }
  ~JPGLibrary()
  {
    Unload();
  }

  void AddRef()
  {
    if (_refCount++==0) Load();
  }
  void Release()
  {
    if (--_refCount==0) Unload();
  }

  IJLERR Init(JPEG_CORE_PROPERTIES* jcprops)
  {
    AddRef();
    if (!_init) return IJL_PROG_NOT_SUPPORTED;
    return _init(jcprops);
  }
  IJLERR Read( JPEG_CORE_PROPERTIES* jcprops, IJLIOTYPE iotype )
  {
    if (!_read) return IJL_PROG_NOT_SUPPORTED;
    return _read(jcprops,iotype);
  }
  IJLERR Free( JPEG_CORE_PROPERTIES* jcprops )
  {
    if (!_read) return IJL_PROG_NOT_SUPPORTED;
    IJLERR ret = _free(jcprops);
    Release();
    return ret;
  }
};

void JPGLibrary::Load()
{
  HMODULE _libHandle = LoadLibrary("bin\\ijl15.dll");
  // if not found, try PATH search
  if (!_libHandle) _libHandle = LoadLibrary("ijl15.dll");
  if (!_libHandle)
  {
    LogF("No jpg import library found");
    return;
  }

  _init = (ijlInitFT *)GetProcAddress(_libHandle,"ijlInit");
  _read = (ijlReadFT *)GetProcAddress(_libHandle,"ijlRead");
  _free = (ijlFreeFT *)GetProcAddress(_libHandle,"ijlFree");

  if (!_init || !_read || !_free)
  {
    LogF("Required JPG functions not found.");
    Unload();
    return;
  }
  LogF("JPGLibrary loaded");
}

void JPGLibrary::Unload()
{
  if (_libHandle)
  {
    LogF("JPGLibrary unloaded");
    FreeLibrary(_libHandle);
    _libHandle = NULL;
    _init = (ijlInitFT *)NULL;
    _read = (ijlReadFT *)NULL;
    _free = (ijlFreeFT *)NULL;
  }
}

static JPGLibrary GJPGLibrary;


/// }} JPG support

static bool IsPowerOfTwo(int x)
{
  while (x>0)
  {
    if (x&1) return x==1;
    x >>= 1;
  }
  return true;
}


int Picture::LoadJPG( const char *S )
{
  Destruct();

  
  JPEG_CORE_PROPERTIES prop;
  memset(&prop,0,sizeof(prop));

  int ret = -1;
  IJLERR err;
  err = GJPGLibrary.Init(&prop);
  if (err!=IJL_OK) return -1;

  prop.JPGBytes = NULL;
  prop.JPGSizeBytes = 0;
  prop.JPGFile = S;

  err = GJPGLibrary.Read(&prop,IJL_JFILE_READPARAMS);
  if (err==IJL_OK && IsPowerOfTwo(prop.JPGWidth) && IsPowerOfTwo(prop.JPGHeight))
  {
    DeleteARGB();
    _w  = prop.JPGWidth;
    _h  = prop.JPGHeight;
    _argb=new PPixel[_w*_h];
    if (!_argb) return -1;
    _alphaWithBlack=false;
    _noAlphaDither=false,_noColorDither=false;
    _maxColor = 0xffffffff;
    _procSource = "";


    switch(prop.JPGChannels)
    {
    case 1:
      prop.JPGColor    = IJL_G;
      prop.DIBChannels = 3;
      prop.DIBColor    = IJL_RGB;
      //Fail("Unsupported");
      break;

    case 3:
      prop.JPGColor    = IJL_YCBCR;
      prop.DIBChannels = 3;
      prop.DIBColor    = IJL_RGB;
      break;

    case 4:
      prop.JPGColor    = IJL_YCBCRA_FPX;
      prop.DIBChannels = 4;
      prop.DIBColor    = IJL_RGBA_FPX;
      break;

    default:
      Fail("Unsupported");
      break;
    }

    prop.DIBWidth    = prop.JPGWidth;
    prop.DIBHeight   = prop.JPGHeight;
    prop.DIBPadBytes = 0;

    AUTO_STATIC_ARRAY(unsigned char,temp,256*256*4);
    temp.Resize(prop.DIBWidth*prop.DIBHeight*prop.DIBChannels);

    prop.DIBBytes = temp.Data();

    err = GJPGLibrary.Read(&prop,IJL_JFILE_READWHOLEIMAGE);
    
    if (err==IJL_OK)
    {
      ret = 0;
      // convert from 888 to 8888
      for (int i=0; i<prop.DIBWidth*prop.DIBHeight; i++)
      {
        int r,g,b,a;
        if (prop.DIBChannels==3)
        {
          r = temp[i*3+0];
          g = temp[i*3+1];
          b = temp[i*3+2];
          a = 0xff;
        }
        else if (prop.DIBChannels==1)
        {
          r = temp[i];
          g = r;
          b = r;
          a = 0xff;
        }
        else // prop.DIBChannels==4)
        {
          r = temp[i*4+0];
          g = temp[i*4+1];
          b = temp[i*4+2];
          a = temp[i*4+3];
        }
        int argb = (a<<24)|(r<<16)|(g<<8)|b;
        _argb[i] = argb;
      }

      // if not power of two, resample it


    }
  }

  GJPGLibrary.Free(&prop);
  return ret;
}

#if 0
// GIF no longer supported - we always want to work in 8888
int Picture::LoadGIF( const char *S )
{
  Destruct();
  /*byte Permut[256];*/
  IMAGE HObr;
  byte *B;
  int i,w,h;
  if( Read_Gif_Image(&HObr,S)<0 ) return -1;
  if( !HObr.Colour_Map ) return -1;
  if( HObr.Colour_Map_Size>256 ) return -1;
  if( HObr.Colour_Map_Size<=0 ) return -1;
  _lenPal=HObr.Colour_Map_Size;
  _w=HObr.iwidth;
  _h=HObr.iheight;
  for( i=0; i<_lenPal; i++ )
  {
    IMAGE_COLOUR *c=HObr.Colour_Map+i;
    _rgbPal[i]=((long)c->Red<<16)|((long)c->Green<<8)|((long)c->Blue)|0xff000000;
  }
  for( i=_lenPal; i<256; i++ ) _rgbPal[i]=0;
  if( _lenPal>256 ) return -1;
  //_lenPal=256;
  _picData=new byte[_w*_h];
  B=_picData;
  if( !B ) return -1;
  int maxIndex=-1;
  for( h=0; h<_h; h++ )
  {
    unsigned char *d=HObr.map_lines[h];
    if( !d ) return -1;
    for( w=0; w<_w; w++ )
    {
      int index=d[w];
      *B++=(byte)index;
      if( index>maxIndex ) maxIndex=index;
    }
  }
  _lenPal=maxIndex+1;
  _noAlphaDither=false;
  _alphaWithBlack=true;
  _noColorDither=false;
  _maxColor = 0xffffffff;
  _procSource = "";
  TouchPalette();
  return 0;
}
#endif

/*
inline int toInt( float f )
{
  int temp;
  _asm
  {
    fld f;
    fistp temp;
  }
  return temp;
}
*/

#define GetA(a) ( ((a)>>24)&0xff )
#define GetR(a) ( ((a)>>16)&0xff )
#define GetG(a) ( ((a)>> 8)&0xff )
#define GetB(a) ( ((a)>> 0)&0xff )

#define MakeRGB(r,g,b) ( ((r)<<16)|((g)<<8)|((b)) )
#define MakeARGB(a,r,g,b) ( ((a)<<24)|((r)<<16)|((g)<<8)|((b)) )


#if 1

class DistributionMatrix
{
  private:
  Temp<float> _matrix;
  int _h,_w;

  protected:
  float &Matrix( int x, int y ){return _matrix[y*_w+x];}
  float Matrix( int x, int y ) const {return _matrix[y*_w+x];}

  public:
  void Normalize();
  DistributionMatrix( int w, int h, ... );

  int W() const {return _w;}
  int H() const {return _h;}
  float operator () ( int x, int y ) const {return Matrix(x,y);}
};


DistributionMatrix::DistributionMatrix( int w, int h, ... )
{
  _w=w;
  _h=h;
  _matrix.Realloc(_w*_h);
  va_list va;
  va_start(va,h);
  int x,y;
  for( y=0; y<_h; y++ ) for( x=0; x<_w; x++ )
  {
    Matrix(x,y)=va_arg(va,int);
  }
  va_end(va);
  Normalize();
}

void DistributionMatrix::Normalize()
{
  float sum=0;
  int x,y;
  for( y=0; y<_h; y++ ) for( x=0; x<_w; x++ ) sum+=Matrix(x,y);
  float invSum=1/sum;
  for( y=0; y<_h; y++ ) for( x=0; x<_w; x++ ) Matrix(x,y)*=invSum;
}

static const DistributionMatrix FSMatrix( 3,2, 0,0,7, 3,5,1 );
static const DistributionMatrix SierraMatrix( 3,2, 0,0,2, 1,1,0 );
static const DistributionMatrix JarvisMatrix( 5,3, 0,0,0,7,5, 3,5,7,5,3, 1,3,5,3,1 );
static const DistributionMatrix StuckiMatrix( 5,3, 0,0,0,8,4, 2,4,8,4,2, 1,2,4,2,1 );


class MatrixDither
{
  protected:
  const DistributionMatrix &_matrix;
  #define MAX_H 8 // max 8 line distribution
  Temp<float> _error[MAX_H]; // previous line
  int _w,_h;

  public:
  MatrixDither( int w, const DistributionMatrix &matrix );
  float GetError( int x ); // use previous lines information
  void SetError( int x, float val ); // distribute between pixels
  void EndOfLine(); // move to next line
};

MatrixDither::MatrixDither( int w, const DistributionMatrix &matrix )
:_w(w),_h(matrix.H()),
_matrix(matrix)
{
  //int mWHalf=_matrix.W()/2;
  for( int y=0; y<_h; y++ )
  {
    _error[y].Realloc(_w);
    for( int x=0; x<_w; x++ ) _error[y][x]=0;
  }
}

float MatrixDither::GetError( int x )
{
  return _error[0][x];
}
void MatrixDither::EndOfLine()
{
  int x,y;
  //int mWHalf=_matrix.W()/2;
  for( y=0; y<_h-1; y++ )
  {
    for( x=0; x<_w; x++ ) _error[y][x]=_error[y+1][x];
  }
  for( x=0; x<_w; x++ ) _error[_h-1][x]=0;
}

void MatrixDither::SetError( int x, float val )
{
  int mW=_matrix.W();
  int mH=_matrix.H();
  int mWHalf=mW/2; // 2 for w=5 ...
  for( int xx=0; xx<mW; xx++ )
  {
    int xDest=x+xx-mWHalf;
    if( xDest>=0 && xDest<_w )
    {
      for( int yy=0; yy<mH; yy++ )
      {
        _error[yy][xDest]+=val*_matrix(xx,yy);
      }
    }
  }
}

class FSDither: public MatrixDither
{
  public:
  FSDither( int w ):MatrixDither(w,FSMatrix){}
};
class SierraDither: public MatrixDither
{
  public:
  SierraDither( int w ):MatrixDither(w,SierraMatrix){}
};
class JarvisDither: public MatrixDither
{
  public:
  JarvisDither( int w ):MatrixDither(w,JarvisMatrix){}
};
class StuckiDither: public MatrixDither
{
  public:
  StuckiDither( int w ):MatrixDither(w,StuckiMatrix){}
};


/**/

class ChromaKeyDither
{
  private:
  //FSDither _dither;
  JarvisDither _ditherA;
  bool _alphaWithBlack;

  bool _doAlpha;

  public:
  ChromaKeyDither( int w, bool alphaWithBlack, bool doAlpha=true )
  :_ditherA(w),
  _alphaWithBlack(alphaWithBlack),
  _doAlpha(doAlpha)
  {
  }
  int GetColor( long argb, int x );
  void EndOfLine();
};

static long CorrectRGB( long argb )
{
  // there is alpha of black mixed into the color
  // i.e. argb=(1-alpha)*color+alpha*black
  // color=argb/(1-alpha)
  int a=((argb>>24)&0xff);
  if( a==0 ) return argb;
  if( a==0xff ) return 0xff000000; // we cannot do anything
  float coef=1/(1-a/255.0);
  int r=toInt( ((argb>>16)&0xff)*coef);
  int g=toInt( ((argb>>8)&0xff)*coef);
  int b=toInt( ((argb>>0)&0xff)*coef);
  if( r>0xff ) r=0xff;
  if( g>0xff ) g=0xff;
  if( b>0xff ) b=0xff;
  return (a<<24)|(r<<16)|(g<<8)|(b<<0);
}

int ChromaKeyDither::GetColor( long argb, int x )
{
  float a=GetA(argb);
  if( _doAlpha ) a+=_ditherA.GetError(x);
  if( a>=0x80 )
  {
    // use transparent pixel
    _ditherA.SetError(x,a-0xff);
    return TRANSPARENT_RGB;
  }
  else
  {
    _ditherA.SetError(x,a);
    if( _alphaWithBlack ) argb=CorrectRGB(argb);
    return argb&0xffffff;
  }
}

void ChromaKeyDither::EndOfLine()
{
  if( _doAlpha ) _ditherA.EndOfLine();
}

#endif

// note: if float calculations appear to slow, it might be faster to use precalculated arrays
static __forceinline unsigned ConvertPrec(unsigned x, int srcBits, int tgtBits)
{
  const float srcMax = (1<<srcBits)-1;
  const float tgtMax = (1<<tgtBits)-1;
  return toInt(x*(tgtMax/srcMax));
}

//! extract component, when extending, replicate lowest bit
static __forceinline unsigned ExtractComponent(unsigned x, unsigned srcShift, unsigned srcMask, unsigned dstShift)
{
  // assume we are always converting to 8b
  Assert((srcMask+1)<<dstShift==0x100);
  // extract source data
  unsigned xc=((x>>srcShift)&srcMask);
  // perform a conversion
  return ConvertPrec(xc,8-dstShift,8);
}

static __forceinline unsigned Extend(unsigned x, unsigned shift)
{
  return ConvertPrec(x,8-shift,8);
}

inline PPixel Convert4444To8888( int dd16 )
{
  // replicate lower pixel?
  int a=ExtractComponent(dd16,12,0xf,4);
  int r=ExtractComponent(dd16,8,0xf,4);
  int g=ExtractComponent(dd16,4,0xf,4);
  int b=ExtractComponent(dd16,0,0xf,4);
  PPixel dd32=MakeARGB(a,r,g,b);
  return dd32;
}

inline PPixel Convert1555To8888( int dd16 )
{
  int a=ExtractComponent(dd16,15,1,7);
  int r=ExtractComponent(dd16,10,0x1f,3);
  int g=ExtractComponent(dd16,5,0x1f,3);
  int b=ExtractComponent(dd16,0,0x1f,3);
  PPixel dd32=MakeARGB(a,r,g,b);
  return dd32;
}

inline int Convert565To8888( int dd16 )
{
  int r=ExtractComponent(dd16,11,0x1f,3);
  int g=ExtractComponent(dd16,5,0x3f,2);
  int b=ExtractComponent(dd16,0,0x1f,3);
  PPixel dd32=MakeARGB(0xff,r,g,b);
  return dd32;
}



inline int Convert8888To4444( PPixel dd32 )
{
  int a=GetA(dd32),r=GetR(dd32),g=GetG(dd32),b=GetB(dd32);
  int ia=ConvertPrec(a,8,4);
  int ir=ConvertPrec(r,8,4);
  int ig=ConvertPrec(g,8,4);
  int ib=ConvertPrec(b,8,4);
  int dd16=(ia<<12)|(ir<<8)|(ig<<4)|(ib<<0);
  return dd16;
}

inline int Convert8888To1555( PPixel dd32 )
{
  int a=GetA(dd32),r=GetR(dd32),g=GetG(dd32),b=GetB(dd32);
  int ia=ConvertPrec(a,8,1);
  int ir=ConvertPrec(r,8,5);
  int ig=ConvertPrec(g,8,5);
  int ib=ConvertPrec(b,8,5);
  int dd16=(ia<<15)|(ir<<10)|(ig<<5)|ib;
  return dd16;
}

inline int Convert8888To565( PPixel dd32 )
{
  int r=GetR(dd32),g=GetG(dd32),b=GetB(dd32);
  int ir=ConvertPrec(r,8,5);
  int ig=ConvertPrec(g,8,6);
  int ib=ConvertPrec(b,8,5);
  int dd16=(ir<<11)|(ig<<5)|ib;
  return dd16;
}

static void Convert4444To8888( PPixel *d32, const word *d16, int nPix )
{
  while( --nPix>=0 )
  {
    int dd16=*d16++;
    int dd32=Convert4444To8888(dd16);
    *d32++=dd32;
  }
}
static void Convert1555To8888( PPixel *d32, const word *d16, int nPix )
{
  while( --nPix>=0 )
  {
    int dd16=*d16++;
    int dd32=Convert1555To8888(dd16);
    *d32++=dd32;
  }
}

static void Convert88To8888( PPixel *d32, const word *d16, int nPix )
{
  while( --nPix>=0 )
  {
    int a=(*d16>>8)&0xff;
    int i=(*d16>>0)&0xff;
    *d32++=MakeARGB(a,i,i,i);
    d16++;
  }
}

#if 1
#define DitherMode FSDither
//#define DitherMode SierraDither
//#define DitherMode JarvisDither
//#define DitherMode StuckiDither

static void Convert8888To4444( word *d16, const PPixel *d32, int w, int h )
{
  // convert with dithering
  //JarvisDither aDither(w);
  DitherMode rDither(w),gDither(w),bDither(w);
  for( int y=0; y<h; y++ )
  {
    for( int x=0; x<w; x++ )
    {
      int ia=GetA(*d32);
      //float a=GetA(*d32);
      float r=GetR(*d32);
      float g=GetG(*d32);
      float b=GetB(*d32);
      // do rounding to nearest
      //a+=aDither.GetError(x);
      r+=rDither.GetError(x),g+=gDither.GetError(x),b+=bDither.GetError(x);
      //int ia=(toInt(a)+8)>>4;
      int ir=(toInt(r)+16)>>4;
      int ig=(toInt(g)+16)>>4;
      int ib=(toInt(b)+16)>>4;
      //Assert( ia>=0 && ia<=16 );
      if( ir<0 ) ir=0;if( ir>15 ) ir=15;
      if( ig<0 ) ig=0;if( ig>15 ) ig=15;
      if( ib<0 ) ib=0;if( ib>15 ) ib=15;
      //aDither.SetError(x,a-ia*16);
      rDither.SetError(x,r-Extend(ir,4));
      gDither.SetError(x,g-Extend(ig,4));
      bDither.SetError(x,b-Extend(ib,4));
      //if( ia<0 ) ia=0;if( ia>15 ) ia=15;
      ia >>= 4;
      *d16++=(ia<<12)|(ir<<8)|(ig<<4)|(ib<<0);
      d32++;
    }
    //aDither.EndOfLine();
    rDither.EndOfLine(),gDither.EndOfLine(),bDither.EndOfLine();
  }
}


static void Convert8888To1555( word *d16, const PPixel *d32, int w, int h )
{
  // convert with dithering
  //JarvisDither aDither(w);
  DitherMode rDither(w),gDither(w),bDither(w);
  for( int y=0; y<h; y++ )
  {
    for( int x=0; x<w; x++ )
    {
      int ia=GetA(*d32)>>7;
      //float a=GetA(*d32);
      float r=GetR(*d32);
      float g=GetG(*d32);
      float b=GetB(*d32);
      // do rounding to nearest
      //a+=aDither.GetError(x);
      r+=rDither.GetError(x),g+=gDither.GetError(x),b+=bDither.GetError(x);
      //int ia=(toInt(a)+8)>>4;
      int ir=(toInt(r)+4)>>3;
      int ig=(toInt(g)+4)>>3;
      int ib=(toInt(b)+4)>>3;
      //Assert( ia>=0 && ia<=16 );
      saturate(ir,0,31);
      saturate(ig,0,31);
      saturate(ib,0,31);
      //aDither.SetError(x,a-ia*16);
      rDither.SetError(x,r-Extend(ir,3));
      gDither.SetError(x,g-Extend(ig,3));
      bDither.SetError(x,b-Extend(ib,3));
      //if( ia<0 ) ia=0;if( ia>15 ) ia=15;
      if( ir<0 ) ir=0;if( ir>31 ) ir=31;
      if( ig<0 ) ig=0;if( ig>31 ) ig=31;
      if( ib<0 ) ib=0;if( ib>31 ) ib=31;
      *d16++=(ia<<15)|(ir<<10)|(ig<<5)|(ib<<0);

      d32++;
    }
    //aDither.EndOfLine();
    rDither.EndOfLine(),gDither.EndOfLine(),bDither.EndOfLine();
  }
}

#endif

static void Convert8888To4444NoDither( word *d16, const PPixel *d32, int w, int h )
{
  // no dithering
  for( int i=0; i<w*h; i++ )
  {
    int dd32=*d32++;
    int dd16=Convert8888To4444(dd32);
    *d16++=dd16;
  }
}
static void Convert8888To1555NoDither( word *d16, const PPixel *d32, int w, int h )
{
  // no dithering
  for( int i=0; i<w*h; i++ )
  {
    int dd32=*d32++;
    int dd16=Convert8888To1555(dd32);
    *d16++=dd16;
  }
}

static void Convert8888To88( word *d16, const PPixel *d32, int w, int h )
{
  // no dithering
  for( int y=0; y<h; y++ )
  {
    for( int x=0; x<w; x++ )
    {
      int a=GetA(*d32),r=GetR(*d32),g=GetG(*d32),b=GetB(*d32);
      // do rounding to nearest
      // brightness is weighted average of r,g,b
      int i=toInt( (R_EYE*r+G_EYE*g+B_EYE*b)*(1/(R_EYE+G_EYE+B_EYE)) );
      if( i<0 ) i=0;
      if( i>255 ) i=255;
      *d16++=(a<<8)|(i<<0);
      d32++;
    }
  }
}


struct DXTBlock64
{
  // see "Compressed Texture Formats" in DX Docs
  word c0,c1; // color data
  word tex0,tex1; // texel data
};

struct DXTBlock64AlphaExplicit
{
  word a[4];
};

struct DXTBlock64AlphaImplicit
{
  byte a0,a1; // 8b alpha
  byte tex[6];
};

TypeIsSimple(DXTBlock64)

struct DXTBlock128
{
  union
  {
    DXTBlock64AlphaExplicit alphaExp;
    DXTBlock64AlphaImplicit alphaImp;
  };
  DXTBlock64 color;
};

TypeIsSimple(DXTBlock128)

// note: int. division very slow - precalc. table instead

static struct Div3Init
{
  char _table[512];

  Div3Init()
  {
    for (int i=256; i<512; i++)
    {
      _table[i] = (i-256+1)/3;
    }
    for (int i=0; i<256; i++)
    {
      _table[i] = -((256-i+1)/3);
    }
  }
  int operator () (int x) const
  {
    x += 256;
    Assert(x>=0 && x<512);
    return _table[x];
  }
} Div3;

static void Create8Alphas8(int *alpha, int a0, int a1)
{
  float inv7 = 1.0/7;
  alpha[0] = a0;
  alpha[1] = a1;

  alpha[2] = toIntFloor((6 * a0 + 1 * a1 + 3) *inv7);    // bit code 010
  alpha[3] = toIntFloor((5 * a0 + 2 * a1 + 3) *inv7);    // bit code 011
  alpha[4] = toIntFloor((4 * a0 + 3 * a1 + 3) *inv7);    // bit code 100
  alpha[5] = toIntFloor((3 * a0 + 4 * a1 + 3) *inv7);    // bit code 101
  alpha[6] = toIntFloor((2 * a0 + 5 * a1 + 3) *inv7);    // bit code 110
  alpha[7] = toIntFloor((1 * a0 + 6 * a1 + 3) *inv7);    // bit code 111  
}

static void Create8Alphas6(int *alpha, int a0, int a1)
{
  float inv5 = 1.0/5;
  alpha[0] = a0;
  alpha[1] = a1;

  alpha[2] = toIntFloor((4 * a0 + 1 * a1 + 2) *inv5);    // bit code 010
  alpha[3] = toIntFloor((3 * a0 + 2 * a1 + 2) *inv5);    // bit code 011
  alpha[4] = toIntFloor((2 * a0 + 3 * a1 + 2) *inv5);    // bit code 100
  alpha[5] = toIntFloor((1 * a0 + 4 * a1 + 2) *inv5);    // bit code 101
  alpha[6] = 0;    // bit code 110
  alpha[7] = 0xff;    // bit code 111  
}


/**
Step may be other that 1 in case blocks are interleaved with some other data
*/
static PPixel *LoadDXT1( const DXTBlock64 *blocks, int w, int h, int stride=1 )
{
  PPixel *pixels = new PPixel[w*h];
  if (!pixels) return NULL;

  // scan 64b packets
  const DXTBlock64 *s = blocks;

  PPixel *line = pixels;
  // convert all levels
  for (int y=0; y<h; y+=4)
  {
    PPixel *base = line;
    for (int x=0; x<w; x+=4, base+=4)
    {
      PPixel color[4];
      // decompress current block
      PPixel c0 = Convert565To8888(s->c0);
      PPixel c1 = Convert565To8888(s->c1);
      if (s->c0>s->c1)
      {
        // 4 color block
        int rc0 = GetR(c0);
        int gc0 = GetG(c0);
        int bc0 = GetB(c0);
        int rc1 = GetR(c1);
        int gc1 = GetG(c1);
        int bc1 = GetB(c1);
        // c0-c1 is positive
        int rd3 = Div3(rc1-rc0);
        int gd3 = Div3(gc1-gc0);
        int bd3 = Div3(bc1-bc0);
        //color[2] = c0 + (c1-c0)/3; // (2 * c0 + c1) / 3;
        //color[3] = c1 - (c1-c0)/3; // (c0 + 2 * c1) / 3;
        // convert 565 to 1555
        // verify c2, c3 components in range
        int rc2 = rc0+rd3;
        int gc2 = gc0+gd3;
        int bc2 = bc0+bd3;

        int rc3 = rc1-rd3;
        int gc3 = gc1-gd3;
        int bc3 = bc1-bd3;
        PPixel c2 = MakeRGB(rc2,gc2,bc2);
        PPixel c3 = MakeRGB(rc3,gc3,bc3);
        color[0]= c0|0xff000000;
        color[1]= c1|0xff000000;
        color[2]= c2|0xff000000;
        color[3]= c3|0xff000000;
      }
      else if (s->c0==s->c1)
      {
        // mono-color transparent block
        PPixel c0 = Convert565To8888(s->c0);
        color[0]= c0|0xff000000;
        color[1]= c0|0xff000000;
        color[2]= c0|0xff000000;
        color[3] = c0&0x00ffffff;
      }
      else // c1<c0
      {
        // 3 color + transparency block
        int rc0 = GetR(c0);
        int gc0 = GetG(c0);
        int bc0 = GetB(c0);
        int rc1 = GetR(c1);
        int gc1 = GetG(c1);
        int bc1 = GetB(c1);

        PPixel c2 = MakeRGB((rc0+rc1)>>1,(gc0+gc1)>>1,(bc0+bc1)>>1);
        
        color[0]= c0|0xff000000;
        color[1]= c1|0xff000000;
        color[2]= c2|0xff000000;
        color[3] = c2&0x00ffffff;
      }

      // get dibits
      int w0 = s->tex0;
      int w1 = s->tex1;

      PPixel *tbase = base;
      *tbase++ = color[(w0    )&3]; // 1:0 Texel[0][0] 
      *tbase++ = color[(w0>> 2)&3]; // 3:2 Texel[0][1] 
      *tbase++ = color[(w0>> 4)&3]; // 5:4 Texel[0][2] 
      *tbase   = color[(w0>> 6)&3]; // 7:6 Texel[0][3] 
      tbase += w-3;

      *tbase++ = color[(w0>> 8)&3]; // 9:8 Texel[1][0] 
      *tbase++ = color[(w0>>10)&3]; // 11:10 Texel[1][1] 
      *tbase++ = color[(w0>>12)&3]; // 13:12 Texel[1][2] 
      *tbase   = color[(w0>>14)&3]; // 15:14 Texel[1][3] 
      tbase += w-3;

      *tbase++ = color[(w1    )&3]; // 1:0 Texel[2][0] 
      *tbase++ = color[(w1>> 2)&3]; // 3:2 Texel[2][1] 
      *tbase++ = color[(w1>> 4)&3]; // 5:4 Texel[2][2] 
      *tbase   = color[(w1>> 6)&3]; // 7:6 Texel[2][3] 
      tbase += w-3;

      *tbase++ = color[(w1>> 8)&3]; // 9:8 Texel[3][0] 
      *tbase++ = color[(w1>>10)&3]; // 11:10 Texel[3][1] 
      *tbase++ = color[(w1>>12)&3]; // 13:12 Texel[3][2] 
      *tbase   = color[(w1>>14)&3]; // 15:14 Texel[3][3] 

      s += stride; // move to next block
    }
    line += 4*w; // skip all 4 decompressed lines
  }
  return pixels;
}

inline void SetA(DWORD &pixel, int a)
{
  pixel = (pixel&0xffffff)|(a<<24);
}

static void LoadDXT23( DWORD *pixels, const DXTBlock128 *blocks, int w, int h )
{
  // scan 128b packets
  const DXTBlock128 *s = blocks;

  PPixel *line = pixels;
  // convert all levels
  for (int y=0; y<h; y+=4)
  {
    PPixel *base = line;
    for (int x=0; x<w; x+=4, base+=4)
    {
      const DXTBlock64AlphaExplicit &block = s->alphaExp;

      PPixel *tbase = base;
      SetA(*tbase++, Extend((block.a[0]>>0)&0xf,4)); // Texel[0][0] 
      SetA(*tbase++, Extend((block.a[0]>>4)&0xf,4)); // Texel[0][1] 
      SetA(*tbase++, Extend((block.a[0]>>8)&0xf,4)); // Texel[0][2] 
      SetA(*tbase,   Extend((block.a[0]>>12)&0xf,4)); // Texel[0][3] 
      tbase += w-3;

      SetA(*tbase++, Extend((block.a[1]>>0)&0xf,4)); // Texel[1][0] 
      SetA(*tbase++, Extend((block.a[1]>>4)&0xf,4)); // Texel[1][1] 
      SetA(*tbase++, Extend((block.a[1]>>8)&0xf,4)); // Texel[1][2] 
      SetA(*tbase,   Extend((block.a[1]>>12)&0xf,4)); // Texel[1][3] 
      tbase += w-3;

      SetA(*tbase++, Extend((block.a[2]>>0)&0xf,4)); // Texel[2][0] 
      SetA(*tbase++, Extend((block.a[2]>>4)&0xf,4)); // Texel[2][1] 
      SetA(*tbase++, Extend((block.a[2]>>8)&0xf,4)); // Texel[2][2] 
      SetA(*tbase,   Extend((block.a[2]>>12)&0xf,4)); // Texel[2][3] 
      tbase += w-3;

      SetA(*tbase++, Extend((block.a[3]>>0)&0xf,4)); // Texel[3][0] 
      SetA(*tbase++, Extend((block.a[3]>>4)&0xf,4)); // Texel[3][1] 
      SetA(*tbase++, Extend((block.a[3]>>8)&0xf,4)); // Texel[3][2] 
      SetA(*tbase,   Extend((block.a[3]>>12)&0xf,4)); // Texel[3][3] 

      s++;
    }
    line += 4*w; // skip all 4 decompressed lines
  }
}
static void LoadDXT45( DWORD *pixels, DXTBlock128 *blocks, int w, int h )
{
  // scan 128b packets
  const DXTBlock128 *s = blocks;

  PPixel *line = pixels;
  // convert all levels
  for (int y=0; y<h; y+=4)
  {
    PPixel *base = line;
    for (int x=0; x<w; x+=4, base+=4)
    {
      const DXTBlock64AlphaImplicit &block = s->alphaImp;
      PPixel *tbase = base;

      int alpha[8];
      if (block.a0>block.a1)
      {
        Create8Alphas8(alpha,block.a0,block.a1);
      }
      else
      {
        Create8Alphas6(alpha,block.a0,block.a1);
      }

      const DWORD mask = 7; // bits = 00 00 01 11

      DWORD bits = *( (DWORD*) & ( block.tex[0] ));

      SetA(*tbase++, alpha[bits & mask]);
      bits >>= 3;
      SetA(*tbase++, alpha[bits & mask]);
      bits >>= 3;
      SetA(*tbase++, alpha[bits & mask]);
      bits >>= 3;
      SetA(*tbase,   alpha[bits & mask]);
      bits >>= 3;
      tbase += w-3;

      SetA(*tbase++, alpha[bits & mask]);
      bits >>= 3;
      SetA(*tbase++, alpha[bits & mask]);
      bits >>= 3;
      SetA(*tbase++, alpha[bits & mask]);
      bits >>= 3;
      SetA(*tbase,   alpha[bits & mask]);
      tbase += w-3;

      // now for last two rows:

      bits = *( (DWORD*) & ( block.tex[3] ));   // last 3 bytes

      SetA(*tbase++, alpha[bits & mask]);
      bits >>= 3;
      SetA(*tbase++, alpha[bits & mask]);
      bits >>= 3;
      SetA(*tbase++, alpha[bits & mask]);
      bits >>= 3;
      SetA(*tbase,   alpha[bits & mask]);
      bits >>= 3;
      tbase += w-3;

      SetA(*tbase++, alpha[bits & mask]);
      bits >>= 3;
      SetA(*tbase++, alpha[bits & mask]);
      bits >>= 3;
      SetA(*tbase++, alpha[bits & mask]);
      bits >>= 3;
      SetA(*tbase,   alpha[bits & mask]);

      s++;
    }
    line += 4*w; // skip all 4 decompressed lines
  }
}

static void fputiw( word W, QOStream &f )
{
  f.put((byte)W);
  f.put((byte)(W>>8));
}
static void fputi24( long W, QOStream &f )
{
  f.put((byte)W);
  f.put((byte)(W>>8));
  f.put((byte)(W>>16));
}
static void fputil( long W, QOStream &f )
{
  f.put((byte)W);
  f.put((byte)(W>>8));
  f.put((byte)(W>>16));
  f.put((byte)(W>>24));
}

enum {MaxRep=128};

static void UlozBBlok( byte *Blok, int *LBlok, QOStream &f )
{
  if( *LBlok>0 )
  {
    int L=*LBlok;
    f.put((byte)(L-1));
    while( --L>=0 )
    {
      f.put((byte)*Blok++);
    }
  }
  *LBlok=0;
}

static void PridejBBlok( byte *Blok, int *LBlok, QOStream &f, byte W )
{
  Blok[*LBlok]=W;
  (*LBlok)++;
  if( *LBlok>=MaxRep )
  {
    UlozBBlok(Blok,LBlok,f);
  }
}

static void PridejBRep( byte *Blok, int *LBlok, QOStream &f, byte LW, int rep )
{
  if( rep>0 )
  {
    if( rep<3 )
    {
      while( --rep>=0 ) PridejBBlok(Blok,LBlok,f,LW);
    }
    else
    {
      if( *LBlok>0 ) UlozBBlok(Blok,LBlok,f);
      f.put((byte)(rep-1+0x80));
      f.put(LW);
    }
  }
}

void SavePACB( QOStream &f, byte *Buf, long L )
{
  int LBloku=0;
  byte LW=0;
  int rep=0;
  byte *Blok=new byte[MaxRep];
  if( !Blok ) return;
  
  while( L>0 )
  {
    byte A=*Buf++;
    L--;
    if( rep<MaxRep && A==LW ) rep++;
    else
    {
      PridejBRep(Blok,&LBloku,f,LW,rep);
      LW=A,rep=1;
    }
  }
  PridejBRep(Blok,&LBloku,f,LW,rep);
  UlozBBlok(Blok,&LBloku,f);
  
  delete[] Blok;
}


static void SavePACLZW( QOStream &f, byte *buf, long l )
{
  QIStrStream strBuf((char *)buf,l);

  EncodeSS(f,(const char *)buf,l);

  #if 0

    istrstream vBuf((char *)buf,l);

    ostrstream ve;
    ostrstream vd;

    // verify
    EncodeSS(l,vBuf,ve);


    istrstream vei(ve.str(),ve.pcount());

    DecodeSS(l,vei,vd);

    ve.freeze(false);

    int dc=vd.pcount();
    if( dc!=l )
    {
      Fail("Bad encode length");
      return;
    }
    // compare data
    if( memcmp(vd.str(),buf,l) )
    {
      Fail("Bad encode");
    }
    vd.freeze(false);

  #endif
}

int fgetiw( QIStream &f )
{
  int c=f.get();
  if( c<0 ) return c;
  int r=f.get();
  if( r<0 ) return r;
  return ((long)r<<8)|c;
}
long fgeti24( QIStream &f )
{
  int c=f.get();if( c<0 ) return c;
  int r=f.get();if( r<0 ) return r;
  c|=r<<8;
  r=f.get();if( r<0 ) return r;
  c|=r<<16;
  return c;
}
long fgetil( QIStream &f )
{
  int c=f.get();if( c<0 ) return c;
  int r=f.get();if( r<0 ) return r;
  c|=r<<8;
  r=f.get();if( r<0 ) return r;
  c|=r<<16;
  r=f.get();if( r<0 ) return r;
  c|=r<<24;
  return c;
}

#define MAGIC_W_LZW 1234
#define MAGIC_H_LZW 8765

int Picture::LoadPCC( const char *src )
{
  int w,h;
  void *data565 = ::PCCLoad(src,&w,&h);
  if (data565)
  {
    Destruct();
    // convert 565 to 888
    _lenPal = 0;
    _sourceFormat = TexFormARGB1555;
    
    PPixel *Buf = new PPixel[w*h];
    const unsigned short *src = (const unsigned short *)data565;
    for (int y=0; y<h; y++) for (int x=0; x<w; x++)
    {
      // convert transparent 0xffdf
      if (src[y*w+x]==0xffdf)
        Buf[y*w+x] = 0;  
      else
        Buf[y*w+x] = Convert565To8888(src[y*w+x]);
    }
    _w = w, _h = h;
    _argb = Buf;
    _alphaWithBlack = true;
    free(data565);
    return 0;
  }
  return -1;

}


int Picture::LoadPAC( QIStream &f, int level )
{
  int pos = f.tellg();
  
  if (LoadPAADesc(f,level)==0) return 0;

  Destruct();
  
  int r=-1;
  byte *B,*Buf=NULL;

  f.seekg(pos,QIOS::beg);

  LoadTaggs(f);

  int C;
  C=fgetiw(f);
  if( f.fail() ) return -1;
  if( C<0 || C>256 ) return -1;
  _lenPal=C;
  int i;
  for( i=0; i<C; i++ )
  {
    _rgbPal[i]=fgeti24(f);
    if (_rgbPal[i]!=TRANSPARENT_RGB) _rgbPal[i] |= 0xff000000;
  }
  if( f.fail() ) return -1;

  _sourceFormat = TexFormP8;

  while( level-->=0 ) // skip all preceding levels
  {
    if (Buf) delete[] Buf;

    int w=fgetiw(f);
    int h=fgetiw(f);

    if( w==MAGIC_W_LZW && h==MAGIC_H_LZW )
    {
      w=fgetiw(f);
      h=fgetiw(f);
      _w=w,_h=h;
      if (w==0 && h==0) return 1;
      Buf=new byte[w*h];
      if (!Buf) return -1;
      int dSize=fgeti24(f);
      (void)dSize;
      if( DecodeSS((char *)Buf,w*h,f) ) r=0;
    }
    else
    {
      _w=w,_h=h;
      if (w==0 && h==0)
      {
        // terminator!!
        return 1;
      }
      
      int dSize=fgeti24(f);
      (void)dSize;
      w*=h;
      B=Buf=new byte[w];
      if( !Buf ) return -1;
      while( w>0 )
      {
        int c=f.get();
        if( f.fail() ) break;
        if( c&0x80 )
        {
          int v=f.get();
          c&=0x7f;
          c++;
          w-=c;
          while( --c>=0 ) *B++=(char)v;
        }
        else
        {
          c++;
          w-=c;
          f.read((char *)B,c);
          B+=c;
        }
        if( f.fail() ) break;
      }
    }
      
    if( !f.fail() ) r=0;
  }
  if( Buf && r<0 ) delete[] Buf,Buf=NULL;
  _picData=Buf;
  _alphaWithBlack=true;
  return r;
}

#define PAA_88   0x8080
#define PAA_4444 0x4444
#define PAA_1555 0x1555
#define PAA_DXT1 0xff01
#define PAA_DXT2 0xff02
#define PAA_DXT3 0xff03
#define PAA_DXT4 0xff04
#define PAA_DXT5 0xff05

void InvertSwizzle(TexSwizzle *invSwizzle, const TexSwizzle *swizzle, int ch)
{
  TexSwizzle swiz = TexSwizzle(TSAlpha+ch);
  if (swizzle[ch]>=TSInvAlpha && swizzle[ch]<=TSInvBlue)
  {
    invSwizzle[swizzle[ch]-TSInvAlpha]=TexSwizzle(swiz+TSInvAlpha-TSAlpha);
  }
  else if (swizzle[ch]<=TSBlue)
  {
    invSwizzle[swizzle[ch]]=swiz;
  }
}

int Picture::LoadPAADesc( QIStream &f, int level )
{
  Destruct();

  int r=-1;

  // fail if there is no descriptor
  int desc;
  desc=fgetiw(f);
  bool i88 = false;
  bool argb1555 = false;
  bool dxt = false;
  if( desc==PAA_88 ) i88=true;
  else if( desc==PAA_1555 ) argb1555=true;
  else if( desc==PAA_4444 ) {} // default 4444 format
  else if( desc==PAA_DXT1 ) dxt=true;
  else if( desc==PAA_DXT2 ) dxt=true;
  else if( desc==PAA_DXT3 ) dxt=true;
  else if( desc==PAA_DXT4 ) dxt=true;
  else if( desc==PAA_DXT5 ) dxt=true;
  else return -1;

  TexSwizzle swizzle[4];
  LoadTaggs(f,swizzle);

  int C=fgetiw(f);
  if( f.fail() ) return -1;
  if( C<0 || C>256 ) return -1;
  _lenPal=0; // start with skipping palette
  int i;
  for( i=0; i<C; i++ ) fgeti24(f);
  if( f.fail() ) return -1;

  while( level-->=0 ) // skip all preceding levels
  {

    int w=fgetiw(f); // get picture dimensions
    int h=fgetiw(f);

    if (w==0 && h==0)
    {
      return 1;
    }

#if USE_LZO_COMPRESSION
    // LZO compression is used for the mipmap data (highest bit of w used, w still can be up to 32768)
    bool lzo = (w & 0x8000) != 0;
    w &= 0x7fff;
#endif

    _w = w, _h = h;

    int dSize=fgeti24(f); // compressed size
    (void)dSize;

    if (dxt)
    {
      // load dxt directly to 8888 format
      _alphaWithBlack=false;
      _noAlphaDither=false,_noColorDither=false;
      // create temporary space for 64b blocks
      if (desc==PAA_DXT1)
      {
        Temp<DXTBlock64> blocks(_w/4*_h/4);
#if USE_LZO_COMPRESSION
        lzo_uint uncompressedSize = _w * _h / 2;
        if (lzo)
        {
          // load to temporary buffer
          Temp<char> compressed(dSize);
          f.read(compressed.Data(), dSize);

          // decompress
          int ok = lzo_init();
          if (ok != LZO_E_OK) return -1;
          ok = lzo1x_decompress((lzo_bytep)compressed.Data(), dSize,
            (lzo_bytep)blocks.Data(), &uncompressedSize, NULL);
          if (ok != LZO_E_OK) return -1;
        }
        else
        {
          f.read((char *)blocks.Data(), uncompressedSize);
        }
#else
        f.read((char *)blocks.Data(),_w*_h/2);
#endif
        if (f.fail()) return -1;
        DeleteARGB();
        _argb = LoadDXT1(blocks,_w,_h);
        if (!_argb) return -1;
        r=0;
        _alphaWithBlack=false;
        _sourceFormat = TexFormDXT1;
      }
      else
      {
        Temp<DXTBlock128> blocks(_w/4*_h/4);
#if USE_LZO_COMPRESSION
        lzo_uint uncompressedSize = _w * _h;
        if (lzo)
        {
          // load to temporary buffer
          Temp<char> compressed(dSize);
          f.read(compressed.Data(), dSize);

          // decompress
          int ok = lzo_init();
          if (ok != LZO_E_OK) return -1;
          ok = lzo1x_decompress((lzo_bytep)compressed.Data(), dSize,
            (lzo_bytep)blocks.Data(), &uncompressedSize, NULL);
          if (ok != LZO_E_OK) return -1;
        }
        else
        {
          f.read((char *)blocks.Data(), uncompressedSize);
        }
#else
        f.read((char *)blocks.Data(),_w*_h);
#endif
        if (f.fail()) return -1;
        DeleteARGB();
        _argb = LoadDXT1(&blocks[0].color,_w,_h,2);
        if (!_argb) return -1;
        if (desc==PAA_DXT2 || desc==PAA_DXT3)
        {
          _sourceFormat = TexFormDXT3;
          LoadDXT23(_argb,blocks,_w,_h);
        }
        else
        {
          _sourceFormat = TexFormDXT5;
          LoadDXT45(_argb,blocks,_w,_h);
        }
        _alphaWithBlack = (desc==PAA_DXT2 || desc==PAA_DXT4);

        r=0;
      }
    }
    else
    {
      Temp<word> buf(W()*H());
      if( !buf ) return -1;
      int size=W()*H()*2; // uncompressed size
      if( DecodeSS((char *)buf.Data(),size,f) ) r=0;
      // convert from ARGB4444 to ARGB8888
      DeleteARGB();
      _argb=new PPixel[_w*_h];
      if (!_argb) return -1;
      _alphaWithBlack=false;
      _noAlphaDither=false,_noColorDither=false;
      if( i88 )
      {
        Convert88To8888(_argb,buf,_w*_h);
        _sourceFormat = TexFormAI88;
      }
      else if( argb1555 )
      {
        Convert1555To8888(_argb,buf,_w*_h);
        _sourceFormat = TexFormARGB1555;
      }
      else
      {
        Convert4444To8888(_argb,buf,_w*_h);
        _sourceFormat = TexFormARGB4444;
      }
    }
  }
  if (_argb)
  {
    TexSwizzle invSwizzle[4]={TSAlpha,TSRed,TSGreen,TSBlue};
    InvertSwizzle(invSwizzle,swizzle,0);
    InvertSwizzle(invSwizzle,swizzle,1);
    InvertSwizzle(invSwizzle,swizzle,2);
    InvertSwizzle(invSwizzle,swizzle,3);
    ChannelSwizzle(invSwizzle);
  }
  return r;
}

int Picture::LoadPAA( QIStream &f, int level )
{
  int pos = f.tellg();
  int ret = LoadPAADesc(f,level);
  if (ret>=0) return ret;
  Destruct(); 

  f.seekg(pos,QIOS::beg);

  int r=-1;
  TexSwizzle swizzle[4];
  LoadTaggs(f,swizzle);
  
  int C=fgetiw(f);
  if( f.fail() ) return -1;
  if( C<0 || C>256 ) return -1;
  _lenPal=0; // start with skipping palette
  int i;
  for( i=0; i<C; i++ ) fgeti24(f);
  if( f.fail() ) return -1;

  _sourceFormat = TexFormARGB4444;
  while( level-->=0 ) // skip all preceding levels
  {
    int w=fgetiw(f); // get picture dimensions
    int h=fgetiw(f);
    _w=w,_h=h;
    if (w==0 && h==0) return 1;

    
    int dSize=fgeti24(f); // compressed size
    (void)dSize;
    int size=W()*H()*2; // uncompressed size

    Temp<word> buf(W()*H());
    if( !buf ) return -1;
    if( DecodeSS((char *)buf.Data(),size,f) ) r=0;
    // convert from ARGB4444 to ARGB8888
    DeleteARGB();
    _argb=new PPixel[_w*_h];
    if (!_argb) return -1;
    _alphaWithBlack=false;
    _noAlphaDither=false,_noColorDither=false;
    Convert4444To8888(_argb,buf,_w*_h);
  }
  if (_argb)
  {
    TexSwizzle invSwizzle[4]={TSAlpha,TSRed,TSGreen,TSBlue};
    InvertSwizzle(invSwizzle,swizzle,0);
    InvertSwizzle(invSwizzle,swizzle,1);
    InvertSwizzle(invSwizzle,swizzle,2);
    InvertSwizzle(invSwizzle,swizzle,3);
    ChannelSwizzle(invSwizzle);
  }
  return r;
}

struct DDS_CAPS
{
  DWORD dwCaps1; /* DDSCAPS_TEXTURE, DDSCAPS_MIPMAP, DDSCAPS_COMPLEX */
  DWORD dwCaps2;  /* DDSCAPS2_CUBEMAP, DDSCAPS2_VOLUME */
  DWORD Reserved[2];
};

struct DDS_PIXELFORMAT
{
  DWORD dwSize;
  DWORD dwFlags; // DDPF_RGB, DDPF_FOURCC
  DWORD dwFourCC;
  DWORD dwRGBBitCount;
  DWORD dwRBitMask;
  DWORD dwGBitMask;
  DWORD dwBBitMask;
  DWORD dwRGBAlphaBitMask;
};

struct DDS_DESC
{
  DWORD dwSize;
  DWORD dwFlags;
  DWORD dwHeight;
  DWORD dwWidth;
  DWORD dwPitchOrLinearSize;
  DWORD dwDepth;
  DWORD dwMipMapCount;
  DWORD dwReserved1[11];
  DDS_PIXELFORMAT ddpfPixelFormat;
  DDS_CAPS ddsCaps;
  DWORD dwReserved2;
};

#define FOURCC_CODE(a,b,c,d) ( (a)|((b)<<8)|((c)<<16)|((d)<<24) )

#define FOURCC_DDS FOURCC_CODE('D','D','S',' ')

// The dwFlags member of the DDS_DESC
#define DDS_F_CAPS  0x00000001
#define DDS_F_HEIGHT  0x00000002
#define DDS_F_WIDTH 0x00000004
#define DDS_F_PITCH 0x00000008
#define DDS_F_PIXELFORMAT 0x00001000
#define DDS_F_MIPMAPCOUNT 0x00020000
#define DDS_F_LINEARSIZE  0x00080000
#define DDS_F_DEPTH 0x00800000

// The ddpfPixelFormat member of the DDS_DESC
#define DDPF_F_ALPHAPIXELS  0x00000001
#define DDPF_F_FOURCC 0x00000004
#define DDPF_F_RGB  0x00000040

// The dwCaps1 member of the DDS_CAPS
#define DDSCAPS_F_COMPLEX 0x00000008
#define DDSCAPS_F_TEXTURE 0x00001000
#define DDSCAPS_F_MIPMAP  0x00400000

// The dwCaps2 member of the DDS_CAPS
#define DDSCAPS2_F_CUBEMAP  0x00000200
#define DDSCAPS2_F_CUBEMAP_POSITIVEX  0x00000400
#define DDSCAPS2_F_CUBEMAP_NEGATIVEX  0x00000800
#define DDSCAPS2_F_CUBEMAP_POSITIVEY  0x00001000
#define DDSCAPS2_F_CUBEMAP_NEGATIVEY  0x00002000
#define DDSCAPS2_F_CUBEMAP_POSITIVEZ  0x00004000
#define DDSCAPS2_F_CUBEMAP_NEGATIVEZ  0x00008000
#define DDSCAPS2_F_VOLUME 0x00200000


int Picture::LoadDDS(const char *src, int level)
{
  Destruct();
  if (level>0) return -1;
  
  QIFStream f;
  f.open(src);
  if (f.fail()) return -1;
  int magic;
  f.read(&magic,sizeof(magic));
  if (magic!=FOURCC_DDS) return -1;
  DDS_DESC desc;
  f.read(&desc,sizeof(desc));
  if (desc.dwSize!=sizeof(desc)) return -1;

  int w = desc.dwWidth;
  int h = desc.dwHeight;

  _w = w;
  _h = h;
  int pixelBitSize = 16;
  TexFormat format =  TexFormDefault;
  if (desc.ddpfPixelFormat.dwFlags&DDPF_F_FOURCC)
  {
    switch (desc.ddpfPixelFormat.dwFourCC)
    {
      case FOURCC_CODE('D','X','T','1'): pixelBitSize = 4, format = TexFormDXT1; break;
      case FOURCC_CODE('D','X','T','2'): pixelBitSize = 8, format = TexFormDXT2; break;
      case FOURCC_CODE('D','X','T','3'): pixelBitSize = 8, format = TexFormDXT3; break;
      case FOURCC_CODE('D','X','T','4'): pixelBitSize = 8, format = TexFormDXT4; break;
      case FOURCC_CODE('D','X','T','5'): pixelBitSize = 8, format = TexFormDXT5; break;
    }
  }
  else if (desc.ddpfPixelFormat.dwFlags&DDPF_F_RGB)
  {
    pixelBitSize = desc.ddpfPixelFormat.dwRGBBitCount;
    if (pixelBitSize==16)
    {
      if (
        desc.ddpfPixelFormat.dwRGBAlphaBitMask==0x8000 &&
        desc.ddpfPixelFormat.dwRBitMask==0x7c00 &&
        desc.ddpfPixelFormat.dwGBitMask==0x3e00 &&
        desc.ddpfPixelFormat.dwBBitMask==0x1f
      )
      {
        format = TexFormARGB1555;
      }
    }
    else if (pixelBitSize==32)
    {
      if (
        desc.ddpfPixelFormat.dwRGBAlphaBitMask==0xff000000 &&
        desc.ddpfPixelFormat.dwRBitMask==0xff0000 &&
        desc.ddpfPixelFormat.dwGBitMask==0xff00 &&
        desc.ddpfPixelFormat.dwBBitMask==0xff
      )
      format = TexFormARGB8888;
    }
    else if (pixelBitSize==24)
    {
      if (
        desc.ddpfPixelFormat.dwRBitMask==0xff0000 &&
        desc.ddpfPixelFormat.dwGBitMask==0xff00 &&
        desc.ddpfPixelFormat.dwBBitMask==0xff
      )
      format = TexFormARGB8888;
    }
  }
  if (format==TexFormDefault)
  {
    return -1;
  }
  int byteSize = w*h*pixelBitSize/8;
  if (desc.dwFlags&DDS_F_LINEARSIZE)
  {
    byteSize = desc.dwPitchOrLinearSize;
  }
  if (desc.dwFlags&DDS_F_PITCH)
  {
    byteSize = desc.dwPitchOrLinearSize*h;
  }
  
  if (format==TexFormDXT1)
  {
    Temp<DXTBlock64> blocks(byteSize/8);
    f.read((char *)blocks.Data(),byteSize);
    if (f.fail()) return -1;
    DeleteARGB();
    _argb = LoadDXT1(blocks,_w,_h);
    if (!_argb) return -1;
  }
  else if (format>=TexFormDXT2 && format<=TexFormDXT5)
  {
    Temp<DXTBlock128> blocks(byteSize/16);
    f.read((char *)blocks.Data(),byteSize);
    if (f.fail()) return -1;
    DeleteARGB();
    _argb = LoadDXT1(&blocks[0].color,_w,_h,2);
    if (!_argb) return -1;
    if (format==TexFormDXT2 || format==TexFormDXT3)
    {
      LoadDXT23(_argb,blocks,_w,_h);
    }
    else
    {
      LoadDXT45(_argb,blocks,_w,_h);
    }
  }
  else if (format==TexFormARGB8888)
  {
    if (pixelBitSize==32)
    {
      if (byteSize!=_w*_h*4) return -1;
      DeleteARGB();
      _argb = new PPixel[_w*_h];
      f.read(_argb,_w*_h*4);
      if (f.fail()) return -1;
    }
    else
    {
      if (byteSize!=_w*_h*3) return -1;
      Temp<char> temp(_w*_h*3);
      f.read(temp,_w*_h*3);
      if (f.fail()) return -1;
      DeleteARGB();
      _argb = new PPixel[_w*_h];
      for (int y=0; y<_h; y++) for (int x=0; x<_w; x++)
      {
        int r = temp[(_w*y+x)*3];
        int g = temp[(_w*y+x)*3+1];
        int b = temp[(_w*y+x)*3+2];
        _argb[y*w+x] = 0xff000000|(r<<16)|(g<<8)|b;
      }
    }
  }
  else
  {
    return -1;
  }
  _maxColor = 0xffffffff;
  _procSource = "";
  _lenPal=0;
  _noAlphaDither=false,_noColorDither=false;

  _alphaWithBlack=false;
  _sourceFormat = format;


  return 0;
}

int Picture::LoadPNG( const char *src )
{
  Destruct();

  _maxColor = 0xffffffff;
  _procSource = "";
  _lenPal=0;
  _noAlphaDither=false,_noColorDither=false;
  _alphaWithBlack=false;

  return GFormatPNG->Load(src,_w,_h,_argb);
}

int Picture::LoadTGA( const char *S )
{
  Destruct();
  
  int np;
  _argb=(PPixel *)TGALoad(S,&_w,&_h,&np);
  if( np!=32 ) return -1;
  _maxColor = 0xffffffff;
  _procSource = "";
  _lenPal=0;
  _noAlphaDither=false,_noColorDither=false;

  //_alphaWithBlack=false;
  _alphaWithBlack = IsBlackInAlpha();
  return 0;
}

int Picture::LoadTransparent()
{
  Destruct();
  #define DEF_SIZE 8
  _w=DEF_SIZE;
  _h=DEF_SIZE;
  _argb=new PPixel[_w*_h];
  PPixel *d=_argb;
  int x,y;
  for( x=0; x<_w; x++ ) for( y=0; y<_h; y++ )
  {
    *d++=0x00000000;
  }
  _alphaWithBlack=true;
  _noAlphaDither=true,_noColorDither=false;
  _lenPal=0;
  _maxColor = 0xffffffff;
  _procSource = "";
  return 0;
}

int Picture::Load( QIStream &in, int level, bool isPaa )
{
  if (!isPaa) return LoadPAC(in,level);
  else return LoadPAA(in,level);
}

int Picture::Load( const char *src, int level )
{
  Destruct();
  const char *ext = NajdiPExt(NajdiNazev(src));
  if( !strcmpi(ext,".pac") )
  {
    QIFStream in;
    if (in.fail()) return -1;
    in.open(src);
    return Load(in,level,false);
  }
  if( !strcmpi(ext,".paa") )
  {
    QIFStream in;
    if (in.fail()) return -1;
    in.open(src);
    return Load(in,level,true);
  }
  if( !strcmpi(ext,".dds") ) return LoadDDS(src,level);
  // all mipmapped formats checked
  if (level>0 ) return -1;
  if( !strcmpi(src,"#transparent#") ) return LoadTransparent();
  //if( !strcmpi(ext,".gif") ) return LoadGIF(src);
  if( !strcmpi(ext,".pcc") ) return LoadPCC(src);
  if( !strcmpi(ext,".tga") ) return LoadTGA(src);
  if( !strcmpi(ext,".jpg") ) return LoadJPG(src);
  if( !strcmpi(ext,".png") ) return LoadPNG(src);
  // TODO: if extension is unknow, try loading as a PAA file ???
  return -1;
  //return Load(in,level,true);
}

#define DXT_ENABLE 1

// texture hints


static const EnumName TexMipmapFilterNames[]=
{
  EnumName(TexMipDefault,                 "Default"),
  EnumName(TexMipFadeOut,                 "FadeOut"),
  EnumName(TexMipNormalizeNormalMap,      "NormalizeNormalMap"),
  EnumName(TexMipNormalizeNormalMapAlpha, "NormalizeNormalMapAlpha"),
  EnumName(TexMipNormalizeNormalMapFade,  "NormalizeNormalMapFade"),
  EnumName(TexMipAlphaNoise,              "AlphaNoise"),
  EnumName(TexMipNormalizeNormalMapNoise, "NormalizeNormalMapNoise"),
  EnumName(TexMipAddAlphaNoise,       "AddAlphaNoise"), // for trees
  EnumName()
};
template<>
const EnumName *GetEnumNames(TexMipmapFilter dummy)
{
  return TexMipmapFilterNames;
}

static const EnumName TexErrorModeNames[]=
{
  EnumName(TexErrEye,        "Eye"),
  EnumName(TexErrDistance,   "Distance"),
  EnumName(TexErrDistanceXY, "DistanceXY"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(TexErrorMode dummy)
{
  return TexErrorModeNames;
}


static const EnumName TexFormatNames[]=
{
  EnumName(TexFormDefault,"Default"),
  EnumName(TexFormARGB1555,"ARGB1555"),
  EnumName(TexFormARGB4444,"ARGB4444"),
  EnumName(TexFormARGB8888,"ARGB8888"),
  EnumName(TexFormAI88,"AI88"),
  EnumName(TexFormDXT1,"DXT1"),
  EnumName(TexFormDXT2,"DXT2"),EnumName(TexFormDXT3,"DXT3"),
  EnumName(TexFormDXT4,"DXT4"),EnumName(TexFormDXT5,"DXT5"),
  EnumName(TexFormP8,"P8"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(TexFormat dummy)
{
  return TexFormatNames;
}


static const EnumName TexSwizzleNames[]=
{
  EnumName(TSAlpha,"A"),
  EnumName(TSRed,"R"),
  EnumName(TSGreen,"G"),
  EnumName(TSBlue,"B"),
  EnumName(TSInvAlpha,"1-A"),
  EnumName(TSInvRed,"1-R"),
  EnumName(TSInvGreen,"1-G"),
  EnumName(TSInvBlue,"1-B"),
  EnumName(TSOne,"1"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(TexSwizzle dummy)
{
  return TexSwizzleNames;
}


// hints
class HintList: public AutoArray<TextureHints>
{
};

static HintList Hints;
static TextureHints DefHints;

TextureHints::TextureHints()
{
  _name = RString();
  _enableDXT = true;
  _enableDynRange = true;
  _limitSize = 65536;
  _mipmapFilter = TexMipDefault;
  _format = TexFormDefault;
  _channelSwizzle[0] = TSAlpha;
  _channelSwizzle[1] = TSRed;
  _channelSwizzle[2] = TSGreen;
  _channelSwizzle[3] = TSBlue;
  _virtualSwizzle = true;
  _autoreduce = false;
  _dithering = false;
  _errorMode = TexErrEye;
}


LSError TextureHints::Serialize(ParamArchive &ar)
{
  CHECK( ar.Serialize("name",_name,0) );
  CHECK( ar.Serialize("limitSize",_limitSize,0,4096) );
  CHECK( ar.Serialize("enableDXT",_enableDXT,0,true) );
  CHECK( ar.Serialize("dynRange",_enableDynRange,0,true) );
  CHECK( ar.SerializeEnum("mipmapFilter",_mipmapFilter,0,TexMipDefault) );
  CHECK( ar.SerializeEnum("format",_format,0,TexFormDefault) );
  CHECK( ar.SerializeEnum("errorMetrics",_errorMode,0,TexErrEye) );

  CHECK( ar.SerializeEnum("channelSwizzleA",_channelSwizzle[0],0,TSAlpha) );
  CHECK( ar.SerializeEnum("channelSwizzleR",_channelSwizzle[1],0,TSRed) );
  CHECK( ar.SerializeEnum("channelSwizzleG",_channelSwizzle[2],0,TSGreen) );
  CHECK( ar.SerializeEnum("channelSwizzleB",_channelSwizzle[3],0,TSBlue) );
  CHECK( ar.Serialize("virtualSwizzle",_virtualSwizzle,0,true) );

  CHECK( ar.Serialize("autoreduce",_autoreduce,0,false) );
  CHECK( ar.Serialize("dithering",_dithering,0,false) );

  return LSOK;
}

bool PictureConfigLoaded(const ParamEntryVal &cfg)
{
  RegisterPicFunctions();
  // parse config file, if bad, return false
  ClassEntry *cls = new ParamClassEntry(const_cast<ParamEntryVal &>(cfg));
  ParamArchiveLoadEntry ar(cls);
  //Serialize(ar);
  ar.Serialize("textureHints",Hints,0);
  return true;
}

void PictureConfigUnloaded()
{
  Hints.Clear();
}

#include <Es/Files/filenames.hpp>

const TextureHints &FindHints(const char *name)
{
  // get only filename from name
  const char *sname = GetFilenameExt(name);
  for (int i=0; i<Hints.Size(); i++)
  {
    const TextureHints &hints = Hints[i];
    //if (!strcmpi(hints._name,sname)) return hints;
    if (Matches(sname,hints._name))
    {
      return hints;
    }
  }
  return DefHints;
}

void Picture::GetHints(TextureHints &hints, const char *S)
{
  // note picture content is currently ignored, only filename is used
  hints = FindHints(S);
}

struct palinfo
{
  int pindex;
  int pcol;
  long rgb;
};

TypeIsSimple(palinfo)

static int CmpPInfo( const void *_P, const void *_p )
{
  struct palinfo *P=(struct palinfo *)_P;
  struct palinfo *p=(struct palinfo *)_p;
  int dif=p->pcol-P->pcol;
  if( dif ) return dif;
  return p->rgb-P->rgb;
}

void Picture::SortPal()
{
  if( IsRGB() ) return;
  
  int i;
  long cnt;
  Temp<palinfo> PInfo(MaxPal);
  Temp<long> NewRGB(MaxPal);
  Temp<byte> RevPal(MaxPal);

  for( i=0; i<MaxPal; i++ ) NewRGB[i]=0;
  for( i=0; i<_lenPal; i++ )
  {
    if( _rgbPal[i]==RESERVED_RGB ) _rgbPal[i]=TRANSPARENT_RGB;
  }
  
  for( i=0; i<_lenPal; i++ )
  {
    long rgb=_rgbPal[i];
    PInfo[i].pindex=i;
    PInfo[i].pcol=GetR(rgb)+GetG(rgb)+GetB(rgb);
    PInfo[i].rgb=rgb;
  }
  // some systems may perform transparency based on 1st pixel color
  //i=_picData[0];
  //PInfo[i].pcol=0x100*4;
  qsort(PInfo,_lenPal,sizeof(*PInfo),CmpPInfo);

  for( i=0; i<_lenPal; i++ )
  {
    NewRGB[i]=_rgbPal[PInfo[i].pindex];
    RevPal[PInfo[i].pindex]=(byte)i;
  }
  // reverse pallette keeps size of the original palette
  int revPalSize = _lenPal;
  
  // compress palette - remove repeated colors
  int last=NewRGB[0];
  for( i=1; i<_lenPal; )
  {
    if( NewRGB[i]==last )
    {
      // color i is equal to i-1
      // all references to color>=i should be replaced with color-1
      int j;
      for( j=0; j<revPalSize; j++ )
      {
        if( RevPal[j]>=i ) --RevPal[j];
      }
      for( j=i; j<_lenPal; j++ ) NewRGB[j-1]=NewRGB[j];
      _lenPal--;
    }
    else
    {
      last=NewRGB[i];
      i++;
    }
  }
  
  
  for( i=0; i<_lenPal; i++ ) _rgbPal[i]=NewRGB[i];
  if( _picData )
  {
    byte *Buf=_picData;
    for( cnt=(long)_w*_h; --cnt>=0; )
    {
      int index=*Buf;
      byte convPixel=RevPal[index];
      *Buf++=convPixel;
      if( convPixel>=_lenPal || convPixel<0 )
      {
        printf("Pallete color error.\n");
        exit(1);
      }
    }
  }

  TouchPalette();
}

//static Picture _picData;


/// conversion from SRGB (8b) to linear space (fixed point 8.8)
struct ConvertFromSRGB
{
  int _table[255+1];
  
  ConvertFromSRGB()
  {
    for (int i=0; i<lenof(_table); i++)
    {
      float srgb = i*(1.0f/(lenof(_table)-1));
      float l = pow((srgb+0.055f)*(1.0f/1.055f),2.4f);
      int lFix = toInt(l*255*256);
      saturate(lFix,0,0xff00);
      _table[i] = lFix;
    }
  }
  int operator () (int i) const {return _table[i];}
} FromSRGB;


/// conversion from linear space (fixed point 8.8b) to SRGB (8b)
struct ConvertToSRGB
{
  unsigned char _table[255*8+1];
  
  ConvertToSRGB()
  {
    for (int i=0; i<lenof(_table); i++)
    {
      float l = i*(1.0f/(lenof(_table)-1));
      float srgb = pow(l,1.0f/2.4f)*1.055f-0.055f;
      int srgbFix = toInt(srgb*255);
      saturate(srgbFix,0,0xff);
      _table[i] = srgbFix;
    }
  }
  
  int operator () (int i) const
  {
    // discard 6b - used only to avoid rounding error during summation
    int index = (i+16)>>5;
    saturate(index,0,lenof(_table)-1);
    return _table[index];
  }
} ToSRGB;

/// SRGB average of 4 pixels
static PPixel CombineSRGB( PPixel rgb0, PPixel rgb1, PPixel rgb2, PPixel rgb3 )
{
  // alpha is always in linear space
  int a= (
    ((rgb0>>24)&0xff)+((rgb1>>24)&0xff)+((rgb2>>24)&0xff)+((rgb3>>24)&0xff)
  ); // a is the only one that could overflow is if was not shifted
  // it cannot overflow now
  // if alpha is 0 or 1 we can perform simple averaging
  if (a==0 || a==0xff*4)
  {
    a=(a+2)>>2; // low 8b
    int r = (FromSRGB(GetR(rgb0))+FromSRGB(GetR(rgb1))+FromSRGB(GetR(rgb2))+FromSRGB(GetR(rgb3))+2)>>2;
    int g = (FromSRGB(GetG(rgb0))+FromSRGB(GetG(rgb1))+FromSRGB(GetG(rgb2))+FromSRGB(GetG(rgb3))+2)>>2;
    int b = (FromSRGB(GetB(rgb0))+FromSRGB(GetB(rgb1))+FromSRGB(GetB(rgb2))+FromSRGB(GetB(rgb3))+2)>>2;
    
    // maximum sum of a is 4*0xff = 0x3fc
    int sr = ToSRGB(r);
    int sg = ToSRGB(g);
    int sb = ToSRGB(b);
    return (a<<24)+(sr<<16)+(sg<<8)+sb;
  }
  else
  {
    int a0 = ((rgb0>>24)&0xff);
    int a1 = ((rgb1>>24)&0xff);
    int a2 = ((rgb2>>24)&0xff);
    int a3 = ((rgb3>>24)&0xff);
    // we want to perform averaging in target space (alpha-multiplied)
    // result of FromSRGB is fixed point 8.8
    int r0 = FromSRGB(GetR(rgb0));
    int g0 = FromSRGB(GetG(rgb0));
    int b0 = FromSRGB(GetB(rgb0));
    int r1 = FromSRGB(GetR(rgb1));
    int g1 = FromSRGB(GetG(rgb1));
    int b1 = FromSRGB(GetB(rgb1));
    int r2 = FromSRGB(GetR(rgb2));
    int g2 = FromSRGB(GetG(rgb2));
    int b2 = FromSRGB(GetB(rgb2));
    int r3 = FromSRGB(GetR(rgb3));
    int g3 = FromSRGB(GetG(rgb3));
    int b3 = FromSRGB(GetB(rgb3));
    // we know a is not a zero
    float r = (r0*a0+r1*a1+r2*a2+r3*a3)*(1.0f/a);
    float g = (g0*a0+g1*a1+g2*a2+g3*a3)*(1.0f/a);
    float b = (b0*a0+b1*a1+b2*a2+b3*a3)*(1.0f/a);
    int sr = ToSRGB(toInt(r));
    int sg = ToSRGB(toInt(g));
    int sb = ToSRGB(toInt(b));
    a=(a+2)>>2; // low 8b
    return (a<<24)+(sr<<16)+(sg<<8)+sb;
  }
}

/// linear average of 4 pixels with alpha weighting
static PPixel CombineAlpha( PPixel rgb0, PPixel rgb1, PPixel rgb2, PPixel rgb3 )
{
  int a= (
    ((rgb0>>24)&0xff)+((rgb1>>24)&0xff)+((rgb2>>24)&0xff)+((rgb3>>24)&0xff)
  ); // a is the only one that could overflow is if was not shifted
  int r= (
    (rgb0&0xff0000)+(rgb1&0xff0000)+(rgb2&0xff0000)+(rgb3&0xff0000)
  );
  int g= (
    (rgb0&0xff00)+(rgb1&0xff00)+(rgb2&0xff00)+(rgb3&0xff00)
  );
  int b= (
    (rgb0&0xff)+(rgb1&0xff)+(rgb2&0xff)+(rgb3&0xff)
  );
  // if alpha is 0 or 1 we can perform simple averaging
  if (a==0 || a==0xff*4)
  {
    // maximum sum of a is 4*0xff = 0x3fc
    // it cannot overflow now
    a=(a+2)>>2; // low 8b
    r=((r+2)>>2)&0xff0000;
    g=((g+2)>>2)&0xff00;
    b=((b+2)>>2); // low 8b
    return (a<<24)+r+g+b;
  }
  else
  {
    int a0 = ((rgb0>>24)&0xff);
    int a1 = ((rgb1>>24)&0xff);
    int a2 = ((rgb2>>24)&0xff);
    int a3 = ((rgb3>>24)&0xff);
    // we want to perform averaging in target space (alpha-multiplied)
    int r0 = GetR(rgb0);
    int g0 = GetG(rgb0);
    int b0 = GetB(rgb0);
    int r1 = GetR(rgb1);
    int g1 = GetG(rgb1);
    int b1 = GetB(rgb1);
    int r2 = GetR(rgb2);
    int g2 = GetG(rgb2);
    int b2 = GetB(rgb2);
    int r3 = GetR(rgb3);
    int g3 = GetG(rgb3);
    int b3 = GetB(rgb3);
    // we know a is not a zero
    float r = (r0*a0+r1*a1+r2*a2+r3*a3)*(1.0f/a);
    float g = (g0*a0+g1*a1+g2*a2+g3*a3)*(1.0f/a);
    float b = (b0*a0+b1*a1+b2*a2+b3*a3)*(1.0f/a);
    int sr = toInt(r);
    int sg = toInt(g);
    int sb = toInt(b);
    a=(a+2)>>2; // low 8b
    return (a<<24)+(sr<<16)+(sg<<8)+sb;
  }
}

/// linear average of 4 pixels
static PPixel Combine( PPixel rgb0, PPixel rgb1, PPixel rgb2, PPixel rgb3 )
{
  int a= (
    ((rgb0>>24)&0xff)+((rgb1>>24)&0xff)+((rgb2>>24)&0xff)+((rgb3>>24)&0xff)
  ); // a is the only one that could overflow is if was not shifted
  int r= (
    (rgb0&0xff0000)+(rgb1&0xff0000)+(rgb2&0xff0000)+(rgb3&0xff0000)
  );
  int g= (
    (rgb0&0xff00)+(rgb1&0xff00)+(rgb2&0xff00)+(rgb3&0xff00)
  );
  int b= (
    (rgb0&0xff)+(rgb1&0xff)+(rgb2&0xff)+(rgb3&0xff)
  );
  // maximum sum of a is 4*0xff = 0x3fc
  // it cannot overflow now
  a=(a+2)>>2; // low 8b
  r=((r+2)>>2)&0xff0000;
  g=((g+2)>>2)&0xff00;
  b=((b+2)>>2); // low 8b
  return (a<<24)+r+g+b;
}


static PPixel Combine( PPixel rgb0, PPixel rgb1 )
{
  //int nTrans=0;
  //if( rgb0==TRANSPARENT_RGB ) return rgb1;
  //if( rgb1==TRANSPARENT_RGB ) return rgb0;
  
  rgb0>>=1;
  rgb1>>=1;
  rgb0&=0x7f7f7f7f;
  rgb1&=0x7f7f7f7f;
  return rgb0+rgb1+0x01010101; // avoid double rounding error
}

PPixel Interpol( PPixel rgb0, PPixel rgb1, float coef )
{
  int a0=(rgb0>>24)&0xff;
  int r0=(rgb0>>16)&0xff;
  int g0=(rgb0>>8)&0xff;
  int b0=(rgb0>>0)&0xff;
  
  int a1=(rgb1>>24)&0xff;
  int r1=(rgb1>>16)&0xff;
  int g1=(rgb1>>8)&0xff;
  int b1=(rgb1>>0)&0xff;
  
  int a=toInt(a0*coef+a1*(1-coef));
  int r=toInt(r0*coef+r1*(1-coef));
  int g=toInt(g0*coef+g1*(1-coef));
  int b=toInt(b0*coef+b1*(1-coef));
  return (a<<24)|(r<<16)|(g<<8)|b;
}

static PPixel Blend( PPixel rgb0, PPixel rgb1, float coef0, float coef1 )
{
  int a0=(rgb0>>24)&0xff;
  int r0=(rgb0>>16)&0xff;
  int g0=(rgb0>>8)&0xff;
  int b0=(rgb0>>0)&0xff;
  
  int a1=(rgb1>>24)&0xff;
  int r1=(rgb1>>16)&0xff;
  int g1=(rgb1>>8)&0xff;
  int b1=(rgb1>>0)&0xff;
  
  int a=toInt(a0*coef0+a1*coef1);
  int r=toInt(r0*coef0+r1*coef1);
  int g=toInt(g0*coef0+g1*coef1);
  int b=toInt(b0*coef0+b1*coef1);
  if( a>255 ) a=255;if( a<0 ) a=0;
  if( r>255 ) r=255;if( r<0 ) r=0;
  if( g>255 ) g=255;if( g<0 ) g=0;
  if( b>255 ) b=255;if( b<0 ) b=0;
  return (a<<24)|(r<<16)|(g<<8)|b;
}

//#define FIND_BITS 5 // how many bits are used in lookup table (4-6 is enough)
#define FIND_BITS 8 // how many bits are used in lookup table (4-6 is enough)
#define FIND_COLORS (1<<(FIND_BITS*3))
#define FIND_MASK ( (1<<FIND_BITS)-1 )

static int CmpPalStat( const void *_P, const void *_p )
{
  struct palStat *P=(struct palStat *)_P;
  struct palStat *p=(struct palStat *)_p;
  if( p->count-P->count>0 ) return +1;
  if( p->count-P->count<0 ) return -1;
  return 0;
}

void Picture::Prefer( const Picture &src )
{
  for( int i=0; i<src._lenPal; i++ )
  {
    long rgb=src._rgbPal[i];
    // add only if it is not already preferred
    bool alreadyIs=false;
    if( _prefer.Size()>0 )
    {
      long *start=&_prefer[0];
      long *end=&_prefer[_prefer.Size()];
      for( ; start<end; start++ )
      {
        if( *start==rgb ) {alreadyIs=true;break;}
      }
      if( !alreadyIs ) _prefer.Add(rgb);
    }
  }
}

int Picture::MinRGBARaw(int &minR,int &minG, int &minB, int &minA) const
{
  // if we are palette, scan palette only
  if (!_argb)
  {
    minR = 0;
    minG = 0;
    minB = 0;
    minA = 0;
    return 0;
  }

  ConvertARGB();
  minR = 0xff;
  minG = 0xff;
  minB = 0xff;
  minA = 0xff;
  int size=_h*_w;
  for( int i=0; i<size; i++ )
  {
    long rgb=_argb[i];
    int a=(rgb>>24)&0xff;
    int r=((rgb>>16)&0xff);
    int g=((rgb>>8)&0xff);
    int b=((rgb>>0)&0xff);
    // ignore colors with alpha = 0
    if (a<1) continue;
    if (minA>a) minA = a;
    if (minR>r) minR = r;
    if (minG>g) minG = g;
    if (minB>b) minB = b;
  }
  int minRGB = minR;
  if (minRGB>minG) minRGB = minG;
  if (minRGB>minB) minRGB = minB;
  return minRGB;
}

int Picture::MaxRGBARaw(int &maxR,int &maxG, int &maxB, int &maxA) const
{
  // if we are palette, scan palette only
  if (!_argb)
  {
    maxR = 0xff;
    maxG = 0xff;
    maxB = 0xff;
    maxA = 0xff;
    return 0xff;
  }

  ConvertARGB();
  maxR = 0;
  maxG = 0;
  maxB = 0;
  maxA = 0;
  int size=_h*_w;
  for( int i=0; i<size; i++ )
  {
    long rgb=_argb[i];
    int a=(rgb>>24)&0xff;
    int r=((rgb>>16)&0xff);
    int g=((rgb>>8)&0xff);
    int b=((rgb>>0)&0xff);
    // ignore colors with alpha = 0
    if (a<1) continue;
    if (maxA<a) maxA = a;
    if (maxR<r) maxR = r;
    if (maxG<g) maxG = g;
    if (maxB<b) maxB = b;
  }
  int maxRGB = maxR;
  if (maxRGB<maxG) maxRGB = maxG;
  if (maxRGB<maxB) maxRGB = maxB;
  return maxRGB;
}

int Picture::MaxRGBA(int &maxR,int &maxG, int &maxB, int &maxA)
{
  MaxRGBARaw(maxR,maxG,maxB,maxA);
  int mr = (_maxColor>>16)&0xff;
  int mg = (_maxColor>>8)&0xff;
  int mb = (_maxColor>>0)&0xff;
  maxR = toInt((mr*maxR)/255.0);
  maxG = toInt((mg*maxG)/255.0);
  maxB = toInt((mb*maxB)/255.0);
  saturate(maxR,0,255);
  saturate(maxG,0,255);
  saturate(maxB,0,255);
  int maxRGB = maxR;
  if (maxRGB<maxG) maxRGB = maxG;
  if (maxRGB<maxB) maxRGB = maxB;
  return maxRGB;
}

PPixel Picture::GetAverageColorCalculated() const
{
  if (!_argb)
  {
    // do not calculate average color for palette textures
    return 0;
  }
  return TransformByMax(GetAverageColorRaw());
}

PPixel Picture::GetMaxColor() const
{
  return TransformByMax(GetMaxColorRaw());
}
PPixel Picture::GetMinColor() const
{
  return TransformByMax(GetMinColorRaw());
}

PPixel Picture::GetAverageColor() const
{
  return TransformByMax(_averageColor);
}

float Picture::MaxDynamicRangeCompression() const
{
  int maxR = (_maxColor>>16)&0xff;
  int maxG = (_maxColor>>8)&0xff;
  int maxB = (_maxColor>>0)&0xff;
  float rFactor = maxR/255.0;
  float gFactor = maxG/255.0;
  float bFactor = maxB/255.0;
  return floatMin(rFactor,gFactor,bFactor);
}

void Picture::DefaultDynamicRange()
{
  // if range is maximal, there is nothing to do any more
  if ((_maxColor&0xffffff)==0xffffff)
  {
    return;
  }
  int maxR = (_maxColor>>16)&0xff;
  int maxG = (_maxColor>>8)&0xff;
  int maxB = (_maxColor>>0)&0xff;
  float rFactor = maxR/255.0;
  float gFactor = maxG/255.0;
  float bFactor = maxB/255.0;
  int size=_h*_w;
  for( int i=0; i<size; i++ )
  {
    long rgb=_argb[i];
    //int a=(rgb>>24)&0xff;
    int r=((rgb>>16)&0xff);
    int g=((rgb>>8)&0xff);
    int b=((rgb>>0)&0xff);
    // ignore colors with alpha = 0
    //if (a<1) continue;
    int nr = toInt(r*rFactor);
    int ng = toInt(g*gFactor);
    int nb = toInt(b*bFactor);
    saturate(nr,0,255);
    saturate(ng,0,255);
    saturate(nb,0,255);
    long nrgb = (rgb&0xff000000)|(nr<<16)|(ng<<8)|nb;
    _argb[i]=nrgb;
  }
  _maxColor = (_maxColor&0xff000000)|0xffffff;
}

const bool DynRangeEnabled = false;

void Picture::MaximizeDynamicRange()
{
  if (DynRangeEnabled)
  {
    // maximize dynamic range to improve texture precision
    int maxR,maxG,maxB,maxA;
    MaxRGBARaw(maxR,maxG,maxB,maxA);
    float rFactor = maxR>0 ? 255.0/maxR : 1;
    float gFactor = maxG>0 ? 255.0/maxG : 1;
    float bFactor = maxB>0 ? 255.0/maxB : 1;
    int size=_h*_w;
    for( int i=0; i<size; i++ )
    {
      long rgb=_argb[i];
      int r=((rgb>>16)&0xff);
      int g=((rgb>>8)&0xff);
      int b=((rgb>>0)&0xff);
      int nr = toInt(r*rFactor);
      int ng = toInt(g*gFactor);
      int nb = toInt(b*bFactor);
      saturate(nr,0,255);
      saturate(ng,0,255);
      saturate(nb,0,255);
      long nrgb = (rgb&0xff000000)|(nr<<16)|(ng<<8)|nb;
      _argb[i]=nrgb;
    }
    #ifdef _DEBUG
    int tstR=0,tstG=0,tstB=0,tstA=0;
    for( int i=0; i<size; i++ )
    {
      long rgb=_argb[i];
      int a=(rgb>>24)&0xff;
      int r=((rgb>>16)&0xff);
      int g=((rgb>>8)&0xff);
      int b=((rgb>>0)&0xff);
      if (a<1) continue;
      if (tstA<r) tstA = r;
      if (tstR<r) tstR = r;
      if (tstG<g) tstG = g;
      if (tstB<b) tstB = b;
    }
    #endif
    // modulate current max range
    int nMaxR = (_maxColor>>16)&0xff;
    int nMaxG = (_maxColor>>8)&0xff;
    int nMaxB = (_maxColor>>0)&0xff;
    nMaxR = toInt((nMaxR*maxR)/255.0);
    nMaxG = toInt((nMaxG*maxG)/255.0);
    nMaxB = toInt((nMaxB*maxB)/255.0);
    saturate(nMaxR,0,255);
    saturate(nMaxG,0,255);
    saturate(nMaxB,0,255);
    //_maxColor = (_maxColor&0xff000000)|(nMaxR<<16)|(nMaxG<<8)|(nMaxB<<0);
    _maxColor = 0xff000000|(nMaxR<<16)|(nMaxG<<8)|(nMaxB<<0);
  }
  else
  {
    DefaultDynamicRange();
    // we can verify dynamic range is not wasted
    int maxR=0,maxG=0,maxB=0;
    int minR=255,minG=255,minB=255;
    int size=_h*_w;
    for( int i=0; i<size; i++ )
    {
      long rgb=_argb[i];
      int r=((rgb>>16)&0xff);
      int g=((rgb>>8)&0xff);
      int b=((rgb>>0)&0xff);
      if (maxR<r) maxR = r;
      if (maxG<g) maxG = g;
      if (maxB<b) maxB = b;
      if (minR>r) minR = r;
      if (minG>g) minG = g;
      if (minB>b) minB = b;
    }
    const int lowRange = 64;
    if (
      (maxR>minR || maxG>minG || maxB>minB) &&
      ( maxR<lowRange && maxG<lowRange && maxB<lowRange )
    )
    {
      RptF(
        "Bit precision not used effectively. Max is %d,%d,%d of 255,255,255",maxR,maxG,maxB
      );
    }
  }
}

bool Picture::Histogram( AutoArray<palStat> &histogram ) const
{
  Temp<int> counts(FIND_COLORS);
  int i;
  // init palette statistics
  for( i=0; i<FIND_COLORS; i++ )
  {
    counts[i]=0;
    //counts[i].rgb=i;
  }
    
  // calculate popularity
  bool isTransparent=false;
  if( _alphaWithBlack )
  {
    int size=_h*_w;
    for( i=0; i<size; i++ )
    {
      
      long rgb=_argb[i];
      int a=(rgb>>24)&0xff;
      if (a<0xff)
      {
        // there is alpha of black mixed into the color
        // i.e. _argb=(1-alpha)*color+alpha*black
        // color=_argb/(1-alpha)
        float coef=1/(1-a/255.0f);
        int r=toInt( ((rgb>>16)&0xff)*coef);
        int g=toInt( ((rgb>>8)&0xff)*coef);
        int b=toInt( ((rgb>>0)&0xff)*coef);
        if( r>0xff ) r=0xff;
        if( g>0xff ) g=0xff;
        if( b>0xff ) b=0xff;
        rgb=(r<<16)|(g<<8)|(b<<0);
        isTransparent=true;
      }
#if FIND_BITS==8
      int index=rgb&0xffffff;
#else
      int r=((rgb>>16)&0xff)>>(8-FIND_BITS);
      int g=((rgb>>8)&0xff)>>(8-FIND_BITS);
      int b=((rgb>>0)&0xff)>>(8-FIND_BITS);
      int index=(r<<(FIND_BITS*2))|(g<<FIND_BITS)|b;
#endif
      counts[index]++;
    }
  }
  else
  {
    int size=_h*_w;
    for( i=0; i<size; i++ )
    {
      
      long rgb=_argb[i];
      int a=(rgb>>24)&0xff;
      if( a<=0xc0 ) isTransparent=true;
#if FIND_BITS==8
      int index=rgb&0xffffff;
#else
      int r=((rgb>>16)&0xff)>>(8-FIND_BITS);
      int g=((rgb>>8)&0xff)>>(8-FIND_BITS);
      int b=((rgb>>0)&0xff)>>(8-FIND_BITS);
      int index=(r<<(FIND_BITS*2))|(g<<FIND_BITS)|b;
#endif
      counts[index]++;
    }
  }

  /**/
  // if there are some prefered colors, improve their histogram count
  for( i=0; i<_prefer.Size(); i++ )
  {
    long rgb=_prefer[i];
    int r=((rgb>>16)&0xff)>>(8-FIND_BITS);
    int g=((rgb>>8)&0xff)>>(8-FIND_BITS);
    int b=((rgb>>0)&0xff)>>(8-FIND_BITS);
    int index=(r<<(FIND_BITS*2))|(g<<FIND_BITS)|b;
    if( counts[index] ) counts[index]+=1024; // strongly prefered
  }
  /**/
  for( i=0; i<FIND_COLORS; i++ ) if( counts[i] )
  {
    
    //MakeRGB(
    //  (i>>(FIND_COLORS*2))&FIND_MASK,
    //  (i>>(FIND_COLORS))&FIND_MASK,
    //  (i>>(0)&FIND_MASK,
    //);
    palStat &stat = histogram.Append();
    stat.count = counts[i];
    stat.rgb = i;
    //Assert( counts[i].rgb>=0 );
  }
  return isTransparent;
}

bool Picture::IsGray() const
{
  // check if the bitmap contains only gray degrees
  // use 4-bit precision comparison
  const PPixel *d32=_argb;
  for( int i=0; i<_w*_h; i++ )
  {
    int a=GetA(*d32),r=GetR(*d32),g=GetG(*d32),b=GetB(*d32);
    // do rounding to nearest
    int ia=(a+8)>>4,ir=(r+8)>>4,ig=(g+8)>>4,ib=(b+8)>>4;
    if( ia>15 ) ia=15;
    if( ir>15 ) ir=15;
    if( ig>15 ) ig=15;
    if( ib>15 ) ib=15;
    if( ir!=ig || ir!=ib ) return false;
    d32++;
  }
  return true;
}

bool Picture::IsUniformColor() const
{
  if (_w*_h<=1) return true;
  // if not an argb picture, we do not want to test. We no longer support palette pictures
  if (!_argb) return false;
  const PPixel color=_argb[0];
  const PPixel *d32=_argb;
  for( int i=0; i<_w*_h; i++ )
  {
    if (*d32++!=color) return false;
  }
  return true;
}

bool Picture::IsPalTransparent() const
{
  for( int i=0; i<_lenPal; i++ )
  {
    long col=_rgbPal[i];
    if( col==TRANSPARENT_RGB ) return true;
    if( col==RESERVED_RGB ) return true;
  }
  return false;
}

bool Picture::IsOpaque() const
{
  // check if the bitmap contains only gray degrees
  // use 4-bit precision comparison
  const PPixel *d32=_argb;
  for( int i=0; i<_w*_h; i++ )
  {
    int a=GetA(*d32);
    int ia=a>>4;
    if( ia!=0xf )
    {
      return false;
    }
    d32++;
  }
  return true;
}

bool Picture::IsMasked() const
{
  // check if the bitmap contains only gray degrees
  // use 4-bit precision comparison
  const PPixel *d32=_argb;
  for( int i=0; i<_w*_h; i++ )
  {
    int a=GetA(*d32);
    /*
    int ia=a>>4;
    if( ia!=0 && ia!=15 )
    {
      return false;
    }
    */
    if( a!=0 && a!=255 )
    {
      return false;
    }
    d32++;
  }
  return true;
}

void Picture::Popularity()
{
  if( !_argb ) return;

  // construct palette
  AutoArray<palStat> histogram;
  bool isTransparent=Histogram(histogram);

  // sort popularity array - maybe too slow
  qsort(&histogram[0],histogram.Size(),sizeof(histogram[0]),CmpPalStat);
  
  // use first 256 as new palette
  int maxCol=256; // sometimes include transparent color
  if( isTransparent ) maxCol--;
  int i;
  if( maxCol>histogram.Size() ) maxCol=histogram.Size();
  for( i=0; i<maxCol; i++ )
  {
    int rgb=histogram[i].rgb;
    _rgbPal[i]=rgb;
  }
  if( isTransparent ) _rgbPal[i++]=TRANSPARENT_RGB;
  _lenPal=i;
  for( ; i<256; i++ ) _rgbPal[i]=0;
  TouchPalette();
}

inline int Min( int a, int b ) {if( a<b ) return a;return b;}
inline int Max( int a, int b ) {if( b<a ) return a;return b;}

inline float Min( float a, float b ) {if( a<b ) return a;return b;}
inline float Max( float a, float b ) {if( b<a ) return a;return b;}

class ColorFloat
{
  public:
  float r,g,b;
  ColorFloat( float rr, float gg, float bb ){r=rr,g=gg,b=bb;}
  ColorFloat( long rgb )
  {
    r=GetR(rgb)*(1.0/255);
    g=GetG(rgb)*(1.0/255);
    b=GetB(rgb)*(1.0/255);
  }
  long GetRGB() const
  {
    int ri=toInt(r*255);if( ri<0 ) ri=0;if( ri>255 ) ri=255;
    int gi=toInt(g*255);if( gi<0 ) gi=0;if( gi>255 ) gi=255;
    int bi=toInt(b*255);if( bi<0 ) bi=0;if( bi>255 ) bi=255;
    return MakeRGB(ri,gi,bi);
  }
};

class ColorRange
{
  private:
  ColorFloat _min,_max,_avg;
  //long _min,_max;
  //long _avg;
  float _n;

  public:
  ColorRange()
  :_min(0xffffff),_max(0),_n(0),_avg(0)
  {}
  ColorRange( const ColorFloat &rgb, float pixels )
  :_min(rgb),_max(rgb),_avg(rgb),_n(pixels)
  {}
  void Init() {_min=0xffffff,_max=0,_n=0,_avg=0;}
  void Merge( const ColorRange &src );
  const ColorFloat &Average() const {return _avg;}

  float SquareSize() const;
  float SizeR() const {return (_max.r-_min.r)*R_EYE;}
  float SizeG() const {return (_max.g-_min.g)*G_EYE;}
  float SizeB() const {return (_max.b-_min.b)*B_EYE;}
  bool Simple() const;
  #define COLOR_EPS ( 1.0/63 )
  bool SimpleR() const {return _max.r-_min.r<COLOR_EPS;}
  bool SimpleG() const {return _max.g-_min.g<COLOR_EPS;}
  bool SimpleB() const {return _max.b-_min.b<COLOR_EPS;}
  void MakeSimple();

  float Size() const {return sqrt(SquareSize());}
  float Pixels() const {return _n;}
};

void ColorRange::MakeSimple()
{
  _min=_max=_avg; // note - this cannot be splitted in given direction!
}

bool ColorRange::Simple() const
{
  if( !SimpleR() ) return false;
  if( !SimpleG() ) return false;
  if( !SimpleB() ) return false;
  return true;
}


void ColorRange::Merge( const ColorRange &src )
{
  float invSum=1.0/(_n+src._n);
  _avg.r=(_avg.r*_n+src._avg.r*src._n)*invSum;
  _avg.g=(_avg.g*_n+src._avg.g*src._n)*invSum;
  _avg.b=(_avg.b*_n+src._avg.b*src._n)*invSum;
  _min.r=Min(_min.r,src._min.r);
  _min.g=Min(_min.g,src._min.g);
  _min.b=Min(_min.b,src._min.b);
  _max.r=Max(_max.r,src._max.r);
  _max.g=Max(_max.g,src._max.g);
  _max.b=Max(_max.b,src._max.b);
  _n+=src._n;
}

float ColorRange::SquareSize() const
{
  float r=(_max.r-_min.r)*0.299;
  float g=(_max.g-_min.g)*0.587;
  float b=(_max.b-_min.b)*0.114;
  return r*r+g*g+b*b;
}

class ColorRangeWithHistogram: public ColorRange
{
  private:
  AutoArray<palStat> _histogram;
  double _error; // sum of square distances of all pixels from average

  public:
  ColorRangeWithHistogram(){}
  void Init() {ColorRange::Init(),_histogram.Clear();}
  void Init( AutoArray<palStat> &_histogram );
  void InitSplit
  (
    const ColorRangeWithHistogram &range,
    ColorRangeWithHistogram &rest,
    const ColorFloat &mid
  );
  void CalculateError();
  double Error() const {return _error;}
};

TypeIsMovableZeroed(ColorRangeWithHistogram)

void ColorRangeWithHistogram::Init( AutoArray<palStat> &histogram )
{
  for( int i=0; i<histogram.Size(); i++ )
  {
    Merge(ColorRange(histogram[i].rgb,histogram[i].count));
  }
  _histogram=histogram;
  CalculateError();
}

void ColorRangeWithHistogram::CalculateError()
{
  // calculate total error
  const ColorFloat &avg=Average();
  double errorSum=0;
  for( int i=0; i<_histogram.Size(); i++ )
  {
    ColorFloat c=_histogram[i].rgb;
    double r=(avg.r-c.r)*R_EYE;
    double g=(avg.g-c.g)*G_EYE;
    double b=(avg.b-c.b)*B_EYE;
    errorSum+=(r*r+g*g+b*b)*_histogram[i].count;
  }
  _error=errorSum;
}

void ColorRangeWithHistogram::InitSplit
(
  const ColorRangeWithHistogram &range,
  ColorRangeWithHistogram &rest,
  const ColorFloat &mid
)
{
  Init();
  for( int i=0; i<range._histogram.Size(); i++ )
  {
    const palStat &item=range._histogram[i];
    ColorFloat c=item.rgb;
    if( c.r>=mid.r || c.g>=mid.g || c.b>=mid.b )
    {
      rest._histogram.Add(item);
      rest.Merge(ColorRange(c,item.count));
    }
    else
    {
      _histogram.Add(item);
      Merge(ColorRange(c,item.count));
    }
  }
  CalculateError();
  rest.CalculateError();
}

void Picture::MedianCut()
{
  if( !_argb ) return;

  int i;
  AutoArray<ColorRangeWithHistogram> ranges;
  // create one big color range
  _lenPal=0;
#if 0
  if( !_noColorDither )
  {
    // we have to fix eight basic colors (RGB cube corners)
    _rgbPal[_lenPal++]=0xff000000;
    _rgbPal[_lenPal++]=0xff0000ff;
    _rgbPal[_lenPal++]=0xff00ff00;
    _rgbPal[_lenPal++]=0xff00fefe; // 0x00ffff is TRANSPARENT_RGB
    _rgbPal[_lenPal++]=0xffff0000;
    _rgbPal[_lenPal++]=0xfffe00fe; // 0xff00ff is TRANSPARENT_RGB
    _rgbPal[_lenPal++]=0xffffff00;
    _rgbPal[_lenPal++]=0xffffffff;
  }
#endif
  int maxCol=256-_lenPal;
  bool isTransparent;
  {
    AutoArray<palStat> histogram;
    isTransparent=Histogram(histogram);
    // calculate requested number of colors
    if( isTransparent ) maxCol--;
    
    if( histogram.Size()<=maxCol )
    {
      // fastest possible - we can use histogram
      int dst=_lenPal;
      for( i=0; i<histogram.Size(); i++ )
      {
        _rgbPal[dst++]=histogram[i].rgb;
      }
      if( isTransparent ) _rgbPal[dst++]=TRANSPARENT_RGB;
      _lenPal=dst;
      for( ; dst<256; dst++ ) _rgbPal[dst]=0;
      TouchPalette();
      return;
    }
    if( maxCol>histogram.Size() ) maxCol=histogram.Size();
    ColorRangeWithHistogram all;
    int a=ranges.Add(all);
    ranges[a].Init(histogram);
  }
  
  while( ranges.Size()<maxCol )
  {
    // find biggest range and split it (histogram is needed for this)
    // biggest is considered:
    // highest Pixels() (if _max>_min, i.e. SquareSize()>0)
    //int maxN=0;
    double maxError=0;
    int maxI=-1;
    for( i=0; i<ranges.Size(); i++ )
    {
      if( ranges[i].Simple() ) continue; // this one cannot be divided
      //int n=ranges[i].Pixels();
      //if( maxN<n ) maxN=n,maxI=i;
      double error=ranges[i].Error();
      if( maxError<error ) maxError=error,maxI=i;
    }
    if( maxI<0 ) break; // no range to divide
    // divide longest edge of range in average point
    i=maxI;
    float r=ranges[i].SizeR();
    float g=ranges[i].SizeG();
    float b=ranges[i].SizeB();
    // caution: Add can change ranges pointer
    ColorFloat rgbMid(1.0,1.0,1.0);
    if( ranges[i].SimpleR() ) r=0;
    if( ranges[i].SimpleG() ) g=0;
    if( ranges[i].SimpleB() ) b=0;
    if( g>r && g>b )
    { // green is longest
      rgbMid.g=ranges[i].Average().g;
    }
    else if( r>g && r>b )
    { // red is longest
      rgbMid.r=ranges[i].Average().r;
    }
    else
    { // b must be longest
      Assert( !ranges[i].SimpleB() );
      rgbMid.b=ranges[i].Average().b;
    }
    ColorRangeWithHistogram empty;
    int low=ranges.Add(empty);
    int high=ranges.Add(empty);
    ranges[low].InitSplit(ranges[i],ranges[high],rgbMid);
    if( ranges[low].Pixels()<=0 || ranges[high].Pixels()<=0 )
    {
      Fail("Singular color range");
      ranges.Delete(high);
      ranges.Delete(low);
      ranges[i].MakeSimple();
    }
    else ranges.Delete(i);
  }

  // make palette from range array
  int dst=_lenPal;
  for( i=0; i<ranges.Size(); i++ )
  {
    _rgbPal[dst++]=ranges[i].Average().GetRGB();
  }
  if( isTransparent ) _rgbPal[dst++]=TRANSPARENT_RGB;
  _lenPal=dst;
  for( ; dst<256; dst++ ) _rgbPal[dst]=0;
  TouchPalette();
}


byte Picture::ExactNearestColor( long rgb, int limit ) const
{
  // try to find exact match
  int i;
  if( limit<0 || limit>_lenPal ) limit=_lenPal;
  for( i=0; i<limit; i++ )
  {
    if( rgb==_rgbPal[i] ) return (byte)i;
  }
  int iMin=0;
  int difMin=INT_MAX;
  //int a=(rgb>>24)&0xff;
  int r=(rgb>>16)&0xff;
  int g=(rgb>>8)&0xff;
  int b=(rgb>>0)&0xff;
  for( i=0; i<limit; i++ )
  {
    //int ap=(_rgbPal[i]>>24)&0xff;
    if( _rgbPal[i]==TRANSPARENT_RGB ) continue; // only exact match possible
    int rp=(_rgbPal[i]>>16)&0xff;
    int gp=(_rgbPal[i]>>8)&0xff;
    int bp=(_rgbPal[i]>>0)&0xff;

    int dif2=
    (
      //(a-rp)*(a-rp)+
      (r-rp)*(r-rp)+
      (g-gp)*(g-gp)+
      (b-bp)*(b-bp)
    );
    if( difMin>dif2 ) difMin=dif2,iMin=i;
  }
  return (byte)iMin;
}

byte Picture::NearestColor( long rgb ) const
{
  // convert from 8-bits to FIND_BITS
  if( rgb==TRANSPARENT_RGB )
  {
    if( _lenPal>0 ) if( _rgbPal[_lenPal-1]==TRANSPARENT_RGB ) return _lenPal-1;
    return ExactNearestColor(rgb);
  }
  if( !_nearestCache )
  {
    _nearestCache=new byte[FIND_COLORS];
    memset(_nearestCache,0,FIND_COLORS);
  }
  if( !_nearestCache ) return ExactNearestColor(rgb);
  int r=((rgb>>16)&0xff)>>(8-FIND_BITS);
  int g=((rgb>>8)&0xff)>>(8-FIND_BITS);
  int b=((rgb>>0)&0xff)>>(8-FIND_BITS);
  int index=(r<<(FIND_BITS*2))|(g<<FIND_BITS)|b;
  if( index>=FIND_COLORS ) return ExactNearestColor(rgb);
  byte res=_nearestCache[index];
  if( !res )
  {
    // color 0 has no entry value
    res=ExactNearestColor(rgb);
    if( res>0 ) _nearestCache[index]=res;
    else _nearestCache[index]=1;
  }
  else if( res==1 )
  {
    // 1 can mean 0 or 1
    res=ExactNearestColor(rgb,2);
  }
  // following condintions should fail, but for sure..
  if( res<0 ) {printf("Color<0\n");return 0;}
  if( res>_lenPal-1 ) {printf("Color<0\n");return _lenPal-1;}
  return res;
}

bool Picture::IsBlackInAlpha() const
{
  ConvertARGB();
  bool opaque = true;
  // check if there is black in alpha
  for( int i=0; i<_w*_h; i++ )
  {
    int col=_argb[i];
    int a=(col>>24)&0xff;
    int r=(col>>16)&0xff;
    int g=(col>>8)&0xff;
    int b=(col>>0)&0xff;
    if (a!=0xff) opaque = false;
    if( a!=0 )
    {
      float invA=255.0/a;
      r=toInt(r*invA);
      g=toInt(g*invA);
      b=toInt(b*invA);
      if( r>256 ) return false;
      if( g>256 ) return false;
      if( b>256 ) return false;
    }
    else
    {
      if( r ) return false;
      if( g ) return false;
      if( b ) return false;
    }
  }
  // if picture is completely opaque, there is no need for alpha correction
  if (opaque) return false;
  return true;
}

void Picture::RemoveBlackFromAlpha() const
{
  ConvertARGB();
  //if( !_alphaWithBlack ) return;
  if( !_alphaWithBlack )
  {
    //_alphaWithBlack=false;
    return;
  }
  long average=GetAverageColorRaw()&0xffffff;
  for( int i=0; i<_w*_h; i++ )
  {
    int col=_argb[i];
    int a=(col>>24)&0xff;
    if( a!=0 )
    {
      int r=(col>>16)&0xff;
      int g=(col>>8)&0xff;
      int b=(col>>0)&0xff;
      float invA=255.0/a;
      r=toInt(r*invA);
      g=toInt(g*invA);
      b=toInt(b*invA);
      if( r<0 ) r=0;if( r>255 ) r=255;
      if( g<0 ) g=0;if( g>255 ) g=255;
      if( b<0 ) b=0;if( b>255 ) b=255;
      _argb[i]=MakeARGB(a,r,g,b);
    }
    else _argb[i]=average;
  }
  // calculate average color
  _alphaWithBlack=false;
}

void Picture::AddBlackToAlpha()
{
  ConvertARGB();
  if( _alphaWithBlack ) return;
  for( int i=0; i<_w*_h; i++ )
  {
    int col=_argb[i];
    int a=(col>>24)&0xff;
    if( a )
    {
      int r=(col>>16)&0xff;
      int g=(col>>8)&0xff;
      int b=(col>>0)&0xff;
      float fA=a/255.0;
      r=toInt(r*fA);
      g=toInt(g*fA);
      b=toInt(b*fA);
      if( r<0 ) r=0;if( r>255 ) r=255;
      if( g<0 ) g=0;if( g>255 ) g=255;
      if( b<0 ) b=0;if( b>255 ) b=255;
      _argb[i]=MakeARGB(a,r,g,b);
    }
    else _argb[i]=0;
  }
  _alphaWithBlack=true;
}

void Picture::HideReserved() const
{
  // reserved color is converted to transparent and picture is marked
  reserved=FALSE;
  for( int i=0; i<_lenPal; i++ )
  {
    if( _rgbPal[i]==RESERVED_RGB ) _rgbPal[i]=TRANSPARENT_RGB,reserved=TRUE;
  }
}

void Picture::ShowReserved() const
{
  if( reserved ) for( int i=0; i<_lenPal; i++ )
  {
    if( _rgbPal[i]==TRANSPARENT_RGB ) _rgbPal[i]=RESERVED_RGB;
  }
  reserved=FALSE;
}

void Picture::DeleteARGB() const
{
  if( _argb ) delete[] _argb,_argb=NULL;
}

int Picture::Compare( const Picture &with ) const
{
  if (with.W()!=W()) return -1;
  if (with.H()!=H()) return -1;
  with.ConvertARGB();
  ConvertARGB();
  return memcmp(with._argb,_argb,sizeof(DWORD)*W()*H());
}

void Picture::ConvertARGB() const
{
  if( !_picData ) return;
  if( _argb ) return;
  _argb=new PPixel[_w*_h];
  if( !_argb ) return;
  HideReserved();
  for( int i=0; i<_w*_h; i++ )
  {
    // convert color-key transparency into alpha channel
    int col=_rgbPal[_picData[i]];
    if( col==TRANSPARENT_RGB ) col=0x00000000;
    if( col==RESERVED_RGB ) col=0x00000000;
    _argb[i]=col;
  }
  _alphaWithBlack=true;
  RemoveBlackFromAlpha();
  delete[] _picData;_picData = NULL;
}

void Picture::ConvertPal() const
{
  if( _lenPal<=0 ) return; // no palette - cannot convert
  if( !_argb ) return;
  if( _picData ) delete[] _picData;
  RemoveBlackFromAlpha();
  _picData=new byte[_w*_h];
  if( !_picData ) return;
  int i=0;
  // maybe: to avoid any error, always touch palette before converting
  //TouchPalette();

  #if 1
  // check if transparent or not
  #if 0
  if (IsOpaque())
  {
    // dither opaque images
    DitherMode ditherR(_w),ditherG(_w),ditherB(_w);
    for( int y=0; y<_h; y++ )
    {
      for( int x=0; x<_w; x++ )
      {
        long rgb=_argb[i];
        // no dithering on transparent (undrawn) pixels
        float r=GetR(rgb);
        float g=GetG(rgb);
        float b=GetB(rgb);
        float rError = ditherR.GetError(x);
        float gError = ditherG.GetError(x);
        float bError = ditherB.GetError(x);
        // max. 1 bit difference allowed
        saturate(rError,-8,+8);
        saturate(gError,-8,+8);
        saturate(bError,-8,+8);
        r+=rError;
        g+=gError;
        b+=bError;
        int ir=toInt(r);if( ir<0 ) ir=0;if( ir>255 ) ir=255;
        int ig=toInt(g);if( ig<0 ) ig=0;if( ig>255 ) ig=255;
        int ib=toInt(b);if( ib<0 ) ib=0;if( ib>255 ) ib=255;
        rgb=MakeRGB(ir,ig,ib);
        //int index=NearestColor(rgb);
        int index=NearestColor(rgb);
        long rgbPic=_rgbPal[index];
        _picData[i]=index;
        ditherR.SetError(x,r-GetR(rgbPic));
        ditherG.SetError(x,g-GetG(rgbPic));
        ditherB.SetError(x,b-GetB(rgbPic));
        i++;
      }
      ditherR.EndOfLine();
      ditherG.EndOfLine();
      ditherB.EndOfLine();
    }
  }
  else
  #endif
  {
    for( int y=0; y<_h; y++ )
    {
      for( int x=0; x<_w; x++ )
      {
        long rgb=_argb[y*_w+x];
        int a=(rgb>>24)&0xff;
        if( a<=0xff-0xd0 )
        {
          _picData[i]=NearestColor(TRANSPARENT_RGB);
        }
        else
        {
          _picData[i]=ExactNearestColor(rgb);
        }
        i++;
      }
    }
  }
  #else
  
  ChromaKeyDither dither(_w,_alphaWithBlack,!_noAlphaDither);
  DitherMode ditherR(_w),ditherG(_w),ditherB(_w);
  for( int y=0; y<_h; y++ )
  {
    for( int x=0; x<_w; x++ )
    {
      long rgb=dither.GetColor(_argb[i],x);
      if( rgb!=TRANSPARENT_RGB && !_noColorDither )
      {
        // no dithering on transparent (undrawn) pixels
        float r=GetR(rgb);
        float g=GetG(rgb);
        float b=GetB(rgb);
        r+=ditherR.GetError(x);
        g+=ditherG.GetError(x);
        b+=ditherB.GetError(x);
        int ir=toInt(r);if( ir<0 ) ir=0;if( ir>255 ) ir=255;
        int ig=toInt(g);if( ig<0 ) ig=0;if( ig>255 ) ig=255;
        int ib=toInt(b);if( ib<0 ) ib=0;if( ib>255 ) ib=255;
        rgb=MakeRGB(ir,ig,ib);
        int index=NearestColor(rgb);
        long rgbPic=_rgbPal[index];
        _picData[i]=index;
        ditherR.SetError(x,r-GetR(rgbPic));
        ditherG.SetError(x,g-GetG(rgbPic));
        ditherB.SetError(x,b-GetB(rgbPic));
      }
      else _picData[i]=NearestColor(rgb);
      i++;
    }
    dither.EndOfLine();
    if( !_noColorDither )
    {
      ditherR.EndOfLine();
      ditherG.EndOfLine();
      ditherB.EndOfLine();
    }
  }
  #endif
}

size_t Picture::GetVRAMSize() const
{
  int nPixels = W()*H();
  switch (_sourceFormat)
  {
    case TexFormARGB8888:
      return nPixels*4;
    case TexFormDefault:
    case TexFormARGB1555:
    case TexFormARGB4444:
    case TexFormAI88:
      return nPixels*2;
    case TexFormDXT1:
      return nPixels/2;
    case TexFormDXT2: case TexFormDXT3:
    case TexFormDXT4: case TexFormDXT5:
      return nPixels;
    case TexFormP8:
      return nPixels;
  }
  return nPixels;
}

void Picture::CalculateAverageColor() const
{
  _averageColor=GetAverageColorRaw(); // save average color including alpha value
}
void Picture::CalculateAverageColorSRGB() const
{
  _averageColor=GetAverageColorRawSRGB(); // save average color including alpha value
}

struct Count
{
  enum {MaxItems=256};
  unsigned char index[MaxItems];
  int count[MaxItems];
  int nItems;

  
  void Add( unsigned char item );
  void Reset(){nItems=0;}  
  int MostCommon() const;
};

void Count::Add( unsigned char item )
{
  for( int i=0; i<nItems; i++ )
  {
    if( index[i]==item )
    {
      count[i]++;
      return;
    }
  }
  index[nItems]=item;
  count[nItems++]=1;
}

int Count::MostCommon() const
{
  int maxI=0;
  int maxC=0;
  for( int i=0; i<nItems; i++ )
  {
    int c=count[i];
    if( c>maxC ) maxC=c,maxI=index[i];
  }
  return maxI;
}

struct BigCount
{
  enum {MaxItems=256};
  int count[MaxItems];

  void Add( unsigned char item );
  void Reset(){memset(count,0,sizeof(count));}
  int MostCommon() const;
};



void BigCount::Add( unsigned char item )
{
  count[item]++;
}
int BigCount::MostCommon() const
{
  int maxI=0,maxC=0;
  for( int i=0; i<MaxItems; i++ )
  {
    int c=count[i];
    if( c>maxC ) maxC=c,maxI=i;
  }
  return maxI;
}

int Picture::ResizeFilter( const Picture &src, int xFactorLog, int yFactorLog )
{
  memcpy(_rgbPal,src._rgbPal,sizeof(_rgbPal));
  _lenPal=src._lenPal;
  _nearestCache=NULL;

  _alphaWithBlack=src._alphaWithBlack;
  _prefer=src._prefer;
  _noAlphaDither=src._noAlphaDither;
  _noColorDither=src._noColorDither;

  int xStep=1<<xFactorLog;
  int yStep=1<<yFactorLog;

  if( src._w<xStep*2 || src._h<yStep*2 ) return 0;
  if( src._w&(xStep-1) || src._h&(yStep-1) ) return 0;

  if( !src._picData ) return 0;

  if( _argb ) delete[] _argb,_argb=NULL;
  _w=src._w>>xFactorLog;
  _h=src._h>>yFactorLog;
  _picData=new byte[_w*_h];
  if( !_picData ) return 0;

  byte *d=_picData;
  /*
  if( yStep*xStep>=32 )
  {
    BigCount count;
    for( int y=0; y<_h; y+=yStep ) for( int x=0; x<_w; x+=xStep )
    {
      count.Reset();

      // TODO: if yStep*xStep>=256, there is more efficient variant 
      for( int yy=0; yy<yStep; yy++ ) for( int xx=0; xx<xStep; xx++ )
      {
        int index=(y+yy)*src._w+(x+xx);
        count.Add(src._picData[index]);
      }
      // select most common color
      *d++=count.MostCommon();
    }
  }
  else
  */
  {
    Count count;
    for( int y=0; y<src._h; y+=yStep ) for( int x=0; x<src._w; x+=xStep )
    {
      count.Reset();

      // TODO: if yStep*xStep>=256, there is more efficient variant 
      for( int yy=0; yy<yStep; yy++ ) for( int xx=0; xx<xStep; xx++ )
      {
        int index=(y+yy)*src._w+(x+xx);
        count.Add(src._picData[index]);
      }
      *d++=count.MostCommon();

      //int index=y*src._w+x;
      //*d++=src._picData[index];
    }
  }

  return 1;
}

/**
@param currentLevel Level the new level of mipmap is derived from
@param skipLevels Determines number of levels to skip (usually 1)
@param minDimension Determines the minimal dimension of the downsized picture
*/

bool Picture::MipmapHalfsize(int skipLevels, int minDimension, const TextureHints &hints)
{
  // Process all skipped levels
  while (--skipLevels>=0)
  {
    
    // Try to create half sized picture
    bool ok = false;
    if (hints._enableDynRange) ok = HalfsizeSRGB();
    else if (
      hints._mipmapFilter==TexMipNormalizeNormalMapAlpha ||
      hints._mipmapFilter==TexMipNormalizeNormalMapNoise ||
      hints._mipmapFilter == TexMipAddAlphaNoise
    ) ok = HalfsizeAlpha();
    else ok = Halfsize();
    if (!ok) return false;
    
    // Test the dimensions to the minimal dimensions
    if ((_w < minDimension) || (_h < minDimension)) return false;
  }
  return true;
}

bool Picture::MipmapFilter(int level, const TextureHints &hints)
{
  
  int currentSize = intMax(W(),H());

  // factor is determined by current resolution
  //                                  64   32    16    8    4     2
  static const float factorTable[]={1,0.8f,0.75f,0.67f,0.5f,0.25f,0.125f, 0};
  const int startFade = 128;
  const float *curFactor = factorTable;
  // Move in the factor table
  while (currentSize<startFade)
  {
    if ( *curFactor>0) curFactor++;
    currentSize <<=1;
  }

  // Choose the filter and run it
  switch (hints._mipmapFilter)
  {
  case TexMipFadeOut:
    FilterFadeOut(*curFactor);
    break;
  case TexMipNormalizeNormalMap:
  case TexMipNormalizeNormalMapAlpha:
    FilterNormalizeNormalMap(1.0f);
    break;
  case TexMipNormalizeNormalMapFade:
    FilterNormalizeNormalMap(*curFactor);
    break;
  case TexMipAlphaNoise:
    FilterAlphaNoise(level);
    break;
  case TexMipNormalizeNormalMapNoise:
    FilterNormalizeNormalMap(1.0f);
    FilterAlphaNoise(level, 0.0f, 1.0f);
    break;
  case TexMipDefault:
    break;

  case TexMipAddAlphaNoise:
    FilterAddAlphaNoise( level );
    break;

  default:
    LogF("Unknown mipmap filter");
    break;
  }
  
  return true;
}

bool Picture::FilterFadeOut(float factor)
{
  int factorI = toLargeInt(factor*(float)0x10000);
  if (factorI==65536) return 0;
  ConvertARGB();
  if( !_argb ) return 0;
  
  int negFactorIm128 = (0x10000-factorI)*128;
  PPixel *d = _argb;
  for ( int i=_w*_h; --i>=0; )
  {
    int c = *d;
    unsigned int a = (c>>24)&0xff;
    unsigned int r = (c>>16)&0xff;
    unsigned int g = (c>> 8)&0xff;
    unsigned int b = (c    )&0xff;
    a = (a*factorI + negFactorIm128)>>16 ;
    r = (r*factorI + negFactorIm128)>>16 ;
    g = (g*factorI + negFactorIm128)>>16 ;
    b = (b*factorI + negFactorIm128)>>16 ;
    c = (a<<24)|(r<<16)|(g<<8)|b;
    *d++=c;
  } 
  return 1;
}

// Factor presents a linear interpolation between (0, 0, 1) and calculated vector
// 1 => calculated vector
// 0 => (0, 0, 1)
bool Picture::FilterNormalizeNormalMap(float factor)
{
  // Convert picture to the ARGB format
  ConvertARGB();
  if( !_argb ) return false;

  PPixel *d = _argb;
  int size = _w * _h;
  for (int i = 0; i < size; i++)
  {
    // Get the pixel value
    int c = *d;

    // Load ARGB variables
    unsigned int a = (c>>24)&0xff;
    unsigned int r = (c>>16)&0xff;
    unsigned int g = (c>> 8)&0xff;
    unsigned int b = (c    )&0xff;

    // Convert RGB into floats
    float fR = r / 127.5f - 1.0f;
    float fG = g / 127.5f - 1.0f;
    float fB = b / 127.5f - 1.0f;

    // Linear interpolation
    fR *= factor;
    fG *= factor;
    fB = fB * factor + (1.0f - factor);

    // Get the invSize of the vector
    float invSize = 1 / sqrt(fR * fR + fG * fG + fB * fB);

    // Normalize
    fR *= invSize;
    fG *= invSize;
    fB *= invSize;

    // Save RGB back to integers
    r = toInt((fR + 1.0f) * 127.5f);
    g = toInt((fG + 1.0f) * 127.5f);
    b = toInt((fB + 1.0f) * 127.5f);

    // Write result
    c = (a<<24)|(r<<16)|(g<<8)|b;
    *d = c;
    d++;
  }

  return true;
}


// JAVA REFERENCE IMPLEMENTATION OF IMPROVED NOISE - COPYRIGHT 2002 KEN PERLIN.

static PerlinNoise SPerlinNoise;

struct WhiteNoise
{
  float p[256][256];
  static const int permutation[];
  
  WhiteNoise();
  float operator () (float x, float y) const;
} SWhiteNoise;

WhiteNoise::WhiteNoise()
{
}


/**
Add noise so that Alpha Testing works nice with continuous alpha surface.
@param orig multiplication factor - range
@param noise addition factor - offset

Default values are set so that alpha = 1 alpha test against 128 is always passed
while for alpha = 0 it is always failed
*/
bool Picture::FilterAlphaNoise(int level, float orig, float noise)
{
  // Convert picture to the ARGB format
  ConvertARGB();
  if( !_argb ) return false;

  srand(1);
  PPixel *d = _argb;
  for (int y = 0; y < _h; y++)
  for (int x = 0; x < _w; x++)
  {
    // Get the pixel value
    int c = *d;

    // Load ARGB variables
    unsigned int a = (c>>24)&0xff;
    unsigned int rgb = c&0xffffff;

    // add noise into a
#if 1
  #if 1   
    float u = x*(1.0f/_w);
    float v = y*(1.0f/_h);
    float scale = 500.0f;
  #else
    float u = x;
    float v = y;
    float scale = 0.1f;
  #endif
    float perlin = SPerlinNoise(u*scale,v*scale)*0.5+0.5;
    float random = perlin;
    /*
    float perlin = SPerlinNoise(u*scale,v*scale);
    
    int seed = toInt(perlin*2048);
    srand(seed);
    float random = rand()/float(RAND_MAX);
    */

    //float random = SWhiteNoise(u*scale,v*scale);
    
    /*
    const int rgbLevel[7]={0xff0000,0x00ff00,0x0000ff,0xffff00,0xff00ff,0x00ffff,0xffffff};
    rgb = rgbLevel[level%7];
    */
#else
    float random = float(rand())/RAND_MAX;
#endif
    
    // Get the noise result    
    float af = a * (1.0f/255) * orig + random * noise;
    
    int ra = toInt(af*255);
    saturate(ra,0,255);
    
    // Write result
    c = (ra<<24)|rgb;
    *d = c;
    d++;
  }

  // Edit alpha so that histogram will be constant instead of gauss
  {
    // Calculate histogram of alpha
    int histogram[256];
    int totalCount = _w * _h;
    {
      // Zero histogram
      for (int i = 0; i < 256; i++) histogram[i] = 0;

      // Add pixels count
      PPixel *d = _argb;
      for (int y = 0; y < _h; y++)
        for (int x = 0; x < _w; x++)
        {
          // Get the pixel value
          int c = *d;

          // Load A
          unsigned int a = (c>>24)&0xff;

          // Increment histogram value
          histogram[a]++;

          // Move to next pixel
          d++;
        }
    }

    // Create the conversion table for each possible value
    int conversion[256];
    {
      // Set impossible value to conversion table (assert during conversion)
      for (int i = 0; i < 256; i++) conversion[i] = -1;

      float sumPixelsInPercent = 0.0f;
      for (int i = 0; i < 256; i++)
      {
        sumPixelsInPercent += (float)histogram[i] / totalCount;
        conversion[i] = toInt(sumPixelsInPercent * 255);
      }
    }

    // Convert the alpha of the texture
    PPixel *d = _argb;
    for (int y = 0; y < _h; y++)
      for (int x = 0; x < _w; x++)
      {
        // Get the pixel value
        int c = *d;

        // Load ARGB variables
        unsigned int a = (c>>24)&0xff;
        unsigned int rgb = c&0xffffff;

        // Write result
        DoAssert(conversion[a] >= 0);
        c = (conversion[a]<<24)|rgb;
        *d = c;

        // Move to next pixel
        d++;
      }
  }

  return true;
}

/**
  add noise to alpha channel:
  maskA = sourceA > 0 ? 1.0f : 0.0f;
  finalA = ( sourceA + perlin ) * 0.5 * maskA;
*/
bool Picture::FilterAddAlphaNoise(int level)
{
  // Convert picture to the ARGB format
  ConvertARGB();
  if( !_argb ) return false;

  srand(1);
  PPixel *d = _argb;
  for (int y = 0; y < _h; y++)
    for (int x = 0; x < _w; x++)
    {
      // Get the pixel value
      int c = *d;

      // Load ARGB variables
      unsigned int a = (c >> 24) & 0xff;
      unsigned int rgb = c & 0xffffff;

      // add noise into a
      float u = x*(1.0f/_w);
      float v = y*(1.0f/_h);
      float scale = 500.0f;

      float perlin = SPerlinNoise(u*scale,v*scale) * 0.5 + 0.5;
      float af;
      if( a > 0 )
        af = ( a * (1.0f/255) + perlin ) * 0.5f;
      else
        af = 0;

      int ra = toInt( af * 255 );
      saturate( ra, 0, 255 );

      // Write result
      c = (ra << 24) | rgb;
      *d = c;
      d++;
    }
  return true;
}

bool Picture::ChangeSizeUniformColor(int w, int h)
{
  if (_w==w && _h==h) return true;
  if (_w<1 || _h<1) return true;

  ConvertARGB();
  if( !_argb ) return false;

  PPixel color = _argb[0];

  PPixel *newData=new PPixel[w*h];
  if (!newData) return false;
  
  delete[] _argb,_argb=newData;
  _w = w;
  _h = h;

  PPixel *d=_argb;
  for( int y=0; y<_h; y++ ) for( int x=0; x<_w; x++ )
  {
    *d++=color;
  }
  
  return true;
}

bool Picture::HalfsizeSRGB()
{
  //if( !_rgbPal ) return 0;
  //if( _w<=2 || _h<=2 ) return 0;
  if( _w&1 || _h&1 ) return false;
  // create next level of mip-map (super-sampled, if possible)
  ConvertARGB();
  if( !_argb ) return false;
  PPixel *half=new PPixel[_w/2*_h/2];
  if( !half ) return false;

  int x,y;
  PPixel *d=half;
  for( y=0; y<_h; y+=2 ) for( x=0; x<_w; x+=2 )
  {
    *d++=CombineSRGB(
      _argb[y*_w+x], _argb[y*_w+x+1],
      _argb[(y+1)*_w+x], _argb[(y+1)*_w+x+1]
    );
  }
  _w/=2;
  _h/=2;
  delete[] _argb,_argb=half;
  
  return true;
}

bool Picture::Halfsize()
{
  //if( !_rgbPal ) return 0;
  //if( _w<=2 || _h<=2 ) return 0;
  if( _w&1 || _h&1 ) return 0;
  // create next level of mip-map (supersampled, if possible)
  ConvertARGB();
  if( !_argb ) return 0;
  PPixel *half=new PPixel[_w/2*_h/2];
  if( !half ) return 0;

  int x,y;
  PPixel *d=half;
  for( y=0; y<_h; y+=2 ) for( x=0; x<_w; x+=2 )
  {
    *d++=Combine(
      _argb[y*_w+x], _argb[y*_w+x+1],
      _argb[(y+1)*_w+x], _argb[(y+1)*_w+x+1]
    );
  }
  _w/=2;
  _h/=2;
  delete[] _argb,_argb=half;
  if( _picData ) delete[] _picData,_picData=NULL;
  
  return 1;
}

bool Picture::HalfsizeAlpha()
{
  //if( !_rgbPal ) return 0;
  //if( _w<=2 || _h<=2 ) return 0;
  if( _w&1 || _h&1 ) return 0;
  // create next level of mip-map (supersampled, if possible)
  ConvertARGB();
  if( !_argb ) return 0;
  PPixel *half=new PPixel[_w/2*_h/2];
  if( !half ) return 0;

  int x,y;
  PPixel *d=half;
  for( y=0; y<_h; y+=2 ) for( x=0; x<_w; x+=2 )
  {
    *d++=CombineAlpha(
      _argb[y*_w+x], _argb[y*_w+x+1],
      _argb[(y+1)*_w+x], _argb[(y+1)*_w+x+1]
    );
  }
  _w/=2;
  _h/=2;
  delete[] _argb,_argb=half;
  if( _picData ) delete[] _picData,_picData=NULL;
  
  return 1;
}

int Picture::HalfsizeX()
{
  //if( !_rgbPal ) return 0;
  if( _w<=2 ) return 0;
  if( _w&1 ) return 0;
  // create next level of mip-map (supersampled, if possible)
  ConvertARGB();
  if( !_argb ) return 0;
  PPixel *half=new PPixel[_w/2];
  if( !half ) return 0;

  int x,y;
  PPixel *d=half;
  for( y=0; y<_h; y++ ) for( x=0; x<_w; x+=2 )
  {
    *d++=Combine(_argb[y*_w+x], _argb[y*_w+x+1]);
  }
  _w/=2;
  delete[] _argb,_argb=half;
  if( _picData ) delete[] _picData,_picData=NULL;
  return 1;
}

int Picture::HalfsizeY()
{
  //if( !_rgbPal ) return 0;
  if( _h<=2 ) return 0;
  if( _h&1 ) return 0;
  // create next level of mip-map (supersampled, if possible)
  ConvertARGB();
  if( !_argb ) return 0;
  PPixel *half=new PPixel[_w*_h/2];
  if( !half ) return 0;

  int x,y;
  PPixel *d=half;
  for( y=0; y<_h; y+=2 ) for( x=0; x<_w; x++ )
  {
    *d++=Combine(_argb[y*_w+x],_argb[(y+1)*_w+x]);
  }
  _h/=2;
  delete[] _argb,_argb=half;
  if( _picData ) delete[] _picData,_picData=NULL;
  
  return 1;
}


int Picture::Doublesize()
{
  // create previous level of mip-map (bilinear interpolation)
  ConvertARGB();
  if( !_argb ) return 0;

  PPixel *doubleS=new PPixel[_w*2*_h*2];
  if( !doubleS ) return 0;

  int x,y;
  for( y=0; y<_h; y++ ) for( x=0; x<_w; x++ )
  {
    PPixel rgb00,rgb01,rgb10,rgb11;
    // borders require special handling
    rgb00=_argb[y*_w+x];
    if( x<_w-1 ) rgb01=_argb[y*_w+x+1];
    else rgb01=rgb00;
    if( y<_h-1 ) rgb10=_argb[(y+1)*_w+x];
    else rgb10=rgb00;
    if( x<_w-1 && y<_w-1 ) rgb11=_argb[(y+1)*_w+x+1];
    else rgb11=rgb00;

    int dstY=y*2;
    int dstX=x*2;
    int dstW=_w*2;

    doubleS[dstY*dstW+dstX]=rgb00;
    doubleS[dstY*dstW+dstX+1]=Combine(rgb00,rgb01);
    doubleS[(dstY+1)*dstW+dstX]=Combine(rgb00,rgb10);
    doubleS[(dstY+1)*dstW+dstX+1]=Combine(rgb00,rgb01,rgb10,rgb11);
  }

  delete[] _argb,_argb=doubleS;
  _w*=2;
  _h*=2;
  if( _picData ) delete[] _picData,_picData=NULL;

  return 1;
}

int Picture::DoublesizeX()
{
  // create previous level of mip-map (bilinear interpolation)
  ConvertARGB();
  if( !_argb ) return 0;
  
  PPixel *doubleS=new PPixel[_w*2*_h];
  if( !doubleS ) return 0;
  
  int x,y;
  for( y=0; y<_h; y++ ) for( x=0; x<_w; x++ )
  {
    PPixel rgb00,rgb01;
    // borders require special handling
    rgb00=_argb[y*_w+x];
    if( x<_w-1 ) rgb01=_argb[y*_w+x+1];
    else rgb01=rgb00;

    int dstY=y;
    int dstX=x*2;
    int dstW=_w*2;

    doubleS[dstY*dstW+dstX]=rgb00;
    doubleS[dstY*dstW+dstX+1]=Combine(rgb00,rgb01);
  }
  
  if( _argb ) delete[] _argb,_argb=NULL;
  if( _picData ) delete[] _picData,_picData=NULL;

  _argb=doubleS;
  _w*=2;
  return 1;
}

int Picture::DoublesizeY()
{
  // create previous level of mip-map (bilinear interpolation)
  ConvertARGB();
  if( !_argb ) return 0;
  
  PPixel *doubleS=new PPixel[_w*_h*2];
  if( !doubleS ) return 0;

  int x,y;
  for( y=0; y<_h; y++ ) for( x=0; x<_w; x++ )
  {
    long rgb00,rgb10;
    // borders require special handling
    rgb00=_argb[y*_w+x];
    if( y<_h-1 ) rgb10=_argb[(y+1)*_w+x];
    else rgb10=rgb00;

    int dstY=y*2;
    int dstX=x;
    int dstW=_w;

    doubleS[dstY*dstW+dstX]=rgb00;
    doubleS[(dstY+1)*dstW+dstX]=Combine(rgb00,rgb10);
  }

  if( _argb ) delete[] _argb,_argb=NULL;
  if( _picData ) delete[] _picData,_picData=NULL;
  
  _argb=doubleS;
  _h*=2;
  
  return 1;
}

static bool XIsPowerOf2( unsigned int x )
{
  while( x>1 )
  {
    if( x&1 ) return false;
    x>>=1;
  }
  return true;
}

bool Picture::IsPowerOf2() const
{
  if( !XIsPowerOf2(_w) ) return false;
  if( !XIsPowerOf2(_h) ) return false;
  return true;
}

void Picture::Crop(int width, int height)
{
  // we cannot crop to a bigger size
  width = intMin(width,_w);
  height = intMin(height,_h);
  // if the result is the same, do nothing
  if (width>=_w && height>=_h) return;
  if (_argb)
  {
    PPixel *argb = new PPixel[width,height];

    for (int y=0; y<height; y++)
    {
      const PPixel *sLine = _argb+_w*y;
      PPixel *tLine = argb+width*y;
      for (int x=0; x<width; x++)
      {
        tLine[x] = sLine[x];
      }
    }
    delete[] _argb;
    _argb = argb;
    _w = width;
    _h = height;
  }
  else
  {
    Fail("Palette crop not implemented");
  }
}

void Picture::Resize( int width, int height )
{
  while( _h<height ) DoublesizeY();
  while( _h>height ) HalfsizeY();
  while( _w<width ) DoublesizeX();
  while( _w>width ) HalfsizeX();
}

int Picture::Blend( const Picture &with, float dstFactor, float srcFactor )
{
  if( with.W()!=W() || with.H()!=H() ) return 0; // no blending possible

  ConvertARGB();
  with.ConvertARGB();
  int x,y;
  // create blended picture in rgb
  //int invW=1.0/_w;
  for( y=0; y<_h; y++ ) for( x=0; x<_w; x++ )
  {
    _argb[y*_w+x]=::Blend(_argb[y*_w+x],with._argb[y*_w+x],dstFactor,srcFactor);
  }
  return 0;
}

void Picture::ModulateAlpha( float factor )
{
  ConvertARGB();
  for( int i=0; i<_w*_h; i++ )
  {
    int col=_argb[i];
    int a=col>>24;
    a=toInt(a*factor);
    if( a<0 ) a=0;if( a>255 ) a=255;
    col&=0xffffff;
    col|=a<<24;
    _argb[i]=col;
  }
}

int Picture::XBlend( Picture &with )
{
  if( with.W()!=W() || with.H()!=H() ) return 0; // no blending possible

  ConvertARGB();
  with.ConvertARGB();
  // this and with must be same dimensions

  int x,y;
  // create blended picture in rgb
  int invW=1.0/_w;
  for( y=0; y<_h; y++ ) for( x=0; x<_w; x++ )
  {
    _argb[y*_w+x]=Interpol(_argb[y*_w+x],with._argb[y*_w+x],x*invW);
  }

  if( _picData ) delete[] _picData,_picData=NULL;
  _lenPal=0;
  
  return 1;
  
}

int Picture::YBlend( Picture &with )
{
  if( with.W()!=W() || with.H()!=H() ) return 0; // no blending possible

  ConvertARGB();
  with.ConvertARGB();

  // this and with must be same dimensions
  
  int x,y;
  // create blended picture in rgb
  float invH=1.0/_h;
  for( y=0; y<_h; y++ ) for( x=0; x<_w; x++ )
  {
    _argb[y*_w+x]=Interpol(_argb[y*_w+x],with._argb[y*_w+x],y*invH);
  }

  // create palette using popularity
  if( _picData ) delete[] _picData,_picData=NULL; 
  _lenPal=0;
  return 1;
  
}

void Picture::SetPixel( int x, int y, PPixel pixel ) const
{
  // if possible use RGB representation
  Assert(_maxColor==0xffffffff);
  if( _argb )
  {
    _argb[y*_w+x]=pixel;
  }
  else
  {
    _rgbPal[_picData[y*_w+x]]=pixel;
  }
}

unsigned int Picture::GetMinColorRaw() const
{
  ConvertARGB();
  int maxR,maxG,maxB,maxA;
  MinRGBARaw(maxR,maxG,maxB,maxA);
  return MakeARGB(maxA,maxR,maxG,maxB);
}
unsigned int Picture::GetMaxColorRaw() const
{
  ConvertARGB();
  int maxR,maxG,maxB,maxA;
  MaxRGBARaw(maxR,maxG,maxB,maxA);
  return MakeARGB(maxA,maxR,maxG,maxB);
}

unsigned int Picture::GetAverageColorRaw() const
{
  ConvertARGB();
  //NormalizeAlpha();
  double rSum=0;
  double gSum=0;
  double bSum=0;
  double aSum=0;
  int n=_w*_h;
  int pTest = 0;
  if( _argb )
  {
    if( !_alphaWithBlack )
    {
      for( int c=0; c<n; c++ )
      {
        int rgb=_argb[c];
        int a=(rgb>>24)&0xff;
        if (a==0) continue; // nothing to add
        float av=a*(1.0/255);
        int r=(rgb>>16)&0xff;
        int g=(rgb>>8)&0xff;
        int b=(rgb>>0)&0xff;
        aSum+=a;
        rSum+=r*av;
        gSum+=g*av;
        bSum+=b*av;
        pTest++;
      }
    }
    else
    {
      for( int c=0; c<n; c++ )
      {
        int rgb=_argb[c];
        int a=(rgb>>24)&0xff;
        int r=(rgb>>16)&0xff;
        int g=(rgb>>8)&0xff;
        int b=(rgb>>0)&0xff;
        aSum+=a;
        rSum+=r;
        gSum+=g;
        bSum+=b;
        pTest++;
      }
    }
  }
  unsigned int a=toInt(aSum/n);
  if( a<0 ) a=0;if( a>255 ) a=255;
  if( aSum<=0 ) return a<<24; // alpha only
  double invASum=255/aSum;
  unsigned int r=toInt(rSum*invASum);
  unsigned int g=toInt(gSum*invASum);
  unsigned int b=toInt(bSum*invASum);
  if( r<0 ) r=0;if( r>255 ) r=255;
  if( g<0 ) g=0;if( g>255 ) g=255;
  if( b<0 ) b=0;if( b>255 ) b=255;
  return (a<<24)|(r<<16)|(g<<8)|(b);
}

unsigned int Picture::GetAverageColorRawSRGB() const
{
  ConvertARGB();
  //NormalizeAlpha();
  double rSum=0;
  double gSum=0;
  double bSum=0;
  double aSum=0;
  int n=_w*_h;
  int pTest = 0;
  if( _argb )
  {
    if( !_alphaWithBlack )
    {
      for( int c=0; c<n; c++ )
      {
        int rgb=_argb[c];
        int a=(rgb>>24)&0xff;
        if (a==0) continue; // nothing to add
        float av=a*(1.0/255);
        int r=(rgb>>16)&0xff;
        int g=(rgb>>8)&0xff;
        int b=(rgb>>0)&0xff;
        aSum+=a;
        rSum+=FromSRGB(r*av);
        gSum+=FromSRGB(g*av);
        bSum+=FromSRGB(b*av);
        pTest++;
      }
    }
    else
    {
      for( int c=0; c<n; c++ )
      {
        int rgb=_argb[c];
        int a=(rgb>>24)&0xff;
        int r=(rgb>>16)&0xff;
        int g=(rgb>>8)&0xff;
        int b=(rgb>>0)&0xff;
        aSum+=a;
        rSum+=FromSRGB(r);
        gSum+=FromSRGB(g);
        bSum+=FromSRGB(b);
        pTest++;
      }
    }
  }
  unsigned int a=toInt(aSum/n);
  if( a<0 ) a=0;if( a>255 ) a=255;
  if( aSum<=0 ) return a<<24; // alpha only
  double invASum=255/aSum;
  unsigned int r=ToSRGB(toInt(rSum*invASum));
  unsigned int g=ToSRGB(toInt(gSum*invASum));
  unsigned int b=ToSRGB(toInt(bSum*invASum));
  if( r<0 ) r=0;if( r>255 ) r=255;
  if( g<0 ) g=0;if( g>255 ) g=255;
  if( b<0 ) b=0;if( b>255 ) b=255;
  return (a<<24)|(r<<16)|(g<<8)|(b);
}

int Picture::SavePalette( QOStream &f )
{
  // save palette
  int i;
  // save average color
  fputiw((word)_lenPal,f); /* poc. barev */
  for( i=0; i<_lenPal; i++ ) fputi24(_rgbPal[i],f);
  //if( f.fail() ) return -1;
  return 0;
}

enum PicFlags
{
  PicFlagAlpha=1,
  PicFlagTransparent=2,
};

int Picture::SaveOffsets( QOStream &f, int *offsets, int nOffsets )
{
  // save offsets tagg
  fputil('TAGG',f);
  fputil('OFFS',f);
  fputil(sizeof(int)*nOffsets,f); // tagg size
  if (!offsets)
  {
    for (int i=0; i<nOffsets; i++) fputil(-1,f); // save temporary placeholder
  }
  else
  {
    for (int i=0; i<nOffsets; i++) fputil(offsets[i],f); // save temporary placeholder
  }
  //if( f.fail() ) return -1;
  return 0;
}

int Picture::LoadTaggs(QIStream &f, TexSwizzle *swizzle)
{
  _averageColor = 0;
  _maxColor = 0xffffffff;
  _procSource = "";
  TexSwizzle swiz[4]={TSAlpha,TSRed,TSGreen,TSBlue};
  for(;;)
  {
    // skip any taggs
    DWORD magic=fgetil(f);
    if( magic!='TAGG' ) {f.seekg(-4,QIOS::cur);break;}
    // some tagg - load it
    DWORD tagg=fgetil(f);
    int size=fgetil(f);
    switch (tagg)
    {
      case 'AVGC':
        if (size==sizeof(_averageColor))
        {
          _averageColor = fgetil(f);
          size -= sizeof(_averageColor);
        }
        break;
      case 'MAXC':
        if (size==sizeof(_maxColor))
        {
          _maxColor = fgetil(f);
          if ((_maxColor&0xff000000)==0)
          {
            // if there is not DR compression for alpha, assume coef. 1
            _maxColor |= 0xff000000;
          }
          size -= sizeof(_maxColor);
        }
        break;
      case 'SWIZ':
        {
          if (size==4*sizeof(char))
          {
            swiz[0]=TexSwizzle(f.get());
            swiz[1]=TexSwizzle(f.get());
            swiz[2]=TexSwizzle(f.get());
            swiz[3]=TexSwizzle(f.get());
            size -= 4*sizeof(char);
          }
        }
        break;
      case 'PROC':
        // procedural source
        Temp<char> buf(size);
        f.read(buf.Data(),size);
        _procSource = RString(buf.Data(),buf.Size());
        size = 0;
        break;
    }
    //Assert( size>=0 );
    f.seekg(size,QIOS::cur);
    if( f.eof() || f.fail() ) break;
  }
  if (swizzle)
  {
    swizzle[0] = swiz[0];
    swizzle[1] = swiz[1];
    swizzle[2] = swiz[2];
    swizzle[3] = swiz[3];
  }
  return 0;
}

int Picture::SaveTaggs( QOStream &f, const TextureHints &hints, TexAlpha texAlpha)
{
  // save AVGC tagg
  if (hints._enableDynRange)
  {
    CalculateAverageColorSRGB();
  }
  else
  {
    CalculateAverageColor();
  }
  fputil('TAGG',f); // tagg magic
  fputil('AVGC',f); // tagg idtf
  fputil(sizeof(_averageColor),f); // tagg size
  fputil(_averageColor,f); // tagg content

  // save MAXC tagg
  //MaximizeDynamicRange();
  fputil('TAGG',f); // tagg magic
  fputil('MAXC',f); // tagg idtf
  fputil(sizeof(_maxColor),f); // tagg size
  fputil(_maxColor,f); // tagg content

  // save flags tagg
  int flags = 0;
  if (IsOpaque()) flags = 0;
  else if (texAlpha == TAForceAlpha) flags |= PicFlagAlpha;
  else if (IsMasked() || (texAlpha == TAForceMasked)) flags |= PicFlagTransparent;
  else flags |= PicFlagAlpha;
  if (flags)
  {
    fputil('TAGG',f);
    fputil('FLAG',f);
    fputil(sizeof(int),f);
    fputil(flags,f);
  }

  // if there is any channel swizzle, save it
  if (
    hints._virtualSwizzle && (
      hints._channelSwizzle[0]!=0 || hints._channelSwizzle[1]!=1 ||
      hints._channelSwizzle[2]!=2 || hints._channelSwizzle[3]!=3
    )
  )
  {
    fputil('TAGG',f);
    fputil('SWIZ',f);
    fputil(4*sizeof(char),f);
    f.put(hints._channelSwizzle[0]);
    f.put(hints._channelSwizzle[1]);
    f.put(hints._channelSwizzle[2]);
    f.put(hints._channelSwizzle[3]);
  }

  if (_procSource.GetLength()>0)
  {
    fputil('TAGG',f);
    fputil('PROC',f);
    fputil(_procSource.GetLength(),f);
    f.write(_procSource,_procSource.GetLength());
  }
  return 0;
}

int Picture::SaveNulPalette( QOStream &f )
{
  // save palette
  fputiw(0,f); /* poc. barev */
  //if( f.fail() ) return -1;
  return 0;
}

int Picture::SaveData( QOStream &f, bool lzw )
{
  // save image size
  if( lzw )
  {
    // save magic to distinguish lzw compressed pac files
    fputiw(MAGIC_W_LZW,f);
    fputiw(MAGIC_H_LZW,f);
  }
  fputiw((word)_w,f);
  fputiw((word)_h,f);
  
  // use string stream to store complete compressed data
  QOStrStream str;
  //str.binary();
  if( lzw ) SavePACLZW(str,_picData,_w*_h);
  else SavePACB(str,_picData,_w*_h);
  
  // store compressed size
  int mipLen=str.pcount();
  const char *mipData=str.str();
  if( !mipData ) return -1;
  fputi24(mipLen,f);
  f.write(mipData,mipLen);
  return 0;
}

int Picture::SaveData4444( QOStream &f, const TextureHints &hints )
{
  // save image size
  fputiw((word)_w,f);
  fputiw((word)_h,f);

  // use string stream to store complete compressed data
  QOStrStream str;
  Temp<word> d4444(_w*_h);
  if (hints._dithering)
  {
    Convert8888To4444(d4444,_argb,_w,_h);
  }
  else
  {
    Convert8888To4444NoDither(d4444,_argb,_w,_h);
  }

  SavePACLZW(str,(byte *)(word *)d4444,_w*_h*2);
  //else SavePACW(str,(word *)_picData,_w*_h*2);
  
  // store compressed size
  int mipLen=str.pcount();
  const char *mipData=str.str();
  if( !mipData ) return -1;
  fputi24(mipLen,f);
  f.write(mipData,mipLen);
  return 0;
}

int Picture::SaveData1555( QOStream &f, const TextureHints &hints )
{
  // save image size
  fputiw((word)_w,f);
  fputiw((word)_h,f);
  
  // use string stream to store complete compressed data
  QOStrStream str;

  Temp<word> d16(_w*_h);
  if (hints._dithering)
  {
    Convert8888To1555(d16,_argb,_w,_h);
  }
  else
  {
    Convert8888To1555NoDither(d16,_argb,_w,_h);
  }

  SavePACLZW(str,(byte *)(word *)d16,_w*_h*2);
  
  // store compressed size
  int mipLen=str.pcount();
  const char *mipData=str.str();
  if( !mipData ) return -1;
  fputi24(mipLen,f);
  f.write(mipData,mipLen);
  return 0;
}

// note: 5, 9, 2 is quite good approximation (and gives sum 16)

/*
const int REyeI = 0.299f*256;
const int GEyeI = 0.587f*256;
const int BEyeI = 0.114f*256;
*/

static int REyeI = 5;
static int GEyeI = 9;
static int BEyeI = 2;

/// setup color error coefficients for color map compression
static void SelectColorError()
{
  REyeI = 5;
  GEyeI = 9;
  BEyeI = 2;
}

/// setup color error coefficients for 3-component normal map compression
static void SelectDistanceError()
{
  REyeI = 5;
  GEyeI = 5;
  BEyeI = 5;
}

/// setup color error coefficients for 2-component normal map compression
/** note: it is not necessary to use this, as there is no error in components which are not used */
static void SelectDistanceXYError()
{
  REyeI = 5;
  GEyeI = 5;
  BEyeI = 0;
}

static void SelectErrorMode(TexErrorMode mode)
{
  switch (mode)
  {
    default:
    case TexErrEye: SelectColorError(); break;
    case TexErrDistance: SelectDistanceError(); break;
    case TexErrDistanceXY: SelectDistanceXYError(); break;
  }
}

inline int Distance8(int a,int b)
{
  return abs(a-b);
}

inline int Distance8888(DWORD a,DWORD b)
{
  return
  (
    abs(int(GetR(a)-GetR(b)))*REyeI+ 
    abs(int(GetG(a)-GetG(b)))*GEyeI+
    abs(int(GetB(a)-GetB(b)))*BEyeI
  );
}

inline int SquareDistance8888(DWORD a,DWORD b)
{
  return
  (
    int(GetR(a)-GetR(b))*int(GetR(a)-GetR(b))*REyeI*REyeI+ 
    int(GetG(a)-GetG(b))*int(GetG(a)-GetG(b))*GEyeI*GEyeI+
    int(GetB(a)-GetB(b))*int(GetB(a)-GetB(b))*BEyeI*BEyeI
  );
}

// Euclidean distance seems to make more sense than a sum 
#define FUNC_DISTANCE8888 SquareDistance8888

inline int Brightness8888(DWORD a)
{
  return
  (
    GetR(a)*REyeI+ 
    GetG(a)*GEyeI+
    GetB(a)*BEyeI
  );
}

static void HandleSameColors(DWORD &c0, DWORD &c1)
{
  // it has no sense to have the same color twice
  // try to make it a little bit brighter, but maintain the same color
  // check if it is zero or very close to it
  if ((c0&0xffdf)==0)
  {
    c1 = (1<<11)|(1<<6)|1;
  }
  else
  {
    int r = (c0>>11)&0x1f;
    int g = (c0>>5)&0x3f;
    int b = (c0>>0)&0x1f;
    int maxRGB = r;
    if (maxRGB<b) maxRGB = b;
    if (maxRGB<(g>>1)) maxRGB = (g>>1);
    float ratio = float(maxRGB+1)/maxRGB;
    int rn = toInt(r*ratio);
    int gn = toInt(g*ratio);
    int bn = toInt(b*ratio);
    saturate(rn,0,0x1f);
    saturate(gn,0,0x3f);
    saturate(bn,0,0x1f);

    c1 = (rn<<11)|(gn<<5)|bn;
  }

  // make sure c1 is different from c0
  if (c0==c1)
  {
    c1 = 0xffff;
    if (c0==c1) c1 = 0;
  }
}

struct ColorCount
{
  int col;
  int count;
  ColorCount(){col=0;count=0;}
  explicit ColorCount(int c, int cc=1){col=c,count=cc;}
};

static void AddColor(ColorCount *colors, int &nColors, int col, int count=1)
{
  for (int c=0; c<nColors; c++)
  {
    if (col==colors[c].col)
    {
      colors[c].count+=count;
      return;
    }
  }
  colors[nColors] = ColorCount(col,count);
  nColors++;
}


typedef void DXTSelectColors(
  DWORD &c0, DWORD &c1, const ColorCount *colors, int nColors
);

static void DXTColorsMinMax( DWORD &c0, DWORD &c1, const ColorCount *colors, int nColors)
{
  // min max all brightness

  DWORD min = 0, max =0;
  int minB = INT_MAX, maxB = -1;
  //Brightness8888
  for (int i=0; i<nColors; i++)
  {
    DWORD pix = colors[i].col;
    int b = Brightness8888(pix);
    if (b<minB) minB=b, min = pix;
    if (b>maxB) maxB=b, max = pix;
  }
  c0 = min;
  c1 = max;
}

static void DXTColorsMinMaxRGB(DWORD &c0, DWORD &c1, const ColorCount *colors, int nColors)
{
  // min max all components independently
  int minR = 255, maxR = 0;
  int minG = 255, maxG = 0;
  int minB = 255, maxB = 0;
  for (int i=0; i<nColors; i++)
  {
    DWORD pix = colors[i].col;
    int r = GetR(pix);
    int g = GetG(pix);
    int b = GetB(pix);
    saturateMin(minR,r), saturateMax(maxR,r);
    saturateMin(minG,g), saturateMax(maxG,g);
    saturateMin(minB,b), saturateMax(maxB,b);
  }
  c0 = 0xff000000|(minR<<16)|(minG<<8)|minB;
  c1 = 0xff000000|(maxR<<16)|(maxG<<8)|maxB;
}

static void DXTColorsMinMaxSoft(DWORD &c0, DWORD &c1, const ColorCount *colors, int nColors)
{
  // select any of 16 pixels
  if (nColors<=0)
  {
    c0 = 0xff000000;
    c1 = 0xffffffff;
    return;
  }
  c0 = Convert8888To565(colors[0].col)&0xffdf;
  c1 = c0;
  HandleSameColors(c0,c1);

  c0 = Convert565To8888(c0);
  c1 = Convert565To8888(c1);
}

static void DXTAvgColor(DWORD &c0, const ColorCount *colors, int nColors)
{
  // keep brightness but convert to same color tone
  // track min and max brigtness
  int sumR = 0, sumG = 0, sumB = 0;
  int pixels = 0;
  for (int i=0; i<nColors; i++)
  {
    DWORD pix = colors[i].col;
    int count = colors[i].count;
    sumR += GetR(pix)*count;
    sumG += GetG(pix)*count;
    sumB += GetB(pix)*count;
    pixels += count;
  }

  float avgFactor = 1.0f/pixels;

  int avgR = toInt(avgFactor*sumR);
  int avgG = toInt(avgFactor*sumG);
  int avgB = toInt(avgFactor*sumB);
  saturate(avgR,0,255);
  saturate(avgG,0,255);
  saturate(avgB,0,255);

  c0 = MakeRGB(avgR,avgG,avgB);
}

static void DXTColorsMonocolor(DWORD &c0, DWORD &c1, const ColorCount *colors, int nColors)
{
  // keep brightness but convert to same color tone
  // track min and max brigtness
  int sumR = 0, sumG = 0, sumB = 0;
  int minBrightness = INT_MAX;
  int maxBrightness = 0;
  //int pixels = 0;
  for (int i=0; i<nColors; i++)
  {
    DWORD pix = colors[i].col;
    int count = colors[i].count;
    sumR += GetR(pix)*count;
    sumG += GetG(pix)*count;
    sumB += GetB(pix)*count;
    //pixels += count;
    int brightness = Brightness8888(pix);
    if (minBrightness>brightness) minBrightness = brightness;
    if (maxBrightness<brightness) maxBrightness = brightness;
  }
  // normalize brightness
  // calculate total brightness of average color
  float sumBrightness = float(sumR)*REyeI+float(sumG)*GEyeI+float(sumB)*BEyeI;
  if (sumBrightness<1e-3 && nColors>0)
  {
    // pick any color with zero alpha to represent transparent areas
    for (int i=0; i<nColors; i++)
    {
      if (colors[i].count>0) continue;
      DWORD pix = colors[i].col;
      sumR += GetR(pix);
      sumG += GetG(pix);
      sumB += GetB(pix);
      int brightness = Brightness8888(pix);
      if (minBrightness>brightness) minBrightness = brightness;
      if (maxBrightness<brightness) maxBrightness = brightness;
    }
    sumBrightness = float(sumR)*REyeI+float(sumG)*GEyeI+float(sumB)*BEyeI;
  }
  if (sumBrightness<=0.1) sumBrightness = 0.1;
  // create a color which has brightness, minBrightness but the same color as sumR, sumG, sumB

  float minFactor = minBrightness/sumBrightness;
  float maxFactor = maxBrightness/sumBrightness;

  int minR = toInt(minFactor*sumR);
  int minG = toInt(minFactor*sumG);
  int minB = toInt(minFactor*sumB);
  saturate(minR,0,255);
  saturate(minG,0,255);
  saturate(minB,0,255);

  int maxR = toInt(maxFactor*sumR);
  int maxG = toInt(maxFactor*sumG);
  int maxB = toInt(maxFactor*sumB);

  saturate(maxR,0,255);
  saturate(maxG,0,255);
  saturate(maxB,0,255);

  c0 = MakeRGB(minR,minG,minB);
  c1 = MakeRGB(maxR,maxG,maxB);
}

static int CmpColorCount( const void *c0, const void *c1 )
{
  const ColorCount *cc0 = static_cast<const ColorCount *>(c0);
  const ColorCount *cc1 = static_cast<const ColorCount *>(c1);
  return cc1->count-cc0->count;
}

static int CmpColorValue( const void *c0, const void *c1 )
{
  const ColorCount *cc0 = static_cast<const ColorCount *>(c0);
  const ColorCount *cc1 = static_cast<const ColorCount *>(c1);
  return cc1->col-cc0->col;
}
/*
static void DXTColorsPopular(
  DWORD &c0, DWORD &c1, const ColorCount *colors, int nColors
)
{
  // select two most popular colors
  // provide default values
  c0 = 0xffffffff;
  c1 = 0xff000000;
  // select two most common colors
  if (nColors>0)
  {
    c0 = Convert1555To8888(colors[0].col);
    if (nColors>1) c1 = Convert1555To8888(colors[1].col);
  }
}
*/

static void DXTColorsMostDistant(
  DWORD &c0, DWORD &c1, const ColorCount *colors, int nColors
)
{
  // select two most popular colors
  // provide default values
  c0 = 0xffffffff;
  c1 = 0xff000000;
  // select two most distant colors
  int maxDist = 0;
  int maxI = 0, maxJ = 0;
  for (int i=0; i<nColors; i++)
  for (int j=0; j<i; j++)
  {
    int distance = FUNC_DISTANCE8888(colors[i].col,colors[j].col);
    if (distance>maxDist)
    {
      maxDist = distance;
      maxI = i;
      maxJ = j;
    }
  }
  c0 = colors[maxI].col;
  c1 = colors[maxJ].col;
}

static DXTSelectColors *DXTSCFunctions[]=
{
  //DXTColorsMostDistant,
  //DXTColorsMinMax,
  DXTColorsMonocolor,
  //DXTColorsMinMaxRGB,
  //DXTColorsPopular,
  //DXTColorsMinMaxSoft
};


const int NDXTSCFunctions = sizeof(DXTSCFunctions)/sizeof(*DXTSCFunctions);

const int DXTSCFunctionExtTransp = NDXTSCFunctions;
const int DXTSCFunctionExtSetBForce = NDXTSCFunctions+1;
const int DXTSCFunctionExtSetBForce3 = NDXTSCFunctions+2;

const int DXTSCFunctionExtSetBForceMM = NDXTSCFunctions+3;
const int DXTSCFunctionExtSetBForceMM3 = NDXTSCFunctions+4;

const int DXTSCFunctionExtImproveBForce = NDXTSCFunctions+5;

const int NDXTSCFunctionsExt = NDXTSCFunctions+6;

#ifdef _DEBUG
#define COUNT_FUNC 1
#endif

#if COUNT_FUNC
static int DXTSCFunctionUsed[NDXTSCFunctionsExt];
#endif

static void CompressDXTABlock(DXTBlock64AlphaExplicit &tgt, const DWORD *pixels)
{
  for (int i=0; i<4; i++)
  {
    word res = (
      ((GetA(pixels[i*4+0])>>4)<<0)|
      ((GetA(pixels[i*4+1])>>4)<<4)|
      ((GetA(pixels[i*4+2])>>4)<<8)|
      ((GetA(pixels[i*4+3])>>4)<<12)
    );
    tgt.a[i] = res;
  }
}

static double CalcErrorNearestAlpha(
  const ColorCount *source, int nSource, const int *alpha, int nAlphas,
  double maxError
)
{
  // scan all source colors and select nearest color for each
  double error = 0;
  for (int i=0; i<nSource; i++)
  {
    // if pixel is tranparent, use transparency
    int p = source[i].col;
    int nearest = 0;
    int nearestDist = INT_MAX;
    for (int c=0; c<nAlphas; c++)
    {
      int dist = Distance8(alpha[c],p);
      if (nearestDist>dist)
      {
        nearestDist = dist;
        nearest = c;
      }
    }
    error += nearestDist*source[i].count;
    // if we are have better solution, there is no need to calculate error any longer
    if (error>=maxError) return error;
  }
  return error;
}

static void SelectNearestAlpha(int *codes,const DWORD *pixels,const int *alpha)
{
  // alpha count is always eight
  // scan all pixels and select nearest color
  for (int i=0; i<16; i++)
  {
    // if pixel is tranparent, use transparency
    int a = GetA(pixels[i]);
    int nearest = 0;
    int nearestDist = INT_MAX;
    for (int c=0; c<8; c++)
    {
      int dist = Distance8(alpha[c],a);
      if (nearestDist>dist)
      {
        nearestDist = dist;
        nearest = c;
      }
    }
    codes[i] = nearest;
  }
}


static void CompressDXTABlock(DXTBlock64AlphaImplicit &tgt, const DWORD *pixels)
{
  // scan how many unique alpha values are present here
  ColorCount alphas[16];
  int nAlphas = 0;
  for (int i=0; i<16; i++)
  {
    int a = GetA(pixels[i]);
    AddColor(alphas,nAlphas,a);
  }
  double bestError = DBL_MAX;

  qsort(alphas,nAlphas,sizeof(*alphas),CmpColorValue);
  Assert(nAlphas>=1);

  // provide some reasonable default values
  int bestA0 = alphas[nAlphas-1].col;
  int bestA1 = alphas[0].col;
  
  // Brute force between existing colors
  // it has no sense to have less then 2 colors
  for (int endAlphas=nAlphas; endAlphas>=2; endAlphas--)
  for (int startAlphas=0; startAlphas<=endAlphas-2; startAlphas++)
  {
    int a0 = alphas[startAlphas].col;
    int a1 = alphas[endAlphas-1].col;
    if (a0==a1)
    {
      if (a0==0xff) a1--;
      else a0++;
    }
    Assert(a0>a1);
    int alpha[8];
    // try 8 colors first
    Create8Alphas8(alpha,a0,a1);
    double error = CalcErrorNearestAlpha(alphas,nAlphas,alpha,8,bestError);
    if (bestError>error)
    {
      bestA0 = a0;
      bestA1 = a1;
      bestError = error;
    }
    // try 6 colors
    Create8Alphas6(alpha,a1,a0);
    error = CalcErrorNearestAlpha(alphas,nAlphas,alpha,8,bestError);
    if (bestError>error)
    {
      bestA0 = a1;
      bestA1 = a0;
      bestError = error;
    }
    // we cannot be better than best
    if (bestError<=0) goto BestFound;
    
  }
  BestFound:
  // select nearest colors

  int alpha[8];
  if (bestA0>bestA1)
  {
    Create8Alphas8(alpha,bestA0,bestA1);
  }
  else
  {
    Create8Alphas6(alpha,bestA0,bestA1);
  }
  int codes[16];
  SelectNearestAlpha(codes,pixels,alpha);
  // encode implicit block
  tgt.a0 = bestA0;
  tgt.a1 = bestA1;
//0 [0][2] (2 LSBs), [0][1], [0][0] 
  tgt.tex[0] = (codes[0])|(codes[1]<<3)|(codes[2]<<6);
//1 [1][1] (1 LSB), [1][0], [0][3], [0][2] (1 MSB) 
  tgt.tex[1] = (codes[2]>>2)|(codes[3]<<1)|(codes[4]<<4)|(codes[5]<<7);
//2 [1][3], [1][2], [1][1] (2 MSBs) 
  tgt.tex[2] = (codes[5]>>1)|(codes[6]<<2)|(codes[7]<<5);

//3 [2][2] (2 LSBs), [2][1], [2][0] 
  tgt.tex[3] = (codes[8])|(codes[9]<<3)|(codes[10]<<6);
//4 [3][1] (1 LSB), [3][0], [2][3], [2][2] (1 MSB) 
  tgt.tex[4] = (codes[10]>>2)|(codes[11]<<1)|(codes[12]<<4)|(codes[13]<<7);
//5 [3][3], [3][2], [3][1] (2 MSBs) 
  tgt.tex[5] = (codes[13]>>1)|(codes[14]<<2)|(codes[15]<<5);
}

// note: source colors are 888 colors, not 565 candidates
static double CalcErrorNearest(
  const ColorCount *source, int nSource, const DWORD *color, int nColors,
  double maxError
)
{
  //ADD_COUNTER(clErN,nSource*nColors);
  // scan all source colors and select nearest color for each
  double error = 0;
  int dCalc = 0;
  for (int i=0; i<nSource; i++)
  {
    // if pixel is transparent, use transparency
    DWORD p = source[i].col;
    int nearest = 0;
    int nearestDist = INT_MAX;
    for (int c=0; c<nColors; c++)
    {
      dCalc++;
      int dist = FUNC_DISTANCE8888(color[c],p);
      if (nearestDist>dist)
      {
        nearestDist = dist;
        nearest = c;
        //if (nearestDist<=0) break;
      }
    }
    error += nearestDist*source[i].count;
    // if we are have better solution, there is no need to calculate error any longer
    if (error>=maxError)
    {
      //ADD_COUNTER(clErI,(i+1)*nColors);
      //ADD_COUNTER(clErD,dCalc);
      return error;
    }
  }
  //ADD_COUNTER(clErI,nSource*nColors);
  //ADD_COUNTER(clErD,dCalc);
  return error;
}

static void SelectNearest(int *codes,const DWORD *pixels, const DWORD *color, int nColors, bool alpha)
{
  // scan all pixels and select nearest color
  for (int i=0; i<16; i++)
  {
    // if pixel is transparent, use transparency
    int p = pixels[i];
    if (!alpha && GetA(p)<128)
    {
      codes[i] = 3;
    }
    else
    {
      int nearest = 0;
      int nearestDist = INT_MAX;
      for (int c=0; c<nColors; c++)
      {
        int dist = FUNC_DISTANCE8888(color[c],p);
        if (nearestDist>dist)
        {
          nearestDist = dist;
          nearest = c;
        }
      }
      codes[i] = nearest;
    }
  }
}


static void Create4Colors(DWORD *color, DWORD c0RGB, DWORD c1RGB)
{
  int rc0 = GetR(c0RGB);
  int gc0 = GetG(c0RGB);
  int bc0 = GetB(c0RGB);
  int rc1 = GetR(c1RGB);
  int gc1 = GetG(c1RGB);
  int bc1 = GetB(c1RGB);

  int rd3 = Div3(rc1-rc0);
  int gd3 = Div3(gc1-gc0);
  int bd3 = Div3(bc1-bc0);
  //color[2] = c0 + (c1-c0)/3; // (2 * c0 + c1) / 3;
  //color[3] = c1 - (c1-c0)/3; // (c0 + 2 * c1) / 3;
  // convert 565 to 1555
  // verify c2, c3 components in range
  int rc2 = rc0+rd3;
  int gc2 = gc0+gd3;
  int bc2 = bc0+bd3;

  int rc3 = rc1-rd3;
  int gc3 = gc1-gd3;
  int bc3 = bc1-bd3;
  int c2RGB = MakeRGB(rc2,gc2,bc2);
  int c3RGB = MakeRGB(rc3,gc3,bc3);
  color[0]= c0RGB|0xff000000;
  color[1]= c1RGB|0xff000000;
  color[2]= c2RGB|0xff000000;
  color[3]= c3RGB|0xff000000;
}

static void Create3Colors(DWORD *color, DWORD c0RGB, DWORD c1RGB)
{
  int rc0 = GetR(c0RGB);
  int gc0 = GetG(c0RGB);
  int bc0 = GetB(c0RGB);
  int rc1 = GetR(c1RGB);
  int gc1 = GetG(c1RGB);
  int bc1 = GetB(c1RGB);

  DWORD c2RGB = MakeRGB((rc0+rc1)>>1,(gc0+gc1)>>1,(bc0+bc1)>>1);

  color[0] = c0RGB|0xff000000;
  color[1] = c1RGB|0xff000000;
  color[2] = c2RGB|0xff000000;
}


static double CompressDXT1Block(DXTBlock64 &tgt, const DWORD *pixels, bool alpha=false )
{
  // we may use different c0,c1 selection algorithms
  // use 565 colors
  // TODO: try different algorithms and select the one with minimum error

  DWORD bestC0=0, bestC1=0;
  int bestCodes[16];
  double bestError = DBL_MAX;
  int bestAlg = 0;

  bool transparent = false;
  bool transparentOnly = true;
  if (!alpha) for (int i=0; i<16; i++)
  {
    DWORD col = pixels[i];
    int a = GetA(col);
    if (a<128) transparent = true;
    else transparentOnly = false;
  }

  // calculate color counts
  ColorCount colors[16];
  int nColors = 0;
  for (int i=0; i<16; i++)
  {
    // check if color is already present
    DWORD pix = pixels[i];
    // do not count transparent pixels
    if (!alpha && GetA(pix)<0x80) continue;
    AddColor(colors,nColors,pix);
  }

  if (!alpha && transparent && transparentOnly)
  {
    // scan the block as if it is not transparent
    // use mono-color - this will get us the average color
    colors[0].col = pixels[0];
    colors[0].count = 1;
    nColors = 1;
    DWORD c0,c1;
    DXTColorsMonocolor(c0,c1,colors,nColors);

    c0 = Convert8888To565(c0);
    c1 = Convert8888To565(c1);
    // note: do indicate transparency we must have: c0<=c1
    if (c0>c1)
    {
      DWORD t=c0;c0=c1;c1=t;
    }

    bestC0 = c0;
    bestC1 = c1;

    for (int i=0; i<16; i++)
    {
      // all pixels transparent - use transparency
      bestCodes[i] = 3;
    }
    bestAlg = DXTSCFunctionExtTransp;
  }
  else
  {
    // sort colors so that the most common are first
    qsort(colors,nColors,sizeof(*colors),CmpColorCount);

    // only unique 565 colors need to be considered for brute force
    ColorCount colors565[16];
    int nColors565 = 0;
    for (int i=0; i<nColors; i++)
    {
      int color565 = Convert8888To565(colors[i].col);
      AddColor(colors565,nColors565,color565,colors[i].count);
    }

    #if 1
    // check simple heuristic algorithms
    for (int alg=0; alg<NDXTSCFunctions; alg++)
    {
      if (bestError<=0) break;

      DXTSelectColors *func = DXTSCFunctions[alg];

      DWORD c0RGB,c1RGB;
      func(c0RGB,c1RGB,colors,nColors);
      
      DWORD c0 = Convert8888To565(c0RGB);
      DWORD c1 = Convert8888To565(c1RGB);

//      if (c0==c1)
//      {
//        HandleSameColors(c0,c1,c0RGB,c1RGB);
//      }

      // convert back again - source colors need to be exact
      c0RGB = Convert565To8888(c0);
      c1RGB = Convert565To8888(c1);
      // 
      // scan pixels and always select nearest color of 3 or 4 colors used
      // 
      // check for transparency
      // if the two colors are the same, we need to handle the block as transparent anyway

      // calculate error
      double error = 0;

      if (transparent)
      {
        // 3 colors + transparency
        DWORD color[4];

        // note: do indicate transparency we must have: c0<=c1
        if (c0>c1)
        {
          DWORD t=c0;c0=c1;c1=t;
          DWORD tRGB=c0RGB;c0RGB=c1RGB;c1RGB=tRGB;
        }

        Create3Colors(color,c0RGB,c1RGB);

        error = CalcErrorNearest(colors,nColors,color,3,bestError);
      }
      else
      {
        // note: to indicate opacity we must have: c0>c1
        if (c0<c1)
        {
          DWORD t=c0;c0=c1;c1=t;
          DWORD tRGB=c0RGB;c0RGB=c1RGB;c1RGB=tRGB;
        }

        DWORD color[4];
        Create4Colors(color,c0RGB,c1RGB);

        error = CalcErrorNearest(colors,nColors,color,4,bestError);
      }
      if (bestError>error)
      {
        bestError = error;
        bestC0 = c0;
        bestC1 = c1;
        bestAlg = alg;
      }
    }
    #endif
    
    // brute force between used colors
    #if 1
    if (bestError>0)
    {
      qsort(colors565,nColors565,sizeof(*colors565),CmpColorCount);
      // start with the most popular colors
      ADD_COUNTER(clErM,16*16*16*7/2);
      ADD_COUNTER(clErE,nColors565*nColors565*nColors565*7/2);
      for (int i=0; i<nColors565; i++) for (int j=0; j<=i; j++)
      {
        DWORD c0 = colors565[i].col;
        DWORD c1 = colors565[j].col;

//        if (c0==c1)
//        {
//          HandleSameColors(c0,c1);
//        }

        DWORD c0RGB = Convert565To8888(c0);
        DWORD c1RGB = Convert565To8888(c1);

        DWORD color[4];
        int nc;
        if (transparent)
        {
          // note: do indicate transparency we must have: c0<=c1
          if (c0>c1)
          {
            DWORD t=c0;c0=c1;c1=t;
            DWORD tRGB=c0RGB;c0RGB=c1RGB;c1RGB=tRGB;
          }
          Create3Colors(color,c0RGB,c1RGB);
          nc = 3;
        }
        else
        {
          // note: to indicate opacity we must have: c0>c1
          if (c0<c1)
          {
            DWORD t=c0;c0=c1;c1=t;
            DWORD tRGB=c0RGB;c0RGB=c1RGB;c1RGB=tRGB;
          }
          Create4Colors(color,c0RGB,c1RGB);
          nc = 4;
        }

        double error = CalcErrorNearest(colors,nColors,color,nc,bestError);
        if (bestError>error)
        {
          bestError = error;
          bestC0 = c0;
          bestC1 = c1;
          bestAlg = DXTSCFunctionExtSetBForce;
        }
        // sometimes we might get better result with 3-point approximation
        if (!alpha && nc==4)
        {
          // note: do indicate transparency we must have: c0<=c1
          if (c0>c1)
          {
            DWORD t=c0;c0=c1;c1=t;
            DWORD tRGB=c0RGB;c0RGB=c1RGB;c1RGB=tRGB;
          }
          Create3Colors(color,c0RGB,c1RGB);
          double error = CalcErrorNearest(colors,nColors,color,3,bestError);
          if (bestError>error)
          {
            // we need to indicate transparency
            bestError = error;
            bestC0 = c0;
            bestC1 = c1;
            bestAlg = DXTSCFunctionExtSetBForce3;
          }
        }
        if (bestError<=0) goto BestFound;

      }
      BestFound:;
    }
    #endif

    #if 0
    if (bestError>0)
    {
      // brute force in the min-max box
      // or around average color
      // this leads to better results sometimes, but it can be extemely slow
      DWORD cMin,cMax;
      DXTColorsMinMaxRGB(cMin,cMax,colors,nColors);
      int minR = GetR(cMin)>>3;
      int minG = GetG(cMin)>>2;
      int minB = GetB(cMin)>>3;
      int maxR = (GetR(cMax)+7)>>3;
      int maxG = (GetG(cMax)+3)>>2;
      int maxB = (GetB(cMax)+7)>>3;
      int count = (maxR-minR+1) * (maxG-minG+1) * (maxB-minB+1);

      // if the search area is too large, do not search
      int step = 1;
      /*
      int maxSize = 2;
      int maxCSize = maxSize*2+1;
      if (count>maxCSize*maxCSize*maxCSize)
      {
        DWORD cAvg;
        DXTAvgColor(cAvg,colors,nColors);
        if (maxR-minR+1>maxCSize)
        {
          int avgR = GetR(cAvg)>>3;
          minR = avgR - maxSize; maxR = avgR + maxSize;
          if (minR<0) minR=0;
          if (maxR>0x1f) maxR=0x1f;
        }

        if (maxG-minG+1>maxCSize)
        {
          int avgG = GetG(cAvg)>>2;
          minG = avgG - maxSize; maxG = avgG + maxSize;
          if (minG<0) minG=0;
          if (maxG>0x3f) maxG=0x3f;
        }

        if (maxB-minB+1>maxCSize)
        {
          int avgB = GetB(cAvg)>>3;
          minB = avgB - maxSize; maxB = avgB + maxSize;
          if (minB<0) minB=0;
          if (maxB>0x1f) maxB=0x1f;
        }
      }
      */

      for (int r0=minR; r0<=maxR; r0+=step)
      for (int g0=minG; g0<=maxG; g0+=step)
      for (int b0=minB; b0<=maxB; b0+=step)
      for (int r1=minR; r1<=r0; r1+=step)
      for (int g1=minG; g1<=g0; g1+=step)
      for (int b1=minB; b1<=b0; b1+=step)
      {
        DWORD c0 = (r0<<10)|(g0<<5)|b0;
        DWORD c1 = (r1<<10)|(g1<<5)|b1;

//        if (c0==c1)
//        {
//          HandleSameColors(c0,c1);
//        }

        // TODO: handle transparency
        DWORD c0RGB = Convert565To8888(c0);
        DWORD c1RGB = Convert565To8888(c1);

        DWORD color[4];
        int nc;
        if (transparent)
        {
          // note: do indicate transparency we must have: c0<=c1
          if (c0>c1)
          {
            DWORD t=c0;c0=c1;c1=t;
            DWORD tRGB=c0RGB;c0RGB=c1RGB;c1RGB=tRGB;
          }
          Create3Colors(color,c0RGB,c1RGB);
          nc = 3;
        }
        else
        {
          // note: to indicate opacity we must have: c0>c1
          if (c0<c1)
          {
            DWORD t=c0;c0=c1;c1=t;
            DWORD tRGB=c0RGB;c0RGB=c1RGB;c1RGB=tRGB;
          }
          Create4Colors(color,c0RGB,c1RGB);
          nc = 4;
        }

        double error = CalcErrorNearest(colors,nColors,color,nc,bestError);
        if (bestError>error)
        {
          // note: to indicate opacity we must have: c0>c1
          bestError = error;
          bestC0 = c0;
          bestC1 = c1;
          bestAlg = DXTSCFunctionExtSetBForceMM;
        }
        // sometimes we might get better result with 3-point approximation
        if (!alpha && nc==4)
        {
          // note: do indicate transparency we must have: c0<=c1
          if (c0>c1)
          {
            DWORD t=c0;c0=c1;c1=t;
            DWORD tRGB=c0RGB;c0RGB=c1RGB;c1RGB=tRGB;
          }
          Create3Colors(color,c0RGB,c1RGB);
          double error = CalcErrorNearest(colors,nColors,color,3,bestError);
          if (bestError>error)
          {
            bestError = error;
            bestC0 = c0;
            bestC1 = c1;
            bestAlg = DXTSCFunctionExtSetBForceMM3;
          }
        }
        if (bestError<=0) goto BestFound2;
      }
      BestFound2:;
    }

    #endif
  }

  // try to improve the result by using brute force search around endpoints
  #if 1
  {
    // bectC0 and bestC1 are 565 encoded
    int rc0 = (bestC0>>11)&0x1f;
    int gc0 = (bestC0>>5)&0x3f;
    int bc0 = (bestC0>>0)&0x1f;
    int rc1 = (bestC1>>11)&0x1f;
    int gc1 = (bestC1>>5)&0x3f;
    int bc1 = (bestC1>>0)&0x1f;
    

    // simple brute force would give:
    //   3x3x3 gives 27 possibilities for one two endpoint
    //   729 possibilities in total - far more then 16 colors brute force
    // by extending range only we get:
    //   2*2*2=8 possibilities for each endpoint
    //   64 possibilities in total
    int rRange = 1;
    int gRange = 1;
    int bRange = 1;

    bool improved = false;
    
    // only extend range, never lower it
    int r0Min = rc0<=rc1 ? rc0-rRange : rc0;
    int r0Max = rc0>rc1 ? rc0+rRange : rc0;
    int r1Min = rc1<rc0 ? rc1-rRange : rc1;
    int r1Max = rc1>=rc0 ? rc1+rRange : rc1;
    saturate(r0Min,0,0x1f);saturate(r0Max,0,0x1f);
    saturate(r1Min,0,0x1f);saturate(r1Max,0,0x1f);

    int g0Min = gc0<=gc1 ? gc0-gRange : gc0;
    int g0Max = gc0>gc1 ? gc0+gRange : gc0;
    int g1Min = gc1<gc0 ? gc1-gRange : gc1;
    int g1Max = gc1>=gc0 ? gc1+gRange : gc1;
    saturate(g0Min,0,0x3f);saturate(g0Max,0,0x3f);
    saturate(g1Min,0,0x3f);saturate(g1Max,0,0x3f);
    
    int b0Min = bc0<=bc1 ? bc0-bRange : bc0;
    int b0Max = bc0>bc1 ? bc0+bRange : bc0;
    int b1Min = bc1<bc0 ? bc1-bRange : bc1;
    int b1Max = bc1>=bc0 ? bc1+bRange : bc1;
    saturate(b0Min,0,0x1f);saturate(b0Max,0,0x1f);
    saturate(b1Min,0,0x1f);saturate(b1Max,0,0x1f);
    

    for (int rn0=r0Min; rn0<=r0Max; rn0++)
    for (int gn0=g0Min; gn0<=g0Max; gn0++)
    for (int bn0=b0Min; bn0<=b0Max; bn0++)
    for (int rn1=r1Min; rn1<=r1Max; rn1++)
    for (int gn1=g1Min; gn1<=g1Max; gn1++)
    for (int bn1=b1Min; bn1<=b1Max; bn1++)
    {
      
      DWORD c0 = (rn0<<11)|(gn0<<5)|bn0;
      DWORD c1 = (rn1<<11)|(gn1<<5)|bn1;
      DWORD c0RGB = Convert565To8888(c0);
      DWORD c1RGB = Convert565To8888(c1);

      DWORD color[4];
      if (!transparent)
      {
        // try opaque encoding: 4 colors
        // note: to indicate opacity we must have: c0>c1
        if (c0<c1)
        {
          DWORD t=c0;c0=c1;c1=t;
          DWORD tRGB=c0RGB;c0RGB=c1RGB;c1RGB=tRGB;
        }
        Create4Colors(color,c0RGB,c1RGB);

        double error = CalcErrorNearest(colors,nColors,color,4,bestError);
        if (bestError>error)
        {
          bestError = error;
          bestC0 = c0;
          bestC1 = c1;
  #if 0 //COUNT_FUNC
          Log("Improved using %d,%d,%d, %d,%d,%d",rn0-rc0,gn0-gc0,bn0-bc0,rn1-rc1,gn1-gc1,bn1-bc1);
  #endif
          improved = true;
        }
      }
      
      if (!alpha)
      {
        // try 3 colors as well
        if (c0>c1)
        {
          DWORD t=c0;c0=c1;c1=t;
          DWORD tRGB=c0RGB;c0RGB=c1RGB;c1RGB=tRGB;
        }
        Create3Colors(color,c0RGB,c1RGB);
        double error = CalcErrorNearest(colors,nColors,color,3,bestError);
        if (bestError>error)
        {
          // we need to indicate transparency
          bestError = error;
          bestC0 = c0;
          bestC1 = c1;
  #if 0 //COUNT_FUNC
          Log("Improved using %d,%d,%d, %d,%d,%d",rn0-rc0,gn0-gc0,bn0-bc0,rn1-rc1,gn1-gc1,bn1-bc1);
  #endif
          improved = true;
        }
      }
      
    }
    #if COUNT_FUNC
    if (improved) DXTSCFunctionUsed[DXTSCFunctionExtImproveBForce]++;
    #endif

  }
  #endif
  
  if (bestC0>bestC1)
  {
    DWORD color[4];
    DWORD bestC0RGB = Convert565To8888(bestC0);
    DWORD bestC1RGB = Convert565To8888(bestC1);
    Create4Colors(color,bestC0RGB,bestC1RGB);
    SelectNearest(bestCodes,pixels,color,4,alpha);
  }
  else
  {
    DWORD color[3];
    DWORD bestC0RGB = Convert565To8888(bestC0);
    DWORD bestC1RGB = Convert565To8888(bestC1);
    Create3Colors(color,bestC0RGB,bestC1RGB);
    SelectNearest(bestCodes,pixels,color,3,alpha);
  }

  #if COUNT_FUNC
  DXTSCFunctionUsed[bestAlg]++;
  #endif

  // store colors to c0 and c1
  tgt.c0 = bestC0;
  tgt.c1 = bestC1;

  // store 2bit codes
  tgt.tex0 =
  (
    (bestCodes[0]<<0)|(bestCodes[1]<<2)|(bestCodes[2]<<4)|(bestCodes[3]<<6)|
    (bestCodes[4]<<8)|(bestCodes[5]<<10)|(bestCodes[6]<<12)|(bestCodes[7]<<14)
  );
  tgt.tex1 =
  (
    (bestCodes[0+8]<<0)|(bestCodes[1+8]<<2)|(bestCodes[2+8]<<4)|(bestCodes[3+8]<<6)|
    (bestCodes[4+8]<<8)|(bestCodes[5+8]<<10)|(bestCodes[6+8]<<12)|(bestCodes[7+8]<<14)
  );
  return bestError;
}

static void DiscretizeAlpha( DWORD *srcData, const DWORD *argbData, int n)
{
  int histogram[256];
  for (int i=0; i<256; i++) histogram[i] = 0;
  // 1) build alpha histogram and average alpha
  int aSum = 0;
  for (int i=0; i<n; i++)
  {
    int a = GetA(argbData[i]);
    histogram[a]++;
    aSum += a;
  }
  // 2) select alpha treshold so that texture does not become more transparent than it was
  // pixels are opaque when pixel.a>=thold
  int aboveThold = 0;
  int thold = 1;
  int desiredSumAlpha = aSum >> 8;
  for (int i=255; i>=0; i--)
  {
    aboveThold += histogram[i];
    // alpha of result picture would be aboveThold/n
    if (aboveThold >= desiredSumAlpha) {thold=i;break;}
  }

  DWORD aThold = unsigned(thold)<<24;
  for (int i=0; i<n; i++)
  {
    // apply treshold
    int pix = argbData[i];
    if ((pix&0xff000000)>=aThold) pix |= 0xff000000;
    else pix &= ~0xff000000;
    srcData[i] = pix;
  }
}

#define AREA_SIZE 8
static void DiscretizeAlphaLocal(DWORD *srcData, const DWORD *argbData, int width, int height)
{
  // For areas
  int maxyy = (height + AREA_SIZE - 1) / AREA_SIZE;
  int maxxx = (width + AREA_SIZE - 1) / AREA_SIZE;
  for (int yy = 0; yy < maxyy; yy++) {
    for (int xx = 0; xx < maxxx; xx++) {
      int maxy = min((yy + 1) * AREA_SIZE, height);
      int maxx = min((xx + 1) * AREA_SIZE, width);

      int histogram[256];
      for (int i=0; i<256; i++) histogram[i] = 0;
      // 1) build alpha histogram and average alpha
      int aSum = 0;
      for (int y = yy * AREA_SIZE; y < maxy; y++) {
        for (int x = xx * AREA_SIZE; x < maxx; x++) {
          int a = GetA(argbData[y * width + x]);
          histogram[a]++;
          aSum += a;
        }
      }
      // 2) select alpha treshold so that texture does not become more transparent than it was
      // pixels are opaque when pixel.a>=thold
      int aboveThold = 0;
      int thold = 1;
      int desiredSumAlpha = aSum >> 8;
      for (int i=255; i>=0; i--)
      {
        aboveThold += histogram[i];
        // alpha of result picture would be aboveThold/n
        if (aboveThold >= desiredSumAlpha) {thold=i;break;}
      }

      DWORD aThold = unsigned(thold)<<24;

      for (int y = yy * AREA_SIZE; y < maxy; y++) {
        for (int x = xx * AREA_SIZE; x < maxx; x++) {
          // apply treshold
          int pix = argbData[y * width + x];
          if ((pix&0xff000000)>=aThold) pix |= 0xff000000;
          else pix &= ~0xff000000;
          srcData[y * width + x] = pix;
        }
      }
    }
  }
}

int Picture::SaveDataDXTOpaque( QOStream &f )
{
#if !USE_LZO_COMPRESSION
  // save image size
  fputiw((word)_w,f);
  fputiw((word)_h,f);
#endif

  // use string stream to store complete compressed data

  Temp<DWORD> srcBuf;
  DWORD *srcData = NULL;


  if (IsOpaque())
  {
    srcData = _argb;
  }
  else
  {
    srcBuf.Realloc(_w*_h);
    // aplly transparency maintaining filter
    srcData = srcBuf.Data();
    DiscretizeAlphaLocal(srcData, _argb, _w, _h);
  }


  if (_w&3 || _h&3) return -1; // cannot compress

  #if COUNT_FUNC
  for (int i=0; i<NDXTSCFunctionsExt; i++)
  {
    DXTSCFunctionUsed[i] = 0;
  }
  DWORD start = ::GetTickCount();
  #endif
  double totalError = 0;

  Temp<DXTBlock64> out(_h/4*_w/4);
  // take 4x4 blocks 
  DXTBlock64 *tgt = out;
  // perform conversion from srcBuf to output buffer
  for (int y=0; y<_h; y+=4)
  {
    for (int x=0; x<_w; x+=4)
    {
      // output to *tgt
      // source from x,y
      DWORD pixels[4][4];
      DWORD *srcBlock = srcData+(y*_w+x);
      for (int xx=0; xx<4; xx++) for (int yy=0; yy<4; yy++)
      {
        pixels[yy][xx] = srcBlock[yy*_w+xx];
      }
      totalError += CompressDXT1Block(*tgt,pixels[0]);

      tgt++;
    }
    #if 0 // COUNT_FUNC
      LogF("y=%d",y);
    #endif
  }

  #if COUNT_FUNC
  LogF(
    "%dx%d: %d ms, total error %g",_w,_h,::GetTickCount()-start,
    sqrt(totalError/((REyeI*REyeI+GEyeI*GEyeI+BEyeI*BEyeI)*(1.0f/3)*_w*_h))
  );
  LogF("Functions used for %dx%d",_w,_h);
  for (int i=0; i<NDXTSCFunctionsExt; i++)
  {
    LogF("  %d: %d",i,DXTSCFunctionUsed[i]);

  }
  #endif

#if USE_LZO_COMPRESSION
  lzo_uint len = out.Size()*sizeof(*out.Data());

  if (_w >= 256)
  {
    // LZO compression
    int ok = lzo_init();
    if (ok == LZO_E_OK)
    {
      // Formula for safe space: output_block_size = input_block_size + (input_block_size / 16) + 64 + 3
      lzo_uint size = len + len / 16 + 64 + 3;

      Temp<char> compressedBuffer(size); // if compressed data will be bigger than original, we will not compress
      Temp<char> workingBuffer(LZO1X_999_MEM_COMPRESS);

      ok = lzo1x_999_compress((lzo_bytep)out.Data(), len, // source
        (lzo_bytep)compressedBuffer.Data(), &size, // destination
        workingBuffer.Data());
      if (ok == LZO_E_OK && size < len) // store only when compression was successful
      {
        // succeeded, can write to the stream

        // save image size
        fputiw((word)(_w | 0x8000), f); // mark the data as LZO compressed
        fputiw((word)_h, f);

        // store compressed size
        fputi24(size, f);
        // store compressed data
        f.write(compressedBuffer.Data(), size);

        return 0;
      }
    }
    // failed, save uncompressed
  }

  // save without LZO compression

  // save image size
  fputiw((word)_w,f);
  fputiw((word)_h,f);
#else
  int len = out.Size()*sizeof(*out.Data());
#endif

  // store compressed size
  fputi24(len,f);
  // store compressed data
  f.write((const char *)out.Data(),len);
  return 0;
}

int Picture::SaveDataDXTAlpha( QOStream &f, bool implicit )
{
#if !USE_LZO_COMPRESSION
  // save image size
  fputiw((word)_w,f);
  fputiw((word)_h,f);
#endif

  // use string stream to store complete compressed data

  Temp<DWORD> srcBuf;
  DWORD *srcData = NULL;


  srcData = _argb;
  
  if (_w&3 || _h&3) return -1; // cannot compress

  #if COUNT_FUNC
  for (int i=0; i<NDXTSCFunctionsExt; i++)
  {
    DXTSCFunctionUsed[i] = 0;
  }
  #endif

  Temp<DXTBlock128> out(_h/4*_w/4);
  // take 4x4 blocks 
  DXTBlock128 *tgt = out;
  // perform conversion from srcBuf to output buffer
  for (int y=0; y<_h; y+=4)
  {
    for (int x=0; x<_w; x+=4)
    {
      // output to *tgt
      // source from x,y
      DWORD pixels[4][4];
      DWORD *srcBlock = srcData+(y*_w+x);
      for (int xx=0; xx<4; xx++) for (int yy=0; yy<4; yy++)
      {
        pixels[yy][xx] = srcBlock[yy*_w+xx];
      }
      CompressDXT1Block(tgt->color,pixels[0],true);
      if (!implicit)
      {
        CompressDXTABlock(tgt->alphaExp,pixels[0]);
      }
      else
      {
        CompressDXTABlock(tgt->alphaImp,pixels[0]);
      }


      tgt++;
    }
  }

  #if COUNT_FUNC
  LogF("Functions used for %dx%d",_w,_h);
  for (int i=0; i<NDXTSCFunctionsExt; i++)
  {
    LogF("  %d: %d",i,DXTSCFunctionUsed[i]);
  }
  #endif

#if USE_LZO_COMPRESSION
  // size of output data
  lzo_uint len = out.Size() * sizeof(*out.Data());

  if (_w >= 256)
  {
    // LZO compression
    int ok = lzo_init();
    if (ok == LZO_E_OK)
    {
      // Formula for safe space: output_block_size = input_block_size + (input_block_size / 16) + 64 + 3
      lzo_uint size = len + len / 16 + 64 + 3;

      Temp<char> compressedBuffer(size); // if compressed data will be bigger than original, we will not compress
      Temp<char> workingBuffer(LZO1X_999_MEM_COMPRESS);

      ok = lzo1x_999_compress((lzo_bytep)out.Data(), len, // source
        (lzo_bytep)compressedBuffer.Data(), &size, // destination
        workingBuffer.Data());
      if (ok == LZO_E_OK && size < len) // store only when compression was successful
      {
        // succeeded, can write to the stream

        // save image size
        fputiw((word)(_w | 0x8000), f); // mark the data as LZO compressed
        fputiw((word)_h, f);

        // store compressed size
        fputi24(size, f);
        // store compressed data
        f.write(compressedBuffer.Data(), size);

        return 0;
      }
    }
    // failed, save uncompressed
  }

  // save without LZO compression

  // save image size
  fputiw((word)_w,f);
  fputiw((word)_h,f);
#else
  int len = out.Size()*sizeof(*out.Data());
#endif

  // store compressed size
  fputi24(len,f);
  // store compressed data
  f.write((const char *)out.Data(),len);

  return 0;
}

int Picture::SaveData88( QOStream &f )
{
  // save image size
  fputiw((word)_w,f);
  fputiw((word)_h,f);
  
  // use string stream to store complete compressed data
  QOStrStream str;
  Temp<word> d88(_w*_h);
  Convert8888To88(d88,_argb,_w,_h);
  SavePACLZW(str,(byte *)(word *)d88,_w*_h*2);
  
  // store compressed size
  int mipLen=str.pcount();
  const char *mipData=str.str();
  if( !mipData ) return -1;
  fputi24(mipLen,f);
  f.write(mipData,mipLen);
  return 0;
}

#define AdaptivePalette MedianCut
//#define AdaptivePalette Popularity

int Picture::SaveTGA( const char *dst )
{
  ConvertARGB();
  DefaultDynamicRange();
  return TGALSave(dst,_w,_h,_argb);
}

int Picture::SavePNG( const char *dst )
{
  ConvertARGB();
  DefaultDynamicRange();
  return GFormatPNG->Save(dst,_w,_h,_argb);
}

const int NMipmaps = 16;

int Picture::SaveMipmapsP8( QOStream &f, const TextureHints &hints )
{
  
  // create a palette
  
  if( _lenPal<=0 )
  {
    AdaptivePalette(); // create palette
    SortPal();
    TouchPalette();
  }
  
  int offsets[NMipmaps];
  memset(offsets,0,sizeof(offsets));
  int nOffsets=0;

  //const int nOffsets = 16;
  if( SaveTaggs(f,hints,TAForceMasked)<0 ) return -1;
  int offStart = f.tellp();
  if( SaveOffsets(f,NULL,NMipmaps)<0 ) return -1;
  if( SavePalette(f)<0 ) return -1;

  Picture temp = *this;
  temp.MipmapFilter(0,hints);
  temp.ConvertPal(); // convert rgb into palette
  //LogF("Init save %dx%d",_w,_h);
  if( temp.SaveData(f,true)<0 ) return -1;

    // create mip-map levels
  for (int i=0; i<12; i++)
  {
    offsets[nOffsets++]=f.tellp();
    if (!MipmapHalfsize(1,1,hints)) break;
    Picture temp = *this;
    if (!temp.MipmapFilter(i+1,hints)) break;
    temp.ConvertPal(); // convert rgb into palette
    if( temp.SaveData(f,true)<0 ) return -1;
  }
  // mark EOF
  {
    for( int i=0; i<6; i++ ) f.put((char)0);
  }
  
  f.seekp(offStart,QIOS::beg);
  SaveOffsets(f,offsets,NMipmaps);

  return 0;
}

int Picture::SaveMipmaps( QOStream &out, const TextureHints &hints )
{
  return SaveMipmaps1555(out,hints);
}

int Picture::SaveMipmaps1555( QOStream &f, const TextureHints &hints )
{
  ConvertARGB();
  CompressDynamicRange(hints._enableDynRange);

  fputiw(PAA_1555,f);
  if( SaveTaggs(f,hints,TAForceMasked)<0 ) return -1;
  int offStart = f.tellp();
  if( SaveOffsets(f,NULL,NMipmaps)<0 ) return -1;
  if( SaveNulPalette(f)<0 ) return -1;

  int offsets[NMipmaps];
  memset(offsets,0,sizeof(offsets));
  int nOffsets=0;

  offsets[nOffsets++]=f.tellp();
  
  Picture swizzled = *this;
  swizzled.MipmapFilter(0,hints);
  swizzled.ChannelSwizzle(hints._channelSwizzle);
  if( swizzled.SaveData1555(f, hints)<0 ) return -1;
  
  for (int i=0; i<12; i++)
  {
    if (!MipmapHalfsize(1, 1, hints)) break;
    offsets[nOffsets++]=f.tellp();
    Picture swizzled = *this;
    swizzled.MipmapFilter(i+1,hints);
    swizzled.ChannelSwizzle(hints._channelSwizzle);
    if( swizzled.SaveData1555(f, hints)<0 ) return -1;
  }

  // mark EOF
  {
    for( int i=0; i<6; i++ ) f.put((char)0);
  }
  
  // return and save taggs again
  f.seekp(offStart,QIOS::beg);
  SaveOffsets(f,offsets,NMipmaps);
  return 0;
}

int Picture::SaveMipmapsDXTOpaque( QOStream &f, const TextureHints &hints )
{
  ConvertARGB();
  CompressDynamicRange(hints._enableDynRange);

  fputiw(PAA_DXT1,f);

  if( SaveTaggs(f,hints,TAForceMasked)<0 ) return -1;
  int offStart = f.tellp();
  if( SaveOffsets(f,NULL,NMipmaps)<0 ) return -1;
  if( SaveNulPalette(f)<0 ) return -1;
  
  int offsets[NMipmaps];
  memset(offsets,0,sizeof(offsets));
  int nOffsets=0;

  SelectErrorMode(hints._errorMode);

  offsets[nOffsets++]=f.tellp();
  
  Picture swizzled = *this;
  swizzled.MipmapFilter(0,hints);
  swizzled.ChannelSwizzle(hints._channelSwizzle);
  if( swizzled.SaveDataDXTOpaque(f)<0 ) return -1;
  
  // create mip-map levels
  for (int i=0; i<12; i++)
  {
    if (!MipmapHalfsize(1, 4, hints)) break;
    offsets[nOffsets++]=f.tellp();
    
    Picture swizzled = *this;
    swizzled.MipmapFilter(i+1,hints);
    swizzled.ChannelSwizzle(hints._channelSwizzle);
    if( swizzled.SaveDataDXTOpaque(f)<0 ) return -1;
  }

  // mark EOF
  {
    for( int i=0; i<6; i++ ) f.put((char)0);
  }

  // return and save taggs again
  f.seekp(offStart,QIOS::beg);
  SaveOffsets(f,offsets,NMipmaps);
  return 0;
}

int Picture::SaveMipmapsDXTAlpha( QOStream &f, const TextureHints &hints )
{
  ConvertARGB();
  CompressDynamicRange(hints._enableDynRange);
  
  // alpha premultiplied - yes or no?
  bool premult = false;
  bool implicit = true;
  bool autodetect = true;
  switch (hints._format)
  {
  case TexFormDXT2:
    premult = true;implicit=false;autodetect=false;
    break;
  case TexFormDXT3:
    premult = false;implicit=false;autodetect=false;
    break;
  case TexFormDXT4:
    premult = true;implicit=true;autodetect=false;
    break;
  case TexFormDXT5:
    premult = false;implicit=true;autodetect=false;
    break;
  }
  // currently only explicit is supported
  // if autodetection is on, we might want to check what compression will be the best
  if (implicit)
  {
    fputiw(premult ? PAA_DXT4 : PAA_DXT5,f);
  }
  else
  {
    fputiw(premult ? PAA_DXT2 : PAA_DXT3,f);
  }

  if (premult)
  {
    AddBlackToAlpha();
  }

  if( SaveTaggs(f, hints, TAForceAlpha)<0 ) return -1;
  int offStart = f.tellp();
  if( SaveOffsets(f,NULL,NMipmaps)<0 ) return -1;
  if( SaveNulPalette(f)<0 ) return -1;

  int offsets[NMipmaps];
  memset(offsets,0,sizeof(offsets));
  int nOffsets=0;

  SelectErrorMode(hints._errorMode);

  offsets[nOffsets++]=f.tellp();
  
  Picture swizzled = *this;
  swizzled.MipmapFilter(0,hints);
  swizzled.ChannelSwizzle(hints._channelSwizzle);
  if( swizzled.SaveDataDXTAlpha(f,implicit)<0 ) return -1;
  
  // create mip-map levels
  for (int i=0; i<12; i++)
  {
    if (!MipmapHalfsize(1, 4, hints)) break;
    offsets[nOffsets++]=f.tellp();
    Picture swizzled = *this;
    swizzled.MipmapFilter(i+1,hints);
    swizzled.ChannelSwizzle(hints._channelSwizzle);
    if( swizzled.SaveDataDXTAlpha(f,implicit)<0 ) return -1;
  }

  // mark EOF
  {
    for( int i=0; i<6; i++ ) f.put((char)0);
  }

  // return and save taggs again
  f.seekp(offStart,QIOS::beg);
  SaveOffsets(f,offsets,NMipmaps);
  return 0;
}

int Picture::SaveMipmaps4444( QOStream &f, const TextureHints &hints )
{
  ConvertARGB();
  CompressDynamicRange(hints._enableDynRange);

  fputiw(PAA_4444,f);
  if( SaveTaggs(f,hints)<0 ) return -1;
  int offStart = f.tellp();
  if( SaveOffsets(f,NULL,NMipmaps)<0 ) return -1;

  if( SaveNulPalette(f)<0 ) return -1;

  int offsets[NMipmaps];
  memset(offsets,0,sizeof(offsets));
  int nOffsets=0;

  offsets[nOffsets++]=f.tellp();

  Picture swizzled = *this;
  swizzled.MipmapFilter(0,hints);
  swizzled.ChannelSwizzle(hints._channelSwizzle);
  if( swizzled.SaveData4444(f, hints)<0 ) return -1;
  
  // create mip-map levels
  for (int i=0; i<12; i++)
  {
    if (!MipmapHalfsize(1, 1, hints)) break;
    offsets[nOffsets++]=f.tellp();
    Picture swizzled = *this;
    swizzled.MipmapFilter(i+1,hints);
    swizzled.ChannelSwizzle(hints._channelSwizzle);
    if( swizzled.SaveData4444(f, hints)<0 ) return -1;
  }

  // mark EOF
  {
    for( int i=0; i<6; i++ ) f.put((char)0);
  }
  
  // return and save taggs again
  f.seekp(offStart,QIOS::beg);
  SaveOffsets(f,offsets,NMipmaps);
  return 0;
}

int Picture::SaveMipmaps88( QOStream &f, const TextureHints &hints )
{
  ConvertARGB();
  // dynamic range compression has not use when format is lossless
  CompressDynamicRange(hints._enableDynRange);

  fputiw(PAA_88,f);

  if( SaveTaggs(f,hints)<0 ) return -1;

  int offStart = f.tellp();
  if( SaveOffsets(f,NULL,NMipmaps)<0 ) return -1;
  if( SaveNulPalette(f)<0 ) return -1;

  int offsets[NMipmaps];
  memset(offsets,0,sizeof(offsets));
  int nOffsets=0;

  offsets[nOffsets++]=f.tellp();

  Picture swizzled = *this;
  swizzled.MipmapFilter(0,hints);
  swizzled.ChannelSwizzle(hints._channelSwizzle);
  if( swizzled.SaveData88(f)<0 ) return -1;

  for (int i=0; i<12; i++)
  {
    if (!MipmapHalfsize(1, 1, hints)) break;
    offsets[nOffsets++]=f.tellp();
    Picture swizzled = *this;
    swizzled.MipmapFilter(i+1,hints);
    swizzled.ChannelSwizzle(hints._channelSwizzle);
    if( swizzled.SaveData88(f)<0 ) return -1;
  }

  // mark EOF
  {
    for( int i=0; i<6; i++ ) f.put((char)0);
  }
  
  // return and save taggs again
  f.seekp(offStart,QIOS::beg);
  SaveOffsets(f,offsets,NMipmaps);
  return 0;
}

static inline void CheckInvSwizzle(TexSwizzle swiz, int &offset, int &mulA, int &addA)
{
  if (swiz==TSOne)
  {
    // one - ignore input (mul by 0) and set it to one (add 255)
    mulA = 0;
    addA = 255;
    offset = 0;
    return;
  }
  mulA = 1, addA = 0;
  switch (swiz)
  {
    case TSInvAlpha: swiz = TSAlpha; mulA = -1, addA = 255; break;
    case TSInvRed: swiz = TSRed; mulA = -1, addA = 255; break;
    case TSInvGreen: swiz = TSGreen; mulA = -1, addA = 255; break;
    case TSInvBlue: swiz = TSBlue; mulA = -1, addA = 255; break;
  }
  offset = swiz<TSOne ? 24-swiz*8 : 0;
}

void Picture::ChannelSwizzle( const TexSwizzle *channelSwizzle)
{
  if (!_argb) return;
  if (
    channelSwizzle[0]==0 && channelSwizzle[1]==1 &&
    channelSwizzle[2]==2 && channelSwizzle[3]==3
  )
  {
    return;
  }

  int nPixel = _w*_h;
  PPixel *pix = _argb;
  int aOffset;
  int rOffset;
  int gOffset;
  int bOffset;
  int mulA,addA;
  int mulR,addR;
  int mulG,addG;
  int mulB,addB;
  CheckInvSwizzle(channelSwizzle[0],aOffset,mulA,addA);
  CheckInvSwizzle(channelSwizzle[1],rOffset,mulR,addR);
  CheckInvSwizzle(channelSwizzle[2],gOffset,mulG,addG);
  CheckInvSwizzle(channelSwizzle[3],bOffset,mulB,addB);
  
  while (--nPixel>=0)
  {
    PPixel p = *pix;
    int a = (p>>aOffset)&0xff;
    int r = (p>>rOffset)&0xff;
    int g = (p>>gOffset)&0xff;
    int b = (p>>bOffset)&0xff;
    *pix++ = MakeARGB(a*mulA+addA,r*mulR+addR,g*mulG+addG,b*mulB+addB);
  }
}

void Picture::ApplyHints(const TextureHints &hints)
{
  QOStrStream tmp;
  Save(tmp,hints,false);
  QIStrStream in;
  in.init(tmp);
  Load(in,0,true);
}

double Picture::Difference(const Picture &with) const
{
  // non ARGB operations not supported
  if (!_argb || !with._argb) return -1;
  // different resolution not supported
  if (with._h!=_h || with._w!=_w) return -1;
  int size=_h*_w;
  double totalError = 0;
  for (int i=0; i<size; i++)
  {
    PPixel a = _argb[i];
    PPixel b = with._argb[i];
    totalError += SquareDistance8888(a,b);
  }
  return sqrt(totalError/((REyeI*REyeI+GEyeI*GEyeI+BEyeI*BEyeI)*(1.0f/3)*size));
}

double Picture::DifferenceRGB(const Picture &with) const
{
  SelectColorError();
  return Difference(with);
}

double Picture::DifferenceXYZ(const Picture &with) const
{
  SelectDistanceError();
  return Difference(with);
}

int Picture::Save(QOStream &out, const TextureHints &hints, bool isPaa)
{
  // hints may require some channel swizzling
  // 

  bool validTexture = roundTo2PowerNCeil(_w)==_w  && roundTo2PowerNCeil(_h)==_h;
  TexFormat form = hints._format;
  if (validTexture)
  {
    while (hints._limitSize<_w || hints._limitSize<_h)
    {
      bool ok = false;
      if (hints._enableDynRange) ok = HalfsizeSRGB();
      else if (
        hints._mipmapFilter==TexMipNormalizeNormalMapAlpha ||
        hints._mipmapFilter==TexMipNormalizeNormalMapNoise ||
        hints._mipmapFilter == TexMipAddAlphaNoise
      ) ok = HalfsizeAlpha();
      else ok = Halfsize();
    }
    if (hints._autoreduce && _argb)
    {
      // detect if all pixels are the same
      // if they are, we can reduce the texture to save space
      if (IsUniformColor())
      {
        // we know each format can represent 4x4
        // some formats may be even able to represent less, however we do not want to detect this now
        int w = intMin(_w,4);
        int h = intMin(_h,4);
        ChangeSizeUniformColor(w,h);
      }
      // when auto-reducing we may discard alpha when it is not used
      if (IsOpaque() && form>=TexFormDXT2 && form<=TexFormDXT5)
      {
        form = TexFormDXT1;
      }
    }
  }
  else
  {
    switch (form)
    {
      case TexFormDXT1:
      case TexFormDXT2: case TexFormDXT3:
      case TexFormDXT4: case TexFormDXT5:
        form = TexFormDefault;
        break;
    }
  }
  // if format given explicitely, use it
  switch (form)
  {
    case TexFormARGB1555: return SaveMipmaps1555(out,hints);
    case TexFormARGB4444: return SaveMipmaps4444(out,hints);
    case TexFormARGB8888: return SaveMipmaps4444(out,hints);
    case TexFormAI88: return SaveMipmaps88(out,hints);
    case TexFormDXT1: return SaveMipmapsDXTOpaque(out,hints);
    case TexFormDXT2: case TexFormDXT3:
    case TexFormDXT4: case TexFormDXT5:
      return SaveMipmapsDXTAlpha(out,hints);
    case TexFormP8:
      return SaveMipmapsP8(out,hints);
    
  }
  // autoselect format, extension can help to decide
  if( isPaa )
  {
    ConvertARGB();
    //if( IsGray() ) return SaveMipmaps88(out,hints);
    //else
    if (IsMasked()/* && (form != TexFormDXT5)*/)
    {
      #if DXT_ENABLE
      if (hints.EnableDXT() && _w>=4 && _h>=4 && validTexture) return SaveMipmapsDXTOpaque(out,hints);
      else return SaveMipmaps1555(out,hints);
      #else
      return SaveMipmaps1555(out,hints);
      #endif
    }
    else
    {
      if (hints.EnableDXT() && _w>=4 && _h>=4 && validTexture) return SaveMipmapsDXTAlpha(out,hints);
      else return SaveMipmaps4444(out,hints);
    }
  }
  else
  {
    // all new PACs save as LZW compressed
    #if DXT_ENABLE
    if (hints.EnableDXT() && _w>=4 && _h>=4 && validTexture) return SaveMipmapsDXTOpaque(out,hints);
    else return SaveMipmaps(out,hints);
    #else
    return SaveMipmaps(out,hints);
    #endif
  }
}

int Picture::Save( const char *D, const TextureHints *explicitHints, RString *err)
{
  const char *ext = NajdiPExt(NajdiNazev(D));
  if( !strcmpi(ext,".tga") ) return SaveTGA(D);
  if( !strcmpi(ext,".png") ) return SavePNG(D);
  const TextureHints &hints = explicitHints ? *explicitHints : FindHints(D);
  QOFStream out;
  out.open(D);
  if (out.fail())
  {
    if (err) *err = Format("Cannot open for writing (%s)", GetErrorName(out.error()));
    RptF("Cannot open %s for writing (%s)", D, GetErrorName(out.error()));
    return -1;
  }
  bool isPaa = strcmpi(NajdiPExt(NajdiNazev(D)),".paa")==0;
  int ret = Save(out,hints,isPaa);
  if (ret>=0)
  {
    out.close();
    if (out.fail())
    {
      if (err) *err = RString("Failed to close output stream");
      return -1;
    }
    return 0;
  }
  return ret;
}


extern "C" void freeSpc( void *mem );
extern "C" void *mallocSpc( size_t size );

void freeSpc( void *mem ) {free(mem);}
void *mallocSpc( size_t size ) {return malloc(size);}

void GlobalAlive()
{
}
