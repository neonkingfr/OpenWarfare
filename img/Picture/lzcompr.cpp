/**************************************************************
	LZSS.C -- A Data Compression Program
	adapted by Suma
**************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "lzcompr.hpp"

//#else

typedef unsigned char byte;

#define N		 4096
#define F		   18
#define THRESHOLD	2

static byte text_buf[N + F - 1];

#define _TWICE 0

#if !_TWICE
	#define lson(n) (lsons[n])
	#define rson(n) (rsons[n])
	#define dad(n)  (dads[n])
	#define NIL N
#else
	#define lson(n) ( *(short*)&( ((byte *)lsons)[n] ) )
	#define rson(n) ( *(short*)&( ((byte *)rsons)[n] ) )
	#define dad(n)  ( *(short*)&( ((byte *)dads)[n] ) )
	#define NIL (2*N)
#endif

static short match_position,match_len;
static short lsons[N+1],rsons[N+257],dads[N+1];


static void InitTree(void)  /* initialize trees */
{
	int  i;

	for (i = N + 1; i <= N + 256; i++) rsons[i] = NIL;
	for (i = 0; i < N; i++) dads[i] = NIL;
}

static void InsertNode(int r)
{
	int  i, p, cmp;
	byte *key;

	cmp = 1;  key = &text_buf[r];  p = (N + 1 + key[0]);
	#if _TWICE
		r<<=1;
		p<<=1;
	#endif
	rson(r) = lson(r) = NIL;  match_len = 0;
	for ( ; ; ) {
		if ( cmp ) {
			if (rson(p) != NIL) p = rson(p);
			else {  rson(p) = r;  dad(r) = p;  return;  }
		} else {
			if (lson(p) != NIL) p = lson(p);
			else {  lson(p) = r;  dad(r) = p;  return;  }
		}
		{
			#if _TWICE
				short ppul=p>>1;
			#else
				#define ppul p
			#endif
			byte *tbp=&text_buf[ppul+1];
			byte *kp=&key[1];
			for (i = 1; i < F; i++)
				if( *kp++!=*tbp++ ) { cmp=kp[-1]>=tbp[-1];break; }
			if (i > match_len) {
				match_position = ppul;
				if ((match_len = i) >= F)  break;
			}
		}
	}
	dad(r) = dad(p);  lson(r) = lson(p);  rson(r) = rson(p);
	dad(lson(p)) = r;  dad(rson(p)) = r;
	if (rson(dad(p)) == p) rson(dad(p)) = r;
	else                   lson(dad(p)) = r;
	dad(p) = NIL;  /* remove p */
}

static void DeleteNode(int p)  /* deletes node p from tree */
{
	int  q;
	#if _TWICE
		p<<=1;
	#endif
	if (dad(p) == NIL) return;  /* not in tree */
	if (rson(p) == NIL) q = lson(p);
	else if (lson(p) == NIL) q = rson(p);
	else {
		q = lson(p);
		if (rson(q) != NIL) {
			do {  q = rson(q);  } while (rson(q) != NIL);
			rson(dad(q)) = lson(q);  dad(lson(q)) = dad(q);
			lson(q) = lson(p);  dad(lson(p)) = q;
		}
		rson(q) = rson(p);  dad(rson(p)) = q;
	}
	dad(q) = dad(p);
	if (rson(dad(p)) == p) rson(dad(p)) = q;  else lson(dad(p)) = q;
	dad(p) = NIL;
}

//#endif
typedef signed char LZChar;
//typedef unsigned char LZChar;

void EncodeSS( QOStream &out, const char *data, long lensb )
{
	QIStrStream in(data,lensb);
	int  i,c,len,r,s,last_match_len,CPtr;
	char CBuf[17];
	char mask;
	int textsize,codesize;
	int csum;
	if( lensb==0 ) return;
	codesize=textsize=0;
	InitTree();  /* initialize trees */
	csum=0;
	CBuf[0]=0;
	CPtr=mask=1;
	s=0;  r=N-F;
	for( i=s; i<r; i++ ) text_buf[i]=' ';
	for( len=0; len<F && !(c=in.get(),in.fail()||in.eof()); len++ )
	{
		text_buf[r+len]=(char)c;
		csum+=(LZChar)c;
	}
	for( i = 1; i<=F; i++ ) InsertNode(r-i);
	InsertNode(r);
	do
	{
		if( match_len>len ) match_len=len;
		if( match_len<=THRESHOLD )
		{
			match_len=1;
			CBuf[0]|=mask;
			CBuf[CPtr++]=text_buf[r];
		}
		else
		{
			int mp=(r-match_position)&(N-1);
			CBuf[CPtr++]=(char)mp;
			CBuf[CPtr++]=(char)(((mp>>4)&0xf0)|(match_len-(THRESHOLD+1)));
		}
		if( (mask<<=1)==0 )
		{
			out.write(CBuf,CPtr);
			codesize+=CPtr;
			CBuf[0]=0; CPtr=mask=1;
		}
		last_match_len=match_len;
		for ( i=0; i<last_match_len && !(c=in.get(),in.fail()||in.eof()); i++ )
		{
			DeleteNode(s);		/* Delete old strings and read new chars */
			text_buf[s]=(char)c;
			csum+=(LZChar)c;
			if ( s<F-1 ) text_buf[s+N]=(char)c; /* beg. of buf. */
			s++;s&=N-1;
			r++;r&=N-1;
			InsertNode(r);
		}
		textsize+=i;
		while( i++<last_match_len )
		{
			DeleteNode(s);					/* EOF => no need to read, but */
			s++;s&=N-1;
			r++;r&=N-1;
			if( --len ) InsertNode(r);		/* buffer may not be empty. */
		}
		//if( out.fail() ) break;
	} while( len>0 );
	if( CPtr>1 )
	{
		out.write(CBuf,CPtr);
		codesize+=CPtr;
	}
	out.write((char *)&csum,sizeof(csum));
	codesize+=sizeof(csum);
	return;
}

bool DecodeSS( char *dst, long lensb, QIStream &in )
{
	int  i,j,r,c,csum=0,csr;
	int  flags;
	if( lensb<=0 ) return 0;
	for( i=0; i<N-F; i++ ) text_buf[i] = ' ';
	r=N-F; flags=0;
	while( lensb>0 )
	{
		if( ((flags>>= 1)&256)==0 )
		{
			c=(unsigned char)in.get();
			flags=c|0xff00;
		}
		if( in.fail() || in.eof() ) break;
		if( flags&1 )
		{
			c=in.get();
			if( in.fail() || in.eof() ) break;
			csum+=(LZChar)c;
			*dst++ = (char)c;
			lensb--;
			text_buf[r]=(char)c;
			r++;r&=(N-1);
		}
		else
		{
			i=(unsigned char)in.get();
			j=(unsigned char)in.get();
			if( in.fail() || in.eof() ) break;
			i|=(j&0xf0)<<4; j&=0x0f; j+=THRESHOLD;
			lensb-=j;
			for( i=r-i,j+=i; i<=j; i++ )
			{
				c=text_buf[i&(N-1)];
				csum+=(LZChar)c;
				*dst++ = (char)c;
				text_buf[r]=(char)c;
				r++;r&=(N-1);
			}
			lensb--;
		}
	}
	in.read((char *)&csr,sizeof(csr));
	if( in.fail() || in.eof() || csr!=csum ) return false;
	return true;
}

