/*!
\file
Normal maps generated using Linda are "wrong" in several aspects:
- normals around the used area are "average" value
- normals are result of supersampling of normal maps which already exhibit this problem

This utility works as follows:

Inputs are a normal map and a color map.

Pass 1 -  Fixing blended normals

Based on the color map we detect where the "invalid" normals are used. Any pixel in the
neighborhood of such normal which differs in the color is checked if it can be an 
"interpolated pixel" and a better" neighbourg is searched for.

Better neighbough is a pixel which has different color from the interpolated pixel
in question and invalid color, and its distance from invalid color is larger than
the distance of interpolated pixel. (Additional test may be done to check if the
pixel could be a result of linear combination of the invalid and interpolated pixels.)

Pass 2 - Removing invalid normals

We detect all "invalid" normals. For each such normal we find a nearest valid normal,
and use its value instead.
*/


#include <direct.h>
#include <io.h>
#include <fcntl.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <El/elementpch.hpp>
#include <El/math/math3d.hpp>
#include <Es/Framework/debugLog.hpp>
#include <Es/Common/win.h>
#include <Es/Common/fltopts.hpp>
#include <Es/Files/filenames.hpp>
#include <Es/Files/fileContainer.hpp>
#include <Es/Containers/array.hpp>
#include <Es/Containers/array2D.hpp>
#include <Es/Strings/rString.hpp>
#include <Es/Strings/bString.hpp>
#include "..\pics\macros.h"
#include "..\pics\fileutil.h"
#include "..\picture\picture.hpp"
#include "..\pal2pace\resource.h"

// command line interface to dll

#include <Es/Framework/consoleBase.h>



static bool InitConfig()
{
	// load texture hits config
	RString retString;
	LoadPictureConfigRetVal ret = LoadPictureConfig(retString);
	if (ret!=LoadPictureConfigOK)
	{
		RString msg;
		UINT msgId = IDS_MSG_NOCONFIG;
		char mod[256];
    HINSTANCE instance = NULL;
		GetModuleFileName(instance,mod,sizeof(mod));
		const char *modname = GetFilenameExt(mod);
		switch (ret)
		{
			case LoadPictureConfigNoRegistry: msgId = IDS_MSG_NOREGISTRY; break;
			case LoadPictureConfigNoFile: msgId = IDS_MSG_NOCONFIG; break;
			case LoadPictureConfigBadFile: msgId = IDS_MSG_BADCONFIG; break;
			case LoadPictureConfigOldExe: msgId = IDS_MSG_CONFIGOLDAPP; break;
		}


		char msgs[1024]; //,buf[1024]; //,app[1024];
		//LoadString(instance,IDS_APPNAME,app,sizeof(app));
		LoadString(instance,msgId,msgs,sizeof(msgs));
		printf(msgs,(const char *)retString,(const char *)modname);
    printf("\n");
		return false;
	}
	return true;
}

struct SourceCoord
{
  int x,y;

  SourceCoord(int xx, int yy):x(xx),y(yy){}

  int Distance(const SourceCoord &s) const
  {
    return (x-s.x)*(x-s.x) + (y-s.y)*(y-s.y);
  }
  bool Valid() const {return x>=0;}
  bool operator != (const SourceCoord &s) const
  {
    return x!=s.x || y!=s.y;
  }

};

TypeIsSimple(SourceCoord)

static inline Vector3 VectorFromColor(PPixel a)
{
  float xa = (a>>16)&0xff;
  float ya = (a>>8)&0xff;
  float za = (a>>0)&0xff;
  const float i255 = 1.0f/255;
  return Vector3(xa*i255,ya*i255,za*i255);
}

static float HowMuchBetterColor(PPixel betterColor, PPixel interpolatedColor, PPixel invalidColor)
{
  Vector3 diffBetter = VectorFromColor(betterColor)-VectorFromColor(invalidColor);
  Vector3 diffInterp = VectorFromColor(interpolatedColor)-VectorFromColor(invalidColor);
  // check if color are coplanar - if not, they are unrelated and should not be substituted

  Vector3 coplanarTest = diffBetter.CrossProduct(diffInterp);
  if (coplanarTest.SquareSize()>0.1)
  {
    // if not coplanar, do not consider it at all
    return 0;
  }
  float distBetter = diffBetter.SquareSize();
  float distInterp = diffInterp.SquareSize();
  return distBetter-distInterp;
}

static int ConvertNormalMap(
  const char *src, const char *nom, const char *dst,
  int algorithm
)
{
  printf("%s+%s -> %s\n",src,nom,dst);
  
	Picture srcPic;
	Picture nomPic;
  Picture tgtPic;
	if (srcPic.Load(src)<0)
  {
    fprintf(stderr,"Error loading source file %s\n",src);
		return 1;
  }
	if (nomPic.Load(nom)<0)
  {
    fprintf(stderr,"Error loading source file %s\n",nom);
		return 1;
  }

  int w = intMin(srcPic.W(),nomPic.W());
  int h = intMin(srcPic.H(),nomPic.H());
  if (srcPic.W()!=nomPic.W() || srcPic.H()!=nomPic.H())
  {
    fprintf(
      stderr,"Warning: Source file dimensions do not match (%dx%d!=%dx%d)\n",
      srcPic.W(),srcPic.H(),nomPic.W(),nomPic.H()
    );
  }
  // normal map must not use any dynamic range compression
  // if it does, it is strange
  if (!nomPic.IsDefaultDynamicRange())
  {
    fprintf(
      stderr,"Warning: Normal map %s dynamic range compressed (%.1f %%)\n",
      nom,nomPic.MaxDynamicRangeCompression()*100
    );
    nomPic.DefaultDynamicRange();
  }
  tgtPic = nomPic; // inherit all atributes

  // resize to target size
  tgtPic.Crop(w,h);

  enum {Valid,Invalid,Interpolated};
  Array2D<bool> invalid;
  invalid.Dim(w,h);

  for (int y=0; y<h; y++) for (int x=0; x<w; x++)
  {
    int srcA = (srcPic.GetPixel(x,y)>>24)&0xff;
    invalid(x,y) = (srcA<0x80) ? Invalid : Valid;
  }

  // pass 1 
	printf("Pass 1\n");
  // detect interpolated values
  if ((algorithm&1)==0)
  {
    for (int y=0; y<h; y++) for (int x=0; x<w; x++)
    {
      if (invalid(x,y)==Valid)
      {
        PPixel checkColor = nomPic.GetPixel(x,y);
        // check if there is any invalid neighbourgh
        PPixel invalidColor = 0;
        PPixel betterColor = 0;
        for (int xx=-1; xx<=1; xx++) for (int yy=-1; yy<=1; yy++)
        {
          int xn = x+xx;
          int yn = y+yy;
          if (xn<0 || xn>=w) continue;
          if (yn<0 || yn>=h) continue;
          if (invalid(xn,yn)!=Invalid) continue;
          // pixel is valid
          // check if better valid neighbourgh exists
          invalidColor = nomPic.GetPixel(xn,yn);
          goto BreakInvalid;
        }
        if (invalidColor==0) continue;
        BreakInvalid:
        // find best valid neighbourgh
        float best = 0;
        for (int xx=-1; xx<=1; xx++) for (int yy=-1; yy<=1; yy++)
        {
          int xn = x+xx;
          int yn = y+yy;
          if (xx==0 && yy==0) continue;
          if (xn<0 || xn>=w) continue;
          if (yn<0 || yn>=h) continue;
          if (invalid(xn,yn)!=Valid) continue;
          // pixel is valid
          PPixel betterCandidate = nomPic.GetPixel(xn,yn);
          float better = HowMuchBetterColor(betterCandidate,checkColor,invalidColor);
          if (better>best)
          {
            best = better;
            betterColor = betterCandidate;
          }
        }
        if (betterColor==0) continue;
        invalid(x,y) = Interpolated;
        // it does not matter what we set - pass 2 will overwrite it anyway
        // but in case pass 2 is skipped by user's decision,
        // it is better to provide a reasonable value here
        tgtPic.SetPixel(x,y,betterColor);
        //tgtPic.SetPixel(x,y,0xff000000);

      }

    }
    
  }


  if ((algorithm&2)==0)
  {
    // pass 2: fix all invalid values with nearest valid one
	  printf("Pass 2\n");

    /// naive approach could be to take each invalid pixel and find nearest valid for it
    // this would be O(N^4)
    // what we do instead is to take each valid pixel and spread it to all invalid neighbourghs
    // and this repeated until we fill everything
    // during filling we remember source coordinates for each pixel, so that
    // we may be able to select a nearest one
    // O(N^3) - still not very good, but much better
    Array2D<SourceCoord> source;
    source.Dim(w,h);
    for (int y=0; y<h; y++) for (int x=0; x<w; x++)
    {
      if (invalid(x,y))
      {
        // pixel invalid - set as far as possible, anything is better
        source(x,y) = SourceCoord(-w,-h);
      }
      else
      {
        source(x,y) = SourceCoord(x,y);
      }
    }
    //int maxIter = 10;
    int maxIter = intMin(w,h);
    for(int iter=1; iter<maxIter; iter++)
    {
      Array2D<SourceCoord> source0 = source;
    
      int progress = 0;
      int left = 0;
      for (int y=0; y<h; y++)
      {
        // how many pixels are filled in this step
        for (int x=0; x<w; x++)
        {
          if (!invalid(x,y)) continue;
          // check each neighbourgh, select a nearest one (valid only)
          int bestX = -1, bestY = -1;
          int bestDist = INT_MAX;
          for (int xx=-1; xx<=1; xx++) for (int yy=-1; yy<=1; yy++)
          {
            int xn = x+xx;
            int yn = y+yy;
            if (xn<0 || xn>=w) continue;
            if (yn<0 || yn>=h) continue;
            if (!source0(xn,yn).Valid()) continue;
            int dist = source0(xn,yn).Distance(SourceCoord(x,y));
            if (dist<=iter*iter && bestDist>dist)
            {
              bestX = xn;
              bestY = yn;
              bestDist = dist;
            }
          }
          left++;
          if (bestX>=0 && source0(bestX,bestY)!=source0(x,y))
          {
            progress++;
            const SourceCoord &coord = source0(bestX,bestY);
            source(x,y) = coord;
            // pixel is valid now - no better path may exists
            invalid(x,y) = Valid;
            tgtPic.SetPixel(x,y,nomPic.GetPixel(coord.x,coord.y));
            //tgtPic.SetPixel(x,y,nomPic.GetPixel(coord.x,coord.y)&0xffffff);
          }
        }
      }
      printf("%4d: %8d, %8d\r",iter,progress,left);
      //if (progress<=0) break;
      if (left<=0) break;
    }
  }

	printf("\nSaving...\n");

	// save target picture
	return tgtPic.Save(dst);
}

int consoleMain( int argc, const char *argv[] )
{
  if (!InitConfig())
  {
    return 1;
  }
	
	const char *src = NULL;
	const char *dst = NULL;
	const char *nom = NULL;
	const char *srcPath = NULL;
	const char *dstPath = NULL;
  int algorithm = 0;

	while( argc>1 )
	{
		const char *arg=argv[1];
		if( *arg=='-' || *arg=='/' )
		{
			arg++;
			static const char norm[]="norm=";
			static const char alg[]="alg=";
			static const char colorPrefix[]="colorPrefix=";
			static const char dstPrefix[]="dstPrefix=";
			if( !strnicmp(arg,norm,strlen(norm)) )
			{
				arg += strlen(norm);
				nom = arg;
			}
			else if( !strnicmp(arg,alg,strlen(alg)) )
			{
				arg += strlen(alg);
				algorithm = atoi(arg);
			}
			else if( !strnicmp(arg,colorPrefix,strlen(colorPrefix)) )
			{
				arg += strlen(colorPrefix);
				srcPath = arg;
			}
			else if( !strnicmp(arg,dstPrefix,strlen(dstPrefix)) )
			{
				arg += strlen(dstPrefix);
				dstPath = arg;
			}
			else goto Usage;
		}
		else
		{
			if( !src ) src=arg;
			else if( !dst ) dst=arg;
			else goto Usage;
		}
		argc--,argv++;
	}

  if (!src)
  {
    Usage:
		printf
		(
			"Usage:\n"
			"fixLindaNMap [options] source [destination]\n\n"
		  "Options:\n\n"
		  "-alg=<number>       algorith selection\n"
      "-norm=<filename>    name of the normal map\n"
      "-colorPrefix=<path> prefix to beadded before color map names\n"
      "-dstPrefix=<path>   prefix to added before target names\n"
      "\n\n"
      "Color map is assumed as an input, normal map name is created based on its name\n"
      "source may be *.ext to process all _no files, but no other wildcards are allowed\n"
		);
		return 1;
  }
  if (src[0]=='*')
  {
    if (dstPath==NULL)
    {
      fprintf(stderr,"-dstPrefix must be used for wildcard operation\n");
      return 1;
    }
    // special case - for each _no. file 
    // and process it it the pair exists
    BString<1024> noMask;
    strcpy(noMask,"*_no");
    strcat(noMask,src+1);
    _finddata_t fd;
    long f = _findfirst(noMask,&fd);
    if (f>=0)
    {
      do
      {
      	if (fd.attrib&_A_SUBDIR) continue;
        char srcTmp[1024];
        char dstTmp[1024];

	      if (srcPath)
	      {
	        // prepend srcPath to source path
	        strcpy(srcTmp,srcPath);
	        TerminateBy(srcTmp,'\\');
	      }
	      else
	      {
	        strcpy(srcTmp,"");
	      }
	      char *srcCat = srcTmp+strlen(srcTmp);
	      
	      // _no should be before ext
	      strcpy(srcCat,fd.name);
	      strlwr(srcCat);
	      char *noPos = strstr(srcCat,"_no.");
	      if (noPos)
	      {
	        strncpy(noPos,"_co.",4);
	      }
	      
	      strcpy(dstTmp,dstPath);
	      TerminateBy(dstTmp,'\\');
	      strcat(dstTmp,fd.name);
	      
	      ConvertNormalMap(srcTmp,fd.name,dstTmp,algorithm);
      } while (_findnext(f,&fd)==0);
      _findclose(f);
      return 0;
    }
    return 1;
  }
  else
  {
    BString<1024> srcTmp;
	  BString<1024> dstTmp;
	  BString<1024> nomTmp;

    const char *srcExt = GetFileExt(GetFilenameExt(src)); 
    if (!nom)
    {
		  strcpy(nomTmp,src);
		  strcpy(unconst_cast(GetFileExt(GetFilenameExt(nomTmp))),"_no");
      strcat(nomTmp,srcExt);
		  nom = nomTmp;
    }
    if (!dst)
	  {
		  // autogenerate destination name
		  strcpy(dstTmp,nom);
      LString ext = unconst_cast(GetFileExt(dstTmp));
      if (!strcmpi(ext,".paa") && !dstPath)
      {
        // name ends with _no.paa, we will make _no_no.paa from it to avoid overwriting
  		  strcpy(ext,"_no.paa");
      }
      else
      {
		    strcpy(ext,".paa");
      }
		  dst = dstTmp;
	  }
	  if (srcPath)
	  {
	    // prepend srcPath to source path
	    strcpy(srcTmp,srcPath);
	    TerminateBy(unconst_cast((const char *)srcTmp),'\\');
	    strcat(srcTmp,src);
	    src = srcTmp;
  	  
	  }

    BString<1024> dstFull;
    if (dstPath)
    {
		  strcpy(dstFull,dstPath);
	    TerminateBy(unconst_cast((const char *)dstFull),'\\');
	    strcat(dstFull,dst);
	    dst = dstFull;
    }

    return ConvertNormalMap(src,nom,dst,algorithm);
  }
}

