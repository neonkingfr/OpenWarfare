/*!
\file
Procedural texture generation.
*/


#include <direct.h>
#include <io.h>
#include <fcntl.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <El/elementpch.hpp>
#include <Es/Framework/debugLog.hpp>
#include <Es/Common/win.h>
#include <Es/Common/fltopts.hpp>
#include <Es/Files/filenames.hpp>
#include <Es/Containers/array.hpp>
#include <Es/Strings/rString.hpp>
#include "..\pics\macros.h"
#include "..\pics\fileutil.h"
#include "..\picture\picture.hpp"
#include "..\pal2pace\resource.h"

// command line interface to dll

#include <Es/Framework/consoleBase.h>



static bool InitConfig()
{
	// load texture hits config
	RString retString;
	LoadPictureConfigRetVal ret = LoadPictureConfig(retString);
	if (ret!=LoadPictureConfigOK)
	{
		RString msg;
		UINT msgId = IDS_MSG_NOCONFIG;
		char mod[256];
    HINSTANCE instance = NULL;
		GetModuleFileName(instance,mod,sizeof(mod));
		const char *modname = GetFilenameExt(mod);
		switch (ret)
		{
			case LoadPictureConfigNoRegistry: msgId = IDS_MSG_NOREGISTRY; break;
			case LoadPictureConfigNoFile: msgId = IDS_MSG_NOCONFIG; break;
			case LoadPictureConfigBadFile: msgId = IDS_MSG_BADCONFIG; break;
			case LoadPictureConfigOldExe: msgId = IDS_MSG_CONFIGOLDAPP; break;
		}


		char msgs[1024]; //,buf[1024]; //,app[1024];
		//LoadString(instance,IDS_APPNAME,app,sizeof(app));
		LoadString(instance,msgId,msgs,sizeof(msgs));
		printf(msgs,(const char *)retString,(const char *)modname);
    printf("\n");
		return false;
	}
	return true;
}

static void Progress(int y, int h, void *context)
{
  printf("%4d of %4d\r",y,h);
}

int consoleMain( int argc, const char *argv[] )
{
  if (!InitConfig())
  {
    return 1;
  }
	AutoArray<RString> source;
	
	const char *src = NULL;
	const char *dst = NULL;

	int w = 256;
	int h = 256;

	char dstTmp[1024];	
	while( argc>1 )
	{
		const char *arg=argv[1];
		if( *arg=='-' || *arg=='/' )
		{
			arg++;
			static const char wText[]="w=";
			static const char hText[]="h=";
			if( !strnicmp(arg,wText,strlen(wText)) )
			{
				arg+=strlen(wText);
				w = atoi(arg);
			}
			else if( !strnicmp(arg,hText,strlen(hText)) )
			{
				arg+=strlen(hText);
				h = atoi(arg);
			}
			else goto Usage;
		}
		else
		{
			if( !src ) src=arg;
			else if( !dst ) dst=arg;
			else goto Usage;
		}
		argc--,argv++;
	}

  if (!src)
  {
    Usage:
		printf
		(
			"Usage:\n"
			"ProcedTex options expression_filename [destination]\n\n"
		  "Options:\n\n"
		  "-w=<number> result texture width\n"
		  "-h=<number> result texture height\n"
		);
		return 1;
  }
  if (!dst)
	{
		// autogenerate destination name
		strcpy(dstTmp,src);
		strcpy(GetFileExt(dstTmp),".paa");
		dst = dstTmp;
	}

	Picture pic;
	pic.CreateOpaque(w,h);

	// src is filename
	QIFStream exp;
	exp.open(src);
	if (exp.fail())
	{
		printf("File %s not found\n",src);
		return 1;
	}

  Buffer<char> buffer;
  buffer.Resize(exp.rest());
  exp.read(buffer.Data(),buffer.Size());
	RString script = RString(buffer.Data(),buffer.Size());

	printf
	(
		"generating texture %s\n",dst
	);
	// perform testing evaluation to detect any error

  pic.CreateProcedural(w,h,script,Progress,NULL);

	printf("\nSaving...\n");

	// save target picture
	int ret = pic.Save(dst);

	return ret;
}

