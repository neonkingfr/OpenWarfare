#pragma warning(disable:4530)

//#include <fstream.h>
#include <fstream>
#include "pics\macros.h"

using namespace std;

void fputiw( word W, ostream &f )
{
	f.put((byte)W);
	f.put((byte)(W>>8));
}
void fputi24( long W, ostream &f )
{
	f.put((byte)W);
	f.put((byte)(W>>8));
	f.put((byte)(W>>16));
}

void SavePACB( ostream &f, byte *Buf, long L );

//#define BUF_OPT ( 64L*1024 )
//#define BUF_MIN ( 1024 )
int SavePAC256( ostream &f, byte *Buf, int W, int H, long *RGB )
{ /* run-length compress. */
	long L=(long)W*H;
	int I;
	fputiw((word)W,f);
	fputiw((word)H,f);
	fputiw(256,f); /* poc. barev */
	for( I=0; I<256; I++ ) fputi24(RGB[I],f);
	
	SavePACB(f,Buf,L);
	
	if( f.fail() ) return -1;
	return 0;
}

/*

int SavePCL256( ostream &f, byte *Buf, int W, int H, long *RGB, int nColors )
{ // lzw compression
	long L=(long)W*H;
	int I;
	fputiw((word)W,f);
	fputiw((word)H,f);
	fputiw(nColors,f); // poc. barev
	for( I=0; I<nColors; I++ ) fputi24(RGB[I],f);
	
	SavePACB(f,Buf,L);
	
	if( f.fail() ) return -1;
	return 0;
}


int SavePCL256( const char *N, byte *Buf, int W, int H, long *RGB, int nColors )
{ // lzw compress.
	ofstream f;
	int r;
	f.open(N,ios::out|ios::binary);
	r=-1;
	if( f.fail() ) return -1;
	return SavePAC256(f,Buf,W,H,RGB,nColors);
}
*/
