//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by ResAnim.rc
//
#define IDD_RESANIM_DIALOG              102
#define IDR_MAINFRAME                   128
#define IDC_FIRST                       1000
#define IDC_LAST                        1001
#define IDC_LOOP_BEG                    1002
#define IDC_FRAME_HALF                  1003
#define IDC_LOOP_END                    1004
#define IDC_LOOP_EXTRACT                1005
#define IDC_NAME                        1006
#define IDC_NORMALIZE_NAME              1007
#define IDC_BROWSE                      1008
#define IDC_OPEN                        1008
#define IDC_SAVE_AS                     1009
#define IDC_RESOL_X                     1010
#define IDC_RESOL_Y                     1011
#define IDC_RESOL_HALF                  1012
#define IDC_SIZE_KB                     1013
#define IDC_NORM_ALPHA                  1014

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1009
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
