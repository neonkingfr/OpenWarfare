// ResAnimDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ResAnim.h"
#include "ResAnimDlg.h"

#include "..\pal2pacL.hpp"
#include "..\picture\picture.hpp"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CResAnimDlg dialog

CResAnimDlg::CResAnimDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CResAnimDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CResAnimDlg)
	m_Name = _T("");
	_first = 0;
	_last = 0;
	_loopBeg = 0;
	_loopEnd = 0;
	_resolX = 0;
	_resolY = 0;
	_sizeKB = 0;
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	_wait=::LoadCursor(NULL,IDC_WAIT);
}

CResAnimDlg::~CResAnimDlg()
{
	::DestroyCursor(_wait);
}

void CResAnimDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CResAnimDlg)
	DDX_Text(pDX, IDC_NAME, m_Name);
	DDX_Text(pDX, IDC_FIRST, _first);
	DDX_Text(pDX, IDC_LAST, _last);
	DDX_Text(pDX, IDC_LOOP_BEG, _loopBeg);
	DDV_MinMaxInt(pDX, _loopBeg, 0, 16384);
	DDX_Text(pDX, IDC_LOOP_END, _loopEnd);
	DDV_MinMaxInt(pDX, _loopEnd, 0, 16384);
	DDX_Text(pDX, IDC_RESOL_X, _resolX);
	DDV_MinMaxInt(pDX, _resolX, 0, 2048);
	DDX_Text(pDX, IDC_RESOL_Y, _resolY);
	DDV_MinMaxInt(pDX, _resolY, 0, 2048);
	DDX_Text(pDX, IDC_SIZE_KB, _sizeKB);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CResAnimDlg, CDialog)
	//{{AFX_MSG_MAP(CResAnimDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_NORMALIZE_NAME, OnNormalizeName)
	ON_BN_CLICKED(IDC_FRAME_HALF, OnFrameHalf)
	ON_BN_CLICKED(IDC_BROWSE, OnBrowse)
	ON_BN_CLICKED(IDC_SAVE_AS, OnSaveAs)
	ON_BN_CLICKED(IDC_LOOP_EXTRACT, OnLoopExtract)
	ON_BN_CLICKED(IDC_RESOL_HALF, OnResolHalf)
	ON_BN_CLICKED(IDC_NORM_ALPHA, OnNormAlpha)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CResAnimDlg message handlers

BOOL CResAnimDlg::OnInitDialog()
{
	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon

	if( !OpenFile() ) return TRUE;
		
	CDialog::OnInitDialog();
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CResAnimDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CResAnimDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CResAnimDlg::OnNormalizeName() 
{
	Busy();
	// load animation using given name format
	CString wildName=WildcardAnimation(m_Name);
	CString prefix,postfix;
	ExtractAnimatedName(m_Name,prefix,postfix);
	WIN32_FIND_DATA fData;
	HANDLE fFile=FindFirstFile(wildName,&fData);
	int name=wildName.ReverseFind('\\');
	if( name<0 ) name=0;
	if( fFile!=INVALID_HANDLE_VALUE )
	{
		int first=INT_MAX;
		for(;;)
		{
			if( fData.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY ) continue;
			CString fullName(wildName,name);
			fullName+='\\';
			fullName+=fData.cFileName;
			CString thisPrefix,thisPostfix;
			int index=ExtractAnimatedName(fullName,thisPrefix,thisPostfix);
			ASSERT( prefix==thisPrefix );
			ASSERT( postfix==thisPostfix );
			CString newName;
			newName.Format("%s%02d%s",(LPCTSTR)prefix,index,(LPCTSTR)postfix);
			if( first>index ) first=index;
			if( fullName!=newName )
			{
				if( rename(fullName,newName)<0 ) throw CString("Cannot rename ")+fullName;
			}
			if( !FindNextFile(fFile,&fData) ) break;
		}
		FindClose(fFile);
		m_Name.Format("%s%02d%s",(LPCTSTR)prefix,first,(LPCTSTR)postfix);
		ScanAnimation(m_Name,_first,_last);
		ScanResolution(_resolX,_resolY);
		RecalcSize();
		UpdateData(FALSE);
	}
}


void CResAnimDlg::ScanAnimation(CString from, int &first, int &last )
{
	// load animation using given name format
	CString wildName=WildcardAnimation(from);
	CString prefix,postfix;
	ExtractAnimatedName(m_Name,prefix,postfix);
	WIN32_FIND_DATA fData;
	HANDLE fFile=FindFirstFile(wildName,&fData);
	int name=wildName.ReverseFind('\\');
	if( name<0 ) name=0;
	if( fFile!=INVALID_HANDLE_VALUE )
	{
		first=INT_MAX;
		last=0;
		for(;;)
		{
			if( fData.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY ) continue;
			CString fullName(wildName,name);
			fullName+='\\';
			fullName+=fData.cFileName;
			CString thisPrefix,thisPostfix;
			int index=ExtractAnimatedName(fullName,thisPrefix,thisPostfix);
			//ASSERT( prefix==thisPrefix );
			//ASSERT( postfix==thisPostfix );
			if( first>index ) first=index;
			if( last<index ) last=index;
			if( !FindNextFile(fFile,&fData) ) break;
		}
		FindClose(fFile);
		//m_Name.Format("%s%02d%s",(LPCTSTR)prefix,first,(LPCTSTR)postfix);
	}
	AutoLoop();
}

void CResAnimDlg::ScanResolution(int & w, int & h)
{
	w=h=0;
	CString prefix,postfix;
	ExtractAnimatedName(m_Name,prefix,postfix);
	CString fName;
	fName.Format("%s%02d%s",(LPCTSTR)prefix,_first,(LPCTSTR)postfix);
	Picture pic;
	if( pic.Load(fName)>=0 )
	{
		w=pic.Width();
		h=pic.Height();
	}
}

static CString GetFileName( const CString &src )
{
	int namePos=src.ReverseFind('\\');
	if( namePos<0 ) return src;
	return src.Mid(namePos);
}

static CString GetExtensions( const CString &src )
{
	CString name=GetFileName(src);
	int pos=name.Find('.');
	if( pos<0 ) return ".";
	return name.Mid(pos);
}

static CString GetExtension( const CString &src )
{
	CString name=GetFileName(src);
	int pos=name.ReverseFind('.');
	if( pos<0 ) return ".";
	return name.Mid(pos);
}

int CResAnimDlg::ExtractAnimatedName(CString from, CString & prefix, CString & postfix)
{
	static const char numbers[]="0123456789";
	// extract prefix and postfix from wildcart name
	int namePos=from.ReverseFind('\\');
	if( namePos<0 ) namePos=0;
	CString name=from.Mid(namePos);
	int num=name.FindOneOf(numbers);
	if( num<0 ) return 0; // error - no number
	int notNum;
	for( notNum=num; strchr(numbers,name[notNum]); notNum++ ) {}
	// reformat postfix to include only extension
	while( notNum<name.GetLength() && name[notNum]!='.' ) notNum++;
	int index=atoi(name.Mid(num));
	// remove any trailing spec. characters from prefix
	static const char specChar[]=" [(-.";
	while( num>0 && strchr(specChar,name[num-1]) ) num--;
	prefix=from.Left(namePos);
	prefix+=name.Left(num);
	int prefixEnd=prefix.GetLength();
	if( prefixEnd<0 || prefix[prefixEnd-1]!='.' ) prefix+=".";
	postfix=name.Mid(notNum);
	return index;
}

CString CResAnimDlg::WildcardAnimation(CString from)
{
	CString wildName=from;
	static const char numbers[]="0123456789";
	int number;
	int namePos=wildName.ReverseFind('\\');
	CString name=wildName.Mid(namePos);
	while( (number=name.FindOneOf(numbers))>=0 )
	{
		name.SetAt(number,'*');
		wildName.SetAt(number+namePos,'*');
	}
	return wildName;
}

void CResAnimDlg::OnFrameHalf() 
{
	Busy();
	// skip every 2nd frame
	CString prefix,postfix;
	ExtractAnimatedName(m_Name,prefix,postfix);
	int dst=1;
	try
	{
		for( int i=_first; i<=_last; i+=2 )
		{
			if( i+1<=_last )
			{
				CString skipName;
				skipName.Format("%s%02d%s",(LPCTSTR)prefix,i+1,(LPCTSTR)postfix);
				if( remove(skipName)<0 ) throw CString("Cannot delete ")+skipName;
			}
			CString oldName,newName;
			oldName.Format("%s%02d%s",(LPCTSTR)prefix,i,(LPCTSTR)postfix);
			newName.Format("%s%02d%s",(LPCTSTR)prefix,dst,(LPCTSTR)postfix);
			if( rename(oldName,newName) )
			{
				throw CString("Cannot rename ")+oldName;
			}
			dst++;
		}
		_first=1;
		_last=dst-1;
		RecalcSize();
		AutoLoop();
		m_Name.Format("%s%02d%s",(LPCTSTR)prefix,_first,(LPCTSTR)postfix);
		UpdateData(FALSE);
	}
	catch( CString &e )
	{
		MessageBox(e,"Animation Manager",MB_ICONERROR|MB_OK);
	}
	catch( CFileException * )
	{
		MessageBox("Animation name not valid","Animation Manager",MB_ICONERROR|MB_OK);
	}
}

void CResAnimDlg::OnBrowse() 
{
	if( OpenFile() ) UpdateData(FALSE);
}

static CString ReadFilter=
	"Animated Texture|*.*.tga;*.*.gif;*.*.paa;*.*.pac|"
	"Animated Source|*.*.tga;*.*.gif|"
	"Animated Destination|*.*.paa;*.*.pac|"
	"All Files (*.*)|*.*||"
;
static CString WriteFilter=
	"Animated Destination|*.*.paa;*.*.pac;*.*.tga||"
;

bool CResAnimDlg::OpenFile()
{
	CFileDialog fileSel
	(
		TRUE,"TGA",NULL,OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT,ReadFilter,this
	);
	if( fileSel.DoModal()==IDCANCEL )
	{
		EndDialog(IDCANCEL);
		return false;
	}
	CString path=fileSel.GetPathName();
	m_Name=path;
	ScanAnimation(m_Name,_first,_last);
	ScanResolution(_resolX,_resolY);
	RecalcSize();
	return true;
}

void CResAnimDlg::OnSaveAs() 
{
	Busy();
	// copy source animation to the new path
	// this includes normalizing filenames

	CString oldPrefix,oldPostfix;
	ExtractAnimatedName(m_Name,oldPrefix,oldPostfix);

	CFileDialog fileSel
	(
		FALSE,"PAA",NULL,OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT,WriteFilter,this
	);

	if( fileSel.DoModal()==IDCANCEL ) return;
	try
	{
		CString path=fileSel.GetPathName();
		int nPos=path.ReverseFind('\\'); // find filename
		if( nPos<0 ) nPos=0;
		CString name=path.Mid(nPos);
		int ePos=name.Find('.');
		if( ePos<0 ) ePos=name.GetLength();
		CString srcExt=GetExtension(m_Name);
		CString tgtExt=".paa";
		if( srcExt==".gif" ) tgtExt=".pac";
		if( srcExt==".tga" ) tgtExt=".tga";
		// make default extension
		path=path.Left(nPos+ePos)+".01"+tgtExt;

		CString newPrefix,newPostfix;
		if( ExtractAnimatedName(path,newPrefix,newPostfix)<0 )
		{
			// invalid name
			AfxThrowFileException(0); 
		}


		// convert from source to destination
		int dst=1;
		for( int i=_first; i<=_last; i++ )
		{
			CString oldName,newName;
			oldName.Format("%s%02d%s",(LPCTSTR)oldPrefix,i,(LPCTSTR)oldPostfix);
			newName.Format("%s%02d%s",(LPCTSTR)newPrefix,dst++,(LPCTSTR)newPostfix);
			DoConvertFile(newName,oldName,-1);
		}
		_first=1;
		_last=dst-1;
		m_Name.Format("%s%02d%s",(LPCTSTR)newPrefix,_first,(LPCTSTR)newPostfix);
		AutoLoop();
		UpdateData(FALSE);
	}
	catch( CString &e )
	{
		MessageBox(e,"Animation Manager",MB_ICONERROR|MB_OK);
	}
	catch( CFileException * )
	{
		MessageBox("Animation name not valid","Animation Manager",MB_ICONERROR|MB_OK);
	}
	
}

void CResAnimDlg::OnLoopExtract() 
{
	Busy();

	CString prefix,postfix;
	ExtractAnimatedName(m_Name,prefix,postfix);

	// create looping animation from any non-looping region
	// there are three source regions considered
	// begin from _first to _loopBeg-1
	// loop from _loopBeg to loopEnd-1
	// end from loopEnd to _last
	if( _loopBeg>=_loopEnd ) return;
	int beginLen=_loopBeg-_first;
	int loopLen=_loopEnd-_loopBeg;
	int endLen=_last+1-_loopEnd;
	// overlapped region length is smallest of all three
	int overlap=beginLen;
	if( overlap>beginLen ) overlap=beginLen;
	if( overlap>loopLen ) overlap=loopLen;
	if( overlap>endLen ) overlap=endLen;
	if( overlap<=1 ) return;
	// merge in beginning
	try
	{
		float invOverlap=1.0/overlap;
		int i;
		/*
		// first of all reduce alpha factor in overlap areas
		for( i=_loopBeg; i<_loopEnd; i++ )
		{
			CString dstName;
			dstName.Format("%s%02d%s",(LPCTSTR)prefix,i,(LPCTSTR)postfix);
			Picture dest;
			if( dest.Load(dstName)<0 ) throw CString("Cannot load ")+dstName;
			dest.ModulateAlpha(0);
			if( dest.Save(dstName)<0 ) throw CString("Cannot save ")+dstName;
		}
		*/
		/*
		for( i=_loopBeg; i<_loopEnd; i++ )
		{
			// make space for 
			CString name;
			name.Format("%s%02d%s",(LPCTSTR)prefix,i,(LPCTSTR)postfix);
			Picture dst;
			if( dst.Load(name)<0 ) throw CString("Cannot load ")+name;
			dst.ModulateAlpha(0.5);
			if( dst.Save(name)<0 ) throw CString("Cannot save ")+name;
		}
		*/
		for( i=0; i<overlap; i++ )
		{
			int srcIndex=_loopEnd+i;
			int dstIndex=_loopBeg+i;
			float merge=(overlap-1-i)*invOverlap*0.5;
			CString srcName,dstName;
			srcName.Format("%s%02d%s",(LPCTSTR)prefix,srcIndex,(LPCTSTR)postfix);
			dstName.Format("%s%02d%s",(LPCTSTR)prefix,dstIndex,(LPCTSTR)postfix);
			Picture dest,src;
			if( dest.Load(dstName)<0 ) throw CString("Cannot load ")+dstName;
			if( src.Load(srcName)<0 ) throw CString("Cannot load ")+srcName;
			dest.Blend(src,1-merge,merge);
			if( dest.Save(dstName)<0 ) throw CString("Cannot save ")+dstName;
		}
		for( i=0; i<overlap; i++ )
		{
			int srcIndex=_loopBeg-1-i;
			int dstIndex=_loopEnd-1-i;
			float merge=(overlap-1-i)*invOverlap*0.5;
			CString srcName,dstName;
			srcName.Format("%s%02d%s",(LPCTSTR)prefix,srcIndex,(LPCTSTR)postfix);
			dstName.Format("%s%02d%s",(LPCTSTR)prefix,dstIndex,(LPCTSTR)postfix);
			Picture dest,src;
			if( dest.Load(dstName)<0 ) throw CString("Cannot load ")+dstName;
			if( src.Load(srcName)<0 ) throw CString("Cannot load ")+srcName;
			dest.Blend(src,1-merge,merge);
			if( dest.Save(dstName)<0 ) throw CString("Cannot save ")+dstName;
		}
		// remove overlapping regions
		for( i=_first; i<_loopBeg; i++ )
		{
			CString name;
			name.Format("%s%02d%s",(LPCTSTR)prefix,i,(LPCTSTR)postfix);
			if( remove(name)<0 ) throw CString("Cannot delete ")+name;
		}
		for( i=_loopEnd; i<=_last; i++ )
		{
			CString name;
			name.Format("%s%02d%s",(LPCTSTR)prefix,i,(LPCTSTR)postfix);
			if( remove(name)<0 ) throw CString("Cannot delete ")+name;
		}
		ScanAnimation(m_Name,_first,_last);
		ScanResolution(_resolX,_resolY);
		RecalcSize();
		UpdateData(FALSE);
		OnNormalizeName(); // do name normalization
	}
	catch( CString &e )
	{
		MessageBox(e,"Animation Manager",MB_ICONERROR|MB_OK);
	}
	catch( CFileException * )
	{
		MessageBox("Animation name not valid","Animation Manager",MB_ICONERROR|MB_OK);
	}
}

void CResAnimDlg::OnResolHalf() 
{
	Busy();
	// reduce resolution
	int bigger=_resolX;
	if( bigger<_resolY ) bigger=_resolY;
	if( bigger>2 )
	{
		bigger>>=1;
		// convert all files and reduce resolution

		try
		{
			CString prefix,postfix;
			if( ExtractAnimatedName(m_Name,prefix,postfix)<0 )
			{
				// invalid name
				AfxThrowFileException(0); 
			}

			// convert from source to destination
			for( int i=_first; i<=_last; i++ )
			{
				CString name;
				name.Format("%s%02d%s",(LPCTSTR)prefix,i,(LPCTSTR)postfix);
				DoConvertFile(name,name,bigger);
			}
			_resolX>>=1;
			_resolY>>=1;
			RecalcSize();
			UpdateData(FALSE);
		}
		catch( CString &e )
		{
			MessageBox(e,"Animation Manager",MB_ICONERROR|MB_OK);
		}
		catch( CFileException * )
		{
			MessageBox("Animation name not valid","Animation Manager",MB_ICONERROR|MB_OK);
		}
	}
}

// callback for pal2pac library

void ConvertError( char const *source, char const*target )
{
	//MessageBox(NULL,
}

bool AskIfOverwrite(char const *,char const *,unsigned int,unsigned int)
{
	return true;
}

void FailHook( char const *text )
{
	OutputDebugString(text);
}

void LogF(char const *txt,...)
{
}



void CResAnimDlg::AutoLoop()
{
	int overlap=(_last+1-_first)/4;
	if( overlap>1 )
	{
		_loopBeg=overlap+_first;
		_loopEnd=_last+1-overlap;
	}
	else
	{
		_loopBeg=_first;
		_loopEnd=_last;
	}
}

void CResAnimDlg::RecalcSize()
{
	_sizeKB=(_resolX*_resolY*(_last+1-_first)*2)/1024;
}

void CResAnimDlg::OnNormAlpha() 
{
	// normalize alpha channel
	Busy();
	try
	{
		CString prefix,postfix;
		if( ExtractAnimatedName(m_Name,prefix,postfix)<0 )
		{
			// invalid name
			AfxThrowFileException(0); 
		}

		// convert from source to destination
		for( int i=_first; i<=_last; i++ )
		{
			CString name;
			name.Format("%s%02d%s",(LPCTSTR)prefix,i,(LPCTSTR)postfix);
			Picture dest;
			if( dest.Load(name)<0 ) throw CString("Cannot load ")+name;
			dest.RemoveBlackFromAlpha();
			if( dest.Save(name)<0 ) throw CString("Cannot save ")+name;
		}
	}
	catch( CString &e )
	{
		MessageBox(e,"Animation Manager",MB_ICONERROR|MB_OK);
	}
	catch( CFileException * )
	{
		MessageBox("Animation name not valid","Animation Manager",MB_ICONERROR|MB_OK);
	}
}

void CResAnimDlg::Busy()
{
	::SetCursor(_wait);
}

void CResAnimDlg::Ready()
{
}
