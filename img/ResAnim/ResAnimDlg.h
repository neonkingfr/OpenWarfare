// ResAnimDlg.h : header file
//

#if !defined(AFX_RESANIMDLG_H__F72B6449_8C0B_11D1_8D3A_00A0C96E64B5__INCLUDED_)
#define AFX_RESANIMDLG_H__F72B6449_8C0B_11D1_8D3A_00A0C96E64B5__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

/////////////////////////////////////////////////////////////////////////////
// CResAnimDlg dialog

class CResAnimDlg : public CDialog
{
// Construction
public:
	void Ready();
	void Busy();
	void RecalcSize();
	static CString WildcardAnimation( CString from );
	static int ExtractAnimatedName( CString from, CString &prefix, CString &postfix );
	CResAnimDlg(CWnd* pParent = NULL);	// standard constructor
	~CResAnimDlg();

// Dialog Data
	//{{AFX_DATA(CResAnimDlg)
	enum { IDD = IDD_RESANIM_DIALOG };
	CString	m_Name;
	int		_first;
	int		_last;
	int		_loopBeg;
	int		_loopEnd;
	int		_resolX;
	int		_resolY;
	int		_sizeKB;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CResAnimDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	HCURSOR _wait;

	// Generated message map functions
	//{{AFX_MSG(CResAnimDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnNormalizeName();
	afx_msg void OnFrameHalf();
	afx_msg void OnBrowse();
	afx_msg void OnSaveAs();
	afx_msg void OnLoopExtract();
	afx_msg void OnResolHalf();
	afx_msg void OnNormAlpha();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void AutoLoop();
	void ScanResolution( int &w, int &h );
	bool OpenFile();
	void ScanAnimation( CString name, int &first, int &last );
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_RESANIMDLG_H__F72B6449_8C0B_11D1_8D3A_00A0C96E64B5__INCLUDED_)
