#include <Es/Common/win.h>
#include <direct.h>
#include <io.h>
#include <fcntl.h>
#include <El/elementpch.hpp>
#include <Es/Strings/rString.hpp>
#include <Es/Files/filenames.hpp>
#include <El/Common/perfProf.hpp>
#include "../pics\macros.h"
#include "../pics\fileutil.h"
#include "../pal2pacL.hpp"
#include "../Picture\picture.hpp"
#include "../pal2pac/resource.h"

// command line interface to dll

static BOOL wasError=TRUE;

static int SConvertFile( const char *DPath, const char *SPath, int size )
{
  int maxSize=GetMaxSize(DPath);
  //printf("Limit size of %s to %d\n",DPath,maxSize);
  if( maxSize>size ) maxSize=size;
  return ConvertFile(DPath,SPath,maxSize);
}

static int SConvert( const char *DPath, const char *SPath, int size )
{
  #if _ENABLE_PERFLOG
    GPerfCounters.Enable();
    int time = GetTickCount();
  #endif
  int maxSize=GetMaxSize(DPath);
  //printf("Limit size of %s to %d\n",DPath,maxSize);
  if( maxSize==0 ) maxSize=-1;
  if( maxSize>size ) maxSize=size;
  int ret = Convert(DPath,SPath,maxSize);
  #if _ENABLE_PERFLOG
    LogF("time %d ms",GetTickCount()-time);
    GPerfCounters.Diagnose();
  #endif
  return ret;
}

static int Size=-1; // output size can be limited

static int ConvertDir( const char *S )
{
  int test;
  
  test=open(S,O_RDONLY);
  if( test>=0 )
  {
    close(test);
    // single file
    PathName D;
    strcpy(D,S);
    const char *sExt=NajdiPExt(NajdiNazev(S));
    char *dExt=NajdiPExt(NajdiNazev(D));
    if (!strcmpi(sExt,".png") || !strcmpi(sExt,".tga") || !strcmpi(sExt,".paa"))
    {
      strcpy(dExt,".paa");
    }
    else
    {
      strcpy(dExt,".pac");
      //strcpy(dExt,".pcl");
    }
    int ret=SConvert(D,S,Size);
    //remove(S);
    return ret;
  }
  
  printf("Scanning folder %s\n",S);

  PathName wPath;
  strcpy(wPath,S);
  const char *N=NajdiNazev(wPath);
  if( !strchr(N,'?') && !strchr(N,'*') )
  {
    EndChar(wPath,'\\');
    strcat(wPath,"*.*");
  }

   _finddata_t c_file;
  long hFile;
  if( (hFile=_findfirst(wPath,&c_file)) != -1L )
  {
    for(;;)
    {
      char *N;
      PathName DPath;
      PathName SPath;
      if( strcmp(c_file.name,".") && strcmp(c_file.name,"..") )
      {

        strcpy(SPath,wPath);

        N=NajdiNazev(SPath);
        strcpy(N,c_file.name);
        if( c_file.attrib&_A_SUBDIR )
        {
          ConvertDir(SPath);
        }

        if
        (
          !strcmpi(NajdiPExt(c_file.name),".gif") ||
          !strcmpi(NajdiPExt(c_file.name),".tga") ||
          !strcmpi(NajdiPExt(c_file.name),".png") ||
          !strcmpi(NajdiPExt(c_file.name),".paa") ||
          !strcmpi(NajdiPExt(c_file.name),".pac")
        )
        {
          strcpy(DPath,SPath);
          const char *sExt=NajdiPExt(NajdiNazev(SPath));
          char *dExt=NajdiPExt(NajdiNazev(DPath));
          if( !strcmpi(sExt,".png") || !strcmpi(sExt,".tga") || !strcmpi(sExt,".paa") )
          {
            strcpy(dExt,".paa");
          }
          else
          {
            strcpy(dExt,".pac");
          }

          if( DoConvertFile(DPath,SPath,Size)<0 ) wasError=TRUE;
        }
      }
      if( _findnext(hFile,&c_file)!=0 ) break;
    }
    _findclose(hFile);
  }
  return 0;
}



static HANDLE hConsoleOutput,hConsoleInput;
  
static void ExitCon()
{
  if( wasError )
  {
    DWORD ret;
    static const char text[]="\n-----------------------\nPress ENTER to continue";
    char buf[80];
    WriteConsole(hConsoleOutput,text,strlen(text),&ret,NULL);
    ReadConsole(hConsoleInput,buf,sizeof(buf),&ret,NULL);
  }
}

void ConvertError(const char *D, const char *S, RString err)
{
  printf("Error (%s) %s->%s\n", (const char*)err, S, D);
}

void ConvertWarning(const char *D, const char *S, RString err)
{
  printf("Warning (%s) %s->%s\n", (const char*)err, S, D);
}

bool AskIfOverwrite( const char *D, const char *S, unsigned dTime, unsigned sTime )
{
  printf("Overwrite %s->%s\n",S,D);
  return true;
}

// DLL has its own static version

static PathName BankName,WaterName;

static bool InitConfig()
{
  // load texture hits config
  RString retString;
  LoadPictureConfigRetVal ret = LoadPictureConfig(retString);
  if (ret!=LoadPictureConfigOK)
  {
    RString msg;
    UINT msgId = IDS_MSG_NOCONFIG;
    char mod[256];
    HINSTANCE instance = NULL;
    GetModuleFileName(instance,mod,sizeof(mod));
    const char *modname = GetFilenameExt(mod);
    switch (ret)
    {
      case LoadPictureConfigNoRegistry: msgId = IDS_MSG_NOREGISTRY; break;
      case LoadPictureConfigNoFile: msgId = IDS_MSG_NOCONFIG; break;
      case LoadPictureConfigBadFile: msgId = IDS_MSG_BADCONFIG; break;
      case LoadPictureConfigOldExe: msgId = IDS_MSG_CONFIGOLDAPP; break;
    }


    char msgs[1024]; //,buf[1024]; //,app[1024];
    //LoadString(instance,IDS_APPNAME,app,sizeof(app));
    LoadString(instance,msgId,msgs,sizeof(msgs));
    printf(msgs,(const char *)retString,(const char *)modname);
    printf("\n");
    return false;
  }
  return true;
}


int main( int argc, const char *argv[] )
{
  CONSOLE_SCREEN_BUFFER_INFO csbi;
  BOOL bLaunched;
  
  // Lets try a trick to determine if we were 'launched' as a seperate
  // screen, or just running from the command line.
  
  hConsoleOutput = GetStdHandle(STD_OUTPUT_HANDLE);
  hConsoleInput = GetStdHandle(STD_INPUT_HANDLE);
  GetConsoleScreenBufferInfo(hConsoleOutput, &csbi);
  bLaunched = ((csbi.dwCursorPosition.X==0) && (csbi.dwCursorPosition.Y==0));
  if ((csbi.dwSize.X<=0) || (csbi.dwSize.Y <= 0)) bLaunched = FALSE;

  if( bLaunched ) atexit(ExitCon);

  //CoInitialize(NULL); // initialize COM interface
  
  const char *S=NULL,*D=NULL;
  
  const char **argv0=argv;
  while( argc>1 )
  {
    const char *arg=argv[1];
    if( *arg=='-' || *arg=='/' )
    {
      arg++;
      static const char SizeText[]="size=";
      if( !strnicmp(arg,SizeText,strlen(SizeText)) )
      {
        arg+=strlen(SizeText);
        Size=atoi(arg);
      }
      else goto Usage;
    }
    else
    {
      if( !S ) S=arg;
      else if( !D ) D=arg;
      else goto Usage;
    }
    argc--,argv++;
  }
  if( !S )
  {
    Usage:
    printf("Arguments used:\n");
    for( int i=1; argv0[i]; i++ )
    {
      printf("'%s'\n",argv0[i]);
    }
    
    printf("Usage:\tpal2pace [options] <source> [<destination>]\n");
    printf("\n");
    printf("Options:\n\t-size=<n>\n");
    printf("\n");
    return 1;
  }

  if (!InitConfig())
  {
    return 1;
  }
  int ret=-1;
  LoadTextureInfos();
  if( S && D ) ret=SConvert(D,S,Size);
  else if( S ) ret=ConvertDir(S);
  if( ret==0 ) wasError=FALSE;
  if( !wasError ) return 0;
  return 1;
}

