/* konvertor obrazku */
/* SUMA 2/1995 */


// #define NOMINMAX
// #include <windows.h>
// #undef RGB

#include <string.h>
#include <stdlib.h>
#include <limits.h>
#include <io.h>
#include <dos.h>
#include <stdarg.h>
#include <fcntl.h>
#include <sys\stat.h>
#include "pics\macros.h"
#include "pics\fileutil.h"
#include "pics\imgload.h"
#include <math.h>
#include <Es/essencepch.hpp>
#include <Es/Framework/debugLog.hpp>
#include <Es/Memory/fastAlloc.hpp>
#include <Es/Types/pointers.hpp>
#include <Es/Containers/array.hpp>
#include <Es/Strings/rstring.hpp>
#include <Es/Files/filenames.hpp>

#pragma warning(disable:4530)
#include <fstream>
#include <strstream>

//#include "textLib.hpp"

//#include <class\lzcompr.hpp>
//#include <class\pointers.hpp>
#include <Es/Memory/checkmem.hpp>

#include "picture\picture.hpp"

//using namespace std;

// sharpness of interpolation

static float XBorder=1.0/8,YBorder=1.0/8; // relative border size

// Shape forces "Nice Banks" (TM) to be used
//static int Shape=0; // special shapes for banks

static int Size=-1; // output size can be limited

Picture Background;
Picture BlendMask;

// how shape works:

// Shape is binary combination, that says which segments are filled with water

// instead of water we use Background for interpolation
// only when Background is transparent, we use water

// shape coding is:
//        top
// left 1 2 right
//       4 8
//       bottom

// in water part is used interpolation to sand (always)

PPixel Interpol( PPixel rgb0, PPixel rgb1, float coef );

int Picture::Bilinear( const Picture &s1, const Picture &s2, const Picture &s3 )
{
	Picture *w0=this;
	const Picture *w1=&s1;
	const Picture *w2=&s2;
	const Picture *w3=&s3;
	
	w0->ConvertARGB(); //,w0->NormalizeAlpha();
	w1->ConvertARGB(); //,w1->NormalizeAlpha();
	w2->ConvertARGB(); //,w2->NormalizeAlpha();
	w3->ConvertARGB(); //,w3->NormalizeAlpha();
	Background.ConvertARGB(); //,Background.NormalizeAlpha();
	BlendMask.ConvertARGB(); //,BlendMask.NormalizeAlpha();
	if( Background._argb ) Background.Resize(_w,_h);
	if( BlendMask._argb ) BlendMask.Resize(_w,_h);
	
	// all pictures must have the same dimensions
	if( w0->W()!=W() || w1->H()!=H() ) return 0; // no blending possible
	if( w1->W()!=w0->W() || w1->H()!=w0->H() ) return 0; // no blending possible
	if( w2->W()!=w0->W() || w2->H()!=w0->H() ) return 0; // no blending possible
	if( w3->W()!=w0->W() || w3->H()!=w0->H() ) return 0; // no blending possible
	
	int xBorder=int(XBorder*_w); // convert from relative to absolute
	int yBorder=int(YBorder*_h);
	
	int x,y;
	// create blended picture in rgb
	PPixel *newArgb=new PPixel[_w*_h];
	float invWB=1.0/(_w-2*xBorder);
	float invHB=1.0/(_h-2*yBorder);
	for( y=0; y<_h; y++ ) for( x=0; x<_w; x++ )
	{
		// leave borders intact - this helps to have correct palette
		float xc,yc;
		if( x<xBorder ) xc=0;
		else if( x>_w-xBorder ) xc=1;
		else xc=(float)(x-xBorder)*invWB;
		
		if( y<yBorder ) yc=0;
		else if( y>_h-yBorder ) yc=1;
		else yc=(float)(y-yBorder)*invHB;
		
		PPixel rgbTop=Interpol(w0->_argb[y*_w+x],w1->_argb[y*_w+x],xc);
		PPixel rgbBot=Interpol(w2->_argb[y*_w+x],w3->_argb[y*_w+x],xc);
		PPixel rgb=Interpol(rgbTop,rgbBot,yc);
		
		if( BlendMask._argb && Background._argb )
		{
			PPixel backCol=BlendMask._argb[y*_w+x];
			float blendCoef=(backCol&0xff)*(1/255.0f);
			PPixel rgbWater=Background._argb[y*_w+x];
			rgb=Interpol(rgbWater,rgb,blendCoef);
		}
		newArgb[y*_w+x]=rgb;
	}

	if( Background._argb && Background._noAlphaDither )
	{
		// convert from rgb to palette
		// do not use alpha dithering on pictures with transparent background
		_noAlphaDither=true;
	}
	
	// palette will be created before saving (after size reduction)
	if( _argb ) delete[] _argb,_argb=NULL;
	if( _picData ) delete[] _picData,_picData=NULL;
	_lenPal=0;

	_argb=newArgb;
	
	return 1;
	
}

/*
*/

extern "C" int __declspec( dllimport )  __stdcall CopyFile
(
	const char *lpExistingFileName, 
	const char *lpNewFileName, 
	int bFailIfExists 
	// flag for operation if file exists 
); 
 

bool AskIfOverwrite( const char *D, const char *S, unsigned dTime, unsigned sTime );
void ConvertError(const char *D, const char *S, RString err);
void ConvertWarning(const char *D, const char *S, RString err);

int DoConvertFile( const char *D, const char *S, int size )
{
  // Error description
  RString err;
  RString saveErr;

  // Load picture
	Picture obr;
	printf("%s->%s\n", S , D);
	if (!(obr.Load(S) >= 0 && obr.Valid()))
  {
    err = RString("Loading of img failed");
    goto Error;
  }

  // no need to sort palette - we do no save palette pictures any more
	//obr.SortPal();

  // Check we have a power of 2 texture
	if (!obr.IsPowerOf2())
  {
    err = RString("Img is not of power of 2 size");
    goto Error;
  }

  // Transform the image as required
  if( size>4 )
  {
    while( obr.Width()>size || obr.Height()>size )
    {
      if (obr.Width()>8 && obr.Height()>8)
      {
        if(!obr.Halfsize()) break;
      }
      else
      {
        ConvertWarning(D, S,Format("min size is %dx%d",obr.Width(),obr.Height()));        
        break;
      }
    }
  }

  // Save the image
	if (obr.Save(D, NULL, &saveErr) < 0)
	{
    err = Format("Img saving failed (%s)", (const char*)saveErr);
	  goto Error;
	}

  // Return success
	return 0;


	//printf("Cannot convert %s.\n",S);
	Error:
	ConvertError(D, S, err);
	return -1;
}

// note: we expect caller (binMake or similar tool) wil check if files are up-to-date or not
int DoCopyFile( const char *D, const char *S, int size )
{
	return DoConvertFile(D,S,size);
}

int ConvertFile( const char *D, const char *S, int size )
{
	return DoConvertFile(D,S,size);
}

//#define MAX_PATH 256

static int AnimatedNumber( const char *name )
{
	// animated name has structure: name.number.ext (e.g. buch.01.tga)
	const char *n=NajdiNazev(name);
	const char *nn=strchr(n,'.');
	if( !nn ) return -1;
	char num[256];
	strcpy(num,nn+1);
	char *ext=strchr(num,'.');
	if( !ext ) return -1;
	*ext=0;
	return atoi(num);
}

static int AnimatedName( const char *name, char *prefix, char *postfix )
{
	strcpy(prefix,name);
	char *n=NajdiNazev(prefix);
	char *nn=strchr(n,'.');
	if( !nn ) return -1;
	int ret=atoi(nn+1);
	char *ext=strchr(nn+1,'.');
	strcpy(postfix,ext); // use postfix
	*++nn=0; // terminate prefix
	if( !ext ) return -1;
	return ret;
}

int Convert( const char *D, const char *S, int size )
{
	
	//if( AnimatedNumber(S)<0 )
  return ConvertFile(D,S,size);
  /*
	// convert animated picture
	if( AnimatedNumber(D)<0 )
	{
		printf("Animated texture syntax error.\n");
		exit(1);
	}
	char sn[MAX_PATH];
	char dn[MAX_PATH];
	strcpy(sn,S);
	strcpy(dn,D);
	char sPrefix[MAX_PATH];
	char sPostfix[MAX_PATH];
	char dPrefix[MAX_PATH];
	char dPostfix[MAX_PATH];
	int start=AnimatedNumber(S);
	AnimatedName(S,sPrefix,sPostfix);
	AnimatedName(D,dPrefix,dPostfix);
	for( int i=start; ; i++ )
	{
		char sna[MAX_PATH];
		char dna[MAX_PATH];
		sprintf(sna,"%s%02d%s",sPrefix,i,sPostfix);
		sprintf(dna,"%s%02d%s",dPrefix,i,dPostfix);
		if( access(sna,0)<0 ) return 0; // file does not exist
		if( ConvertFile(dna,sna,size)<0 ) break;
	}
	return 0;
  */
}

int Copy( const char *D, const char *S, int size )
{
	
	if( AnimatedNumber(S)<0 ) return DoCopyFile(D,S,size);
	// convert animated picture
	if( AnimatedNumber(D)<0 )
	{
		printf("Animated texture syntax error.\n");
		exit(1);
	}
	char sn[MAX_PATH];
	char dn[MAX_PATH];
	strcpy(sn,S);
	strcpy(dn,D);
	char sPrefix[MAX_PATH];
	char sPostfix[MAX_PATH];
	char dPrefix[MAX_PATH];
	char dPostfix[MAX_PATH];
	int start=AnimatedNumber(S);
	AnimatedName(S,sPrefix,sPostfix);
	AnimatedName(D,dPrefix,dPostfix);
	for( int i=start; ; i++ )
	{
		char sna[MAX_PATH];
		char dna[MAX_PATH];
		sprintf(sna,"%s%02d%s",sPrefix,i,sPostfix);
		sprintf(dna,"%s%02d%s",dPrefix,i,dPostfix);
		if( access(sna,0)<0 ) return 0; // file does not exist
		if( DoCopyFile(dna,sna,size)<0 ) break;
	}
	return 0;
}

int DoCombine( const char *D, const char *S1, const char *S2, const char *S3, const char *S4 )
{
	// D is destination, S1,S2 are sources

	printf( "%s=%s+%s+%s+%s\n",D,S1,S2,S3,S4 );
	
	Picture Obr1,Obr2;
	Picture Obr3,Obr4;
	if( Obr1.Load(S1)>=0 && Obr2.Load(S2)>=0 )
	if( Obr3.Load(S3)>=0 && Obr4.Load(S4)>=0 )
	{
		Obr1.Prefer(Obr1);
		Obr1.Prefer(Obr2);
		Obr1.Prefer(Obr3);
		Obr1.Prefer(Obr4);
		// maybe we could also prefer backround colors
		if( Background.Valid() ) Obr1.Prefer(Background);
		Obr1.Bilinear(Obr2,Obr3,Obr4);
		
		if( Size>1 )
		{
			while( Obr1.Width()>Size || Obr1.Height()>Size )
			{
				Obr1.Halfsize();
			}
		}
	
		//if( Obr1.SaveMipmaps(D)<0 ) goto Error;
		if( Obr1.Save(D)<0 ) goto Error;
		return 0;
		
		Error:
		printf("Cannot save %s.\n",D);
	}
	else
	{
		printf("Cannot load %s,%s.\n",S1,S2);
	}
	return -1;


}

int CombTexture
(
	const char *D, const char *S1, const char *S2, const char *S3, const char *S4,
	int size, const char *bank, const char *water
)
{
	if( bank && *bank )
	{
		if( BlendMask.Load(bank)<0 ) return -1;
	}
	else BlendMask.Delete();
	if( water && *water )
	{
		if( Background.Load(water)<0 ) return -1;
	}
	else Background.Delete();
	Size=size;
	return DoCombine(D,S1,S2,S3,S4);
}

// global texture info

#define ObjConfig "Software\\Suma\\Objektiv"

static RString RegStringValue( HKEY key, const char *name )
{
	static BYTE buf[256];
	DWORD bufSize=sizeof(buf);
	if( ERROR_SUCCESS==RegQueryValueEx(key,name,NULL,NULL,buf,&bufSize) )
	{
		return (char *)buf;
	}
	return RString();
}

struct TxtInfo
{
	char name[32];
	short oldLevel,maxSize;
	int flags;
};

TypeIsSimple(TxtInfo);

// load global texture import information
static char InfoName[512];
static AutoArray<TxtInfo> TextureInfos;
static unsigned TInfoTime;

static void ConfigRegistry()
{
	HKEY key;
	if
	(
		RegOpenKeyEx
		(
			HKEY_LOCAL_MACHINE,ObjConfig,
			0,KEY_READ,&key
		)==ERROR_SUCCESS
	)
	{
		RString r;
		r=RegStringValue(key,"Final Txt Source");
		if( r ) strcpy(InfoName,r);
		strcpy(InfoName,r);
		RegCloseKey(key);
		strcat(InfoName,"\\TexInfo.bin");
	}
}

static unsigned int FileTime( const char *path )
{
	if( !path ) return NULL;
	struct stat info;
	if( stat(path,&info)<0 ) return 0;  // does not exist - return oldest possible
	return info.st_mtime;
}

bool LoadTextureInfos()
{
	ConfigRegistry();
	QIFStream in;
	unsigned time=FileTime(InfoName);
	if( time>TInfoTime )
  {
	  TInfoTime=time;
	  in.open(InfoName);
	  TextureInfos.Clear();
	  for(;;)
	  {
		  TxtInfo info;
		  in.read((char *)&info,sizeof(info));
		  if( in.fail() || in.eof() ) break;
		  TextureInfos.Add(info);
	  }
	  #if 0
	  char buf[256];
	  sprintf(buf,"Texture infos: %d",TextureInfos.Size());
	  MessageBox(NULL,buf,"Texture Conversion",MB_OK);
	  #endif
  }

  return true;
}

static TxtInfo &GetInfoShortName( const char *name )
{
	int i;
	for( i=0; i<TextureInfos.Size(); i++ )
	{
		TxtInfo &info=TextureInfos[i];
		if( !strcmpi(name,info.name) ) return info;
	}
	i=TextureInfos.Add();
	TxtInfo &info=TextureInfos[i];
	strncpy(info.name,name,sizeof(info.name));
	info.oldLevel=0;
	info.maxSize=1024;
	info.flags=0;
	return info;
}

int GetMaxSize( const char *name )
{
	const char *n=strrchr(name,'\\');
	if( n ) return GetInfoShortName(n+1).maxSize;
	else return GetInfoShortName(name).maxSize;
}
