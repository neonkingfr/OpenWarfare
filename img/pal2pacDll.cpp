/* konvertor obrazku */
/* SUMA 2/1995 */

#include <windows.h>
#include <shlobj.h>
#include <stdio.h>
#include <Es/essencepch.hpp>
#include <Es/Containers/array.hpp>
#include <Es/Memory/fastAlloc.hpp>
#include <Es/Memory/checkMem.hpp>
#include <Es/Files/filenames.hpp>
#include "../pics/macros.h"
#include "../pics/fileutil.h"
#include "../pics/imgload.h"
#include "../Picture\picture.hpp"
#include "../pal2pac/resource.h"

//#include <fstream.h>
//#include <class\rString.cpp>

#include "../pal2pacL.hpp"
#include "../pal2pac.hpp"
// DLL entry points



static const char *FileInfo( const char *name, unsigned time )
{
	return name;
}

void ConvertError(const char *D, const char *S, RString err)
{
	char buf[512];
	_snprintf(buf,sizeof(buf),"Cannot convert '%s'\nto '%s'.",S,D);
	MessageBox(NULL,buf,"Texture Conversion",MB_ICONWARNING|MB_OK|MB_TASKMODAL);
}

bool AskIfOverwrite( const char *D, const char *S, unsigned dTime, unsigned sTime )
{
	char buf[512];
	_snprintf(buf,sizeof(buf),"Overwrite texture '%s'\nwith '%s'?",D,S);
	int ret=MessageBox(NULL,buf,"Texture Conversion",MB_ICONWARNING|MB_YESNO);
	return ret==IDYES;
}

int MYDLL UpdateTexture( const char *dest, const char *src, int size )
{
	// if source is .pac or .paa, copy only
	int maxSize=GetMaxSize(dest);
	if( size>maxSize ) size=maxSize;
	const char *sExt=NajdiPExt(NajdiNazev(src));
	const char *dExt=NajdiPExt(NajdiNazev(dest));
	if( strcmpi(sExt,dExt) )
	{ // format change required
		return Convert(dest,src,size);
	}
	else
	{
		return Copy(dest,src,size);
	}
}

int MYDLL AddTexture( const char *libPath, const char *textPath, int size )
{
	char dest[512];
	strcpy(dest,libPath);
	EndChar(dest,'\\');
	strcat(dest,NajdiNazev(textPath));
	
	if( !strcmpi(NajdiPExt(NajdiNazev(textPath)),".tga") )
	{
		strcpy(NajdiPExt(NajdiNazev(dest)),".PAA");
	}
	else
	{
		strcpy(NajdiPExt(NajdiNazev(dest)),".PAC");
	}
	
	return Convert(dest,textPath,size);
}

int MYDLL CombineTexture
(
	const char *D, const char *S1, const char *S2, const char *S3, const char *S4,
	int size, const char *bank, const char *water
)
{
	return CombTexture(D,S1,S2,S3,S4,size,bank,water);
}

void MYDLL TextureMix
(
	const char *dst,
	const char *rb, const char *lb, const char *rt, const char *lt,
	const char *bankDirectory, const char *bankTexture
)
{
	// lt is z+0,x+0
	// rt is z+0,x+1
	// lb is z+1,x+0
	// rb is z+1,x+1
	if( rb && lb && rt && lt )
	{
		// no bank necessary
		CombTexture(dst,rb,lb,rt,lt,1024,NULL,NULL);
	}
	else
	{
		// select bank name appropriatelly
		char bankName[256];
		strcpy(bankName,bankDirectory);
		EndChar(bankName,'\\');

    // detect water surface to enable bank generation
    int maskWater=0;
    if( !lt ) maskWater|=8,lt=bankTexture;
    if( !rt ) maskWater|=4,rt=bankTexture;
    if( !lb ) maskWater|=2,lb=bankTexture;
    if( !rb ) maskWater|=1,rb=bankTexture;
		sprintf(bankName+strlen(bankName),"bank%02d.pac",15-maskWater);
		CombTexture(dst,rb,lb,rt,lt,128,bankName,"#transparent#");
	}
}

COLORREF MYDLL TextureColor( const char *s )
{
	// read the texture
	Picture pic;
	pic.Load(s);
	long argb=pic.GetAverageColor();
	int r=(argb>>16)&0xff;
	int g=(argb>>8)&0xff;
	int b=(argb>>0)&0xff;
	return RGB(r,g,b);
}

DWORD MYDLL TextureColorARGB( const char *s )
{
	Picture pic;
	pic.Load(s);
	return pic.GetAverageColor();
}

void MYDLL CreateTextureData( TextureDataStruct &data, const char *name )
{
	data._w = data._h = 0;
	data._data = NULL;
	if (name)
	{
		Picture temp;
		if (temp.Load(name)>=0)
		{
			data._data = new DWORD[temp.W()*temp.H()];
			temp.RemoveBlackFromAlpha();
			temp.CopyARGB(data._data);
			data._h = temp.H();
			data._w = temp.W();
		}
	}
}

void MYDLL DestroyTextureData( TextureDataStruct &data )
{
	if (data._data) delete[] data._data;
	data._data = NULL;
	data._w = 0;
	data._h = 0;
}

static bool InitConfig(HINSTANCE instance)
{
	// load texture hits config
	RString retString;
	LoadPictureConfigRetVal ret = LoadPictureConfig(retString);
	if (ret!=LoadPictureConfigOK)
	{
		RString msg;
		UINT msgId = IDS_MSG_NOCONFIG;
		char mod[256];
		GetModuleFileName(instance,mod,sizeof(mod));
		const char *modname = GetFilenameExt(mod);
		switch (ret)
		{
			case LoadPictureConfigNoRegistry: msgId = IDS_MSG_NOREGISTRY; break;
			case LoadPictureConfigNoFile: msgId = IDS_MSG_NOCONFIG; break;
			case LoadPictureConfigBadFile: msgId = IDS_MSG_BADCONFIG; break;
			case LoadPictureConfigOldExe: msgId = IDS_MSG_CONFIGOLDAPP; break;
		}


		char msgs[1024],buf[1024],app[1024];
		LoadString(instance,IDS_APPNAME,app,sizeof(app));
		LoadString(instance,msgId,msgs,sizeof(msgs));
		snprintf(buf,sizeof(buf),msgs,(const char *)retString,(const char *)modname);
		MessageBox(NULL,buf,app,MB_OK|MB_ICONERROR);
		return false;
	}
	return true;
}

int APIENTRY DllMain( HANDLE hdll, DWORD	reason, LPVOID reserved )
{
	(void)hdll,(void)reserved;
	LoadTextureInfos();
	switch( reason )
	{
	case DLL_THREAD_ATTACH:
		 /* do thread initialization */
		 break;

	case DLL_THREAD_DETACH:
		 /* do thread cleanup */
		 break;
	case DLL_PROCESS_ATTACH:
		if (!InitConfig((HINSTANCE)hdll))
		{
			return FALSE;
		}
		 /* do process initialization */
		 break;
	case DLL_PROCESS_DETACH:
		 /* do process cleanup */
		 break;
	}
	return( 1 ); 			 /* indicate success */
	/* returning 0 indicates initialization failure */

}
