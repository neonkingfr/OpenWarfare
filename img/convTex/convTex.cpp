#include "stdafx.h"
#include <direct.h>
#include <io.h>
#include <fcntl.h>
#include "..\pics\macros.h"
#include "..\pics\fileutil.h"
#include "..\pal2pacL.hpp"

// command line interface to dll

static BOOL wasError=TRUE;

static int SConvertFile( const char *DPath, const char *SPath, int size )
{
	int maxSize=GetMaxSize(DPath);
	printf("Limit size of %s to %d",DPath,maxSize);
	if( maxSize>size ) maxSize=size;
	return ConvertFile(DPath,SPath,maxSize);
}

static int SConvert( const char *DPath, const char *SPath, int size )
{
	int maxSize=GetMaxSize(DPath);
	printf("Limit size of %s to %d",DPath,maxSize);
	if( maxSize==0 ) maxSize=-1;
	if( maxSize>size ) maxSize=size;
	return Convert(DPath,SPath,maxSize);
}

static int Size=-1; // output size can be limited

static int ConvertDir( const char *S )
{
	int test;
	
	test=open(S,O_RDONLY);
	if( test>=0 )
	{
		close(test);
		// single file
		PathName D;
		strcpy(D,S);
		const char *sExt=NajdiPExt(NajdiNazev(S));
		char *dExt=NajdiPExt(NajdiNazev(D));
		if( !strcmpi(sExt,".tga") || !strcmpi(sExt,".paa") )
		{
			strcpy(dExt,".paa");
		}
		else
		{
			strcpy(dExt,".pac");
			//strcpy(dExt,".pcl");
		}
		int ret=SConvert(D,S,Size);
		//remove(S);
		return ret;
	}
	
	printf("Scanning folder %s\n",S);

	PathName wPath;
	strcpy(wPath,S);
	const char *N=NajdiNazev(wPath);
	if( !strchr(N,'?') && !strchr(N,'*') )
	{
		EndChar(wPath,'\\');
		strcat(wPath,"*.*");
	}

	 _finddata_t c_file;
	long hFile;
	if( (hFile=_findfirst(wPath,&c_file)) != -1L )
	{
		for(;;)
		{
			char *N;
			PathName DPath;
			PathName SPath;
			if( strcmp(c_file.name,".") && strcmp(c_file.name,"..") )
			{

				strcpy(SPath,wPath);

				N=NajdiNazev(SPath);
				strcpy(N,c_file.name);
				if( c_file.attrib&_A_SUBDIR )
				{
					ConvertDir(SPath);
				}

				if
				(
					!strcmpi(NajdiPExt(c_file.name),".gif") ||
					!strcmpi(NajdiPExt(c_file.name),".tga") ||
					//!strcmpi(NajdiPExt(c_file.name),".paa") ||
					//!strcmpi(NajdiPExt(c_file.name),".pcl") ||
					!strcmpi(NajdiPExt(c_file.name),".pac")
				)
				{
					strcpy(DPath,SPath);
					const char *sExt=NajdiPExt(NajdiNazev(SPath));
					char *dExt=NajdiPExt(NajdiNazev(DPath));
					if( !strcmpi(sExt,".tga") || !strcmpi(sExt,".paa") )
					{
						strcpy(dExt,".paa");
					}
					else
					{
						strcpy(dExt,".pac");
						//strcpy(dExt,".pcl");
					}

					if( SConvertFile(DPath,SPath,Size)<0 ) wasError=TRUE;
				}
			}
			if( _findnext(hFile,&c_file)!=0 ) break;
		}
		_findclose(hFile);
	}
	return 0;
}



static HANDLE hConsoleOutput,hConsoleInput;
	
static void ExitCon()
{
	if( wasError )
	{
		DWORD ret;
		static const char text[]="\n-----------------------\nPress ENTER to continue";
		char buf[80];
		WriteConsole(hConsoleOutput,text,strlen(text),&ret,NULL);
		ReadConsole(hConsoleInput,buf,sizeof(buf),&ret,NULL);
	}
}

void __cdecl LogF( const char *format, ... )
{
	char buf[512];
		
	va_list arglist;
	va_start( arglist, format );

	vsprintf(buf,format,arglist);
	strcat(buf,"\n");
	OutputDebugString(buf);

	#if 0
		// create or append to session log
		static bool notFirst;
		
		FILE *f;
		if( notFirst )  f=fopen("debug.log","at");
		else f=fopen("debug.log","wt"),notFirst=true;
		if( !f ) return;
		vfprintf(f,format,arglist);
		fputc('\n',f);
		fclose(f);
	#endif

	va_end( arglist );

}

void __cdecl FailHook( const char *text )
{
	OutputDebugString(text);
	__asm int 3
}

void ConvertError( const char *D, const char *S )
{
	printf("Error %s->%s\n",S,D);
}

bool AskIfOverwrite( const char *D, const char *S, unsigned dTime, unsigned sTime )
{
	printf("Overwrite %s->%s\n",S,D);
	return true;
}

typedef enum {ComNone,ComCombine} Command;

// vars not used...
static float XBorder=1.0/8,YBorder=1.0/8; // relative border size
// DLL has its own static version

static PathName BankName,WaterName;


int main( int argc, const char *argv[] )
{
	CONSOLE_SCREEN_BUFFER_INFO csbi;
	BOOL bLaunched;
	
	// Lets try a trick to determine if we were 'launched' as a seperate
	// screen, or just running from the command line.
	
	hConsoleOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	hConsoleInput = GetStdHandle(STD_INPUT_HANDLE);
	GetConsoleScreenBufferInfo(hConsoleOutput, &csbi);
	bLaunched = ((csbi.dwCursorPosition.X==0) && (csbi.dwCursorPosition.Y==0));
	if ((csbi.dwSize.X<=0) || (csbi.dwSize.Y <= 0)) bLaunched = FALSE;

	if( bLaunched ) atexit(ExitCon);

	//CoInitialize(NULL); // initialize COM interface
	
	const char *S=NULL,*D=NULL,*T0=NULL,*T1=NULL,*T2=NULL;
	
	Command command=ComNone;
	const char **argv0=argv;
	while( argc>1 )
	{
		const char *arg=argv[1];
		if( *arg=='-' || *arg=='/' )
		{
			arg++;
			static const char XBText[]="xborder=";
			static const char YBText[]="yborder=";
			//static const char ShapeText[]="shape=";
			static const char MaskText[]="mask=";
			static const char WaterText[]="water=";
			static const char SizeText[]="size=";
			if( !strcmpi(arg,"combine") ) command=ComCombine;
			else if( !strnicmp(arg,XBText,strlen(XBText)) )
			{
				arg+=strlen(XBText);
				XBorder=(float)atof(arg);
			}
			else if( !strnicmp(arg,YBText,strlen(YBText)) )
			{
				arg+=strlen(YBText);
				YBorder=(float)atof(arg);
			}
			else if( !strnicmp(arg,SizeText,strlen(SizeText)) )
			{
				arg+=strlen(SizeText);
				Size=atoi(arg);
			}
			else if( !strnicmp(arg,MaskText,strlen(MaskText)) )
			{
				arg+=strlen(MaskText);
				printf("Bank %s\n",arg);
				strcpy(BankName,arg);
			}
			else if( !strnicmp(arg,WaterText,strlen(WaterText)) )
			{
				arg+=strlen(WaterText);
				printf("Water %s\n",arg);
				strcpy(WaterName,arg);
			}
			else goto Usage;
		}
		else
		{
			if( !S ) S=arg;
			else if( !D ) D=arg;
			else if( !T0 ) T0=arg;
			else if( !T1 ) T1=arg;
			else if( !T2 ) T2=arg;
			else goto Usage;
		}
		argc--,argv++;
	}
	if( command==ComNone && T0 ) goto Usage;
	if( command!=ComNone && !T2 ) goto Usage;
	if( !S )
	{
		Usage:
		printf("Arguments used:\n");
		for( int i=1; argv0[i]; i++ )
		{
			printf("'%s'\n",argv0[i]);
		}
		
		printf("Usage:\tpal2pac [saveoptions] <source> [<destination>]\n");
		printf("or\t pal2pac -combine [saveoptions] [options] <source1> <source2> <source3> <source4> <destination>\n");
		printf("\n");
		printf("SaveOptions:\t-size=<n>\n");
		printf("\n");
		printf("Options:\t-xborder=<bordersize> (0<bordersize<0.5, default 0.125)\n");
		printf("\t-yborder=<bordersize>\n");
		printf("\t-mask=<maskname>\n");
		printf("\t-water=<watername>\n");
		exit(1);
	}

	int ret=-1;
	if( command!=ComNone )
	{
		//if( !*BankName ) goto Usage;
		//if( !*WaterName ) goto Usage;
		ret=CombTexture
		(
			T2,S,D,T0,T1,
			Size,BankName,WaterName
		);
	}
	else
	{
		LoadTextureInfos();
		if( S && D ) ret=SConvert(D,S,Size);
		else if( S ) ret=ConvertDir(S);
	}
	if( ret==0 ) wasError=FALSE;
	if( !wasError ) return 0;
	return 1;
}

