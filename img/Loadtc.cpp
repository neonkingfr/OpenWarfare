/* �ten� do True Color */
/* SUMA 1993/4-1995/12 */

#include <stdlib.h>
//#include <tos.h>
#include <ctype.h>
#include <string.h>
#include "pics\macros.h"
#include "pics\imgload.h"
#include "pics\memspc.h"
#include "pics\fileutil.h"

#include "obrazky.h"

static void Konvert888to555( Obrazek *Obr )
{
	long L=(long)Obr->W*Obr->H,l=L;
	byte *B=(byte *)Obr->Obr;
	word *D=(word *)Obr->Obr;
	while( --L>=0 )
	{
		word r=*B++;
		word g=*B++;
		word b=*B++;
		*D++=((r>>3)<<10)|((g>>3)<<5)|(b>>3);
	}
	Obr->NPlanu=16;
}

static void Konvert8888to4444( Obrazek *Obr )
{
	long L=(long)Obr->W*Obr->H,l=L;
	byte *B=(byte *)Obr->Obr;
	word *D=(word *)Obr->Obr;
	while( --L>=0 )
	{
		word a=*B++;
		word r=*B++;
		word g=*B++;
		word b=*B++;
		*D++=((a>>4)<<12)|((r>>4)<<8)|((g>>4)<<4)|(b>>4);
	}
	Obr->NPlanu=16;
}

static int Konvert565to888( Obrazek *Obr )
{
	(void)Obr;
	/*
	long N=(long)Obr->W*Obr->H;
	struct rgb565 *S=(struct rgb565 *)Obr->Obr;
	char *D=(char *)malloc(N*3),*DD=D;
	if( DD )
	{
		struct rgb565 *SS=S;
		while( --N>=0 )
		{
			*D++=(char)(S->r<<3);
			*D++=(char)(S->g<<2);
			*D++=(char)(S->b<<3);
			S++;
		}
		free(SS);
		Obr->Obr=DD;
	}
	if( DD ) return 0;
	*/
	return -1;
}

static int NactiPCC( const char *N, Obrazek *Obr )
{
	Obr->Obr=PCCLoad(N,&Obr->W,&Obr->H);
	Obr->NPlanu=16;
	if( !Obr->Obr ) return -1;
	Obr->Direct=True;
	Obr->Gray=False;
	return 0;
}

static int NactiTGA( const char *N, Obrazek *Obr )
{
	Obr->Obr=TGALoad(N,&Obr->W,&Obr->H,&Obr->NPlanu);
	if( !Obr->Obr ) return -1;
	Obr->Direct=True;
	Obr->Gray=False;
	return 0;
}

int NactiTC( const char *S, Obrazek *Obr )
{
	int ret;
	const char *E=NajdiExt(NajdiNazev(S));
	if( !strcmpi(E,"PCC") ) ret=NactiPCC(S,Obr);
	else if( !strcmpi(E,"TGA") ) ret=NactiTGA(S,Obr);
	else ret=-1;
	if( ret>=0 )
	{
		printf("Source: %s %dx%dx%db\n",S,Obr->W,Obr->H,Obr->NPlanu);
	}
	return ret;
}

int NactiTC16( const char *S, Obrazek *Obr )
{
	int ret=NactiTC(S,Obr);
	if( ret>=0 )
	{
		if( Obr->NPlanu==32 )
		{
			printf("Transforming format from 32b to 16b (4444)\n");
			Konvert8888to4444(Obr);
		}
		if( Obr->NPlanu==24 )
		{
			printf("Transforming format from 24b to 16b (555)\n");
			Konvert888to555(Obr);
		}
		else if( Obr->NPlanu==16 ){}
		else
		{
			printf("Input format must be 16b or 24b\n");
			freeSpc(Obr->Obr);
			return -1;
		}
	}
	else printf("Source read error.\n");
	return ret;
}

int NactiTC24( const char *S, Obrazek *Obr )
{
	int ret=NactiTC(S,Obr);
	if( ret>=0 )
	{
		if( Obr->NPlanu==24 )
		{
		}
		else if( Obr->NPlanu==16 )
		{
			printf("Transforming format from 16b to 24b.\n");
			if( Konvert565to888(Obr) )
			{
				printf("Not enough memory.\n");
				exit(1);
			}
		}
		else
		{
			printf("Input format must be 16b or 24b.\n");
			freeSpc(Obr->Obr);
			return -1;
		}
	}
	return ret;
}
