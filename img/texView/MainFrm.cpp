// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "texView.h"
#include "texViewDoc.h"
#include "texViewView.h"

#include "MainFrm.h"

#include <El/Scc/scc.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern SccFunctions *Scc;

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_DROPFILES()
	ON_WM_CLOSE()
	ON_WM_ENDSESSION()
	ON_COMMAND(ID_VIEW_INFO_BAR, OnViewInfoBar)
	ON_UPDATE_COMMAND_UI(ID_VIEW_INFO_BAR, OnUpdateViewInfoBar)
	ON_COMMAND(ID_VIEW_SETTINGS_BAR, OnViewSettingsBar)
	ON_UPDATE_COMMAND_UI(ID_VIEW_SETTINGS_BAR, OnUpdateViewSettingsBar)
	ON_COMMAND(ID_SCC_OPTIONS, OnSccOptions)
	//}}AFX_MSG_MAP
	ON_UPDATE_COMMAND_UI(ID_INDICATOR_PAGE, OnUpdatePage)

  ON_COMMAND(ID_VIEW_RGB0, OnViewRgb0)
  ON_UPDATE_COMMAND_UI(ID_VIEW_RGB0, OnUpdateViewRgb0)
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_INDICATOR_PAGE,
	ID_SEPARATOR,           // status line indicator
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame()
{
	_setName="";
	_setNumber=0;
	_setCount=0;
	_textureName="";
  _displayRGB01=false;
}

CMainFrame::~CMainFrame()
{
  m_wndLViewType.Detach();
  m_wndRViewType.Detach();
  m_wndLFormat.Detach();
  m_wndRFormat.Detach();
}

void CMainFrame::LoadSccState(const char *regName)
{
  CWinApp* pApp = AfxGetApp();
  int connected = pApp->GetProfileInt(regName,"connected",0);
  if (connected)
  {
    CString server = pApp->GetProfileString(regName,"server");
    CString project = pApp->GetProfileString(regName,"project");
    CString workingDir = pApp->GetProfileString(regName,"workingDir");
    Scc->Init(
      connected ? (const char *)server : NULL,
      project.IsEmpty() ? (const char *)NULL : project,
      workingDir.IsEmpty() ? (const char *)NULL : workingDir,
      m_hWnd
    );
  }
}

void CMainFrame::SaveSccState(const char *regName)
{
  if (Scc->Opened())
  {
  	CWinApp* pApp = AfxGetApp();
    pApp->WriteProfileInt(regName,"connected",1);
    pApp->WriteProfileString(regName,"server",Scc->GetServerName());
    pApp->WriteProfileString(regName,"project",Scc->GetProjName());
    pApp->WriteProfileString(regName,"workingDir",Scc->GetLocalPath());
  }
}

void CMainFrame::OnSccOptions() 
{
  if (Scc->Opened())
  {
    //pApp->WriteProfileString(regName,"server",Scc->GetServerName());
    //pApp->WriteProfileString(regName,"project",Scc->GetProjName());
    //pApp->WriteProfileString(regName,"workingDir",Scc->GetLocalPath());
    Scc->ChooseProject();
  }
  else
  {
    Scc->Init(NULL,NULL,NULL,m_hWnd);
  }
}

void CMainFrame::LoadWndState()
{
  if ((GetKeyState(VK_SHIFT)&0x8000)==0 || (GetKeyState(VK_CONTROL)&0x8000)==0)
  {
    LoadBarState("BARS");
    // connect to SCC (use info. stored in registry is possible)
  }
  LoadSccState("Scc");
}

void CMainFrame::SaveWndState()
{
  SaveSccState("Scc");
  if ((GetKeyState(VK_SHIFT)&0x8000)==0 || (GetKeyState(VK_CONTROL)&0x8000)==0)
  {
    SaveBarState("BARS");
  }
}


void CMainFrame::FillViewCombo(CComboBox &combo, bool enableNone)
{
  combo.ResetContent();
  int item;
  if (enableNone)
  {
    item = combo.AddString("None"); combo.SetItemData(item,ViewNone);
  }
  item = combo.AddString("RGBA"); combo.SetItemData(item,ViewRGBA);
  item = combo.AddString("RGB"); combo.SetItemData(item,ViewRGB);
  item = combo.AddString("Alpha"); combo.SetItemData(item,ViewAlpha);
  item = combo.AddString("RGB - DR Compressed"); combo.SetItemData(item,ViewRGB_DR);
  item = combo.AddString("Channel R"); combo.SetItemData(item,ViewChannelR);
  item = combo.AddString("Channel G"); combo.SetItemData(item,ViewChannelG);
  item = combo.AddString("Channel B"); combo.SetItemData(item,ViewChannelB);
  item = combo.AddString("DXT Sectors"); combo.SetItemData(item,ViewDXTSectors);
  if (enableNone)
  {
    item = combo.AddString("RGB Difference"); combo.SetItemData(item,ViewRGBDiff);
  }
  combo.SetCurSel(0);
  //combo.EnableWindow(FALSE);
}

#include <El/Enum/enumNames.hpp>
#include ".\mainfrm.h"

void CMainFrame::FillFormatCombo(CComboBox &combo)
{
  combo.ResetContent();
  static const TexFormat formats[] =
  {
    TexFormARGB8888,
    TexFormDXT1,
    TexFormDXT3,
    TexFormDXT5,
    TexFormARGB1555,TexFormARGB4444,
    TexFormAI88
  };
  for (int i=0; i<sizeof(formats)/sizeof(*formats); i++)
  {
    TexFormat f = formats[i];
    int item = combo.AddString(FindEnumName(f));
    combo.SetItemData(item,f);
  }
  /*
  item = combo.AddString("RGBA8888"); combo.SetItemData(item,TexFormARGB8888);
  item = combo.AddString("RGBA4444"); combo.SetItemData(item,TexFormARGB4444);
  item = combo.AddString("DXT1"); combo.SetItemData(item,TexFormDXT1);
  item = combo.AddString("DXT2/3"); combo.SetItemData(item,TexFormDXT3);
  item = combo.AddString("DXT4/5"); combo.SetItemData(item,TexFormDXT5);
  */
  combo.SetCurSel(0);
  //combo.EnableWindow(FALSE);
}

void CMainFrame::InitUpdateDoc(CTexViewDoc *doc)
{
  TextureSet *set = doc->GetTextureSet(0);
  if (!set) return;
  TexFormat f = set->_item.GetSourceFormat();
  int selL = 0;
  for (int i=0; i<m_wndLFormat.GetCount(); i++)
  {
    if (m_wndLFormat.GetItemData(i)==f)
    {
      selL = i;
      break;
    }
  }
  int selR = 0;
  for (int i=0; i<m_wndRFormat.GetCount(); i++)
  {
    if (m_wndRFormat.GetItemData(i)==f)
    {
      selR = i;
      break;
    }
  }
  m_wndLFormat.SetCurSel(selL);
  m_wndRFormat.SetCurSel(selR);
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;
	
  #define TV_TOOLBAR 0
  #if TV_TOOLBAR
	if (!m_wndToolBar.Create(this) ||
		!m_wndToolBar.LoadToolBar(IDR_MAINFRAME))
	{
		TRACE0("Failed to create toolbar\n");
		return -1;      // fail to create
	}
  #endif

  if (!m_wndSettingsBar.Create(this,IDD_PREVIEW,CBRS_TOP,IDD_PREVIEW))
  {
		TRACE0("Failed to create setting bar\n");
		return -1;      // fail to create
  }

  if (!m_wndInfoBar.Create(this,IDD_INFOBAR,CBRS_RIGHT,IDD_INFOBAR))
  {
		TRACE0("Failed to create info bar\n");
		return -1;      // fail to create
  }

  m_wndLViewType.Attach(m_wndSettingsBar.GetDlgItem(IDC_VIEW_L)->GetSafeHwnd());
  m_wndRViewType.Attach(m_wndSettingsBar.GetDlgItem(IDC_VIEW_R)->GetSafeHwnd());
  m_wndSettingsBar.SetWindowText("View settings");
  m_wndInfoBar.SetWindowText("Info");

  m_wndLFormat.Attach(m_wndSettingsBar.GetDlgItem(IDC_FORMAT_L)->GetSafeHwnd());
  m_wndRFormat.Attach(m_wndSettingsBar.GetDlgItem(IDC_FORMAT_R)->GetSafeHwnd());

  m_infoAvgColor.Attach(m_wndInfoBar.GetDlgItem(IDC_AVG_COLOR)->GetSafeHwnd());
  m_infoMaxColor.Attach(m_wndInfoBar.GetDlgItem(IDC_MAX_COLOR)->GetSafeHwnd());
  m_infoPixColor.Attach(m_wndInfoBar.GetDlgItem(IDC_PIX_COLOR)->GetSafeHwnd());

  m_infoAvgColorText.Attach(m_wndInfoBar.GetDlgItem(IDC_AVG_COLOR_TEXT)->GetSafeHwnd());
  m_infoMaxColorText.Attach(m_wndInfoBar.GetDlgItem(IDC_MAX_COLOR_TEXT)->GetSafeHwnd());
  m_infoPixColorText.Attach(m_wndInfoBar.GetDlgItem(IDC_PIX_COLOR_TEXT)->GetSafeHwnd());

	if (!m_wndStatusBar.Create(this) ||
		!m_wndStatusBar.SetIndicators(indicators,
		  sizeof(indicators)/sizeof(UINT)))
	{
		TRACE0("Failed to create status bar\n");
		return -1;      // fail to create
	}

	m_wndStatusBar.SetPaneInfo(0,m_wndStatusBar.GetItemID(0),SBPS_NORMAL,300);
	m_wndStatusBar.SetPaneInfo(1,m_wndStatusBar.GetItemID(1),SBPS_STRETCH,200);

	// TODO: Remove this if you don't want tool tips or a resizeable toolbar
  #if TV_TOOLBAR
	m_wndToolBar.SetBarStyle(m_wndToolBar.GetBarStyle() |
		CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC);
  #endif

  m_wndSettingsBar.EnableDocking(CBRS_ALIGN_ANY);
  m_wndInfoBar.EnableDocking(CBRS_ALIGN_ANY);

	EnableDocking(CBRS_ALIGN_ANY);
  #if TV_TOOLBAR
  	m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
	  DockControlBar(&m_wndToolBar,AFX_IDW_DOCKBAR_TOP);
  #endif
	DockControlBar(&m_wndSettingsBar,AFX_IDW_DOCKBAR_TOP);
	DockControlBar(&m_wndInfoBar,AFX_IDW_DOCKBAR_BOTTOM);
	//FloatControlBar(&m_wndInfoBar,CPoint(300,100),CBRS_ALIGN_RIGHT);

  LoadWndState();

  FillViewCombo(m_wndLViewType,false);
  FillViewCombo(m_wndRViewType);

  FillFormatCombo(m_wndLFormat);
  FillFormatCombo(m_wndRFormat);

  _displayRGB01=AfxGetApp()->GetProfileInt("View","DisplayRGB01",0)!=0;

	return 0;
}

BEGIN_MESSAGE_MAP(CStaticColorRect, CStatic)
   ON_WM_CTLCOLOR_REFLECT()
END_MESSAGE_MAP()

const COLORREF BK_COLOR = RGB(255,255,0); // yellow
const COLORREF FG_COLOR = RGB(255,0,0);   // red

CStaticColorRect::CStaticColorRect()
{
  _colorSet = false;
}
CStaticColorRect::~CStaticColorRect()
{
  if (_colorSet) _brush.DeleteObject();
}

void CStaticColorRect::ResetColor(bool update)
{
  if (_colorSet)
  {
    _brush.DeleteObject();
    _colorSet = false;
  }
  SetWindowText("");
  if (update)
  {
    Invalidate(FALSE);
  }
}

void CStaticColorRect::SetColor(int r, int g, int b, bool update)
{
  COLORREF color = RGB(r,g,b);
  
  if (_color!=color || !_colorSet)
  {
    _color = color;
    LOGBRUSH brush;
    brush.lbColor = _color;
    brush.lbHatch = 0;
    brush.lbStyle = BS_SOLID;
    if (_colorSet) _brush.DeleteObject();
    _brush.CreateBrushIndirect(&brush);
    _colorSet = true;
    if (update)
    {
      SetWindowText("");
      Invalidate(FALSE);
    }
  }
}

HBRUSH CStaticColorRect::CtlColor(CDC* pDC, UINT nCtlColor)
{
  if (_colorSet)
  {
    //CBrush s_brush(_color);
    pDC->SetBkColor(_color);    // required for edit controls
    pDC->SetTextColor(COLORREF(0));  // ditto
    return _brush;    
  }
  return NULL;
}

void CMainFrame::SetColor(
  CStaticColorRect &color, CStatic &text, const char *prompt, int r, int g, int b
)
{
  CString str;
  if (_displayRGB01)
    str.Format("%s: %0.3f,%0.3f,%0.3f",prompt,r/255.0f,g/255.0f,b/255.0f);
  else
    str.Format("%s: %d,%d,%d",prompt,r,g,b);
  text.SetWindowText(str);
  color.SetColor(r,g,b);
}

void CMainFrame::SetAvgColor(int r, int g, int b)
{
  SetColor(m_infoAvgColor,m_infoAvgColorText,"Avg: ",r,g,b);
}
void CMainFrame::SetMaxColor(int r, int g, int b)
{
  SetColor(m_infoMaxColor,m_infoMaxColorText,"Max: ",r,g,b);
}
void CMainFrame::SetPixColor(int x, int y, int r, int g, int b, int a)
{
  if (r>=0 && a>=0 && x>=0)
  {
    CString str;
    CString pixCoord;
    pixCoord.Format("%d,%d",x,y);
    if (!_displayRGB01)
      str.Format(
        "%s: %d,%d,%d  %d",
        (const char *)pixCoord,r,g,b,a);
      
    else
      str.Format(
      "%s: %0.3f,%0.3f,%0.3f  %0.3f",
      (const char *)pixCoord,r/255.0f,g/255.0f,b/255.0f,a/255.0f);
      

    m_infoPixColorText.SetWindowText(str);
    m_infoPixColor.SetColor(r,g,b);
  }
  else
  {
    m_infoPixColorText.SetWindowText("");
    m_infoPixColor.ResetColor();
  }
}

void CMainFrame::SetStatusText(const char * text)
{
	m_wndStatusBar.SetPaneText( 1,text, TRUE );
}

void CMainFrame::OnUpdatePage(CCmdUI *pCmdUI)
{
  pCmdUI->Enable(); 

  CString strPage;
	strPage.Format
	(
		"%dx%d of %dx%d",
		_itemW,_itemH,_setW,_setH
	);
  pCmdUI->SetText( strPage );
}


BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CFrameWnd::PreCreateWindow(cs);
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers


void CMainFrame::OnDropFiles(HDROP hDropInfo) 
{

	//SetActiveWindow();      // activate us first !
	UINT nFiles = ::DragQueryFile(hDropInfo, (UINT)-1, NULL, 0);

	CWinApp* pApp = AfxGetApp();

	//ProcessMessage

	for (UINT iFile = 0; iFile < nFiles; iFile++)
	{
		TCHAR szFileName[_MAX_PATH];
		::DragQueryFile(hDropInfo, iFile, szFileName, _MAX_PATH);
		// check if file can be opened
		const char *ext=strrchr(szFileName,'.');
		if( ext )
		{
			pApp->OpenDocumentFile(szFileName);
		}
		else
		{
			MSG msg;
			msg.message=WM_USER;
			msg.wParam=0;
			msg.lParam=(LPARAM)szFileName;
			pApp->ProcessMessageFilter(0,&msg);
		}
	}
	::DragFinish(hDropInfo);
}

void CMainFrame::OnClose() 
{
  SaveWndState();
	
	CFrameWnd::OnClose();
}

void CMainFrame::OnEndSession(BOOL bEnding)
{
  if (bEnding) SaveWndState();
  CFrameWnd::OnEndSession(bEnding);
}


CTexViewView *CMainFrame::GetView() const
{
  //if (m_wndSplitter.m_hWnd==NULL)
  {
    return (CTexViewView *)GetActiveView();
  }
  //return (CTexViewView *)m_wndSplitter.GetPane(0,0);
}
CTexViewStatus2View *CMainFrame::GetStatus2View() const
{
  return NULL;
  //if (m_wndSplitter.m_hWnd==NULL) return NULL;
  //return (CTexViewStatus2View *)m_wndSplitter.GetPane(1,0);
}

BOOL CMainFrame::OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext) 
{
  #if 0
	if (m_wndSplitter.Create(this, 2, 2, CSize(100,100), pContext))
  {
    return TRUE;
  }
  else
  #endif
  {
  	return CFrameWnd::OnCreateClient(lpcs, pContext);
  }
}

void CMainFrame::OnViewInfoBar() 
{
	ShowControlBar(&m_wndInfoBar, (m_wndInfoBar.GetStyle() & WS_VISIBLE) == 0, FALSE);
}

void CMainFrame::OnUpdateViewInfoBar(CCmdUI* pCmdUI) 
{
  BOOL active = (m_wndInfoBar.GetStyle() & WS_VISIBLE)!=0;
  pCmdUI->SetCheck(active);
	
}

void CMainFrame::OnViewSettingsBar() 
{
	ShowControlBar(&m_wndSettingsBar, (m_wndSettingsBar.GetStyle() & WS_VISIBLE) == 0, FALSE);
}

void CMainFrame::OnUpdateViewSettingsBar(CCmdUI* pCmdUI) 
{
  BOOL active = (m_wndSettingsBar.GetStyle() & WS_VISIBLE)!=0;
  pCmdUI->SetCheck(active);
}


void CMainFrame::OnViewRgb0()
{
  _displayRGB01=!_displayRGB01;
  ((CTexViewView *)GetActiveView())->RefreshStatus();
  ((CTexViewView *)GetActiveView())->UpdatePixelInfo();

}

void CMainFrame::OnUpdateViewRgb0(CCmdUI *pCmdUI)
{
  pCmdUI->SetCheck(_displayRGB01);
  AfxGetApp()->WriteProfileInt("View","DisplayRGB01",_displayRGB01?1:0);
}
