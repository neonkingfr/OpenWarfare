#if !defined(AFX_PROCEDFILTER_H__4336F210_BC9F_4D1B_BFDE_263CA01F4AA9__INCLUDED_)
#define AFX_PROCEDFILTER_H__4336F210_BC9F_4D1B_BFDE_263CA01F4AA9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ProcedFilter.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// ProcedFilter dialog

class ProcedFilter : public CDialog
{
// Construction
public:
	ProcedFilter(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(ProcedFilter)
	enum { IDD = IDD_PROCED_FILTER };
	CString	_expression;
	CString	_init;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ProcedFilter)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ProcedFilter)
	virtual BOOL OnInitDialog();
	afx_msg void OnPfLoad();
	afx_msg void OnPfSave();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROCEDFILTER_H__4336F210_BC9F_4D1B_BFDE_263CA01F4AA9__INCLUDED_)
