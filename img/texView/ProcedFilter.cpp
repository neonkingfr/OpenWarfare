// ProcedFilter.cpp : implementation file
//

#include "stdafx.h"
#include "texview.h"
#include "ProcedFilter.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#include <El/QStream/qStream.hpp>


/////////////////////////////////////////////////////////////////////////////
// ProcedFilter dialog


ProcedFilter::ProcedFilter(CWnd* pParent /*=NULL*/)
	: CDialog(ProcedFilter::IDD, pParent)
{
	//{{AFX_DATA_INIT(ProcedFilter)
	_expression = _T("");
	_init = _T("");
	//}}AFX_DATA_INIT
}


void ProcedFilter::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ProcedFilter)
	DDX_Text(pDX, IDC_EDIT1, _expression);
	DDX_Text(pDX, IDC_EDIT2, _init);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ProcedFilter, CDialog)
	//{{AFX_MSG_MAP(ProcedFilter)
	ON_BN_CLICKED(IDC_PF_LOAD, OnPfLoad)
	ON_BN_CLICKED(IDC_PF_SAVE, OnPfSave)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ProcedFilter message handlers

BOOL ProcedFilter::OnInitDialog() 
{
  if (_expression.IsEmpty())
  {
    _expression = 
      "c = (\r\n"
      "src pixel [u-du,v]+\r\n"
      "src pixel [u+du,v]+\r\n"
      "src pixel [u,v-dv]+\r\n"
      "src pixel [u,v+dv]+\r\n"
      "src pixel [u,v]\r\n"
      ")/5"
    ;
  }
	CDialog::OnInitDialog();
	
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void ProcedFilter::OnPfLoad() 
{
  // let user select a file to load
  CString defext;
	defext.LoadString(IDS_FILEFILTEREXT);
  CString filters;
	filters.LoadString(IDS_FILEFILTFILTERS);
  CFileDialog fdlg(
		TRUE,defext,NULL,OFN_FILEMUSTEXIST|OFN_LONGNAMES,
		filters,NULL
	);
  if (fdlg.DoModal()!=IDOK) return;
  
	// we have a filename  - we may load now
	// we have a filename  - we may save now
  QIFStream f;
  f.open(fdlg.GetPathName());
  int size = f.rest();
  CString data;
  char *readBuffer = data.GetBufferSetLength(size);
  if (f.read(readBuffer,size)==size)
  {
    // load both init and expression
    _expression = data;
    UpdateData(FALSE);
  }
	
}

void ProcedFilter::OnPfSave() 
{
  // let user select a file to load
  CString defext;
	defext.LoadString(IDS_FILEFILTEREXT);
  CString filters;
	filters.LoadString(IDS_FILEFILTFILTERS);
  CFileDialog fdlg(
		FALSE,defext,NULL,OFN_LONGNAMES|OFN_HIDEREADONLY|OFN_OVERWRITEPROMPT,
		filters,NULL
	);
  if (fdlg.DoModal()!=IDOK) return;
	
  UpdateData(TRUE);

	// we have a filename  - we may save now
  QOFStream f(fdlg.GetPathName());
  // we need to save both init and expression
  f.write((const char *)_expression,_expression.GetLength());
  f.close();
}
