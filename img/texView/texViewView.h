// texViewView.h : interface of the CTexViewView class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEXMERGEVIEW_H__7E5B94A6_7BE3_11D3_8373_00A0C9DF4D61__INCLUDED_)
#define AFX_TEXMERGEVIEW_H__7E5B94A6_7BE3_11D3_8373_00A0C9DF4D61__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

enum ViewChannel
{
  ViewNone,
  ViewRGB,
  ViewRGBA,
  ViewRGB_DR,
  ViewAlpha,
  ViewChannelR,
  ViewChannelG,
  ViewChannelB,
  ViewDXTSectors,
  ViewRGBDiff,
};

class CTexViewView : public CScrollView
{
private:
	// cursor position
	int _actSet;

  int _wheelAccumulate;
	int _zoom; // log 2 of zoom
  ViewChannel _channelL,_channelR;
  TexFormat _formatL,_formatR;
  // view can show two different views of the same document
  // both are stored in the bitmaps here
  Picture _pictureL;
  Picture _pictureR;
  /// total error - in the comparison mode only
  double _pictureRDiffXYZ;
  double _pictureRDiffRGB;
  CBitmap _bitmapL;
  CBitmap _bitmapR;
  bool _drag; ///< true while dragging
  CPoint _dragStart;
  CPoint _dragCur;
  int _curX,_curY; ///< mouse cursor coordinates (logical, in texture)

private:
  void ConvertPicture(
    Picture &tgt, TexFormat format, const Picture &src
  );
	PictureData *UpdateBitmaps(bool left=true, bool right=true);

protected: // create from serialization only
	CTexViewView();
	DECLARE_DYNCREATE(CTexViewView)

  void RefreshStatus();

// Attributes
public:
	CTexViewDoc *GetDocument() { return (CTexViewDoc*)m_pDocument; }
	const CTexViewDoc *GetDocument() const { return (const CTexViewDoc*)m_pDocument; }

	int GetActSet() const {return _actSet;}
  void GetActARGB(int &a, int &r, int &g, int &b) const;

// Operations
public:
	// notify function enable multiple views keep synchronized
	// currently there is only one view and Notify is not implemented

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTexViewView)
	public:
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual void OnInitialUpdate();
	protected:
	virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	virtual BOOL OnScrollBy(CSize sizeScroll, BOOL bDoScroll = TRUE);
	//}}AFX_VIRTUAL

// Implementation
public:
	void UpdateScroll();
	void UpdatePage(bool updateBitmaps=true);
  void ApplyOffset(CPoint pt);
	CPoint GetLogicalPos(CPoint pt, bool isRight);
	CPoint GetLogicalPos(CPoint pt, bool *right=NULL);
  void OffsetViewStart(CPoint pt);
  void DrawBarAndRect(
    CDC &dc,int x, int y, int w, int h, DWORD frame, DWORD fill
  );

	void AddItem( const char *name );

  void SetChannelL(ViewChannel channel);
  void SetChannelR(ViewChannel channel);

  void SetFormatL(TexFormat format);
  void SetFormatR(TexFormat format);

	virtual ~CTexViewView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

  /// call when pixel info should be updated
  void UpdatePixelInfo();
protected:
  /// get current texture data
  const TextureSet *GetTexture() const;
  /// get bottom coordinate of the texture bitmap
  int GetTexBottom(const TextureSet *set=NULL) const;

  /// get left coordinate of the right texture bitmap
  int GetRTexLeft(const TextureSet *set=NULL) const;

// Generated message map functions
protected:
	//{{AFX_MSG(CTexViewView)
	afx_msg void OnSetAdd();
	afx_msg void OnSetNext();
	afx_msg void OnSetPrev();
	afx_msg void OnItemAdd();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnViewZoomIn();
	afx_msg void OnViewZoomOut();
	afx_msg void OnMipGenerate();
	afx_msg void OnMipGenDetailTex();
	afx_msg void OnMipGenNormalTex();
	afx_msg void OnMipmapGeneratespecialNormalizednormaltexture();
  afx_msg void OnMipmapGeneratespecialNormalizednormaltextureWithFading();
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnCaptureChanged(CWnd *pWnd);
	afx_msg BOOL OnMouseWheel(UINT nFlags, short zDelta, CPoint pt);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnPreviewMipmapColortexture();
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TEXMERGEVIEW_H__7E5B94A6_7BE3_11D3_8373_00A0C9DF4D61__INCLUDED_)
