// texViewView.cpp : implementation of the CTexViewView class
//

#include "stdafx.h"
#include "texView.h"

#include "texViewDoc.h"
#include "mainFrm.h"
#include "texViewView.h"
#include <Es/Strings/rString.hpp>
#include ".\texviewview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTexViewView

IMPLEMENT_DYNCREATE(CTexViewView, CScrollView)

BEGIN_MESSAGE_MAP(CTexViewView, CScrollView)
  //{{AFX_MSG_MAP(CTexViewView)
  ON_COMMAND(ID_SET_ADD, OnSetAdd)
  ON_COMMAND(ID_SET_NEXT, OnSetNext)
  ON_COMMAND(ID_SET_PREV, OnSetPrev)
  ON_COMMAND(ID_ITEM_ADD, OnItemAdd)
  ON_WM_ERASEBKGND()
  ON_COMMAND(ID_VIEW_ZOOM_IN, OnViewZoomIn)
  ON_COMMAND(ID_VIEW_ZOOM_OUT, OnViewZoomOut)
  ON_COMMAND(ID_MIP_GENERATE, OnMipGenerate)
  ON_COMMAND(ID_MIP_GENDETAILTEX, OnMipGenDetailTex)
  ON_COMMAND(ID_MIP_GEN_NORMALTEX, OnMipGenNormalTex)
  ON_COMMAND(ID_MIPMAP_GENERATESPECIAL_NORMALIZEDNORMALTEXTURE, OnMipmapGeneratespecialNormalizednormaltexture)
  ON_COMMAND(ID_MIPMAP_GENERATESPECIAL_NORMALIZEDNORMALTEXTUREWITHFADING, OnMipmapGeneratespecialNormalizednormaltextureWithFading)
	ON_WM_RBUTTONDOWN()
	ON_WM_RBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_SETCURSOR()
	ON_WM_CAPTURECHANGED()
	ON_WM_MOUSEWHEEL()
	//}}AFX_MSG_MAP
  ON_COMMAND(ID_PREVIEWMIPMAP_COLORTEXTURE, OnPreviewMipmapColortexture)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTexViewView construction/destruction

CTexViewView::CTexViewView()
{
  _actSet=0;
  _zoom=0;
  _wheelAccumulate=0;
  _channelL = ViewRGBA;
  _channelR = ViewNone;
  _formatL = TexFormARGB8888;
  _formatR = TexFormARGB8888;
  _drag = false;
  _curX = _curY = -1;
}

CTexViewView::~CTexViewView()
{
}

BOOL CTexViewView::PreCreateWindow(CREATESTRUCT& cs)
{
  // TODO: Modify the Window class or styles here by modifying
  //  the CREATESTRUCT cs

  return CScrollView::PreCreateWindow(cs);
}

#define Compose(a,r,g,b)   ((a)<<24)|((r)<<16)|((g)<<8)|((b)<<0)

static bool BitmapFromTexture(
  CDC &dc,CBitmap &bitmap, Picture *tex, Picture *sec, ViewChannel channel
)
{
  if (channel==ViewNone)
  {
    bitmap.DeleteObject();
    return true;
  }
  bool showAlpha = channel==ViewAlpha || channel==ViewRGBA;
  bool showDR = channel==ViewRGB_DR;
  Temp<DWORD> data(tex->W()*tex->H());
  for (int y=0; y<tex->H(); y++)
  for (int x=0; x<tex->W(); x++)
  {
    DWORD pixel = showDR ? tex->GetPixelRaw(x,y) : tex->GetPixel(x,y);

    int a = showAlpha ? (pixel>>24)&0xff : 0xff;

    int c;
    if (channel!=ViewAlpha)
    {
      if (channel==ViewChannelR)
      {
        c=RGB(pixel>>16,pixel>>16,pixel>>16);
      }
      else if (channel==ViewChannelG)
      {
        c=RGB(pixel>>8,pixel>>8,pixel>>8);
      }
      else if (channel==ViewChannelB)
      {
        c=RGB(pixel & 0xFF,pixel & 0xFF,pixel & 0xFF);
      }
      else if (channel==ViewRGBDiff)
      {
        int secPixel = sec->GetPixel(x,y);
        
        int rs = (secPixel>>16)&0xff;
        int gs = (secPixel>> 8)&0xff;
        int bs = (secPixel    )&0xff;
        int rp = (pixel>>16)&0xff;
        int gp = (pixel>> 8)&0xff;
        int bp = (pixel    )&0xff;
        int r = (rp-rs)*4+0x80;
        int g = (gp-gs)*4+0x80;
        int b = (bp-bs)*4 +0x80;
        saturate(r,0,255);
        saturate(g,0,255);
        saturate(b,0,255);
        c = Compose(0,r,g,b);
      }
      else if (a!=0xff)
      {
        int r = (pixel>>16)&0xff;
        int g = (pixel>> 8)&0xff;
        int b = (pixel    )&0xff;
        int o = 0x80*(0xff-a);
        r=(r*a+o)>>8;
        g=(g*a+o)>>8;
        b=(b*a+o)>>8;
        saturate(r,0,255);
        saturate(g,0,255);
        saturate(b,0,255);
        c = Compose(0,r,g,b);
      }
      else
      {
        c = pixel&0xffffff;
      }
    }    
    else
    {
      c = RGB(a,a,a);
    }
    
    if (channel==ViewDXTSectors && ((x & 3)==0 && (y & 3)==0))
    {
      c =(c & 0xFEFEFEFE)+0x10101010;
      if (c & 0x100) c|=0xFF;
      if (c & 0x10000) c|=0xFF00;
      if (c & 0x1000000) c|=0xFF0000;
    }

    data[tex->W()*y+x] = c;
  }
  bitmap.DeleteObject();
  BITMAPINFOHEADER hdr;
  memset(&hdr,0,sizeof(hdr));
  hdr.biSize=sizeof(hdr);
  hdr.biCompression=BI_RGB;
  hdr.biHeight=-tex->H();
  hdr.biWidth=tex->W();
  hdr.biPlanes=1;
  hdr.biBitCount=32;  
  HBITMAP bp=CreateDIBitmap(dc,&hdr,CBM_INIT,data,(BITMAPINFO *)&hdr,DIB_RGB_COLORS);
  if (bp==0) return false;
  bitmap.Attach(bp);
  return true;
}

static void DrawBitmap(CDC &dc, CBitmap &bitmap, CRect rect)
{
  CDC compatible;
  if (compatible.CreateCompatibleDC(&dc))
  {
    void *prevObj = compatible.SelectObject(bitmap);
    BITMAP info;
    if (bitmap.GetBitmap(&info))
    {
      dc.SetStretchBltMode(COLORONCOLOR);
      dc.StretchBlt
      (
        rect.left,rect.top,rect.right-rect.left,rect.bottom-rect.top,
        &compatible,0,0,info.bmWidth,info.bmHeight,SRCCOPY
      );
    }
    compatible.SelectObject(prevObj);
  }
}

void CTexViewView::DrawBarAndRect(
  CDC &dc,int x, int y, int w, int h, DWORD frame, DWORD fill
)
{
  CBrush interiorBrush(fill);
  CPen framePen(0,1,frame);
  dc.SelectObject(&interiorBrush);
  dc.SelectObject(&framePen);
  CRect r;
  r.top = y;
  r.left = x;
  r.right = x+w;
  r.bottom = y+h;
  dc.Rectangle(&r);
}


void CTexViewView::ConvertPicture(
  Picture &tgt, TexFormat format, const Picture &src
)
{
  tgt = src;
  if (format!=TexFormARGB8888 && format!=src.GetSourceFormat())
  {
    // some degradation is needed based on format
    TextureHints hints;
    hints._format = format;
    tgt.ApplyHints(hints);
  }
  else if (format!=src.GetSourceFormat())
  {
    tgt.ResetSourceFormat();
  }
}

PictureData *CTexViewView::UpdateBitmaps(bool left, bool right)
{
  CTexViewDoc* doc = GetDocument();
  ASSERT_VALID( doc);
  CClientDC dc(this);

  PictureData *tex = NULL;
  const TextureSet *set=doc->GetTextureSet(_actSet);
  if( set && set->_itemSet)
  {
    tex=set->_item.GetTex();
  }

  if( tex )
  {
    CWaitCursor wait;
	  CMainFrame *frame = (CMainFrame *)AfxGetMainWnd();
	  frame->SetStatusText("Busy");

    if (left)
    {
      if (_channelL!=ViewNone)
      {
        ConvertPicture(_pictureL,_formatL,*tex);
      }
      else
      {
        _pictureL.Delete();
      }
      BitmapFromTexture(dc,_bitmapL,&_pictureL,&_pictureR,_channelL);
    }
    if (right)
    {
      if (_channelR!=ViewNone)
      {
        ConvertPicture(_pictureR,_formatR,*tex);
        if (_channelR==ViewRGBDiff)
        {
          _pictureRDiffRGB = _pictureR.DifferenceRGB(_pictureL);
          _pictureRDiffXYZ = _pictureR.DifferenceXYZ(_pictureL);
        }
      }
      else
      {
        _pictureR.Delete();
      }
      BitmapFromTexture(dc,_bitmapR,&_pictureR,&_pictureL,_channelR);
    }
	  frame->SetStatusText("Ready");
  }
  else
  {
    _bitmapL.DeleteObject();
    _bitmapR.DeleteObject();
  }
  return tex;

}


void CTexViewView::SetChannelL(ViewChannel channel)
{
  if (_channelL==channel) return;
  _channelL = channel;
  UpdateBitmaps(true,false);
  //UpdateScroll();
  Invalidate();
}
void CTexViewView::SetChannelR(ViewChannel channel)
{
  if (_channelR==channel) return;
  _channelR = channel;
  UpdateBitmaps(false,true);
  UpdateScroll();
  Invalidate();
}

void CTexViewView::SetFormatL(TexFormat format)
{
  if (_formatL==format) return;
  _formatL = format;
  UpdateBitmaps(true,false);
  Invalidate();
}

void CTexViewView::SetFormatR(TexFormat format)
{
  if (_formatR==format) return;
  _formatR = format;
  UpdateBitmaps(false,true);
  Invalidate();
}

int CTexViewView::GetTexBottom(const TextureSet *set) const
{
  if (!set) set=GetTexture();
  int texBottom = Zoom(0,_zoom) + 1 + Zoom(set->_h,_zoom);
  return texBottom;
}

int CTexViewView::GetRTexLeft(const TextureSet *set) const
{
  if (!set) set=GetTexture();
  int texBottom = Zoom(0,_zoom) + Zoom(set->_w,_zoom) + 13;
  return texBottom;
}

void CTexViewView::UpdatePixelInfo()
{
  const TextureSet *set = GetTexture();
  CMainFrame *frame = (CMainFrame *)AfxGetMainWnd();
  if (frame)
  {
    if (_curX>=0 && _curX<set->_w && _curY>=0 && _curY<set->_h)
    {
      int a,r,g,b;
      GetActARGB(a,r,g,b);
      frame->SetPixColor(_curX,_curY,r,g,b,a);
    }
  }
  
}

const TextureSet *CTexViewView::GetTexture() const
{
  const CTexViewDoc* doc = GetDocument();
  ASSERT_VALID( doc);

  return doc->GetTextureSet(_actSet);
}

void CTexViewView::RefreshStatus()
{
  CMainFrame *frame = (CMainFrame *)AfxGetMainWnd();

  const TextureSet *set = GetTexture();

  PictureData *tex = NULL;
  if( set && set->_itemSet)
  {
    tex=set->_item.GetTex();
  }
  if (tex)
  {
    int maxR,maxG,maxB,maxA;
    tex->MaxRGBA(maxR,maxG,maxB,maxA);

    PPixel avgColor = tex->GetAverageColor();
    if ((avgColor&0xffffff)==0 || _actSet>0)
    {
      // zero most likely means color is not calculated
      // _actSet means we are viewing different mipmap
      // note: we always calculate average in linear space here
      avgColor = tex->GetAverageColorCalculated();
    }
    //int avgA = (avgColor>>24)&0xff;
    int avgR = (avgColor>>16)&0xff;
    int avgG = (avgColor>> 8)&0xff;
    int avgB = (avgColor    )&0xff;
    frame->SetAvgColor(avgR,avgG,avgB);
    frame->SetMaxColor(maxR,maxG,maxB);
  }

}

/////////////////////////////////////////////////////////////////////////////
// CTexViewView drawing

void CTexViewView::OnDraw(CDC* idc)
{
  CRect rect;
  GetClientRect(&rect);

#define MEMORY 0
#if MEMORY
  CDC dc;
  CBitmap dcBitmap;
  dc.CreateCompatibleDC(idc);
  dcBitmap.CreateCompatibleBitmap(idc, rect.Width(), rect.Height());
  dc.SelectObject(&dcBitmap);

  //dc.SelectObject(GetStockObject(WHITE_PEN));
  //dc.Rectangle(rect);
  dc.SelectObject(GetStockObject(BLACK_PEN));
#else
  CDC &dc = *idc;
#endif

  CPoint org=idc->GetViewportOrg();
  
  rect.left-=org.x;
  rect.right-=org.x;
  rect.top-=org.y;
  rect.bottom-=org.y;

  //CTexViewDoc* doc = GetDocument();
  //ASSERT_VALID( doc);

  CFont font;
  font.CreateFont(
    17,0,0,0,FW_NORMAL,FALSE,FALSE,FALSE,
    ANSI_CHARSET,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,
    DEFAULT_QUALITY,FIXED_PITCH|FF_MODERN,NULL
  );
  CRect clipRect;
  dc.GetClipBox(clipRect);
  //Log(
  //  "Clip %d,%d - %d,%d",
  //  clipRect.left,clipRect.top,clipRect.Width(),clipRect.Height()
  //);

  const TextureSet *set=GetTexture();

  //CBrush brush(RGB(64,128,64));
  //dc.FillRect(&rect,&brush);

  CBrush brushBg(RGB(128,128,128));
  dc.FillRect(&rect,&brushBg);
  void *prevFont = dc.SelectObject(&font);

  PictureData *tex = NULL;
  if( set && set->_itemSet)
  {
    tex=set->_item.GetTex();
  }

  if (tex)
  {
    int texBottom = GetTexBottom(set);
    CRect rectL(
      Zoom(0,_zoom)+1,
      Zoom(0,_zoom)+1,
      Zoom(0,_zoom)+Zoom(set->_w,_zoom)+1,
      Zoom(0,_zoom)+Zoom(set->_h,_zoom)+1
    );
    if (_channelL!=ViewNone && !(clipRect&rectL).IsRectEmpty())
    {
      DrawBitmap(dc,_bitmapL,rectL);
    }
    CRect frameL = rectL;
    --frameL.top;
    --frameL.left;
    ++frameL.bottom;
    ++frameL.right;
    CBrush blackBrush(RGB(0,0,0));
    dc.FrameRect(&frameL,&blackBrush);
    if (_channelR!=ViewNone)
    {
      int texRL = GetRTexLeft(set);
      CRect rectR(
        texRL,Zoom(0,_zoom)+1,
        texRL+Zoom(set->_w,_zoom),Zoom(0,_zoom)+Zoom(set->_h,_zoom)+1
      );
      if (!(clipRect&rectR).IsRectEmpty())
      {
        DrawBitmap(dc,_bitmapR,rectR);
      }
      CRect frameR = rectR;
      --frameR.top;
      --frameR.left;
      ++frameR.bottom;
      ++frameR.right;
      dc.FrameRect(&frameR,&blackBrush);
    }

    /*
    int maxR,maxG,maxB,maxA;
    tex->MaxRGBA(maxR,maxG,maxB,maxA);
    DrawBarAndRect(dc,
      0,texBottom+10,20,20,
      0,RGB(maxR,maxG,maxB)
    );

    PPixel avgColor = tex->GetAverageColor();
    //! average color calculated
    PPixel avgColorR = tex->GetAverageColorCalculated();

    int avgA = (avgColor>>24)&0xff;
    int avgR = (avgColor>>16)&0xff;
    int avgG = (avgColor>> 8)&0xff;
    int avgB = (avgColor    )&0xff;

    int avgAR = (avgColorR>>24)&0xff;
    int avgRR = (avgColorR>>16)&0xff;
    int avgGR = (avgColorR>> 8)&0xff;
    int avgBR = (avgColorR    )&0xff;
    DrawBarAndRect(dc,0,texBottom+30,20,20,RGB(0,0,0),RGB(avgRR,avgGR,avgBR));

    int dist = abs(avgR-avgRR)+abs(avgG-avgGR)+abs(avgB-avgBR)+abs(avgA-avgAR);
    if (dist>5)
    {
      DrawBarAndRect(
        dc,0,texBottom+50,20,20,
        dist>32 ? RGB(192,0,0) : RGB(0,0,0),
        RGB(avgR,avgG,avgB)
      );
    }
    */
    int prev = dc.SetBkMode(TRANSPARENT);
    int sizeL = _pictureL.GetVRAMSize();
    CString text = CString(Format("Size: %s",(const char *)FormatByteSize(sizeL)));
    CSize extent = dc.GetTextExtent(text);
    dc.ExtTextOut(
      5,texBottom+5,0,NULL,
      text,
      NULL
    );
    if (_channelR==ViewRGBDiff)
    {
      // compare pictures and show comparison result
      int minX = 5+extent.cx+10;
      int x = Zoom(set->_w,_zoom)+Zoom(0,_zoom)+13;
      if (x<minX) x = minX;
      dc.ExtTextOut(
        x,texBottom+5,0,NULL,
        CString(Format("Diff: XYZ=%g, RGB=%g",_pictureRDiffXYZ,_pictureRDiffRGB)),
        NULL
      );
    }
    else if (_channelR!=ViewNone)
    {
      int sizeR = _pictureR.GetVRAMSize();
      int minX = 5+extent.cx+10;
      int x = Zoom(set->_w,_zoom)+Zoom(0,_zoom)+13;
      if (x<minX) x = minX;
      dc.ExtTextOut(
        x,texBottom+5,0,NULL,
        CString(Format("Size: %s",(const char *)FormatByteSize(sizeR))),
        NULL
      );
    }

    dc.SetBkMode(prev);
  }
  else
  {
    //dc.FillRect(&rect,&brushBg);
  }

  dc.SelectObject(prevFont);
  #if MEMORY
    idc->BitBlt(0, 0, rect.Width(), rect.Height(), &dc, 0, 0, SRCCOPY);
  #endif

}

/////////////////////////////////////////////////////////////////////////////
// CTexViewView diagnostics

#ifdef _DEBUG
void CTexViewView::AssertValid() const
{
  CScrollView::AssertValid();
}

void CTexViewView::Dump(CDumpContext& dc) const
{
  CScrollView::Dump(dc);
}

#endif //_DEBUG


/////////////////////////////////////////////////////////////////////////////
// CTexViewView message handlers


void CTexViewView::OnInitialUpdate() 
{
  CScrollView::OnInitialUpdate();
  _actSet = 0;
  _curX=_curY = -1;
  // force format combos to be reset
  CTexViewDoc *doc=GetDocument();
  CMainFrame *frame = (CMainFrame *)AfxGetMainWnd();
  if (frame)
  {
    frame->SetPixColor(-1,-1,-1,-1,-1,-1);
  }
 if (doc && frame)
  {
    PictureData *tex = NULL;
    const TextureSet *set=doc->GetTextureSet(_actSet);
    if( set && set->_itemSet)
    {
      tex=set->_item.GetTex();
    }
    if (tex)
    {
      _formatL = _formatR = tex->GetSourceFormat();
    }
    frame->InitUpdateDoc(doc);
  }
}

void CTexViewView::OnSetAdd() 
{
}

// attributes

void CTexViewView::OnSetNext() 
{
  CTexViewDoc *doc=GetDocument();
  int maxSet=doc->GetTextureSetCount()-1;
  if( _actSet<maxSet )
  {
    _actSet++;
    UpdatePage();
  }
}

void CTexViewView::OnSetPrev() 
{
  //CTexViewDoc *doc=GetDocument();
  if( _actSet>0 )
  {
    _actSet--;
    UpdatePage();
  }
}

void CTexViewView::OnItemAdd() 
{
}

void CTexViewView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint) 
{
  CTexViewDoc *doc=GetDocument();
  if (doc)
  {
    PictureData *tex = NULL;
    const TextureSet *set=doc->GetTextureSet(_actSet);
    if( set && set->_itemSet)
    {
      tex=set->_item.GetTex();
    }

    if( tex )
    {
      _formatL = _formatR = tex->GetSourceFormat();
    }
    
  }
  _curX=_curY = -1;

  CMainFrame *frame = (CMainFrame *)AfxGetMainWnd();
  if (frame)
  {
    frame->SetPixColor(-1,-1,-1,-1,-1,-1);
  }

  UpdatePage();
}

BOOL CTexViewView::OnEraseBkgnd(CDC* dc) 
{
  //return CScrollView::OnEraseBkgnd(pDC);
  return 0;
}



void CTexViewView::OnViewZoomIn() 
{
  _wheelAccumulate=0;
  if( _zoom<+4 )
  {
    _zoom++;
    UpdatePage(false); 
  }
}

void CTexViewView::OnViewZoomOut() 
{
  _wheelAccumulate=0;
  if( _zoom>-4 )
  {
    _zoom--;
    UpdatePage(false);
  }
}

void CTexViewView::GetActARGB(int &a, int &r, int &g, int &b) const
{
  a = r = g = b = -1;
  const TextureSet *set = GetTexture();
  Picture *pic = set->_item.GetTex();
  if (pic)
  {
    int picX = _curX>>_actSet;
    int picY = _curY>>_actSet;
    if (picX>=0 && picX<set->_item.w && picY>=0 && picY<set->_item.h)
    {
      unsigned int pix = pic->GetPixel(picX,picY);
      a = (pix>>24)&0xff;
      r = (pix>>16)&0xff;
      g = (pix>>8)&0xff;
      b = (pix>>0)&0xff;
    }
  }
}

CPoint CTexViewView::GetLogicalPos( CPoint pt, bool isRight)
{
  CPoint orig=GetDeviceScrollPosition();
  if (!isRight)
  {
    return CPoint(Zoom(pt.x+orig.x-1,-_zoom),Zoom(pt.y+orig.y-1,-_zoom));
  }
  else
  {
    int rl = GetRTexLeft();
    return CPoint(Zoom(pt.x+orig.x-rl,-_zoom),Zoom(pt.y+orig.y-1,-_zoom));
  }
}

CPoint CTexViewView::GetLogicalPos(CPoint pt, bool *right)
{
  // convert mouse coord to logical
  // take care of CScrollView offset
  CPoint orig=GetDeviceScrollPosition();
  // check if we are within the first texture
  int rl = GetRTexLeft();
  bool isRight = _channelR!=ViewNone && pt.x+orig.x>=rl;
  if (right) *right = isRight;
  return GetLogicalPos(pt,isRight);
}

void CTexViewView::AddItem( const char *name )
{
  CTexViewDoc *doc=GetDocument();
  doc->AddItem(name);
}

void CTexViewView::OnMipGenerate() 
{
  _actSet = 0;

  CTexViewDoc *doc=GetDocument();
  doc->GenerateMipmaps();
}

void CTexViewView::UpdateScroll()
{
  const CTexViewDoc *doc=GetDocument();

  const TextureSet *set=doc->GetTextureSet(_actSet);
  if( set )
  {
    if (_channelR!=ViewNone)
    {
      SetScrollSizes( MM_TEXT, CSize(Zoom(set->_w,_zoom)*2+14,Zoom(set->_h,_zoom)+25) );
    }
    else
    {
      SetScrollSizes( MM_TEXT, CSize(Zoom(set->_w,_zoom)+2,Zoom(set->_h,_zoom)+25) );
    }
  }
}

void CTexViewView::UpdatePage(bool updateBitmaps)
{
  UpdateScroll();
  Invalidate();
  if (updateBitmaps)
  {
    UpdateBitmaps();
  }
  RefreshStatus();
}


void CTexViewView::OnMipGenDetailTex() 
{
  _actSet = 0;

  CTexViewDoc *doc=GetDocument();
  doc->GenerateMipmaps(MipDetail);
  
}

void CTexViewView::OnMipGenNormalTex() 
{
  _actSet = 0;

  CTexViewDoc *doc=GetDocument();
  doc->GenerateMipmaps(MipNormal);
}

void CTexViewView::OnMipmapGeneratespecialNormalizednormaltexture() 
{
  _actSet = 0;

  CTexViewDoc *doc=GetDocument();
  doc->GenerateMipmaps(MipNormalizeNormalMap);
}

void CTexViewView::OnMipmapGeneratespecialNormalizednormaltextureWithFading() 
{
  _actSet = 0;

  CTexViewDoc *doc=GetDocument();
  doc->GenerateMipmaps(MipNormalizeNormalMapFade);
}

void CTexViewView::OnPreviewMipmapColortexture()
{
  _actSet = 0;

  CTexViewDoc *doc=GetDocument();
  doc->GenerateMipmaps(MipAlphaNoise);
}

void CTexViewView::OnRButtonDown(UINT nFlags, CPoint point) 
{
  _drag = true;
  _dragStart = point;
  _dragCur = CPoint(0,0);

  SetCursor(LoadCursor(NULL,IDC_SIZEALL));
  SetCapture();

	
	CScrollView::OnRButtonDown(nFlags, point);
}

void CTexViewView::OnRButtonUp(UINT nFlags, CPoint point) 
{
  if (_drag)
  {
    ApplyOffset(_dragStart-point);
  }

  _drag = false;
  SetCursor(AfxGetApp()->LoadCursor(IDC_VISIBLECROSS));
  ReleaseCapture();
  
	CScrollView::OnRButtonUp(nFlags, point);
}

void CTexViewView::ApplyOffset(CPoint pt)
{
  OnScrollBy(pt-_dragCur);
  _dragCur = pt;
}


void CTexViewView::OnMouseMove(UINT nFlags, CPoint point) 
{
  if ((nFlags&MK_RBUTTON) && _drag)
  {
    // change view origin
    ApplyOffset(_dragStart-point);
  }
  else
  {
    CPoint pt = GetLogicalPos(point);
    const TextureSet *set = GetTexture();
    if (pt.x>=0 && pt.x<set->_w && pt.y>=0 && pt.y<set->_h)
    {
      _curX = pt.x;
      _curY = pt.y;
      UpdatePixelInfo();
    }
  }
	
	CScrollView::OnMouseMove(nFlags, point);
}

BOOL CTexViewView::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
  if (nHitTest==HTCLIENT)
  {
    if (_drag)
    {
      SetCursor(LoadCursor(NULL,IDC_SIZEALL));
      return TRUE;
    }
    CPoint pt;
    GetCursorPos(&pt);
    ScreenToClient(&pt);
    CPoint log = GetLogicalPos(pt);
    const TextureSet *set = GetTexture();
    if (log.x>=0 && log.x<set->_w && log.y>=0 && log.y<set->_h)
    {
      SetCursor(AfxGetApp()->LoadCursor(IDC_VISIBLECROSS));
    }
    else
    {
      SetCursor(LoadCursor(NULL,IDC_ARROW));
    }
    return true;
  }
	
	return CScrollView::OnSetCursor(pWnd, nHitTest, message);
}

BOOL CTexViewView::OnScrollBy(CSize sizeScroll, BOOL bDoScroll) 
{
	return CScrollView::OnScrollBy(sizeScroll, bDoScroll);
}

void CTexViewView::OnCaptureChanged(CWnd *pWnd) 
{
  _drag = false;
	CScrollView::OnCaptureChanged(pWnd);
}

void CTexViewView::OffsetViewStart(CPoint pt)
{
	int xMax = GetScrollLimit(SB_HORZ);
	int yMax = GetScrollLimit(SB_VERT);

  CPoint oldScrollPos;
  oldScrollPos.x = GetScrollPos(SB_HORZ);
  oldScrollPos.y = GetScrollPos(SB_VERT);
  CPoint pto = pt;
	// don't scroll if there is no valid scroll range (ie. no scroll bar)
	CScrollBar* pBar;
	DWORD dwStyle = GetStyle();
	pBar = GetScrollBarCtrl(SB_VERT);
	if ((pBar != NULL && !pBar->IsWindowEnabled()) ||
		(pBar == NULL && !(dwStyle & WS_VSCROLL)))
	{
		// vertical scroll bar not enabled
		pt.y = 0;
    yMax = 0;
	}
	pBar = GetScrollBarCtrl(SB_HORZ);
	if ((pBar != NULL && !pBar->IsWindowEnabled()) ||
		(pBar == NULL && !(dwStyle & WS_HSCROLL)))
	{
		// horizontal scroll bar not enabled
		pt.x = 0;
    xMax = 0;
	}

	// adjust current x position
	int x = oldScrollPos.x + pt.x;
	if (x < 0)
		x = 0;
	else if (x > xMax)
		x = xMax;

	// adjust current y position
	int y = oldScrollPos.y + pt.y;
	if (y < 0)
		y = 0;
	else if (y > yMax)
		y = yMax;

	SetScrollPos(SB_HORZ, x, FALSE);
	SetScrollPos(SB_VERT, y, FALSE);
  // if scrollbar movement cannot compensate for zooming, move mouse cursor
  #if 1
  if (x-oldScrollPos.x!=pto.x || y-oldScrollPos.y!=pto.y)
  {
    // move mouse so that we are pointing at the same pixel
    CPoint curPos;
    GetCursorPos(&curPos);
    curPos.x += pto.x-(x-oldScrollPos.x);
    curPos.y += pto.y-(y-oldScrollPos.y);
    CRect rect;
    GetClientRect(rect);
    ClientToScreen(rect);
    if (curPos.x<rect.left) curPos.x = rect.left;
    else if (curPos.x>rect.right) curPos.x = rect.right;
    if (curPos.y<rect.top) curPos.y = rect.top;
    else if (curPos.y>rect.bottom) curPos.y = rect.bottom;
    SetCursorPos(curPos.x,curPos.y);
  }
  #endif
  Invalidate(FALSE);
}

BOOL CTexViewView::OnMouseWheel(UINT nFlags, short zDelta, CPoint pt) 
{
  //CRect rect;
  ScreenToClient(&pt);
  _wheelAccumulate += zDelta;
  const int delta = WHEEL_DELTA;
  while (_wheelAccumulate<=-delta)
  {
    _wheelAccumulate += delta;
    if( _zoom<+4 )
    {
      // pt should keep its position
      bool right;
      CPoint logPtOld = GetLogicalPos(pt,&right);
      _zoom++;
      UpdateScroll();
      CPoint logPtNew = GetLogicalPos(pt,right);
      CPoint offset = logPtOld-logPtNew;
      CPoint scaledOffset(Zoom(offset.x,_zoom),Zoom(offset.y,_zoom));
      OffsetViewStart(scaledOffset);
    }
  }
  while (_wheelAccumulate>=delta)
  {
    _wheelAccumulate -= delta;
    if( _zoom>-4 )
    {
      bool right;
      CPoint logPtOld = GetLogicalPos(pt,&right);
      _zoom--;
      UpdateScroll();
      CPoint logPtNew = GetLogicalPos(pt,right);
      CPoint offset = logPtOld-logPtNew;
      CPoint scaledOffset(Zoom(offset.x,_zoom),Zoom(offset.y,_zoom));
      OffsetViewStart(scaledOffset);
    }
  }
	return FALSE;
}


