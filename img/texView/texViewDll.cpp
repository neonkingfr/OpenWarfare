// Dll interface for TexView functionality
#if 0
// does not work - some messaging or something like this is needed
// replaces older pal2pac, provides an alternative to command line

#include "stdafx.h"

#include "texview.h"
#include "..\img\Picture\picture.hpp"
#include <Es/essencepch.hpp>
#include <Es/Containers/array.hpp>
#include <Es/Memory/fastAlloc.hpp>
#include <Es/Memory/checkMem.hpp>
#include <Es/Files/filenames.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static bool InitConfig(HINSTANCE instance)
{
	// load texture hits config
	RString retString;
	LoadPictureConfigRetVal ret = LoadPictureConfig(retString);
	if (ret!=LoadPictureConfigOK)
	{
		RString msg;
		UINT msgId = IDS_MSG_NOCONFIG;
		char mod[256];
		GetModuleFileName(instance,mod,sizeof(mod));
		const char *modname = GetFilenameExt(mod);
		switch (ret)
		{
			case LoadPictureConfigNoRegistry: msgId = IDS_MSG_NOREGISTRY; break;
			case LoadPictureConfigNoFile: msgId = IDS_MSG_NOCONFIG; break;
			case LoadPictureConfigBadFile: msgId = IDS_MSG_BADCONFIG; break;
			case LoadPictureConfigOldExe: msgId = IDS_MSG_CONFIGOLDAPP; break;
		}


		char msgs[1024],buf[1024],app[1024];
		LoadString(instance,IDS_APPNAME,app,sizeof(app));
		LoadString(instance,msgId,msgs,sizeof(msgs));
		snprintf(buf,sizeof(buf),msgs,(const char *)retString,(const char *)modname);
		MessageBox(NULL,buf,app,MB_OK|MB_ICONERROR);
		return false;
	}
	return true;
}

int APIENTRY DllMain( HANDLE hdll, DWORD	reason, LPVOID reserved )
{
	(void)hdll,(void)reserved;
	switch( reason )
	{
	case DLL_THREAD_ATTACH:
		 /* do thread initialization */
		 break;

	case DLL_THREAD_DETACH:
		 /* do thread cleanup */
		 break;
	case DLL_PROCESS_ATTACH:
		if (!InitConfig((HINSTANCE)hdll))
		{
			return FALSE;
		}
		 /* do process initialization */
		 break;
	case DLL_PROCESS_DETACH:
		 /* do process cleanup */
		 break;
	}
	return( 1 ); 			 /* indicate success */
	/* returning 0 indicates initialization failure */

}


#define MYDLL __declspec(dllexport) __stdcall

// dynamic linking types etc...

extern "C"
{

int MYDLL UpdateTexture( const char *dest, const char *src, int size );

};

int MYDLL UpdateTexture( const char *dest, const char *src, int size )
{
	Picture pic;
	if( pic.Load(src)>=0 && pic.Valid() )
	{
		pic.SortPal();
		return pic.Save(dest);
	}
	return -1;
}

#endif