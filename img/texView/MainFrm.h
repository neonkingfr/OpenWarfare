// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__7E5B94A2_7BE3_11D3_8373_00A0C9DF4D61__INCLUDED_)
#define AFX_MAINFRM_H__7E5B94A2_7BE3_11D3_8373_00A0C9DF4D61__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CTexViewDoc;
class CTexViewView;
class CTexViewStatusView;
class CTexViewStatus2View;

class CStaticColorRect: public CStatic
{
  COLORREF _color;
  CBrush _brush;
  bool _colorSet;


  public:
  CStaticColorRect();
  ~CStaticColorRect();
  void SetColor(int r, int g, int b, bool update=true);
  void ResetColor(bool update=true);

  // message map handler
  afx_msg HBRUSH CtlColor(CDC* pDC, UINT nCtlColor);
  DECLARE_MESSAGE_MAP()
};

class CMainFrame : public CFrameWnd
{
protected:
	CSplitterWnd m_wndSplitter;
protected: // create from serialization only
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// Attributes
public:
	int _setNumber;
	int _setCount;
	CString _setName;
	CString _textureName;
	int _setW,_setH;
	int _itemW,_itemH;
	int _actR,_actG,_actB,_actA;
  bool _displayRGB01;
	
// Operations
public:

// Overrides

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	protected:
	virtual BOOL OnCreateClient(LPCREATESTRUCT lpcs, CCreateContext* pContext);
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:
  void LoadSccState(const char *regName);
  void SaveSccState(const char *regName);
  void LoadWndState();
  void SaveWndState();
  void FillViewCombo(CComboBox &combo, bool enableNone=true);
  void FillFormatCombo(CComboBox &combo);

protected:  // control bar embedded members
	CStatusBar  m_wndStatusBar;
	//CToolBar    m_wndToolBar;
  CDialogBar  m_wndSettingsBar;
  CDialogBar  m_wndInfoBar;
public:
  CComboBox   m_wndLViewType;
  CComboBox   m_wndRViewType;

  CComboBox   m_wndLFormat;
  CComboBox   m_wndRFormat;

  CStaticColorRect m_infoAvgColor;
  CStaticColorRect m_infoMaxColor;
  CStaticColorRect m_infoPixColor;

  CStatic m_infoAvgColorText;
  CStatic m_infoMaxColorText;
  CStatic m_infoPixColorText;

  CStatic m_infoFileSizeText;

  CTexViewView *GetView() const;
  CTexViewStatus2View *GetStatus2View() const;

  void SetAvgColor(int r, int g, int b);
  void SetMaxColor(int r, int g, int b);
  void SetPixColor(int x, int y, int r, int g, int b, int a);
  void SetFileSize(int size);

  void InitUpdateDoc(CTexViewDoc *doc);
  void SetStatusText(const char * text);
// Generated message map functions
protected:
  void SetColor(CStaticColorRect &color, CStatic &text, const char *prompt, int r, int g, int b);

	afx_msg void OnUpdatePage(CCmdUI *pCmdUI);
	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnDropFiles(HDROP hDropInfo);
	afx_msg void OnClose();
	afx_msg void OnEndSession(BOOL bEnding);
	afx_msg void OnViewInfoBar();
	afx_msg void OnUpdateViewInfoBar(CCmdUI* pCmdUI);
	afx_msg void OnViewSettingsBar();
	afx_msg void OnUpdateViewSettingsBar(CCmdUI* pCmdUI);
	afx_msg void OnSccOptions();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnViewRgb0();
  afx_msg void OnUpdateViewRgb0(CCmdUI *pCmdUI);
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__7E5B94A2_7BE3_11D3_8373_00A0C9DF4D61__INCLUDED_)
