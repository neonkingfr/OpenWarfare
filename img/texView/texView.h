// texView.h : main header file for the TEXMERGE application
//

#if !defined(AFX_TEXMERGE_H__7E5B949E_7BE3_11D3_8373_00A0C9DF4D61__INCLUDED_)
#define AFX_TEXMERGE_H__7E5B949E_7BE3_11D3_8373_00A0C9DF4D61__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CTexViewApp:
// See texView.cpp for the implementation of this class
//

class CTexViewApp : public CWinApp
{

public:
	CTexViewApp();
	~CTexViewApp();

// Overrides
	virtual BOOL ProcessMessageFilter( int code, LPMSG lpMsg );
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTexViewApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	virtual BOOL OnIdle(LONG lCount);
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CTexViewApp)
	afx_msg void OnAppAbout();
	afx_msg void OnFilePref();
	afx_msg void OnSelchangeViewL();
	afx_msg void OnSelchangeViewR();
	afx_msg void OnSelchangeFormatL();
	afx_msg void OnSelchangeFormatR();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TEXMERGE_H__7E5B949E_7BE3_11D3_8373_00A0C9DF4D61__INCLUDED_)
