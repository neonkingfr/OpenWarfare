// ProcedTex.cpp : implementation file
//

#include "stdafx.h"
#include "texview.h"
#include "ProcedTex.h"
#include "texviewDoc.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// ProcedTex dialog


ProcedTex::ProcedTex(CWnd* pParent, CTexViewDoc *doc)
	: CDialog(ProcedTex::IDD, pParent),_doc(doc)
{
  _destroyed = false;
	//{{AFX_DATA_INIT(ProcedTex)
	_expression = _T("");
	_h = 0;
	_w = 0;
	//}}AFX_DATA_INIT
}

BOOL ProcedTex::DestroyWindow()
{
  _destroyed = true;
  return base::DestroyWindow();
}


void ProcedTex::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(ProcedTex)
	DDX_Text(pDX, IDC_EDIT1, _expression);
	DDX_Text(pDX, IDC_HEIGHT, _h);
	DDV_MinMaxInt(pDX, _h, 4, 65536);
	DDX_Text(pDX, IDC_WIDTH, _w);
	DDV_MinMaxInt(pDX, _w, 4, 65536);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(ProcedTex, CDialog)
	//{{AFX_MSG_MAP(ProcedTex)
	ON_WM_CLOSE()
	ON_BN_CLICKED(IDC_APPLY, OnApply)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// ProcedTex message handlers

void ProcedTex::OnClose() 
{
	CDialog::OnClose();
  DestroyWindow();
}

void ProcedTex::OnApply() 
{
	UpdateData();
  _doc->GenerateProcedural(_w,_h,_expression);
}

BOOL ProcedTex::OnInitDialog() 
{
  if (_expression.IsEmpty())
  {
    _expression = 
      "r=1;\r\n"
      "g=1;\r\n"
      "b=1;\r\n"
      "a=1;";
  }
  if (_w<4) _w = 128;
  if (_h<4) _h = 128;
	CDialog::OnInitDialog();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
