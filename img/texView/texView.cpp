// texView.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "shlobj.h"
#include "texView.h"

#include "MainFrm.h"
#include "texViewDoc.h"
#include "texViewView.h"
#include <Es/Files/filenames.hpp>

//#include "..\lib\paramFile.hpp"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//#include <class\rstring.cpp>


/////////////////////////////////////////////////////////////////////////////
// CTexViewApp

BEGIN_MESSAGE_MAP(CTexViewApp, CWinApp)
	//{{AFX_MSG_MAP(CTexViewApp)
	ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
	ON_COMMAND(ID_FILE_PREF, OnFilePref)
	ON_CBN_SELCHANGE(IDC_VIEW_L, OnSelchangeViewL)
	ON_CBN_SELCHANGE(IDC_VIEW_R, OnSelchangeViewR)
	ON_CBN_SELCHANGE(IDC_FORMAT_L, OnSelchangeFormatL)
	ON_CBN_SELCHANGE(IDC_FORMAT_R, OnSelchangeFormatR)
	//}}AFX_MSG_MAP
	// Standard file based document commands
	ON_COMMAND(ID_FILE_NEW, CWinApp::OnFileNew)
	ON_COMMAND(ID_FILE_OPEN, CWinApp::OnFileOpen)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CTexViewApp construction

CTexViewApp::CTexViewApp()
{
}

CTexViewApp::~CTexViewApp()
{
	//TerminateProcess(GetCurrentProcess(),0);
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CTexViewApp object

CTexViewApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CTexViewApp initialization

class CMySingleDocTemplate: public CSingleDocTemplate 
{
public:
  CMySingleDocTemplate( UINT nIDResource, CRuntimeClass* pDocClass, CRuntimeClass* pFrameClass, CRuntimeClass* pViewClass )
  :CSingleDocTemplate(nIDResource,pDocClass,pFrameClass,pViewClass)
  {
  }
  BOOL GetDocString(CString& rString, DocStringIndex i) const;
  CDocTemplate::Confidence MatchDocType(
    const char* pszPathName, CDocument*& rpDocMatch
  );

};


BOOL CMySingleDocTemplate::GetDocString(
  CString& rString, DocStringIndex i
) const
{
  CString strTemp,strLeft,strRight;
  
  AfxExtractSubString(strTemp, m_strDocStrings, (int)i);
  
  /*
  if(i == CDocTemplate::filterExt)
  {
    int nFindPos=strTemp.Find(';');
    if(-1 != nFindPos) {
      //string contains two extensions
      strLeft=strTemp.Left(nFindPos+1);
      strRight="*"+strTemp.Right(lstrlen((const
        char*)strTemp)-nFindPos-1);
      strTemp=strLeft+strRight;
    }
  }
  */
  rString = strTemp;
  return TRUE;
}

CDocTemplate::Confidence CMySingleDocTemplate::MatchDocType(
  const char* pszPathName, CDocument*& rpDocMatch
)
{
  ASSERT(pszPathName != NULL);
  rpDocMatch = NULL;
  
  // go through all documents
  /*
  POSITION pos = GetFirstDocPosition();
  while (pos != NULL)
  {
    CDocument* pDoc = GetNextDoc(pos);
    if (pDoc->GetPathName() == pszPathName) {
      // already open
      rpDocMatch = pDoc;
      return yesAlreadyOpen;
    }
  }  // end while
  */
  
  // see if it matches either suffix
  CString strFilterExt;
  if (GetDocString(strFilterExt, CDocTemplate::filterExt) &&
    !strFilterExt.IsEmpty())
  {
    // see if extension matches
    ASSERT(strFilterExt[0] == '.');
    CString ext1,ext2;
    int nDot = CString(pszPathName).ReverseFind('.');
    //const char* pszDot = nDot < 0 ? NULL : pszPathName + nDot;
    
    int nSemi = strFilterExt.Find(';');
    if(-1 != nSemi)   {
      // string contains two extensions
      ext1=strFilterExt.Left(nSemi);
      ext2=strFilterExt.Mid(nSemi+2);
      // check for a match against either extension
      if (nDot >= 0 && (lstrcmpi(pszPathName+nDot, ext1) == 0
        || lstrcmpi(pszPathName+nDot,ext2) ==0))
        return yesAttemptNative; // extension matches
    }
    else
    { // string contains a single extension
      if (nDot >= 0 && (lstrcmpi(pszPathName+nDot,
        strFilterExt)==0))
        return yesAttemptNative;  // extension matches
    }
  }
  return yesAttemptForeign; //unknown document type
}

class CTexViewCommandLineInfo: public CCommandLineInfo
{
  // if both src and dst are specified, we will perform automated conversion
  CString src,dst;
  enum NextArg
  {
    ArgUnknown,ArgSrc,ArgDst
  };
  NextArg nextArg;

public:
  CTexViewCommandLineInfo();

  bool BatchOp();
  // override
	virtual void ParseParam(const TCHAR* pszParam, BOOL bFlag, BOOL bLast);

};

CTexViewCommandLineInfo::CTexViewCommandLineInfo()
{
  nextArg = ArgUnknown;
}

void CTexViewCommandLineInfo::ParseParam(const TCHAR* pszParam, BOOL bFlag, BOOL bLast)
{
  if (bFlag)
  {
    if (!strcmpi(pszParam,"-convert")) nextArg = ArgSrc;
  }
  else
  {
    switch (nextArg)
    {
      case ArgSrc: src = pszParam; nextArg=ArgDst;break;
      case ArgDst: dst = pszParam; nextArg=ArgUnknown;break;
    }
  }
}

bool CTexViewCommandLineInfo::BatchOp()
{
  if (src.IsEmpty() || dst.IsEmpty()) return false;
  // both src and dst provided - perform batch op

	Picture pic;
	if( pic.Load(src)>=0 && pic.Valid() )
	{
		pic.SortPal();

		if( pic.Save(dst)<0 )
    {
      fprintf(stderr,"Error while saving %s",cc_cast(dst));
      // error while saving
      return true;
    }
		return true;
		
	}
  fprintf(stderr,"Error while loading %s",cc_cast(src));

  return true;
}

BOOL CTexViewApp::InitInstance()
{
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	// Change the registry key under which our settings are stored.
	// You should modify this string to be something appropriate
	// such as the name of your company or organization.
	SetRegistryKey(_T("Bohemia Interactive Studio"));

	LoadStdProfileSettings();  // Load standard INI file options (including MRU)

	// Register the application's document templates.  Document templates
	//  serve as the connection between documents, frame windows and views.

	CSingleDocTemplate* pDocTemplate;
	pDocTemplate = new CMySingleDocTemplate(
	//pDocTemplate = new CSingleDocTemplate(
		IDR_MAINFRAME,
		RUNTIME_CLASS(CTexViewDoc),
		RUNTIME_CLASS(CMainFrame),       // main SDI frame window
		RUNTIME_CLASS(CTexViewView));
	AddDocTemplate(pDocTemplate);

	// Enable DDE Execute open
	EnableShellOpen();
	RegisterShellFileTypes(TRUE);

	// load texture hints config
	RString retString;
	LoadPictureConfigRetVal ret = LoadPictureConfig(retString);
	if (ret!=LoadPictureConfigOK)
	{
		CString msg;
		UINT msgId = IDS_MSG_NOCONFIG;
		char mod[256];
		GetModuleFileName(NULL,mod,sizeof(mod));
		CString modname = GetFilenameExt(mod);
		switch (ret)
		{
			case LoadPictureConfigNoRegistry: msgId = IDS_MSG_NOREGISTRY; break;
			case LoadPictureConfigNoFile: msgId = IDS_MSG_NOCONFIG; break;
			case LoadPictureConfigBadFile: msgId = IDS_MSG_BADCONFIG; break;
			case LoadPictureConfigOldExe: msgId = IDS_MSG_CONFIGOLDAPP; break;
		}


		AfxFormatString2(msg,msgId,(const char *)retString,(const char *)modname);
		AfxMessageBox(msg);
		return FALSE;
	}

	// Parse command line for standard shell commands, DDE, file open
	CTexViewCommandLineInfo autoCmd;
	ParseCommandLine(autoCmd);

  if (autoCmd.BatchOp())
  {
    return FALSE;
  }
  
  // if there was no command line, parse using default parser
	CCommandLineInfo cmdInfo;
	ParseCommandLine(cmdInfo);
  
  //if (cmdInfo.Auto)
	// Dispatch commands specified on the command line
	if (!ProcessShellCommand(cmdInfo))
		return FALSE;

	// The one and only window has been initialized, so show and update it.
	m_pMainWnd->ShowWindow(SW_SHOW);
	m_pMainWnd->UpdateWindow();

	// Enable drag/drop open
	m_pMainWnd->DragAcceptFiles();

	return TRUE;
}

int CTexViewApp::ExitInstance()
{
  UnloadPictureConfig();
  return CWinApp::ExitInstance();
}

BOOL CTexViewApp::ProcessMessageFilter( int code, LPMSG lpMsg )
{
#if 0 // It makes no sense to ask for extension - see CMainFrame::OnDropFiles where WM_USER is invoked. The name needs not to be a string at all (and leads to crash on Vista)
	// MSG
	if( lpMsg->message==WM_USER )
	{
		// lParam is file name of dropped file
		const char *name=(const char *)lpMsg->lParam;

		// file is not document
		CMainFrame *main=(CMainFrame *)m_pMainWnd;
		CTexViewDoc *doc=(CTexViewDoc *)main->GetActiveDocument();
		//CTexViewView *view=(CTexViewView *)main->GetActiveView();

		// check file extension
		// file can be texture or model
		const char *ext=strrchr(name,'.');
		if( ext )
		{
			if( !strcmpi(ext,".pac") || !strcmpi(ext,".paa") || !strcmpi(ext,".jpg") )
			{
				// file is texture
				doc->AddItem(name);
			}
			else
			{
				Log("Unknown file %s dropped",name);
				// unknown file - ignore
			}
		}
		return TRUE;
	}
#endif
	return CWinApp::ProcessMessageFilter(code,lpMsg);
}

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
		// No message handlers
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// App command to run the dialog
void CTexViewApp::OnAppAbout()
{
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
}

const char szAppName[]="TexView";

/////////////////////////////////////////////////////////////////////////////
// CTexViewApp commands

BOOL CTexViewApp::OnIdle(LONG lCount) 
{
	CMainFrame *main=(CMainFrame *)m_pMainWnd;
	CTexViewDoc *doc=(CTexViewDoc *)main->GetActiveDocument();
  if (!doc) return CWinApp::OnIdle(lCount);
	CTexViewView *view=(CTexViewView *)main->GetView();

	main->_setNumber=view->GetActSet();
	main->_setCount=doc->GetTextureSetCount();

	const TextureSet *set=doc->GetTextureSet(view->GetActSet());
	if( set )
	{
		const TextureItem *item=&set->_item;
		main->_setW=set->_w;
		main->_setH=set->_h;
		main->_itemW=item->w;
		main->_itemH=item->h;
		main->_textureName=item->GetTexture();
    view->GetActARGB(main->_actA,main->_actR,main->_actG,main->_actB);
	}
	else
	{
		main->_setW=0;
		main->_setH=0;
		main->_itemW=0;
		main->_itemH=0;
		main->_textureName="";
		main->_actA = -1;
		main->_actR = -1;
		main->_actG = -1;
		main->_actB = -1;
	}
	return CWinApp::OnIdle(lCount);
}


static int CALLBACK BrowseCallbackProc
( 
    HWND hwnd,  UINT uMsg,  LPARAM lParam,  LPARAM lpData
)
{
	BROWSEINFO *bInfo=(BROWSEINFO *)lpData;
	switch( uMsg )
	{
		case BFFM_SELCHANGED:
		{
			LPITEMIDLIST idList=(LPITEMIDLIST)lParam;
			char buffer[MAX_PATH];
			if( ::SHGetPathFromIDList(idList, buffer) )
			{
				LPARAM lParam=(LPARAM)buffer;
				::SendMessage(hwnd,BFFM_SETSTATUSTEXT,0,lParam);
			}
			else
			{
				LPARAM lParam=(LPARAM)"Please select folder";
				::SendMessage(hwnd,BFFM_SETSTATUSTEXT,0,lParam);
			}
		}
		break;
		case BFFM_INITIALIZED:
		{
				LPARAM lParam=(LPARAM)bInfo->pszDisplayName;
				::SendMessage(hwnd,BFFM_SETSELECTION,1,lParam);
		}
		break;
	}
	return 0;
}

bool BrowseForFolder( HWND parent, CString title, CString &path )
{
  BROWSEINFO bi; 
  HRESULT err;
  LPMALLOC shMalloc;
  err=::CoGetMalloc(1,&shMalloc);
  if( err!=S_OK ) return false;
  // Allocate a buffer to receive browse information. 
  LPSTR buffer = (LPSTR) shMalloc->Alloc(MAX_PATH);
  if( !buffer ) return false;

	bool ret=false;
  strcpy(buffer,path);
  // Fill in the BROWSEINFO structure. 
  bi.hwndOwner = NULL;
  bi.pidlRoot = NULL; 
  bi.pszDisplayName = buffer; 
  bi.lpszTitle = title;
  bi.ulFlags = BIF_RETURNONLYFSDIRS|BIF_STATUSTEXT; 
  bi.lpfn = BrowseCallbackProc; 
  bi.lParam = (LPARAM)&bi;

  // Browse for a folder and return its PIDL. 
  LPITEMIDLIST browse = ::SHBrowseForFolder(&bi);
  if( browse )
  { 
      // Show the display name, title, and file system path. 
      ::SHGetPathFromIDList(browse, buffer);
      path=buffer;
			ret=true;
      //path=AddFolder(path,"Poseidon");
      shMalloc->Free(browse); 
  } 
  // Clean up. 
  shMalloc->Free(buffer); 
  shMalloc->Release(); // release interface
	return ret;
}

void CTexViewApp::OnFilePref()
{
}


void CTexViewApp::OnSelchangeViewL() 
{
	CMainFrame *main=(CMainFrame *)m_pMainWnd;
	CTexViewView *view=(CTexViewView *)main->GetView();
  int sel = main->m_wndLViewType.GetCurSel();
  int data = sel>=0 ? main->m_wndLViewType.GetItemData(sel) : -1;
  ViewChannel ch = data>=0 ? (ViewChannel)data : ViewNone;
  view->SetChannelL(ch);
}

void CTexViewApp::OnSelchangeViewR() 
{
	CMainFrame *main=(CMainFrame *)m_pMainWnd;
	CTexViewView *view=(CTexViewView *)main->GetView();
  int sel = main->m_wndRViewType.GetCurSel();
  int data = sel>=0 ? main->m_wndRViewType.GetItemData(sel) : -1;
  ViewChannel ch = data>=0 ? (ViewChannel)data : ViewNone;
  view->SetChannelR(ch);
  main->m_wndRFormat.EnableWindow(ch!=ViewNone);
}

void CTexViewApp::OnSelchangeFormatL() 
{
	CMainFrame *main=(CMainFrame *)m_pMainWnd;
	CTexViewView *view=(CTexViewView *)main->GetView();
  int sel = main->m_wndLFormat.GetCurSel();
  int data = sel>=0 ? main->m_wndLFormat.GetItemData(sel) : -1;
  TexFormat f = data>=0 ? (TexFormat)data : TexFormARGB8888;
  view->SetFormatL(f);
}

void CTexViewApp::OnSelchangeFormatR() 
{
	CMainFrame *main=(CMainFrame *)m_pMainWnd;
	CTexViewView *view=(CTexViewView *)main->GetView();
  int sel = main->m_wndRFormat.GetCurSel();
  int data = sel>=0 ? main->m_wndRFormat.GetItemData(sel) : -1;
  TexFormat f = data>=0 ? (TexFormat)data : TexFormARGB8888;
  view->SetFormatR(f);
}
