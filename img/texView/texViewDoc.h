// texMergeDoc.h : interface of the CTexViewDoc class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_TEXMERGEDOC_H__7E5B94A4_7BE3_11D3_8373_00A0C9DF4D61__INCLUDED_)
#define AFX_TEXMERGEDOC_H__7E5B94A4_7BE3_11D3_8373_00A0C9DF4D61__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class QOStream;
class QIStream;

//#include "..\lib\pacText.hpp"

inline int Zoom( int val, int zoom )
{
	if( zoom>=0 ) return val<<zoom;
	return val>>-zoom;
}

/*
class PacLevelSys: public RemoveLinks,public PacLevel,public CLRefLink
{
};
*/

#include "..\img\Picture\picture.hpp"

class PictureData: public Picture,public RefCount
{
};

struct TextureItem
{
private:
	CString _name; // path name (relative to current directory)
	int _level;
	mutable Ref<PictureData> _tex;
public:
	int scale; // scale (can be negative - zoom out)
	// following data is calculated from texture file
	int w,h; // texture w,h
public:
	TextureItem();

	CString GetName() const {return _name;} // path name (relative to current directory)

	void Copy( const TextureItem &item );
	void operator = ( const TextureItem &item ) {Clear();Copy(item);}
	TextureItem( const TextureItem &item ) {Copy(item);}

	void Clear();
	void SetTexture( CString tex, int level );
	void SetTexture( CString tex, int level, const Picture &pic );
	CString GetTexture() const {return _name;}
	int GetLevel() const {return _level;}
  TexFormat GetSourceFormat() const;

	int UsedW() const; // area used in set
	int UsedH() const;

  void SetTex(PictureData *pic);

	PictureData *GetTex() const; // load if neccessary
	void Compact(); // unload if neccessary
};

TypeIsGeneric(TextureItem);

const int MinSetDimension=64;
const int MaxSetDimension=2048;

// do not know anything about CString structure
// maybe it is movable?

TypeIsGeneric(CString);

class TextureSet
{
public:
	TextureItem _item;
	int _w,_h; // max. dimensions
	bool _alpha;
	bool _itemSet;

	TextureSet();
	void Clear();
};

TypeIsMovable(TextureSet);

//! mipmap generation mode (mipmap filter selection)
enum GenMipMode
{
	MipDefault,                 // Autoselect
	MipDetail,                  // Fade out mipmaps
	MipNormalizeNormalMap,      // Normal map normalization filter
  MipNormalizeNormalMapFade,  // Normal map normalization filter with fading
	MipNormal,                  // Normal mipmaps
	MipAlphaNoise,              // Alpha noise
  MipNormalizeNormalMapNoise, // Normal map normalization with noise in alpha filter
  MipAddAlphaNoise,           // add noise to existing alpha (for trees)
};

class ProcedTex;

class CTexViewDoc : public CDocument
{

protected: // create from serialization only
	CTexViewDoc();
	DECLARE_DYNCREATE(CTexViewDoc)

  bool ValidateProcedTex();
  void CloseProcedTex();
// Attributes
public:

private:
	AutoArray<TextureSet> _textures;
  // remember some document properties so that we can detect document change
  QFileSize _size;
  long _time;

  // all files in the directory
  RString _filesWild;
  FindArrayKey<RString> _files;
  ProcedTex *_procedTex;
  /// last procedural filter source
  RString _filter;

  bool InitFind();

// Operations
public:
	const TextureSet *GetTextureSet( int i ) const
	{
		if( i>=0 && i<_textures.Size() ) return &_textures[i];
		return NULL;
	}
	TextureSet *GetTextureSet( int i )
	{
		if( i>=0 && i<_textures.Size() ) return &_textures[i];
		return NULL;
	}
	int GetTextureSetCount() const {return _textures.Size();}
	void Clear();

  BOOL DoSave(LPCTSTR lpszPathName, BOOL bReplace);
// Overrides
	virtual BOOL IsModified();
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CTexViewDoc)
	public:
	virtual BOOL OnNewDocument();
	virtual void Serialize(CArchive& ar);
	virtual void OnCloseDocument();
	virtual BOOL OnOpenDocument(LPCTSTR lpszPathName);
	virtual BOOL OnSaveDocument(LPCTSTR lpszPathName);
	//}}AFX_VIRTUAL

// Implementation
public:
  void GenerateProcedural(int w, int h, const char *expression);
	void GenerateMipmaps(GenMipMode mode = MipDefault);
  void AddItem( const char *name);
	int AddItem( const char *name, int level );
	int AddItem(PictureData *pic);

	virtual ~CTexViewDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif


// Generated message map functions
protected:
	//{{AFX_MSG(CTexViewDoc)
	afx_msg void OnFileOpen();
	afx_msg void OnFileNext();
	afx_msg void OnFilePrev();
	afx_msg void OnFileRefresh();
	afx_msg void OnUpdateFileNext(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFilePrev(CCmdUI* pCmdUI);
	afx_msg void OnUpdateFileRefresh(CCmdUI* pCmdUI);
	afx_msg void OnGenProc();
	afx_msg void OnFilterProc();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_TEXMERGEDOC_H__7E5B94A4_7BE3_11D3_8373_00A0C9DF4D61__INCLUDED_)
