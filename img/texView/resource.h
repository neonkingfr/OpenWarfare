//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by texView.rc
//
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_TEXMERTYPE                  129
#define IDS_SAVE_ERROR                  129
#define IDD_PREVIEW                     131
#define IDD_INFOBAR                     134
#define IDD_PROCED_TEX                  135
#define IDD_PROCED_FILTER               136
#define IDC_CURSOR1                     136
#define IDC_VISIBLECROSS                136
#define IDI_OLD_MAINFRAME               138
#define IDI_ICON1                       143
#define IDI_ICON2                       144
#define IDC_VIEW_L                      1001
#define IDC_VIEW_R                      1002
#define IDC_FORMAT_L                    1003
#define IDC_FORMAT_R                    1004
#define IDC_MAX_COLOR_TEXT              1011
#define IDC_AVG_COLOR_TEXT              1012
#define IDC_PIX_COLOR_TEXT              1013
#define IDC_MAX_COLOR                   1016
#define IDC_AVG_COLOR                   1017
#define IDC_PIX_COLOR                   1018
#define IDC_PIX_COLOR_G                 1019
#define IDC_CHECK1                      1020
#define IDC_EDIT1                       1021
#define IDC_APPLY                       1022
#define IDC_EDIT2                       1022
#define IDC_WIDTH                       1023
#define IDC_HEIGHT                      1024
#define IDC_PF_LOAD                     1025
#define IDC_PF_SAVE                     1026
#define ID_TIMER_PIXELINFO              10000
#define ID_SET_ADD                      32771
#define ID_SET_DELETE                   32772
#define ID_SET_DUPLICATE                32773
#define ID_ITEM_ADD                     32774
#define ID_ITEM_DELETE                  32775
#define ID_SET_NEXT                     32776
#define ID_SET_PREV                     32777
#define ID_ITEM_PREV                    32778
#define ID_ITEM_NEXT                    32779
#define ID_VIEW_ZOOM_IN                 32780
#define ID_VIEW_ZOOM_OUT                32781
#define ID_SET_GROW                     32783
#define ID_SET_SHRINK                   32784
#define ID_ITEM_GROW                    32785
#define ID_ITEM_SHRINK                  32786
#define ID_SET_GROW_ALL                 32788
#define ID_SET_SHRINK_ALL               32789
#define ID_SET_GROW_X                   32791
#define ID_SET_SHRINK_X                 32792
#define ID_SET_GROW_Y                   32793
#define ID_SET_SHRINK_Y                 32794
#define ID_ITEM_ROT_LEFT                32795
#define ID_ITEM_ROT_RIGHT               32796
#define ID_SET_ARRANGE                  32799
#define ID_SET_MODEL                    32800
#define ID_SET_COPY                     32801
#define ID_SET_MERGE                    32802
#define ID_FILE_EXPORT                  32806
#define ID_FILE_PREF                    32807
#define ID_FILE_PROP                    32808
#define ID_MIP_GENERATE                 32811
#define ID_MIP_GENERATESPEC             32812
#define ID_MIP_GENDETAILTEX             32814
#define ID_MIP_GEN_NORMALTEX            32816
#define ID_MIPMAP_GENERATESPECIAL_NORMALIZEDNORMALTEXTURE 32817
#define ID_FILE_PREVIEW                 32819
#define ID_FILE_REFRESH                 32820
#define ID_FILE_NEXT                    32821
#define ID_FILE_PREV                    32822
#define ID_VIEW_INFO_BAR                32823
#define ID_VIEW_SETTINGS_BAR            32824
#define ID_GEN_PROC                     32825
#define ID_FILTER_PROC                  32826
#define ID_SCC_CHECKIN                  32828
#define ID_SCC_CHECKOUT                 32829
#define ID_SCC_GET                      32830
#define ID_SCC_OPTIONS                  32831
#define ID_MIPMAP_GENERATESPECIAL_NORMALIZEDNORMALTEXTUREWITHFADING 32834
#define ID_VIEW_RGB0                    32835
#define ID_PREVIEWMIPMAP_COLORTEXTURE   32836
#define ID_INDICATOR_PAGE               59142
#define IDS_FILEOPENFILTERS             61204
#define IDS_FILEDEFEXT                  61205
#define IDS_MSG_NOREGISTRY              61206
#define IDS_MSG_NOCONFIG                61207
#define IDS_MSG_BADCONFIG               61208
#define IDS_MSG_CONFIGERR               61209
#define IDS_MSG_CONFIGOLDAPP            61210
#define IDS_FILEFILTEREXT               61211
#define IDS_FILEFILTFILTERS             61212
#define IDS_APPNAME                     61213
#define IDS_MSG_BADCONFIG2              61214
#define IDS_MSG_FILE_LOADERROR          61214

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        145
#define _APS_NEXT_COMMAND_VALUE         32837
#define _APS_NEXT_CONTROL_VALUE         1026
#define _APS_NEXT_SYMED_VALUE           102
#endif
#endif
