#if !defined(AFX_PROCEDTEX_H__0E49DC8D_0E82_4688_B64C_0947286988D7__INCLUDED_)
#define AFX_PROCEDTEX_H__0E49DC8D_0E82_4688_B64C_0947286988D7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ProcedTex.h : header file
//

class CTexViewDoc;
/////////////////////////////////////////////////////////////////////////////
// ProcedTex dialog

class ProcedTex : public CDialog
{

  typedef CDialog base;

  bool _destroyed;
  CTexViewDoc *_doc;

public:
// Construction
	ProcedTex(CWnd* pParent, CTexViewDoc *doc);   // standard constructor
  BOOL DestroyWindow();
  bool IsDestroyed() const {return _destroyed;}

// Dialog Data
	//{{AFX_DATA(ProcedTex)
	enum { IDD = IDD_PROCED_TEX };
	CString	_expression;
	int		_h;
	int		_w;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(ProcedTex)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(ProcedTex)
	afx_msg void OnClose();
	afx_msg void OnApply();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROCEDTEX_H__0E49DC8D_0E82_4688_B64C_0947286988D7__INCLUDED_)
