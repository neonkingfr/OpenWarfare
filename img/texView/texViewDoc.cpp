// texViewDoc.cpp : implementation of the CTexViewDoc class
//

#include "stdafx.h"
#include "texView.h"

#include "texViewDoc.h"
#include "procedTex.h"
#include "procedFilter.h"
#include "mainFrm.h"
#include <io.h>

#include <Es/Types/lLinks.hpp>
#include <Es/Files/filenames.hpp>
#include <Es/Algorithms/qSort.hpp>

//#include "..\posAccess\posAccess.hpp"
//#include "..\lib\shape.hpp"
#include "..\img\Picture\picture.hpp"
#include <El/Scc/scc.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CTexViewDoc

IMPLEMENT_DYNCREATE(CTexViewDoc, CDocument)

BEGIN_MESSAGE_MAP(CTexViewDoc, CDocument)
	//{{AFX_MSG_MAP(CTexViewDoc)
	ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
	ON_COMMAND(ID_FILE_NEXT, OnFileNext)
	ON_COMMAND(ID_FILE_PREV, OnFilePrev)
	ON_COMMAND(ID_FILE_REFRESH, OnFileRefresh)
	ON_UPDATE_COMMAND_UI(ID_FILE_NEXT, OnUpdateFileNext)
	ON_UPDATE_COMMAND_UI(ID_FILE_PREV, OnUpdateFilePrev)
	ON_UPDATE_COMMAND_UI(ID_FILE_REFRESH, OnUpdateFileRefresh)
	ON_COMMAND(ID_GEN_PROC, OnGenProc)
	ON_COMMAND(ID_FILTER_PROC, OnFilterProc)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

// SCC selector

static MsSccFunctions MsScc;

SccFunctions *Scc = &MsScc;

/////////////////////////////////////////////////////////////////////////////
// CTexViewDoc construction/destruction

CTexViewDoc::CTexViewDoc()
{
  _size = 0;
  _time = 0;
  _procedTex = NULL;
}

void CTexViewDoc::CloseProcedTex()
{
  if (!_procedTex) return;
  if (!_procedTex->IsDestroyed())
  {
    _procedTex->DestroyWindow();
  }
  delete _procedTex;
  _procedTex = NULL;
}

bool CTexViewDoc::ValidateProcedTex()
{
  if (_procedTex)
  {
    if (_procedTex->IsDestroyed())
    {
      delete _procedTex;
      _procedTex = NULL;
      return false;
    }
    return true;
  }
  return false;
}

void CTexViewDoc::Clear()
{
  _size = 0;
  _time = 0 ;
	_textures.Clear();
	_textures.Add();
  //_files.Clear();
  //_filesWild = "";
  CloseProcedTex();
}

static inline int CmpFilename(const RString *s1, const RString *s2)
{
  // sort by extension first
  const char *ext1 = GetFileExt(*s1);
  const char *ext2 = GetFileExt(*s2);
  int d = strcmp(ext1,ext2);
  if (d) return d;
  return strcmpi(*s1,*s2);
}

bool CTexViewDoc::InitFind()
{
  // current document filename
  CString path = GetPathName();
  char wild[1024];
  strcpy(wild,path);
  strcpy(GetFilenameExt(wild),"*");
  if (!strcmpi(wild,_filesWild)) return _files.Size()>0;
  _filesWild = wild;
  
  // create a wildcard search
  //const char *srcFilename = GetFilenameExt(path);
  _finddata_t findData;
  long findHandle = _findfirst(wild,&findData);
  // iterate until the match is found
  do
  {
    if (findData.attrib&_A_SUBDIR) continue;
    const char *ext = GetFileExt(findData.name);
    if (
      !strcmpi(ext,".tga") ||
      !strcmpi(ext,".paa") ||
      !strcmpi(ext,".pac") ||
      !strcmpi(ext,".dds") ||
      !strcmpi(ext,".png")
    )
    {
      strcpy(GetFilenameExt(wild),findData.name);
      _files.Add(wild);
    }
  }
  while (_findnext(findHandle,&findData)==0);
  // no match - cannot find
  _findclose(findHandle);
  QSort(_files,CmpFilename);
  //_findFailed = true;
  return _files.Size()>0;
}

CTexViewDoc::~CTexViewDoc()
{
	_textures.Clear();
}

BOOL CTexViewDoc::IsModified()
{
	return false;
}

BOOL CTexViewDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// (SDI documents will reuse this document)
	// always start with one texture set (empty)
	_textures.Clear();
	_textures.Add();

	return TRUE;
}



// implementation of underlying classes

TextureItem::TextureItem()
{
	Clear();
}

void TextureItem::Clear()
{
	scale=0;
	_name="";
	_level=-1;
	// file information
	w=h=0;
}

// global texture heap

int TextureItem::UsedW() const
{
	int zw=Zoom(w,scale);
	return zw;
}

int TextureItem::UsedH() const
{
	int zh=Zoom(h,scale);
	return zh;
}

void TextureItem::SetTexture( CString tex, int level )
{
	if( strcmpi(_name,tex) || _level!=level )
	{
		_name=tex;
		_tex=NULL;
		_level=level;
	}

  Ref<PictureData> texture=new PictureData();

	if (texture->Load(_name,_level)<0)
	{
	  texture = NULL;
	  // if loading other mipmap fails, we do not care
	  // this is normal for many file formats which do not contain mipmaps
	  if (level==0)
	  {
		  CString msg;
		  AfxFormatString1(msg,IDS_MSG_FILE_LOADERROR,(const char *)_name);
		  AfxMessageBox(msg);
	  }
	}
	else if (texture->W()<=0 || texture->H()<=0 ) texture = NULL;

	_tex=texture;

	if( !texture )
	{
		w=0;
		h=0;
	}
	else
	{
		w=texture->W();
		h=texture->H();
	}
}

void TextureItem::SetTexture( CString tex, int level, const Picture &pic )
{
	if( strcmpi(_name,tex) || _level!=level )
	{
		_name=tex;
		_tex=NULL;
		_level=level;
	}
	_tex = new PictureData;
	_tex->Picture::operator =(pic);
	w=_tex->W();
	h=_tex->H();
}

TexFormat TextureItem::GetSourceFormat() const
{
  PictureData *tex = GetTex();
  if (!tex) return TexFormDefault;
  return tex->GetSourceFormat();
  
}

void TextureItem::SetTex(PictureData *pic)
{
  _tex = pic;
  _name = "";
  _level = 0;
  w = pic->Width();
  h = pic->Height();
}

PictureData *TextureItem::GetTex() const
{
	if (_level<0) return NULL;
	// load if neccessary
	return _tex;
}

void TextureItem::Compact()
{
	// TODO: implement
	// unload if neccessary
}


void TextureItem::Copy( const TextureItem &item )
{
	_name=item._name; // path name (relative to current directory)
	_level=item._level;
	_tex=item._tex;
	scale=item.scale;
	w=item.w;
	h=item.h;
}

TextureSet::TextureSet()
{
	_w=16;
	_h=16;
	_itemSet=false;
}
void TextureSet::Clear()
{
	_itemSet=false;
}

#define Compose(a,r,g,b) \
	((a)<<24)|((r)<<16)|((g)<<8)|((b)<<0)



/////////////////////////////////////////////////////////////////////////////
// CTexViewDoc serialization

void CTexViewDoc::Serialize(CArchive& ar)
{
}

/////////////////////////////////////////////////////////////////////////////
// CTexViewDoc diagnostics

#ifdef _DEBUG
void CTexViewDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CTexViewDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CTexViewDoc commands

void CTexViewDoc::AddItem( const char *name)
{
  Clear();
  for (int i=0; i<12; i++)
  {
    AddItem(name,i);
  }
}

int CTexViewDoc::AddItem(PictureData *pic)
{
	// add a new set
	// check if last set is empty
	// if not, add a new set
	int setIndex =_textures.Size()-1;
	if (setIndex<0 || _textures[setIndex]._itemSet)
	{
		setIndex = _textures.Add();
	}
	TextureSet *set=GetTextureSet(setIndex);
	if( !set ) return -1;

	TextureItem &item=set->_item;
	// find some free position
  item.SetTex(pic);
	item.scale=0;
	set->_w=item.w<<0;
	set->_h=item.h<<0;

	set->_itemSet = true;
	//SetModifiedFlag();
	return 0;
}

int CTexViewDoc::AddItem( const char *name, int level )
{
	// add a new set
	// check if last set is empty
	// if not, add a new set
	int setIndex =_textures.Size()-1;
	if (setIndex<0 || _textures[setIndex]._itemSet)
	{
		setIndex = _textures.Add();
	}
	TextureSet *set=GetTextureSet(setIndex);
	if( !set ) return -1;

	TextureItem &item=set->_item;
	item.SetTexture(name,level);
	// find some free position
	item.scale=level;
	if (!item.GetTex())
	{
		// delete the set
		if (setIndex>0)
		{
			_textures.Delete(setIndex);
		}
		return -1;
	}
	set->_w=item.w<<level;
	set->_h=item.h<<level;

	set->_itemSet = true;
	//SetModifiedFlag();
	return 0;
}

void CTexViewDoc::OnCloseDocument() 
{
	CDocument::OnCloseDocument();
}

BOOL CTexViewDoc::OnOpenDocument(LPCTSTR lpszPathName) 
{
  CWaitCursor wait;
	
  // check old document
  _size = QIFileFunctions::GetFileSize(lpszPathName);
  _time = QIFileFunctions::TimeStamp(lpszPathName);

	Clear();
	for (int i=0; i<12; i++)
	{
		int ret = AddItem(lpszPathName,i);
    if (ret) break;
	}

  CMainFrame *frame = (CMainFrame *)AfxGetMainWnd();
  if (frame)
  {
    frame->InitUpdateDoc(this);
  }

	return TRUE;
}

BOOL CTexViewDoc::OnSaveDocument(LPCTSTR lpszPathName) 
{
	// TODO: save best mipmap
	if (_textures.Size()<=0) return TRUE; // nothing to save

	TextureSet *set=GetTextureSet(0);
  if (!set) return TRUE;

	PictureData *data = set->_item.GetTex();
  if (!data) return TRUE;
	Picture temp = *data;
	if (temp.Save(lpszPathName)>=0)
	{
		SetModifiedFlag(FALSE);
    OnOpenDocument(lpszPathName);
	  return TRUE;
	}
	else
	{
	  ReportSaveLoadException(lpszPathName,NULL,true,IDS_SAVE_ERROR);
	  return FALSE;
	}
}

BOOL CTexViewDoc::DoSave(LPCTSTR lpszPathName, BOOL bReplace)
	// Save the document data to a file
	// lpszPathName = path name where to save document file
	// if lpszPathName is NULL then the user will be prompted (SaveAs)
	// note: lpszPathName can be different than 'm_strPathName'
	// if 'bReplace' is TRUE will change file name if successful (SaveAs)
	// if 'bReplace' is FALSE will not change path name (SaveCopyAs)
{
	CString newName = lpszPathName;
	if (newName.IsEmpty())
	{
		CDocTemplate* pTemplate = GetDocTemplate();
		ASSERT(pTemplate != NULL);

		newName = m_strPathName;
		if (bReplace && newName.IsEmpty())
		{
			newName = m_strTitle;
			// check for dubious filename
			int iBad = newName.FindOneOf(_T(" #%;/\\"));
			if (iBad != -1)
				newName.ReleaseBuffer(iBad);

      // append the default suffix if there is one
      CString strExt;
      if (pTemplate->GetDocString(strExt, CDocTemplate::filterExt) &&
          !strExt.IsEmpty())
      {
        ASSERT(strExt[0] == '.');

        int nSemi = strExt.Find(';');
        if(nSemi>=0)
        {
          strExt = strExt.Left(nSemi);
        }

        newName += strExt;
      }
		}

		if (!AfxGetApp()->DoPromptFileName(newName,
		  bReplace ? AFX_IDS_SAVEFILE : AFX_IDS_SAVEFILECOPY,
		  OFN_HIDEREADONLY | OFN_PATHMUSTEXIST, FALSE, pTemplate))
			return FALSE;       // don't even attempt to save
	}

	CWaitCursor wait;

	if (!OnSaveDocument(newName))
	{
		if (lpszPathName == NULL)
		{
			// be sure to delete the file
			TRY
			{
				CFile::Remove(newName);
			}
			CATCH_ALL(e)
			{
				TRACE0("Warning: failed to delete file after failed SaveAs.\n");
				e->Delete();
			}
			END_CATCH_ALL
		}
		return FALSE;
	}

	// reset the title and change the document name
	if (bReplace)
		SetPathName(newName);

	return TRUE;        // success
}

struct ProcProgressCtx
{
  CMainFrame *frame;
  CTexViewDoc *doc;
};
static void ProcProgress(int y, int h, void *context)
{
  ProcProgressCtx *ctx = (ProcProgressCtx *)context;
  CString progress;
  progress.Format("Generating %d of %d",y+1,h);
  ctx->frame->SetStatusText(progress);
}

void CTexViewDoc::GenerateProcedural(int w, int h, const char *expression)
{
	// perform testing evaluation to detect any error

  CWaitCursor wait;
  CMainFrame *frame = (CMainFrame *)AfxGetMainWnd();
	frame->SetStatusText("Generating");
  Ref<PictureData> pic = new PictureData;
  ProcProgressCtx ctx;
  ctx.frame = frame;
  ctx.doc = this;
  int res = pic->CreateProcedural(w,h,expression,ProcProgress,&ctx);
  if (res<0) return;


  RString source;
  TextureSet *set=GetTextureSet(0);
  if (set)
  {
	  PictureData *data = set->_item.GetTex();
    if (data)
    {
      source = data->GetProcSource();
    }
  }
  if (source.GetLength()==0)
  {
    m_strTitle.Empty();
	  m_strPathName.Empty();      // no path name yet
	  SetModifiedFlag(TRUE);     // make dirty
    UpdateFrameCounts();
  }

  _size = 0;
  _time = 0 ;
	_textures.Clear();
	_textures.Add();

  AddItem(pic);
  if (frame)
  {
    frame->InitUpdateDoc(this);
  }
	UpdateAllViews(NULL);
}

void CTexViewDoc::GenerateMipmaps(GenMipMode mode )
{
	if (_textures.Size()<1 ) return;

  CWaitCursor wait;
	CMainFrame *frame = (CMainFrame *)AfxGetMainWnd();
	frame->SetStatusText("Busy");

	_textures.Resize(1);

	int level = 0;

	TextureHints hints = FindHints(_textures[0]._item.GetName());
	switch (mode)
	{
		case MipDetail:
      hints._mipmapFilter = TexMipFadeOut;
			break;
		case MipNormalizeNormalMap:
      hints._mipmapFilter = TexMipNormalizeNormalMap;
			break;
    case MipNormalizeNormalMapFade:
      hints._mipmapFilter = TexMipNormalizeNormalMapFade;
      break;
		case MipNormal:
      hints._mipmapFilter = TexMipDefault;
			break;
	  case MipAlphaNoise:
	    hints._mipmapFilter = TexMipAlphaNoise;
	    break;
    case MipNormalizeNormalMapNoise:
      hints._mipmapFilter = TexMipNormalizeNormalMapNoise;
      break;
	}

	int last = _textures.Size()-1;
	PictureData *pic = _textures[last]._item.GetTex();
	if (!pic) return;
  Picture temp = *pic;

	pic->MipmapFilter(level,hints);
  
	for(;;)
	{
		if (temp.W()<=2 || temp.H()<=2 ) break;

		if (!temp.MipmapHalfsize(1, 1, hints)) break;
		TextureSet &set=_textures[_textures.Add()];
		level++;

		Picture filtered = temp;
		filtered.MipmapFilter(level, hints);

		TextureItem &item=set._item;
		item.SetTexture(set._item.GetTexture(),level,filtered);
		// find some free position
		item.scale=level;
		set._w=item.w<<level;
		set._h=item.h<<level;

		set._itemSet = true;
	}
	UpdateAllViews(NULL);
}

void CTexViewDoc::OnFileOpen() 
{
  CString defext;
	defext.LoadString(IDS_FILEDEFEXT);
  CString filters;
	filters.LoadString(IDS_FILEOPENFILTERS);
  CFileDialog fdlg
	(
		TRUE,defext,NULL,OFN_HIDEREADONLY|OFN_FILEMUSTEXIST|OFN_LONGNAMES,
		filters,NULL
	);
  if (fdlg.DoModal()!=IDOK) return;

	AfxGetApp()->OpenDocumentFile(fdlg.GetPathName());
}

void CTexViewDoc::OnFileNext() 
{
  if (!InitFind()) return;

  CString path = GetPathName();
  int i = _files.FindKey((const char *)path);
  if (i<0) return;
  i++;
  if (i>=_files.Size()) i=0;

	AfxGetApp()->OpenDocumentFile(_files[i]);
}

void CTexViewDoc::OnFilePrev() 
{
  if (!InitFind()) return;

  CString path = GetPathName();
  int i = _files.FindKey((const char *)path);
  if (i<0) return;
  i--;
  if (i<0) i=_files.Size()-1;

	AfxGetApp()->OpenDocumentFile(_files[i]);
}

void CTexViewDoc::OnUpdateFileNext(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable();
}

void CTexViewDoc::OnUpdateFilePrev(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable();
}

void CTexViewDoc::OnFileRefresh() 
{
  // reread the file
  CString path = GetPathName();
	AfxGetApp()->OpenDocumentFile(path);

}


void CTexViewDoc::OnUpdateFileRefresh(CCmdUI* pCmdUI) 
{
  CString path = GetPathName();
  bool changed = (
    QIFileFunctions::TimeStamp(path)!=_time ||
    QIFileFunctions::GetFileSize(path)!=_size
  );
  pCmdUI->Enable(changed);
}

void CTexViewDoc::OnGenProc() 
{
  if (!ValidateProcedTex())
  {
    _procedTex = new ProcedTex(AfxGetMainWnd(),this);
    _procedTex->Create(ProcedTex::IDD);
    RString source;
  	TextureSet *set=GetTextureSet(0);
    if (set)
    {
	    PictureData *data = set->_item.GetTex();
      if (data)
      {
        source = data->GetProcSource();
        if (source.GetLength()>0)
        {
          _procedTex->_expression = source;
          _procedTex->_w = data->W();
          _procedTex->_h = data->H();
          _procedTex->UpdateData(FALSE);
        }
      }
    }
    
  }
  else
  {
    _procedTex->BringWindowToTop();
  }
  // open procedural texture creation dialogue
	
}

void CTexViewDoc::OnFilterProc() 
{
  TextureSet *set=GetTextureSet(0);
  if (set)
  {
	  PictureData *data = set->_item.GetTex();
    if (data)
    {
      // open procedural filter dialogue
      ProcedFilter filter(AfxGetMainWnd());

      filter._expression = _filter;

      if (filter.DoModal()==IDOK)
      {
        _filter = cc_cast(filter._expression);
        CWaitCursor wait;
        CMainFrame *frame = (CMainFrame *)AfxGetMainWnd();
        ProcProgressCtx ctx;
        ctx.frame = frame;
        ctx.doc = this;
        int res = data->FilterProcedural(filter._expression,ProcProgress,&ctx);
        if (res>=0)
        {
          frame->InitUpdateDoc(this);
        	UpdateAllViews(NULL);
        }
      }
    }
  }
}
