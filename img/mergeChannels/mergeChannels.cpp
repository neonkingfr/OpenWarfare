/*!
\file
Merge channels from two input files.
Any combination of rgba/rgba is possible, including swizzling.
Primary purpose is batch operation on _no./_co. pairs.
*/


#include <direct.h>
#include <io.h>
#include <fcntl.h>
#include <stdarg.h>
#include <string.h>
#include <stdlib.h>
#include <El/elementpch.hpp>
#include <El/math/math3d.hpp>
#include <Es/Framework/debugLog.hpp>
#include <Es/Common/win.h>
#include <Es/Common/fltopts.hpp>
#include <Es/Files/filenames.hpp>
#include <Es/Files/fileContainer.hpp>
#include <Es/Containers/array.hpp>
#include <Es/Strings/rString.hpp>
#include <Es/Strings/bString.hpp>
#include "..\pics\macros.h"
#include "..\pics\fileutil.h"
#include "..\picture\picture.hpp"
#include "..\pal2pace\resource.h"

#include <Es/Framework/consoleBase.h>

static bool InitConfig()
{
	// load texture hits config
	RString retString;
	LoadPictureConfigRetVal ret = LoadPictureConfig(retString);
	if (ret!=LoadPictureConfigOK)
	{
		RString msg;
		UINT msgId = IDS_MSG_NOCONFIG;
		char mod[256];
    HINSTANCE instance = NULL;
		GetModuleFileName(instance,mod,sizeof(mod));
		const char *modname = GetFilenameExt(mod);
		switch (ret)
		{
			case LoadPictureConfigNoRegistry: msgId = IDS_MSG_NOREGISTRY; break;
			case LoadPictureConfigNoFile: msgId = IDS_MSG_NOCONFIG; break;
			case LoadPictureConfigBadFile: msgId = IDS_MSG_BADCONFIG; break;
			case LoadPictureConfigOldExe: msgId = IDS_MSG_CONFIGOLDAPP; break;
		}


		char msgs[1024]; //,buf[1024]; //,app[1024];
		//LoadString(instance,IDS_APPNAME,app,sizeof(app));
		LoadString(instance,msgId,msgs,sizeof(msgs));
		printf(msgs,(const char *)retString,(const char *)modname);
    printf("\n");
		return false;
	}
	return true;
}

/// perform swizzle on a single texel pair
class SwizzleOperator
{
  int _swizzle1[4];
  int _swizzle2[4];
 
  /// swizzle one input
  unsigned int SwizzleInput(unsigned int s, const int *swizzle) const;
  
  public:
  explicit SwizzleOperator(const char *mask);
  
  unsigned int operator () (unsigned int s1, unsigned int s2);
};

SwizzleOperator::SwizzleOperator(const char *mask)
{
  // parse mask
  int cnt = 0;
  while (*mask && cnt<4)
  {
    char c = *mask;
    char channel = tolower(c);
    int id = 0;
    switch (channel)
    {
      case 'a': id = 3; break;
      case 'r': id = 2; break;
      case 'g': id = 1; break;
      case 'b': id = 0; break;
    }
    if (channel!=c)
    {
      // upper case - use source 1
      _swizzle1[3-cnt] = id;
      _swizzle2[3-cnt] = -1; // -1 will eliminate any input
    }
    else
    {
      _swizzle2[3-cnt] = id;
      _swizzle1[3-cnt] = -1;
    }
    cnt++,mask++;
  }
  // any missing channels should be taken directly from a source
  while (cnt<4)
  {
    _swizzle1[3-cnt] = cnt;
    _swizzle2[3-cnt] = -1;
    cnt++;
  }
  
}

unsigned int SwizzleOperator::SwizzleInput(unsigned int s, const int *swizzle) const
{
  unsigned int ret = 0;
  for (int i=0; i<4; i++)
  {
    if (swizzle[i]<0) continue;
    int sShift = swizzle[i]*8;
    int dShift = i*8;
    ret |= ((s>>sShift)&0xff)<<dShift;
  }
  return ret;
}

unsigned int SwizzleOperator::operator () (unsigned int s1, unsigned int s2)
{
  return SwizzleInput(s1,_swizzle1)|SwizzleInput(s2,_swizzle2);
}

static int MergeChannels(
  const char *src, const char *nom, const char *dst, const char *mask
)
{
  printf("%s+%s -> %s\n",src,nom,dst);
  
	Picture srcPic;
	Picture nomPic;
  Picture tgtPic;
	if (srcPic.Load(src)<0)
  {
    fprintf(stderr,"Error loading source file %s\n",src);
		return 1;
  }
	if (nomPic.Load(nom)<0)
  {
    fprintf(stderr,"Error loading source file %s\n",nom);
		return 1;
  }

  int w = intMin(srcPic.W(),nomPic.W());
  int h = intMin(srcPic.H(),nomPic.H());
  if (srcPic.W()!=nomPic.W() || srcPic.H()!=nomPic.H())
  {
    fprintf(
      stderr,"Warning: Source file dimensions do not match (%dx%d!=%dx%d)\n",
      srcPic.W(),srcPic.H(),nomPic.W(),nomPic.H()
    );
  }
  // normal map must not use any dynamic range compression
  // if it does, it is strange
  if (!nomPic.IsDefaultDynamicRange())
  {
    fprintf(
      stderr,"Warning: Normal map %s dynamic range compressed (%.1f %%)\n",
      nom,nomPic.MaxDynamicRangeCompression()*100
    );
    nomPic.DefaultDynamicRange();
  }
  srcPic.DefaultDynamicRange();
  tgtPic = nomPic; // inherit all attributes

  // resize to target size
  tgtPic.Crop(w,h);

  // build a swizzle operator
  SwizzleOperator swizzle(mask);
  
  for (int y=0; y<h; y++) for (int x=0; x<w; x++)
  {
    unsigned int src1 = nomPic.GetPixelARGB(x,y);
    unsigned int src2 = srcPic.GetPixelARGB(x,y);
    tgtPic.SetPixel(x,y,swizzle(src1,src2));
  }
  
	printf("\nSaving...\n");

	// save target picture
	return tgtPic.Save(dst);
}

int consoleMain( int argc, const char *argv[] )
{
  if (!InitConfig())
  {
    return 1;
  }
	
	const char *src = NULL;
	const char *dst = NULL;
	const char *nom = NULL;
	const char *srcPath = NULL;
	const char *dstPath = NULL;
  const char *mergemask = "aRGB";

	while( argc>1 )
	{
		const char *arg=argv[1];
		if( *arg=='-' || *arg=='/' )
		{
			arg++;
			static const char norm[]="norm=";
			static const char mask[]="mask=";
			static const char colorPrefix[]="colorPrefix=";
			static const char dstPrefix[]="dstPrefix=";
			if( !strnicmp(arg,norm,strlen(norm)) )
			{
				arg += strlen(norm);
				nom = arg;
			}
			else if( !strnicmp(arg,mask,strlen(mask)) )
			{
				arg += strlen(mask);
				mergemask = arg;
			}
			else if( !strnicmp(arg,colorPrefix,strlen(colorPrefix)) )
			{
				arg += strlen(colorPrefix);
				srcPath = arg;
			}
			else if( !strnicmp(arg,dstPrefix,strlen(dstPrefix)) )
			{
				arg += strlen(dstPrefix);
				dstPath = arg;
			}
			else goto Usage;
		}
		else
		{
			if( !src ) src=arg;
			else if( !dst ) dst=arg;
			else goto Usage;
		}
		argc--,argv++;
	}

  if (!src)
  {
    Usage:
		printf
		(
			"Usage:\n"
			"mergeChannels [options] source [destination]\n\n"
		  "Options:\n\n"
		  "-mask=<number>      merge mask. Upper case means 1st input,\n"
		  "                    Lower case means 2nd input\n"
		  "                    Target order is ARGB\n"
      "-norm=<filename>    name of the normal map\n"
      "-colorPrefix=<path> prefix to be added before color map names\n"
      "-dstPrefix=<path>   prefix to added before target names\n"
      "\n\n"
      "Color map is assumed as an input, normal map name is created based on its name\n"
      "source may be *.ext to process all _no files, but no other wild-cards are allowed\n"
      "\n\n"
      "Example:\n\n"
      "  mergeChannels-dstPrefix=Dst -mask=aRGB *.tga"
		);
		return 1;
  }
  if (src[0]=='*')
  {
    if (dstPath==NULL)
    {
      fprintf(stderr,"-dstPrefix must be used for wild-card operation\n");
      return 1;
    }
    // special case - for each _no. file 
    // and process it it the pair exists
    BString<1024> noMask;
    strcpy(noMask,"*_no");
    strcat(noMask,src+1);
    _finddata_t fd;
    long f = _findfirst(noMask,&fd);
    if (f>=0)
    {
      do
      {
      	if (fd.attrib&_A_SUBDIR) continue;
        char srcTmp[1024];
        char dstTmp[1024];

	      if (srcPath)
	      {
	        // prepend srcPath to source path
	        strcpy(srcTmp,srcPath);
	        TerminateBy(srcTmp,'\\');
	      }
	      else
	      {
	        strcpy(srcTmp,"");
	      }
	      char *srcCat = srcTmp+strlen(srcTmp);
	      
	      // _no should be before ext
	      strcpy(srcCat,fd.name);
	      strlwr(srcCat);
	      char *noPos = strstr(srcCat,"_no.");
	      if (noPos)
	      {
	        strncpy(noPos,"_co.",4);
	      }
	      
	      strcpy(dstTmp,dstPath);
	      TerminateBy(dstTmp,'\\');
	      strcat(dstTmp,fd.name);
	      
	      MergeChannels(srcTmp,fd.name,dstTmp,mergemask);
      } while (_findnext(f,&fd)==0);
      _findclose(f);
      return 0;
    }
    return 1;
  }
  else
  {
    BString<1024> srcTmp;
	  BString<1024> dstTmp;
	  BString<1024> nomTmp;

    const char *srcExt = GetFileExt(GetFilenameExt(src)); 
    if (!nom)
    {
		  strcpy(nomTmp,src);
		  strcpy(unconst_cast(GetFileExt(GetFilenameExt(nomTmp))),"_no");
      strcat(nomTmp,srcExt);
		  nom = nomTmp;
    }
    if (!dst)
	  {
		  // autogenerate destination name
		  strcpy(dstTmp,nom);
      LString ext = unconst_cast(GetFileExt(dstTmp));
      if (!strcmpi(ext,".paa") && !dstPath)
      {
        // name ends with _no.paa, we will make _no_no.paa from it to avoid overwriting
  		  strcpy(ext,"_no.paa");
      }
      else
      {
		    strcpy(ext,".paa");
      }
		  dst = dstTmp;
	  }
	  if (srcPath)
	  {
	    // prepend srcPath to source path
	    strcpy(srcTmp,srcPath);
	    TerminateBy(unconst_cast((const char *)srcTmp),'\\');
	    strcat(srcTmp,src);
	    src = srcTmp;
  	  
	  }

    BString<1024> dstFull;
    if (dstPath)
    {
		  strcpy(dstFull,dstPath);
	    TerminateBy(unconst_cast((const char *)dstFull),'\\');
	    strcat(dstFull,dst);
	    dst = dstFull;
    }

    return MergeChannels(src,nom,dst,mergemask);
  }
}

