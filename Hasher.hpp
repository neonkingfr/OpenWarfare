#ifndef _GENERIC_HASHER
#define _GENERIC_HASHER

template<class TYPE>
class GenericHasher 
{
public:
  static const size_t bucket_size = 10; // mean bucket size that the container should try not to exceed
  static const size_t min_buckets = (1 << 10); // minimum number of buckets, power of 2, >0
  
  size_t operator()(TYPE key)  const
  {
    size_t hash_value = 0;
    const char *keyVal = key;

    int i=0;
    while (keyVal[i])
    {
      hash_value = 31*hash_value+keyVal[i];
      i++;
    }

    return hash_value;
  }

  bool operator  ()(TYPE left, TYPE right)  const
  {
    return strcmp(left,right);
  }
};

#endif