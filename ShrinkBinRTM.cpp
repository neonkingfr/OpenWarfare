// ShrinkBinRTM.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "ShrinkBinRTM.h"

#include <fstream>

#include "El\QStream\qbStream.hpp"
#include "El\QStream\serializeBin.hpp"
#include "El\Math\mathStore.hpp"
#include "El\ParamFile\paramFile.hpp"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#include "lib/animationMetaData.hpp"

using namespace std;

/*#define MAX_NAMED_SEL 2048
int FindNamedSelNoCaseSensitive( const char *name )
{
  int i;
  for( i=0; i<MAX_NAMED_SEL; i++ )
  {
    if( _namedSel[i] && !stricmp(_namedSel[i]->Name(),name) )
    {
      return i;
    }
  }
  return -1;
}*/

#define MagicLength 8
static const char *BMTRMagic="BMTR";
static const char *RTM_Magic="RTM_";

class RTMdata
{
 Vector3 _step;
 bool _reversed; 
 int _nPhases;
 int _nAnim;
 int _preloadCount;
 bool _lzoCompression;
 AutoArray<RString> _selections;
 AutoArray<float> _phaseTimes;
 AutoArray<AutoArray<Matrix4Quat16b>> _buffer;
 AnimMetaInfo _metaInfo;

 int _version;

 int SerializeBin(SerializeBinStream &f);

public:
  RTMdata()
  {
    _preloadCount=0;
    _nPhases = 0;
    _lzoCompression = true;
    _version = INT_MAX;
  }

  enum {
    RtmFail=-1,
    RtmBin,
    RtmRaw
  };
  int Open(const char *file)
  {
    ifstream in;
    in.open(file,ios::binary|ios::in);
    float xStep=0,yStep=0,zStep=0;
    int nAnim=0,nSel=0;

    char magic[MagicLength+1];
    in.read(magic,MagicLength);
    magic[MagicLength]=0;
    in.close();

    if (!strncmp(BMTRMagic,magic,4)) // binarized RTM
    {
      QIFStreamB inqif;
      inqif.AutoOpen(file);
      if (inqif.fail() || inqif.rest()<=0) return -1; //RptF("Animation %s not found or empty",file);
      SerializeBinStream f(&inqif);
      SerializeBin(f);
      inqif.close();
      return RtmBin;
    }
    else if (!strncmp(RTM_Magic,magic,4)) // raw RTM
    {
      return RtmRaw;
    }
    return RtmFail;
  }
  int Save(const char *file)
  {
    QOFStream inqif;
    inqif.open(file);
    SerializeBinStream f(&inqif);
    int result = SerializeBin(f);
    inqif.close();
    return result;
  }
    
  int Resize(int resize)
  {
    if (_nPhases<=1) return 0; //special case, vizems etc.
    if (!resize)
    { //special case: we want to preserve only first and last phases of animation (for Lite server addons)
      if (_nPhases<=1) return -1;
      _phaseTimes[1] = _phaseTimes[_nPhases-1];
      _buffer[1] = _buffer[_nPhases-1];
      _nPhases = 2;
      _phaseTimes.Resize(_nPhases);
      _phaseTimes.Realloc(_nPhases);
      _buffer.Resize(_nPhases);
      _buffer.Realloc(_nPhases);
    }
    else
    {
      if (_nPhases<=resize) return -1;
      int factor = _nPhases/resize;
      _nPhases /= factor;
      for ( int i=1; i<_phaseTimes.Size()/factor; i++ )
      {
        _phaseTimes[i] = _phaseTimes[i*factor];
        _buffer[i] = _buffer[i*factor];
      }
      _phaseTimes.Resize(_phaseTimes.Size()/factor);
      _buffer.Resize(_buffer.Size()/factor);
      _phaseTimes.Realloc(_phaseTimes.Size());
      _buffer.Realloc(_buffer.Size());
    }
    return 0;
  }
};

int RTMdata::SerializeBin(SerializeBinStream &f)
{
  if (!f.Version('RTMB')) return -1;

  const int actVersion = 4; // 4 for metadata
  const int minVersion = 2;

  if (f.IsLoading())
  { 
    if (!f.Version(minVersion, actVersion))
    {
      f.SetError(SerializeBinStream::EBadVersion);
      return -1;
    }

    _version = f.GetVersion();
  }
  else
  {
    f.Version(minVersion,_version);
  }

  if (f.GetVersion() >= 3) f.UseLZOCompression();

  f.Transfer(_reversed);
  f.Transfer(_step[0]);
  f.Transfer(_step[1]);
  f.Transfer(_step[2]);

  f.Transfer(_nPhases);
  f.Transfer(_preloadCount);
 
  if (f.IsSaving())
  {
    f.SaveInt(_nAnim);
  }
  else
  {
    _nAnim = f.LoadInt();
    _lzoCompression = f.IsUsingLZOCompression();
  }

  f.TransferBasicArray(_selections);

  // serialize meta information
  if (f.GetVersion() >= AnimMetaInfo::AnimFileVersion)
  {
    _metaInfo.SerializeBin(f);
  }

  f.TransferBinaryArray(_phaseTimes);

  if (_nPhases > 0)
  {
    _buffer.Realloc(_nPhases);
    _buffer.Resize(_nPhases);
    for (int i=0; i<_nPhases; i++)
    {
      f.TransferBinaryArray(_buffer[i]);
    }
  }
  return 0;
}


int main(int argc, char* argv[], char* envp[])
{
  if (argc<4) return 0;

  RTMdata rtm;
  switch ( rtm.Open(argv[2]) )
  {
  case RTMdata::RtmBin:
    if (rtm.Resize(atoi(argv[1]))==0)
    {
      if(rtm.Save(argv[3])!=0)
      {
        printf("Unable to save %s",argv[3]);
      }
    }
    return 0;
    break;
  case RTMdata::RtmRaw:
    {
      // just copy the source file over the destination
      if (_stricmp(argv[2], argv[3]) != 0) // do not copy to itself
      { 
        QIFStream in;
        in.open(argv[2]);
        QOFStream out;
        out.open(argv[3]);
        in.copy(out);
        out.close();
        in.close();    
      }
      return 0;
    }
  case RTMdata::RtmFail:
    printf("Unable to load %s",argv[2]);
    return 1;
  }
    
	return 0;
}
